﻿using System;
using System.Text;
using System.Xml;
using System.Collections;

namespace DataQueryParser
{
	#region ParserBase

	/// <summary>
	/// An abstract class which is used as the base class for parsers.
	/// It contains basic methods which allow you to create a logical tree
	/// from a document.
	/// </summary>
	public abstract class ParserBase
	{
		#region Consts

		/// <summary>
		/// The name of the Value xml attribute.
		/// </summary>
		protected const string cValueXmlAttributeName = "Value";

		/// <summary>
		/// The name of the Tag Type xml attribute.
		/// </summary>
		protected const string cTagTypeXmlAttributeName = "Type";

		/// <summary>
		/// The name of the root node in the xml representation of a parsed text.
		/// </summary>
		protected const string cRootXmlNodeName = "ParsedDocument";
		
		/// <summary>
		/// The name of the Tag xml node.
		/// </summary>
		protected const string cTagXmlNodeName = "Tag";

		/// <summary>
		/// The name of the Text xml node.
		/// </summary>
		protected const string cTextXmlNodeName = "Text";

		/// <summary>
		/// The white space text (is used when adding white 
		/// spaces between text elements).
		/// </summary>
		public const string cWhiteSpace = " ";

		/// <summary>
		/// The new line string.
		/// </summary>
		public const string cNewLine = "\r\n";

		#endregion

		#region Fields

		/// <summary>
		/// The tree representation of a parsed document as an xml document.
		/// </summary>
		private XmlDocument fParsedDocument;

		#endregion

		#region Methods

		/// <summary>
		/// Parses the specified text.
		/// </summary>
		public void Parse(string text)
		{
			#region Check the arguments

			if (text == null)
				throw new ArgumentNullException("text");

			#endregion

			fParsedDocument = new XmlDocument();
			XmlNode myRootNode = fParsedDocument.CreateElement(cRootXmlNodeName);
			fParsedDocument.AppendChild(myRootNode);

			try
			{
				ParseBlock(myRootNode, null, text, 0);
			}
			catch
			{
				fParsedDocument = null;
				throw;
			}
		}

        public ArrayList GetOutputColumn()
        {
            ArrayList list = new ArrayList();
            foreach (XmlNode elm in fParsedDocument.FirstChild.ChildNodes)
            {
                if (elm.Attributes["Type"].Value.Trim().ToLower() == "select")
                {
                    string strOut = "";
                    foreach (XmlNode innerElm in elm.ChildNodes)
                    {
                        if (innerElm.Name.Trim().ToLower() == "text")
                        {
                            if (innerElm.Attributes["Value"].Value == ",")
                            {
                                if(strOut.Length>0) list.Add(strOut);
                                strOut = "";
                            }
                            else strOut = innerElm.Attributes["Value"].Value;
                        }
                        else if (innerElm.Name.Trim().ToLower() == "tag")
                        {
                            //if (innerElm.Attributes["Type"].Value.ToUpper() == "BRACES")
                            if (innerElm.Attributes["Type"].Value.ToUpper() == "SQUARE_BRACKET")
                            {
                                foreach (XmlNode innerMostElm in innerElm.ChildNodes)
                                {
                                    if (innerMostElm.Name.Trim().ToLower() == "text") strOut = innerMostElm.Attributes["Value"].Value;
                                }
                            }
                            else if (innerElm.Attributes["Type"].Value.ToUpper() == "QUOTED_IDENTIFIER") strOut = innerElm.Attributes["Value"].Value;
                            else if (innerElm.Attributes["Type"].Value.ToUpper() == "STRING_LITERAL") strOut = innerElm.Attributes["Value"].Value;
                        }
                    }
                    if(strOut.Length>0) list.Add(strOut);
                }
            }
            return list;
        }

        public ArrayList GetTables()
        {
            ArrayList list = new ArrayList();
            foreach (XmlNode elm in fParsedDocument.FirstChild.ChildNodes)
            {
                if (elm.Attributes["Type"] != null)
                {
                    if (elm.Attributes["Type"].Value.Trim().ToLower() == "from" || elm.Attributes["Type"].Value.Trim().ToLower() == "join")
                    {
                        string strOut = "";
                        foreach (XmlNode innerElm in elm.ChildNodes)
                        {
                            if (innerElm.Name.Trim().ToLower() == "text")
                            {
                                if (innerElm.Attributes["Value"].Value == ",")
                                {
                                    if (strOut.Length > 0) list.Add(strOut);
                                    strOut = "";
                                }
                                else strOut = innerElm.Attributes["Value"].Value;
                                break;
                            }
                        }
                        if (strOut.Length > 0) list.Add(strOut);
                    }
                }
            }
            return list;
        }

		/// <summary>
		/// Parses the specified block of a text.
		/// </summary>
		/// <returns>
		/// Returns the end position of the parsed block.
		/// </returns>
		protected int ParseBlock(XmlNode parentNode, TagBase parentTag, string text, int position)
		{
			#region Check the arguments

			if (parentNode == null)
				throw new ArgumentNullException("parentNode");

			CheckTextAndPositionArguments(text, position);

			#endregion

			while (position < text.Length)
			{
				if (IsSkipWhiteSpace)
					SkipWhiteSpace(text, ref position);

				if (position == text.Length)
					break;

				#region Read the parent tag ending

				if (parentTag != null)
				{
					int myParentTagEndingEndPosition = parentTag.MatchEnd(text, position);
					if (myParentTagEndingEndPosition >= 0)
					{
						position = myParentTagEndingEndPosition;
						return position;
					}
				}

				#endregion

				Type myTagType = IsTag(text, position);
				if (myTagType != null)
				{
					#region Read a tag

					#region Create the tag class instance

					TagBase myTag = Activator.CreateInstance(myTagType) as TagBase;
					position = myTag.InitializeFromText(this, text, position, parentTag);

					#endregion

					#region Create an xml node for the tag

					XmlNode myTagXmlNode = CreateTagXmlNode(myTag);
					parentNode.AppendChild(myTagXmlNode);

					#endregion

					if (myTag.HasContents)
						position = ParseBlock(myTagXmlNode, myTag, text, position);

					#endregion
				}
				else
				{
					#region Read text

					string myText = ReadWordOrSeparator(text, ref position, !IsSkipWhiteSpace);
					parentNode.AppendChild(CreateTextXmlNode(myText));

					#endregion
				}
			}

			if (parentTag != null && !parentTag.CanTerminateByStringEnd)
				throw new Exception("Invalid format");

			return position;
		}

		/// <summary>
		/// Checks whether there is a tag in the text at the specified position, and returns its type.
		/// </summary>
		protected Type IsTag(string text, int position)
		{
			foreach (Type myTagType in Tags)
			{
				MatchTagAttributeBase myMatchTagAttribute = TagBase.GetTagMatchAttribute(myTagType);
				if (myMatchTagAttribute.Match(text, position))
					return myTagType;
			}

			return null;
		}

		/// <summary>
		/// Creates an xml node for the specified tag.
		/// </summary>
		protected XmlNode CreateTagXmlNode(TagBase tag)
		{
			#region Check the arguments

			if (tag == null)
				throw new ArgumentNullException();

			#endregion

			CheckXmlDocInitialized();

			XmlElement myTagNode = fParsedDocument.CreateElement(cTagXmlNodeName);

			XmlAttribute myTypeAttribute = fParsedDocument.CreateAttribute(cTagTypeXmlAttributeName);
			myTypeAttribute.Value = tag.Type;
			myTagNode.Attributes.Append(myTypeAttribute);

			if (tag.Value != null)
			{
				XmlAttribute myValueAttribute = fParsedDocument.CreateAttribute(cValueXmlAttributeName);
				myValueAttribute.Value = tag.Value;
				myTagNode.Attributes.Append(myValueAttribute);
			}

			return myTagNode;
		}

		/// <summary>
		/// Creates an xml node for the specified text.
		/// </summary>
		protected XmlNode CreateTextXmlNode(string text)
		{
			#region Check the arguments

			if (text == null)
				throw new ArgumentNullException("text");

			#endregion

			CheckXmlDocInitialized();

			XmlElement myTextNode = fParsedDocument.CreateElement(cTextXmlNodeName);

			XmlAttribute myValueAttribute = fParsedDocument.CreateAttribute(cValueXmlAttributeName);
			myValueAttribute.Value = text;
			myTextNode.Attributes.Append(myValueAttribute);

			return myTextNode;
		}

		/// <summary>
		/// Skips the white space symbols located at the specified position.
		/// </summary>
		public static void SkipWhiteSpace(string text, ref int position)
		{
			#region Check the parameters

			CheckTextAndPositionArguments(text, position);

			#endregion

			while (position < text.Length)
			{
				if (!char.IsWhiteSpace(text, position))
					break;
				position++;
			}
		}

		/// <summary>
		/// Reads a single word or separator at the specified position.
		/// </summary>
		public static string ReadWordOrSeparator(string text, ref int position, bool treatWhiteSpaceAsSeparator)
		{
			#region Check the parameters

			CheckTextAndPositionArguments(text, position);

			#endregion

			int myStartPosition = position;

			while (position < text.Length)
			{
				#region Check is white space

				if (char.IsWhiteSpace(text, position))
				{
					if (position == myStartPosition && treatWhiteSpaceAsSeparator)
					{
						if (position + cNewLine.Length <= text.Length && text.Substring(position, cNewLine.Length) == cNewLine)
							position += cNewLine.Length;
						else
							position += 1;
					}
					break;
				}

				#endregion

				#region Check is separator

				if (!char.IsLetterOrDigit(text, position) && text[position] != '_')
				{
					if (position == myStartPosition)
						position++;
					break;
				}

				#endregion

				position++;
			}

			if (position == myStartPosition)
				return string.Empty;

			return text.Substring(myStartPosition, position - myStartPosition);
		}

		/// <summary>
		/// Checks the text and position parameters.
		/// </summary>
		public static void CheckTextAndPositionArguments(string text, int position)
		{
			#region Check the parameters

			if (text == null)
				throw new ArgumentNullException("text");

			if (position < 0 || position >= text.Length)
				throw new ArgumentOutOfRangeException("position");

			#endregion
		}

		/// <summary>
		/// Checks whether the m_ParsedDocument is initialized.
		/// </summary>
		protected void CheckXmlDocInitialized()
		{
			if (fParsedDocument == null)
				throw new Exception("Xml document is not initialized");
		}

		#endregion

		#region Properties

		/// <summary>
		/// Indicates whether the white space is considered as a non-valuable
		/// character.
		/// </summary>
		protected abstract bool IsSkipWhiteSpace { get; }

		/// <summary>
		/// The list of all available tags.
		/// </summary>
		protected abstract Type[] Tags { get; }

		/// <summary>
		/// Gets the parsed document in xml format.
		/// </summary>
		protected XmlDocument ParsedDocument
		{
			get
			{
				CheckXmlDocInitialized();

				return fParsedDocument;
			}
		}

		#endregion
	}

	#endregion
}
