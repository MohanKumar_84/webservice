﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Threading;
using System.Net;

//using System.Web.Security;
/// <summary>
/// Summary description for clsHTTP
/// </summary>
/// 
namespace mFicient
{
    public class HTTP
    {
        internal String strURL = "";
        private String UseURL = "";
        internal int Retry = 0, Timeout = 30000;
        private String strPostData = "", strParamJoiner = "";
        private int intPostAttempts = 0;
        internal int RetryDelay = 50;
        internal String ResponseText = "", ValidResponse = "";
        internal String InvalidResponse = "";
        internal Boolean blnRequestSuccess = false;
        internal String ErrorMessage = "";
        private Byte[] ContentData;
        private String strRequestMethod = "GET", strUsername = "", strPassword = "";
        private Boolean blnAuthenticationRequired = false;
        private Boolean blnWaitForResponse = true;
        private string strUserAgent = string.Empty;

        public HTTP(String URL)
        {
            int intPostQstnMark = 0;
            strURL = URL;
            try
            {
                intPostQstnMark = strURL.IndexOf("?");
                if (intPostQstnMark > 0)
                {
                    UseURL = strURL.Substring(0, intPostQstnMark);
                    strPostData = strURL.Substring(intPostQstnMark + 1, strURL.Length - UseURL.Length - 1);
                    strParamJoiner = "&";
                }
                else
                {
                    UseURL = strURL;
                }
            }
            catch
            {
                UseURL = strURL;
            }
        }
        public HTTP(String URL, Boolean _blnWaitForResponse)
        {
            int intPostQstnMark = 0;
            strURL = URL;
            blnWaitForResponse = _blnWaitForResponse;
            try
            {
                intPostQstnMark = strURL.IndexOf("?");
                if (intPostQstnMark > 0)
                {
                    UseURL = strURL.Substring(0, intPostQstnMark);
                    strPostData = strURL.Substring(intPostQstnMark + 1, strURL.Length - UseURL.Length - 1);
                    strParamJoiner = "&";
                }
                else
                {
                    UseURL = strURL;
                }
            }
            catch
            {
                UseURL = strURL;
            }
        }
        internal String URL
        {
            get
            {
                URL = strURL;
                return URL;
            }
            set
            {
                int intPostQstnMark = 0;
                strURL = value;
                try
                {
                    intPostQstnMark = strURL.IndexOf("?");
                    if (intPostQstnMark > 0)
                    {
                        UseURL = strURL.Substring(0, intPostQstnMark);
                        strPostData = strURL.Substring(intPostQstnMark + 1, strURL.Length - UseURL.Length - 1);
                        strParamJoiner = "&";
                    }
                    else
                    {
                        UseURL = strURL;
                    }
                }
                catch
                {
                    UseURL = strURL;
                }
            }
        }
        internal string UserAgent
        {
            set
            {
                strUserAgent = value;
            }
        }
        internal Boolean HttpAuthenticationRequired
        {
            get
            {
                HttpAuthenticationRequired = blnAuthenticationRequired;
                return HttpAuthenticationRequired;
            }
            set
            {
                blnAuthenticationRequired = value;
            }
        }
        internal void SetHttpCredentials(String UID, String PWD)
        {
            strUsername = UID;
            strPassword = PWD;
        }
        internal void AddRequestData(String VarName, String VarValue)
        {
            if (VarName.Trim().Length <= 0)
            {
                return;
            }
            strPostData = strPostData + strParamJoiner + VarName + "=" + System.Web.HttpUtility.UrlEncode(VarValue);
            strParamJoiner = "&";
        }
        internal void ClearRequestData()
        {
            strPostData = "";
            strParamJoiner = "";
        }

        internal enum EnumHttpMethod
        {
            HTTP_POST = 1,
            HTTP_GET = 2
        };

        internal EnumHttpMethod HttpRequestMethod
        {
            get
            {
                switch (strRequestMethod.Trim().ToUpper())
                {
                    case "POST":
                        HttpRequestMethod = EnumHttpMethod.HTTP_POST;
                        break;
                    case "GET":
                        HttpRequestMethod = EnumHttpMethod.HTTP_GET;
                        break;
                }
                return HttpRequestMethod;
            }
            set
            {
                switch (value)
                {
                    case EnumHttpMethod.HTTP_GET:
                        strRequestMethod = "GET";
                        break;
                    case EnumHttpMethod.HTTP_POST:
                        strRequestMethod = "POST";
                        break;
                }
            }
        }

        public HttpResponseStatus Request()
        {
            HttpStatusCode StatusCode = HttpStatusCode.NotFound;
            blnRequestSuccess = false;
            try
            {
                int i = 0;
                intPostAttempts = 0;
                ResponseText = "";
                ErrorMessage = "";
                if (UseURL.Trim().Length <= 0)
                {
                    ErrorMessage = "URL not defined for HTTP request";
                    blnRequestSuccess = false;
                }
                try
                {
                    if (strRequestMethod == "POST")
                    {
                        ContentData = System.Text.Encoding.UTF8.GetBytes(strPostData);
                    }
                }
                catch (Exception ex)
                {
                    ErrorMessage = ex.Message;
                    blnRequestSuccess = false;
                }
                HttpWebRequest myHttpWebRequest = null;
                HttpWebResponse myHttpWebResponse = null;
                StreamReader myHttpWebReader = null;
                String myHttpWebResult = "";
                Stream myHttpRequestStream = null;
                NetworkCredential myHttpCredentials = null;
                for (i = 0; i <= Retry; i++)
                {
                    ErrorMessage = "";
                    ResponseText = "";
                    try
                    {
                        //Creates an HttpWebRequest with the specified URL.
                        switch (strRequestMethod)
                        {
                            case "POST":
                                myHttpWebRequest = (HttpWebRequest)WebRequest.Create(UseURL);
                                break;
                            case "GET":
                                if (strPostData.Trim().Length > 0)
                                {
                                    myHttpWebRequest = (HttpWebRequest)WebRequest.Create(UseURL + "?" + strPostData);
                                }
                                else
                                {
                                    myHttpWebRequest = (HttpWebRequest)WebRequest.Create(UseURL);
                                }
                                break;
                        }

                        myHttpWebRequest.Timeout = Timeout;
                        myHttpWebRequest.AllowAutoRedirect = true;
                        myHttpWebRequest.MaximumAutomaticRedirections = 10;
                        myHttpWebRequest.KeepAlive = true;
                        myHttpWebRequest.Method = strRequestMethod;
                        if (!string.IsNullOrEmpty(strUserAgent)) myHttpWebRequest.UserAgent = strUserAgent;

                        if (strRequestMethod == "POST")
                        {
                            myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                            myHttpWebRequest.ContentLength = ContentData.Length;
                            myHttpRequestStream = myHttpWebRequest.GetRequestStream();
                            myHttpRequestStream.Write(ContentData, 0, ContentData.Length);
                            myHttpRequestStream.Close();
                        }
                        myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                        StatusCode = myHttpWebResponse.StatusCode;
                        // wait for Response 
                        if (blnWaitForResponse)
                        {
                            if (StatusCode == HttpStatusCode.OK)
                            {

                                myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                myHttpWebResult = myHttpWebReader.ReadToEnd();
                                ResponseText = myHttpWebResult;
                                blnRequestSuccess = true;
                            }
                            else
                            {
                                try
                                {
                                    myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                    myHttpWebResult = myHttpWebReader.ReadToEnd();
                                    ResponseText = myHttpWebResult;
                                }
                                catch
                                {
                                }
                                if (ResponseText.Trim().Length <= 0)
                                {
                                    ResponseText = myHttpWebResponse.StatusDescription;
                                }
                                if (ResponseText.Trim().Length <= 0)
                                    ResponseText = "ERROR " + myHttpWebResponse.StatusCode.ToString();
                                ErrorMessage = ResponseText;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        if (myHttpRequestStream != null)
                        {
                            myHttpRequestStream.Close();
                            myHttpRequestStream = null;
                        }
                        if (myHttpWebReader != null)
                        {
                            myHttpWebReader.Close();
                            myHttpWebReader = null;
                        }
                        if (myHttpWebResponse != null)
                        {
                            myHttpWebResponse.Close();
                            myHttpWebResponse = null;
                        }
                        if (myHttpWebRequest != null) myHttpWebRequest = null;
                        if (myHttpCredentials != null) myHttpCredentials = null;
                    }

                    if (blnRequestSuccess || !blnWaitForResponse)
                    {
                        break;
                    }
                    if (i < Retry) Thread.Sleep(RetryDelay);
                    Thread.Sleep(1);
                }
                if (!blnRequestSuccess && ErrorMessage.Trim().Length <= 0)
                {
                    ErrorMessage = "HTTP Request Failed";
                }
            }
            catch
            {
            }
            return new HttpResponseStatus(blnRequestSuccess, StatusCode, ErrorMessage, ResponseText);
        }
    }
}