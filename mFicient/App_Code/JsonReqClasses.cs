﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicient
{
    public class JsonRequestClasses<T>
    {
        [DataMember]
        public T req { get; set; }
    }

    [DataContract]
    public class RequestCommonFields
    {

        /// <summary>
        /// function code
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// request id 
        /// </summary>
        [DataMember]
        public string rid { get; set; }
    }
    public class JsonReqCompanyRegistration : RequestCommonFields
    {
        /// <summary>
        /// data
        /// </summary>
        [DataMember]
        public JsonReqCompanyRegistrationData data { get; set; }
    }
    public class JsonReqCompanyRegistrationData
    {
        /// <summary>
        /// Company Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [DataMember]
        public string fnm { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        [DataMember]
        public string mnm { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [DataMember]
        public string lnm { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        /// Mobile No
        /// </summary>
        [DataMember]
        public string mob { get; set; }

        /// <summary>
        /// Enterprise name
        /// </summary>
        [DataMember]
        public string ennm { get; set; }

        /// <summary>
        /// Street address 1
        /// </summary>
        [DataMember]
        public string add1 { get; set; }

        /// <summary>
        /// Street address 2
        /// </summary>
        [DataMember]
        public string add2 { get; set; }

        /// <summary>
        /// Street add 3
        /// </summary>
        [DataMember]
        public string add3 { get; set; }

        /// <summary>
        /// City
        /// </summary>
        [DataMember]
        public string city { get; set; }

        /// <summary>
        ///State
        /// </summary>
        [DataMember]
        public string state { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [DataMember]
        public string cntry { get; set; }

        /// <summary>
        /// Timezone ID
        /// </summary>
        [DataMember]
        public string tzn { get; set; }

        /// <summary>
        /// zip
        /// </summary>
        [DataMember]
        public string zip { get; set; }

        /// <summary>
        /// email
        /// </summary>
        [DataMember]
        public string em { get; set; }
    }
    public class JsonReqGetCountryCodes : RequestCommonFields
    {
        /// <summary>
        /// data
        /// </summary>
        [DataMember]
        public JsonReqGetCountryCodesData data { get; set; }
    }
    public class JsonReqGetCountryCodesData
    {
        /// <summary>
        /// Company Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }
    }
    public class JsonReqGetTimezoneList : RequestCommonFields
    {
        /// <summary>
        /// data
        /// </summary>
        [DataMember]
        public JsonReqGetTimezoneListData data { get; set; }
    }
    public class JsonReqGetTimezoneListData 
    {
        /// <summary>
        /// Company Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }
    }
}