﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicient
{
    [DataContract]
    public class ResponseStatus
    {
        //status code
        [DataMember]
        public string cd { get; set; }

        //status description
        [DataMember]
        public string desc { get; set; }
    }
    [DataContract]
    public class JsonResponseParser<T>
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public ResponseFields<T> resp { get; set; }
    }
    [DataContract]
    public class ResponseCommonFields
    {

        /// <summary>
        /// function code from response
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [DataMember]
        public ResponseStatus status { get; set; }
    }
    public class ResponseFields<T> : ResponseCommonFields
    {
        /// <summary>
        /// data from response
        /// </summary>
        [DataMember]
        public T data { get; set; }
    }
}