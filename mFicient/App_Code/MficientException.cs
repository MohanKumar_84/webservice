﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicient
{
    [Serializable]
    public class MficientException : ApplicationException
    {
        public MficientException() { }
        public MficientException(string message) : base(message) { }
        public MficientException(string message, Exception inner) : base(message, inner) { }
        protected MficientException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}