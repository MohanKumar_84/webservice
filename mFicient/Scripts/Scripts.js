﻿//Modal pop up action buttons are center aligned
//we have to change the width of div according to the 
//no of buttons in the action div.
var POP_UP_ACTION_DIV_2 = 50; //for 2 buttons
var POP_UP_ACTION_DIV_3 = 77; //for 3 buttons
var POP_UP_ACTION_DIV_MOZ_2 = 60; //for 2 buttons
var POP_UP_ACTION_DIV_MOZ_3 = 90; //for 3 buttons

function showModalPopUp(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == undefined) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    $('.ui-dialog-titlebar-close').show();
}

function xferBack() {
    document.forms[0].submit();
    return true;
}

function showModalPopUpWithOutHeader(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == undefined) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    $('.ui-dialog-titlebar-close').hide();
}
function closeModalPopUp(divIdToClose) {
    $('#' + divIdToClose).dialog('close');
}
function showAlert(message, divIdToShowMsgWithHash) {
    $.wl_Alert(message, 'info', divIdToShowMsgWithHash);
}
function showWait() {
    $("#divWaitBox").dialog({
        modal: true,
        width: 0,
        height: 0,
        closeOnEscape: false,
        resizable: false,
        autoOpen: true,
        open: function () {
            //$(".ui-dialog-titlebar-close", $(this).parent()).hide(); //hides the little 'x' button
            $("#divWaitBox").siblings(".ui-dialog-titlebar").hide();
        }
    });
}
function hideWait() {
    $("#divWaitBox").dialog("close");
    $(".ui-dialog-titlebar").show();
}

function hideHTMLElements(commaSeparatedIdsOfElements) {
    var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
    for (var i = 0; i < aryOfElements.length; i++) {
        $('#' + aryOfElements[0]).hide();
    }
}

Repeater =
{
    ADD_USER_USER_DETAIL: 0
}

function showHTMLElements(commaSeparatedIdsOfElements) {
    var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
    for (var i = 0; i < aryOfElements.length; i++) {
        $('#' + aryOfElements[0]).show();
    }
}

function stopDefAction(evt) {
    evt.preventDefault();
}

function showWaitModal() {
    $("#divWaitBox").dialog({
        modal: true,
        closeOnEscape: false,
        resizable: false,
        autoOpen: true,
        dialogClass: "ui-dialog-waitbox-spinner"
    });
    if ($('#divWaitBox').length > 0) {

        if ($($('#divWaitBox')[0].parentElement).length > 0) {
            $($('#divWaitBox')[0].parentElement).width(60);
            setModalPosition($('#divWaitBox').parent());
            //setModalPosition($(window));
            $($('#divWaitBox')[0].previousElementSibling).hide();
            $($('#divWaitBox')[0].parentElement).addClass("waitBoxCorner");
        }
    };
}

function hideWaitModal() {
    $("#divWaitBox").dialog("close");
}

DialogType = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showDialogImage(type, divToPrependImg) {

    var divToPrependImage = getDivToPrependImage(divToPrependImg);
    if (divToPrependImage) {
        if ($.browser.msie) {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError");
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning");
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo");
                    break;
            }
        }
        else if ($.browser.webkit) {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError dialogErrorWebKit");
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning dialogWarningWebKit");
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo dialogInfoWebKit");
                    break;
            }
        }
        else {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    break;
            }
        }


    }
}
function getDivToPrependImage(divToPrependImage) {
    if (divToPrependImage == undefined) {
        return $('#wl_dialog_alert').prev();
    }
    else {
        return $('#' + divToPrependImage).prev();
    }
}
function removeDialogImage(divToRemoveImage) {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    if (divToRemoveImage == undefined) {
        $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove();
    }
    else {
        $('#' + divToRemoveImage).prev().find($('#dialogImageId')).remove();
    }

}
function getDialogImageSpan(divToPrependImage) {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    if (divToPrependImage == undefined) {
        return $('#wl_dialog_alert').prev().find('#dialogImageId');
    }
    else {
        return $('#' + divToPrependImage).prev().find('#dialogImageId');
    }

}
function stopDefualtOfDateTxtBox(txtBox) {
    if (txtBox) {
        txtBox.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
    }
}


function rptRowClick(row, repeater) {
    processRowClick(row);
}
function processRowClick(row) {
    var hidUsedAfterPostBack = $('[id$=hidRowValuesUsedAfterPostBack]');
    var strValuesReqAfterPostBack = '';
    $.each(row.children, function () {
        if (this.title == "id") {
            var td = this;
            $.each(td.children, function () {
                if (strValuesReqAfterPostBack.length == 0)
                    strValuesReqAfterPostBack += ($.trim(this.innerHTML));
                else
                    strValuesReqAfterPostBack += ($.trim(';' + this.innerHTML));
            })
            hidUsedAfterPostBack.val(strValuesReqAfterPostBack);
        }
    });
    UpdateRowClick();
}
function UpdateRowClick() {
    $('[id$=btnRowClickPostBack]').click();
}
function rptHeaderClickForSorting(header, hidFieldUsedForInfo) {
    UpdateRowClick();
}

function setWidthOfDropDown() {
    $("select").each(function (i) {
        var parentDiv = this.parent("div");
        if (parentDiv && parentDiv != undefined) {
            this.width(parentDiv.outerWidth(false));
        }
    })
}
MessageOption = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showMessage(message, optionForScript, dialogImageType) {
    var strOption = '';
    switch (optionForScript) {
        case DialogType.Error:
            strOption = '$.alert.Error';
            break;
        case DialogType.Info:
            strOption = '$.alert.Information';
            break;
    }
    $.alert(message, strOption); showDialogImage(dialogImageType);
}

function showConfirmation(message) {

    var blnConfirm = confirm(message);
    if (blnConfirm) {
        isCookieCleanUpRequired('false');
    }
    return blnConfirm;
}

COOKIE_MSID = "MFMSID";
COOKIE_MUEID = "MFMUEID";

function createCookie(cookieName, cookieValue, cookieExpiryMins) {
    if (cookieExpiryMins) {
        var date = new Date();
        date.setTime(date.getTime() + (cookieExpiryMins * 60 * 1000));
        var expires = "; expires=" + date.toUTCString();
    }
    else var expires = "";
    document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
//right now it is used only changing the divs of modal pop up
//action divs.So do the firefox related changes here only.
//But change the name of function after testing.
function changeWidthOfDiv(divId, iWidth, inPercent) {
    var divToChange = $('#' + divId);
    if ($.browser.mozilla) {
        if (iWidth === POP_UP_ACTION_DIV_2) iWidth = POP_UP_ACTION_DIV_MOZ_2;
        else if (iWidth === POP_UP_ACTION_DIV_3) iWidth = POP_UP_ACTION_DIV_MOZ_3;
    }
    if (inPercent) {
        $(divToChange).width(iWidth.toString() + "%");
    }
    else {
        $(divToChange).width(iWidth);
    }
}
function changeWidthOfActionDiv(divId, iWidth, inPercent) {
    var divToChange = $('#' + divId);
    if (inPercent) {
        $(divToChange).width(iWidth.toString() + "%");
    }
    else {
        $(divToChange).width(iWidth);
    }
}

function cancelEventBubbling(e) {
    if (!e)
        e = window.event;

    //IE9 & Other Browsers
    if (e.stopPropagation) {
        e.stopPropagation();
    }
    //IE8 and Lower
    else {
        e.cancelBubble = true;
    }
}
function childHandler(e) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    alert('child clicked');

}; //Modal pop up action buttons are center aligned
//we have to change the width of div according to the 
//no of buttons in the action div.
var POP_UP_ACTION_DIV_2 = 50; //for 2 buttons
var POP_UP_ACTION_DIV_3 = 77; //for 3 buttons
var POP_UP_ACTION_DIV_MOZ_2 = 60; //for 2 buttons
var POP_UP_ACTION_DIV_MOZ_3 = 90; //for 3 buttons

function showModalPopUp(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == undefined) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    $('.ui-dialog-titlebar-close').show();
}

function xferBack() {
    document.forms[0].submit();
    return true;
}

function showModalPopUpWithOutHeader(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == undefined) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    $('.ui-dialog-titlebar-close').hide();
}
function closeModalPopUp(divIdToClose) {
    $('#' + divIdToClose).dialog('close');
}
function showAlert(message, divIdToShowMsgWithHash) {
    $.wl_Alert(message, 'info', divIdToShowMsgWithHash);
}
function showWait() {
    $("#divWaitBox").dialog({
        modal: true,
        width: 0,
        height: 0,
        closeOnEscape: false,
        resizable: false,
        autoOpen: true,
        open: function () {
            //$(".ui-dialog-titlebar-close", $(this).parent()).hide(); //hides the little 'x' button
            $("#divWaitBox").siblings(".ui-dialog-titlebar").hide();
        }
    });
}
function hideWait() {
    $("#divWaitBox").dialog("close");
    $(".ui-dialog-titlebar").show();
}

function hideHTMLElements(commaSeparatedIdsOfElements) {
    var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
    for (var i = 0; i < aryOfElements.length; i++) {
        $('#' + aryOfElements[0]).hide();
    }
}

Repeater =
{
    ADD_USER_USER_DETAIL: 0
}

function showHTMLElements(commaSeparatedIdsOfElements) {
    var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
    for (var i = 0; i < aryOfElements.length; i++) {
        $('#' + aryOfElements[0]).show();
    }
}

function stopDefAction(evt) {
    evt.preventDefault();
}

function spinDiv(_bol) {
    if (_bol) {
        showModalPopUp('spinDiv', '', 0);
    }
    else {
        $('#spinDiv').dialog('close');
    }
}

DialogType = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showDialogImage(type, divToPrependImg) {

    var divToPrependImage = getDivToPrependImage(divToPrependImg);
    if (divToPrependImage) {
        if ($.browser.msie) {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError");
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning");
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo");
                    break;
            }
        }
        else if ($.browser.webkit) {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError dialogErrorWebKit");
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning dialogWarningWebKit");
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo dialogInfoWebKit");
                    break;
            }
        }
        else {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    break;
            }
        }


    }
}
function getDivToPrependImage(divToPrependImage) {
    if (divToPrependImage == undefined) {
        return $('#wl_dialog_alert').prev();
    }
    else {
        return $('#' + divToPrependImage).prev();
    }
}
function removeDialogImage(divToRemoveImage) {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    if (divToRemoveImage == undefined) {
        $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove();
    }
    else {
        $('#' + divToRemoveImage).prev().find($('#dialogImageId')).remove();
    }

}
function getDialogImageSpan(divToPrependImage) {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    if (divToPrependImage == undefined) {
        return $('#wl_dialog_alert').prev().find('#dialogImageId');
    }
    else {
        return $('#' + divToPrependImage).prev().find('#dialogImageId');
    }

}
function setModalPosition(divToSet) {

    PgDim = getPageSize();
    var vleft = ((PgDim[2] - divToSet.width()) / 2);
    var vtop = ((PgDim[3] - divToSet.height()) / 2);
    //    PgDim = getPageSize();
    //    var vleft = ($(window).width() / 2);
    //    var vtop = ($(window).height() / 2);
    //divToSet.offset({ top: vtop, left: vleft });
    divToSet.css('top', vtop);
    divToSet.css('left', vleft);
}

function st(obj) {
    return o(obj).style;
}


function getPageSize() {
    var xScroll, yScroll;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    }
    // all but Explorer Mac 
    else if (document.body.scrollHeight > document.body.offsetHeight) {
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    }
    // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari 
    else {
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    // all except Explorer
    if (self.innerHeight) {
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    }
    // Explorer 6 Strict Mode 
    else if (document.documentElement && document.documentElement.clientHeight) {
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }
    // other Explorers 
    else if (document.body) {
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }
    if (yScroll < windowHeight) pageHeight = windowHeight;
    else pageHeight = yScroll;
    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    }
    else {
        pageWidth = xScroll;
    }
    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;


}



function o(obj) {
    if (typeof obj == 'object') {
        return obj;
    }
    else {
        return document.getElementById(obj);
    }
}

function stopDefualtOfDateTxtBox(txtBox) {
    if (txtBox) {
        txtBox.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
    }
}


function rptRowClick(row, repeater) {
    processRowClick(row);
}
function processRowClick(row) {
    var hidUsedAfterPostBack = $('[id$=hidRowValuesUsedAfterPostBack]');
    var strValuesReqAfterPostBack = '';
    $.each(row.children, function () {
        if (this.title == "id") {
            var td = this;
            $.each(td.children, function () {
                if (strValuesReqAfterPostBack.length == 0)
                    strValuesReqAfterPostBack += ($.trim(this.innerHTML));
                else
                    strValuesReqAfterPostBack += ($.trim(';' + this.innerHTML));
            })
            hidUsedAfterPostBack.val(strValuesReqAfterPostBack);
        }
    });
    UpdateRowClick();
}
function UpdateRowClick() {
    $('[id$=btnRowClickPostBack]').click();
}
function rptHeaderClickForSorting(header, hidFieldUsedForInfo) {
    UpdateRowClick();
}

function setWidthOfDropDown() {
    $("select").each(function (i) {
        var parentDiv = this.parent("div");
        if (parentDiv && parentDiv != undefined) {
            this.width(parentDiv.outerWidth(false));
        }
    })
}
MessageOption = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showMessage(message, optionForScript, dialogImageType) {
    var strOption = '';
    switch (optionForScript) {
        case DialogType.Error:
            strOption = '$.alert.Error';
            break;
        case DialogType.Info:
            strOption = '$.alert.Information';
            break;
    }
    $.alert(message, strOption); showDialogImage(dialogImageType);
}

function showConfirmation(message) {

    var blnConfirm = confirm(message);
    if (blnConfirm) {
        isCookieCleanUpRequired('false');
    }
    return blnConfirm;
}

COOKIE_MSID = "MFMSID";
COOKIE_MUEID = "MFMUEID";

function createCookie(cookieName, cookieValue, cookieExpiryMins) {
    if (cookieExpiryMins) {
        var date = new Date();
        date.setTime(date.getTime() + (cookieExpiryMins * 60 * 1000));
        var expires = "; expires=" + date.toUTCString();
    }
    else var expires = "";
    document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
//right now it is used only changing the divs of modal pop up
//action divs.So do the firefox related changes here only.
//But change the name of function after testing.
function changeWidthOfDiv(divId, iWidth, inPercent) {
    var divToChange = $('#' + divId);
    if ($.browser.mozilla) {
        if (iWidth === POP_UP_ACTION_DIV_2) iWidth = POP_UP_ACTION_DIV_MOZ_2;
        else if (iWidth === POP_UP_ACTION_DIV_3) iWidth = POP_UP_ACTION_DIV_MOZ_3;
    }
    if (inPercent) {
        $(divToChange).width(iWidth.toString() + "%");
    }
    else {
        $(divToChange).width(iWidth);
    }
}
function changeWidthOfActionDiv(divId, iWidth, inPercent) {
    var divToChange = $('#' + divId);
    if (inPercent) {
        $(divToChange).width(iWidth.toString() + "%");
    }
    else {
        $(divToChange).width(iWidth);
    }
}

function cancelEventBubbling(e) {
    if (!e)
        e = window.event;

    //IE9 & Other Browsers
    if (e.stopPropagation) {
        e.stopPropagation();
    }
    //IE8 and Lower
    else {
        e.cancelBubble = true;
    }
}
function childHandler(e) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    alert('child clicked');

}; 