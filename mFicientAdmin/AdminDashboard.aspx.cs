﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class dashboard : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;



            if (!IsPostBack)
            {
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
                Page.SetFocus(ltlUserType);
                BindEnquiryRepeater();
                BindOrderRepeater();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }

        }

        public void BindEnquiryRepeater()
        {
            CheckLoginAdmin ad = new CheckLoginAdmin();
            DataTable dt = ad.GetEnquiryList();
            if (dt.Rows.Count > 0)
            {
                lnkdetails.Visible = true;
                lblInfo.Text = "<h1>Enquiry Details</h1>";
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lnkdetails.Visible = false;
                lblInfo.Text = "<h1>no details of enquiries added yet</h1> ";
            }
        }

        public void BindOrderRepeater()
        {
            AddServerDetails detail = new AddServerDetails();
            DataTable objdt = detail.GetAdminList();
            if (objdt.Rows.Count > 0)
            {
                lnkorderdet.Visible = true;
                lblHeaderInfo.Text = "<h1>Order Details</h1>";
                rptUser.DataSource = objdt;
                rptUser.DataBind();
            }
            else if (objdt.Rows.Count == 0)
            {
                lnkorderdet.Visible = false;
                lblHeaderInfo.Text = "<h1>no plan orders added yet</h1> ";
            }
        }

        protected void rptU_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lid");
            if (e.CommandName == "Info")
            {
                FillOrderDetails(lit_id.Text);
            }
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ProcessRequest":
                    processPayment();
                    break;
            }
        }

        protected void btnPaymentSave_Click(object sender, EventArgs e)
        {
        }

        protected void btnPaymentCancel_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divPaymentModalContainer", this.Page);
            rptUserDetails.Visible = true;
        }


        void processPayment()
        {
            Utilities.showModalPopup("divPaymentModalContainer", this.Page, "Amount details", "", false, false);
            //updPaymentModalContainer.Update();
        }
        protected void lnkok_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("DivUserDetails", this.Page, "Details", "350", true);
            enabledisablepanels("Info");
        }

        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlBox.Visible = false;
            }
            else
            {
                pnlBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        protected void FillOrderDetails(string CompanyId)
        {
            AddServerDetails add = new AddServerDetails();
            char[] Type = add.GetDetailsArray();
            foreach (char ans in Type)
            {
                if (ans == '1')
                {
                    loname.Text = "Renew Plan";
                }
                else if (ans == '2')
                {
                    loname.Text = "Add User";
                }
                else if (ans == '3')
                {
                    loname.Text = "Plan Upgrade";
                }
                else
                {
                    loname.Text = "Plan Downgrade";
                }
            }

            string Code = add.GetPlan();
            GetServerDetailsAdmin det = new GetServerDetailsAdmin();
            det.OrderDetails(CompanyId);
            DataTable dt = det.ResultTable;
            if (dt.Rows.Count > 0)
            {
                Hiddetails.Value = CompanyId;
                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
                lplan.Text = Convert.ToString(dt.Rows[0]["PLAN_NAME"]);
                lusers.Text = Convert.ToString(dt.Rows[0]["MAX_USER"]);
                lwrflow.Text = Convert.ToString(dt.Rows[0]["MAX_WORKFLOW"]);
                ldate.Text = Convert.ToString(dt.Rows[0]["EXPIRY DATE"]);
                lcode.Text = Code;
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        void Info(RepeaterCommandEventArgs e)
        {
            Label company_id = (Label)e.Item.FindControl("lid");
            FillOrderDetails(company_id.Text);
        }

    }
}
