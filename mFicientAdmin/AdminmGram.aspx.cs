﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class AdminmGram : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        string strQuery;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;



            if (!IsPostBack)
            {
                BindAllCompany();
                BindAllDeviceType();
                setTheInitialSheduleTime(null);
            }
            else
            {
               ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }

        }

        public void  BindAllCompany()
        {
          DataSet ds1=  getAllEnterpriseDeatils();
          drpenterprisedetails.Items.Clear();
          if (ds1.Tables[0].Rows.Count > 0)
          {
              drpenterprisedetails.DataSource = ds1;
              drpenterprisedetails.DataTextField = "COMPANY_NAME";
              drpenterprisedetails.DataValueField = "COMPANY_ID";
              drpenterprisedetails.DataBind();
          
              drpenterprisedetails.Items.Insert(0, new ListItem("All", "-1"));
          }
          else
          {
            
              drpenterprisedetails.Items.Insert(0, new ListItem("All", "-1"));
          }
          
        }

        void setTheInitialSheduleTime(UpdatePanel updPanel)
        {
            if (updPanel != null)
            {
                Utilities.runPostBackScript("makeDatePickerWithInitialDate();chkSheduledHrsMinClicked(this);", updPanel, "Initialise Date");
            }
            else
            {
                Utilities.runPageStartUpScript(this.Page, "Initialise Date", "makeDatePickerWithInitialDate();chkSheduledHrsMinClicked(this);");
            }
            BindMessageSecduceDropDown();
      
            ddlHours.SelectedIndex = ddlHours.Items.IndexOf(ddlHours.Items.FindByValue("12"));
            ddlMinutes.SelectedIndex = ddlMinutes.Items.IndexOf(ddlMinutes.Items.FindByValue("0"));
         
        }

        public void BindAllDeviceType()
        {
         DataSet ds=GetTypeofDevice();
         drpdevices.Items.Clear();
         if (ds.Tables[0].Rows.Count > 0)
         {
             drpdevices.DataSource = ds;
             drpdevices.DataTextField = "PHONE_DEVICE_TYPE";
             drpdevices.DataValueField = "PHONE_DEVICE_TYPE_CODE";
             drpdevices.DataBind();
         
             drpdevices.Items.Insert(0, new ListItem("All", "-1"));
         }
         else
         {
             drpdevices.Items.Insert(0, new ListItem("All", "-1"));
           
         }
        }


       


        public void BindAllUser(string companyID)
        {
       
            mGarmAdminDetails objgetUsers = new mGarmAdminDetails(companyID,"");
            if (objgetUsers.StatusCode == 0)
            {
                drpUserdetails.Items.Clear();
                if (objgetUsers.ResultDataSet.Tables[0].Rows.Count > 0)
                {
                    drpUserdetails.DataSource = objgetUsers.ResultDataSet.Tables[0];
                    drpUserdetails.DataTextField = "CompleteName";
                    drpUserdetails.DataValueField = "USER_NAME";
                    drpUserdetails.DataBind();
                    drpUserdetails.Items.Insert(0, new ListItem("All", "-1"));
                    
                }
                else
                {
                    drpUserdetails.Items.Insert(0, new ListItem("All", "-1"));
                }
                

            }
            else
            {
                drpUserdetails.Items.Insert(0, new ListItem("Select", "-1"));
            }

        }
        DataSet getAllEnterpriseDeatils()
        {
            strQuery = "select COMPANY_ID,COMPANY_NAME from dbo.ADMIN_TBL_COMPANY_DETAIL";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds;

        }

        DataSet GetAllDeviceDetails()
        {
            strQuery = "select  from dbo.ADMIN_TBL_COMPANY_DETAIL";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds;
        }

        DataSet GetTypeofDevice()
        {
            strQuery = "select * from ADMIN_TBL_MST_MOBILE_DEVICE";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds;
        }

        void bindHoursDropDown()
        {
            ddlHours.Items.Clear();
            for (int i = 0; i <= 23; i++)
            {
                string text = i.ToString().Length == 1 ? "0" + i.ToString() : i.ToString();
                ddlHours.Items.Add(new ListItem(text, i.ToString()));
            }
        }
        void bindMinDropDown()
        {
            ddlMinutes.Items.Clear();
            ddlMinutes.Items.Add(new ListItem("00", "0"));
            ddlMinutes.Items.Add(new ListItem("15", "15"));
            ddlMinutes.Items.Add(new ListItem("30", "30"));
            ddlMinutes.Items.Add(new ListItem("45", "45"));
        }
        void bindExpiryDropDown()
        {
            ddlExpiryDays.Items.Clear();
            for (int i = 1; i <= 30; i++)
            {
                ddlExpiryDays.Items.Add(
                    new ListItem(
                        i.ToString().Length == 1 ? "0" + i.ToString() : i.ToString(),
                        i.ToString()
                        )
                        );
            }
        }

        void BindMessageSecduceDropDown()
        {
            bindHoursDropDown();
            bindMinDropDown();
            bindExpiryDropDown();
        }


        DataSet GetAllEnterpriseUser(string deviceType)
        {
            strQuery = "select USER_ID,COMPANY_ID,USER_NAME  from dbo.TBL_USER_DETAIL ;";
            if (deviceType != "") strQuery += "select * from tbl_registered_device where device_type='" + deviceType + "';";
            else strQuery += "select * from tbl_registered_device ;";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
           
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds;
        }

        DataSet GetUserDevice(string deviceType,string  companyId,string userId)
        {
            if (deviceType != "") strQuery += "select * from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id and r.company_id=u.Company_id  where device_type=@device_type and user_name=@user_name  and r.COMPANY_ID=@COMPANY_ID;";
            else strQuery += "select * from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id  and r.company_id=u.Company_id   where user_name=@user_name and r.COMPANY_ID=@COMPANY_ID;";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
            sqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
            sqlCommand.Parameters.AddWithValue("@user_name", userId);
            sqlCommand.Parameters.AddWithValue("@device_type", deviceType);
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds;
        }


        DataSet getDevicedetailscompanyId()
        {
            strQuery = "select COMPANY_ID from dbo.ADMIN_TBL_COMPANY_DETAIL";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds;
        }

        protected void drpenterprisedetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpenterprisedetails.SelectedValue == "-1")
            {
                drpUserdetails.Enabled = false;
            
            }
            else if (drpenterprisedetails.SelectedValue != "0" )
            {
                drpUserdetails.Enabled = true;
                
                BindAllUser(drpenterprisedetails.SelectedValue.ToString());
            }
            Utilities.runPostBackScript("makeDatePickerWithInitialDate();", this, "Initialise Date");
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                List<mFicientCommonProcess.mGarmUserdetails> userdetails = new List<mFicientCommonProcess.mGarmUserdetails>();
                DateTime dtSelectedTimeInUtc = DateTime.UtcNow;
                if (!chkSheduleImmediate.Checked && txtSendDate.Text != "")
                {
                    dtSelectedTimeInUtc =
                       getTimeInUtc(
                       txtSendDate.Text,
                       Convert.ToInt32(ddlHours.SelectedValue),
                       Convert.ToInt32(ddlMinutes.SelectedValue),
                       0,
                       '-');
                    if (!isTimeSelectedWithinTheStipulatedTime(dtSelectedTimeInUtc))
                    {
                        Utilities.showAlert("Time selected not within 48 hrs", "divContainer", this);
                        return;
                    }
                }
                string devicetype = drpdevices.SelectedIndex == 0 ? "" : drpdevices.SelectedValue;
 
                if (drpenterprisedetails.SelectedValue == "-1")
                {
                    DataSet ds = GetAllEnterpriseUser(devicetype);
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        userdetails.Add(new mFicientCommonProcess.mGarmUserdetails ( Convert.ToString(dr["USER_NAME"]), Convert.ToString(dr["COMPANY_ID"]),  devicetype,  getDeviceList(ds.Tables[1], Convert.ToString(dr["USER_id"]), "")));
                    }
                }
                else
                {
                    if (drpUserdetails.SelectedValue == "-1")
                    {
                        mGarmAdminDetails objgetUsers = new mGarmAdminDetails(drpenterprisedetails.SelectedItem.Value, devicetype);
                        if (objgetUsers.StatusCode == 0)
                        {
                            foreach (DataRow dr in objgetUsers.ResultDataSet.Tables[0].Rows)
                            {
                                userdetails.Add(new mFicientCommonProcess.mGarmUserdetails (  Convert.ToString(dr["USER_NAME"]),  drpenterprisedetails.SelectedValue,  devicetype,  getDeviceList(objgetUsers.ResultDataSet.Tables[1], Convert.ToString(dr["USER_id"]), "")));
                            }
                        }
                    }
                    else
                    {
                        DataSet ds = GetUserDevice(devicetype, drpenterprisedetails.SelectedValue, drpUserdetails.SelectedItem.Value);
                        userdetails.Add(new mFicientCommonProcess.mGarmUserdetails ( drpUserdetails.SelectedItem.Value,  drpenterprisedetails.SelectedValue,  devicetype,  getDeviceList(ds.Tables[0], "", drpUserdetails.SelectedItem.Value)));
                    }
                }
                

                mGarmAdminDetails objmgramdetail = new mGarmAdminDetails(lblmficient.Text, txtMessage.InnerText, Convert.ToInt32(ddlExpiryDays.SelectedValue),chkSheduleImmediate.Checked ? DateTime.UtcNow.Ticks : dtSelectedTimeInUtc.Ticks,
                            chkSheduleImmediate.Checked ? DateTime.UtcNow.Ticks : dtSelectedTimeInUtc.Ticks, userdetails);
                objmgramdetail.SaveAdminMGramProcess();
                    if (objmgramdetail.StatusCode == 0)
                    {
                     Utilities.showMessage("Information Saved successfully.", this, DIALOG_TYPE.Info);
                     txtMessage.InnerText = "";
                    }
                    else
                     {
                     Utilities.showMessage("Internal Server Error",this, DIALOG_TYPE.Error);
                     }
            }
            catch
            {
                Utilities.showMessage("Internal server error", updPushMessageForm, DIALOG_TYPE.Error);
            }

            Utilities.runPostBackScript("makeDatePickerWithInitialDate();", this, "Initialise Date");

        }

        List<List<string>> getDeviceList(DataTable dt, string userId ,string user_name)
        {//Convert.ToString(dr["USER_id"]) 
            List<List<string>> deviceIds = new List<List<string>>();
            DataRow[] deviceRows;
            if(!string.IsNullOrEmpty(userId))
                deviceRows = dt.Select("USER_ID = '" + userId+ "'");
            else 
                deviceRows = dt.Select("USER_Name = '" + user_name+ "'");
            if(deviceRows!=null && deviceRows.Length>0)
                foreach (DataRow deviceRow in deviceRows)
                {
                    List<string> userDevice = new List<string>();
                    userDevice.Add(Convert.ToString(deviceRow["DEVICE_PUSH_MESSAGE_ID"]));
                    userDevice.Add(Convert.ToString(deviceRow["DEVICE_TYPE"]));
                    userDevice.Add(Convert.ToString(deviceRow["DEVICE_ID"]));
                    deviceIds.Add(userDevice);
                }
            return deviceIds;
        }
        bool isTimeSelectedWithinTheStipulatedTime(DateTime selectedDateInUtc)
        {
            DateTime rightNow = DateTime.UtcNow;
            DateTime dateSelectedForSendingMsg = selectedDateInUtc;
            DateTime dtUtcDateAfter48Hours =
                rightNow.AddHours(48);

            long lngTicksRightNow = rightNow.Ticks;
            long lngDateSelectedInUtc = dateSelectedForSendingMsg.Ticks;
            long lngDateAfter48HrsUtc = dtUtcDateAfter48Hours.Ticks;

            /*
             *Allow Selection of a date which is after the present date.
             *And should be within 48 hours after the present date.
            */
            if ((lngTicksRightNow < lngDateSelectedInUtc) &&
                (lngDateSelectedInUtc <= lngDateAfter48HrsUtc))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
       
        public static DateTime getTimeInUtc(string dateString,
          int hrs,
          int min,
          int sec,
          char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            return new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0]),
                hrs,
                min,
                sec
                ).ToUniversalTime();
        }

    }
}