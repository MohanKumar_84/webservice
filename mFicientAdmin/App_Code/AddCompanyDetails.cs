﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class CompanyDetailsAdmin
    {
        public DataTable GetCompanyDetailsPending()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select  distinct d.COMPANY_NAME,d.COMPANY_ID,a.IS_TRIAL,PlanDetails.PLAN_NAME,PlanLog.TRANSACTION_TYPE, case when a.IS_TRIAL='1' then p.PLAN_CODE + '(Trial)' when
                                                a.IS_TRIAL='0' then p.PLAN_CODE  end as
                                                'Plan Code' from ADMIN_TBL_COMPANY_CURRENT_PLAN p
                                                INNER JOIN 
                                                (SELECT AdmPlans.PLAN_CODE,AdmPlans.PLAN_NAME,'P' AS PLAN_TYPE FROM ADMIN_TBL_PLANS AS AdmPlans
                                                UNION
                                                SELECT TrialPlans.PLAN_CODE,TrialPlans.PLAN_NAME,'T' AS PLAN_TYPE FROM ADMIN_TBL_TRIAL_PLANS AS TrialPlans
                                                ) AS PlanDetails
                                                ON PlanDetails.PLAN_CODE = p.PLAN_CODE
                                                inner join 
                                                ADMIN_TBL_COMPANY_DETAIL d on d.COMPANY_ID=p.COMPANY_ID inner join ADMIN_TBL_COMPANY_ADMINISTRATOR a
                                                on d.ADMIN_ID=a.ADMIN_ID
                                                LEFT OUTER JOIN ADMIN_TBL_PLAN_PURCHASE_LOG AS PlanLog
                                                ON p.PLAN_CODE = PlanLog.PLAN_CODE
                                                where d.SM_APPROVED=@smapproved");
                objCmd.Parameters.AddWithValue("@smapproved", 0);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetCompanyDetailsApproved()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select distinct d.COMPANY_NAME,d.COMPANY_ID,a.IS_TRIAL,PlanDetails.PLAN_NAME,PlanLog.TRANSACTION_TYPE, case when a.IS_TRIAL='1' then p.PLAN_CODE + '(Trial)' when
                                                a.IS_TRIAL='0' then p.PLAN_CODE  end as
                                                'Plan Code' from ADMIN_TBL_COMPANY_CURRENT_PLAN p inner join 
                                                ADMIN_TBL_COMPANY_DETAIL d on d.COMPANY_ID=p.COMPANY_ID
                                                INNER JOIN 
                                                (SELECT AdmPlans.PLAN_CODE,AdmPlans.PLAN_NAME,'P' AS PLAN_TYPE FROM ADMIN_TBL_PLANS AS AdmPlans
                                                UNION
                                                SELECT TrialPlans.PLAN_CODE,TrialPlans.PLAN_NAME,'T' AS PLAN_TYPE FROM ADMIN_TBL_TRIAL_PLANS AS TrialPlans
                                                ) AS PlanDetails
                                                ON PlanDetails.PLAN_CODE = p.PLAN_CODE
                                                 inner join ADMIN_TBL_COMPANY_ADMINISTRATOR a
                                                on d.ADMIN_ID=a.ADMIN_ID 
                                                LEFT OUTER JOIN ADMIN_TBL_PLAN_PURCHASE_LOG AS PlanLog
                                                ON p.PLAN_CODE = PlanLog.PLAN_CODE
                                                where d.SM_APPROVED=@smapproved");
                objCmd.Parameters.AddWithValue("@smapproved", 1);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void CompanyDetails(string CompanyId)
        {
            StatusCode = -1000; // for error
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select distinct d.COMPANY_NAME,d.STREET_ADDRESS1,d.STREET_ADDRESS2,
                                            d.STREET_ADDRESS3,d.CITY_NAME,d.STATE_NAME,c.COUNTRY_NAME,d.ZIP,d.SUPPORT_EMAIL,d.SUPPORT_CONTACT
                                            from ADMIN_TBL_COMPANY_DETAIL d inner join
                                            ADMIN_TBL_MST_COUNTRY c on d.COUNTRY_CODE=c.COUNTRY_CODE where d.COMPANY_ID=@companyid ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@companyid", CompanyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    StatusDescription = "No records to display";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusCode = -1000;
                }
             }
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public DataSet Resultset
        {
            get;
            set;
        }
    }
}