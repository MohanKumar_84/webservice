﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdmin
{
    public class AddCurrentPlan
    {
        public void Process()
        {
            try
            {
                CurrentPlanDetail currentPlan = new CurrentPlanDetail(this.CompanyId);
                currentPlan.Process();
                DataTable dtCurrentPlanDtl = currentPlan.PlanDetails;
                if (dtCurrentPlanDtl != null)
                {
                    if (dtCurrentPlanDtl.TableName == "Plan")
                    {
                        string query = @"UPDATE ADMIN_TBL_COMPANY_CURRENT_PLAN
                                         SET PLAN_CODE = @PLAN_CODE, MAX_WORKFLOW = @MAX_WORKFLOW, PLAN_CHANGE_DATE = @PLAN_CHANGE_DATE, USER_CHARGE_PM = @USER_CHARGE_PM,
                                         MAX_USER = @MAX_USER, VALIDITY = @VALIDITY, PURCHASE_DATE = @PURCHASE_DATE 
                                         WHERE COMPANY_ID = @COMPANY_ID;";

                        SqlCommand objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                        objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                        objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                        objSqlCommand.Parameters.AddWithValue("@MAX_USER", Convert.ToInt32(this.MaxUsers));
                        objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDouble(this.ChargePM));
                        objSqlCommand.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(this.Validity));
                        objSqlCommand.Parameters.AddWithValue("@PURCHASE_DATE", DateTime.Now.Ticks);
                        objSqlCommand.Parameters.AddWithValue("@PLAN_CHANGE_DATE", DateTime.Now.Ticks);

                        if (Utilities.ExecuteNonQueryRecord(objSqlCommand) > 0)
                        {
                            this.StatusCode = 0;
                            this.StatusDescription = "";
                        }
                        else
                        {
                            this.StatusDescription = "Error Inserting record";
                            this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;

                        }
                    }
                    else
                    {
                        string query = @"INSERT INTO ADMIN_TBL_COMPANY_CURRENT_PLAN(COMPANY_ID,PLAN_CODE,MAX_WORKFLOW,MAX_USER,USER_CHARGE_PM,CHARGE_TYPE,VALIDITY,PURCHASE_DATE,PLAN_CHANGE_DATE,BALANCE_AMOUNT,NEXT_MONTH_PLAN,NEXT_MONTH_PLAN_LAST_UPDATED) 
                                            VALUES(@COMPANY_ID,@PLAN_CODE,@MAX_WORKFLOW,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE,@VALIDITY,@PURCHASE_DATE,@PLAN_CHANGE_DATE,@BALANCE_AMOUNT,@NEXT_MONTH_PLAN,@NEXT_MONTH_PLAN_LAST_UPDATED);";

                        SqlCommand objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                        objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                        objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                        objSqlCommand.Parameters.AddWithValue("@MAX_USER", Convert.ToInt16(this.MaxUsers));
                        objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDecimal(this.ChargePM));
                        objSqlCommand.Parameters.AddWithValue("@CHARGE_TYPE", this.UserChargeType);
                        objSqlCommand.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(this.Validity));
                        objSqlCommand.Parameters.AddWithValue("@PURCHASE_DATE", DateTime.Now.Ticks);
                        objSqlCommand.Parameters.AddWithValue("@PLAN_CHANGE_DATE", 0);
                        objSqlCommand.Parameters.AddWithValue("@BALANCE_AMOUNT", 0);
                        objSqlCommand.Parameters.AddWithValue("@NEXT_MONTH_PLAN", this.PlanCode);
                        objSqlCommand.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);
                        if (Utilities.ExecuteNonQueryRecord(objSqlCommand) > 0)
                        {
                            this.StatusCode = 0;
                            this.StatusDescription = "";
                        }
                        else
                        {
                            this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
            }
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }
        public string MaxWorkFlow
        {
            set;
            get;
        }
        public string MaxUsers
        {
            set;
            get;
        }
        public string HighestWorkflow
        {
            set;
            get;
        }
        public string UserChargeType
        {
            set;
            get;
        }
        public string ChargePM
        {
            set;
            get;
        }
        public string Validity
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}