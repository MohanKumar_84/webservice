﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdmin
{
    public class AddCurrentPlanPurchaseHistory
    {
        public AddCurrentPlanPurchaseHistory()
        {

        }
        public AddCurrentPlanPurchaseHistory(string companyId, string chargePerMnth, string currencyType, string noOfUsers, string maxWorkFlows, string validity, string priceWithoutDiscount
                                            , string discountPercentage, string actualPrice, string transactionType, string planCode, string planName, string resellerId)
        {
            CompanyId = companyId;
            this.ChargePM = chargePerMnth;
            this.UserChargeType = currencyType;
            this.MaxUsers = noOfUsers;
            this.MaxWorkFlow = maxWorkFlows;
            this.Validity = validity;
            this.PriceWithoutDiscount = priceWithoutDiscount;
            this.DiscountPercent = discountPercentage;
            this.ActualPrice = actualPrice;
            this.TransactionType = transactionType;
            this.PlanCode = planCode;
            this.PlanName = planName;
            this.ResellerID = resellerId;
        }
        public void Process()
        {
            this.StatusCode = -1000;//for error;//applied transaction on 22/9/2012
            
            SqlTransaction sqlTransaction = null;
            SqlConnection con;
            Utilities.SqlConnectionOpen(out con);
            int iError = -1000;
            try
            {
                using (con)
                {
                    sqlTransaction = con.BeginTransaction();
                   iError= savePurchaseLog(con, sqlTransaction);
                   if (iError == 0)
                   {
                       updatePlanRequest(con, sqlTransaction);
                       sqlTransaction.Commit();
                       StatusCode = 0;
                   }
                }
            }
            catch 
            {
                this.StatusDescription = "Internal server error";
                this.StatusCode = -1000;
            }
        }
        int updatePlanRequest(SqlConnection con, SqlTransaction transaction)
        {
            int iError = -1000;
            try
            {

                string query = @"UPDATE ADMIN_TBL_PLAN_ORDER_REQUEST
                                SET STATUS = 1 WHERE COMPANY_ID = @CompanyID
                                AND PLAN_CODE = @PlanCode
                                AND REQUEST_TYPE = @RequestType
                                AND STATUS = @StatusToUpdate;";

                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                objSqlCommand.Parameters.AddWithValue("@RequestType", this.TransactionType);
                objSqlCommand.Parameters.AddWithValue("@StatusToUpdate",PLAN_REQUEST_STATUS.UNAPPROVED);
               int iRowsEffected= objSqlCommand.ExecuteNonQuery();
                iError = 0;
            }
            catch (Exception e)
            {
                throw e;
            }
            return iError;
        }

        int savePurchaseLog(SqlConnection con, SqlTransaction transaction)
        {
            int iError = -1000;
            try
            {
                //ADMIN_TBL_COMPANY_PLAN_PURCHASE_LOG changed to ADMIN_TBL_PLAN_PURCHASE_LOG
                string query = @"INSERT INTO ADMIN_TBL_PLAN_PURCHASE_LOG(COMPANY_ID,PLAN_CODE,MAX_WORKFLOW,MAX_USER,USER_CHARGE_PM,CHARGE_TYPE,VALIDITY,PRICE_WITHOUT_DISCOUNT,DISCOUNT_PER,ACTUAL_PRICE,PURCHASE_DATE,TRANSACTION_ID,TRANSACTION_TYPE,PLAN_NAME,APPROVED,APPROVED_DATE,GET_PAYMENT,GET_PAYMENT_DATE,RESELLER_ID,AMOUNT_RECEIVED)
                                VALUES(@COMPANY_ID,@PLAN_CODE,@MAX_WORKFLOW,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE,@VALIDITY,@PRICE_WITHOUT_DISCOUNT,@DISCOUNT_PER,@ACTUAL_PRICE,@PURCHASE_DATE,@TRANSACTION_ID,@TRANSACTION_TYPE,@PLAN_NAME,@APPROVED,@APPROVED_DATE,@GET_PAYMENT,@GET_PAYMENT_DATE,@RESELLER_ID,@AMOUNT_RECEIVED);";

                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                objSqlCommand.Parameters.AddWithValue("@MAX_USER", Convert.ToInt16(this.MaxUsers));
                objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDecimal(this.ChargePM));
                objSqlCommand.Parameters.AddWithValue("@CHARGE_TYPE", this.UserChargeType);
                objSqlCommand.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(this.Validity));
                objSqlCommand.Parameters.AddWithValue("@PRICE_WITHOUT_DISCOUNT", Convert.ToDecimal(this.PriceWithoutDiscount));
                objSqlCommand.Parameters.AddWithValue("@DISCOUNT_PER", Convert.ToDecimal(this.DiscountPercent));
                objSqlCommand.Parameters.AddWithValue("@ACTUAL_PRICE", Convert.ToDecimal(this.ActualPrice));
                objSqlCommand.Parameters.AddWithValue("@PURCHASE_DATE", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@TRANSACTION_ID", this.TransactionType);
                objSqlCommand.Parameters.AddWithValue("@TRANSACTION_TYPE", this.TransactionType.Trim());
                objSqlCommand.Parameters.AddWithValue("@PLAN_NAME", PlanName);
                objSqlCommand.Parameters.AddWithValue("@APPROVED", false);
                objSqlCommand.Parameters.AddWithValue("@APPROVED_DATE", 0);
                objSqlCommand.Parameters.AddWithValue("@GET_PAYMENT", false);
                objSqlCommand.Parameters.AddWithValue("@GET_PAYMENT_DATE", 0);
                objSqlCommand.Parameters.AddWithValue("@RESELLER_ID", ResellerID);
                objSqlCommand.Parameters.AddWithValue("@AMOUNT_RECEIVED", "0");//24/9/2012
                objSqlCommand.ExecuteNonQuery();
                iError = 0;
            }
            catch (Exception e)
            {
                throw e;
            }
            return iError;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }
        public string MaxWorkFlow
        {
            set;
            get;
        }
        public string MaxUsers
        {
            set;
            get;
        }

        public string TotalUsers
        {
            set;
            get;
        }
        public string UserChargeType
        {
            set;
            get;
        }
        public string ChargePM
        {
            set;
            get;
        }
        public string Validity
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string PriceWithoutDiscount
        {
            set;
            get;
        }
        public string DiscountPercent
        {
            set;
            get;
        }
        public string ActualPrice
        {
            set;
            get;
        }
        public string TransactionType
        {
            set;
            get;
        }
        public string PlanName
        {
            set;
            get;
        }
        public string ResellerID
        {
            set;
            get;
        }
    }
}