﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class AddServerDetails
    {
        public DataTable GetServerList()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select SERVER_ID, SERVER_NAME,SERVER_URL,SERVER_IP_ADDRESS,IS_ENABLED from ADMIN_TBL_MST_SERVER order by SERVER_NAME");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return null;
        }

        public DataTable EnquiryListSMStatus(string LoginName,string Id)
        {
            try
            {
             SqlCommand sqlCommand = new SqlCommand(@"select distinct e.ENQUIRY_ID, e.COMPANY_NAME,e.TYPE_OF_COMPANY,
            case when e.STATUS= '0'  OR E.SALES_MNGR_ID=''then 'New' when STATUS= '1' OR e.SALES_MNGR_ID!='' and e.SALES_ID=''
            then 'Processing (Sales Manager)' when STATUS= '2' OR e.SALES_MNGR_ID!='' and e.SALES_ID!=''
            and e.RESELLER_ID='' then 'Processing(Sales Person)' when STATUS= '3' then 'Processing (Reseller)'
            when STATUS= '4' then 'Successfully processed' when STATUS ='5' then 'Processing failed' end as STATUS
            from ADMIN_TBL_ENQUIRY_DETAIL e inner join ADMIN_TBL_LOGIN_USER_DETAIL u on e.COUNTRY_CODE=u.COUNTRY_CODE where e.COUNTRY_CODE
            in(select u.COUNTRY_CODE from ADMIN_TBL_LOGIN_USER_DETAIL u where u.LOGIN_NAME=@loginname and e.SALES_MNGR_ID=@salesMgrId)");
                sqlCommand.Parameters.AddWithValue("@loginname", LoginName);
                sqlCommand.Parameters.AddWithValue("@salesMgrId", Id);
                 DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
                
        }

        public string GetMgrId(string Loginname,string type)
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL WHERE LOGIN_NAME=@loginname AND TYPE=@type");
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@loginname", Loginname);
                objCmd.Parameters.AddWithValue("@type", type);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    string strcode = Convert.ToString(objDataSet.Tables[0].Rows[0]["LOGIN_ID"]);
                    return strcode;
                }
                else
                {
                    return "";
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable EnquiryListS(string LoginName,string Type)
        {
            
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select distinct e.ENQUIRY_ID, e.COMPANY_NAME,e.TYPE_OF_COMPANY, case when e.STATUS= '0' 
            OR E.SALES_MNGR_ID='' then 'New' when STATUS= '1' OR e.SALES_MNGR_ID!='' and e.SALES_ID='' then 'Processing (Sales Manager)'
            when STATUS= '2' OR e.SALES_MNGR_ID!='' and e.SALES_ID!='' and e.RESELLER_ID='' then 'Processing(Sales Person)'
            when STATUS= '3' then 'Processing (Reseller)' when STATUS= '4' then 'Successfully processed'  when STATUS ='5' then 'Processing failed'
            end as STATUS from ADMIN_TBL_ENQUIRY_DETAIL e inner join ADMIN_TBL_LOGIN_USER_DETAIL u
            on e.COUNTRY_CODE=u.COUNTRY_CODE where e.SALES_ID in(select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL u
            where LOGIN_NAME=@loginname and TYPE=@type)");
                sqlCommand.Parameters.AddWithValue("@loginname", LoginName);
                sqlCommand.Parameters.AddWithValue("@type", Type);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable EnquiryListR(string LoginName)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select distinct e.ENQUIRY_ID, e.COMPANY_NAME,e.TYPE_OF_COMPANY ,
                                                case when e.STATUS= '0' 
                                            OR E.SALES_MNGR_ID=''
						                    then 'New'							 
                                            when STATUS= '1' OR e.SALES_MNGR_ID!='' and e.SALES_ID=''
                                             then 'Processing (Sales Manager)'
                                            when STATUS= '2' OR e.SALES_MNGR_ID!='' and e.SALES_ID!=''
                                            and e.RESELLER_ID='' then
                                             'Processing(Sales Person)'
                                            when STATUS= '3' then 'Processing (Reseller)'
                                            when STATUS= '4' then 'Successfully processed'
                                            when STATUS ='5' then 'Processing failed' end as STATUS
                                                    from ADMIN_TBL_ENQUIRY_DETAIL e
                                                    inner join ADMIN_TBL_LOGIN_USER_DETAIL u
                                                    on e.COUNTRY_CODE=u.COUNTRY_CODE where e.RESELLER_ID in(select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL u
                                                    where LOGIN_NAME=@loginname)");
                sqlCommand.Parameters.AddWithValue("@loginname", LoginName);
                 DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetEnquiryDetails()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select ENQUIRY_ID, COMPANY_NAME,TYPE_OF_COMPANY,case when STATUS= '0' 
            OR E.SALES_MNGR_ID='' then 'New' when STATUS= '1' OR e.SALES_MNGR_ID!='' and e.SALES_ID=''
            then 'Processing (Sales Manager)' when STATUS= '2' OR e.SALES_MNGR_ID!='' and e.SALES_ID!=''
            and e.RESELLER_ID='' then 'Processing(Sales Person)' when STATUS= '3' then 'Processing (Reseller)'
            when STATUS= '4' then 'Successfully processed' when STATUS ='5' then 'Processing failed'
            end as STATUS  from ADMIN_TBL_ENQUIRY_DETAIL e");
                 DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Can be used for any user.Pass the type
        /// </summary>
        /// <param name="userType"></param>
        /// <returns></returns>
        public DataTable GetOrderList(USER_TYPE userType)
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select distinct d.COMPANY_NAME
                                                ,CASE WHEN plans.PLAN_NAME IS NULL THEN TrialPlans.PLAN_NAME ELSE plans.PLAN_NAME  END AS CURRENT_PLAN_NAME,
                                                CASE WHEN plans.PLAN_ID IS NULL THEN TrialPlans.PLAN_ID ELSE plans.PLAN_ID  END AS CURRENT_PLAN_ID
                                        ,d.COMPANY_ID,p.PLAN_NAME,r.REQUEST_TYPE ,r.PLAN_CODE, case when r.REQUEST_TYPE='1' then 'RenewPlan ('+ r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='2' then 'Add User (' + r.PLAN_CODE + ')'
                                        WHEN R.REQUEST_TYPE='3' then 'Plan Upgrade (' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='4' then 'Plan Downgrade (' + r.PLAN_CODE + ')'
                                        when r.REQUEST_TYPE='5' then 'Plan Purchase (' + r.PLAN_CODE + ')'
                                        end as 'ORDER' from  ADMIN_TBL_COMPANY_DETAIL d inner join
                                                            ADMIN_TBL_PLAN_ORDER_REQUEST r on
                                                r.COMPANY_ID=d.COMPANY_ID 
                                                inner join ADMIN_TBL_PLANS p on p.PLAN_CODE=r.PLAN_CODE
                                        INNER JOIN ADMIN_TBL_COMPANY_CURRENT_PLAN AS CmpCurrentPlan
                                        ON CmpCurrentPlan.COMPANY_ID = d.COMPANY_ID
                                        LEFT OUTER JOIN ADMIN_TBL_PLANS plans
                                        ON CmpCurrentPlan.PLAN_CODE = plans.PLAN_CODE
                                        LEFT OUTER JOIN ADMIN_TBL_TRIAL_PLANS AS TrialPlans
                                        ON CmpCurrentPlan.PLAN_CODE = TrialPlans.PLAN_CODE
                                        WHERE STATUS =@Status");//where status =1,d.COMPANY_ID added on 22-9-2012//last  inner join 2 outer join to get current plan of company added on 1/10/2012

                int iStatus = 0;
                switch (userType)
                {
                    case USER_TYPE.Reseller:
                        iStatus = (int)PLAN_REQUEST_STATUS.UNAPPROVED;
                        break;
                    case USER_TYPE.SalesManager:
                        iStatus = (int)PLAN_REQUEST_STATUS.RESELLER_APPROVED;
                        break;
                    case USER_TYPE.Account:
                        iStatus = (int)PLAN_REQUEST_STATUS.SALES_APPROVED;
                        break;
                }
                objCmd.Parameters.AddWithValue("@Status", iStatus);
                 DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
            
        }
        /// <summary>
        /// Used in Reseller only 
        /// </summary>
        /// <param name="requestType"></param>
        /// <param name="planCode"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public DataTable GetPlanOrderRequestDetail(string requestType, string planCode, string companyId)
        {
            try
            {
                //query changed on 20/9/2012 Added company id//status =@Status added on 31/10/2012
                string strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REQUEST 
                                WHERE REQUEST_TYPE = @REQUEST_TYPE 
                                AND PLAN_CODE = @PLAN_CODE AND COMPANY_ID =@CompanyID
                                AND STATUS =@Status";
                SqlCommand objCmd = new SqlCommand(strQuery);
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@REQUEST_TYPE", requestType);
                objCmd.Parameters.AddWithValue("@PLAN_CODE", planCode);
                objCmd.Parameters.AddWithValue("@CompanyId", companyId);
                objCmd.Parameters.AddWithValue("@Status", PLAN_REQUEST_STATUS.UNAPPROVED);
                DataTable dt = Utilities.SelectDataFromSQlCommand(objCmd).Tables[0];
                return dt;
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
           
        }

        public DataTable GetAdminList()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select r.*, d.COMPANY_NAME,d.COMPANY_ID,p.PLAN_NAME,r.REQUEST_TYPE ,r.PLAN_CODE,case when r.REQUEST_TYPE='1' then 'RenewPlan('+ r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='2' then 'Add User(' + r.PLAN_CODE + ')'
                                        WHEN R.REQUEST_TYPE='3' then 'Plan Upgrade(' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='4' then 'Plan Downgrade(' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='5' then 'Plan Purchase (' + r.PLAN_CODE + ')'
                                        end as 'ORDER' from  ADMIN_TBL_COMPANY_DETAIL d inner join
                                                            ADMIN_TBL_PLAN_ORDER_REQUEST r on
                                                r.COMPANY_ID=d.COMPANY_ID 
                                                inner join ADMIN_TBL_PLANS p on p.PLAN_CODE=r.PLAN_CODE
                                                WHERE STATUS =1 order by COMPANY_NAME");//where status = 1,request type ='5',d.COMPANY_ID ,r.REQUEST_TYPE ,r.PLAN_CODE,  added on 24/9/2012  Mohan
                 DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                 return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                return null;
            }
           
        }

        public string GetPlan()
        {
            string strCode = string.Empty;
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select PLAN_CODE from ADMIN_TBL_PLAN_ORDER_REQUEST");
                objCmd.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    strCode = Convert.ToString(objDataSet.Tables[0].Rows[0]["PLAN_CODE"]);
                    return strCode;
                }
                else
                {
                    return strCode = "";
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strCode;
        }


        public char[] GetDetailsArray()
        {
            char[] strtype;
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select REQUEST_TYPE from ADMIN_TBL_PLAN_ORDER_REQUEST");
                objCmd.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                strtype = Convert.ToString(objDataSet.Tables[0].Rows[0]["REQUEST_TYPE"]).ToCharArray();
                return strtype;
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }

        public string Serverid
        {
            set;
            get;
        }
        public string Servername
        {
            set;
            get;
        }
        public string IP
        {
            set;
            get;
        }

        public string URL
        {
            get;
            set;

        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
