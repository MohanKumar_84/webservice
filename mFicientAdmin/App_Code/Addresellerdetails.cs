﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class AddresellerdetailsAdmin
    {
        public AddresellerdetailsAdmin()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resellername"></param>
        /// <param name="email"></param>
        /// <param name="contactnumber"></param>
        /// <param name="discount"></param>
        /// <param name="createdon"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="city"></param>
        /// <param name="country"></param>
        /// <param name="isPaymentApproval"></param>
        /// <param name="isEnabled"></param>
        /// <param name="resellerSalesPersons"></param>
        /// <param name="isDirectSalesPerson"></param>
        public AddresellerdetailsAdmin(string resellername,
            string email, string contactnumber, string discount,
            DateTime createdon, string login,
            string password, string city, string country,
            byte isPaymentApproval, byte isEnabled
            , string[] resellerSalesPersons
            , bool isDirectSalesPerson)
        {
            this.ResellerName = resellername;
            this.Email = email;
            this.ContactNumber = contactnumber;
            this.Discount = discount;
            this.CreatedOn = createdon;
            this.Loginname = login;
            this.Password = password;
            this.City = city;
            this.Country = country;
            this.IsPaymentApproval = isPaymentApproval;
            this.IsEnabled = isEnabled;
            this.ResellersSalesPersons = resellerSalesPersons;
            this.IsDirectSalesReseller = isDirectSalesPerson;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
                //check if the login name that is provided is unique
                DataTable dtblAllLoginNames = getAllLoginNames();
                string strFilter = String.Format("LOGIN_NAME ='{0}'", this.Loginname);
                if (dtblAllLoginNames.Select(strFilter).Length > 0)
                {
                    this.StatusDescription = "Login name already taken";
                    return;
                }
                else
                {
                    if (this.IsDirectSalesReseller)
                    {
                        GetResellerDetailsAdmin objGetReseller = new
                        GetResellerDetailsAdmin();
                        string strResellerId = objGetReseller.getDirectSalesResellerId();
                        if (objGetReseller.StatusCode != 0)
                        {
                            throw new Exception();
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(strResellerId))
                            {
                                throw new MficientException("A direct sales reseller already exists.");
                            }
                        }
                    }
                    try
                    {
                        SqlTransaction transaction = null;
                        SqlConnection con;
                        Utilities.SqlConnectionOpen(out con);
                        string resellerId;
                        try
                        {
                            using (con)
                            {
                                transaction = con.BeginTransaction();
                                saveResellerDetails(con, transaction, out resellerId);
                                saveResellerSalesPersons(this.ResellersSalesPersons, resellerId, con, transaction);
                                transaction.Commit();
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                            {
                                this.StatusDescription = "Internal server error,Please try again";
                            }
                            this.StatusDescription = "Internal server error,Please try again";
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                            {
                                this.StatusDescription = "Internal server error,Please try again";
                            }
                            this.StatusDescription = "Internal server error,Please try again";
                        }
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                        {
                            this.StatusDescription = "Internal server error,Please try again";
                        }
                        this.StatusDescription = "Internal server error,Please try again";
                    }
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }

        DataTable getAllLoginNames()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("SELECT LOGIN_NAME FROM ADMIN_TBL_RESELLER_DETAIL UNION SELECT LOGIN_NAME FROM ADMIN_TBL_LOGIN_USER_DETAIL");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }

        }

        void saveResellerSalesPersons(string[] resellerSalesPersons, string resellerId, SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"INSERT INTO [ADMIN_TBL_RESELLER_SALES_PERSONS]
                                   ([RESELLER_ID]
                                   ,[SELLER_ID])
                             VALUES
                                   (@RESELLER_ID
                                   ,@SELLER_ID)";

                for (int i = 0; i < resellerSalesPersons.Length; i++)
                {
                    SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                    cmd.Parameters.AddWithValue("@RESELLER_ID", resellerId);
                    cmd.Parameters.AddWithValue("@SELLER_ID", resellerSalesPersons[i]);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusDescription = "Internal server error,Please try again";
                }
            }
        }

        void saveResellerDetails(SqlConnection con, SqlTransaction transaction, out string resellerId)
        {
            resellerId = Utilities.GetMd5Hash(this.ResellerName + this.Email + DateTime.UtcNow.Ticks);
            try
            {
                string sql = @"INSERT INTO ADMIN_TBL_RESELLER_DETAIL([RESELLER_NAME],[EMAIL],[CONTACT_NUMBER],[IS_ENABLE],[DISCOUNT],[CREATED_ON],[RESELLER_ID],[LOGIN_NAME],[PASSWORD],[CITY]
                                                                ,[COUNTRY],[IS_PAYMENT_APPROVAL]) VALUES 
                         (@resellername,@email,@Contactnum,@IsEnable,@discount,@createdon,@resellerid,@login,@password,@city,@country,@isPaymentApproval);";

                SqlCommand objSqlCmd = new SqlCommand(sql, con, transaction);
                objSqlCmd.Parameters.AddWithValue("@resellername", this.ResellerName);
                objSqlCmd.Parameters.AddWithValue("@email", this.Email);
                objSqlCmd.Parameters.AddWithValue("@Contactnum", this.ContactNumber);
                objSqlCmd.Parameters.AddWithValue("@IsEnable", this.IsEnabled == 1 ? 1 : 0);
                objSqlCmd.Parameters.AddWithValue("@discount", this.Discount);
                objSqlCmd.Parameters.AddWithValue("@createdon", this.CreatedOn);
                objSqlCmd.Parameters.AddWithValue("@resellerid", resellerId);
                objSqlCmd.Parameters.AddWithValue("@login", this.Loginname);
                objSqlCmd.Parameters.AddWithValue("@password", Utilities.GetMd5Hash(this.Password));
                objSqlCmd.Parameters.AddWithValue("@city", this.City);
                objSqlCmd.Parameters.AddWithValue("@country", this.Country);
                objSqlCmd.Parameters.AddWithValue("@isPaymentApproval", this.IsPaymentApproval == 1 ? 1 : 0);
                objSqlCmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        public string ResellerName
        {
            set;
            get;
        }
        public string Email
        {
            set;
            get;
        }
        public string ContactNumber
        {
            get;
            set;
        }
        public string SellerId
        {
            get;
            set;
        }
        public string ReSellerId
        {
            get;
            set;
        }
        public DateTime CreatedOn
        {
            get;
            set;
        }
        public string Loginname
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string[] ResellersSalesPersons
        {
            set;
            get;
        }
        public string Discount
        {
            set;
            get;
        }
        public string City
        {
            set;
            get;
        }
        public string Country
        {
            set;
            get;
        }
        public Byte IsPaymentApproval
        {
            set;
            get;
        }
        public Byte IsEnabled
        {
            set;
            get;
        }
        public bool IsDirectSalesReseller
        {
            private set;
            get;
        }
    }
}
