﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class CheckLoginAdmin
    {
        
        public DataTable GetEnquiryList()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select  top 5 case when r.RESELLER_NAME IS null then 'Not Alloted' else r.RESELLER_NAME +'(' +'Alloted'+')' 
                end as Status ,c.COUNTRY_NAME,d.COMPANY_NAME,d.EXPECTED_WF from ADMIN_TBL_MST_COUNTRY c inner join ADMIN_TBL_ENQUIRY_DETAIL d on c.COUNTRY_CODE=d.COUNTRY_CODE
                left outer join ADMIN_TBL_RESELLER_DETAIL r on d.RESELLER_ID=r.RESELLER_ID
                 order by d.ENQUIRY_DATETIME desc");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetSMlist(string loginname)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select top 5 d.COMPANY_NAME,d.EXPECTED_WF,c.COUNTRY_NAME from ADMIN_TBL_ENQUIRY_DETAIL d inner join
                                                    ADMIN_TBL_MST_COUNTRY c 
                                                    on d.COUNTRY_CODE=c.COUNTRY_CODE  where d.SALES_MNGR_ID in 
                                                    (select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname)");
                sqlCommand.Parameters.AddWithValue("@loginname", loginname);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                 return ds.Tables[0];
                
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetSPlist(string loginname)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select d.COMPANY_NAME,d.EXPECTED_WF,c.COUNTRY_NAME from ADMIN_TBL_ENQUIRY_DETAIL d inner join ADMIN_TBL_MST_COUNTRY c 
                                                    on d.COUNTRY_CODE=c.COUNTRY_CODE  where d.SALES_ID in 
                                                    (select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname)");
                sqlCommand.Parameters.AddWithValue("@loginname", loginname);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }


        public DataTable GetRlist(string loginname)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select d.COMPANY_NAME,d.EXPECTED_WF,c.COUNTRY_NAME from ADMIN_TBL_ENQUIRY_DETAIL d inner join ADMIN_TBL_MST_COUNTRY c 
                                                    on d.COUNTRY_CODE=c.COUNTRY_CODE  where d.RESELLER_ID in 
                                                    (select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname)");
                sqlCommand.Parameters.AddWithValue("@loginname", loginname);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public int count
        {
            get;
            set;
        }
        public string loginname
        {
            get;
            set;
        }

        public string pass
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}