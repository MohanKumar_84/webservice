﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class CompanyRegistrationNew
    {
        MFECompanyAdmin _cmpAdmin;
        MFECompanyInfo _cmpInfo;
        MFECurrentPlan _currPlan;
        MFEPlanPurchaseLog _purchaseLog;

        #region Previous Constructor
        //public CompanyRegistrationNew(string _Email, string _AccessCode, string _FirstName, string _MiddleName, string _LastName, string _Mobile, string _StreetAddress1,
        //    string _StreetAddress2, string _StreetAddress3, string _CityName, string _StateName, string _CountryCode, string _Zip, string _DateOfBirth, string _Gender,
        //    string _CompanyName, string _CompanyRegNo, string _CompanyStreetAddress1, string _CompanyStreetAddress2, string _CompanyStreetAddress3,
        //    string _CompanyCityName, string _CompanyStateName, string _CompanyCountryCode, string _CompanyZip, string _SupportEmail, string _SupportContact, string _ResellerId
        //    , string _planCode, string _maxWorkFlow, string _maxUsers, string _userChargeType, string _chargePerMnth, string _validity, string _totalUsers
        //    , string _priceWithoutDiscount, string _discountAmount, string _amountPayable, string _transactionType, bool isTrial, string enquiryId
        //    , int pushMessagePerDay, int pushMessagePerMnth, string planName
        //    , string companyId)//isTrial,enquiryId ,int pushMessagePerDay,int pushMessagePerMnth= //25/9/2012//planName on 27/9/2012
        //{
        //    Email = _Email;
        //    AccessCode = _AccessCode;
        //    FirstName = _FirstName;
        //    MiddleName = _MiddleName;
        //    LastName = _LastName;
        //    Mobile = _Mobile;

        //    StreetAddress1 = _StreetAddress1;
        //    StreetAddress2 = _StreetAddress2;
        //    StreetAddress3 = _StreetAddress3;
        //    CityName = _CityName;
        //    StateName = _StateName;
        //    CountryCode = _CountryCode;
        //    Zip = _Zip;
        //    DateOfBirth = _DateOfBirth;
        //    Gender = _Gender;
        //    CompanyName = _CompanyName;
        //    CompanyRegNo = _CompanyRegNo;
        //    CompanyStreetAddress1 = _CompanyStreetAddress1;
        //    CompanyStreetAddress2 = _CompanyStreetAddress2;
        //    CompanyStreetAddress3 = _CompanyStreetAddress3;
        //    CompanyCityName = _CompanyCityName;
        //    CompanyStateName = _CompanyStateName;
        //    CompanyCountryCode = _CompanyCountryCode;
        //    CompanyZip = _CompanyZip;
        //    SupportEmail = _SupportEmail;
        //    SupportContact = _SupportContact;
        //    ResellerId = _ResellerId;
        //    this.IsTrial = isTrial;
        //    this.EnquiryId = enquiryId;
        //    this.PushMessagePerDay = pushMessagePerDay;
        //    this.PushMessagePerMnth = pushMessagePerMnth;
        //    this.CompanyId = companyId;
        //    //for payment
        //    this.PlanCode = _planCode;
        //    this.MaxWorkFlow = _maxWorkFlow;
        //    this.MaxUsers = _maxUsers;
        //    this.UserChargeType = _userChargeType;
        //    this.ChargePM = _chargePerMnth;
        //    this.Validity = _validity;
        //    //this.TotalUsers = _totalUsers;
        //    this.PriceWithoutDiscount = _priceWithoutDiscount;
        //    this.DiscountAmount = _discountAmount;
        //    this.AmountPayable = _amountPayable;
        //    this.TransactionType = _transactionType;

        //    this.PlanName = planName;

        //}
        #endregion
        public CompanyRegistrationNew(
            MFECompanyAdmin cmpAdmin,
            MFECurrentPlan currPlan,
            MFECompanyInfo cmpInfo,
            MFEPlanPurchaseLog purchaseLog)
        {
            //using CmpAdmin
            Email = cmpAdmin.EmailId;
            AccessCode = cmpAdmin.Password;
            FirstName = cmpAdmin.Firstname;
            MiddleName = cmpAdmin.MiddleName;
            LastName = cmpAdmin.LastName;
            Mobile = cmpAdmin.MobileNo;
            StreetAddress1 = cmpAdmin.StreetAddress1;
            StreetAddress2 = cmpAdmin.StreetAddress2;
            StreetAddress3 = cmpAdmin.StreetAddress3;
            CityName = cmpAdmin.CityName;
            StateName = cmpInfo.StateName;
            CountryCode = cmpAdmin.CountryCode;
            Zip = cmpAdmin.Zip;
            DateOfBirth = Convert.ToString(cmpAdmin.Dob);
            Gender = cmpAdmin.Gender;
            this.IsTrial = cmpAdmin.IsTrial;
            ResellerId = cmpAdmin.ResellerId;

            //Using Company Info
            CompanyName = cmpInfo.CompanyName;
            CompanyRegNo = cmpInfo.RegisterationNo;
            CompanyStreetAddress1 = cmpInfo.StreetAddress1;
            CompanyStreetAddress2 = cmpInfo.StreetAddress2;
            CompanyStreetAddress3 = cmpInfo.StreetAddress3;
            CompanyCityName = cmpInfo.CityName;
            CompanyStateName = cmpInfo.StateName;
            CompanyCountryCode = cmpInfo.CountryCode;
            CompanyZip = cmpInfo.Zip;
            SupportEmail = cmpInfo.SupportEmail;
            SupportContact = cmpInfo.SupportContact;
            this.TimezoneId = cmpInfo.TimezoneId;

            //Using Current Plan
            this.EnquiryId = String.Empty;
            this.PushMessagePerDay = currPlan.PushMsgPerDay;
            this.PushMessagePerMnth = currPlan.PushMsgPerMonth;
            this.CompanyId = cmpInfo.CompanyId;
            //for payment
            this.PlanCode = currPlan.PlanCode;
            this.MaxWorkFlow = Convert.ToString(currPlan.MaxWorkFlow);
            this.MaxUsers = Convert.ToString(currPlan.MaxUser);
            this.UserChargeType = currPlan.ChargeType;
            this.ChargePM = Convert.ToString(currPlan.UserChargePerMonth);
            this.Validity = Convert.ToString(currPlan.Validity);
            this.PlanName = currPlan.NextMnthPlan;
            //this.TotalUsers = _totalUsers;

            //Using Purchase Log
            this.PriceWithoutDiscount = Convert.ToString(purchaseLog.PriceWithoutDiscount);
            this.DiscountAmount = Convert.ToString(purchaseLog.DiscountAmount);
            this.AmountPayable = Convert.ToString(purchaseLog.ActualPrice);
            this.TransactionType = Convert.ToString(purchaseLog.TransactionType);

            this.CmpAdmin = cmpAdmin;
            this.CmpInfo = cmpInfo;
            this.CurrPlan = currPlan;
            this.PurchaseLog = purchaseLog;

        }

        public void Process(HttpContext context)
        {
            StatusCode = -1000;//for error
            string strUniqueUserID;
            RegistrationDatetime = DateTime.UtcNow.Ticks;

            string strPassword = Utilities.GetMd5Hash(AccessCode);
            strUniqueUserID = Utilities.GetMd5Hash(Email + Mobile + FirstName + MiddleName + LastName + RegistrationDatetime.ToString()).Substring(0, 16);
            TransactionId = Utilities.GetMd5Hash(FirstName + MiddleName + LastName + RegistrationDatetime.ToString()).Substring(0, 16);

            SqlTransaction transaction = null;
            SqlConnection con;
            Utilities.SqlConnectionOpen(out con);
            try
            {
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        saveCompanyAdministrator(transaction, con, strUniqueUserID, strPassword, DateOfBirth);
                        saveCompanyDetails(transaction, con, strUniqueUserID);


                        saveCompanyCurrentPlan(transaction, con);
                        if (!IsTrial)
                        {
                            savePlanPurchaseLog(transaction, con);
                        }

                        transaction.Commit();
                        try
                        {
                            SaveEmailInfo.companyRegistration(this.CmpAdmin.EmailId,
                                this.CmpAdmin.Firstname + " " + this.CmpAdmin.MiddleName + " " + this.CmpAdmin.LastName,
                                this.CmpAdmin.EmailId,
                                this.CmpInfo.CompanyId,
                                context);
                        }
                        catch
                        { }
                        StatusCode = 0;
                        StatusDescription = "";
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        void saveCompanyAdministrator(SqlTransaction transaction, SqlConnection con, string adminId, string password, string dateOfBirth)
        {
            try
            {

                string query = @"INSERT INTO ADMIN_TBL_COMPANY_ADMINISTRATOR(ADMIN_ID,EMAIL_ID,ACCESS_CODE,FIRST_NAME,LAST_NAME,MOBILE,STREET_ADDRESS1
                                                                          ,STREET_ADDRESS2,STREET_ADDRESS3,CITY_NAME,STATE_NAME,COUNTRY_CODE,ZIP,DATE_OF_BIRTH,
                                                                          REGISTRATION_DATETIME,GENDER,MIDDLE_NAME,RESELLER_ID,IS_TRIAL)
                                                                  VALUES(@ADMIN_ID,@EMAIL_ID,@ACCESSCODE,@FIRSTNAME,@LASTNAME,@MOBILE,@STREETADDRESS1
                                                                         ,@STREETADDRESS2,@STREETADDRESS3,@CITYNAME,@STATENAME,@COUNTRYCODE,@ZIP,@DATEOFBIRTH,
                                                                        @REGISTRATIONDATETIME,@GENDER,@MIDDLENAME,@RESELLER_ID,@IsTrial)";
                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);

                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", adminId);
                objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", Email.ToLower());
                objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", password);
                objSqlCommand.Parameters.AddWithValue("@FIRSTNAME", FirstName);
                objSqlCommand.Parameters.AddWithValue("@LASTNAME", LastName);
                objSqlCommand.Parameters.AddWithValue("@MOBILE", Mobile);
                objSqlCommand.Parameters.AddWithValue("@STREETADDRESS1", StreetAddress1);
                objSqlCommand.Parameters.AddWithValue("@STREETADDRESS2", StreetAddress2);
                objSqlCommand.Parameters.AddWithValue("@STREETADDRESS3", StreetAddress3);
                objSqlCommand.Parameters.AddWithValue("@CITYNAME", CityName);
                objSqlCommand.Parameters.AddWithValue("@STATENAME", StateName);
                objSqlCommand.Parameters.AddWithValue("@COUNTRYCODE", CountryCode);
                objSqlCommand.Parameters.AddWithValue("@ZIP", Zip);
                objSqlCommand.Parameters.AddWithValue("@DATEOFBIRTH", DateOfBirth);
                objSqlCommand.Parameters.AddWithValue("@REGISTRATIONDATETIME", RegistrationDatetime);
                objSqlCommand.Parameters.AddWithValue("@GENDER", Gender);
                objSqlCommand.Parameters.AddWithValue("@MIDDLENAME", MiddleName);
                objSqlCommand.Parameters.AddWithValue("@RESELLER_ID", ResellerId);
                objSqlCommand.Parameters.AddWithValue("@IsTrial", IsTrial ? 1 : 0);//25/9/2012
                objSqlCommand.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void saveCompanyDetails(SqlTransaction transaction, SqlConnection con, string adminId)
        {
            try
            {
                string strQuery = @"INSERT INTO ADMIN_TBL_COMPANY_DETAIL(COMPANY_ID,ADMIN_ID,COMPANY_NAME,REGISTRATION_NO,STREET_ADDRESS1,
					            STREET_ADDRESS2,STREET_ADDRESS3,CITY_NAME,STATE_NAME,COUNTRY_CODE,ZIP,
                                SUPPORT_EMAIL,SUPPORT_CONTACT,LOGO_IMAGE_NAME,RESELLER_ID
                                ,UPDATED_ON,ENQUIRY_ID,SM_APPROVED,SM_APPROVED_DATE,TIMEZONE_ID,IS_BLOCKED,BLOCK_UNBLOCK_ID)
		                        VALUES(
                                @COMPANY_ID,@ADMIN_ID,@COMPANY_NAME,@REGISTRATION_NO,@STREET_ADDRESS1,
				                @STREET_ADDRESS2,@STREET_ADDRESS3,@CITY_NAME,@STATE_NAME,@COUNTRY_CODE,@ZIP,@SupportEmail,@SupportContact,@LOGO_IMAGE_NAME,@RESELLER_ID
                               ,@UpdatedOn,@EnquiryId,@SMApproved,@SMApprovedDate,@TIMEZONE_ID,
                               @IS_BLOCKED,@BLOCK_UNBLOCK_ID);";

                SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", adminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_NAME", CompanyName);
                objSqlCommand.Parameters.AddWithValue("@REGISTRATION_NO", CompanyRegNo);
                objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS1", StreetAddress1);
                objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS2", StreetAddress2);
                objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS3", StreetAddress3);
                objSqlCommand.Parameters.AddWithValue("@CITY_NAME", CityName);
                objSqlCommand.Parameters.AddWithValue("@STATE_NAME", StateName);
                objSqlCommand.Parameters.AddWithValue("@COUNTRY_CODE", CountryCode);
                objSqlCommand.Parameters.AddWithValue("@ZIP", Zip);
                objSqlCommand.Parameters.AddWithValue("@SupportEmail", SupportEmail);
                objSqlCommand.Parameters.AddWithValue("@SupportContact", SupportContact);
                objSqlCommand.Parameters.AddWithValue("@LOGO_IMAGE_NAME", "");
                objSqlCommand.Parameters.AddWithValue("@RESELLER_ID", ResellerId);
                objSqlCommand.Parameters.AddWithValue("@UpdatedOn", RegistrationDatetime);
                objSqlCommand.Parameters.AddWithValue("@EnquiryId", EnquiryId);
                objSqlCommand.Parameters.AddWithValue("@SMApproved", 0);
                objSqlCommand.Parameters.AddWithValue("@SMApprovedDate", 0);
                objSqlCommand.Parameters.AddWithValue("@TIMEZONE_ID", this.TimezoneId);
                objSqlCommand.Parameters.AddWithValue("@IS_BLOCKED", 0);
                objSqlCommand.Parameters.AddWithValue("@BLOCK_UNBLOCK_ID", String.Empty);
                objSqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        void saveCompanyCurrentPlan(SqlTransaction transaction, SqlConnection con)
        {
            try
            {
                string query = @"INSERT INTO ADMIN_TBL_COMPANY_CURRENT_PLAN(COMPANY_ID,PLAN_CODE,MAX_WORKFLOW,MAX_USER,USER_CHARGE_PM,CHARGE_TYPE,VALIDITY
                            ,PURCHASE_DATE,PLAN_CHANGE_DATE,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH,FEATURE3,FEATURE4,FEATURE5,FEATURE6,FEATURE7,FEATURE8,FEATURE9,FEATURE10,BALANCE_AMOUNT,NEXT_MONTH_PLAN,NEXT_MONTH_PLAN_LAST_UPDATED)
                            VALUES(@COMPANY_ID,@PLAN_CODE,@MAX_WORKFLOW,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE,@VALIDITY,@PURCHASE_DATE,@PLAN_CHANGE_DATE
                            ,@PUSHMESSAGE_PERDAY,@PUSHMESSAGE_PERMONTH,@FEATURE3,@FEATURE4,@FEATURE5,@FEATURE6,@FEATURE7,@FEATURE8,@FEATURE9,@FEATURE10,@BALANCE_AMOUNT,@NEXT_MONTH_PLAN,@NEXT_MONTH_PLAN_LAST_UPDATED);";

                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                objSqlCommand.Parameters.AddWithValue("@MAX_USER", Convert.ToInt16(this.MaxUsers));
                objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDecimal(this.ChargePM));
                objSqlCommand.Parameters.AddWithValue("@CHARGE_TYPE", this.UserChargeType);
                objSqlCommand.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(this.Validity));
                objSqlCommand.Parameters.AddWithValue("@PURCHASE_DATE", RegistrationDatetime);
                objSqlCommand.Parameters.AddWithValue("@PLAN_CHANGE_DATE", 0);
                objSqlCommand.Parameters.AddWithValue("@PUSHMESSAGE_PERDAY", this.PushMessagePerDay);
                objSqlCommand.Parameters.AddWithValue("@PUSHMESSAGE_PERMONTH", this.PushMessagePerMnth);
                objSqlCommand.Parameters.AddWithValue("@FEATURE3", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE4", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE5", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE6", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE7", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE8", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE9", 0);
                objSqlCommand.Parameters.AddWithValue("@FEATURE10", 0);

                objSqlCommand.Parameters.AddWithValue("@BALANCE_AMOUNT", 0);
                objSqlCommand.Parameters.AddWithValue("@NEXT_MONTH_PLAN", this.PlanCode);
                objSqlCommand.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);

                objSqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        void savePlanPurchaseLog(SqlTransaction transaction, SqlConnection con)
        {
            try
            {
                string strQuery = @"INSERT INTO [ADMIN_TBL_PLAN_PURCHASE_LOG]
           ([COMPANY_ID],[PLAN_CODE],[MAX_WORKFLOW]
           ,[MAX_USER],[USER_CHARGE_PM],[CHARGE_TYPE]
           ,[VALIDITY],[PRICE_WITHOUT_DISCOUNT],[DISCOUNT_PER]
           ,[ACTUAL_PRICE],[PURCHASE_DATE],[TRANSACTION_ID]
           ,[TRANSACTION_TYPE],[PLAN_NAME],[APPROVED]
           ,[APPROVED_DATE],[GET_PAYMENT],[GET_PAYMENT_DATE]
           ,[RESELLER_ID],[AMOUNT_RECEIVED])
     VALUES
           (@COMPANY_ID,@PLAN_CODE,@MAX_WORKFLOW
           ,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE
           ,@VALIDITY,@PRICE_WITHOUT_DISCOUNT,@DISCOUNT_PER
           ,@ACTUAL_PRICE,@PURCHASE_DATE,@TRANSACTION_ID
           ,@TRANSACTION_TYPE,@PLAN_NAME,@APPROVED
           ,@APPROVED_DATE,@GET_PAYMENT,@GET_PAYMENT_DATE
           ,@RESELLER_ID,@AMOUNT_RECEIVED)";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                cmd.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                cmd.Parameters.AddWithValue("@MAX_USER", Convert.ToInt32(this.MaxUsers));
                cmd.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDouble(this.ChargePM));
                cmd.Parameters.AddWithValue("@CHARGE_TYPE", this.UserChargeType);
                cmd.Parameters.AddWithValue("@VALIDITY", Convert.ToInt64(this.Validity));
                cmd.Parameters.AddWithValue("@PRICE_WITHOUT_DISCOUNT", Convert.ToDecimal(this.PriceWithoutDiscount));
                //cmd.Parameters.AddWithValue("@DISCOUNT_PER", Convert.ToDecimal(this.DiscountPercent));//
                cmd.Parameters.AddWithValue("@DISCOUNT_PER", Convert.ToDecimal(this.DiscountAmount));//
                cmd.Parameters.AddWithValue("@ACTUAL_PRICE", Convert.ToDecimal(this.AmountPayable));
                cmd.Parameters.AddWithValue("@PURCHASE_DATE", this.RegistrationDatetime);
                cmd.Parameters.AddWithValue("@TRANSACTION_ID", this.TransactionId);
                cmd.Parameters.AddWithValue("@TRANSACTION_TYPE", this.TransactionType);
                cmd.Parameters.AddWithValue("@PLAN_NAME", this.PlanName);
                cmd.Parameters.AddWithValue("@APPROVED", 0);
                cmd.Parameters.AddWithValue("@APPROVED_DATE", 0);
                cmd.Parameters.AddWithValue("@GET_PAYMENT", 0);
                cmd.Parameters.AddWithValue("@GET_PAYMENT_DATE", 0);
                cmd.Parameters.AddWithValue("@RESELLER_ID", this.ResellerId);
                cmd.Parameters.AddWithValue("@AMOUNT_RECEIVED", 0);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string Validate()
        {
            return "";
        }

        string getUniqueCompanyId()
        {
            string strNewCompanyId = string.Empty;
            //changed on 25/9/2012
            //string strCompanyId = Utilities.GetMd5Hash(strCompanyName + DateTime.UtcNow.Ticks.ToString()).Substring(0, 16);
            //string strCompanyIdPreFix = "1";
            string strCompanyId = Utilities.GetMd5Hash(CompanyName + DateTime.UtcNow.Ticks.ToString());
            int index = 0;
            int iNextIndexToStart = 0;
            Boolean blnIsCompanyIdUnique = false;
            //  string strNewCompanyId = string.Empty;
            do
            {
                //we have to get company id which is of 8 characters
                //strNewCompanyId = strCompanyIdPreFix + strCompanyId.Substring(index, 8);
                //blnIsCompanyIdUnique = isCompanyIdUnique(strNewCompanyId);
                //index = index + 1;//25/9/2012
                strNewCompanyId = companyIdNotStartingWithADigit(strCompanyId, index, out iNextIndexToStart);
                blnIsCompanyIdUnique = isCompanyIdUnique(strNewCompanyId);
                index = iNextIndexToStart;
            }
            while (blnIsCompanyIdUnique == false);

            return strNewCompanyId;
        }
        bool isCompanyIdUnique(string companyId)
        {
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"SELECT COMPANY_ID 
                                                        FROM ADMIN_TBL_COMPANY_DETAIL
                                                        WHERE COMPANY_ID = @CompanyId");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CompanyDet()
        {

        }

        string companyIdNotStartingWithADigit(string companyId, int index, out int nextIndexToStart)
        {
            bool isValidString = false;
            string strNewCompanyId, strOriginalCompanyId;
            strOriginalCompanyId = companyId;
            int iNumber;
            do
            {
                try
                {
                    if (int.TryParse(companyId.Substring(index, 1), out iNumber))
                    {
                        index++;
                        //string str1 = companyId.Substring(index);
                        //string str = companyId.Substring(index);
                        //companyId = companyId.Substring(index);//gets the index of the first character in the md5
                    }
                    else
                    {
                        companyId = strOriginalCompanyId.Substring(index, 8);
                        isValidString = true;
                    }
                }
                catch (Exception e)
                {

                }
            }
            while (!isValidString);
            strNewCompanyId = companyId;
            nextIndexToStart = index + 1;
            return strNewCompanyId;

        }

        #region Properties
        public string Email
        {
            get;
            set;
        }
        public string AccessCode
        {
            get;
            set;
        }
        public string FirstName
        {
            get;
            set;
        }
        public string MiddleName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        public string Gender
        {
            get;
            set;
        }
        public string Mobile
        {
            get;
            set;

        }
        public string CompanyId
        {
            get;
            set;
        }
        public string StreetAddress1
        {
            get;
            set;
        }
        public string StreetAddress2
        {
            get;
            set;
        }
        public string StreetAddress3
        {
            get;
            set;
        }
        public string CityName
        {
            get;
            set;
        }
        public string StateName
        {
            get;
            set;
        }
        public string CountryCode
        {
            get;
            set;

        }
        public string Zip
        {
            get;
            set;
        }
        public string DateOfBirth
        {
            get;
            set;
        }
        public string CompanyName
        {
            get;
            set;
        }
        public string CompanyRegNo
        {
            get;
            set;
        }
        public string CompanyStreetAddress1
        {
            get;
            set;
        }
        public string CompanyStreetAddress2
        {
            get;
            set;
        }
        public string CompanyStreetAddress3
        {
            get;
            set;
        }
        public string CompanyCityName
        {
            get;
            set;

        }
        public string CompanyStateName
        {
            get;
            set;
        }
        public string CompanyCountryCode
        {
            get;
            set;
        }
        public string CompanyZip
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SupportEmail
        {
            get;
            set;
        }
        public string SupportContact
        {
            get;
            set;
        }
        public string ResellerId
        {
            get;
            set;
        }


        public string PlanCode
        {
            set;
            get;
        }
        public string MaxWorkFlow
        {
            set;
            get;
        }
        public string MaxUsers
        {
            set;
            get;
        }
        public string HighestWorkflow
        {
            set;
            get;
        }
        public string UserChargeType
        {
            set;
            get;
        }
        public string ChargePM
        {
            set;
            get;
        }
        public string Validity
        {
            set;
            get;
        }






        public string AdminId
        {
            set;
            get;
        }
        //public string TotalUsers
        //{
        //    set;
        //    get;
        //}
        public string PriceWithoutDiscount
        {
            set;
            get;
        }
        public string DiscountAmount
        {
            set;
            get;
        }
        public string AmountPayable
        {
            set;
            get;
        }
        public string TransactionType
        {
            set;
            get;
        }

        public bool IsTrial
        {
            set;
            get;
        }

        public string EnquiryId
        {
            set;
            get;
        }

        public int PushMessagePerDay
        {
            set;
            get;
        }

        public int PushMessagePerMnth
        {
            set;
            get;
        }

        public long RegistrationDatetime
        {
            set;
            get;
        }

        public string TransactionId
        {
            set;
            get;
        }

        public string PlanName
        {
            set;
            get;
        }

        public string TimezoneId
        {
            private set;
            get;
        }
        public MFECompanyAdmin CmpAdmin
        {
            get { return _cmpAdmin; }
            private set { _cmpAdmin = value; }
        }

        public MFECompanyInfo CmpInfo
        {
            get { return _cmpInfo; }
            private set { _cmpInfo = value; }
        }

        public MFECurrentPlan CurrPlan
        {
            get { return _currPlan; }
            private set { _currPlan = value; }
        }


        public MFEPlanPurchaseLog PurchaseLog
        {
            get { return _purchaseLog; }
            private set { _purchaseLog = value; }
        }
        #endregion
    }
}