﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class CurrentPlanDetail
    {
        public CurrentPlanDetail(string _CompanyId)
        {
            CompanyId = _CompanyId;
            IsNewQuery = false;
        }
        //19/9/2012.Get plan details using new query.The above constructor was used in many classes before and working properly.
        //so not changing it now.
        public CurrentPlanDetail(string _CompanyId, bool isNewQuery,bool getFromCPTable)
        {
            CompanyId = _CompanyId;
            IsNewQuery = true;
            GetFromCPTable = getFromCPTable;
        }

        
        public void Process()
        {
            try
            {
                /*string strQuery = @"SELECT * FROM ADMIN_TBL_COMPANY_CURRENT_PLAN where COMPANY_ID=@COMPANY_ID;
                select IS_TRIAL,REGISTRATION_DATETIME from ADMIN_TBL_COMPANY_ADMINISTRATOR a inner join ADMIN_TBL_COMPANY_DETAIL c on a.ADMIN_ID=c.ADMIN_ID where c.COMPANY_ID=@COMPANY_ID";*/
                string strQuery = getSqlQuery();//19/9/2012
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsPriceDtls = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPriceDtls.Tables[0].Rows.Count > 0)
                {
                    PlanDetails = dsPriceDtls.Tables[0];
                    PlanDetails.TableName = "Plan";
                }
                else
                {
                    if (!GetFromCPTable)
                    {
                        PlanDetails = dsPriceDtls.Tables[1];
                        PlanDetails.TableName = "Trial";
                    }
                    else
                    {
                        PlanDetails = dsPriceDtls.Tables[0];
                    }
                }                
            }
            catch (Exception e)
            {
              this.StatusCode= (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
              this.StatusDescription = e.Message;
            }
        }

        string getSqlQuery()
        {
            string strSqlQuery = "";
            try
            {
                if (!IsNewQuery)
                {
                    strSqlQuery = @"SELECT * FROM ADMIN_TBL_COMPANY_CURRENT_PLAN where COMPANY_ID=@COMPANY_ID;
                select IS_TRIAL,REGISTRATION_DATETIME from ADMIN_TBL_COMPANY_ADMINISTRATOR a inner join ADMIN_TBL_COMPANY_DETAIL c on a.ADMIN_ID=c.ADMIN_ID where c.COMPANY_ID=@COMPANY_ID";

                }
                else
                {
                    if (!GetFromCPTable)
                    {
                        strSqlQuery = @"SELECT CompCurrPlan.*,CompCurrPlan.MAX_USER AS MaxUser,Plns.PLAN_CODE AS PlanCode,Plns.MAX_WORKFLOW AS MaxWorkFlow,Plns.PUSHMESSAGE_PERDAY AS PushMessagePerDay
                                ,Plns.PUSHMESSAGE_PERMONTH AS PushMessagePerMonth
                                FROM ADMIN_TBL_COMPANY_CURRENT_PLAN AS CompCurrPlan
                                INNER JOIN ADMIN_TBL_PLANS AS Plns
                                ON CompCurrPlan.PLAN_CODE = Plns.PLAN_CODE
                                WHERE CompCurrPlan.COMPANY_ID = @COMPANY_ID;

                                SELECT CompCurrPlan.*,CmpAdmin.IS_TRIAL,CmpAdmin.REGISTRATION_DATETIME,Plns.PLAN_CODE AS PlanCode,Plns.MAX_WORKFLOW AS MaxWorkFlow,Plns.MAX_USER AS MaxUser,Plns.PUSHMESSAGE_PERDAY AS PushMessagePerDay
                                ,Plns.PUSHMESSAGE_PERMONTH AS PushMessagePerMonth
                                FROM ADMIN_TBL_COMPANY_CURRENT_PLAN AS CompCurrPlan
                                INNER JOIN ADMIN_TBL_TRIAL_PLANS AS Plns
                                ON CompCurrPlan.PLAN_CODE = Plns.PLAN_CODE
                                INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
                                ON CmpDtl.COMPANY_ID  = CompCurrPlan.COMPANY_ID
                                INNER JOIN ADMIN_TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
                                ON CmpDtl.ADMIN_ID = CmpAdmin.ADMIN_ID
                                WHERE CompCurrPlan.COMPANY_ID = @COMPANY_ID;";
                    }
                    else
                    {
                        strSqlQuery = @"SELECT * FROM TBL_COMPANY_DETAIL
                                    WHERE COMPANY_ID = @COMPANY_ID;";
                    }
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strSqlQuery;
        }

        public DataTable PlanDetails
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public bool IsNewQuery
        {
            get;
            set;
        }
        public bool GetFromCPTable
        {
            get;
            set;
        }
    }
}