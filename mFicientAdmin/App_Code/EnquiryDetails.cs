﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class EnquiryDetailsAdmin
    {
        DataSet ds;
        public DataTable GetCountrydd(string CountryName)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select ENQUIRY_ID, COMPANY_NAME,TYPE_OF_COMPANY from ADMIN_TBL_ENQUIRY_DETAIL where COUNTRY_CODE in
                                                    (select COUNTRY_CODE from ADMIN_TBL_MST_COUNTRY where COUNTRY_NAME=@countryname)");
                sqlCommand.Parameters.AddWithValue("@countryname", CountryName);
                ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetStatusdd(string status)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select COMPANY_NAME,TYPE_OF_COMPANY from ADMIN_TBL_ENQUIRY_DETAIL where STATUS=@status");
                sqlCommand.Parameters.AddWithValue("@status", status);
                ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        //public DataTable GetRDetails(string loginname)
        //{
        //    try
        //    {
        //        string strQuery = "select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname;";
        //        SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //        objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
        //        objSqlCommand.CommandType = CommandType.Text;
        //        ds = Utilities.SelectDataFromSQlCommand(objSqlCommand);
        //        return ResultTable = ds.Tables[0];
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        //public DataTable GetRemarkcode(string remark)
        //{
        //    try
        //    {
        //        string strQuery = "SELECT REMARK_CODE FROM admin_tbl_enquiry_remarks WHERE REMARK=@remark;";
        //        SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //        objSqlCommand.Parameters.AddWithValue("@remark", remark);
        //        objSqlCommand.CommandType = CommandType.Text;
        //        ds = Utilities.SelectDataFromSQlCommand(objSqlCommand);
        //        return ResultTable = ds.Tables[0];
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
        public void UpdateEnquiryDetails(string remark,string status,string enquiryid)
        {
            try
            {
                string sql = @"update ADMIN_TBL_ENQUIRY_DETAIL set STATUS=@status, REMARK_CODE=@remark where ENQUIRY_ID=@enquiryid";

                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.Parameters.AddWithValue("@remark", remark);
                objSqlCmd.Parameters.AddWithValue("@status", status);
                objSqlCmd.Parameters.AddWithValue("@enquiryid", enquiryid);
                objSqlCmd.CommandType = CommandType.Text;

                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {

                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = 1000;
                    this.StatusDescription = "Record cant be inserted";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = 1000;
                this.StatusDescription = e.Message;
            }
        }

        public DataTable GetUserDetails(string loginname,string type)
        {
            try
            {
                string strQuery = "select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where TYPE=@type and USER_NAME=@loginname;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
                objSqlCommand.Parameters.AddWithValue("@type", type);
                objSqlCommand.CommandType = CommandType.Text;
                ds = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                return ds.Tables[0];
            }
            catch
            {
                return null;
            }
        }


        public string GetId(string loginname,string Type)
        {
            string Id = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand(@"select LOGIN_ID  from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname and TYPE=@type");
                cmd.Parameters.AddWithValue("@loginname", loginname);
                cmd.Parameters.AddWithValue("@type", "SM");
                DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                     Id = Convert.ToString(objDs.Tables[0].Rows[0]["LOGIN_ID"]);
                    return Id;
                }
                else
                {
                    return Id = "";
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return Id;
        }

        public string GetSalesExecutiveId(string loginname,string Type)
        {
            string Id = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand(@"select LOGIN_ID  from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname and TYPE=@type");
                cmd.Parameters.AddWithValue("@loginname", loginname);
                cmd.Parameters.AddWithValue("@type", "S");
                DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    Id = Convert.ToString(objDs.Tables[0].Rows[0]["LOGIN_ID"]);
                    return Id;
                }
                else
                {
                    return Id="";
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void GetRemarks()
        {
            try
            {
                string strQuery = "select remark,REMARK_CODE from admin_tbl_enquiry_remarks";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }       

        public void UpdateDetailsR(string status,string SalesId, string Resellerid,string EnquiryId)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"update ADMIN_TBL_ENQUIRY_DETAIL set RESELLER_ID=@resellerid,SALES_ID=@salesid,ALLOTED_RESALLER_DATE=@date,STATUS=@status where ENQUIRY_ID=@enquiryid;");
                objSqlCmd.Parameters.AddWithValue("@resellerid", Resellerid);
                objSqlCmd.Parameters.AddWithValue("@status", status);
                objSqlCmd.Parameters.AddWithValue("@enquiryid", EnquiryId);
                objSqlCmd.Parameters.AddWithValue("@salesid", SalesId);
                objSqlCmd.Parameters.AddWithValue("@date", DateTime.Now.Ticks);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch(Exception e)
             {
                 this.StatusDescription = e.Message;
                 this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
             }
        }

        public void UpdateStatus(string status, string enquiryid)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"update ADMIN_TBL_ENQUIRY_DETAIL set STATUS=@status where ENQUIRY_ID=@enquiryid");
                objSqlCmd.Parameters.AddWithValue("@enquiryid", enquiryid);
                objSqlCmd.Parameters.AddWithValue("@status", status);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;

             }
        }

        public void UpdateDetailsSP(string status,string mgrId, string userid,string EnquiryId)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"update ADMIN_TBL_ENQUIRY_DETAIL set SALES_ID=@salesid,SALES_MNGR_ID=@mgrid,ALLOTED_SALES_DATE=@date,STATUS=@status where ENQUIRY_ID=@enquiryid");
                objSqlCmd.Parameters.AddWithValue("@salesid", userid);
                objSqlCmd.Parameters.AddWithValue("@enquiryid", EnquiryId);
                objSqlCmd.Parameters.AddWithValue("@status", status);
                objSqlCmd.Parameters.AddWithValue("@mgrid", mgrId);
                objSqlCmd.Parameters.AddWithValue("@date", DateTime.Now.Ticks);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    this.StatusDescription = "record insert error";
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        public string GetUname(string EnquiryId, string Type)
        {
            string strName = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand(@"select LOGIN_NAME from ADMIN_TBL_LOGIN_USER_DETAIL where TYPE=@type and LOGIN_ID in(select SALES_ID
                        from ADMIN_TBL_ENQUIRY_DETAIL where ENQUIRY_ID=@enquiryid)");
                cmd.Parameters.AddWithValue("@enquiryid", EnquiryId);
                cmd.Parameters.AddWithValue("@type", "S");
                DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    strName = Convert.ToString(objDs.Tables[0].Rows[0]["LOGIN_NAME"]);
                    return strName;
                }
                else
                {
                    return strName = "";
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return strName;
        }
        
        public DataTable ResultTable
        {
            get;
            set;
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }   
    }
}