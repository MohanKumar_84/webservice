﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetCompanyDetailsFromCPTable
    {
        MFECompanyInfo _companyInfo;
        string _companyId;
        string _statusDescription;
        int _statusCode;




        #region Public Properties
        public MFECompanyInfo CompanyInfo
        {
            get { return _companyInfo; }
            private set { _companyInfo = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        #endregion

        public GetCompanyDetailsFromCPTable(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                SqlCommand cmd = sqlCmdForCompanyDetail();
                DataSet dsCmpDetails = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsCmpDetails == null) throw new Exception();
                if (dsCmpDetails.Tables[0].Rows.Count > 0)
                {
                    this.CompanyInfo = this.fillCmpInfoFromDtbl(dsCmpDetails.Tables[0]);
                }
                else
                {
                    this.CompanyInfo = new MFECompanyInfo();
                }
                this.StatusCode = 0;
                this.StatusDescription = string.Empty;
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        MFECompanyInfo fillCmpInfoFromDtbl(DataTable companyDetail)
        {
            MFECompanyInfo objCmpInfo = new MFECompanyInfo();
            objCmpInfo.CompanyId = Convert.ToString(companyDetail.Rows[0]["COMPANY_ID"]);
            objCmpInfo.CompanyName = Convert.ToString(companyDetail.Rows[0]["COMPANY_NAME"]);
            return objCmpInfo;
        }

        string sqlQueryForCompanyDetails()
        {
            return @"SELECT * FROM TBL_COMPANY_DETAIL
                    WHERE COMPANY_ID = @COMPANY_ID";
        }
        SqlCommand sqlCmdForCompanyDetail()
        {
            SqlCommand cmd = new SqlCommand(sqlQueryForCompanyDetails());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }
    }
}