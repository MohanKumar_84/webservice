﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetMasterServerDetails
    {
        string _statusDescription;
        int _statusCode;
        #region Public Property
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }

        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        #endregion
        public List<MFEmficientServer> getMficientServersDetails()
        {
            List<MFEmficientServer> lstMficientServers =
                    new List<MFEmficientServer>();
            try
            {
                string strQuery = getAllServersSqlQuery();
                SqlCommand cmd = getCommandToGetAllServers(strQuery);
                DataSet dsServerDetail = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsServerDetail == null) throw new Exception(
                    (
                    (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR
                    ).ToString()
                    );
                foreach (DataRow row in dsServerDetail.Tables[0].Rows)
                {
                    lstMficientServers.Add(getServerFromDatarow(row));
                }
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return lstMficientServers;
        }
        public MFEmficientServer getDefaultMficientServer()
        {
            MFEmficientServer objMficientServer = new MFEmficientServer();
            try
            {
                string strQuery = getDefaultServerSqlQuery();
                SqlCommand cmd = getCommandToGetDefaultServer(strQuery);
                DataSet dsServerDetail = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsServerDetail == null &&
                    dsServerDetail.Tables[0].Rows.Count == 0)//should get a record
                {
                    throw new Exception(
                    (
                    (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR
                    ).ToString()
                    );
                }
                objMficientServer = getServerFromDatarow(dsServerDetail.Tables[0].Rows[0]);
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
                return objMficientServer;
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return objMficientServer;
        }
        MFEmficientServer getServerFromDatarow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEmficientServer objMficientServer =
                        new MFEmficientServer();
            objMficientServer.ServerId = Convert.ToString(row["SERVER_ID"]);
            objMficientServer.ServerName = Convert.ToString(row["SERVER_NAME"]);
            objMficientServer.ServerIpAddress = Convert.ToString(row["SERVER_IP_ADDRESS"]);
            objMficientServer.ServerURL = Convert.ToString(row["SERVER_URL"]);
            objMficientServer.IsEnabled = Convert.ToBoolean(row["IS_ENABLED"]);
            objMficientServer.IsDefault = Convert.ToBoolean(row["IS_DEFAULT"]);
            return objMficientServer;
        }
        string getAllServersSqlQuery()
        {
            string strQuery = @"SELECT *
                                FROM [ADMIN_TBL_MST_SERVER]
                                WHERE IS_ENABLED = 1";
            return strQuery;
        }
        string getDefaultServerSqlQuery()
        {
            string strQuery = @"SELECT *
                                FROM [ADMIN_TBL_MST_SERVER]
                                WHERE IS_DEFAULT =@IS_DEFAULT
                                AND IS_ENABLED = 1";
            return strQuery;
        }
        SqlCommand getCommandToGetAllServers(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            return cmd;
        }
        SqlCommand getCommandToGetDefaultServer(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@IS_DEFAULT", 1);
            return cmd;
        }
    }
}