﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetResetPasswordLogForUser
    {
        string _userId;
        string _statusDescription;
        int _statusCode;

        #region Public Properties
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }


        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        public string UserId
        {
            get { return _userId; }
            private set { _userId = value; }
        }
        #endregion
        public GetResetPasswordLogForUser(string userId)
        {
            this.UserId = userId;
        }
        public MFEResetPasswordLog getLatestPwdLog()
        {
            MFEResetPasswordLog objPwdLog = new MFEResetPasswordLog();
            try
            {
                SqlCommand cmd = getCommandForLatestPwdLog();
                DataSet dsPwdLog = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsPwdLog == null) throw new Exception();
                if (dsPwdLog.Tables[0].Rows.Count > 0)
                {
                    objPwdLog.UserId = Convert.ToString(dsPwdLog.Tables[0].Rows[0]["USER_ID"]);
                    objPwdLog.PasswordCode = Convert.ToString(dsPwdLog.Tables[0].Rows[0]["NEW_ACCESSCODE"]);
                    objPwdLog.ResetDatetime = Convert.ToInt64(dsPwdLog.Tables[0].Rows[0]["ACCESSCODE_RESET_DATETIME"]);
                    objPwdLog.IsExpired = Convert.ToBoolean(dsPwdLog.Tables[0].Rows[0]["IS_USED"]);
                    objPwdLog.IsUsed = Convert.ToBoolean(dsPwdLog.Tables[0].Rows[0]["IS_EXPIRED"]);
                }
            }
            catch (MficientException ex)
            {
                this.StatusDescription = ex.Message;
                this.StatusCode = -1000;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = String.Empty;
            }
            return objPwdLog;
        }

        string getSqlQueryForAllPwdLog()
        {
            return @"SELECT * FROM ADMIN_TBL_RESET_PASSWORD_LOG
                      WHERE USER_ID = @USER_ID
                       AND IS_USED =0
                        AND IS_EXPIRED =0
                      ORDER BY ACCESSCODE_RESET_DATETIME DESC";
        }
        SqlCommand getCommandForAllPwdLog()
        {
            SqlCommand cmd = new SqlCommand(getSqlQueryForAllPwdLog());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            return cmd;
        }
        string getSqlQueryForLatestPwdLog()
        {
            return @"SELECT TOP 1 * FROM ADMIN_TBL_RESET_PASSWORD_LOG
                    WHERE USER_ID = @USER_ID
                    AND IS_USED =0
                    AND IS_EXPIRED =0
                    ORDER BY ACCESSCODE_RESET_DATETIME DESC";
        }
        SqlCommand getCommandForLatestPwdLog()
        {
            SqlCommand cmd = new SqlCommand(getSqlQueryForLatestPwdLog());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            return cmd;
        }
    }
}