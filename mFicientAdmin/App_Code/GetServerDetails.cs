﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace mFicientAdmin
{
    public class GetServerDetailsAdmin
    {
        public void DeleteServer(string serverid)
        {
            try
            {
                string sql = @"delete from ADMIN_TBL_MST_SERVER where SERVER_ID=@serverid";
                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.CommandType = CommandType.Text;
                objSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;

                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public string GetCompanyId(string ServerId)
        {
                 string CompanyId=string.Empty;
                 try
                 {
                     SqlCommand cmd = new SqlCommand(@" select COMPANY_ID from ADMIN_TBL_SERVER_MAPPING where SERVER_ID=@serverid");
                     cmd.Parameters.AddWithValue("@serverid", ServerId);
                     DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                     if (objDs.Tables[0].Rows.Count > 0)
                     {
                         CompanyId = Convert.ToString(objDs.Tables[0].Rows[0]["COMPANY_ID"]);
                         return CompanyId;
                     }
                     else
                     {
                        return CompanyId = "";
                     }
                 }
                 catch(Exception e)
                 {
                     this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                     this.StatusDescription = e.Message;
                 }
                 return CompanyId;
        }

        public void ServerDetails(string serverid)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select SERVER_ID, SERVER_NAME,SERVER_IP_ADDRESS,SERVER_URL,IS_ENABLED from ADMIN_TBL_MST_SERVER Where SERVER_ID=@serverid ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "No records to display";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public DataSet GetSdetails()
        {
            try
            {
                DataSet objDataSet;
                SqlCommand ObjSqlCmd = new SqlCommand(@"select SERVER_NAME,SERVER_IP_ADDRESS,SERVER_URL,IS_ENABLED from ADMIN_TBL_MST_SERVER Where SERVER_ID=@SERVER_ID ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@SERVER_ID", ServerId);
                objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                return Resultset; 
            }
            catch(Exception e)
            {
                this.StatusCode = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR);
                this.StatusDescription = e.Message;
            }
            return Resultset;
        }

        public void EnquiryDetails(string EnquiryId)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select e.STATUS,e.ENQUIRY_ID, e.COMPANY_NAME,c.COUNTRY_NAME,e.TYPE_OF_COMPANY,
                                                    e.CONTACT_PERSON,e.CONTACT_NUMBER,e.CONTACT_EMAIL,e.ENQUIRY_DATETIME,e.SALES_MNGR_ID,e.SALES_ID,e.RESELLER_ID 
                                                    from ADMIN_TBL_ENQUIRY_DETAIL e  inner join ADMIN_TBL_MST_COUNTRY c on e.COUNTRY_CODE=c.COUNTRY_CODE
                                                    where e.ENQUIRY_ID=@enquiryid ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@enquiryid", EnquiryId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "No records to display";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR);
                this.StatusDescription = e.Message;
            }
        }

        public void OrderDetails(string CompanyId)
        {
            try
            {
                SqlCommand Objsqlcmd = new SqlCommand(@"select d.COMPANY_ID, d.COMPANY_NAME, p.PLAN_NAME,pp.MAX_USER,pp.MAX_WORKFLOW,pp.PURCHASE_DATE + pp.VALIDITY as 'EXPIRY DATE'
                                    from ADMIN_TBL_PLANS p inner join ADMIN_TBL_PLAN_PURCHASE_LOG pp on p.PLAN_CODE=pp.PLAN_CODE inner join ADMIN_TBL_COMPANY_DETAIL d
                                    on pp.COMPANY_ID=d.COMPANY_ID where d.COMPANY_ID=@companyid");
                Objsqlcmd.CommandType = CommandType.Text;
                Objsqlcmd.Parameters.AddWithValue("@companyid", CompanyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(Objsqlcmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "No records to display";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR);
                this.StatusDescription = e.Message;
            }
        }

        public string GetId(string Username, string Password, out string Role)
        {
                Role = "";
                try
                {
                SqlCommand cmd = new SqlCommand(@"select ADMIN_ID,ACCESS_CODE, 'A' as Type from ADMIN_TBL_ADMIN_LOGIN where ADMIN_NAME=@username union
                                             select RESELLER_ID as ADMIN_ID,PASSWORD as ACCESS_CODE,'R' as Type from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@username union
                                            select LOGIN_ID as ADMIN_ID,PASSWORD as ACCESS_CODE,Type from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@username;");
                cmd.Parameters.AddWithValue("@username", Username);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    string password = Utilities.GetMd5Hash(Password).ToLower();
                    if (password == objDataSet.Tables[0].Rows[0]["ACCESS_CODE"].ToString().ToLower())
                    {
                        Role = objDataSet.Tables[0].Rows[0]["Type"].ToString();
                        return objDataSet.Tables[0].Rows[0]["ADMIN_ID"].ToString();
                    }
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return "";
        }
        public DataTable GetList()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select distinct d.COMPANY_ID, d.COMPANY_NAME +'(' + d.COMPANY_ID +')'as 'Company' ,case WHEN 
                      s.SERVER_NAME IS NULL then 'Not Alloted'
                        ELSE
                            s.SERVER_NAME
                        end as 'Server Name'
                    ,case WHEN mb.MBUZZ_SERVER_NAME IS NULL then 'Not Alloted'
                     ELSE
                    MB.MBUZZ_SERVER_NAME end as 'MBuzz Server Name',
                    case when mp.MP_SERVER_NAME  IS NULL then 'Not Alloted'
                    else
                     mp.MP_SERVER_NAME end as 'MPluggin Server'
                     from
                     ADMIN_TBL_COMPANY_DETAIL d left join ADMIN_TBL_SERVER_MAPPING m
                      on d.COMPANY_ID=m.COMPANY_ID left join ADMIN_TBL_MST_SERVER s on m.SERVER_ID=s.SERVER_ID
                    left join ADMIN_TBL_MBUZZ_SERVER mb on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID
                    left join ADMIN_TBL_MST_MP_SERVER mp
                    on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID");
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

         public DataTable ResultTable
        {
            set;
            get;
        }

         public int StatusCode
         {
             set;
             get;
         }
         public string StatusDescription
         {
             set;
             get;
         }

         public DataSet Resultset
         {
             get;
             set;
         }

         public string ServerId
         {
             get;
             set;
         }
        
        }
    }


 