﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetTestConnectionUrl
    {

        public GetTestConnectionUrl(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                string strQuery = @"SELECT ms.SERVER_URL FROM ADMIN_TBL_SERVER_MAPPING sm INNER JOIN ADMIN_TBL_MST_SERVER ms ON 
                                ms.SERVER_ID=sm.SERVER_ID WHERE sm.COMPANY_ID=@COMPANY_ID;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(cmd);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable.Rows.Count > 0)
                    StatusCode = 0;
                else
                    StatusCode = -1001;
                StatusDescription = "";
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }


        public string CompanyId
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public DataTable ResultTable
        {
            get;
            set;
        }
    }
    }
