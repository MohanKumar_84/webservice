﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetUserTypeWithDetailsByUsername
    {
        MFEAdministrator _admin;
        MFEAdminPanelUsers _user;
        MFEReseller _reseller;
        string _userName;
        USER_TYPE _eUserType;
        int _statusCode;
        string _statusDescription;

        #region Public Properties
        public MFEAdministrator Admin
        {
            get { return _admin; }
            private set { _admin = value; }
        }
        public MFEAdminPanelUsers User
        {
            get { return _user; }
            private set { _user = value; }
        }
        public MFEReseller Reseller
        {
            get { return _reseller; }
            private set { _reseller = value; }
        }
        public string UserName
        {
            get { return _userName; }
            private set { _userName = value; }
        }
        public USER_TYPE EUserType
        {
            get { return _eUserType; }
            private set { _eUserType = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }

        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        #endregion
        public GetUserTypeWithDetailsByUsername(string userName)
        {
            this.UserName = userName;
            this.StatusCode = -1000;
            this.StatusDescription = MficientConstants.INTERNAL_ERROR;
        }
        public void Process()
        {
            try
            {
                SqlCommand cmd = getSqlCommandToGetUsrIdAndPwd();
                DataSet dsDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsDtls != null)
                {
                    this.EUserType = getUserTypeFromResponseDS(dsDtls);

                    switch (this.EUserType)
                    {
                        case USER_TYPE.Reseller:
                            fillUserDetailByUSerType(
                                this.EUserType,
                                dsDtls.Tables[1].Rows[0]
                                );
                            break;
                        case USER_TYPE.Admin:
                            fillUserDetailByUSerType(
                                this.EUserType,
                                dsDtls.Tables[0].Rows[0]
                                );
                            break;
                        case USER_TYPE.SalesManager:
                        case USER_TYPE.Account:
                        case USER_TYPE.Support:
                        case USER_TYPE.SalesPerson:
                            fillUserDetailByUSerType(
                                this.EUserType,
                                dsDtls.Tables[2].Rows[0]
                                );
                            break;

                    }
                    this.StatusCode = 0;
                    this.StatusDescription = String.Empty;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }

        string getSqlQueryForUsrIdAndPwd()
        {
            return @"SELECT *, 'A' AS TYPE from ADMIN_TBL_ADMIN_LOGIN where ADMIN_NAME=@USER_NAME;
                     SELECT *,'R' as TYPE from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@USER_NAME ;
                     select * FROM ADMIN_TBL_LOGIN_USER_DETAIL WHERE LOGIN_NAME=@USER_NAME;";
        }
        SqlCommand getSqlCommandToGetUsrIdAndPwd()
        {
            SqlCommand cmd = new SqlCommand(getSqlQueryForUsrIdAndPwd());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_NAME", this.UserName);
            return cmd;
        }

        USER_TYPE getUserTypeFromResponseDS(DataSet dsDtls)
        {
            USER_TYPE eUserType;
            if (dsDtls == null) throw new ArgumentNullException();
            if (dsDtls.Tables[0].Rows.Count > 0) eUserType = USER_TYPE.Admin;
            else if (dsDtls.Tables[1].Rows.Count > 0) eUserType = USER_TYPE.Reseller;
            else if (dsDtls.Tables[2].Rows.Count > 0)
            {
                eUserType =
                             Utilities.getUserTypeFromUserCode(
                             Convert.ToString(dsDtls.Tables[2].Rows[0]["TYPE"])
                             );
            }
            else
            {
                throw new MficientException("Invalid username.No user found.");
            }
            return eUserType;
        }
        void fillUserDetailByUSerType(
            USER_TYPE userType,
            DataRow row)
        {
            switch (userType)
            {
                case USER_TYPE.Reseller:
                    this.Reseller = getResellerFromDataRow(row);
                    break;
                case USER_TYPE.SalesManager:
                case USER_TYPE.Account:
                case USER_TYPE.Support:
                case USER_TYPE.SalesPerson:
                    this.User = getAdminPanelUserFromDataRow(row);
                    break;
                case USER_TYPE.Admin:
                    this.Admin = getAdministratorFromDataRow(row);
                    break;
            }
        }
        MFEAdminPanelUsers getAdminPanelUserFromDataRow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEAdminPanelUsers objUser = new MFEAdminPanelUsers();
            objUser.LoginName = Convert.ToString(row["LOGIN_NAME"]);
            objUser.Password = Convert.ToString(row["PASSWORD"]);
            objUser.Email = Convert.ToString(row["EMAIL"]);
            objUser.IsEnabled = Convert.ToBoolean(row["IS_ENABLED"]);
            objUser.ContactNo = Convert.ToString(row["CONTACT_NUMBER"]);
            objUser.CreatedOn = Convert.ToDateTime(row["CREATED_ON"]);
            objUser.LoginId = Convert.ToString(row["LOGIN_ID"]);
            objUser.ManagerId = Convert.ToString(row["MANAGER_ID"]);
            objUser.CountryCode = Convert.ToString(row["COUNTRY_CODE"]);
            objUser.City = Convert.ToString(row["CITY"]);
            objUser.UserType = Convert.ToString(row["TYPE"]);
            objUser.UserName = Convert.ToString(row["USER_NAME"]);
            objUser.IsServerAllotted = Convert.ToString(row["IS_SERVER_ALLOCATION"]) == "0" ? false : true;
            return objUser;
        }
        MFEReseller getResellerFromDataRow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEReseller objReseller = new MFEReseller();
            objReseller.Name = Convert.ToString(row["RESELLER_NAME"]);
            objReseller.Email = Convert.ToString(row["EMAIL"]);
            objReseller.ContactNo = Convert.ToString(row["CONTACT_NUMBER"]);
            objReseller.IsEnabled = Convert.ToBoolean(row["IS_ENABLE"]);
            objReseller.Discount = Convert.ToDecimal(row["DISCOUNT"]);
            objReseller.CreatedOn = Convert.ToDateTime(row["CREATED_ON"]);
            objReseller.ResellerId = Convert.ToString(row["RESELLER_ID"]);
            objReseller.LoginName = Convert.ToString(row["LOGIN_NAME"]);
            objReseller.Password = Convert.ToString(row["PASSWORD"]);
            objReseller.City = Convert.ToString(row["CITY"]);
            objReseller.Country = Convert.ToString(row["COUNTRY"]);
            objReseller.IsPaymentApproved = Convert.ToBoolean(row["IS_PAYMENT_APPROVAL"]);

            return objReseller;
        }
        MFEAdministrator getAdministratorFromDataRow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEAdministrator objAdmin = new MFEAdministrator();
            objAdmin.AdminName = Convert.ToString(row["ADMIN_NAME"]);
            objAdmin.Password = Convert.ToString(row["ACCESS_CODE"]);
            objAdmin.Email = Convert.ToString(row["EMAIL"]);
            objAdmin.AdminId = Convert.ToString(row["ADMIN_ID"]);
            return objAdmin;
        }
    }
}