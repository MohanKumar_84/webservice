﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetUsersOfCompany
    {
        int _statusCode;
        string _companyId, _statusDescription;
        List<MFEMobileUser> _userList;
        
        
        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        

        public List<MFEMobileUser> UserList
        {
            get { return _userList; }
            set { _userList = value; }
        }

        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }



        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }
        #endregion
        public GetUsersOfCompany(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                SqlCommand cmd = this.sqlCmdToGetAllUsers();
                DataSet dsUserList= Utilities.SelectDataFromSQlCommand(cmd);
                if (dsUserList == null) throw new Exception();
                this.UserList = new List<MFEMobileUser>();
                if (dsUserList.Tables[0].Rows.Count > 0)
                {
                   this.UserList= getUserListFromDtble(dsUserList.Tables[0]);
                }
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        string sqlQueryToGetAllUsers()
        {
            return @"SELECT * FROM TBL_USER_DETAIL
                    WHERE COMPANY_ID = @COMPANY_ID";
        }
        SqlCommand sqlCmdToGetAllUsers()
        {
            SqlCommand cmd = new SqlCommand(sqlQueryToGetAllUsers());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }
        List<MFEMobileUser> getUserListFromDtble(DataTable userListDtbl)
        {
            if (userListDtbl == null) throw new ArgumentNullException();
            List<MFEMobileUser> lstUsers = new List<MFEMobileUser>();
            foreach (DataRow row in userListDtbl.Rows)
            {
                lstUsers.Add(getUserFromDRow(row));
            }
            return lstUsers;
        }
        MFEMobileUser getUserFromDRow(DataRow userDtlRow)
        {
            if (userDtlRow == null) throw new ArgumentNullException();
            MFEMobileUser objMobileUser = new
            MFEMobileUser();
            objMobileUser.UserId = Convert.ToString(userDtlRow["USER_ID"]);
            objMobileUser.EmailId = Convert.ToString(userDtlRow["EMAIL_ID"]);
            objMobileUser.Password = Convert.ToString(userDtlRow["ACCESS_CODE"]);
            objMobileUser.FirstName = Convert.ToString(userDtlRow["FIRST_NAME"]);
            objMobileUser.LastName = Convert.ToString(userDtlRow["LAST_NAME"]);
            objMobileUser.Mobile = Convert.ToString(userDtlRow["MOBILE"]);
            objMobileUser.CompanyId= Convert.ToString(userDtlRow["COMPANY_ID"]);
            objMobileUser.Dob= Convert.ToInt64(userDtlRow["DATE_OF_BIRTH"]);
            objMobileUser.RegistrationDatetime = Convert.ToInt64(userDtlRow["REGISTRATION_DATETIME"]);
            objMobileUser.SubAdminId = Convert.ToString(userDtlRow["SUBADMIN_ID"]);
            objMobileUser.LocationId = Convert.ToString(userDtlRow["LOCATION_ID"]);
            objMobileUser.DesignationId= Convert.ToString(userDtlRow["DESIGNATION_ID"]);
            objMobileUser.EmployeeNo = Convert.ToString(userDtlRow["EMPLOYEE_NO"]);
            objMobileUser.Username = Convert.ToString(userDtlRow["USER_NAME"]);
            objMobileUser.IsActive= Convert.ToBoolean(userDtlRow["IS_ACTIVE"]);
            objMobileUser.RegionId = Convert.ToString(userDtlRow["REGION_ID"]);
            objMobileUser.IsBlocked = Convert.ToBoolean(userDtlRow["IS_BLOCKED"]);
            objMobileUser.AllowMessenger = Convert.ToBoolean(userDtlRow["ALLOW_MESSENGER"]);
            objMobileUser.IsOfflineWorkAllowed = Convert.ToBoolean(userDtlRow["IS_OFFLINE_WORK"]);
            objMobileUser.UpdatedOn = Convert.ToInt64(userDtlRow["UPDATE_ON"]);
            objMobileUser.DesktopMessenger = Convert.ToBoolean(userDtlRow["DESKTOP_MESSENGER"]);
            objMobileUser.RequestedBy = Convert.ToString(userDtlRow["REQUESTED_BY"]);
            objMobileUser.DomainId = Convert.ToString(userDtlRow["DOMAIN_ID"]);

            return objMobileUser;
        }
    }
}