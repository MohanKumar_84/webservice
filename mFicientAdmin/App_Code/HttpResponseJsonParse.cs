﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientAdmin
{
    public class HttpResponseJsonParse
    {
        string _json, _functionCode, _requestId, _respCode, _respDesc;
        ResponseStatus _respStatus;

        public string RespDesc
        {
            get { return _respDesc; }
        }
        public string RespCode
        {
            get { return _respCode; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string FunctionCode
        {
            get { return _functionCode; }
        }
        public ResponseStatus RespStatus
        {
            get { return _respStatus; }
        }
        public string Json
        {
            get { return _json; }
        }

        public HttpResponseJsonParse(string json)
        {
            _json = json;
            AdminWebServiceCommonResponse objResponse = JsonHelper.DeserialiseJson<AdminWebServiceCommonResponse>(json);
            _functionCode = objResponse.resp.func;
            _requestId = objResponse.resp.rid;
            _respCode = objResponse.resp.status.cd;
            _respDesc = objResponse.resp.status.desc;
            _respStatus = objResponse.resp.status;
        }
    }

    public class AdminWebServiceCommonResponse
    {
        /// <summary>
        /// resp
        /// </summary>
        [DataMember]
        public WSCommonResponse resp { get; set; }
    }

    public class WSCommonResponse
    {
        /// <summary>
        /// function code
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// Request Id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [DataMember]
        public ResponseStatus status { get; set; }
    }

    
}