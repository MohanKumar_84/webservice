﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient; 

namespace mFicientAdmin
{
    public class Login
    {
        private string strSessionId, strRole;
        private SqlConnection objSqlConnection = null;
        public Login(string _UserName, string _Password, string _DeviceTokenTypeCode, string _DeviceToken, string _HostName, string _IpAddress)
        {
            this.UserName = _UserName;
            this.Password = _Password;
            this.DeviceTokenTypeCode = _DeviceTokenTypeCode;
            this.DeviceToken = _DeviceToken;
            this.HostName = _HostName;
            this.IpAddress = _IpAddress;
        }

        public string IsValidLogin(string username, string password)
        {
            try
            {
                //changed on 16-7-2012 Mohan
                //password field was converted to MD5 Hash
                string strQuery = @"SELECT RESELLER_ID  FROM ADMIN_TBL_RESELLER_DETAIL WHERE LOGIN_NAME=@Username AND PASSWORD=@UPassword ";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.Parameters.AddWithValue("@Username", username);
                objSqlCommand.Parameters.AddWithValue("@UPassword", Utilities.GetMd5Hash(password));
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    strRole = "R";
                    return objDataSet.Tables[0].Rows[0][0].ToString();
                }

                else
                {
                    string Query = @"select LOGIN_ID,TYPE  from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@Username and PASSWORD=@UPassword ";
                    SqlCommand SqlCommand = new SqlCommand(Query);
                    SqlCommand.Parameters.AddWithValue("@Username", username);
                    SqlCommand.Parameters.AddWithValue("@UPassword", Utilities.GetMd5Hash(password));
                    SqlCommand.CommandType = CommandType.Text;
                    DataSet ResultSet = Utilities.SelectDataFromSQlCommand(SqlCommand);
                    if (ResultSet.Tables[0].Rows.Count > 0)
                    {
                        strRole = ResultSet.Tables[0].Rows[0]["TYPE"].ToString();
                        return ResultSet.Tables[0].Rows[0][0].ToString();
                    }
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return "";
        }

        public void LoginCheck()
        {
            string strDeviceDetail = "", strSId = "", strIP = "";
            Boolean blnIsAlreadyLogin = false;
            this.StatusCode = 0;
            string userId = IsValidLogin(UserName, Password);
            if (userId !="")
            {
                blnIsAlreadyLogin = Utilities.IsUserLoggedIn(userId, this.DeviceTokenTypeCode, this.DeviceToken, out strSId, out strDeviceDetail, out strIP);
                if (strSId == "")
                {
                    if (blnIsAlreadyLogin)
                    {
                        this.StatusCode = (int)USERLOGIN_ERRORS.USER_ALREADY_LOGGED_ERROR;
                        SaveLoginHistory(0, userId, blnIsAlreadyLogin, out strSId);
                    }
                    else
                    {
                        SaveLoginHistory(1, userId, blnIsAlreadyLogin, out strSId);
                    }
                }
                else
                {
                    if (strIP == this.IpAddress)
                    {
                        SqlCommand objSqlCommand = new SqlCommand(@"update ADMIN_TBL_LOGIN_USER_SESSION set SESSION_ACTIVITY_DATETIME=@SESSION_ACTIVITY_DATETIME where SESSION_HOST_IP=@SESSION_HOST_IP and
                                USER_ID=@USER_ID and SESSION_ID=@SESSION_ID;");
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@USER_ID", userId);
                        objSqlCommand.Parameters.AddWithValue("@SESSION_ID", strSId);
                        objSqlCommand.Parameters.AddWithValue("@SESSION_HOST_IP", this.IpAddress);
                        objSqlCommand.Parameters.AddWithValue("@SESSION_ACTIVITY_DATETIME", DateTime.Now.Ticks);
                        int Records = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    }
                    else
                    {
                        this.StatusCode = (int)USERLOGIN_ERRORS.USER_ALREADY_LOGGED_ERROR;
                        this.StatusDescription = "User already logged.";
                    }
                }
            }
            else
            {
                this.StatusCode = (int)USERLOGIN_ERRORS.EMAIL_OR_PASSWORD_ERROR;
                this.StatusDescription = "Incorrect Username or password.";
            }
            strSessionId = strSId;
        }

        public void Process()
        {
            string strDeviceDetail = "", strSId = "", strIP = "";
            Boolean blnIsAlreadyLogin = false;
            this.StatusCode = 0;
            string userId = Utilities.IsValid(UserName, Password);
            if (userId != "")
            {
                blnIsAlreadyLogin = Utilities.IsUserLoggedIn(userId, this.DeviceTokenTypeCode, this.DeviceToken, out strSId, out strDeviceDetail, out strIP);
                if (strSId == "")
                {
                    if (blnIsAlreadyLogin)
                    {
                        this.StatusCode = (int)USERLOGIN_ERRORS.USER_ALREADY_LOGGED_ERROR;
                        SaveLoginHistory(0, userId, blnIsAlreadyLogin, out strSId);
                    }
                    else
                    {
                        SaveLoginHistory(1, userId, blnIsAlreadyLogin, out strSId);
                    }
                }
                else
                {
                    if (strIP == this.IpAddress)
                    {
                        SqlCommand objSqlCommand = new SqlCommand(@"update ADMIN_TBL_LOGIN_USER_SESSION set SESSION_ACTIVITY_DATETIME=@SESSION_ACTIVITY_DATETIME where SESSION_HOST_IP=@SESSION_HOST_IP and
                                USER_ID=@USER_ID and SESSION_ID=@SESSION_ID;");
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@USER_ID", userId);
                        objSqlCommand.Parameters.AddWithValue("@SESSION_ID", strSId);
                        objSqlCommand.Parameters.AddWithValue("@SESSION_HOST_IP", this.IpAddress);
                        objSqlCommand.Parameters.AddWithValue("@SESSION_ACTIVITY_DATETIME", DateTime.Now.Ticks);
                        int Records = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    }
                    else
                    {
                        this.StatusCode = (int)USERLOGIN_ERRORS.USER_ALREADY_LOGGED_ERROR;
                        this.StatusDescription = "User already logged.";
                    }
                }
            }
            else
            {
                this.StatusCode = (int)USERLOGIN_ERRORS.EMAIL_OR_PASSWORD_ERROR;
                this.StatusDescription = "Incorrect Email or password.";
            }
            strSessionId = strSId;
        }

        /// <summary>

        /// Description :Save login history 
        /// Created By: Rahul Chaturvedi
        /// Created On :23/01/2011
        /// </summary>
        private void SaveLoginHistory(int _LoginSuccess, string _UserId, Boolean _IsAlreadyLogin, out string _SessionId)
        {
            _SessionId = "";
            Utilities.SqlConnectionOpen(out objSqlConnection);
            SqlTransaction objSqlTransaction = null;
            try
            {

                string strQuery = @"INSERT INTO ADMIN_TBL_LOGIN_USER_HISTORY(USER_ID,LOGOUT_TYPE,LOGIN_DATETIME,LOGOUT_DATETIME,LOGIN_DEVICE_TOKEN,LOGIN_DEVICE_TYPE,LOGIN_SUCCESS,LOGIN_HOST_NAME,LOGIN_HOST_IP,LOGIN_DEVICE_TYPE_CODE)
                                VALUES(@USER_ID,@LOGOUT_TYPE,@LOGIN_DATETIME,@LOGOUT_DATETIME,@DEVICE_TOKEN,@LOGIN_DEVICE_TYPE,@LOGIN_SUCCESS,@LOGIN_HOST_NAME,@LOGIN_HOST_IP,@DEVICE_TYPE_CODE);";

                objSqlTransaction = objSqlConnection.BeginTransaction();
                SqlCommand objSqlCommand = new SqlCommand(strQuery, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", _UserId);
                objSqlCommand.Parameters.AddWithValue("@LOGIN_DATETIME", DateTime.Now.Ticks);
                objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", 0);
                objSqlCommand.Parameters.AddWithValue("@LOGIN_DEVICE_TYPE", "Normal");
                objSqlCommand.Parameters.AddWithValue("@LOGOUT_TYPE", 0);
                objSqlCommand.Parameters.AddWithValue("@LOGIN_SUCCESS", Convert.ToBoolean(_LoginSuccess));
                objSqlCommand.Parameters.AddWithValue("@LOGIN_HOST_NAME", this.HostName);
                objSqlCommand.Parameters.AddWithValue("@LOGIN_HOST_IP", this.IpAddress);
                objSqlCommand.Parameters.AddWithValue("@DEVICE_TOKEN", this.DeviceToken);
                objSqlCommand.Parameters.AddWithValue("@DEVICE_TYPE_CODE", this.DeviceTokenTypeCode);

                int intExecuteCount = objSqlCommand.ExecuteNonQuery();

                if (_LoginSuccess == 1 && !_IsAlreadyLogin && intExecuteCount > 0)
                {
                    _SessionId = GetSessionId(_UserId);
                    strQuery = @"INSERT INTO ADMIN_TBL_LOGIN_USER_SESSION(USER_ID,SESSION_START_DATETIME,SESSION_ACTIVITY_DATETIME,SESSION_HOST_IP,SESSION_ID,LOGIN_DEVICE_TYPE_CODE,LOGIN_DEVICE_TOKEN)
                                                       VALUES(@USER_ID,@SESSION_START_DATETIME,@SESSION_ACTIVITY_DATETIME,@SESSION_HOST_IP,@SESSION_ID,@LOGIN_DEVICE_TYPE_CODE,@LOGIN_DEVICE_TOKEN);";
                    objSqlCommand = new SqlCommand(strQuery, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@USER_ID", _UserId);
                    objSqlCommand.Parameters.AddWithValue("@SESSION_START_DATETIME", DateTime.Now.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@SESSION_ACTIVITY_DATETIME", DateTime.Now.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@SESSION_HOST_IP", this.IpAddress);
                    objSqlCommand.Parameters.AddWithValue("@SESSION_ID", _SessionId);
                    objSqlCommand.Parameters.AddWithValue("@LOGIN_DEVICE_TYPE_CODE", this.DeviceTokenTypeCode);
                    objSqlCommand.Parameters.AddWithValue("@LOGIN_DEVICE_TOKEN", this.DeviceToken);

                    objSqlCommand.ExecuteNonQuery();
                }

                objSqlTransaction.Commit();
            }
            catch //(Exception ex)
            {
                objSqlTransaction.Rollback();
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }

            finally
            {
                Utilities.SqlConnectionClose(objSqlConnection);
            }

        }


        /// Description :Genrate session id 
        /// Created By: Rahul Chaturvedi
        /// Created On :23/01/2011

        /// </summary>
        public string GetSessionId(string _UserId)
        {
            string strSessionIdPreFix = "1";
            string strHashCode = Utilities.GetMd5Hash(this.UserName + _UserId + DateTime.Now.Ticks.ToString());

            if (this.DeviceToken != string.Empty)
            {
                strSessionIdPreFix = "2";
            }

            int index = 0;
            Boolean blnIsSessionIdValid = false;
            string strNewSessioId = string.Empty;
            do
            {
                strNewSessioId = strSessionIdPreFix + strHashCode.Substring(index, 15);
                blnIsSessionIdValid = IsSessionIdValid(strNewSessioId);
                index = index + 1;

            }
            while (blnIsSessionIdValid == false);
           
            return strNewSessioId;
        }

        /// <summary>

        /// Description :Check session validation
        /// Created By: Rahul Chaturvedi
        /// Created On :23/01/2011

        /// </summary>
        private Boolean IsSessionIdValid(string _SessionId)
        {
            Boolean blnIsSessionIdValid = true;
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"SELECT USER_ID FROM ADMIN_TBL_LOGIN_USER_SESSION WHERE SESSION_ID=@SESSION_ID;");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SESSION_ID", _SessionId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                    blnIsSessionIdValid = false;
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
            }
            return blnIsSessionIdValid;
        }

        //public Boolean IsAdmin
        //{
        //    get;
        //    set;
        //}
        public string AdminId
        {
            get;
            set;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            set;
        }
        public string DeviceTokenTypeCode
        {
            get;
            set;
        }
        public string DeviceToken
        {
            get;
            set;
        }
        public string HostName
        {
            get;
            set;
        }
        public string IpAddress
        {
            get;
            set;
        }
        public string SessionId
        {
            get
            {
                return strSessionId;
            }
        }
        public string Role
        {
            get
            {
                return strRole;
            }
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public int intStatusCode
        {
            get;
            set;
        }

        public string strStatusDescription
        {
            get;
            set;
        }
    }
    }
