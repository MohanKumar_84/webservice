﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class ManagePlanAdmin
    {
        public void ExecuteInsert(string plancode, string planname, int maxworkflow, decimal userchargeinr, decimal userchargeusd ,int msgperday,int msgpermonth)
        {
            try
            {
                string sql = @"INSERT INTO ADMIN_TBL_PLANS (PLAN_CODE, PLAN_NAME,MAX_WORKFLOW, USER_CHARGE_INR_PM,USER_CHARGE_USD_PM,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH) VALUES 
                            (@plancode,@planname,@maxworkflow,@userchargeinr,@userchargeusd,@msgperday,@msgpermonth)";

                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.Parameters.AddWithValue("@plancode", plancode);
                objSqlCmd.Parameters.AddWithValue("@planname", planname);
                objSqlCmd.Parameters.AddWithValue("@maxworkflow", maxworkflow);
                objSqlCmd.Parameters.AddWithValue("@userchargeinr", userchargeinr);
                objSqlCmd.Parameters.AddWithValue("@userchargeusd", userchargeusd);
                objSqlCmd.Parameters.AddWithValue("@msgperday", msgperday);
                objSqlCmd.Parameters.AddWithValue("@msgpermonth", msgpermonth);
                objSqlCmd.CommandType = CommandType.Text;
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    this.StatusDescription = "Record insert error";

                }
            }
            catch
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = "record insert error";
            }
        }

        public string plancode
        {
            get;
            set;

        }

        public string planname
        {
            get;
            set;
        }

        public int maxworkflow
        {
            get;
            set;
        }

        public decimal userchargeinr
        {
            get;
            set;
        }

        public decimal userchargeusd
        {
            get;
            set;
        }

        public int msgperday
        {
            get;
            set;
        }

        public int msgpermonth
        {
            get;
            set;
        }


        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
      

    }
}