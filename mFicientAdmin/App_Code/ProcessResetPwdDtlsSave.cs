﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class ProcessResetPwdDtlsSave
    {
        public ProcessResetPwdDtlsSave(
            USER_TYPE userType,
            string userId,
            string emailId,
            string loginName,
            HttpContext context)
        {
            this.EUserType = userType;
            this.UserId = userId;
            this.Email = emailId;
            this.LoginName = loginName;
            this.Context = context;
        }
        int intStatusCode;
        public void Process()
        {
            try
            {
                int iRecordEffected = -1;
                string strNewResetCode = getNewResetCode();
                SqlCommand cmd = getSqlCommand(strNewResetCode);
                iRecordEffected = MSSqlClient.ExecuteNonQueryRecord(cmd);
                if (iRecordEffected <= 0) throw new Exception();
                else
                {
                    SaveEmailInfo.resetPassword(
                        this.LoginName,
                        this.Email,
                        Utilities.getUserTypeCodeFromUserType(this.EUserType),
                        this.UserId,
                        strNewResetCode,
                        this.Context);
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = MficientConstants.INTERNAL_ERROR;
            }
        }

        string getSqlQuery()
        {
            return @"INSERT INTO ADMIN_TBL_RESET_PASSWORD_LOG(USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED) 
                        VALUES(@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,@IS_USED,@IS_EXPIRED);";
        }
        SqlCommand getSqlCommand(string resetCode)
        {
            SqlCommand cmd = new
            SqlCommand(getSqlQuery());

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            cmd.Parameters.AddWithValue("@ACCESSCODE",Utilities.GetMd5Hash(resetCode));
            cmd.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@IS_USED", 0);
            cmd.Parameters.AddWithValue("@IS_EXPIRED", 0);

            return cmd;
        }
        //        public void ProcessForMobileUser()
        //        {
        //            try
        //            {
        //                int intExecuteRecordsCount = -1;
        //                string NewPassword = GetPassword(UserName);
        //                SqlCommand objSqlCommand = new SqlCommand(@"SELECT * FROM ADMIN_TBL_USER_DETAIL
        //                                                        WHERE USER_NAME = @UserName
        //                                                        AND SUBADMIN_ID = @SubAdminId");
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@UserName", this.UserName);
        //                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
        //                DataTable dt = Utilities.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
        //                if (dt.Rows.Count > 0)
        //                {
        //                    objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_RESET_PASSWORD_LOG(USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED) 
        //                        VALUES(@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,0);");
        //                    objSqlCommand.CommandType = CommandType.Text;
        //                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", NewPassword);
        //                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["USER_ID"].ToString());
        //                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);
        //                    intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
        //                    if (intExecuteRecordsCount > 0)
        //                    {
        //                        Utilities.SendInfoEmail(dt.Rows[0]["EMAIL_ID"].ToString(), "You forget your password so to access your account please use " + NewPassword + " . Please ", "Forget password of mFicient");
        //                        intStatusCode = 0;
        //                    }
        //                    else
        //                    {
        //                        intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
        //                    }
        //                }
        //                else
        //                {
        //                    intStatusCode = 1000001;
        //                }
        //            }
        //            catch 
        //            {
        //                intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
        //            }
        //        }


        private string getNewResetCode()
        {
            string strGuid, strResetCode;
            do
            {
                Guid objGuid = new Guid();
                objGuid = Guid.NewGuid();
                strGuid = Convert.ToString(objGuid);
                string[] stringArray = { "0", "0", "O", "o", "I", "L", "1", "l", "s", "S", "5", "9", "Q", "q", "-" };
                foreach (string strItem in stringArray)
                {
                    strGuid = strGuid.Replace(strItem, "");
                }
            }
            while (strGuid.Length < 12);
            strResetCode = strGuid.Substring(0, 6);
            return strResetCode;
        }

        public int StatusCode
        {
            get
            {
                return intStatusCode;
            }
            private set
            {
                intStatusCode = value;
            }
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public string LoginName
        {
            get;
            set;
        }
        public string SubAdminId
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            private set;
        }
        public USER_TYPE EUserType
        {
            get;
            private set;
        }
        public string Email
        {
            get;
            private set;
        }
        public HttpContext Context
        {
            get;
            private set;
        }
    }
}