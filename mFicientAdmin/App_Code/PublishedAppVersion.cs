﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdmin
{
    public class PublishedAppVersion
    {
        public PublishedAppVersion()
        {
           
        }

        public DataTable Process()
        {
            try
            {
                string strQuery = "";
                this.StatusCode = -1000;

                strQuery = @"select PHONE_DEVICE_TYPE_CODE AS OS,MOBILE_APP_VERSION_MAJOR,MOBILE_APP_VERSION_MINOR,MOBILE_APP_VERSION_REV,
                          MINIMUM_APP_VERSION_MAJOR1,MINIMUM_APP_VERSION_MINOR1,MINIMUM_APP_VERSION_REV1 from dbo.ADMIN_TBL_MST_MOBILE_DEVICE;";
                SqlCommand cmd = new SqlCommand(strQuery);
                DataSet dsPublishedversionInfo = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPublishedversionInfo == null) throw new Exception("Internal server error.");
                if (dsPublishedversionInfo.Tables.Count > 0 &&
                    dsPublishedversionInfo.Tables[0].Rows.Count > 0)
                {
                    this.PublishedVersion = dsPublishedversionInfo.Tables[0];
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
                
            }
            catch (Exception e)
            {
                this.StatusCode = -1000;
                this.StatusDescription = e.Message;
            }
            return PublishedVersion;
        }


        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public DataTable PublishedVersion
        {
            set;
            get;
        }
       
    }
}