﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class PurchaseRequestApprovalBySales
    {
        /// <summary>
        /// for approval of company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="planCode"></param>
        /// <param name="transactionType"></param>
        /// <param name="isTrial"></param>
        /// <param name="isCompanyRegistration">pass true</param>
        public PurchaseRequestApprovalBySales(string companyId, string planCode, string transactionType, bool isTrial, bool isCompanyRegistration)
        {
            this.CompanyId = companyId;
            this.PlanCode = planCode;
            this.TransactionType = transactionType;
            this.IsTrial = isTrial;
            this.IsCompanyRegistration = isCompanyRegistration;
        }
        /// <summary>
        /// for approving plan (purchase,upgrade,downgrade,renew)
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="planCode"></param>
        /// <param name="transactionType"></param>
        /// <param name="isCompanyRegistration">pass false</param>
        public PurchaseRequestApprovalBySales(string companyId, string planCode, string transactionType, bool isCompanyRegistration)
        {
            this.CompanyId = companyId;
            this.PlanCode = planCode;
            this.TransactionType = transactionType;
            this.IsCompanyRegistration = isCompanyRegistration;
        }
        public void Process()
        {

            try
            {

                if (!IsCompanyRegistration)
                {
                    updatePlanRequest();
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    SqlTransaction transaction = null;
                    SqlConnection con;
                    Utilities.SqlConnectionOpen(out con);
                    try
                    {
                        using (con)
                        {
                            transaction = con.BeginTransaction();

                            if (!IsTrial)
                            {
                                //approvePurchaseRequest(con, transaction);should be done at accounts
                                updateSMApproved(con, transaction, DateTime.UtcNow.Ticks);
                            }
                            else
                            {
                                long lngApprovedDate = DateTime.UtcNow.Ticks;
                                updateSMApproved(con, transaction, lngApprovedDate);
                                transferCompanyAdministratorData(con, transaction, lngApprovedDate);
                                transferCompanyCurrentPlanData(con, transaction);
                                transferCompanyDetailData(con, transaction);
                                saveDefualtCompanyAccSettings(transaction, con);
                            }

                            //commit
                            transaction.Commit();
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    StatusCode = 0;
                    StatusDescription = "";
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal error";
            }
        }
//        void approvePurchaseRequest(SqlConnection con, SqlTransaction transaction)
//        {
//            try
//            {
//                string sqlQuery = @"UPDATE ADMIN_TBL_PLAN_PURCHASE_LOG
//                            SET APPROVED = 1
//                            ,APPROVED_DATE = @ApprovedDate
//                            WHERE COMPANY_ID = @CompanyId
//                            AND PLAN_CODE = @PlanCode
//                            AND TRANSACTION_TYPE =@TransactionType";
//                SqlCommand cmd = new SqlCommand(sqlQuery, con, transaction);
//                cmd.Parameters.AddWithValue("@ApprovedDate", DateTime.UtcNow.Ticks);
//                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
//                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
//                cmd.Parameters.AddWithValue("@TransactionType", this.TransactionType);
//                //Utilities.ExecuteNonQueryRecord(cmd);
//                cmd.ExecuteNonQuery();
//            }
//            catch (Exception e)
//            {
//                throw e;
//            }
//        }
        void saveDefualtCompanyAccSettings(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"INSERT INTO [TBL_ACCOUNT_SETTINGS]
                                           ([COMPANY_ID]
                                           ,[MAX_DEVICE_PER_USER]
                                           ,[MAX_MPLUGIN_AGENTS]
                                           ,[ALLOW_DELETE_COMMAND]
                                           ,[ALLOW_ACTIVE_DIRECTORY_USER])
                                     VALUES
                                           (@COMPANY_ID
                                           ,@MAX_DEVICE_PER_USER
                                           ,@MAX_MPLUGIN_AGENTS
                                           ,@ALLOW_DELETE_COMMAND
                                           ,@ALLOW_ACTIVE_DIRECTORY_USER)";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@MAX_DEVICE_PER_USER", 1);
            cmd.Parameters.AddWithValue("@MAX_MPLUGIN_AGENTS", 1);
            cmd.Parameters.AddWithValue("@ALLOW_DELETE_COMMAND", 0);
            cmd.Parameters.AddWithValue("@ALLOW_ACTIVE_DIRECTORY_USER", 0);
            cmd.ExecuteNonQuery();
            #region Previous Code
//            try
//            {
//                string strQuery = @"INSERT INTO[TBL_ACCOUNT_SETTINGS]
//                                       ([COMPANY_ID]
//                                       ,[MAX_DEVICE_PER_USER]
//                                       ,[MPLUGIN_PASSWORD])
//                                 VALUES
//                                       (@COMPANY_ID
//                                       ,@MAX_DEVICE_PER_USER
//                                       ,@MPLUGIN_PASSWORD)";

//                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//                cmd.CommandType = CommandType.Text;
//                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                cmd.Parameters.AddWithValue("@MAX_DEVICE_PER_USER", 1);
//                cmd.Parameters.AddWithValue("@MPLUGIN_PASSWORD", "123456");
//                cmd.ExecuteNonQuery();
//            }

//            catch (Exception ex)
//            {
//                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    throw ex;
//                }
//                // throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
//            }
            #endregion
        }


        void transferCompanyDetailData(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_COMPANY_ADMINISTRATOR
                                SELECT @CompanyId,[ADMIN_ID],[EMAIL_ID],[ACCESS_CODE],[FIRST_NAME]
                                      ,[LAST_NAME],[STREET_ADDRESS1],[STREET_ADDRESS2]
                                      ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
                                      ,[ZIP],[DATE_OF_BIRTH] ,[REGISTRATION_DATETIME]
                                      ,[GENDER],[MIDDLE_NAME],[STREET_ADDRESS3]
                                      ,[MOBILE],[IS_TRIAL]
                                 FROM ADMIN_TBL_COMPANY_ADMINISTRATOR
                                WHERE ADMIN_ID =(SELECT CmpAdmin.ADMIN_ID FROM ADMIN_TBL_COMPANY_ADMINISTRATOR CmpAdmin
		                                INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
		                                ON CmpAdmin.ADMIN_ID = CmpDtl.ADMIN_ID
		                                WHERE CmpDtl.COMPANY_ID = @CompanyId)";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                //Utilities.ExecuteNonQueryRecord(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void transferCompanyAdministratorData(SqlConnection con, SqlTransaction transaction, long updatedOn)
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_COMPANY_DETAIL([COMPANY_ID]
                                   ,[COMPANY_NAME],[REGISTRATION_NO]
                                   ,[STREET_ADDRESS1],[STREET_ADDRESS2]
                                   ,[STREET_ADDRESS3],[CITY_NAME]
                                   ,[STATE_NAME],[COUNTRY_CODE]
                                   ,[ZIP],[ADMIN_ID]
                                   ,[LOGO_IMAGE_NAME],[SUPPORT_EMAIL]
                                   ,[SUPPORT_CONTACT],[UPDATED_ON]
                                   ,[TIMEZONE_ID],[IS_BLOCKED]
                                    ,[BLOCK_UNBLOCK_ID])
                                    SELECT [COMPANY_ID],[COMPANY_NAME],[REGISTRATION_NO]
                                          ,[STREET_ADDRESS1],[STREET_ADDRESS2],[STREET_ADDRESS3]
                                          ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
                                          ,[ZIP],[ADMIN_ID],[LOGO_IMAGE_NAME]
                                          ,[SUPPORT_EMAIL],[SUPPORT_CONTACT]
                                          ,@UpdatedOn,[TIMEZONE_ID],[IS_BLOCKED],[BLOCK_UNBLOCK_ID]
                                     FROM ADMIN_TBL_COMPANY_DETAIL
                                    WHERE COMPANY_ID = @CompanyId";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@UpdatedOn", updatedOn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void transferCompanyCurrentPlanData(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_COMPANY_CURRENT_PLAN
                                    SELECT * FROM ADMIN_TBL_COMPANY_CURRENT_PLAN
                                    WHERE COMPANY_ID = @CompanyId";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void updateSMApproved(SqlConnection con, SqlTransaction transaction, long approvedDate)
        {
            try
            {
                string strQuery = @"UPDATE ADMIN_TBL_COMPANY_DETAIL
                                    SET SM_APPROVED = 1
                                    ,SM_APPROVED_DATE = @ApprovedDate
                                    WHERE COMPANY_ID = @CompanyId";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@ApprovedDate", approvedDate);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void updatePlanRequest()
        {
            //4 cancelled, 3 accounts approved.There are records with these codes.That is the process is complete for this record
            //we have to update only those request which is active,that is the records which are reseller approved
            //see PLAN_REQUEST_STATUS enum
            try
            {
                string strQuery = @"UPDATE ADMIN_TBL_PLAN_ORDER_REQUEST
                                SET STATUS = @SalesApproved
                                WHERE COMPANY_ID = @CompanyId
                                AND PLAN_CODE = @PlanCode
                                AND REQUEST_TYPE = @RequestType
                                AND STATUS != 4 AND STATUS !=3
                                AND STATUS =@ResellerApproved ";//last added on 18/10/2012
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@RequestType", this.TransactionType);
                cmd.Parameters.AddWithValue("@ResellerApproved", (int)PLAN_REQUEST_STATUS.RESELLER_APPROVED);
                cmd.Parameters.AddWithValue("@SalesApproved", (int)PLAN_REQUEST_STATUS.SALES_APPROVED);
                Utilities.ExecuteNonQueryRecord(cmd);
            }
            catch (Exception e)
            {
                throw e;
            }
            
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string PlanCode
        {
            get;
            set;
        }
        public string TransactionType
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public bool IsTrial
        {
            get;
            set;
        }
        public bool IsCompanyRegistration
        {
            get;
            set;
        }
    }
}