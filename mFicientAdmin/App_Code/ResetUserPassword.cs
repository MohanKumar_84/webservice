﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class ResetUserPassword
    {
         public ResetUserPassword()
        {}

         public ResetUserPassword(string userName,string subAdminId)
         {
             this.UserName=userName;
             this.SubAdminId=SubAdminId;
         }

      
        int intStatusCode;
        public void Process()
        {
            try
            {
                int intExecuteRecordsCount = -1;

                string NewPassword = GetPassword(UserName);

                SqlCommand objSqlCommand = new SqlCommand(@"select userID,EMAIL  from (select a.SUBADMIN_ID as userID,a.USER_NAME,b.COMPANY_ID,a.EMAIL,a.ADMIN_ID from ADMIN_TBL_SUB_ADMIN a inner join ADMIN_TBL_COMPANY_DETAIL b on a.ADMIN_ID=b.ADMIN_ID union
            select a.ADMIN_ID as userID,a.EMAIL_ID as USER_NAME,b.COMPANY_ID,a.EMAIL_ID,a.ADMIN_ID  from ADMIN_TBL_COMPANY_ADMINISTRATOR a inner join ADMIN_TBL_COMPANY_DETAIL b on a.ADMIN_ID=b.ADMIN_ID ) a where USER_NAME=@USER_NAME and a.COMPANY_ID=@COMPANY_ID;");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", UserName);

                DataTable dt = Utilities.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_USERS_RESET_PASSWORD_LOG(USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED) 
                        VALUES(@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,0);");

                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", NewPassword);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["userID"].ToString());
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.Now.Ticks);

                    intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        Utilities.SendInfoEmail(dt.Rows[0]["EMAIL"].ToString(), "You forget your password so to access your account please use " + NewPassword + " . Please ", "Forget password of mFicient");
                        intStatusCode = 0;
                    }
                    else
                    {
                        intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    }
                }
                else
                {
                    intStatusCode = 1000001;
                }
            }
            catch 
            {
                intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        public void ProcessForMobileUser()
        {
            try
            {
                int intExecuteRecordsCount = -1;

                string NewPassword = GetPassword(UserName);

                SqlCommand objSqlCommand = new SqlCommand(@"SELECT * FROM ADMIN_TBL_USER_DETAIL
                                                        WHERE USER_NAME = @UserName
                                                        AND SUBADMIN_ID = @SubAdminId");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@UserName", this.UserName);
                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);

                DataTable dt = Utilities.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_USERS_RESET_PASSWORD_LOG
                    (USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED) 
                        VALUES(@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,0);");

                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", NewPassword);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["USER_ID"].ToString());
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);

                    intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        Utilities.SendInfoEmail(dt.Rows[0]["EMAIL_ID"].ToString(), "You forget your password so to access your account please use " + NewPassword + " . Please ", "Forget password of biz mobile");
                        intStatusCode = 0;
                    }
                    else
                    {
                        intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    }
                }
                else
                {
                    intStatusCode = 1000001;
                }
            }
            catch
            {
                intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }

        private string GetPassword(string _UserId)
        {
            string strGuid, strPassword;
            do
            {
                Guid objGuid = new Guid();
                objGuid = Guid.NewGuid();
                strGuid = Convert.ToString(objGuid);
                string[] stringArray = { "0", "0", "O", "o", "I", "L", "1", "l", "s", "S", "5", "9", "Q", "q", "-" };
                foreach (string strItem in stringArray)
                {
                    strGuid = strGuid.Replace(strItem, "");
                }
            }
            while (strGuid.Length < 12);

            Boolean bolHashCodeNotEmpty = false;
            strPassword = strGuid.Substring(0, 6);
            do
            {
                strPassword = Utilities.GetMd5Hash(strPassword);
                if (strPassword.Length > 0)
                    bolHashCodeNotEmpty = true;
            }
            while (bolHashCodeNotEmpty == false);
             return strPassword;
        }

        public int StatusCode
        {
            get
            {
                return intStatusCode;
            }
        }
        public string UserName
        {
            get;
            set;
        }
        public string CompanyID
        {
            get;
            set;
        }

        public string SubAdminId
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return UserName;
            }
        }
    }
  }