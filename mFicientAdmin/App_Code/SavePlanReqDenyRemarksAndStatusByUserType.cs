﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class SavePlanReqDenyRemarksAndStatusByUserType
    {
        public SavePlanReqDenyRemarksAndStatusByUserType(string companyId, string planCode, string requestType, USER_TYPE userType, string remarksCode)
        {

            this.CompanyId = companyId;
            this.PlanCode = planCode;
            this.RequestType = requestType;
            this.UserType = userType;
            this.RemarksCode = remarksCode;
        }

        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                string strQuery = @"UPDATE ADMIN_TBL_PLAN_ORDER_REQUEST
                                SET STATUS = @NewStatus
                                ,REMARKS = @Remarks
                                WHERE COMPANY_ID = @CompanyId
                                AND REQUEST_TYPE = @RequestType
                                AND PLAN_CODE = @PlanCode
                                AND STATUS = @PreviousStatus";
                SqlCommand cmd = new SqlCommand(strQuery);
                //cmd.Parameters.AddWithValue("@NewStatus",this.
                int iNewStatus = 0, iPreviousStatus = 0;
                switch (this.UserType)
                {
                    case USER_TYPE.Reseller:
                        iNewStatus = (int)PLAN_REQUEST_STATUS.RESELLER_DENIED;
                        iPreviousStatus = (int)PLAN_REQUEST_STATUS.UNAPPROVED;
                        break;
                    case USER_TYPE.SalesManager:
                        iNewStatus = (int)PLAN_REQUEST_STATUS.SALES_DENIED;
                        iPreviousStatus = (int)PLAN_REQUEST_STATUS.RESELLER_APPROVED;
                        break;
                    case USER_TYPE.Account:
                        iNewStatus = (int)PLAN_REQUEST_STATUS.ACCOUNTS_DENIED;
                        iPreviousStatus = (int)PLAN_REQUEST_STATUS.SALES_APPROVED;
                        break;

                }
                cmd.Parameters.AddWithValue("@Remarks", this.RemarksCode);
                cmd.Parameters.AddWithValue("@NewStatus", iNewStatus);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@RequestType", this.RequestType);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@PreviousStatus", iPreviousStatus);

                Utilities.ExecuteNonQueryRecord(cmd);
                StatusCode = 0;
                StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "";
            }
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public int PreviousStatus
        {
            set;
            get;
        }

        public int NewStatus
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string RequestType
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }
        public USER_TYPE UserType
        {
            get;
            set;
        }
        public string RemarksCode
        {
            get;
            set;
        }
    }
}