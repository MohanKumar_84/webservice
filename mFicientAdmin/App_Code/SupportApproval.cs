﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class SupportApprovalAdmin
    {
        public void GetServerName()
        {
            try
            {
            string strQuery = "select SERVER_NAME,SERVER_ID from ADMIN_TBL_MST_SERVER";
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet.Tables[0].Rows.Count > 0)
            {
                ResultTable = objDataSet.Tables[0];
            }
            else
            {
               this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
               this.StatusDescription = "Record Not Found";
            }
            }   
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void GetMBuzzServer()
        {
            try
            {
                string strQuery = "select MBUZZ_SERVER_NAME,MBUZZ_SERVER_ID from ADMIN_TBL_MBUZZ_SERVER";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record Not Found";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void GetMPlugginServer()
        {
            try
            {
                string strQuery = "select MP_SERVER_NAME,MP_SERVER_ID from ADMIN_TBL_MST_MP_SERVER";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record Not Found";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }
        
        public bool CheckUpdateInsert(string serverid,string mbuzzserverid,string companyid,string Mpluginserverid)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"if exists (select company_id from ADMIN_TBL_SERVER_MAPPING where COMPANY_ID =@companyid) 
                                        update ADMIN_TBL_SERVER_MAPPING set SERVER_ID=@serverid ,MBUZZ_SERVER_ID=@mbuzzserver,MPLUGIN_SERVER_ID=@mpluginserverid
                                        where COMPANY_ID =@companyid
                                        else
                                        insert into ADMIN_TBL_SERVER_MAPPING(SERVER_ID,MBUZZ_SERVER_ID,COMPANY_ID,SERVER_ADDED_ON,MBUZZ_ADDED_ON,MPLUGIN_SERVER_ID,MPLUGIN_ADDED_ON) values(@serverid,@mbuzzserver,@companyid,@serveradded,@mbuzzadded,@mpluginserverid,@mpluginadded)");
                cmd.Parameters.AddWithValue("@serverid", serverid);
                cmd.Parameters.AddWithValue("@mbuzzserver", mbuzzserverid);
                cmd.Parameters.AddWithValue("@companyid", companyid);
                cmd.Parameters.AddWithValue("@mpluginserverid", Mpluginserverid);
                cmd.Parameters.AddWithValue("@serveradded", DateTime.Now.Ticks);
                cmd.Parameters.AddWithValue("@mbuzzadded", DateTime.Now.Ticks);
                cmd.Parameters.AddWithValue("@mpluginadded", DateTime.Now.Ticks);

                int intResult = Utilities.ExecuteNonQueryRecord(cmd);
                if (intResult > 0)
                {
                    return true;
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
            }
              return false;
        }
        
        public void GetCompany(string CompanyId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"select d.COMPANY_NAME, s.
                                                SERVER_NAME 
                                                ,mb.MBUZZ_SERVER_NAME ,mp.MP_SERVER_NAME
                                                 from
                                                ADMIN_TBL_COMPANY_DETAIL d inner join ADMIN_TBL_SERVER_MAPPING m
                                                on d.COMPANY_ID=m.COMPANY_ID inner join ADMIN_TBL_MST_SERVER s on m.SERVER_ID=s.SERVER_ID
                                                inner join ADMIN_TBL_MBUZZ_SERVER mb on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID inner join ADMIN_TBL_MST_MP_SERVER
                                                mp on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID where d.COMPANY_ID=@companyid");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@companyid", CompanyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    this.ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record Not Found";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}