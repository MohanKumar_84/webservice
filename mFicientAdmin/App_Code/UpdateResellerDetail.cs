﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class UpdateResellerDetailAdmin
    {
        public void UpdateReseller(string resellername, string email, string contactnumber, string discount, string resellerid,byte isEnabled,byte isPaymentApproval,string[]resellerSalesPersons,string city,string countryCode)
        {
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);


            //try
            //{
            //    con.Open();

            //    objSqlCmd.CommandType = CommandType.Text;
            //    Utilities.ExecuteNonQueryRecord(objSqlCmd);

            //}

            //catch (System.Data.SqlClient.SqlException ex)
            //{
            //    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
            //    {
            //        throw ex;
            //    }
            //}


            //finally
            //{
            //    Utilities.SqlConnectionClose(con);
            //}

            try
            {
                SqlTransaction transaction = null;
                SqlConnection con;
                Utilities.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        transaction = con.BeginTransaction();
                        updateResellerDetails(resellername, email, contactnumber, discount, resellerid, isEnabled, isPaymentApproval,city,countryCode, con, transaction);
                        deleteResellerSalesPersons(con, transaction, resellerid);
                        saveResellerSalesPersons(resellerSalesPersons, resellerid, con, transaction);
                        transaction.Commit();
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        this.StatusDescription = "Internal server error,Please try again";
                    }
                    this.StatusDescription = "Internal server error,Please try again";
                }
                catch (Exception ex)
                {
                    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        this.StatusDescription = "Internal server error,Please try again";
                    }
                    this.StatusDescription = "Internal server error,Please try again";
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusDescription = "Internal server error,Please try again";
                }
                this.StatusDescription = "Internal server error,Please try again";
            }
        }

        void deleteResellerSalesPersons(SqlConnection con, SqlTransaction transaction,string resellerId)
        {
            try
            {
                string strQuery = @"DELETE FROM ADMIN_TBL_RESELLER_SALES_PERSONS
                                WHERE RESELLER_ID = @ResellerId";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@ResellerId", resellerId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        void saveResellerSalesPersons(string[] resellerSalesPersons, string resellerId, SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"INSERT INTO [TBL_RESELLER_SALES_PERSONS]
                                   ([RESELLER_ID]
                                   ,[SELLER_ID])
                             VALUES
                                   (@RESELLER_ID
                                   ,@SELLER_ID)";

                for (int i = 0; i < resellerSalesPersons.Length; i++)
                {
                    SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                    cmd.Parameters.AddWithValue("@RESELLER_ID", resellerId);
                    cmd.Parameters.AddWithValue("@SELLER_ID", resellerSalesPersons[i]);
                    cmd.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }
        void updateResellerDetails(string resellername, string email, string contactnumber, string discount, string resellerid, byte isEnabled, byte isPaymentApproval
                                   , string city, string countryCode, SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                //,LOGIN_NAME='@login',PASSWORD='@password',CREATED_ON='@createdon' not required
                string strQuery = @"update ADMIN_TBL_RESELLER_DETAIL set RESELLER_NAME=@resellername,EMAIL=@email,CONTACT_NUMBER=@Contactnum,DISCOUNT=@discount
                ,[IS_ENABLE]=@isEnabled,[IS_PAYMENT_APPROVAL]=@isPaymentApproval,[CITY]=@city,[COUNTRY]=@countrycode
                WHERE RESELLER_ID=@resellerId";
                SqlCommand objSqlCmd = new SqlCommand(strQuery, con, transaction);
                objSqlCmd.CommandType = CommandType.Text;
                objSqlCmd.Parameters.AddWithValue("@resellername", resellername);
                objSqlCmd.Parameters.AddWithValue("@email", email);
                objSqlCmd.Parameters.AddWithValue("@Contactnum", contactnumber);
                objSqlCmd.Parameters.AddWithValue("@discount", discount);
                objSqlCmd.Parameters.AddWithValue("@resellerId", resellerid);
                objSqlCmd.Parameters.AddWithValue("@isEnabled", isEnabled == 1 ? 1 : 0);
                objSqlCmd.Parameters.AddWithValue("@isPaymentApproval", isPaymentApproval == 1 ? 1 : 0);
                objSqlCmd.Parameters.AddWithValue("@city", city);
                objSqlCmd.Parameters.AddWithValue("@countryCode", countryCode);
                objSqlCmd.ExecuteNonQuery();
             }

            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }

        }
        public string ResellerName
        {
            set;
            get;
        }
        public string Email
        {
            set;
            get;
        }
        public string ContactNumber
        {
            get;
            set;
        }

        public string SellerId
        {
            get;
            set;
        }

        public string ResellerId
        {
            get;
            set;
        }

        public string Loginname
        {
            get;
            set;
        }

        public string password
        {
            get;
            set;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }

}