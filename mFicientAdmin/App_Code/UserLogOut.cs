﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class UserLogOutAdmin
    {
        public void Process(string UserId,string SessionId)
        {

            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                Utilities.SqlConnectionOpen(out objSqlConnection);
                string query = @"DELETE FROM ADMIN_TBL_LOGIN_SESSION where USER_ID=@userid and SESSION_ID=@SessionId";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@userid", UserId);
                objSqlCommand.Parameters.AddWithValue("@SessionId", SessionId);

                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }

                query = @"update ADMIN_TBL_LOGIN_HISTORY set LOGOUT_DATETIME=@LOGOUT_DATETIME, LOGOUT_TYPE='normal' where USER_ID=@userid and LOGIN_DEVICE_TOKEN=login_device_token
                              and LOGIN_DEVICE_TYPE_CODE=@login_device_type_code AND LOGOUT_DATETIME=0";

                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@userid", UserId);
                objSqlCommand.Parameters.AddWithValue("@login_device_type_code", "web");
                objSqlCommand.Parameters.AddWithValue("@login_device_token", "");
                objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", DateTime.Now.Ticks);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                  ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString();
                }
                objSqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                objSqlTransaction.Rollback();
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
            }

        }


        string strId;
        public string GetLOginId()
        {
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"select ADMIN_ID from ADMIN_TBL_ADMIN_LOGIN ");
                objSqlCommand.CommandType = CommandType.Text;
                // objSqlCommand.Parameters.AddWithValue("@type", Type);
                DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                strId = objDataset.Tables[0].Rows[0]["ADMIN_ID"].ToString();
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strId;
        }


        string struserid;
        public string GetUserId(string username)
        {
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"select ADMIN_ID from ADMIN_TBL_ADMIN_LOGIN where ADMIN_NAME=@username");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@username", username);
                DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                struserid = objDataset.Tables[0].Rows[0]["ADMIN_ID"].ToString();
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return struserid;
        }

       public string UserId
        {
            set;
            get;
        }
        public string SessionID
        {
            set;
            get;
        }
        public string LocationName
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }

    }
