﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
using System.Globalization;
using System.Runtime.Serialization.Json;


namespace mFicientAdmin
{

    public enum TRIAL_ACCOUNT_VALIDATION
    {
        MAX_SUBADMIN = 1,
        MAX_USER = 2,
        MAX_FORM = 5,
        MAX_WORKFLOW = 2,
        VALIDITY = 30,
    }

    public enum PLAN_RENEW_OR_CHANGE_REQUESTS
    {
        RENEW = 1,
        USERS_ADDED = 2,
        PLAN_UPGRADE = 3,
        PLAN_DOWNGRADE = 4,
        PLAN_PURCHASE = 5,
        TRIAL = 6,
    }
    public enum DATABASE_ERRORS//9000000
    {
        DATABASE_CONNECTION_ERROR = 9000001,
        RECORD_NOT_FOUND_ERROR = 9000002,
        RECORD_INSERT_ERROR = 9000003,
        RECORD_DELETE_ERROR = 9000004,
        USER_ISNOT_ADMIN = 9000005,
    }

    public enum USERLOGIN_ERRORS
    {
        USER_ALREADY_LOGGED_ERROR = 1100201,
        EMAIL_OR_PASSWORD_ERROR = 1100202,
        PASSWORD_ERROR = 1100203,
    }
    public enum REGISTERUSER_ERRORS
    {
        EMAIL_ALREADY_EXISTS = 1000101
    }

    public enum PLAN_REQUEST_STATUS
    {
        UNAPPROVED = 0,
        RESELLER_APPROVED = 1,
        SALES_APPROVED = 2,
        ACCOUNTS_APPROVED = 3,
        CANCELLED = 4,
        RESELLER_DENIED = 5,
        SALES_DENIED = 6,
        ACCOUNTS_DENIED = 7,
    }

    public enum USER_TYPE
    {
        Reseller = 1,
        SalesManager = 2,
        Account = 3,
        Support = 4,
        SalesPerson = 5,
        Admin = 6
    }
    public enum COMPANY_ACCESS_CHANGE_TYPE
    {
        Block = 1,
        Unblock = 2
    }
    public enum DIALOG_TYPE
    {
        None,
        Error,
        Warning,
        Info
    }
    public enum CURRENCY_TYPE
    { 
        INR,
        USD
    }
    public class Utilities
    {
        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;

            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

            objSqlDataAdapter.Fill(ObjDataSet);

            SqlConnectionClose(objSqlConnection);

            return ObjDataSet;
        }

        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        public static void BindCountryDropDown(DropDownList _ddlDropdownId)
        {
            GetCountryList countryList = new GetCountryList();
            countryList.Process();
            DataTable dt = countryList.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataValueField = "COUNTRY_CODE";
            _ddlDropdownId.DataTextField = "COUNTRY_NAME";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("Select Country", "-1"));

        }

        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }

        /// <summary>
        /// Description :Create 32 bit Hash Code
        /// </summary>
        public static string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }

        /// <summary>
        /// Description :Email out box insert
        /// </summary>
        public static bool SendInfoEmail(string _Email, string _EmailBody, string _EmailSubject)
        {
            string strPostDateTime = DateTime.Now.Ticks.ToString();
            string strEmailID = GetMd5Hash(_Email + strPostDateTime.ToString().Substring(0, 16));
            //GetEmailID(_Email, strPostDateTime, out strEmailID);
            SqlCommand objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_EMAIL_OUTBOX(EMAIL_ID,EMAIL,SUBJECT,BODY,POST_TIME,SEND_TIME,STATUS)
                            VALUES(@EMAIL_ID,@EMAIL,@SUBJECT,@BODY,@POST_TIME,@SEND_TIME,@STATUS)");
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", strEmailID);
            objSqlCommand.Parameters.AddWithValue("@EMAIL", _Email);
            objSqlCommand.Parameters.AddWithValue("@SUBJECT", _EmailSubject);
            objSqlCommand.Parameters.AddWithValue("@POST_TIME", DateTime.Now.Ticks);
            objSqlCommand.Parameters.AddWithValue("@BODY", _EmailBody);
            objSqlCommand.Parameters.AddWithValue("@SEND_TIME", 0);
            objSqlCommand.Parameters.AddWithValue("@STATUS", 0);
            int intResult = Utilities.ExecuteNonQueryRecord(objSqlCommand);
            if (intResult > 0)
            {
                return true;
            }
            return false;
        }

        public static string IsValid(string username, string password)
        {
            string strQuery = @"SELECT RESELLER_ID  FROM ADMIN_TBL_RESELLER_DETAIL WHERE LOGIN_NAME=@Username AND PASSWORD=@UPassword ";
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.Parameters.AddWithValue("@Username", username);
            objSqlCommand.Parameters.AddWithValue("@UPassword", Utilities.GetMd5Hash(password));
            objSqlCommand.CommandType = CommandType.Text;
            DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);

            if (objDataSet.Tables[0].Rows.Count > 0)
            {
                return objDataSet.Tables[0].Rows[0][0].ToString();
            }
            else
            {
                string Query = @"select LOGIN_ID  from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@Username and PASSWORD=@UPassword ";
                SqlCommand SqlCommand = new SqlCommand(Query);
                SqlCommand.Parameters.AddWithValue("@Username", username);
                SqlCommand.Parameters.AddWithValue("@UPassword", password);
                SqlCommand.CommandType = CommandType.Text;
                DataSet ResultSet = Utilities.SelectDataFromSQlCommand(SqlCommand);
                if (ResultSet.Tables[0].Rows.Count > 0)
                {
                    return ResultSet.Tables[0].Rows[0][0].ToString();
                }
            }
            return "";
        }

        public static Boolean IsLoggedIn(string _Userid, string _DeviceTokenTypeCode, string _DeviceToken, out string _SessionID, out string _DeviceDetail, out string _IP)
        {
            _DeviceDetail = "";
            _IP = "";
            Boolean blnIsAlreadyLogin = false;
            string strQuery = @"SELECT a.USER_ID, a.LOGIN_DEVICE_TYPE_CODE,b.SESSION_ID,b.LOGIN_DEVICE_TOKEN,a.LOGIN_HOST_IP FROM 
			                        ADMIN_TBL_LOGIN_HISTORY a INNER JOIN ADMIN_TBL_LOGIN_SESSION b ON a.USER_ID = b.USER_ID 
                                    AND a.LOGIN_DEVICE_TOKEN = b.LOGIN_DEVICE_TOKEN AND a.LOGIN_DEVICE_TYPE_CODE = b.LOGIN_DEVICE_TYPE_CODE 
                                    WHERE LOGOUT_DATETIME=@LOGOUT_DATETIME  and LOGIN_SUCCESS =@LOGIN_SUCCESS and a.USER_ID=@userid ";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@userid", _Userid);
            objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", 0);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_SUCCESS", 1);
            _SessionID = "";
            DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
            if (_DeviceTokenTypeCode == "web")
            {
                foreach (DataRow objdataRow in objDataSet.Tables[0].Rows)
                {
                    if (objdataRow["LOGIN_DEVICE_TYPE_CODE"].ToString() == "web")
                    {
                        blnIsAlreadyLogin = true;
                        _SessionID = objdataRow["SESSION_ID"].ToString();
                        _IP = objdataRow["LOGIN_HOST_IP"].ToString();
                        break;
                    }
                }
            }
            return blnIsAlreadyLogin;
        }

        public static Boolean IsUserLoggedIn(string _Userid, string _DeviceTokenTypeCode, string _DeviceToken, out string _SessionID, out string _DeviceDetail, out string _IP)
        {
            _DeviceDetail = "";
            _IP = "";
            Boolean blnIsAlreadyLogin = false;
            string strQuery = @"SELECT a.USER_ID,a.LOGIN_DEVICE_TYPE_CODE,b.SESSION_ID,b.LOGIN_DEVICE_TOKEN,a.LOGIN_HOST_IP FROM 
			                        ADMIN_TBL_LOGIN_USER_HISTORY a INNER JOIN ADMIN_TBL_LOGIN_USER_SESSION b ON a.USER_ID = b.USER_ID 
                                    AND a.LOGIN_DEVICE_TOKEN = b.LOGIN_DEVICE_TOKEN AND a.LOGIN_DEVICE_TYPE_CODE = b.LOGIN_DEVICE_TYPE_CODE 
                                    WHERE LOGOUT_DATETIME=@LOGOUT_DATETIME  and LOGIN_SUCCESS =@LOGIN_SUCCESS and a.USER_ID=@userid ";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@userid", _Userid);
            objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", 0);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_SUCCESS", 1);
            _SessionID = "";
            DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
            if (_DeviceTokenTypeCode == "web")
            {
                foreach (DataRow objdataRow in objDataSet.Tables[0].Rows)
                {
                    if (objdataRow["LOGIN_DEVICE_TYPE_CODE"].ToString() == "web")
                    {
                        blnIsAlreadyLogin = true;
                        _SessionID = objdataRow["SESSION_ID"].ToString();
                        _IP = objdataRow["LOGIN_HOST_IP"].ToString();
                        break;
                    }
                }
            }
            return blnIsAlreadyLogin;
        }

        public static bool IsValidEmail(string _email)
        {
            if (!string.IsNullOrEmpty(_email))
            {
                string strRegex = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                    + @"([a-zA-Z\-]+\.)+[a-zA-Z]{2,4})$";

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_email))
                    return (true);
                else
                    return (false);
            }
            else
                return false;
        }

        public static bool IsValidString(string _InputString, bool _a_z, bool _A_Z, bool _0_9, string _SpecialChars, int _MinLength, int _MaxLength, bool _StartSpaceAllowed, bool _EndSpaceAllowed)
        {
            try
            {
                if ((!_StartSpaceAllowed && _InputString.StartsWith(" ")) || (!_EndSpaceAllowed && _InputString.EndsWith(" ")))
                {
                    return false;
                }

                if ((_MaxLength < _MinLength) || (_MinLength >= 0 && _InputString.Length < _MinLength) || (_MaxLength >= 0 && _InputString.Length > _MaxLength))
                {
                    return false;
                }

                /* Create a regex expression based on _a-z and _A_Z and _0_9 and DisallowedCharacters */

                if (string.IsNullOrEmpty(_InputString))
                {
                    return true;
                }

                string strRegex = "";

                if (_a_z)
                {
                    strRegex += "a-z";
                }

                if (_A_Z)
                {
                    strRegex += "A-Z";
                }

                if (_0_9)
                {
                    strRegex += "0-9";
                }

                string strSpecialCharacters = _SpecialChars.Replace(@"\", @"\\");

                if (strSpecialCharacters.Contains("]"))
                {
                    strRegex = "]" + strRegex;
                    strSpecialCharacters = strSpecialCharacters.Replace("]", "");
                }

                if (strSpecialCharacters.Contains("^"))
                {
                    strRegex += "^";
                    strSpecialCharacters = strSpecialCharacters.Replace("^", "");
                }

                if (strSpecialCharacters.Contains("-"))
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters.Replace("-", "") + "-" + "]+$";
                }
                else
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters + "]+$";
                }

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_InputString))
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }
        public static void BindResellerddl(DropDownList _ddlR, string ID)
        {
            SqlCommand cmd = new SqlCommand(@"select RESELLER_NAME,r.RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL  r inner join ADMIN_TBL_RESELLER_SALES_PERSONS s on r.RESELLER_ID=s.RESELLER_ID where s.SELLER_ID in (select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@SELLER_ID) ");
            cmd.Parameters.AddWithValue("@SELLER_ID", ID);
            cmd.CommandType = CommandType.Text;
            DataSet ds = Utilities.SelectDataFromSQlCommand(cmd);
            _ddlR.DataSource = ds;
            _ddlR.DataTextField = "RESELLER_NAME";
            _ddlR.DataValueField = "RESELLER_ID";
            _ddlR.DataBind();
            _ddlR.Items.Insert(0, new ListItem("--Select--", "-1"));
        }

        #region Alert Message And PopUp Javascript Call
        public static void showMessage(string message, System.Web.UI.Page page, DIALOG_TYPE dialogType)
        {
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "Message", @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.Page page, string keyForScript, DIALOG_TYPE dialogType)
        {
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), keyForScript, @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        public static void closeModalPopUp(string divIdToClose, System.Web.UI.Page page)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "CloseModal", @"$('#" + divIdToClose + "').dialog('close')", true);
        }
        public static void closeModalPopUp(string divIdToClose, System.Web.UI.Page page, string keyForScript)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), keyForScript, @"$('#" + divIdToClose + "').dialog('close')", true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="headerText"></param>
        /// <param name="width">Pass empty string for default width</param>
        /// <param name="showCloseButton"></param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.Page page, string headerText, string width, bool showCloseButton)
        {
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + ")", true);
            }
        }
        public static void showModalPopup(string divIdToShow, System.Web.UI.Page page, string headerText, string width, bool showCloseButton, bool isResizable)
        {
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
        }

        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.Page page)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }

        public static void showMessage(string message, System.Web.UI.Page page)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "Message", @"$.alert(" + "'" + message + "'" + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.Page page, string keyForScript)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), keyForScript, @"$.alert(" + "'" + message + "'" + ");", true);
        }
        public static void showAlertOnPageStartUp(string message, string divIdToShowMsg, System.Web.UI.Page page, string scriptKey)
        {
            page.ClientScript.RegisterStartupScript(
                page.GetType(),
                scriptKey,
                @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }

        #region With Update Panel
        public static int getModalPopUpWidth(MODAL_POP_UP_NAME name)
        {
            int iWidth = 0;
            switch (name)
            {
                case MODAL_POP_UP_NAME.USER_DETAIL:
                    iWidth = 650;
                    break;
                case MODAL_POP_UP_NAME.DEVICE_DETAILS_REPEATER:
                    iWidth = 600;
                    break;
                case MODAL_POP_UP_NAME.SUB_ADMIN_DETAILS:
                    iWidth = 400;
                    break;
                case MODAL_POP_UP_NAME.MANAGE_GROUP_POPUPS:
                    iWidth = 350;
                    break;
                case MODAL_POP_UP_NAME.POST_BACK_CONFIRMATION:
                    iWidth = 320;
                    break;
                case MODAL_POP_UP_NAME.CHANGE_PASSWORD:
                    iWidth = 320;
                    break;
            }
            return iWidth;
        }

        public static void closeModalPopUp(string divIdToClose, System.Web.UI.UpdatePanel updPanel)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "CloseModal", @"$('#" + divIdToClose + "').dialog('close')", true);
        }
        public static void closeModalPopUp(string divIdToClose, System.Web.UI.UpdatePanel updPanel, string keyForScript)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"$('#" + divIdToClose + "').dialog('close')", true);
        }
        public static void closeModalPopUp(string divIdToClose, System.Web.UI.UpdatePanel updPanel, string keyForScript, string divForRemovingImg)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"$('#" + divIdToClose + "').dialog('close');removeDialogImage(" + "'" + divForRemovingImg + "'" + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.UpdatePanel updPanel, DIALOG_TYPE dialogType)
        {
            if (updPanel == null) return;
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "Message", @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.UpdatePanel updPanel, string keyForScript, DIALOG_TYPE dialogType)
        {
            if (updPanel == null) return;
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        /// <summary>
        /// The pop up is not resizable
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="headerText"></param>
        /// <param name="width">Pass empty string for default width</param>
        /// <param name="showCloseButton"></param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string headerText, string width, bool showCloseButton)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + "false" + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + "false" + ")", true);
            }
        }
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string headerText, string width, bool showCloseButton, bool isResizable)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="keyForScript"></param>
        /// <param name="headerText"></param>
        /// <param name="width">Pass empty string for default width</param>
        /// <param name="showCloseButton"></param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string keyForScript, string headerText, string width, bool showCloseButton)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + ")", true);
            }
        }
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string keyForScript, string headerText, string width, bool showCloseButton, bool isResizable)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="headerText"></param>
        /// <param name="width"></param>
        /// <param name="showCloseButton"></param>
        /// <param name="dialogType"></param>
        /// <param name="div">Div whose header has to be prepended with image.</param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string headerText, string width, bool showCloseButton, bool isResizable, DIALOG_TYPE dialogType, string div)
        {
            if (updPanel == null) return;
            string strTrueFalse = "";
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            string showDialogImageType = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    break;
                case DIALOG_TYPE.Warning:
                    showDialogImageType = "DialogType.Warning";
                    break;
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="keyForScript"></param>
        /// <param name="headerText"></param>
        /// <param name="width"></param>
        /// <param name="showCloseButton"></param>
        /// <param name="isResizable"></param>
        /// <param name="dialogType"></param>
        /// <param name="div">Div whose header has to be prepended with image.</param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string keyForScript, string headerText, string width, bool showCloseButton, bool isResizable, DIALOG_TYPE dialogType, string div)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            string showDialogImageType = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    break;
                case DIALOG_TYPE.Warning:
                    showDialogImageType = "DialogType.Warning";
                    break;
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }
        }

        public static string getPostBackControlName(System.Web.UI.Page page)
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                return control.ID;
            else return "";
        }

        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.UpdatePanel updPanel)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "AlertMessage", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.UpdatePanel updPanel, string scriptKey)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), scriptKey, @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.Page page, string scriptKey)
        {
            ScriptManager.RegisterStartupScript(page, typeof(Page), scriptKey, @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptToRun">; separated scripts,functionName.Pass as verbatim string</param>
        /// <param name="page"></param>
        /// <param name="scriptKey"></param>
        public static void runPostBackScript(string scriptToRun, System.Web.UI.UpdatePanel updPanel, string scriptKey)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), scriptKey, scriptToRun, true);
        }
        public static void runPostBackScript(string scriptToRun, System.Web.UI.Page page, string scriptKey)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), scriptKey, scriptToRun, true);
        }
        public static void runPageStartUpScript(System.Web.UI.Page page, string key,
                                                string scriptToRun)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), key, scriptToRun, true);
        }
        #endregion
        #endregion
        /// <summary>
        /// date in dd-mmm-yyyy
        /// </summary>
        /// <param name="date"></param>
        public static long convertDateToTicks(string date, out string errorDescription)
        {
            errorDescription = "";
            string[] aryDateParts = date.Split('-');
            try
            {
                //DateTime dtDateSelected = new DateTime(Convert.ToInt32(aryDateParts[0]), Convert.ToInt32(aryDateParts[1]), Convert.ToInt32(aryDateParts[2]));
                DateTime dtDateSelected = new DateTime(Convert.ToInt32(aryDateParts[2]), Convert.ToInt32(aryDateParts[1]), Convert.ToInt32(aryDateParts[0]));
                return dtDateSelected.Ticks;
            }
            catch (FormatException e)
            {
                errorDescription = "Date not in valid format.Please enter valid date";
                return -1000;
            }
            catch (Exception e)
            {
                errorDescription = "Please enter a valid date";
                return -1000;
            }
        }

        /// <summary>
        /// date in dd-mm-yyyy
        /// </summary>
        /// <param name="date"></param>
        public static bool isValidDOB(string dateOfBirth)
        {
            if (!string.IsNullOrEmpty(dateOfBirth))
            {
                try
                {
                    string[] aryDateParts = dateOfBirth.Split('-');
                    //DateTime dt = new DateTime(int.Parse(aryDateParts[0]), int.Parse(aryDateParts[1]), int.Parse(aryDateParts[2]));
                    DateTime dt = new DateTime(int.Parse(aryDateParts[2]), int.Parse(aryDateParts[1]), int.Parse(aryDateParts[0]));
                    if (dt < DateTime.UtcNow.Date)
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
        #region Data Encryption

        public static string EncryptString(string s)
        {
            string strEnc = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strEnc;
                }

                int key1 = (DateTime.Now.Second % 10) + 2;
                int key2 = ((key1 + DateTime.Now.Minute) % 10) + 4;
                string strThisAscii = "";

                for (int i = 0; i < s.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key1 - key2);
                    }
                    else
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key2 - key1);
                    }

                    do
                    {
                        strThisAscii = "0" + strThisAscii;

                    } while (strThisAscii.Length < 4);

                    strEnc = strThisAscii + strEnc;
                }

                strEnc = (Hex(key1 * 10) + strEnc + Hex(key2 * 10)).ToLower();

                strEnc = strEnc.Replace("a", "g").Replace("b", "i").Replace("c", "k").Replace("d", "m").Replace("e", "o").Replace("f", "q").Replace("1", "s").Replace("2", "u");
                strEnc = strEnc.Replace("3", "v").Replace("4", "t").Replace("5", "r").Replace("6", "p").Replace("7", "n").Replace("8", "l").Replace("9", "j").Replace("0", "h");
            }
            catch
            {
                strEnc = "";
            }
            return strEnc;
        }

        public static string DecryptString(string s)
        {
            string strDec = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strDec;
                }

                s = s.Replace("h", "0").Replace("j", "9").Replace("l", "8").Replace("n", "7").Replace("p", "6").Replace("r", "5").Replace("t", "4").Replace("v", "3");
                s = s.Replace("u", "2").Replace("s", "1").Replace("q", "f").Replace("o", "e").Replace("m", "d").Replace("k", "c").Replace("i", "b").Replace("g", "a");

                int key1 = (int)HexToDec(s.Substring(0, 2)) / 10;
                int key2 = (int)HexToDec(s.Substring(s.Length - 2, 2)) / 10;

                s = s.Substring(2, s.Length - 4);

                string strThisChar = "";
                bool oddChar = true;

                for (int i = s.Length - 4; i >= 0; i -= 4)
                {
                    if (oddChar)
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key1 + key2);
                        oddChar = false;
                    }
                    else
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key2 + key1);
                        oddChar = true;
                    }

                    strDec += strThisChar;
                }

                return strDec;
            }
            catch
            {
            }
            return "";
        }

        public static string Hex(int n)
        {
            try
            {
                return n.ToString("X");
            }
            catch
            {
            }
            return "";
        }

        public static int HexToDec(string s)
        {
            int intResult;
            try
            {
                intResult = Convert.ToInt32(s, 16);
            }
            catch
            {
                intResult = 0;
            }
            return intResult;
        }

        public static string StringReverse(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            StringBuilder rs = new StringBuilder(s.Length);
            for (int i = s.Length - 1; i >= 0; i--)
            {
                rs.Append(s.Substring(i, 1));
            }
            return rs.ToString();
        }

        private static short Asc(string String)
        {
            return Encoding.Default.GetBytes(String)[0];
        }

        private static string Chr(int CharCode)
        {
            if (CharCode > 255)
            {
                return "";
            }
            return Encoding.Default.GetString(new[] { (byte)CharCode });
        }

        #endregion

        public static DataTable GetMainMenuList(string strRoles)
        {
            DataTable dtMenuList = null;
            try
            {
                string strMenuListKey = CacheManager.GetKey(CacheManager.CacheType.mficientMenu, string.Empty, strRoles, string.Empty, string.Empty, string.Empty, string.Empty);
                dtMenuList = CacheManager.Get<DataTable>(strMenuListKey);
                if (dtMenuList == null)
                {
                    DataSet dsMenuList = SelectDataFromSQlCommand(new SqlCommand("select id,Link,PAGE,CLASS,HEADER,HELP_TEXT from ADMIN_TBL_MENU_ROLE_WISE where PARENT_ID=0 And role in(" + strRoles + ") order by id"));
                    if (dsMenuList != null)
                    {
                        dtMenuList = dsMenuList.Tables[0];
                        CacheManager.Insert<DataSet>(strMenuListKey, dtMenuList, DateTime.UtcNow, TimeSpan.FromSeconds(900));
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return dtMenuList;
        }
        public static DataSet GetSubMenuList(string Pagename, string ParentID)
        {
            return SelectDataFromSQlCommand(new SqlCommand("select id,Link,PAGE,CLASS,HEADER,HELP_TEXT from ADMIN_TBL_MENU_ROLE_WISE where PARENT_ID=" + ParentID + ";select id from TBL_MENU_ROLE_WISE where PAGE='" + Pagename + "' and PARENT_ID=" + ParentID + " order by id"));
        }

        public static string UrlEncode(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return HttpUtility.UrlEncode(s).Replace(@"'", @"%27");
        }

        public static string SerializeJson<T>(object obj)
        {
            T objResponse = (T)obj;
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objResponse.GetType());
            serializer.WriteObject(stream, objResponse);
            stream.Position = 0;
            StreamReader streamReader = new StreamReader(stream);
            return streamReader.ReadToEnd();
        }

        public static string getFinalJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"resp\":" + jsonWithDetails + "}";
            return strFinalJson;
        }

        public static T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }

        public static string getRequestJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"req\":" + jsonWithDetails + "}";
            return strFinalJson;
        }

        #region  User type Code
        public static string getUserTypeCodeFromUserType(USER_TYPE userType)
        {
            string strUserType = String.Empty;
            switch (userType)
            {
                case USER_TYPE.Reseller:
                    strUserType = "RS";
                    break;
                case USER_TYPE.SalesManager:
                    strUserType = "SM";
                    break;
                case USER_TYPE.Account:
                    strUserType = "ACC";
                    break;
                case USER_TYPE.Support:
                    strUserType = "SUPP";
                    break;
                case USER_TYPE.SalesPerson:
                    strUserType = "S";
                    break;
                case USER_TYPE.Admin:
                    strUserType = "A";
                    break;
            }
            return strUserType;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when the code is not defined</exception>
        public static USER_TYPE getUserTypeFromUserCode(string code)
        {
            string strEnumCode = getUserTypeEnumCodeFromStringCode(code);
            if (String.IsNullOrEmpty(strEnumCode)) throw new Exception("User code is not defined");
            USER_TYPE userType =
               (USER_TYPE)Enum.Parse(typeof(USER_TYPE), strEnumCode);
            if (Enum.IsDefined(typeof(USER_TYPE), userType))
            {
                return userType;
            }
            else
            {
                throw new Exception("User code is not defined");
            }
        }
        private static string getUserTypeEnumCodeFromStringCode(string code)
        {
            string enumCode = String.Empty;
            switch (code)
            {
                case "R":
                    enumCode = ((int)USER_TYPE.Reseller).ToString();
                    break;
                case "S":
                    enumCode = ((int)USER_TYPE.SalesPerson).ToString();
                    break;
                case "SM":
                    enumCode = ((int)USER_TYPE.SalesManager).ToString();
                    break;
                case "SUPP":
                    enumCode = ((int)USER_TYPE.Support).ToString();
                    break;
                case "ACC":
                    enumCode = ((int)USER_TYPE.Account).ToString();
                    break;
                case "A":
                    enumCode = ((int)USER_TYPE.Admin).ToString();
                    break;
            }
            return enumCode;
        }
        #endregion

        #region Input Uniform After PostBack
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="key"></param>
        /// <param name="isPostBack"></param>
        /// <param name="otherScripts">pass semicolon separated scripts</param>
        public static void makeInputFieldsUniform(System.Web.UI.Page page, string key, bool isPostBack, string otherScripts)
        {
            if (isPostBack)
            {
                ScriptManager.RegisterStartupScript(page, typeof(Page), key, "$(\"div:not('.selector')>select\").uniform();$(\"input:not(.checker input,.uploader input)\").uniform();" + otherScripts, true);
            }
            else
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), key, "$(\"div:not('.selector')>select\").uniform();$(\"input:not(.checker input,.uploader input)\").uniform();" + otherScripts, true);
            }
        }
        public static void makeInputFieldsUniformIfMenuClickedAgain(System.Web.UI.Page page, string otherScripts)
        {
            ScriptManager.RegisterStartupScript(page, typeof(Page), "MakeInputUniformInMenuIsClickedAgain", "$(\"div:not('.selector')>select\").uniform();$(\"input:not(.checker input,.uploader input)\").uniform();" + otherScripts, true);
        }
        #endregion

        #region Admin Panel Validity Helper
        /// <summary>
        /// 
        /// </summary>
        /// <param name="validity"></param>
        /// <param name="purchaseDateAsTicks"></param>
        /// <returns></returns>
        internal static DateTime getValidTillAsDateTime(double validity,
             long purchaseDateAsTicks)
        {
            DateTime purchaseDateAsDatetime = new DateTime(purchaseDateAsTicks, DateTimeKind.Utc);
            DateTime dtValidTill = purchaseDateAsDatetime.AddMonths(Convert.ToInt32(validity));
            return dtValidTill;
        }
        #endregion

        public static string getFullLocalFormattedDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return getFullFormattedDateForUI(dtInLocalTimezone);
        }
        public static string getFullFormattedDateForUI(DateTime date)
        {
            return date.ToString("F",
                  System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }
        public static bool validateMobileNo(string mobileNo)
        {
            try
            {
                string strErrorMsg = "Invalid contact no.";
                if (String.IsNullOrEmpty(mobileNo)) throw new ArgumentNullException("Please provice a contact no.");
                if (mobileNo.Length < 7 || mobileNo.Length > 20) throw new MficientException(strErrorMsg);
                char chrFirstChar = Convert.ToChar(mobileNo.Substring(0, 1));
                if (chrFirstChar != '+') throw new MficientException(strErrorMsg);
                char chrSecondChar = Convert.ToChar(mobileNo.Substring(1, 1));
                int iSecondChar = 0;
                try
                {
                    //check if it is a number.
                    iSecondChar = int.Parse(chrSecondChar.ToString());
                }
                catch
                {
                    throw new MficientException(strErrorMsg);
                }
                if (iSecondChar == 0) throw new MficientException(strErrorMsg);
                string strNoFromThirdChar = mobileNo.Substring(2, mobileNo.Length - 2);
                try
                {
                    ulong.Parse(strNoFromThirdChar);
                }
                catch
                {
                    throw new MficientException(strErrorMsg);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Company Plan
        internal static double roundOfAmountByCurrencyType(double amount, CURRENCY_TYPE currencyType)
        {
            double dblFinalAmount = 0;
            //for inr round to nearest 10 rs and for usd round to nearest 0.1 $
            switch (currencyType)
            {
                case CURRENCY_TYPE.INR:
                    int iAmount = Convert.ToInt32(Math.Ceiling(amount));
                    if ((iAmount % 10) >= 5)
                    {
                        iAmount = iAmount + (10 - (iAmount % 10));
                    }
                    else
                    {
                        iAmount = iAmount - (iAmount % 10);
                    }
                    dblFinalAmount = iAmount;
                    break;
                case CURRENCY_TYPE.USD:
                    dblFinalAmount = Math.Round(amount, 1);
                    break;
            }
            return dblFinalAmount;
        }
        #endregion
    }
}