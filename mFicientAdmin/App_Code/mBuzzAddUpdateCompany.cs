﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientAdmin
{
    public class mBuzzAddUpdateCompany
    {
         #region Private Members

        private string strJson;
        private mBuzzAddUpdateCompanyJson addNewContactRequestJson;

        #endregion

        #region Public Properties

        public string RequestJson
        {
            get
            {
                return strJson;
            }
        }

        #endregion

        #region Constructor

        public mBuzzAddUpdateCompany(string _msgRefId, string _enterpriseId, string _enterpriseName,string serverurl)
        {
            addNewContactRequestJson = new mBuzzAddUpdateCompanyJson();
            mBuzzAddUpdateCompanyJsonData objmBuzzAddUpdateCompanyJsonData = new mBuzzAddUpdateCompanyJsonData();
            addNewContactRequestJson.mrid = _msgRefId;
            addNewContactRequestJson.enid = _enterpriseId;
            addNewContactRequestJson.type = ((int)MessageCode.ADD_UPDATE_COMPANY).ToString();
            objmBuzzAddUpdateCompanyJsonData.ennm = _enterpriseName;
            objmBuzzAddUpdateCompanyJsonData.surl = serverurl;
            addNewContactRequestJson.data = objmBuzzAddUpdateCompanyJsonData;
            strJson = CreateRequestJson();
        }

        #endregion

        #region Private Methods

        private string CreateRequestJson()
        {

            string json = Utilities.SerializeJson<mBuzzAddUpdateCompanyJson>(addNewContactRequestJson);
            return Utilities.getRequestJson(json);
        }

        #endregion
    }

    #region Classes

    [DataContract]
    public class mBuzzAddUpdateCompanyJson : mBuzzRequestJson
    {
        public mBuzzAddUpdateCompanyJson()
        { }

        [DataMember]
        public mBuzzAddUpdateCompanyJsonData data { get; set; }
    }

    [DataContract]
    public class mBuzzAddUpdateCompanyJsonData
    {
        [DataMember]
        public string ennm { get; set; }

        [DataMember]
        public string surl { get; set; }
    }
    #endregion
}