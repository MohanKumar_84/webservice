﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientAdmin
{
    [DataContract]
    public class mBuzzRequestJson
    {
        public mBuzzRequestJson() { }

        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public string mrid { get; set; }

        [DataMember]
        public string type { get; set; }
    }
}