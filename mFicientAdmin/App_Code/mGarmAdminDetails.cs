﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace mFicientAdmin
{
    public class mGarmAdminDetails
    {
        List<mFicientCommonProcess.mGarmUserdetails> _usernames;
        long _pickUpTime, _sendDatetime;
        int _Days;
        long _expirationAfter;
        string  _category,_Devicetype,
            

         _message, _statusDescription;

        public mGarmAdminDetails()
        {
        }


      public  mGarmAdminDetails(string CompanyID,string deviceType)
      {
          this.CompanyId = CompanyID;
          ProcessGetUser(deviceType);
      }


      public mGarmAdminDetails(string category, string message, int days, long sendDatetime, long expirationAfter, List<mFicientCommonProcess.mGarmUserdetails> _lst)
      {
          this.Category = category;
          this.Message = message;
          this.SendDatetime = sendDatetime;
          this.ExpirationAfter = expirationAfter;
          this.Usernames = _lst;
          this.Days = days;
         // this.Devicetype = devicetype;
      }
      public void ProcessGetUser(string deviceType)
      {
          try
          {
              this.StatusCode = -1000;
              string strQuery = "select FIRST_NAME  +space(1)+ LAST_NAME + '(' + USER_NAME + ')' as CompleteName,USER_ID,MOBILE,EMAIL_ID,USER_NAME  from dbo.TBL_USER_DETAIL where COMPANY_ID=@COMPANY_ID;";
              if (deviceType != "") strQuery += "select * from tbl_registered_device where device_type='" + deviceType + "' and  COMPANY_ID=@COMPANY_ID;";
              else strQuery += "select * from tbl_registered_device where  COMPANY_ID=@COMPANY_ID;";
              SqlCommand sqlCommand = new SqlCommand(strQuery);
              sqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
              DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
              this.ResultDataSet = ds;
              if (ds.Tables[0].Rows.Count > 0)
              {
                  this.StatusCode = 0;
              }
          }
          catch (Exception ex)
          {

              this.StatusCode = -1000;
          }

      }
      public void SaveAdminMGramProcess()
      {
          try
          {
              insertMsgDtls();
          }
          catch
          {
              this.StatusCode = -1000;
              this.StatusDescription = "Internal server error.";
          }
      }

      void insertMsgDtls()
      {
          string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
              MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
          SqlTransaction transaction = null;
          SqlConnection con;
          MSSqlClient.SqlConnectionOpen(out con, strConnectionString);
          try
          {
              using (con)
              {
                  using (transaction = con.BeginTransaction())
                  {
                      mFicientCommonProcess.SendNotification.SaveMgramMessage(this.Usernames, con, transaction, this.Category, this.Message, this.SendDatetime, this.Days);

                      transaction.Commit();
                  }
              }

          }
          catch (SqlException e)
          {
              this.StatusCode = -1000;
              if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
              {
                  this.StatusDescription = "There was error in connection";
              }
              this.StatusDescription = "Internal server error.";
          }
          catch (Exception e)
          {
              this.StatusCode = -1000;
              if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
              {
                  this.StatusDescription = "There was error in connection";
              }
              this.StatusDescription = "Internal server error.";
          }
          finally
          {
              if (con != null)
              {
                  con.Dispose();
              }
              if (transaction != null)
              {
                  transaction.Dispose();
              }
          }
      }
      string getSqlQuery()
      {

          return @"INSERT INTO [TBL_MGRAM_MSG]
                       ([MGRAM_MSG_ID],[MSG_ID],[ENTERPRISE_ID]
                       ,[PICK_UP_TIME],[CATEGORY]
                       ,[USERNAME],[SEND_DATETIME]
                       ,[STATUS],[MESSAGE]
                       ,[EXPIRATION_DATETIME],[SENT_DATETIME],[IS_NOTIFICATION_SENT]
                      ,[DEVICE_TYPE])
                 VALUES
                       (@MGRAM_MSG_ID,@MSG_ID,@ENTERPRISE_ID
                       ,@PICK_UP_TIME,@CATEGORY
                       ,@USERNAME,@SEND_DATETIME
                       ,@STATUS,@MESSAGE
                       ,@EXPIRATION_DATETIME,@SENT_DATETIME,@IS_NOTIFICATION_SENT,
                       @DEVICE_TYPE)";
      }
      long getExpirationDateInTicks()
      {

          DateTime dtSendTime = DateTime.UtcNow;
          DateTime dtExpiryTime = DateTime.UtcNow;
          long lngSendTime = dtSendTime.Ticks;
          if (this.SendDatetime != 0)
          {
              dtSendTime = new DateTime(this.SendDatetime, DateTimeKind.Utc);
          }
         dtExpiryTime = dtSendTime.AddDays(this.Days);
          return dtExpiryTime.Ticks;
      }

      void savePushMsgOutboxDtls( string _enterpriseId, string _userName,string notificationType,string _devTokenID,string _devType,string _deviceID,SqlConnection con,SqlTransaction transaction)
      {
         SqlCommand cmd = new SqlCommand(getQuery(),con,transaction);
          cmd.CommandType = CommandType.Text;
          cmd.Parameters.AddWithValue("@PUSH_NOTIFICATION_ID", Utilities.GetMd5Hash(_enterpriseId + DateTime.UtcNow.Ticks + _userName));
          cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _enterpriseId);
          cmd.Parameters.AddWithValue("@BADGE_COUNT", 1);
          cmd.Parameters.AddWithValue("@USERNAME", _userName);
          cmd.Parameters.AddWithValue("@FROM_USER_FIRSTNAME", String.Empty);
          cmd.Parameters.AddWithValue("@SOURCE", 0);
          cmd.Parameters.AddWithValue("@NOTIFICATION_TYPE", notificationType);
          cmd.Parameters.AddWithValue("@PUSH_DATETIME", DateTime.UtcNow.Ticks);
          cmd.Parameters.AddWithValue("@DEVICE_TOKEN_ID", _devTokenID);
          cmd.Parameters.AddWithValue("@DEVICE_TYPE", _devType);
          cmd.Parameters.AddWithValue("@DEVICE_ID", _deviceID);
          cmd.Parameters.AddWithValue("@INFO", "");
          int iRowsEffected = cmd.ExecuteNonQuery();
      }

      string getQuery()
      {
          return @"INSERT INTO [TBL_PUSHMESSAGE_OUTBOX]
           ([PUSH_NOTIFICATION_ID],[ENTERPRISE_ID]
           ,[BADGE_COUNT],[USERNAME]
           ,[FROM_USER_FIRSTNAME],[SOURCE]
           ,[NOTIFICATION_TYPE],[PUSH_DATETIME],
            [DEVICE_TOKEN_ID],[DEVICE_TYPE],[DEVICE_ID],[INFO])
     VALUES
           (@PUSH_NOTIFICATION_ID,@ENTERPRISE_ID
           ,@BADGE_COUNT,@USERNAME
           ,@FROM_USER_FIRSTNAME,@SOURCE
           ,@NOTIFICATION_TYPE,
           @PUSH_DATETIME
           ,@DEVICE_TOKEN_ID
           ,@DEVICE_TYPE,@DEVICE_ID,@INFO);";

      }

      public DataSet ResultDataSet { get; set; }

      public string CompanyId { get; set; }

      
      public int StatusCode { get; set; }

      public string Category
      {
          get { return _category; }
          private set { _category = value; }
      }
      public string Message
      {
          get { return _message; }
          private set { _message = value; }
      }
      public long SendDatetime
      {
          get { return _sendDatetime; }
          private set { _sendDatetime = value; }
      }
      public int Days
      {
          get { return  _Days; }
          private set { _Days = value; }
      }



      public string Devicetype
      {
          get { return _Devicetype; }
          private set { _Devicetype = value; }
      }

        public long ExpirationAfter
        {
            get { return _expirationAfter; }
            private set { _expirationAfter = value; }
        }
        public List<mFicientCommonProcess.mGarmUserdetails> Usernames
        {
            get { return _usernames; }
            private set { _usernames = value; }
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
    }
    //public class mGarmUserdetails
    //{
    //    //public string Id { get; set; }
    //    public string Name { get; set; }
    //    public string CompanyId { get; set; }
    //    public string Devicetype { get; set; }
    //    public List<List<string>> DeviceIds { get; set; }

    //}
}