﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class ManageCpReseller : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.BindCountryDropDown(ddlcountry);
            AddresellerdetailsAdmin ad = new AddresellerdetailsAdmin();
            DataTable dt = ad.GetResellerList();
            if (dt.Rows.Count > 0)
            {
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
       
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {

            clearControls();
            enableDisablePanels("Add");
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
       

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidDetailsFormMode.Value != "")
            {
                //Edit
                GetResellerDetailsAdmin resellerdetail = new GetResellerDetailsAdmin();
                resellerdetail.ResellerDetails(hidDetailsFormMode.Value,ddlcountry.SelectedValue.ToString());

                DataTable dt = resellerdetail.ResultTable;
                if (dt.Rows.Count > 0)
                {
                    UpdateResellerDetails();
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                }
            }
            else
            {
                //new user
                SaveResellerDetails();
                //Utilities.Insertgdserver(g);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }

        }

        protected void SaveResellerDetails()
        {
            string strMessage = ValidatePanel();
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage);
            }
            else
            {
                AddresellerdetailsAdmin add = new AddresellerdetailsAdmin();
                //no longer require this page Changed on 3-7-2012
                //add.AddReseller(txtname.Text,txtemail.Text,txtnumber.Text,txtdiscount.Text,Convert.ToDateTime(txtdate.Text),txtlogin.Text,txtpassword.Text);
                if (add.StatusCode == 0)
                {
                    showStickyOrNormalMessage("Reseller Details added  successfully.", "sticky");
                    enableDisablePanels("");
                    hidPasswordEntered.Value = "";
                }
                else if (add.StatusCode == 1)
                {
                    showStickyOrNormalMessage(add.StatusDescription, "sticky");
                }
                else
                {
                    showStickyOrNormalMessage("Internal Error.Please try again", "sticky");
                }
            }
        }
        void showStickyOrNormalMessage(string message, string type)
        {
            if (type.ToLower() == "sticky")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingSticky", @"$.msg('" + message + "',{sticky:true});", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingNormal", @"$.msg('" + message + "');", true);
            }
        }
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        public void UpdateResellerDetails()
        {
            string strMessage = ValidatePanel();


            UpdateResellerDetailAdmin updatedetail = new UpdateResellerDetailAdmin();
            //updatedetail.UpdateReseller(txtname.Text,txtemail.Text,txtnumber.Text,txtdiscount.Text,Convert.ToDateTime(txtdate.Text),txtresellerid.Text,txtlogin.Text,txtpassword.Text);

            int intStatusCode = updatedetail.StatusCode;
            string strDescription = updatedetail.StatusDescription;

            if (intStatusCode == 0)
            {
                showStickyOrNormalMessage("Reseller details updated successfully", "sticky");
                enableDisablePanels("");
            }
            else
            {
                showStickyOrNormalMessage("Internal Error.Please try again", "sticky");
            }
        }

        public string ValidatePanel()
        {
            string strmessage = "";
            if (!Utilities.IsValidString(txtname.Text, true, true, true, " ", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Reseller Name.<br />";
            }


            if(!Utilities.IsValidEmail(txtemail.Text))
            {
                strmessage = strmessage + "Please Enter correct Email.<br />";
            }

            if (!Utilities.IsValidString(txtnumber.Text, true, true, true, "/.:?&@=", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Contact Number.<br />";
            }

            if (!Utilities.IsValidString(txtdiscount.Text, false, false, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Discount.<br />";
            }

            if (!Utilities.IsValidString(txtdate.Text, false, false, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Date of Creation.<br />";
            }

           
            if (!Utilities.IsValidString(txtresellerid.Text, false, false, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Resellerid.<br />";

            }
            if (!Utilities.IsValidString(txtlogin.Text, false, false, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Login Name.<br />";
            }
            if (!Utilities.IsValidString(txtpassword.Text, false, false, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Password.<br />";
            }

             return strmessage;
        }





        protected void btnCancel_Click(object sender, EventArgs e)
        {

            clearControls();
            enableDisablePanels("");
        }

         void enableDisablePanels(string mode)
        {

            if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Edit")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }
        protected void clearControls()
        {
            txtname.Text = "";
            txtemail.Text = "";
            txtdate.Text="";
            txtdiscount.Text = "";
            txtnumber.Text = "";
            txtresellerid.Text = "";
            txtlogin.Text = "";
            txtpassword.Text = "";
            hidDetailsFormMode.Value = "";
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            //Literal lit_Name = (Literal)item.FindControl("lit_LoginName");
            Label lit_id = (Label)item.FindControl("lblserId");

            if (e.CommandName == "Edit")
            {
                FillResellerDetails(lit_id.Text);
            }
        }


        protected void FillResellerDetails(string resellerId)
        {
            GetResellerDetailsAdmin resellerdetails = new GetResellerDetailsAdmin();
            resellerdetails.ResellerDetails(resellerId,ddlcountry.SelectedValue.ToString());
            DataTable dt = resellerdetails.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = resellerId;
                txtname.Text = Convert.ToString(dt.Rows[0]["RESELLER_NAME"]);
                txtemail.Text = Convert.ToString(dt.Rows[0]["EMAIL"]);
                txtnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
                txtdiscount.Text = Convert.ToString(dt.Rows[0]["DISCOUNT"]);
                txtdate.Text = Convert.ToString(dt.Rows[0]["CREATED_ON"]);
               // txtsellerid.Text = Convert.ToString(dt.Rows[0]["SELLER_ID"]);
                txtlogin.Text=Convert.ToString(dt.Rows[0]["LOGIN_NAME"]);
                txtpassword.Text = Convert.ToString(dt.Rows[0]["PASSWORD"]);
                ddlcountry.SelectedValue = Convert.ToString(dt.Rows[0]["COUNTRY"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }
        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    processEdit(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;

            }
        }

        void processEdit(RepeaterCommandEventArgs e)
        {
            hidDetailsFormMode.Value = "Edit";
            Label reseller_id = (Label)e.Item.FindControl("lblresellerid");
            FillResellerDetails(reseller_id.Text);
            enableDisablePanels("Edit");
        }



    }
}