﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageDefaultServers.aspx.cs" Inherits="mFicientAdmin.ManageDefaultServers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
<style type="text/css">
    .customFieldset
    {
        background-color:White; 
        background-image:none;   
    }
    .customFieldset .selector>span
    {
        width:300px;    
    }
    .serverSaveActionBtnContainer
    {
        padding-top:15px;    
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div >
        <asp:UpdatePanel ID="updManageServers" runat="server">
            <ContentTemplate>
                <div id="divManageMficientServers" class="g12 customFieldset">
                    <div class="g3">
                        <asp:Label ID="lblManageWSServers" runat="server" Text="Web Service Server:" CssClass="label labelFieldset"
                            AssociatedControlID="ddlWebserviceServers"></asp:Label>
                    </div>
                    <div class="g6">
                        <asp:DropDownList ID="ddlWebserviceServers" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="g3 serverSaveActionBtnContainer">
                        <asp:Button ID="btnWSServerSave" runat="server" Text="Save" OnClick="btnWSServerSave_Click"/>
                    </div>
                </div>
                <div id="divManageMbuzzServers" class="g12 customFieldset">
                    <div class="g3">
                        <asp:Label ID="lblManageMbuzzServers" runat="server" Text="Mbuzz Server:" CssClass="label labelFieldset"
                            AssociatedControlID="ddlMbuzzServers"></asp:Label>
                    </div>
                    <div class="g6">
                        <asp:DropDownList ID="ddlMbuzzServers" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="g3 serverSaveActionBtnContainer">
                        <asp:Button ID="btnMbuzzServerSave" runat="server" Text="Save" OnClick="btnMbuzzServerSave_Click"/>
                    </div>
                </div>
                <div id="divManageMpluginServers" class="g12 customFieldset">
                    <div class="g3">
                        <asp:Label ID="lblMpluginServer" runat="server" Text="mPlugin Server:" CssClass="label labelFieldset"
                            AssociatedControlID="ddlMpluginServer"></asp:Label>
                    </div>
                    <div class="g6">
                        <asp:DropDownList ID="ddlMpluginServer" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="g3 serverSaveActionBtnContainer">
                        <asp:Button ID="btnMpluginServerSave" runat="server" Text="Save" OnClick="btnMpluginServerSave_Click" />
                    </div>
                </div>
                <div>
                    <asp:HiddenField ID="hdi" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
        }
        function prm_endRequest() {
            hideWaitModal();
        }
    </script>
</asp:Content>
