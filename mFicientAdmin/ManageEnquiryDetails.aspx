﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageEnquiryDetails.aspx.cs" Inherits="mFicientAdmin.ManageEnquiryDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <div style="display: none; margin-left: 30px; border: arial 10px grey; width: 450px;">
                            <div style="float: left;">
                                <asp:DropDownList ID="ddlcountry" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div style="width: 200px">
                                <asp:DropDownList ID="ddlstatus" runat="server">
                                    <asp:ListItem>--Select Status--</asp:ListItem>
                                    <asp:ListItem Text="New" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Processing Sales Manager" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Processing Sales Executive" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Processing Reseller" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="Closed" Value="5"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:Button ID="bttnGet" runat="server" Width="80px" Text="Apply Filter" OnClick="bttnGet_Click" />
                        </div>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>ENQUIRY DETAILS</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display:none;">
                                                        Enquiry Id
                                                    </th>
                                                    <th >
                                                        COMPANY
                                                    </th>
                                                    <th>
                                                        BUSINESS TYPE
                                                    </th>
                                                    <th>STATUS</th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("ENQUIRY_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE_OF_COMPANY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                        OnClick="lnkok_Click"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("ENQUIRY_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE_OF_COMPANY") %>'></asp:Label>
                                                </td>
                                                 <td>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                        OnClick="lnkok_Click"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div id="divUserDtlsModal" class="modalPopUpDetails">
            <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMobileUserForm" Width="300px" Visible="false" runat="server">
                        <div style="margin-bottom: 20px; margin-top: 5px; display: none;">
                            <div style="float: left; margin-right: 18px;">
                                <asp:Label ID="label8" Text="<strong>Enquiry Id</strong>" runat="server"></asp:Label></div>
                            <div style="float: left;">
                                <asp:Label ID="lid" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-right: 32px; float: left; margin-bottom: 10px;">
                            <div style="float: left; margin-right: 66px; margin-top: 7px;">
                                <asp:Label ID="label2" Text="<strong>Company</strong>" runat="server"></asp:Label></div>
                            <div style="float: left; margin-top: 5px;">
                                <asp:Label ID="lname" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label3" Text="<strong>Country</strong>" runat="server"></asp:Label></div>
                            <div style="margin-top: 2px; margin-left: 118px;">
                                <asp:Label ID="lcountry" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label1" Text="<strong>Business Type</strong>" runat="server"></asp:Label></div>
                            <div style="margin-left: 119px; margin-top: 5px; ">
                                <asp:Label ID="ltype" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label4" Text="<strong>Contact Person</strong>" runat="server"></asp:Label></div>
                            <div style="margin-left: 120px; margin-top: 5px;">
                                <asp:Label ID="lperson" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label6" Text="<strong>Contact Number</strong>" runat="server"></asp:Label></div>
                            <div style="margin-left: 118px; margin-top: 5px;">
                                <asp:Label ID="lnumber" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label5" Text="<strong>Email</strong>" runat="server"></asp:Label></div>
                            <div style="margin-left: 119px; margin-top: 5px;">
                                <asp:Label ID="lemail" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label7" Text="<strong>Enquiry Date</strong>" runat="server"></asp:Label></div>
                            <div style="margin-left: 118px; margin-top: 5px;">
                                <asp:Label ID="Enquirydate" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div style="margin-bottom: 15px;">
                            <div style="float: left; margin-top: 2px;">
                                <asp:Label ID="label9" Text="<strong>Enquiry Status</strong>" runat="server"></asp:Label></div>
                            <div style="margin-left: 118px; margin-top: 5px;">
                                <asp:Label ID="lstatus" runat="server"></asp:Label>
                            </div>
                        </div>
                        <section>
                                    <div id="Remarkdiv" runat="server" style="margin-bottom: 20px; margin-top: 6px;">
                                        <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 2px;">
                                            <asp:Label ID="label10" Text="<strong>Remark</strong>" runat="server"></asp:Label></div>
                                        <div style="margin-left: 80px; margin-top: 5px; margin-right: 50px;">
                                            <asp:Label ID="lblR" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    </div>
                                </section>
                        <%--<fieldset>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label8" Text="<strong>Enquiry Id</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label11" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lid" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label1" Text="<strong>Company</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label13" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lname" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label2" Text="<strong>Country</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label14" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lcountry" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label3" Text="<strong>Business Type</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label15" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="ltype" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label4" Text="<strong>Contact Person</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label16" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lperson" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label5" Text="<strong>Contact Number</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label17" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lnumber" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label6" Text=" <strong>Email</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label18" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lemail" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label7" Text="<strong>Enquiry Date</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label19" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="Enquirydate" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="label9" Text="<strong>Enquiry Status</strong>" runat="server"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label20" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lstatus" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div id="Remarkdiv" runat="server" style="margin-bottom: 20px; margin-top: 6px;">
                                        <div class="column1">
                                            <asp:Label ID="label10" Text="<strong>Remark</strong>" runat="server"></asp:Label>
                                        </div>
                                        <div class="column2">
                                            <asp:Label ID="Label21" runat="server" Text=":"></asp:Label>
                                        </div>
                                        <div class="column3">
                                            <asp:Label ID="lblR" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>--%>
                        <div style="position: relative; left: 33px; bottom: 10px;">
                            <asp:LinkButton ID="lnkAddNewUser" runat="server" Text="Process" CssClass="repeaterLink fr"
                                PostBackUrl="~/CompanyRegistration.aspx"></asp:LinkButton>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>
