﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class ManageEnquiryDetailsSP : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!IsPostBack)
            {
                lnkreseller.Visible = false;
                lnksales.Visible = false;
                BindResellerDropDown(ddlreseller);
                BindSalesDropDown(ddlsalesp);
                BindRemarksDropDown(ddlremarks);
                LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
                Label enquiryId = (Label)rptUserDetails.FindControl("lblid");
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/

               if (string.IsNullOrEmpty(hfsValue))
                {
                    Response.Redirect(@"Default.aspx");
                }
                string Role = strRole.ToUpper();
                if (Role == "A")
                {
                    ltlUserType.Text = "Admin";
                    SMstatus.Visible = false;
                    ChangePass.PostBackUrl = "~/ChangePassword.aspx";
                }
                else if (Role == "R")
                {
                    divresellerlogin.Visible = true;
                    resellerdiv.Visible = false;
                    salesdiv.Visible = false;
                    ltlUserType.Text = "Reseller";
                    SMstatus.Visible = false;
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                    AddServerDetails ad = new AddServerDetails();
                    DataTable dt = ad.EnquiryListR(strUserName);
                    if (dt.Rows.Count > 0)
                    {
                        lblHeaderInfo.Text = "<h1>Enquiry Details</h1>";
                        rptUserDetails.Visible = true;
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        rptUserDetails.Visible = false;
                        lblHeaderInfo.Text = "<h1>no Reseller's enquiries added yet</h1>";
                    }
                }
                else if (Role == "S")
                {
                    allotedreseller.Visible = false;
                    divresellerlogin.Visible = false;
                    resellerdiv.Visible = true;
                    salesdiv.Visible = false;
                    ltlUserType.Text = "Sales Executive";
                    SMstatus.Visible = false;
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                    AddServerDetails ad = new AddServerDetails();
                    DataTable dt = ad.EnquiryListS(strUserName, "S");
                    if (dt.Rows.Count > 0)
                    {
                        lblHeaderInfo.Text = "<h1>Enquiry Details</h1>";
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        rptUserDetails.Visible = false;
                        lblHeaderInfo.Text = "<h1>no sales executive's enquiries added yet</h1>";
                    }
                }
                else if (Role == "SM")
                {
                    BindListNew();
                    divresellerlogin.Visible = false;
                    salesdiv.Visible = true;
                    resellerdiv.Visible = false;
                    ltlUserType.Text = "Sales Manager";
                    SMstatus.Visible = true;
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                  }

                else if (Role == "SUPP")
                {
                    ltlUserType.Text = "Support";
                    SMstatus.Visible = false;
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                    AddServerDetails ad = new AddServerDetails();
                    DataTable dt = ad.GetEnquiryDetails();
                    if (dt.Rows.Count > 0)
                    {
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        rptUserDetails.Visible = false;
                        lblHeaderInfo.Text = "<h1>no support person's enquiries added yet</h1>";
                    }
                }

                else if (Role == "ACC")
                {
                    ltlUserType.Text = "Accounts";
                    SMstatus.Visible = false;
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                    AddServerDetails ad = new AddServerDetails();
                    DataTable dt = ad.GetEnquiryDetails();
                    if (dt.Rows.Count > 0)
                    {
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        rptUserDetails.Visible = false;
                        lblHeaderInfo.Text = "<h1>no accounts person's enquiries added yet</h1>";
                    }
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
            }
       
        }
        void BindRemarksDropDown(DropDownList _ddlDropdownId)
        {
            EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
            get.GetRemarks();
            DataTable dt = get.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataValueField = "REMARK_CODE";
            _ddlDropdownId.DataTextField = "REMARK";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("--Select-- ", "-1"));
        }
         void BindResellerDropDown(DropDownList _ddlDropdownId)
        {
            GetResellerDetailsAdmin get = new GetResellerDetailsAdmin();
            get.GetReseller();
            DataTable dt = get.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataValueField = "RESELLER_ID";
            _ddlDropdownId.DataTextField = "LOGIN_NAME";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("--Select-- ", "-1"));
        }

         void BindSalesDropDown(DropDownList _ddlDropdownId)
         {
             GetResellerDetailsAdmin get = new GetResellerDetailsAdmin();
             get.GetList("S");
             DataTable dt = get.ResultTable;
             _ddlDropdownId.DataSource = dt;
             _ddlDropdownId.DataValueField = "LOGIN_ID";
             _ddlDropdownId.DataTextField = "USER_NAME";
             _ddlDropdownId.DataBind();
             _ddlDropdownId.Items.Insert(0, new ListItem("--Select-- ", "-1"));
         }
        void BindListNew()
        {
            Sdiv.Visible = true;
            alloteddiv.Visible = false;
            AddServerDetails get = new AddServerDetails();
            DataTable objDt=get.EnquiryListSMStatus(strUserName, "");
            if (objDt.Rows.Count > 0)
            {
                rptUserDetails.Visible = true;
                lblHeaderInfo.Text = "<h1>New Enquiry Details</h1>";
                rptUserDetails.DataSource = objDt;
                rptUserDetails.DataBind();
            }
            else if (objDt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no new enquiries added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        public void BindListProcessed()
        {
            Sdiv.Visible = false;
            alloteddiv.Visible = true;
            AddServerDetails get = new AddServerDetails();
            string strId = get.GetMgrId(strUserName, "SM");
            DataTable objDt = get.EnquiryListSMStatus(strUserName, strId);
            if (objDt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Processed Enquiry Details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = objDt;
                rptUserDetails.DataBind();
            }
            else if (objDt.Rows.Count == 0)
            {
                rptUserDetails.Visible = false;
                lblHeaderInfo.Text = "<h1>no processed enquiries added yet</h1>";
            }
        }

        protected void radListSelection_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (radListSelection.SelectedValue == "1")
                {
                    BindListNew();
                }
                else if (radListSelection.SelectedValue == "2")
                {
                    BindListProcessed();
                }
            }

        protected void lnksales_Click(object sender, EventArgs e)
        {
            Sdiv.Visible = true;
            alloteddiv.Visible = true;
            lnksales.Visible = false;
        }

        protected void lnkreseller_Click(object sender, EventArgs e)
        {
            Rdiv.Visible = true;
            lnkreseller.Visible = false;
        }
       
        protected void lnkok_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Details</strong>", "430", true,false);
            enabledisablepanels("Info");
            upUserDtlsModal.Update();
         }

        protected void bttnprocess_Click(object sender, EventArgs e)
        {
            if (rdlist1.SelectedValue == "1")
            {
                bttnprocess.PostBackUrl = "CompanyRegistration.aspx";
                EnquiryDetailsAdmin update = new EnquiryDetailsAdmin();
                update.UpdateStatus("3", hdi.Value);
                Utilities.showMessage("Company has been created and enquiries has been closed", this.Page, "second script", DIALOG_TYPE.Info);
            }

            else if (rdlist1.SelectedValue == "2")
            {
                Utilities.showModalPopup("divRemark", this.Page, "<strong>Remarks</strong>", "500", true,false);
                UpdatePanelRemark.Update();
                pnlremark.Visible = true;
                EnquiryDetailsAdmin update = new EnquiryDetailsAdmin();
                update.UpdateStatus("4", hdi.Value); 
            }
        }

        protected void rdlist1_SelectedIndexChanged(object sender, EventArgs e)
          {
            if (rdlist1.SelectedValue == "1")
            {
                bttnprocess.PostBackUrl = "CompanyRegistration.aspx";
                EnquiryDetailsAdmin update = new EnquiryDetailsAdmin();
                update.UpdateStatus("3", hdi.Value);
             }

            else if (rdlist1.SelectedValue == "2")
            {
                    Utilities.showModalPopup("divRemark", this.Page, "<strong>Remarks</strong>", "350", true,false);
                    UpdatePanelRemark.Update();
                    pnlremark.Visible = true;
                    EnquiryDetailsAdmin update = new EnquiryDetailsAdmin();
                    update.UpdateStatus("4", hdi.Value);
            }
        }

        protected void lnkSave_Click(object sender, EventArgs e)
        {
          EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
          //DataTable dt=  get.GetRemarkcode(ddlremarks.SelectedValue);
          //string strRemarkcode = Convert.ToString(dt.Rows[0]["REMARK_CODE"]);
          get.UpdateEnquiryDetails(ddlremarks.SelectedItem.Value, "4", hdi.Value);
           Utilities.showMessage("Remarks have been added successfully", this.Page, "second script", DIALOG_TYPE.Info);
           Utilities.closeModalPopUp("divRemark", this.Page);
           Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
        }

        protected void lnkallotREseller_Click(object sender, EventArgs e)
        {
           SaveDetailsR();
        }

         protected void lnkspallote_Click(object sender, EventArgs e)
        {
            if (ddlsalesp.SelectedValue == "-1")
            {
                Utilities.showMessage("Please select atleast one Sales Person to allote", this.Page, "second script", DIALOG_TYPE.Warning);
            }
            else
            {
                SaveDetailsSP();
            }
        }

        public void SaveDetailsR()
        {
             if (ddlreseller.SelectedValue != "-1")
            {
                EnquiryDetailsAdmin enq = new EnquiryDetailsAdmin();
                //DataTable dt = enq.GetRDetails(ddlreseller.SelectedValue.ToString());
                //string Rid = dt.Rows[0]["RESELLER_ID"].ToString();
                string SalesId=  enq.GetSalesExecutiveId(strUserName, "S");
                enq.UpdateDetailsR("2", SalesId, ddlreseller.SelectedItem.Value, hdi.Value);
                Utilities.showMessage("Reseller has been alloted", this.Page, "second script", DIALOG_TYPE.Info);
                lstatus.Text = "Processing(Sales Person)";
                Rdiv.Visible = false;
                lnkreseller.Visible = true;
                allotedreseller.Visible = true;
                Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                lallotedreseller.Text = Convert.ToString(ddlreseller.SelectedItem);
            }
            else
            {
                Utilities.showMessage("Please select atleast one Reseller to allot", this.Page, "second script", DIALOG_TYPE.Warning);           
            }
        }

       public void SaveDetailsSP()
        {
            if (ddlsalesp.SelectedValue != "-1")
            {
                EnquiryDetailsAdmin enq = new EnquiryDetailsAdmin();
                //DataTable dt = enq.GetUserDetails(ddlsalesp.SelectedValue.ToString(), "S");
                //string loginId = dt.Rows[0]["LOGIN_ID"].ToString();
                string MgrId=  enq.GetId(strUserName,"SM");
                enq.UpdateDetailsSP("1", MgrId, ddlsalesp.SelectedItem.Value, hdi.Value);
                Utilities.showMessage("Sales Person has been alloted", this.Page, "second script", DIALOG_TYPE.Info);
                BindListNew();
                Sdiv.Visible = false;
                alloteddiv.Visible = true;
                lnksales.Visible = true;
                Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                lallotedSP.Text = Convert.ToString(ddlsalesp.SelectedItem);
            }

            else if(ddlsalesp.SelectedValue=="-1")
            {
                Utilities.showMessage("Please select atleast one Sales Person to allote", this.Page, "second script", DIALOG_TYPE.Warning);              
            }

        }
      protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lblid");
            if (e.CommandName == "Info")
            {
                FillEnquiryDetails(lit_id.Text);
                hdi.Value = lit_id.Text;
                //hidden field set
            }
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
            }
        }

        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        protected void FillEnquiryDetails(string EnquiryId)
        {
            GetServerDetailsAdmin enquirydetails = new GetServerDetailsAdmin();
            enquirydetails.EnquiryDetails(EnquiryId);
            DataTable dt = enquirydetails.ResultTable;
            string strEnquiryid = Convert.ToString(dt.Rows[0]["ENQUIRY_ID"]);
            if (Convert.ToString(dt.Rows[0]["SALES_MNGR_ID"]) == "" || Convert.ToString(dt.Rows[0]["STATUS"]) == "0")
            {
                lstatus.Text = "New";
            }
            else if (Convert.ToString(dt.Rows[0]["SALES_MNGR_ID"]) != "" && Convert.ToString(dt.Rows[0]["SALES_ID"]) == "" || Convert.ToString(dt.Rows[0]["STATUS"]) == "1")
            {
                EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
                string LoginName = get.GetUname(EnquiryId, "S");
                lallotedSP.Text = LoginName;
                lstatus.Text = "Processing(Sales Manager)";
            }
            else if (Convert.ToString(dt.Rows[0]["SALES_MNGR_ID"]) != "" && Convert.ToString(dt.Rows[0]["SALES_ID"]) != "" && Convert.ToString(dt.Rows[0]["RESELLER_ID"]) == ""
                || Convert.ToString(dt.Rows[0]["STATUS"]) == "2")
            {
                EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
                string LoginName = get.GetUname(EnquiryId, "S");
                lallotedSP.Text = LoginName;
                lstatus.Text = "Processing(Sales Person)";
            }
            else if (Convert.ToString(dt.Rows[0]["STATUS"]) == "3")
            {
                EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
                string LoginName = get.GetUname(EnquiryId, "S");
                lallotedSP.Text = LoginName;
                lstatus.Text = "Processing (Reseller)";
            }

            else if (Convert.ToString(dt.Rows[0]["STATUS"]) == "4")
            {
                EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
                string LoginName = get.GetUname(EnquiryId, "S");
                lallotedSP.Text = LoginName;
                lstatus.Text = "Successfully processed";
            }

            else if (Convert.ToString(dt.Rows[0]["STATUS"]) == "5")
            {
                lstatus.Text = "Processing failed";
            }
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = EnquiryId;
                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
                lcountry.Text = Convert.ToString(dt.Rows[0]["COUNTRY_NAME"]);
                ltype.Text = Convert.ToString(dt.Rows[0]["TYPE_OF_COMPANY"]);
                lperson.Text = Convert.ToString(dt.Rows[0]["CONTACT_PERSON"]);
                lnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
                lemail.Text = Convert.ToString(dt.Rows[0]["CONTACT_EMAIL"]);
                DateTime EnqDate = new DateTime(Convert.ToInt64(dt.Rows[0]["ENQUIRY_DATETIME"]));
                Enquirydate.Text = EnqDate.ToShortDateString();
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }
        void Info(RepeaterCommandEventArgs e)
        {
            Label enquiry_id = (Label)e.Item.FindControl("lblid");
            hdi.Value = enquiry_id.Text;
            FillEnquiryDetails(enquiry_id.Text);
        }

        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            pnlMobileUserForm.Visible = false;
            pnlRepeaterBox.Visible = true;
        }
    }
}