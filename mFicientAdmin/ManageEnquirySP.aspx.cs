﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class ManageEnquirySP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {




            EnquiryDetailsAdmin enqdet = new EnquiryDetailsAdmin();
           // string ResellerName = enqdet.GetAllocatedR("A");
          //  lstlbl.Text = ResellerName;


           
          
            if (!Page.IsPostBack)
            {
                Utilities.BindResellerddl(ddlR,"P");
            }
           

            AddServerDetails ad = new AddServerDetails();
           // DataTable dt = ad.GetEnquiryList();
            //if (dt.Rows.Count > 0)
            //{
            //    rptUserDetails.DataSource = dt;
            //    rptUserDetails.DataBind();
            //}

            if (HttpContext.Current.Items.Contains("hfs"))
            {

                hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
            }
            else
            {


                if (Page.PreviousPage != null)
                {
                    string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                    prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                    ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");

                    hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;


                }
            }      
        }


        protected void btnok_Click(object sender, EventArgs e)
        {
            SaveDetailsR();
        }


        protected void ddlSP_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void lnkok_Click(object sender, EventArgs e)
        {
            //clearControls();
            enabledisablepanels("Info");
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);

        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lblid");

            if (e.CommandName == "Info")
            {
                FillEnquiryDetails(lit_id.Text);
            }
        }

        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;

            }

        }

        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
                // pnlUserForm.Visible = true;

            }
            else
            {
                // clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        protected void FillEnquiryDetails(string CompanyName)
        {
            GetServerDetailsAdmin enquirydetails = new GetServerDetailsAdmin();
            enquirydetails.EnquiryDetails(CompanyName);
            DataTable dt = enquirydetails.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = CompanyName;
              //  lid.Text = Convert.ToString(dt.Rows[0]["ENQUIRY_ID"]);

                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
                lcountry.Text = Convert.ToString(dt.Rows[0]["COUNTRY_NAME"]);
                ltype.Text = Convert.ToString(dt.Rows[0]["TYPE_OF_COMPANY"]);
                lperson.Text = Convert.ToString(dt.Rows[0]["CONTACT_PERSON"]);
                lnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
                lemail.Text = Convert.ToString(dt.Rows[0]["CONTACT_EMAIL"]);
                Enquirydate.Text = Convert.ToString(dt.Rows[0]["ENQUIRY_DATETIME"]);


            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        void Info(RepeaterCommandEventArgs e)
        {
            Label enquiry_id = (Label)e.Item.FindControl("lblid");
            FillEnquiryDetails(enquiry_id.Text);
        }


        public void SaveDetailsR()
        {

            //if (ddlR.SelectedIndex != ddlR.Items.IndexOf(ddlR.Items.FindByText("--Please Select--")))
            //{
            if (ddlR.SelectedValue != "-1")
            {
                EnquiryDetailsAdmin enq = new EnquiryDetailsAdmin();
                DataTable dt = enq.GetRDetails(ddlR.SelectedValue.ToString());
                string Rid = dt.Rows[0]["RESELLER_ID"].ToString();
              //  enq.UpdateDetailsR("A", Rid,hdi.Value);

                showStickyOrNormalMessage("Reseller has been allocated", "sticky");
            }

            else
            {
                showStickyOrNormalMessage("Please select atleast one Reseller to allocate", "sticky");
            }
            
        }

        void showStickyOrNormalMessage(string message, string type)
        {
            if (type.ToLower() == "sticky")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingSticky", @"$.msg('" + message + "',{sticky:true});", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingNormal", @"$.msg('" + message + "');", true);
            }
        }
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        protected void txtcompany_TextChanged(object sender, EventArgs e)
        {

        }

        protected void expectedwf_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtresellerid_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        protected void hidDetailsFormMode_ValueChanged(object sender, EventArgs e)
        {

        }

        protected void lb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            pnlMobileUserForm.Visible = false;
            pnlRepeaterBox.Visible = true;
        }

        protected void lstbind_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}