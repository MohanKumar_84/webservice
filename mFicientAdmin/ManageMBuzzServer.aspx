﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true" CodeBehind="ManageMBuzzServer.aspx.cs" Inherits="mFicientAdmin.ManageMBuzzServer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">

    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>MBuzz SERVER DETAILS</h1>"></asp:Label>
                                        </div>

                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server"  OnItemCommand="rptUserDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th >
                                                        MBUZZ SERVER
                                                    </th>
                                                    
                                                    <th >
                                                     SERVER IP
                                                    </th>
                                                    <th >
                                                        PORT NUMBER
                                                    </th>
                                                    <th >
                                                       ENABLED
                                                    </th>
                                                   
                                                   <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td >
                                                    <asp:Label ID="lblsername" runat="server" Text='<%#Eval("MBUZZ_SERVER_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblserId" runat="server" Text='<%#Eval("MBUZZ_SERVER_ID") %>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblip" runat="server" Text='<%# Eval("SERVER_IP") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%# Eval("PORT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Lblenabled" runat="server" Text='<%# Eval("ENABLED") %>'></asp:Label>
                                                </td>
                                             
                                                <td>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete"  CssClass="repeaterLink"  CommandName="Delete"></asp:LinkButton>
                                                    
                                                </td>


                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td >
                                                    <asp:Label ID="lblsername" runat="server" Text='<%#Eval("MBUZZ_SERVER_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblserId" runat="server" Text='<%#Eval("MBUZZ_SERVER_ID") %>' Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblip" runat="server" Text='<%# Eval("SERVER_IP") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%# Eval("PORT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Lblenabled" runat="server" Text='<%# Eval("ENABLED") %>'></asp:Label>
                                                </td>
                                               
                                                <td>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete"  CssClass="repeaterLink"  CommandName="Delete"></asp:LinkButton>
                                                    
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        

           <div class="modalPopUpDetails">
            <div id="divdelete" style="display: none">
                <asp:UpdatePanel runat="server" ID="DeletePanel" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="Div2" style="width: 100%;">
                                <div style="float: left; width: 100%;">
                                    <asp:Panel ID="pnlremark" Visible="true" runat="server">
                                        <div style="margin-bottom: 20px;margin-left: 10px;">
                                            <div style="float:left; margin-right: 10px; margin-top: 10px; ">
                                                <asp:Label ID="label14" Text="<strong>Are you sure want to delete the server?</strong>" runat="server"></asp:Label></div>
                                            <br />
                                            <br />
                                        </div>
                                       <div class="SubProcborderDiv">
                                        <div class="SubProcBtnDiv" align="center">
                                            <asp:Button ID="bttnok" runat="server" Width="60px" Text="Ok" OnClick="bttnok_Click"></asp:Button>
                                            <asp:Button ID="bttncancel" runat="server" Width="60px" Text="Cancel" OnClick="bttncancel_Click">
                                            </asp:Button>
                                        </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <div class="modalPopUpDetails">
            <div id="divNotDelete" style="display: none">
                <asp:UpdatePanel runat="server" ID="NotDeletePnl" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="Div3" style="width: 100%;">
                                <div style="float: left; width: 100%;">
                                    <asp:Panel ID="Panel1" Visible="true" runat="server">
                                        <div style="margin-bottom: 20px; margin-top: 10px; margin-left: 15px;">
                                            <div style="font-family:Microsoft Sans Serif;">
                                                <asp:Label ID="label5" Text="This server cannot be deleted because presently client is using it.Please allot other server to client first and then delete it."
                                                    runat='server'></asp:Label>
                                            </div>
                                            </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>


    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            // $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            // $("input").uniform();
            hideWaitModal();
        }
        </script>
</asp:Content>
