﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageMplugginServer.aspx.cs" Inherits="mFicientAdmin.ManageMplugginServer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>MPluggin SERVER DETAILS</h1>"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="add" CssClass="repeaterLink fr"
                                                OnClick="lnkAddNewMobileUser_Click"></asp:LinkButton>
                                        </div>

                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server"  OnItemCommand="rptUserDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th >
                                                         SERVER
                                                    </th>
                                                    
                                                    <th style="display:none;" >
                                                      IP
                                                    </th>
                                                    <th >
                                                        PORT NUMBER
                                                    </th>
                                                    <th>URL</th>
                                                   <th>
                                                      
                                                    </th>
                                                    <th></th>
                                                  
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td style="display:none;">
                                            <asp:Label ID="lblid" runat="server" Text='<%#Eval("MP_SERVER_ID") %>' ></asp:Label>
                                            </td>
                                                <td >
                                                    <asp:Label ID="lblsername" runat="server" Text='<%#Eval("MP_SERVER_NAME") %>'></asp:Label>
                                                    
                                                </td>
                                                <td style="display:none;">
                                                    <asp:Label ID="lblip" runat="server" Text='<%# Eval("MP_SERVER_IP_ADDRESS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblport" runat="server" Text='<%# Eval("MP_SERVER_PORT") %>'></asp:Label>
                                                </td>
                                                 <td style="display:none;">
                                                    <asp:Label ID="lblenabled" runat="server" Text='<%# Eval("IS_ENABLED") %>'></asp:Label>
                                                </td>
                                                <td>
                                                 <asp:Label ID="lblurl" runat="server" Text='<%# Eval("MP_HTTP_URL") %>'></asp:Label>
                                                </td>
                                             <td style="width:30px;">
                                                    <asp:LinkButton ID="lnkedit" runat="server" Text="Edit"  CssClass="repeaterLink"  CommandName="Edit" ></asp:LinkButton>

                                                </td>
                                                <td style="width:30px;">
                                                    <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete"  CssClass="repeaterLink"  CommandName="Delete" ></asp:LinkButton>
                                                    
                                                </td>

                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                               <td style="display:none;">
                                            <asp:Label ID="lblid" runat="server" Text='<%#Eval("MP_SERVER_ID") %>' ></asp:Label>
                                            </td>
                                                <td >
                                                    <asp:Label ID="lblsername" runat="server" Text='<%#Eval("MP_SERVER_NAME") %>'></asp:Label>
                                                    
                                                </td>
                                                <td style="display:none;">
                                                    <asp:Label ID="lblip" runat="server" Text='<%# Eval("MP_SERVER_IP_ADDRESS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblport" runat="server" Text='<%# Eval("MP_SERVER_PORT") %>'></asp:Label>
                                                </td>
                                                
                                                 <td  style="display:none;">
                                                    <asp:Label ID="lblenabled" runat="server" Text='<%# Eval("IS_ENABLED") %>'></asp:Label>
                                                </td>
                                                <td>
                                                 <asp:Label ID="lblurl" runat="server" Text='<%# Eval("MP_HTTP_URL") %>'></asp:Label>
                                                </td>
                                               
                                             <td  style="width:30px;" >
                                                    <asp:LinkButton ID="lnkedit" runat="server" Text="Edit"  CssClass="repeaterLink"  CommandName="Edit" ></asp:LinkButton>
                                                    
                                                </td>
                                                 <td style="width:30px;">
                                                    <asp:LinkButton ID="lnkdelete" runat="server" Text="Delete"  CssClass="repeaterLink"  CommandName="Delete"  ></asp:LinkButton>
                                                    
                                                </td>   
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 85%;">
                                <asp:Panel ID="pnlMobileUserForm" Visible="true" runat="server">
                                    <fieldset>
                                        <div style="margin-bottom: 40px;">
                                            <div style="float: left; margin-right: 40px;">
                                                <asp:Label ID="label1" Text="<strong>Name</strong>" Style="position: relative; top: 4px;"
                                                    runat="server"></asp:Label></div>
                                            <div style="float: left; width: 175px;">
                                                <asp:TextBox ID="txtname" runat="server"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 80px; display:none;">
                                            <div style="float: left; margin-right: 61px;">
                                                <asp:Label ID="label2" Text="<strong>IP </strong>" runat="server" Style="position: relative;
                                                    top: 4px;"></asp:Label></div>
                                            <div style="float: left; width: 175px;">
                                                <asp:TextBox ID="txtip" runat="server"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div id="portdiv" runat="server" style="margin-bottom: 80px;">
                                            <div style="float: left; margin-right: 47px;">
                                                <asp:Label ID="label3" Text="<strong>Port</strong>" runat="server" Style="position: relative;
                                                    top: 4px;"></asp:Label></div>
                                            <div style="float: left; width: 175px;margin-top:4px;">
                                                <asp:Label ID="lport" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 10px; margin-top: 15px;">
                                            <div style="float: left; margin-right: 49px;">
                                                <asp:Label ID="label6" Text="<strong>URL</strong>" runat="server" Style="position: relative;
                                                    top: 4px;"></asp:Label></div>
                                            <div style="float: left; margin-top: 5px;">
                                                <asp:Label ID="Label7" runat="server" Text="http:/https:"></asp:Label></div>
                                            <div style="float: right; width: 100px;">
                                                <asp:TextBox ID="txturl" runat="server"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-top: 10px;">
                                            <div style="float: left; margin-right: 29px;margin-top:3px;">
                                                <asp:Label ID="label4" runat="server" Text="<strong>Enabled</strong>" Style="position: relative;
                                                    top: 4px;"></asp:Label></div>
                                            <div style="float: left; margin-top: 5px;">
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" /></div>
                                        </div>
                                    </fieldset>
                                    <section id="buttons">
                                 <div class="SubProcborderDiv" style=" margin-left:20px;">
                                        <div class="SubProcBtnDiv" align="center">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                </div>
                                </div>
                                </section>
                                </asp:Panel>
                            </div>
                        </div>
                        <asp:HiddenField ID="hidUserId" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="modalPopUpDetails">
            <div id="divdelete" style="display: none">
                <asp:UpdatePanel runat="server" ID="DeletePanel" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="Div2" style="width: 100%;">
                                <div style="float: left; width: 100%;">
                                    <asp:Panel ID="pnlremark" Visible="true" runat="server">
                                        <div style="margin-bottom: 10px; margin-top: 10px; margin-left: 10px;">
                                            <div style="float: left; margin-right: 10px;">
                                                <asp:Label ID="label14" Text="<strong>Are you sure want to delete the server?</strong>"
                                                    runat="server"></asp:Label></div>
                                            <br />
                                            <br />
                                        </div>
                                        <div class="SubProcborderDiv">
                                            <div class="SubProcBtnDiv" align="center">
                                                <asp:Button ID="bttnok" runat="server" Width="60px" Text="Ok" OnClick="bttnok_Click">
                                                </asp:Button>
                                                <asp:Button ID="bttncancel" runat="server" Width="60px" Text="Cancel" OnClick="bttncancel_Click">
                                                </asp:Button>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="modalPopUpDetails">
            <div id="divNotDelete" style="display: none">
                <asp:UpdatePanel runat="server" ID="NotDeletePnl" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="Div3" style="width: 100%;">
                                <div style="float: left; width: 100%;">
                                    <asp:Panel ID="Panel1" Visible="true" runat="server">
                                        <div style="margin-bottom: 20px; margin-top: 10px; margin-left: 15px;">
                                            <div>
                                                <asp:Label ID="label5" Text="This server cannot be deleted because presently client is using it.Please allot other server to client first and then delete it."
                                                    runat="server"></asp:Label></div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            // $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>
