﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;


namespace mFicientAdmin
{
    public partial class ManageMplugginServer : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!Page.IsPostBack)
            {
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }

                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                BindReapeter();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }
            
        }
        public void BindReapeter()
        {
            MPluginnServerAdmin get = new MPluginnServerAdmin();
            DataTable dt = get.GetServerList();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>mPlugin Server Details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>There are no details of mPlugin server added yet</h1>";
            }
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            clearControls();
            portdiv.Visible = false;
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Add mPlugin Server Details</strong>", "350", true,false);
            upUserDtlsModal.Update();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidDetailsFormMode.Value != "")
            {
                //Edit
                MPluginnServerAdmin serverdetail = new MPluginnServerAdmin();
                serverdetail.MplugginServerDetails(hidDetailsFormMode.Value);
                DataTable dt = serverdetail.ResultTable;
                if (dt.Rows.Count > 0)
                {
                    string serverid = Convert.ToString(dt.Rows[0]["MP_SERVER_ID"]);
                    UpdateServerDetails(serverid);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                }
            }
            else
            {
                //new user
                SaveServerDetails();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }
            BindReapeter();
            pnlRepeaterBox.Visible = true;
        }

        protected void SaveServerDetails()
        {
            string strMessage = ValidatePanelAdd();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }
            else
            {
                MPluginnServerAdmin add = new MPluginnServerAdmin();
                add.AddServerDetails(txtname.Text, Utilities.GetMd5Hash(txtname.Text + DateTime.Now.Ticks.ToString()),txtip.Text,"443", chkIsActive.Checked,txturl.Text);
                hidPasswordEntered.Value = "";
                Utilities.showMessage("MpluginServer Details have been added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                enableDisablePanels("");
                Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                }
            }

        void enableDisablePanels(string mode)
        {
            if (mode == "Add")
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Edit")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
        }
        protected void clearControls()
        {
            txtname.Text = "";
            txtip.Text = "";
           // lport.Text = "";
            txturl.Text = "";
            hidDetailsFormMode.Value = "";
        }

        public void UpdateServerDetails(string serverid)
        {
            string strMessage = ValidatePanelAdd();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }
            else
            {
                MPluginnServerAdmin update = new MPluginnServerAdmin();
                update.UpdateServer(serverid, txtname.Text, txtip.Text, "443", chkIsActive.Checked,txturl.Text);
                int intStatusCode = update.StatusCode;
                string strDescription = update.StatusDescription;
                if (intStatusCode == 0)
                {
                    Utilities.showMessage("mPluginServer Details have been Updated successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    enableDisablePanels("");
                    Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                }
                else
                {
                    Utilities.showMessage("Internal Error,Please Try Again", this.Page, "second script", DIALOG_TYPE.Info);
                    Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                }
            }
        }

        public string ValidatePanelAdd()
        {
            string strmessage = "";
            if (String.IsNullOrEmpty(txtname.Text))
            {
                strmessage = strmessage + "Please enter server name<br />";
            }

            else if (!Utilities.IsValidString(txtname.Text, true, true, false,".", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct server name<br />";
            }

            if (String.IsNullOrEmpty(txturl.Text))
            {
                strmessage = strmessage + "Please enter server URL<br />";
            }

            else if (!IsValidUrl(txturl.Text))
            {
                strmessage = strmessage + "Please enter correct server URL<br />";
            }
            return strmessage;
        }
      
        public static bool IsValidUrl(string _Url)
        {
            if (!string.IsNullOrEmpty(_Url))
            {
                string strRegex = @"^([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$";
                Regex re = new Regex(strRegex);
                if (re.IsMatch(_Url))
                    return (true);
                else
                    return (false);
            }
            else
                return false;
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    processEdit(e);
                    Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Edit mPluginServer Details</strong>", "350", true,false);
                    enableDisablePanels("Edit");
                    upUserDtlsModal.Update();
                    break;
                case "Delete":
                    processDelete(e);
                    break;
            }
        }

        protected void bttnok_Click(object sender, EventArgs e)
        {
            MPluginnServerAdmin delete = new MPluginnServerAdmin();
            delete.DeleteServer(hdi.Value);
            Utilities.showMessage("mPluginServer Details have been deleted successfully", this.Page, "second script", DIALOG_TYPE.Info);
            Utilities.closeModalPopUp("divdelete", this.Page);
            BindReapeter();
        }

        protected void bttncancel_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divdelete", this.Page);
        }
        void processDelete(RepeaterCommandEventArgs e)
        {
            hidDetailsFormMode.Value = "Edit";
            Label server_id = (Label)e.Item.FindControl("lblid");
            DeleteServer(server_id.Text);
            hdi.Value = server_id.Text;
        }

        void processEdit(RepeaterCommandEventArgs e)
        {
            portdiv.Visible = true;
            hidDetailsFormMode.Value = "Edit";
            Label server_id = (Label)e.Item.FindControl("lblid");
            FillServerDetails(server_id.Text);
            hdi.Value = server_id.Text;
            enableDisablePanels("Edit");
        }
       
        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lblid");
            if (e.CommandName == "Edit")
            {
                FillServerDetails(lit_id.Text);
            }
        }

        protected void DeleteServer(string serverid)
        {
           MPluginnServerAdmin get = new MPluginnServerAdmin();
           string CompanyId=get.GetCompanyId(serverid);
           if (!String.IsNullOrEmpty(CompanyId) )
           {
               Utilities.showModalPopup("divNotDelete", this.Page, "<strong><img src=images/dialog_error.png runat=server />&nbsp;Error</strong>", "300", true,false);
               NotDeletePnl.Update();
           }
          else
           {
               Utilities.showModalPopup("divdelete", this.Page, "<strong><img src=images/dialog_info.png runat=server />&nbsp;Delete</strong>", "300", false,false);
               DeletePanel.Update();
           }
        }
        protected void FillServerDetails(string serverid)
        {
            MPluginnServerAdmin get = new MPluginnServerAdmin();
            get.MplugginServerDetails(serverid);
            DataTable dt = get.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = serverid;
                txtname.Text = Convert.ToString(dt.Rows[0]["MP_SERVER_NAME"]);
                txtip.Text = Convert.ToString(dt.Rows[0]["MP_SERVER_IP_ADDRESS"]);
                lport.Text = Convert.ToString(dt.Rows[0]["MP_SERVER_PORT"]);
                chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["IS_ENABLED"]);
                txturl.Text = Convert.ToString(dt.Rows[0]["MP_HTTP_URL"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
    }
}