﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class ManageTrialPlans : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!Page.IsPostBack)
            {
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");

                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                BindRepeater();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }

            if (Page.IsPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform(); $(\"input\").uniform();", true);
            }
        }

        void BindRepeater()
        {
           ManageTrialPlanAdmin get = new ManageTrialPlanAdmin();
           DataTable dt= get.GetTrialPlanList();
           if (dt.Rows.Count > 0)
           {
               lblHeaderInfo.Text = "<h1>Trial Plan Details</h1>";
               rptUserDetails.Visible = true;
               rptUserDetails.DataSource = dt;
               rptUserDetails.DataBind();
           }
           else if (dt.Rows.Count == 0)
           {
               lblHeaderInfo.Text = "<h1>There are no details of Trial plans added yet</h1>";
               rptUserDetails.Visible = false;
           }
        }

        void EnableDisable(string mode)
        {
            if (mode == "Add")
            {
                txt.Visible = true;
                lbl.Visible = false;
            }
            else if (mode == "Edit")
            {
                lbl.Visible = true;
                txt.Visible = false;
            }
        }

        void enableDisablePanels(string mode)
        {
            if (mode == "Add")
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Edit")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        protected void clearControls()
        {
            txtcode.Text = "";
            Plan.Text = "";
            txtplanname.Text = "";
            txtworkflow.Text = "";
            txtusers.Text = "";
            txtmsgperday.Text = "";
            txtmsgpermonth.Text = "";
            txtmonth.Text = "";
            hidDetailsFormMode.Value = "";
        }

        public string ValidatePanel()
        {
            string strmessage = "";
            if (String.IsNullOrEmpty(txtcode.Text))
            {
                strmessage = strmessage + "Please enter plan code<br />";
            }
            else if (!Utilities.IsValidString(txtcode.Text, true, true, true, " ", 1, 100, false, false))
            {
                strmessage = strmessage + "Please enter correct plan code<br />";
            }

            if (String.IsNullOrEmpty(txtplanname.Text))
            {
                strmessage = strmessage + "Please enter plan name<br />";
            }

            else if (!Utilities.IsValidString(txtplanname.Text, true, true, false, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct plan name<br />";
            }

            if (String.IsNullOrEmpty(txtworkflow.Text))
            {
                strmessage = strmessage + "Please enter workflow<br />";
            }
            else if (!Utilities.IsValidString(txtworkflow.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct Workflow<br />";
            }

            if (String.IsNullOrEmpty(txtmonth.Text))
            {
                strmessage = strmessage + "Please enter number of months<br />";
            }
            else if (!Utilities.IsValidString(txtmonth.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of months<br />";
            }

            if (String.IsNullOrEmpty(txtusers.Text))
            {
                strmessage = strmessage + "Please enter number of users<br />";
            }
            else if (!Utilities.IsValidString(txtusers.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of users<br />";
            }

            if (String.IsNullOrEmpty(txtmsgperday.Text))
            {
                strmessage = strmessage + "Please enter number of messages per day<br />";
            }

            else  if (!Utilities.IsValidString(txtmsgperday.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per day<br />";
            }


            if (String.IsNullOrEmpty(txtmsgpermonth.Text))
            {
                strmessage = strmessage + "Please enter number of messages per month<br />";
            }

            else if(!Utilities.IsValidString(txtmsgpermonth.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per month<br />";
            }

            return strmessage;
        }

        public string ValidatePanelEdit()
        {
            string strmessage = "";

            if (String.IsNullOrEmpty(txtplanname.Text))
            {
                strmessage = strmessage + "Please enter plan name<br />";
            }

            else if (!Utilities.IsValidString(txtplanname.Text, true, true, false, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct plan name<br />";
            }

            if (String.IsNullOrEmpty(txtworkflow.Text))
            {
                strmessage = strmessage + "Please enter workflow<br />";
            }
            else if (!Utilities.IsValidString(txtworkflow.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct Workflow<br />";
            }

            if (String.IsNullOrEmpty(txtmonth.Text))
            {
                strmessage = strmessage + "Please enter number of months<br />";
            }
            else if (!Utilities.IsValidString(txtmonth.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of months<br />";
            }

            if (String.IsNullOrEmpty(txtusers.Text))
            {
                strmessage = strmessage + "Please enter number of users<br />";
            }
            else if (!Utilities.IsValidString(txtusers.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of users<br />";
            }

            if (String.IsNullOrEmpty(txtmsgperday.Text))
            {
                strmessage = strmessage + "Please enter number of messages per day<br />";
            }

            else if (!Utilities.IsValidString(txtmsgperday.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per day<br />";
            }

            if (String.IsNullOrEmpty(txtmsgpermonth.Text))
            {
                strmessage = strmessage + "Please enter number of messages per month<br />";
            }

            else if (!Utilities.IsValidString(txtmsgpermonth.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per month<br />";
            }

            return strmessage;
        }
        protected void lnkedit_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Edit Trial Plan Details</strong>", "500", true,false);
            enableDisablePanels("Edit");
            upUserDtlsModal.Update();
        }
        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Add Trial Plan Details</strong>", "500", true,false);
            enableDisablePanels("Add");
            upUserDtlsModal.Update();
            EnableDisable("Add");
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidDetailsFormMode.Value != "")
            {
                //Edit
                ManageTrialPlanAdmin get = new ManageTrialPlanAdmin();
                get.TrialPlanDetails(hidDetailsFormMode.Value);
                DataTable dt = get.ResultTable;
                if (dt.Rows.Count > 0)
                {
                    UpdateTrialPlanDetails(hidDetailsFormMode.Value);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                }
            }
            else
            {
                //new user
                SaveTrialPlanDetails();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }

            BindRepeater();
            pnlRepeaterBox.Visible = true;
        }


        protected void UpdateTrialPlanDetails(string PlanId)
        {
            EnableDisable("Edit");
            string strMessage = ValidatePanelEdit();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }
            else
            {
                ManageTrialPlanAdmin update = new ManageTrialPlanAdmin();
                update.UpdateTrialPlan(PlanId, txtplanname.Text, Convert.ToInt32(txtworkflow.Text), Convert.ToInt32(txtusers.Text), Convert.ToInt32(txtmsgperday.Text), Convert.ToInt32(txtmsgpermonth.Text),Convert.ToInt32(txtmonth.Text));
                int intStatusCode = update.StatusCode;
                string strDescription = update.StatusDescription;
                if (intStatusCode == 0)
                {
                    Utilities.showMessage("Trial Plan Details have been Updated successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    enableDisablePanels("");
                    Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                }
                else
                {
                    Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Info);
                }
            }
        }

        protected void SaveTrialPlanDetails()
        {
            string strMessage = ValidatePanel();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }

            else
            {
                ManageTrialPlanAdmin add = new ManageTrialPlanAdmin();
                add.AddTrialPlan(txtcode.Text, Utilities.GetMd5Hash(txtplanname.Text + DateTime.Now.Ticks.ToString()), txtplanname.Text, Convert.ToInt32(txtworkflow.Text), Convert.ToInt32(txtusers.Text), Convert.ToInt32(txtmsgperday.Text), Convert.ToInt32(txtmsgpermonth.Text), Convert.ToInt32(txtmonth.Text));
                enableDisablePanels("");
                hidPasswordEntered.Value = "";
                Utilities.showMessage("Trial Plan Details have been added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                enableDisablePanels("");
                Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
            }
                BindRepeater();
                EnableDisable("Add");
            }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearControls();
            enableDisablePanels("");
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;

            Label lit_id = (Label)item.FindControl("lblplanid");

            if (e.CommandName == "Edit")
            {
                FillPlanDetails(lit_id.Text);
            }
        }

        protected void FillPlanDetails(string planid)
        {
            EnableDisable("Edit");
            ManageTrialPlanAdmin get = new ManageTrialPlanAdmin();
            get.TrialPlanDetails(planid);
            DataTable dt = get.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = planid;

                Plan.Text = Convert.ToString(dt.Rows[0]["PLAN_CODE"]);
                txtplanname.Text = Convert.ToString(dt.Rows[0]["PLAN_NAME"]);
                txtworkflow.Text = Convert.ToString(dt.Rows[0]["MAX_WORKFLOW"]);
                txtusers.Text = Convert.ToString(dt.Rows[0]["MAX_USER"]);
                txtmsgperday.Text = Convert.ToString(dt.Rows[0]["PUSHMESSAGE_PERDAY"]);
                txtmsgpermonth.Text = Convert.ToString(dt.Rows[0]["PUSHMESSAGE_PERMONTH"]);
                txtmonth.Text = Convert.ToString(dt.Rows[0]["MONTH"]);

            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }
        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }
         protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
         {
             switch (e.CommandName)
             {
                 case "Edit":
                     processEdit(e);
                     ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                     break;

             }
         }
         void processEdit(RepeaterCommandEventArgs e)
         {
             hidDetailsFormMode.Value = "Edit";
             Label plan_id = (Label)e.Item.FindControl("lblplanid");
             FillPlanDetails(plan_id.Text);
             enableDisablePanels("Edit");
         }
    }
}