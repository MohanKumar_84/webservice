﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="PaymentApproval.aspx.cs" Inherits="mFicientAdmin.PaymentApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Plan Purchase Details</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptPlanPurchaseDtls" runat="server">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        CompanyId
                                                    </th>
                                                    <th style="display: none;">
                                                        ResellerId
                                                    </th>
                                                    <th>
                                                        Company Name
                                                    </th>
                                                    <th>
                                                        Plan Name
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th>
                                                        Reseller Name
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td>
                                                    <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblResellerId" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblResellerName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDetails" runat="server" Text="Details" CssClass="repeaterLink"
                                                        CommandName="Details"></asp:LinkButton>&nbsp;
                                                    <asp:LinkButton ID="lnkApprove" runat="server" Text="Approve" CssClass="repeaterLink"
                                                        CommandName="Approve"></asp:LinkButton>&nbsp;
                                                    <asp:LinkButton ID="lnkPaid" runat="server" Text="Paid" CssClass="repeaterLink" CommandName="Paid"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td>
                                                    <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblResellerId" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblResellerName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDetails" runat="server" Text="Details" CssClass="repeaterLink"
                                                        CommandName="Details"></asp:LinkButton>&nbsp;
                                                    <asp:LinkButton ID="lnkApprove" runat="server" Text="Approve" CssClass="repeaterLink"
                                                        CommandName="Approve"></asp:LinkButton>&nbsp;
                                                    <asp:LinkButton ID="lnkPaid" runat="server" Text="Paid" CssClass="repeaterLink" CommandName="Paid"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                            </asp:Panel>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div>
            <asp:HiddenField ID="hfs" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>
