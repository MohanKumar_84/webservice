﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class PlanOrders : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        private string strUpgradedPrice, strPlanCode, strCompId;
        enum MODAL_POP_UP_TYPE
        {
            ADD_USER,
            RENEW,
            PURCHASE,
            UPGRADE_DOWNGRADE,
            PALN_DETAILS,
            COMPANY_DETAILS,
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (Page.IsPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }
            BindOrderRepeater();
        }

        #region Events

        protected void rptPlanOrders_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    hidPriceAmountPayable.Value = "";//this is used for storing the final payable amount after calculation
                    //using javascript.so setting it every time when the details is shown.
                    Label lblCompID = (Label)e.Item.FindControl("lblCompID");
                    Label lblRequestType = (Label)e.Item.FindControl("lblRequestType");
                    Label lblPlanCode = (Label)e.Item.FindControl("lblPlanCode");
                    LinkButton lnkOrder = (LinkButton)e.Item.FindControl("lnkOrder");
                    ShowPlanDetails(lblCompID.Text);
                    strPlanCode = lblPlanCode.Text;
                    strCompId = lblCompID.Text;
                    if (e.CommandName.ToLower() == "plandetail")
                    {
                        Utilities.showModalPopup("divPlanDetailBox", this.Page, "Details", "350", true, false);
                        upPlanDtlsModal.Update();
                    }

                    else if (e.CommandName.ToLower() == "planorder")
                    {

                        switch (lblRequestType.Text)
                        {
                            case "1"://renew
                                showPlanOrderDetail(PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW, strPlanCode, strCompId);
                                //showPlanOrderDetail(lblRequestType.Text, strPlanCode);
                                break;

                            case "2"://user add
                                // AddUser();
                                //Utilities.showModalPopup("divAddUserModal", this.Page, "AddUser", "400", true);
                                showPlanOrderDetail(PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED, strPlanCode, strCompId);
                                break;

                            case "3"://plan upgrade
                                //lblUpgradedPlan.Text = lblPlanCode.Text;
                                //CalculateUpgradedAmount(out strUpgradedPrice);
                                //decimal priceDiff = Convert.ToDecimal(strUpgradedPrice) - Convert.ToDecimal(lblUserChargePerMonth.Text);
                                //lblAmount.Text = (Convert.ToDecimal(lblMonths.Text) * priceDiff * Convert.ToDecimal(lblMaxUsers.Text)).ToString();
                                //Utilities.showModalPopup("divPlanUpgradeModal", this.Page, "Upgrade Plan", "400", true);
                                showPlanOrderDetail(PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE, strPlanCode, strCompId);
                                break;

                            case "4"://plan downgrade
                                //lblUpgradedPlan.Text = lblPlanCode.Text;
                                //CalculateUpgradedAmount(out strUpgradedPrice);
                                //decimal diff = Convert.ToDecimal(lblUserChargePerMonth.Text) - Convert.ToDecimal(strUpgradedPrice);
                                //lblAmount.Text = (Convert.ToDecimal(lblMonths.Text) * diff * Convert.ToDecimal(lblMaxUsers.Text)).ToString();
                                //Utilities.showModalPopup("divPlanUpgradeModal", this.Page, "Downgrade Plan", "400", true);
                                showPlanOrderDetail(PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE, strPlanCode, strCompId);
                                break;
                            case "5"://plan purchase
                                //lblUpgradedPlan.Text = lblPlanCode.Text;
                                //CalculateUpgradedAmount(out strUpgradedPrice);
                                showPlanOrderDetail(PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE, strPlanCode, strCompId);
                                break;
                        }
                    }
                    else if (e.CommandName.ToLower() == "companydetail".ToLower())
                    {
                        processShowCompanyDetail(e);
                    }
                }
                catch (Exception ex)
                {
                    Utilities.showMessage(ex.Message, this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
        }
        void processShowCompanyDetail(RepeaterCommandEventArgs e)
        {
            Label lblCompanyId = (Label)e.Item.FindControl("lblCompID");
            ucCompanyDetails.processShowCompanyDetail(lblCompanyId.Text);
            Utilities.showModalPopup("divCompanyDtlsModal", this.Page, "Details", "350", true, false);
            updCompanyDetails.Update();
        }
        protected void txtAddUsers_TextChanged(object sender, EventArgs e)
        {
            CalculatePayableAmount();
        }

        protected void ddlPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateUpgradedAmount(out strUpgradedPrice);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int status = 0;
            try
            {
                // AddCurrentPlanPurchaseHistory("Add User", txtAddUsers.Text, lblPrice.Text, lblAllowableDiscount.Text,lblPayableAmount.Text,lblUserChargePerMonth.Text, out status);
                if (status == 0)
                {
                    Utilities.showMessage("user added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("User addition unsuccessful", this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
            catch
            {
                Utilities.showMessage("User addition unsuccessful", this.Page, "second script", DIALOG_TYPE.Warning);
            }
        }

        protected void btnUpgrade_Click(object sender, EventArgs e)
        {
        }

        protected void btnRenew_Click(object sender, EventArgs e)
        {
            //int status;
            //try
            //{
            //    //AddCurrentPlanPurchaseHistory("Renew Plan", lblMaxUsers.Text, "0", "0", lblRenewOrgPrice.Text, lblUserChargePerMonth.Text, out status);
            //    //Utilities.closeModalPopUp("divRenewPlanModal", this.Page);
            //    //if (status == 0)
            //    //{
            //    //    showStickyOrNormalMessage("Plan Renewed successfully", "sticky");
            //    //}
            //    //else
            //    //{
            //    //    showStickyOrNormalMessage("Plan has not been renewed.Please try again", "Sticky");
            //    //}
            //}
            //catch
            //{
            //    showStickyOrNormalMessage("Plan has not been renewed.Please try again", "Sticky");
            //}

            DataTable dtblPlanDetails = getPlanDtls(lblRenewPlan_Code.Text, Convert.ToInt32(Convert.ToDouble(lblRenewMnths.Text)), lblRenewChrgPerMnthUnit.Text);
            if (dtblPlanDetails != null && dtblPlanDetails.Rows.Count > 0)
            {
                DataRow detailsRow = dtblPlanDetails.Rows[0];
                string strPayableAmount;
                if (hidPriceAmountPayable.Value != "" && hidPriceAmountPayable.Value != "0")
                {
                    strPayableAmount = hidPriceAmountPayable.Value;
                }
                else
                {
                    strPayableAmount = lblPurchaseOrgPrice.Text;
                }
                int iError =
                    AddCurrentPlanPurchaseHistory(
                    ((Button)sender).CommandArgument.Split(';')[0],
                    lblRenewChargeperMnth.Text,
                    lblRenewChrgPerMnthUnit.Text,
                    lblRenewUsers.Text,
                    ((int)detailsRow["MAX_WORKFLOW"]).ToString(),
                    lblRenewMnths.Text,
                    lblRenewOrgPrice.Text,
                    txtRenewDiscountAmt.Text,
                    strPayableAmount,
                    ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW).ToString(),
                    lblRenewPlan_Code.Text,
                    (string)detailsRow["PLAN_NAME"]);

                if (iError == 0)
                {
                    Utilities.closeModalPopUp("divRenewPlanBox", this.Page);
                    BindOrderRepeater();
                }
                else
                {
                    showAlert("Internal error.", "divRenewPlan", this.Page);
                }
            }
            else
            {
                showAlert("Internal error.", "divRenewPlan", this.Page);
            }
        }

        protected void btnPurchase_Click(object sender, EventArgs e)
        {
            if (txtPurchaseDiscountAmt.Text == "")
            {
                Utilities.showMessage("Please enter valid discount amount.", this.Page, "Error", DIALOG_TYPE.Error);
                return;
            }
            DataTable dtblPlanDetails = getPlanDtls(lblPurchasePlan_Code.Text, Convert.ToInt32(Convert.ToDouble(lblPurchaseMnths.Text)), lblPurchaseChrgPerMnthUnit.Text);
            if (dtblPlanDetails != null && dtblPlanDetails.Rows.Count > 0)
            {
                DataRow detailsRow = dtblPlanDetails.Rows[0];
                string strPayableAmount;
                if (hidPriceAmountPayable.Value != "" && hidPriceAmountPayable.Value != "0")
                {
                    strPayableAmount = hidPriceAmountPayable.Value;
                }
                else
                {
                    strPayableAmount = lblPurchaseOrgPrice.Text;
                }
                //int iError = AddCurrentPlanPurchaseHistory(((Button)sender).CommandArgument, lblPurchaseChargeperMnth.Text, lblPurchaseChrgPerMnthUnit.Text, lblPurchaseUsers.Text, ((int)detailsRow["MAX_WORKFLOW"]).ToString()
                //,lblPurchaseMnths.Text, lblPurchaseOrgPrice.Text, txtPurchaseDiscountAmt.Text, lblPurchasePayableAmount.Text, ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString(),
                //lblPurchasePlan_Code.Text, (string)detailsRow["PLAN_NAME"]);//29/10/2012
                int iError =
                    AddCurrentPlanPurchaseHistory(
                    ((Button)sender).CommandArgument.Split(';')[0],
                    lblPurchaseChargeperMnth.Text,
                    lblPurchaseChrgPerMnthUnit.Text,
                    lblPurchaseUsers.Text,
                    ((int)detailsRow["MAX_WORKFLOW"]).ToString()
                     , lblPurchaseMnths.Text,
                     lblPurchaseOrgPrice.Text,
                     txtPurchaseDiscountAmt.Text,
                     strPayableAmount,
                     ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString(),
                     lblPurchasePlan_Code.Text,
                     (string)detailsRow["PLAN_NAME"]
                     );
                if (iError == 0)
                {
                    Utilities.closeModalPopUp("divPurchasePlanBox", this.Page);
                    BindOrderRepeater();
                }
                else
                {
                    showAlert("Internal error.", "divPurchasePlan", this.Page);
                }
            }
            else
            {
                showAlert("Internal error.", "divPurchasePlan", this.Page);
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            DataTable dtblPlanDetails = getPlanDtls(lblAddUserPlan_Code.Text, Convert.ToInt32(Convert.ToDouble(lblAddUserMnths.Text)), lblAddUserChrgPerMnthUnit.Text);
            if (dtblPlanDetails != null && dtblPlanDetails.Rows.Count > 0)
            {
                DataRow detailsRow = dtblPlanDetails.Rows[0];
                string strPayableAmount;
                if (hidPriceAmountPayable.Value != "" && hidPriceAmountPayable.Value != "0")
                {
                    strPayableAmount = hidPriceAmountPayable.Value;
                }
                else
                {
                    strPayableAmount = lblPurchaseOrgPrice.Text;
                }
                int iError = AddCurrentPlanPurchaseHistory(((Button)sender).CommandArgument.Split(';')[0], lblAddUserChargeperMnth.Text, lblAddUserChrgPerMnthUnit.Text, lblAddUserUsers.Text, ((int)detailsRow["MAX_WORKFLOW"]).ToString()
                     , lblAddUserMnths.Text, lblAddUserOrgPrice.Text, txtAddUserDiscountAmt.Text, strPayableAmount, ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED).ToString(),
                     lblAddUserPlan_Code.Text, (string)detailsRow["PLAN_NAME"]);
                if (iError == 0)
                {
                    Utilities.closeModalPopUp("divAddUserPlanBox", this.Page);
                    BindOrderRepeater();
                }
                else
                {
                    showAlert("Internal error.", "divAddUserPlan", this.Page);
                }
            }
            else
            {
                showAlert("Internal error.", "divAddUserPlan", this.Page);
            }
        }
        #endregion

        #region Miscellaneous

        private void BindOrderRepeater()
        {
            AddServerDetails addServerDetails = new AddServerDetails();
            DataTable dtOrderList = addServerDetails.GetOrderList(USER_TYPE.Reseller);//USER_TYPE.Reseller added on 4/10/2012//New Function created
            if (dtOrderList.Rows.Count > 0)
            {
                rptPlanOrders.DataSource = dtOrderList;
                rptPlanOrders.DataBind();
                lblHeaderInfo.Text = "<h1>Plan Orders</h1>";
            }
            else
            {
                dtOrderList = null;
                rptPlanOrders.DataSource = dtOrderList;
                rptPlanOrders.DataBind();
                lblHeaderInfo.Text = "<h1>no plan orders added yet</h1>";
            }
        }
        void showAlert(string message, string divIdToShowMsg, System.Web.UI.Page page)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        private void ShowPlanDetails(string _compID)
        {
            //changed on 1/10/2012.We had to get the trial plan details as well which was not possible through the current process
            //CurrentPlanDetail currentPlan = new CurrentPlanDetail(_compID, true,false);
            //currentPlan.Process();
            //DataTable dtCurrentPlanDtl = currentPlan.PlanDetails;
            //if (dtCurrentPlanDtl != null)
            //{
            //    if (dtCurrentPlanDtl.TableName == "Plan")
            //    {
            //        lblMaxWorkflow.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["MAX_WORKFLOW"]);
            //        lblUserChargePerMonth.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["USER_CHARGE_PM"]);
            //        lblChargeType.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["CHARGE_TYPE"]);
            //        lblMaxUsers.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["MAX_USER"]);

            //        GetPlanList objPlanList = new GetPlanList(strPlanCode);
            //        objPlanList.Process();
            //        DataTable dtblPlanDtls = objPlanList.PlanDetails;
            //        if (dtblPlanDtls != null)
            //        {
            //            if (dtblPlanDtls.Rows.Count > 0)
            //            {
            //                lblPlanName.Text = Convert.ToString(dtblPlanDtls.Rows[0]["PLAN_NAME"]);
            //            }
            //        }
            //        DateTime purchaseDate = new DateTime(Convert.ToInt64(dtCurrentPlanDtl.Rows[0]["PURCHASE_DATE"].ToString()));
            //        lblPurchaseDate.Text = purchaseDate.ToLongDateString();
            //        DateTime validPeriod = purchaseDate.AddMonths(Convert.ToInt32(dtCurrentPlanDtl.Rows[0]["VALIDITY"].ToString()));
            //        TimeSpan time = validPeriod.Subtract(DateTime.Now);
            //        string strValidMonths = (time.Days / 30.4368499).ToString("N2");
            //        lblValidity.Text = validPeriod.ToLongDateString();
            //    }
            //}

            GetCompanyDetailsWithCurrentPlan objGetCompanyDetail = new GetCompanyDetailsWithCurrentPlan(_compID);
            objGetCompanyDetail.Process();
            if (objGetCompanyDetail.StatusCode == 0)
            {
                DataTable dtblCompanyDetail = objGetCompanyDetail.CompanyDetail;
                // DataTable dtCurrentPlanDtl = currentPlan.PlanDetails;
                if (dtblCompanyDetail != null && dtblCompanyDetail.Rows.Count > 0)
                {
                    lblPlanName.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["PLAN_NAME"]);
                    lblMaxWorkflow.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["MAX_WORKFLOW"]);
                    lblUserChargePerMonth.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["USER_CHARGE_PM"]);
                    lblChargeType.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["CHARGE_TYPE"]);
                    lblMaxUsers.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["MAX_USER"]);
                    DateTime purchaseDate = new DateTime(Convert.ToInt64(dtblCompanyDetail.Rows[0]["PURCHASE_DATE"].ToString()));
                    lblPurchaseDate.Text = purchaseDate.ToLongDateString();
                    DateTime validPeriod = purchaseDate.AddMonths(Convert.ToInt32(Math.Ceiling(Convert.ToDouble(dtblCompanyDetail.Rows[0]["VALIDITY"].ToString()))));
                    TimeSpan time = validPeriod.Subtract(DateTime.Now);
                    string strValidMonths = (time.Days / 30.4368499).ToString("N2");
                    lblValidity.Text = validPeriod.ToLongDateString();
                }
            }
            else
            {
                //showStickyOrNormalMessage("Internal server error", "sticky");
                throw new Exception("Internal server error");
            }
        }

        private void RenewPlan(string requestType, string planCode)
        {

        }
        void showPlanOrderDetail(PLAN_RENEW_OR_CHANGE_REQUESTS requestType, string planCode, string companyId)
        {
            switch (requestType)
            {
                case PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW:
                    showRenewRequestDetailAndSetCmdArgOfDenyBtn(((int)PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW).ToString(), planCode, companyId);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE:
                    showPlanUpgradeDowngradeReqDetailAndSetCmdArgOfDenyBtn(((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE).ToString(), planCode, companyId);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE:
                    showPlanPurchaseReqDetailAndSetCmdArgOfDenyBtn(((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString(), planCode, companyId);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE:
                    showPlanUpgradeDowngradeReqDetailAndSetCmdArgOfDenyBtn(((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE).ToString(), planCode, companyId);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED:
                    showAddUserRequestDetailAndSetCmdArgOfDenyBtn(((int)PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED).ToString(), planCode, companyId);
                    break;
            }
        }
        void showRenewRequestDetailAndSetCmdArgOfDenyBtn(string requestType, string planCode, string companyId)
        {
            try
            {
                DataTable dtOrderDetails = getRequestOrderDetail(requestType, planCode, companyId);
                DataTable dtblPlanDetail = null;
                CURRENCY_TYPE eCurrType = CURRENCY_TYPE.INR;
                lblRenewPlan_Code.Text = planCode;
                string strNoOfUsers = "", strNoOfMonths = "", strCurrencyType = "", strCurrencyUnit = "";
                if (dtOrderDetails != null && dtOrderDetails.Rows.Count > 0)
                {
                    dtblPlanDetail = getPlanDtls(planCode, Convert.ToInt32(dtOrderDetails.Rows[0]["NUMBER_MONTH"]), Convert.ToString(dtOrderDetails.Rows[0]["CURRENCY_TYPE"]));
                    strNoOfUsers = dtOrderDetails.Rows[0]["MOBILE_USERS"].ToString();
                    strNoOfMonths = dtOrderDetails.Rows[0]["NUMBER_MONTH"].ToString();
                    strCurrencyType = dtOrderDetails.Rows[0]["CURRENCY_TYPE"].ToString();
                }
                lblRenewMnths.Text = strNoOfMonths;
                lblRenewUsers.Text = strNoOfUsers;
                if (dtblPlanDetail != null && dtblPlanDetail.Rows.Count > 0)
                {
                    if (strCurrencyType != "" && strCurrencyType.ToLower() == "inr")
                    {
                        lblRenewChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                        strCurrencyUnit = "INR";
                        lblRenewChrgPerMnthUnit.Text = strCurrencyUnit;
                        eCurrType = CURRENCY_TYPE.INR;
                    }
                    else if (strCurrencyType != "" && strCurrencyType.ToLower() == "usd")
                    {
                        lblRenewChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                        strCurrencyUnit = "USD";
                        lblRenewChrgPerMnthUnit.Text = strCurrencyUnit;
                        eCurrType = CURRENCY_TYPE.USD;
                    }
                }
                //if (dtblDiscounts != null && dtblDiscounts.Rows.Count > 0)
                //{
                //    if (strNoOfMonths != "")
                //    {
                //        lblRenewDiscount.Text = getDiscount(dtblDiscounts, strNoOfMonths).ToString();
                //    }
                //    else
                //    {
                //        lblRenewDiscount.Text = "0";
                //    }
                //}
                string strPayableAmount = "";
                if (strNoOfUsers != "" && strNoOfMonths != "" && lblRenewChargeperMnth.Text != "")
                {
                    lblRenewOrgPrice.Text = 
                        calculatePrice(strNoOfUsers, strNoOfMonths, lblRenewChargeperMnth.Text, "",eCurrType, out strPayableAmount);
                    lblRenewOrgPrcUnit.Text = strCurrencyUnit;
                }
                else
                {
                    lblRenewOrgPrice.Text = "0";
                }
                //if (strNoOfUsers != "" && strNoOfMonths != "" && lblRenewChargeperMnth.Text != "" && lblRenewDiscount.Text != "")
                //{
                //    lblRenewDiscountPrice.Text = calculatePrice(strNoOfUsers, strNoOfMonths, lblRenewChargeperMnth.Text, lblRenewDiscount.Text, out strPayableAmount);
                //    lblRenewDiscountUnit.Text = strCurrencyUnit;
                //}

                lblRenewPayableAmount.Text = strPayableAmount;
                lblRenewPaybleAmntUnit.Text = strCurrencyUnit;
                txtAddUserDiscountAmt.Text = "0";
                btnRenew.CommandArgument = companyId;
                setCommandArgumentsOfDenyButtons(requestType, planCode, companyId, btnRenewDenyRequest, MODAL_POP_UP_TYPE.RENEW);
                upRenewPlanModal.Update();
                Utilities.showModalPopup("divRenewPlanBox", this.Page, "Renew Plan", "350", true, false);

            }
            catch 
            {
                //Utilities.showMessage(ex.Message, this.Page, "Error Message", DIALOG_TYPE.Error);
                Utilities.showMessage("Internal server error.", this.Page, "Error Message", DIALOG_TYPE.Error);
            }
        }
        void showPlanPurchaseReqDetailAndSetCmdArgOfDenyBtn(string requestType, string planCode, string companyId)
        {
            try
            {
                DataTable dtOrderDetails = getRequestOrderDetail(requestType, planCode, companyId);
                DataTable dtblPlanDetail = null;
                CURRENCY_TYPE eCurrType = CURRENCY_TYPE.INR;

                lblPurchasePlan_Code.Text = planCode;

                string strNoOfUsers = "", strNoOfMonths = "", strCurrencyType = "", strCurrencyUnit = "";
                if (dtOrderDetails != null && dtOrderDetails.Rows.Count > 0)
                {
                    dtblPlanDetail = getPlanDtls(
                        planCode,
                        Convert.ToInt32(dtOrderDetails.Rows[0]["NUMBER_MONTH"]),
                        Convert.ToString(dtOrderDetails.Rows[0]["CURRENCY_TYPE"])
                        );
                    strNoOfUsers = dtOrderDetails.Rows[0]["MOBILE_USERS"].ToString();
                    strNoOfMonths = Convert.ToInt32(dtOrderDetails.Rows[0]["NUMBER_MONTH"]).ToString();
                    strCurrencyType = dtOrderDetails.Rows[0]["CURRENCY_TYPE"].ToString();
                }
                lblPurchaseMnths.Text = strNoOfMonths;
                lblPurchaseUsers.Text = strNoOfUsers;
                if (dtblPlanDetail != null && dtblPlanDetail.Rows.Count > 0)
                {
                    if (strCurrencyType != "" && strCurrencyType.ToLower() == "inr")
                    {
                        lblPurchaseChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                        strCurrencyUnit = "INR";
                        lblPurchaseChrgPerMnthUnit.Text = strCurrencyUnit;
                        eCurrType = CURRENCY_TYPE.INR;
                    }
                    else if (strCurrencyType != "" && strCurrencyType.ToLower() == "usd")
                    {
                        lblPurchaseChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                        strCurrencyUnit = "USD";
                        lblPurchaseChrgPerMnthUnit.Text = strCurrencyUnit;
                        eCurrType = CURRENCY_TYPE.USD;
                    }
                }
                //if (dtblDiscounts != null && dtblDiscounts.Rows.Count > 0)
                //{
                //    if (strNoOfMonths != "")
                //    {
                //        lblPurchaseDiscount.Text = getDiscount(dtblDiscounts, strNoOfMonths).ToString();
                //    }
                //    else
                //    {
                //        lblPurchaseDiscount.Text = "0";
                //    }
                //}
                string strPayableAmount = "";
                if (strNoOfUsers != "" && strNoOfMonths != "" && lblPurchaseChargeperMnth.Text != "")
                {

                    lblPurchaseOrgPrice.Text = 
                        calculatePrice(
                        strNoOfUsers,
                        strNoOfMonths,
                        lblPurchaseChargeperMnth.Text,
                        "",
                        eCurrType,
                        out strPayableAmount);
                    lblPurchaseOrgPrcUnit.Text = strCurrencyUnit;
                }
                else
                {
                    lblPurchaseOrgPrice.Text = "0";
                }
                //if (strNoOfUsers != "" && strNoOfMonths != "" && lblPurchaseChargeperMnth.Text != "" && txtPurchaseDiscountAmt.Text != "")
                //{
                //    lblPurchaseDiscountPrice.Text = calculatePrice(strNoOfUsers, strNoOfMonths, lblPurchaseChargeperMnth.Text, lblPurchaseDiscount.Text, out strPayableAmount);
                //    lblPurchaseDiscountUnit.Text = strCurrencyUnit;
                //}

                lblPurchasePayableAmount.Text = strPayableAmount;
                lblPurchasePaybleAmntUnit.Text = strCurrencyUnit;
                txtPurchaseDiscountAmt.Text = "0";
                btnPurchase.CommandArgument = companyId;
                setCommandArgumentsOfDenyButtons(requestType, planCode, companyId, btnPurchaseDenyRequest, MODAL_POP_UP_TYPE.PURCHASE);
                upPurchasePlanModal.Update();
                Utilities.showModalPopup("divPurchasePlanBox", this.Page, "Purchase Plan", "350", true, false);
            }
            catch 
            {
                //Utilities.showMessage(ex.Message, this.Page, "Error Message", DIALOG_TYPE.Error);
                Utilities.showMessage("Internal server error.", this.Page, "Error Message", DIALOG_TYPE.Error);
            }

        }
        void showAddUserRequestDetailAndSetCmdArgOfDenyBtn(string requestType, string planCode, string companyId)
        {
            try
            {
                hidPriceAmountPayable.Value = "";
                DataTable dtOrderDetails = getRequestOrderDetail(requestType, planCode, companyId);
                DataTable dtblPlanDetail = null;
                lblAddUserPlan_Code.Text = planCode;
                CURRENCY_TYPE eCurrType = CURRENCY_TYPE.INR;
                string strNoOfUsers = "", strNoOfMonths = "", strCurrencyType = "", strCurrencyUnit = "";
                if (dtOrderDetails != null && dtOrderDetails.Rows.Count > 0)
                {
                    dtblPlanDetail = getPlanDtls(planCode, Convert.ToInt32(dtOrderDetails.Rows[0]["NUMBER_MONTH"]), Convert.ToString(dtOrderDetails.Rows[0]["CURRENCY_TYPE"]));
                    strNoOfUsers = dtOrderDetails.Rows[0]["MOBILE_USERS"].ToString();
                    strNoOfMonths = dtOrderDetails.Rows[0]["NUMBER_MONTH"].ToString();
                    strCurrencyType = dtOrderDetails.Rows[0]["CURRENCY_TYPE"].ToString();
                }
                lblAddUserMnths.Text = strNoOfMonths;
                lblAddUserUsers.Text = strNoOfUsers;
                if (dtblPlanDetail != null && dtblPlanDetail.Rows.Count > 0)
                {
                    if (strCurrencyType != "" && strCurrencyType.ToLower() == "inr")
                    {
                        lblAddUserChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                        strCurrencyUnit = "INR";
                        lblAddUserChrgPerMnthUnit.Text = strCurrencyUnit;
                        eCurrType = CURRENCY_TYPE.INR;
                    }
                    else if (strCurrencyType != "" && strCurrencyType.ToLower() == "usd")
                    {
                        lblAddUserChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                        strCurrencyUnit = "USD";
                        lblAddUserChrgPerMnthUnit.Text = strCurrencyUnit;
                        eCurrType = CURRENCY_TYPE.USD;
                    }
                    lblAddUserPlanName.Text = ((string)dtblPlanDetail.Rows[0]["PLAN_NAME"]).ToString();
                }
                //if (dtblDiscounts != null && dtblDiscounts.Rows.Count > 0)
                //{
                //    if (strNoOfMonths != "")
                //    {
                //        lblAddUserDiscount.Text = getDiscount(dtblDiscounts, strNoOfMonths).ToString();
                //    }
                //    else
                //    {
                //        lblAddUserDiscount.Text = "0";
                //    }
                //}
                string strPayableAmount = "";
                if (strNoOfUsers != "" && strNoOfMonths != "" && lblAddUserChargeperMnth.Text != "")
                {
                    lblAddUserOrgPrice.Text = calculatePrice(strNoOfUsers, strNoOfMonths, lblAddUserChargeperMnth.Text, "",eCurrType, out strPayableAmount);
                    lblAddUserOrgPrcUnit.Text = strCurrencyUnit;
                }
                else
                {
                    lblAddUserOrgPrice.Text = "0";
                }
                //if (strNoOfUsers != "" && strNoOfMonths != "" && lblAddUserChargeperMnth.Text != "" && lblAddUserDiscount.Text != "")
                //{
                //    lblAddUserDiscountPrice.Text = calculatePrice(strNoOfUsers, strNoOfMonths, lblAddUserChargeperMnth.Text, lblAddUserDiscount.Text, out strPayableAmount);
                //    lblAddUserDiscountUnit.Text = strCurrencyUnit;
                //}
                string strMonthsRemaining = "";
                DataTable dtblCompanyCurrentPlan = getCompanyCurrentPlanDetail(companyId);
                if (dtblCompanyCurrentPlan != null && dtblCompanyCurrentPlan.Rows.Count > 0)
                {
                    lblAddUserValidity.Text = getValidityOfPlan(out strMonthsRemaining, (Convert.ToInt64(dtblCompanyCurrentPlan.Rows[0]["PURCHASE_DATE"])).ToString(), (Convert.ToDouble(dtblCompanyCurrentPlan.Rows[0]["VALIDITY"])).ToString());
                }

                lblAddUserPayableAmount.Text = strPayableAmount;
                lblAddUserPaybleAmntUnit.Text = strCurrencyUnit;
                txtAddUserDiscountAmt.Text = "0";
                btnAddUser.CommandArgument = companyId;
                setCommandArgumentsOfDenyButtons(requestType, planCode, companyId, btnAddUserDenyRequest, MODAL_POP_UP_TYPE.ADD_USER);
                upAddUserPlanModal.Update();
                Utilities.showModalPopup("divAddUserPlanBox", this.Page, "Add User", "350", true, false);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", this.Page, "Error Message", DIALOG_TYPE.Error);
            }
        }
        void showPlanUpgradeDowngradeReqDetailAndSetCmdArgOfDenyBtn(string requestType, string planCode, string companyId)
        {
            DataTable dtOrderDetails = getRequestOrderDetail(requestType, planCode, companyId);
            DataTable dtblPlanDetail = null;
            lblUpgradeDowngradePlan_Code.Text = planCode;
            string strNoOfUsers = "", strNoOfMonths = "", strCurrencyType = "", strCurrencyUnit = "";
            if (dtOrderDetails != null && dtOrderDetails.Rows.Count > 0)
            {
                dtblPlanDetail = getPlanDtls(planCode, Convert.ToInt32(dtOrderDetails.Rows[0]["NUMBER_MONTH"]), Convert.ToString(dtOrderDetails.Rows[0]["CURRENCY_TYPE"]));
                strNoOfUsers = dtOrderDetails.Rows[0]["MOBILE_USERS"].ToString();
                strNoOfMonths = dtOrderDetails.Rows[0]["NUMBER_MONTH"].ToString();
                strCurrencyType = dtOrderDetails.Rows[0]["CURRENCY_TYPE"].ToString();
            }
            lblUpgradeDowngradeMnths.Text = strNoOfMonths;
            lblUpgradeDowngradeUsers.Text = strNoOfUsers;
            if (dtblPlanDetail != null && dtblPlanDetail.Rows.Count > 0)
            {
                if (strCurrencyType != "" && strCurrencyType.ToLower() == "inr")
                {
                    lblUpgradeDowngradeChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                    strCurrencyUnit = "INR";
                    lblUpgradeDowngradeChrgPerMnthUnit.Text = strCurrencyUnit;
                }
                else if (strCurrencyType != "" && strCurrencyType.ToLower() == "usd")
                {
                    lblUpgradeDowngradeChargeperMnth.Text = ((Decimal)dtblPlanDetail.Rows[0]["PRICE"]).ToString();
                    strCurrencyUnit = "USD";
                    lblUpgradeDowngradeChrgPerMnthUnit.Text = strCurrencyUnit;
                }
                lblUpgradeDowngradeMaxWrkFlows.Text = (string)dtblPlanDetail.Rows[0]["MAX_WORKFLOW"].ToString();
                lblUpgradeDowngradePlanName.Text = (string)dtblPlanDetail.Rows[0]["PLAN_NAME"].ToString();
            }
            DataTable dtblCmpCurrentPlan = getCompanyCurrentPlanDetail(companyId);
            if (dtblCmpCurrentPlan != null && dtblCmpCurrentPlan.Rows.Count > 0)
            {
                //lblUpgradeDowngradePayableAmount.Text = calculateUpgradeDowngradeAmount(Convert.ToDouble(lblUpgradeDowngradeMnths.Text), Convert.ToInt32(lblUpgradeDowngradeUsers.Text)
                //      , Convert.ToDouble(strCurrencyType.ToLower() == "inr" ? (decimal)dtblCmpCurrentPlan.Rows[0]["USER_CHARGE_INR_PM"] : (decimal)dtblCmpCurrentPlan.Rows[0]["USER_CHARGE_USD_PM"])
                //      , Convert.ToDouble(lblUpgradeDowngradeChargeperMnth.Text));

                lblUpgradeDowngradePayableAmount.Text = calculateUpgradeDowngradeAmount(Convert.ToDouble(lblUpgradeDowngradeMnths.Text), Convert.ToInt32(lblUpgradeDowngradeUsers.Text)
                      , Convert.ToDouble(dtblCmpCurrentPlan.Rows[0]["USER_CHARGE_PM"])
                      , Convert.ToDouble(lblUpgradeDowngradeChargeperMnth.Text));
                lblUpgradeDowngradePayableAmountUnit.Text = strCurrencyType;
            }


            //change buttons text and command argument
            if (requestType.ToLower() == "4")
            {
                btnUpgradeDowngrade.Text = "Downgrade";
                btnUpgradeDowngrade.CommandName = "Downgrade";
            }
            else if (requestType.ToLower() == "3")
            {
                btnUpgradeDowngrade.Text = "Upgrade";
                btnUpgradeDowngrade.CommandName = "Upgrade";
            }
            btnUpgradeDowngrade.CommandArgument = companyId;
            setCommandArgumentsOfDenyButtons(requestType, planCode, companyId, btnUpgradeDowngradeDenyReq, MODAL_POP_UP_TYPE.UPGRADE_DOWNGRADE);
            upUpgradeDowngradePlanModal.Update();
            Utilities.showModalPopup("divUpgradeDowngradePlanBox", this.Page, btnUpgradeDowngrade.Text + " " + "Plan", "350", true, false);
        }
        void showPlanDowngradeReqDetail(string requestType, string planCode)
        {

        }
        DataTable getRequestOrderDetail(string requestType, string planCode, string companyId)
        {
            AddServerDetails addServerDetails = new AddServerDetails();
            DataTable dtOrderDetails = addServerDetails.GetPlanOrderRequestDetail(requestType, planCode, companyId);
            return dtOrderDetails;
        }
        private void AddUser()
        {
            //GetDiscountPrice(lblValidMonths.Text);
            //CalculatePayableAmount();
        }
        /// <summary>
        /// not required
        /// </summary>
        /// <param name="validMonths"></param>

        private void GetDiscountPrice(string validMonths)
        {
            //bool flag = false;
            //GetPlanList objPlanList = new GetPlanList();
            //objPlanList.PlanDiscounts();
            //DataTable dtblPlanDiscounts = objPlanList.PlanDiscountList;
            //foreach (DataRow row in dtblPlanDiscounts.Rows)
            //{
            //    if (Convert.ToBoolean(row["ENABLED"].ToString()) == true)
            //    {
            //        string fromMonth = row["FROM_MONTH"].ToString();
            //        string toMonth = row["TO_MONTH"].ToString();
            //        if (validMonths == fromMonth || validMonths == toMonth)
            //        {
            //            lblAllowableDiscount.Text = row["DISCOUNT"].ToString();
            //            flag = true;
            //        }
            //        else
            //        {
            //            flag = false;
            //        }
            //    }
            //}
            //if (!flag)
            //{
            //    lblAllowableDiscount.Text = "0";
            //}
        }
        /// <summary>
        /// not required
        /// </summary>
        private void CalculatePayableAmount()
        {
            //if (txtAddUsers.Text == "")
            //{
            //    txtAddUsers.Text = "1";
            //}
            //lblPrice.Text = (Convert.ToDecimal(txtAddUsers.Text) * Convert.ToDecimal(lblUserChargePerMonth.Text) * Convert.ToDecimal(lblValidMonths.Text)).ToString();
            //lblDiscountAmount.Text = "0";
            //lblPayableAmount.Text = lblPrice.Text;
        }
        void showStickyOrNormalMessage(string message, string type)
        {
            if (type.ToLower() == "sticky")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingSticky", @"$.msg('" + message + "',{sticky:true});", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingNormal", @"$.msg('" + message + "');", true);
            }
        }
        private void CalculateUpgradedAmount(out string strPlanPrice)
        {
            strPlanPrice = "";
            ////lblMonths.Text = lblValidMonths.Text;
            //lblMonths.Text = "";
            //GetPlanList objPlanList = new GetPlanList(lblUpgradedPlan.Text);
            //objPlanList.Process();
            //DataTable dtblPlanDtls = objPlanList.PlanDetails;
            //if (dtblPlanDtls != null)
            //{
            //    if (dtblPlanDtls.Rows.Count > 0)
            //    {
            //        lblWorkFlow.Text = Convert.ToString(dtblPlanDtls.Rows[0]["MAX_WORKFLOW"]);
            //        if (lblChargeType.Text == "INR")
            //        {
            //            strPlanPrice = Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_INR_PM"]);
            //        }
            //        else if (lblChargeType.Text == "USD")
            //        {
            //            strPlanPrice = Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_USD_PM"]);
            //        }
            //    }
            //}
        }
        protected int AddCurrentPlanPurchaseHistory(
            string companyId, string chargePerMnth,
            string currencyType, string noOfUsers,
            string maxWorkFlows, string validity,
            string priceWithoutDiscount, string discountPercentage,
            string actualPrice, string transactionType,
            string planCode, string planName)
        {
            int iError = -1000;//for error
            LogoutUsersAdmin objLogout = new LogoutUsersAdmin();
            string strResellerId = objLogout.GetResellerId(strUserName);

            AddCurrentPlanPurchaseHistory addCurrentPlanPurchaseHistory =
                new AddCurrentPlanPurchaseHistory(companyId, chargePerMnth,
                    currencyType, noOfUsers,
                    maxWorkFlows, validity,
                    priceWithoutDiscount, discountPercentage,
                    actualPrice, transactionType,
                    planCode, planName,
                    strResellerId);
            addCurrentPlanPurchaseHistory.Process();
            int status = addCurrentPlanPurchaseHistory.StatusCode;
            string strStatusDescription = addCurrentPlanPurchaseHistory.StatusDescription;
            if (status == 0)
            {
                //showStickyOrNormalMessage("Ordered successfully", "sticky");
                iError = 0;
            }
            else
            {
                //showStickyOrNormalMessage("Internal server error.Please try again", "sticky");
                iError = -1000;
            }
            return iError;
        }
        #region also used in accountapproval.aspx
        //also used in account approval.aspx
        protected DataTable getPlanDiscounts(string validMonths)
        {
            GetPlanList objPlanList = new GetPlanList();
            objPlanList.PlanDiscounts();
            return objPlanList.PlanDiscountList;
        }
        //DataTable getPlanDtlWithDiscounts(string planCode, int months, string currency)
        //{
        //    GetPlanList objPlanList;
        //    if (planCode == "")
        //    {
        //        objPlanList = new GetPlanList(currency, months);
        //    }
        //    else
        //    {
        //        objPlanList = new GetPlanList(planCode,currency,months);
        //    }
        //    //objPlanList.PlanDiscounts();
        //    //planDiscounts = objPlanList.PlanDiscountList;
        //    objPlanList.Process();
        //    return objPlanList.PlanDetails;
        //}

        DataTable getPlanDtls(string planCode, int months, string currency)
        {
            GetPlanList objPlanList;
            if (planCode == "")
            {
                objPlanList = new GetPlanList(currency, months);
            }
            else
            {
                objPlanList = new GetPlanList(planCode, currency, months);
            }
            objPlanList.Process();
            return objPlanList.PlanDetails;
        }
        int getDiscount(DataTable planDiscountDtlsDTbl, string months)
        {
            if (planDiscountDtlsDTbl.Rows.Count > 0)
            {
                string filter = String.Format("FROM_MONTH <= '{0}' AND TO_MONTH>='{1}'", Math.Floor(Convert.ToDouble(months)), Math.Floor(Convert.ToDouble(months)));
                DataRow[] rows = planDiscountDtlsDTbl.Select(filter);
                if (rows.Length > 0)
                {

                    return Convert.ToInt32((string)rows[0]["DISCOUNT"]);
                }
                else
                {
                    return 0;
                }
            }
            else
                return 0;
        }
        /// <summary>
        /// Returns price without discount.
        /// </summary>
        /// <param name="noOfUsers"></param>
        /// <param name="noOfMonths"></param>
        /// <param name="pricePerUserPerMonth"></param>
        /// <param name="discountAmount"></param>
        /// <param name="currencyType"></param>
        /// <param name="payableAmount">Price after discount amount,which is actually to be paid</param>
        /// <returns></returns>
        string calculatePrice(
            string noOfUsers,
            string noOfMonths,
            string pricePerUserPerMonth,
            string discountAmount,
            CURRENCY_TYPE currencyType,
            out string payableAmount)
        {
            payableAmount = "";
            double priceWithoutDiscount = (int.Parse(noOfUsers) * double.Parse(noOfMonths) * double.Parse(pricePerUserPerMonth));
            if (discountAmount == "")
            {
                payableAmount = (Utilities.roundOfAmountByCurrencyType(Math.Round(priceWithoutDiscount, 2), currencyType)).ToString();
            }
            else
            {
                double priceAfterDiscount = (int.Parse(noOfUsers) * double.Parse(noOfMonths) * double.Parse(pricePerUserPerMonth)) - Convert.ToDouble(discountAmount);
                payableAmount = (Utilities.roundOfAmountByCurrencyType(Math.Round(priceAfterDiscount, 2), currencyType)).ToString(); ;
            }
            return (Utilities.roundOfAmountByCurrencyType(Math.Round(priceWithoutDiscount, 2), currencyType)).ToString();
        }
        DataTable getCompanyCurrentPlanDetail(string companyId)
        {
            CurrentPlanDetail objCurrentPlan = new CurrentPlanDetail(companyId, true, false);
            objCurrentPlan.Process();
            return objCurrentPlan.PlanDetails;
        }
        string getValidityOfPlan(out string monthsRemaining, string purchaseDate, string validityMonths)
        {
            DateTime dtPurchaseDate = new DateTime(Convert.ToInt64(purchaseDate));
            DateTime dtValidPeriod = dtPurchaseDate.AddMonths(Convert.ToInt32(Convert.ToDouble(validityMonths)));
            TimeSpan ts = dtValidPeriod.Subtract(DateTime.Now);
            monthsRemaining = (ts.Days / 30.4368499).ToString("N2");
            return dtValidPeriod.ToLongDateString();
        }
        string calculateUpgradeDowngradeAmount(double months, int noOfUsers, double currentPlanChrgPerMnth, double newPlanChrgPerMnth)
        {
            double dblAmountAlreadyPaid = months * noOfUsers * currentPlanChrgPerMnth;
            double dblAmountToPay = months * noOfUsers * newPlanChrgPerMnth;
            return (dblAmountToPay - dblAmountAlreadyPaid).ToString();
        }
        #endregion
        protected void btnUpgradeDowngrade_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtblPlanDetails = getPlanDtls(lblUpgradeDowngradePlan_Code.Text, Convert.ToInt32(Math.Floor(Convert.ToDouble(lblUpgradeDowngradeMnths.Text))), lblUpgradeDowngradeChrgPerMnthUnit.Text);
                DataTable dtblCompanyCurrentPlan = getCompanyCurrentPlanDetail(((Button)sender).CommandArgument);
                string strRequestType;
                if (((Button)sender).CommandName.ToLower() == "upgrade")
                {
                    strRequestType = ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE).ToString();
                }
                else
                {
                    strRequestType = ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE).ToString();
                }

                if (dtblPlanDetails.Rows.Count > 0 && dtblCompanyCurrentPlan.Rows.Count > 0)
                {
                    DataRow planDetailRow = dtblPlanDetails.Rows[0];
                    DataRow cmpCurrntPlanRow = dtblCompanyCurrentPlan.Rows[0];
                    //string strMonthsRemaining= getValidityOfPlan(out strMonthsRemaining,Convert.ToInt64(cmpCurrntPlanRow["PURCHASE_DATE"]).ToString(),Convert.ToDouble(cmpCurrntPlanRow["VALIDITY"]).ToString())
                    //int iError = AddCurrentPlanPurchaseHistory(((Button)sender).CommandArgument, (lblUpgradeDowngradeChrgPerMnthUnit.Text.ToLower() == "inr" ? (decimal)planDetailRow["USER_CHARGE_INR_PM"] : (decimal)planDetailRow["USER_CHARGE_USD_PM"]).ToString()
                    //            , lblUpgradeDowngradeChrgPerMnthUnit.Text, ((int)cmpCurrntPlanRow["MAX_USER"]).ToString(), ((int)planDetailRow["MAX_WORKFLOW"]).ToString()
                    //     , lblUpgradeDowngradeMnths.Text, lblUpgradeDowngradePayableAmount.Text, "0", lblUpgradeDowngradePayableAmount.Text, strRequestType,
                    //     lblUpgradeDowngradePlan_Code.Text, (string)planDetailRow["PLAN_NAME"]);

                    int iError = AddCurrentPlanPurchaseHistory(((Button)sender).CommandArgument.Split(';')[0], Convert.ToString(planDetailRow["PRICE"])
                                , lblUpgradeDowngradeChrgPerMnthUnit.Text, ((int)cmpCurrntPlanRow["MAX_USER"]).ToString(), ((int)planDetailRow["MAX_WORKFLOW"]).ToString()
                         , lblUpgradeDowngradeMnths.Text, lblUpgradeDowngradePayableAmount.Text, "0", lblUpgradeDowngradePayableAmount.Text, strRequestType,
                         lblUpgradeDowngradePlan_Code.Text, (string)planDetailRow["PLAN_NAME"]);
                    if (iError == 0)
                    {
                        Utilities.closeModalPopUp("divUpgradeDowngradePlanBox", this.Page);
                        BindOrderRepeater();
                    }
                    else
                    {
                        showAlert("Internal error.", "divUpgradeDowngradePlanBox", this.Page);
                    }
                }
                else
                {
                    showAlert("Internal error.", "divUpgradeDowngradePlanBox", this.Page);
                }
            }
            catch
            {
                showAlert("Internal error.", "divUpgradeDowngradePlanBox", this.Page);
            }
        }
        #endregion

        protected void btnSaveDenyRemarks_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                string strCommandArgument = btnSender.CommandArgument;//; separated values companyId,planCode,RequestType
                string[] aryCommandArgumentValues = strCommandArgument.Split(';');
                SavePlanReqDenyRemarksAndStatusByUserType objSavePlanReqDenyRemarks = new SavePlanReqDenyRemarksAndStatusByUserType(aryCommandArgumentValues[0], aryCommandArgumentValues[1]
                                                                                         , aryCommandArgumentValues[2], USER_TYPE.Reseller, ddlRemarks.SelectedValue);
                objSavePlanReqDenyRemarks.Process();
                if (objSavePlanReqDenyRemarks.StatusCode == 0)
                {
                    Utilities.showMessage("Denied successfully", this.Page, DIALOG_TYPE.Info);
                    BindOrderRepeater();
                    Utilities.closeModalPopUp("divDenyRequestBox", this.Page, "Close Remarks model");
                    MODAL_POP_UP_TYPE modalPopUpType = (MODAL_POP_UP_TYPE)Enum.Parse(typeof(MODAL_POP_UP_TYPE), aryCommandArgumentValues[4]);
                    string strModalPopUpToClose = "";
                    switch (modalPopUpType)
                    {
                        case MODAL_POP_UP_TYPE.ADD_USER:
                            strModalPopUpToClose = "divAddUserPlanBox";
                            break;
                        case MODAL_POP_UP_TYPE.PURCHASE:
                            strModalPopUpToClose = "divPurchasePlanBox";
                            break;
                        case MODAL_POP_UP_TYPE.RENEW:
                            strModalPopUpToClose = "divRenewPlanBox";
                            break;
                        case MODAL_POP_UP_TYPE.UPGRADE_DOWNGRADE:
                            strModalPopUpToClose = "divUpgradeDowngradePlanBox";
                            break;
                    }
                    Utilities.closeModalPopUp(strModalPopUpToClose, this.Page, "Close Plan Request Details model");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, DIALOG_TYPE.Error);
            }

        }
        protected void btnDenyRequest_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                bindDenyRequestDropDown();
                btnSaveDenyRemarks.CommandArgument = btnSender.CommandArgument;
                updDenyRequest.Update();
                Utilities.showModalPopup("divDenyRequestBox", this.Page, "Select Remarks", "350", false, false);

            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, "Show Error Message", DIALOG_TYPE.Error);
            }
        }
        void setCommandArgumentsOfDenyButtons(string requestType, string planCode, string companyId, Button buttonToSetArg, MODAL_POP_UP_TYPE popUpType)
        {
            buttonToSetArg.CommandArgument = companyId + ";" + planCode + ";" + requestType + ";" + USER_TYPE.Reseller + ";" + popUpType;
        }
        void bindDenyRequestDropDown()
        {
            GetPlanPurchaseDenyRemarks objPlanPurchaseDenyRemarks = new GetPlanPurchaseDenyRemarks();
            objPlanPurchaseDenyRemarks.Process();
            if (objPlanPurchaseDenyRemarks.StatusCode == 0)
            {
                DataTable dtblPlanPurchaseDenyRemarks = objPlanPurchaseDenyRemarks.ResultTable;
                if (dtblPlanPurchaseDenyRemarks.Rows.Count > 0)
                {
                    ddlRemarks.DataSource = dtblPlanPurchaseDenyRemarks;
                    ddlRemarks.DataTextField = "REMARK";
                    ddlRemarks.DataValueField = "REMARK_CODE";
                    ddlRemarks.DataBind();
                }
                else
                {
                    //the remarks should exist in the database table
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }
        protected void btnCancelDenyRemarks_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divDenyRequestBox", this.Page, "Close Remarks Div");
            btnSaveDenyRemarks.CommandArgument = "";
        }
    }
}