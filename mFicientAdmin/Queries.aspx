﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="Queries.aspx.cs" Inherits="mFicientAdmin.Queries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        a.linkSelected:link
        {
            color: #8F8F97;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divTab" style="margin-left: 60px;">
        <br />
        <%--<ul id="tabList">
                                <li id="Mbqueries">--%>
        <asp:LinkButton ID="lnk1" runat="server" Text="Open Queries" OnClick="lnk1_Click"></asp:LinkButton>&nbsp;&nbsp;&nbsp;|
        <%--<a id="MbQueries" href="#Mobile Queries" onclick="storeSelectedTabIndex(1);">
                                    MobileUsers Queries</a>--%>
        <%--</li>
                                <li id="Myqueries">--%>
        <%--<a id="lnkMy" href="#My Queries" onclick="storeSelectedTabIndex(2);">
                                    My Queries</a>--%>
        <asp:LinkButton ID="lnk2" runat="server" Text="Closed Queries" OnClick="lnk2_Click"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
        <%-- </li>--%>
        <%-- <li id="Resolved">--%>
        <%--<a id="lnkResolved" href="#Resolved Queries" onclick="storeSelectedTabIndex(3);">
                                    Resolved</a>--%>
       <%-- <asp:LinkButton ID="lnk3" runat="server" Text="Resolved Queries" OnClick="lnk3_Click"></asp:LinkButton>&nbsp;&nbsp;&nbsp;|
        <%--  </li>--%>
        <%--  <li id="Closed">--%>
        <%--<a id="lnkClosed" href="#Closed Queries" onclick="storeSelectedTabIndex(4);">
                                    Closed</a>--%>
<%--        <asp:LinkButton ID="lnk4" runat="server" Text="Closed Queries" OnClick="lnk4_Click"></asp:LinkButton>
--%>     
        <%-- </li>--%>
        <%--</ul>--%>
    </div>
    <div id="PageCanvasContent">
        <div id="divRepeater">
            <asp:UpdatePanel ID="upd" runat="server">
                <ContentTemplate>
                    <section>
                            <div id="divInformation">
                            </div>
                        </section>
                    <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server"> </asp:Label></div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"   >
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                <th style="display:none;">
                                                User Id
                                                </th>
                                                <th>USER NAME</th>
                                                   <th>
                                                TOKEN NUMBER
                                                   </th>
                                                   
                                                    <th >
                                                     QUERY SUBJECT
                                                    </th>
                                                    <th>
                                                    POSTED ON
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td style="display:none;">
                                            
                                             <asp:Label ID="lblid" runat="server" Text='<%#Eval("USER_ID") %>'></asp:Label>
                                            </td>
                                               <td >
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("NAME") %>'></asp:Label>
                                                </td>

                                                 <td >
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%#Eval("TOKEN_NO") %>'></asp:Label>
                                                </td>


                                             <td>
                                              <asp:Label ID="lblquery" runat="server" Text='<%# Eval("QUERY_SUBJECT") %>'></asp:Label>
                                                </td>
                                             
                                             <td>
                                              <asp:Label ID="lbldate" runat="server" Text='<%# Eval("PostedDate") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                   OnClick="lnkok_Click" ></asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td style="display:none;">
                                            
                                             <asp:Label ID="lblid" runat="server" Text='<%#Eval("USER_ID") %>'></asp:Label>
                                            </td>
                                             <td >
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("NAME") %>'></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%#Eval("TOKEN_NO") %>'></asp:Label>
                                                </td>

                                               
                                               
                                                <td>
                                              <asp:Label ID="lblquery" runat="server" Text='<%# Eval("QUERY_SUBJECT") %>'></asp:Label>
                                                </td>
                                             
                                             <td>
                                              <asp:Label ID="lbldate" runat="server" Text='<%# Eval("PostedDate") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                     OnClick="lnkok_Click"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                           
                        </section>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none;">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 85%; margin-left: 25px;">
                                <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                    <%--<%--<fieldset>--%>
                                    <div style="margin-bottom: 20px; margin-top: 5px;">
                                        <div style="float: left; margin-right: 18px;">
                                            <asp:Label ID="label8" Text="<strong>User Name</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left;">
                                            <asp:Label ID="lname" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-right: 32px; float: left;">
                                        <div style="float: left; margin-right: 37px; margin-top: 5px;">
                                            <asp:Label ID="label2" Text="<strong>Subject</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left; margin-top: 5px;">
                                            <asp:Label ID="lsubject" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 2px;">
                                            <asp:Label ID="label3" Text="<strong>Query</strong>" runat="server"></asp:Label></div>
                                        <div style="margin-left: 80px; margin-top: 5px; margin-right: 50px;">
                                            <asp:Label ID="lquery" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div>
                                        <asp:Label ID="lblcomments" Text="<strong>Comments</strong>" runat="server"></asp:Label></div>
                                    <div id="Comments" runat="server" style="margin-bottom: 5px; overflow-y: auto; margin-top: 5px;">
                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <div style="float: left; margin-right: 50px;">
                                                    <asp:Label ID="lblcommentby" runat="server" Text=' <%#Eval("ADMIN_NAME") %>'></asp:Label></div>
                                                &nbsp;&nbsp;&nbsp;
                                                <div style="float: left;">
                                                    <asp:Label ID="lblcomment" runat="server" Text='<%# Eval("COMMENT") %>'></asp:Label></div>
                                                <div style="clear: both;">
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div style="margin-top: 10px;">
                                        <asp:Label ID="lblcomment" Text="<strong>Add a Comment</strong>" runat="server"></asp:Label><br />
                                        <div style="margin-top: 5px;margin-bottom:20px;">
                                            <asp:TextBox ID="txtcomment" runat="server" Width="200px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;
                                            <asp:Button ID="bttncomment" runat="server" Text="Save" OnClick="bttncomment_Click" />
                                        </div>
                                    </div>
                                    <%--<div style="margin-top: 10px; float: left; margin-right: 30px;">
                                            <asp:Label ID="label7" Text="<strong>Resolved</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left; margin-top: 10px;">
                                            <asp:CheckBox ID="chkIsResolved" runat="server" />
                                        </div>--%>
                                    <div style="clear: both;">
                                    </div>
                                    <%--</fieldset>--%>
                                </asp:Panel>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div style="clear: both">
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
    </div>
    <div>
        
        <asp:HiddenField ID="hidTabSelected" runat="server" />
        <asp:HiddenField ID="hidPriceAmountPayable" runat="server" />
        <asp:HiddenField ID="hidPasswordEntered" runat="server" />
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>
