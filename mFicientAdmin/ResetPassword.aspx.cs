﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class resetPassword : System.Web.UI.Page
    {
        public enum ResetPassword_ERROR_CODES
        {
            InvalidErrorCode,
            InvalidUserType,
            InvalidQueryString,
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (Request.QueryString != null)
                {
                    pnlResetPasswordInfo.Visible = true;
                    if (Request.QueryString.Count > 0)
                    {
                        setHidQueryStringValue(Request.QueryString.Get(0));
                        if (String.IsNullOrEmpty(hidQueryString.Value))
                        {
                            Utilities.showAlertOnPageStartUp(
                            "Invalid Link.",
                            "content", this.Page, "Invalid link");
                            return;
                        }
                        else
                        {
                            if (getCountOfQueryStringParams(hidQueryString.Value) < 2)
                            {
                                Utilities.showAlertOnPageStartUp(
                            "Invalid Link.",
                            "content", this.Page, "Invalid link");
                                return;
                            }
                            else
                            {
                                processQueryStringRequest(hidQueryString.Value);
                            }
                        }
                    }
                    else
                    {
                        Utilities.showAlertOnPageStartUp(
                        "Invalid Link.",
                        "content", this.Page, "Invalid link");
                        return;
                    }
                }
                else
                {
                    Utilities.showAlertOnPageStartUp(
                        "Invalid Link.",
                        "content", this.Page, "Invalid link");
                    return;
                }

            }
        }

        //Reset Password 
        void getUserTypeFromQueryString(
            string queryString,
            out USER_TYPE userType,
            out string loginName,
            out string userId)
        {
            string strUserType;
            getValuesFromQueryString(
                queryString,
                out loginName,
                out strUserType,
                out userId);

            if (!String.IsNullOrEmpty(queryString))
            {
                try
                {
                    userType = Utilities.getUserTypeFromUserCode(strUserType);
                }
                catch
                {
                    throw new
                    MficientException(((int)ResetPassword_ERROR_CODES.InvalidUserType).ToString());
                }

            }
            else
            {
                throw new MficientException(((int)ResetPassword_ERROR_CODES.InvalidQueryString).ToString());
            }
        }


        string[] getSplitValuesFromQueryString(string queryString)
        {
            return Utilities.DecryptString(queryString).Split('~');
        }

        void getValuesFromQueryString(string queryString, out string loginName, out string usertype, out string userId)
        {
            loginName = string.Empty;
            usertype = string.Empty;
            userId = string.Empty;
            string[] arySplitValues = this.getSplitValuesFromQueryString(queryString);
            if (arySplitValues.Length >= 3)
            {
                loginName = returnUsernameFromSplitValue(arySplitValues);
                usertype = returnUserTypeFromSplitValue(arySplitValues);
                userId = returnUserIDFromSplitValue(arySplitValues);
            }
        }

        string returnUsernameFromSplitValue(string[] aryQueryStringSplitValues)
        {
            string strUsername = String.Empty;
            if (aryQueryStringSplitValues.Length >= 3)
            {
                strUsername = aryQueryStringSplitValues[0];
            }
            return strUsername;
        }
        string returnUserTypeFromSplitValue(string[] aryQueryStringSplitValues)
        {
            string strUsertype = String.Empty;
            if (aryQueryStringSplitValues.Length >= 3)
            {
                strUsertype = aryQueryStringSplitValues[1];
            }
            return strUsertype;
        }
        string returnUserIDFromSplitValue(string[] aryQueryStringSplitValues)
        {
            string strUserID = String.Empty;
            if (aryQueryStringSplitValues.Length >= 3)
            {
                strUserID = aryQueryStringSplitValues[2];
            }
            return strUserID;
        }

        int getCountOfQueryStringParams(string queryString)
        {
            return getSplitValuesFromQueryString(queryString).Length;
        }
        object getUserDetailByUserType(string queryString, out USER_TYPE userType)
        {
            object objToReturn = null;
            string loginName = String.Empty;
            string strUserId = String.Empty;
            getUserTypeFromQueryString(queryString, out userType, out loginName, out strUserId);
            switch (userType)
            {
                case USER_TYPE.Reseller:
                    GetResellerDetailsAdmin objResellerDtl = new
                        GetResellerDetailsAdmin(strUserId, loginName);
                    objResellerDtl.Process();
                    objToReturn = objResellerDtl;
                    break;
                case USER_TYPE.SalesManager:
                case USER_TYPE.Account:
                case USER_TYPE.Support:
                case USER_TYPE.SalesPerson:
                    GetUserDetailsAdmin objUserDtl = new
                    GetUserDetailsAdmin();
                    MFEAdminPanelUsers objAdminPanelUser =
                        objUserDtl.GetUserDetailsByNameAndUserCode(
                        loginName,
                        Utilities.getUserTypeCodeFromUserType(userType)
                        );
                    objToReturn = objAdminPanelUser;
                    break;
                case USER_TYPE.Admin:
                    GetAdminDetails objAdminDtl = new
                    GetAdminDetails(GetAdminDetails.GET_RECORDS_BY.usernameAndId,
                    loginName, strUserId);
                    objAdminDtl.Process();
                    objToReturn = objAdminDtl;
                    break;
            }
            return objToReturn;
        }

        void processQueryStringRequest(string queryString)
        {
            try
            {
                USER_TYPE eUserType;
                object objUserDtl =
                    getUserDetailByUserType(queryString,
                    out eUserType);
                if (objUserDtl == null)
                    throw new MficientException(((int)ResetPassword_ERROR_CODES.InvalidQueryString).ToString());

                GetResellerDetailsAdmin objResellerDtl;
                GetAdminDetails objAdminDtl;
                MFEAdminPanelUsers objOtherUserDetail;

                objResellerDtl = objUserDtl as GetResellerDetailsAdmin;
                objAdminDtl = objUserDtl as GetAdminDetails;
                objOtherUserDetail = objUserDtl as MFEAdminPanelUsers;

                switch (eUserType)
                {
                    case USER_TYPE.Reseller:
                        if (objResellerDtl.StatusCode != 0)
                        {
                            Utilities.showAlertOnPageStartUp(
                                "Could not get user details.Please try again.",
                                "content", this.Page, "CouldNotGetUserDetail");
                            setHidQueryStringValue(String.Empty);
                        }
                        else if (String.IsNullOrEmpty(objResellerDtl.Reseller.ResellerId))
                        {
                            Utilities.showAlertOnPageStartUp("Invalid Link.",
                                "content", this.Page, "Invalid link");
                            setHidQueryStringValue(String.Empty);
                        }
                        else if (!objResellerDtl.Reseller.IsEnabled)
                        {
                            Utilities.showAlertOnPageStartUp(
                                "The user is blocked.Resetting the password is not allowed.",
                                "content", this.Page, "UserBlocked");
                            setHidQueryStringValue(String.Empty);
                        }
                        else
                        {
                            GetResetPasswordLogForUser objGetResetPasswordLog =
                                new GetResetPasswordLogForUser(objResellerDtl.Reseller.ResellerId);
                            MFEResetPasswordLog objResetPwdlog = objGetResetPasswordLog.getLatestPwdLog();
                            if (objGetResetPasswordLog.StatusCode != 0)
                            {
                                Utilities.showAlertOnPageStartUp(
                                objGetResetPasswordLog.StatusDescription,
                                "content", this.Page, "UserBlocked");
                                setHidQueryStringValue(String.Empty);
                            }
                            else
                            {
                                if (isResetPwdRequestWithinStipulatedTime(objResetPwdlog.ResetDatetime))
                                {

                                    setHidUserIdAndEmailValue(objResellerDtl.Reseller.ResellerId, Utilities.getUserTypeCodeFromUserType(eUserType));
                                    showDetailsInControls();
                                    pnlResetPasswordInfo.Visible = true;

                                }
                                else
                                {
                                    Utilities.showAlertOnPageStartUp(
                                        "Your reset password code has expired.",
                                        "content", this.Page, "Code expired.");
                                }
                            }
                        }
                        break;
                    case USER_TYPE.SalesManager:
                    case USER_TYPE.Account:
                    case USER_TYPE.Support:
                    case USER_TYPE.SalesPerson:
                        if (objOtherUserDetail == null)
                        {
                            Utilities.showAlertOnPageStartUp("Could not get user details.Please try again.",
                                "content", this.Page, "Invalid link");
                            setHidQueryStringValue(String.Empty);
                        }
                        if (String.IsNullOrEmpty(objOtherUserDetail.LoginId))
                        {
                            Utilities.showAlertOnPageStartUp("Invalid Link.",
                                "content", this.Page, "Invalid link");
                            setHidQueryStringValue(String.Empty);
                        }
                        else if (!objOtherUserDetail.IsEnabled)
                        {
                            Utilities.showAlertOnPageStartUp(
                                "The user is blocked.Resetting the password is not allowed.",
                                "content", this.Page, "UserBlocked");
                            setHidQueryStringValue(String.Empty);
                        }
                        else
                        {
                            GetResetPasswordLogForUser objGetResetPasswordLog =
                                new GetResetPasswordLogForUser(objOtherUserDetail.LoginId);
                            MFEResetPasswordLog objResetPwdlog = objGetResetPasswordLog.getLatestPwdLog();
                            if (objGetResetPasswordLog.StatusCode != 0)
                            {
                                Utilities.showAlertOnPageStartUp(
                                objGetResetPasswordLog.StatusDescription,
                                "content", this.Page, "InternalError");
                                setHidQueryStringValue(String.Empty);
                            }
                            else
                            {
                                if (isResetPwdRequestWithinStipulatedTime(objResetPwdlog.ResetDatetime))
                                {
                                    setHidUserIdAndEmailValue(objOtherUserDetail.LoginId, Utilities.getUserTypeCodeFromUserType(eUserType));
                                    showDetailsInControls();
                                    pnlResetPasswordInfo.Visible = true;
                                }
                                else
                                {
                                    Utilities.showAlertOnPageStartUp(
                                        "Your reset password code has expired.",
                                        "content", this.Page, "Code expired.");
                                }
                            }
                        }
                        break;
                    case USER_TYPE.Admin:
                        if (objAdminDtl.StatusCode != 0)
                        {
                            Utilities.showAlertOnPageStartUp(
                                "Could not get user details.Please try again.",
                                "content", this.Page, "CouldNotGetUserDetail");
                            setHidQueryStringValue(String.Empty);
                        }
                        else if (String.IsNullOrEmpty(objAdminDtl.Admin.AdminId))
                        {
                            Utilities.showAlertOnPageStartUp("Invalid Link.",
                                "content", this.Page, "Invalid link");
                            setHidQueryStringValue(String.Empty);
                        }
                        else
                        {
                            GetResetPasswordLogForUser objGetResetPasswordLog =
                                new GetResetPasswordLogForUser(objAdminDtl.Admin.AdminId);
                            MFEResetPasswordLog objResetPwdlog = objGetResetPasswordLog.getLatestPwdLog();
                            if (objGetResetPasswordLog.StatusCode != 0)
                            {
                                Utilities.showAlertOnPageStartUp(
                                objGetResetPasswordLog.StatusDescription,
                                "content", this.Page, "UserBlocked");
                                setHidQueryStringValue(String.Empty);
                            }
                            else
                            {
                                if (isResetPwdRequestWithinStipulatedTime(objResetPwdlog.ResetDatetime))
                                {
                                    setHidUserIdAndEmailValue(objAdminDtl.Admin.AdminId, Utilities.getUserTypeCodeFromUserType(eUserType));
                                    showDetailsInControls();
                                    pnlResetPasswordInfo.Visible = true;

                                }
                                else
                                {
                                    Utilities.showAlertOnPageStartUp(
                                        "Your reset password code has expired.",
                                        "content", this.Page, "Code expired.");
                                }
                            }
                        }
                        break;
                }


            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    Utilities.showAlertOnPageStartUp(
                          getErrorMessageFromCode(ex.Message),
                         "content", this.Page, "Error"); ;
                }
                else
                {
                    Utilities.showAlertOnPageStartUp(
                          ex.Message,
                         "content", this.Page, "Error");
                }
            }
            catch
            {
                Utilities.showAlertOnPageStartUp(
                        "Internal server error.",
                        "content", this.Page, "Error");
            }

        }
        /// <summary>
        /// This Reset Pwd Request should be within 48 hrs 
        /// </summary>
        /// <param name="newPwdDtlsDTable"></param>
        /// <returns></returns>
        bool isResetPwdRequestWithinStipulatedTime(long pwdResetDatetime)
        {
            // DateTime dtNewPwdReqDateTime = new DateTime(lngNewPwdRequestedTime);
            //TimeSpan tsTimeIntervalBtwnReqAndUsage = DateTime.UtcNow - dtNewPwdReqDateTime;
            //48 hrs 48 * 60 * 60 * 10000000;
            if ((DateTime.UtcNow.Ticks - pwdResetDatetime) > 1728000000000)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Modal pop up reset button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(hidQueryString.Value))
                {
                    Utilities.showAlert(
                        "Invalid Username and / or Reset code and / or Company Id",
                        "content", updResetPwdInfo, "ShowErrorMsg");
                }
                else
                {
                    USER_TYPE eUserType;
                    string struserId, strLoginName;
                    getUserTypeFromQueryString(hidQueryString.Value,
                       out eUserType, out strLoginName, out struserId);

                    GetResetPasswordLogForUser objGetResetPasswordLog =
                                new GetResetPasswordLogForUser(struserId);
                    MFEResetPasswordLog objResetPwdlog = objGetResetPasswordLog.getLatestPwdLog();
                    if (objGetResetPasswordLog.StatusCode != 0)
                    {
                        Utilities.showAlertOnPageStartUp(
                        objGetResetPasswordLog.StatusDescription,
                        "content", this.Page, "InternalError");
                        setHidQueryStringValue(String.Empty);
                    }
                    else
                    {
                        if (objResetPwdlog.PasswordCode == Utilities.GetMd5Hash(txtResetCode.Value.Trim()))
                        {
                            Utilities.showModalPopup(
                                "divModalContainer",
                                updResetPwdInfo,
                                "Change Password",
                                Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.CHANGE_PASSWORD)), false);
                        }
                        else
                        {
                            Utilities.showAlert(
                                "Reset Code is incorrect",
                                "content", updResetPwdInfo,
                                "ShowErrorMsg");
                        }
                    }

                }
            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    Utilities.showAlert(
                          getErrorMessageFromCode(ex.Message),
                         "content", updResetPwdInfo, "Error"); ;
                }
                else
                {
                    Utilities.showAlert(
                          ex.Message,
                         "content", updResetPwdInfo, "Error");
                }
            }
            catch
            {
                Utilities.showAlert(
                        "Internal server error.",
                        "content", updResetPwdInfo, "Error");
            }
        }

        protected void btnSavePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtNewPassword.Text))
                {
                    Utilities.showAlert("Please enter a password.",
                        "divChangePwdError", updModalContainer);
                }
                else if (txtNewPassword.Text != txtRetypeNewPassword.Text)
                {
                    Utilities.showAlert("Password entered does not match.",
                        "divChangePwdError", updModalContainer);
                }
                else
                {
                    USER_TYPE eUserType;
                    string struserId, strLoginName;
                    getUserTypeFromQueryString(hidQueryString.Value,
                       out eUserType, out strLoginName, out struserId);
                    SaveNewPassword objSaveNewPassword =
                                new SaveNewPassword(
                                    struserId,
                                    strLoginName,
                                    txtNewPassword.Text.Trim(),
                                    eUserType,
                                    this.Context);
                    objSaveNewPassword.Process();

                    if (objSaveNewPassword.StatusCode == 0)
                    {
                        Utilities.closeModalPopUp("divModalContainer", updModalContainer);
                        Utilities.showAlert(
                            "Password changed successfully.",
                            "content",
                            updModalContainer);
                    }
                    else
                    {
                        //show error message that there was some internal error
                        Utilities.showAlert(
                            "Internal server error.Please try again.",
                            "content",
                            updModalContainer);
                    }
                }
            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    Utilities.showAlert(
                          getErrorMessageFromCode(ex.Message),
                         "content", updResetPwdInfo, "Error"); ;
                }
                else
                {
                    Utilities.showAlert(
                          ex.Message,
                         "content", updResetPwdInfo, "Error");
                }
            }
            catch
            {
                Utilities.showAlert(
                        "Internal server error.",
                        "content", updResetPwdInfo, "Error");
            }
        }

        void showDetailsInControls()
        {
            string strLoginName, strUserId, strUserType = String.Empty;
            getValuesFromQueryString(hidQueryString.Value, out strLoginName, out strUserType, out strUserId);
            lblUserName.Text = strLoginName;
        }

        #region Error Message Helpers
        string getErrorMessageFromCode(string errorCode)
        {
            string strErrorMsg = String.Empty;
            ResetPassword_ERROR_CODES eErrorCode =
               (ResetPassword_ERROR_CODES)Enum.Parse(typeof(ResetPassword_ERROR_CODES), errorCode);
            if (Enum.IsDefined(typeof(ResetPassword_ERROR_CODES), eErrorCode))
            {
                switch (eErrorCode)
                {
                    case ResetPassword_ERROR_CODES.InvalidUserType:
                        strErrorMsg = "Invalid link.";
                        break;
                    case ResetPassword_ERROR_CODES.InvalidQueryString:
                        strErrorMsg = "Invalid link.";
                        break;
                }
            }
            else
            {
                throw new MficientException(((int)ResetPassword_ERROR_CODES.InvalidErrorCode).ToString());
            }
            return strErrorMsg;
        }
        bool isErrorADefinedErrorCode(string error)
        {
            bool isDefined = false;
            try
            {
                ResetPassword_ERROR_CODES eErrorCode =
               (ResetPassword_ERROR_CODES)Enum.Parse(typeof(ResetPassword_ERROR_CODES), error);
                if (Enum.IsDefined(typeof(ResetPassword_ERROR_CODES), eErrorCode))
                {
                    isDefined = true;
                }
                else
                    isDefined = false;
            }
            catch
            {
                isDefined = false;
            }
            return isDefined;
        }
        #endregion
        #region Set HTML Controls Value
        void setHidQueryStringValue(string value)
        {
            hidQueryString.Value = value;
        }
        void setHidUsrIdAndType(string value)
        {
            hidUsrIdAndType.Value = value;
        }

        void setHidUserIdAndEmailValue(string userId, string type)
        {
            string strValue =
                Utilities.EncryptString(userId + "&" + type);
            setHidUsrIdAndType(strValue);
        }
        #endregion
    }
}