﻿function showModalPopUp(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == undefined) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    if ($('#' + divToOpen)) {
        ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > a.ui-dialog-titlebar-close').show();
    }
}

function xferBack() {
    document.forms[0].submit();
    return true;
}

function showModalPopUpWithOutHeader(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == undefined) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    if ($('#' + divToOpen)) {
        ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > a.ui-dialog-titlebar-close').hide();
    }
}
function closeModalPopUp(divIdToClose) {
    $('#' + divIdToClose).dialog('close');
}
function showWait() {
    //$("body").addClass("loading");
    $("#divWaitBox").dialog({
        modal: true,
        width: 0,
        height: 0,
        closeOnEscape: false,
        resizable: false,
        autoOpen: true,
        open: function () {
            //$(".ui-dialog-titlebar-close", $(this).parent()).hide(); //hides the little 'x' button
            $("#divWaitBox").siblings(".ui-dialog-titlebar").hide();
        }
    });
    //$("#divWaitBox").height(30).width(230);
}
function hideWait() {
    //$("body").addClass("loading");
    $("#divWaitBox").dialog("close");
    $(".ui-dialog-titlebar").show();
}
//function hideWaitModal() {
//    $("body").removeClass("loading");
//}


DialogType = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showDialogImage(type) {

    var divToPrependImage = getDivToPrependImage();
    if (divToPrependImage) {
        switch (type) {

            case DialogType.Error:
                divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                break;
            case DialogType.Warning:
                divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                break;
            case DialogType.Info:
                divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                break;
        }
    }
}

function getDivToPrependImage() {
    //return $('.ui-dialog-titlebar');
    return $('#wl_dialog_alert').prev();
}
function removeDialogImage() {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove();
}
function hideHTMLElements(commaSeparatedIdsOfElements) {
    var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
    for (var i = 0; i < aryOfElements.length; i++) {
        $('#' + aryOfElements[0]).hide();
    }
}

Repeater =
{
    ADD_USER_USER_DETAIL: 0
}

function showHTMLElements(commaSeparatedIdsOfElements) {
    var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
    for (var i = 0; i < aryOfElements.length; i++) {
        $('#' + aryOfElements[0]).show();
    }
}




function stopDefAction(evt) {
    evt.preventDefault();
}

function spinDiv(_bol) {
    if (_bol) {
        showModalPopUp('spinDiv', '', 0);
    }
    else {
        $('#spinDiv').dialog('close');
    }
}

function showWaitModal() {

    $("#divWaitBox").dialog({
        modal: true,
        closeOnEscape: false,
        resizable: false,
        autoOpen: true
    });
    if ($('#divWaitBox').length > 0) {
        if ($($('#divWaitBox')[0].parentElement).length > 0) {
            $($('#divWaitBox')[0].parentElement).width(60);
            setModalPosition($('#divWaitBox').parent());
            $($('#divWaitBox')[0].previousElementSibling).hide();
            $($('#divWaitBox')[0].parentElement).addClass("waitBoxCorner");
        }
    }
}

function hideWaitModal() {
    //spinDiv(false);
    $("#divWaitBox").dialog("close");
}

function setModalPosition(divToSet) {

    PgDim = getPageSize();
    var vleft = ((PgDim[2] - divToSet.width()) / 2);
    var vtop = ((PgDim[3] - divToSet.height()) / 2);
    divToSet.offset({ top: vtop, left: vleft });
}

function st(obj) {
    return o(obj).style;
}


function getPageSize() {
    var xScroll, yScroll;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    }
    // all but Explorer Mac 
    else if (document.body.scrollHeight > document.body.offsetHeight) {
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    }
    // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari 
    else {
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    // all except Explorer
    if (self.innerHeight) {
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    }
    // Explorer 6 Strict Mode 
    else if (document.documentElement && document.documentElement.clientHeight) {
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }
    // other Explorers 
    else if (document.body) {
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }
    if (yScroll < windowHeight) pageHeight = windowHeight;
    else pageHeight = yScroll;
    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    }
    else {
        pageWidth = xScroll;
    }
    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;


}



function o(obj) {
    if (typeof obj == 'object') {
        return obj;
    }
    else {
        return document.getElementById(obj);
    }
}



function stopDefualtOfDateTxtBox(txtBox) {
    if (txtBox) {
        txtBox.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
    }
}


function rptRowClick(row, repeater) {
    switch (repeater)
    {
        case Repeater.ADD_USER_USER_DETAIL:
        addUserUserDtlRowClick(row);
        break;
    }
    //UpdateUpdBtnRowIndex();
}
function addUserUserDtlRowClick(row) {
    var hidUsedAfterPostBack = $('[id$=hidRowValuesUsedAfterPostBack]');
    var strValuesReqAfterPostBack=''; 
    $.each(row.children, function () {
        if (this.title == "id") {
            var td = this;
            $.each(td.children, function () {
                if (strValuesReqAfterPostBack.length == 0)
                    strValuesReqAfterPostBack +=($.trim(this.innerHTML));
                else
                    strValuesReqAfterPostBack +=($.trim(';' + this.innerHTML));
            })
            hidUsedAfterPostBack.val(strValuesReqAfterPostBack);
        }
    });
    UpdateUserDtlRowClick();
}




