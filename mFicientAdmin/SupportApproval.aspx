﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="SupportApproval.aspx.cs" Inherits="mFicientAdmin.SupportApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divInfomationForApproveDisable">
    </div>
    <div class="clear">
    </div>
    <section>
            <div id="divRepeater" style="margin: 5px;">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                       <section class="clear" style="margin-bottom:5px;">
                            <div id="div1">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>COMPANY DETAILS</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                            
                                <asp:Repeater ID="rptUserDetails" runat="server"  OnItemCommand="rptUserDetails_ItemCommand"  OnItemDataBound="rptUserDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                <th style="display:none;">
                                                CompanyId
                                                </th>
                                                   <th>
                                                  COMPANY
                                                   </th>
                                                    <th>
                                                      MFICIENT SERVER
                                                    </th>
                                                    <th>
                                                      MBUZZ SERVER
                                                    </th>
                                                    <th> MPLUGGIN SERVER</th>
                                                    <th>
                                                    </th>
                                               
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                           <td style="display:none;">
                                           <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                           </td>
                                               <td >
                                                    <asp:Label ID="lblcompany" runat="server" Text='<%#Eval("Company") %>'></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:Label ID="lblserver" runat="server" Text='<%#Eval("Server Name") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmbuzzserver" runat="server" Text='<%# Eval("MBuzz Server Name") %>'></asp:Label>
                                                </td>
                                              <td>
                                                    <asp:Label ID="lblmplugginserver" runat="server" Text='<%# Eval("MPluggin Server") %>'></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:LinkButton ID="lnkchange" runat="server" Text="Change" CssClass="repeaterLink" CommandName="Change"
                                                        OnClick="lnkchange_Click"></asp:LinkButton>
                                                
                                                <asp:LinkButton ID="lnkallote" runat="server" Text="Allote" CssClass="repeaterLink" CommandName="Allote"
                                                        OnClick="lnkallote_Click"></asp:LinkButton>
                                                        </td>
                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                          
                                          <td style="display:none;">
                                           <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                           </td>

                                               <td >
                                                    <asp:Label ID="lblcompany" runat="server" Text='<%#Eval("Company") %>'></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:Label ID="lblserver" runat="server" Text='<%#Eval("Server Name") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmbuzzserver" runat="server" Text='<%# Eval("MBuzz Server Name") %>'></asp:Label>
                                                </td>
                                               <td>
                                                    <asp:Label ID="lblmplugginserver" runat="server" Text='<%# Eval("MPluggin Server") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:LinkButton ID="lnkchange" runat="server" Text="Change" CssClass="repeaterLink" CommandName="Change"
                                                        OnClick="lnkchange_Click"></asp:LinkButton>
                                                
                                                
                                                    <asp:LinkButton ID="lnkallote" runat="server" Text="Allote" CssClass="repeaterLink" CommandName="Allote"
                                                        OnClick="lnkallote_Click"></asp:LinkButton>
                                                        </td>
                                                
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                           
                        </section>
                       <section>
                            
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPreviousServerId" runat="server" />
                              <asp:HiddenField ID="hidPreviousMbuzzId" runat="server" />
                        </div>
                        

                        </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    <div class="modalPopUpDetails">
        <div id="divUserDtlsModal" style="display: none">
            <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="ProcImgUpd">
                        <div id="DivUserDetails" style="width: 85%;">
                            <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                <fieldset>
                                    <div id="lbl" runat="server" style="margin-bottom: 20px;">
                                        <div style="float: left; margin-right: 30px;">
                                            <asp:Label ID="label8" Text="<strong>Company Name</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left;">
                                            <asp:Label ID="lname" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-right: 10px; width: 100px; margin-top: 5px;">
                                            <asp:Label ID="label1" Text="<strong>mFicient Server</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left;">
                                            <asp:DropDownList ID="ddlserver" runat="server" style="opacity:0;width:100px;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-right: 35px; margin-top: 5px;">
                                            <asp:Label ID="label2" Text="<strong>Mbuzz Server</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left;">
                                            <asp:DropDownList ID="ddlmbuzz" runat="server"  style="opacity:0;width:100px;">
                                            </asp:DropDownList>
                                        </div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                          <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-right: 20px; margin-top: 5px;">
                                            <asp:Label ID="label5" Text="<strong>MPluggin Server</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left; ">
                                            <asp:DropDownList ID="ddlmpluggin" runat="server" style="opacity:0;width:100px;">
                                            </asp:DropDownList>
                                        </div>
                                        </div>

                                </fieldset>
                                <div style="position: relative; left: 33px; bottom: 10px;">
                                    <asp:LinkButton ID="lnkAddNewUser" runat="server" Text="Save" CssClass="repeaterLink fr"
                                        OnClick="btnSave_Click"></asp:LinkButton>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
     <script type="text/javascript">
         Sys.Application.add_init(application_init);
         function application_init() {
             //Sys.Debug.trace("Application.Init");
             var prm = Sys.WebForms.PageRequestManager.getInstance();
             prm.add_initializeRequest(prm_initializeRequest);
             prm.add_endRequest(prm_endRequest);
         }
         function prm_initializeRequest() {
             // $("input").uniform();
             showWaitModal();
         }
         function prm_endRequest() {
             //  $("input").uniform();
             hideWaitModal();
         }
    </script>
</asp:Content>
