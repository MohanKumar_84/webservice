﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class TeamList1 : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;




            if (!Page.IsPostBack)
            {
               /* if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/

                if (string.IsNullOrEmpty(hfsValue))
                    Response.Redirect(@"Default.aspx");
                string Role = strRole.ToUpper();
                if (Role == "SM")
                {
                    BindRepeaterSalesMgr();
                }
                else if (Role == "S")
                {
                    SMstatus.Visible = false;
                    BindRepeaterReseller();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
            }
            //BindRepeaterSalesMgr();
        }

        void BindRepeaterSalesMgr()
        {
            GetTeamListAdmin get = new GetTeamListAdmin();
            string MgrId = get.GetMgrId(strUserName);
            DataTable dt = get.SalesPersonList(MgrId);
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Alloted Sales Executive List</h1>";
                rptResellerDetails.Visible = false;
                rptSalesDetails.Visible = true;
                rptSalesDetails.DataSource = dt;
                rptSalesDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no Sales Executive are added by sales manager yet</h1>";
                rptSalesDetails.Visible = false;
                rptSalesDetails.Visible = false;
            }
        }

        void BindRepeaterReseller()
        {
            GetTeamListAdmin get = new GetTeamListAdmin();
            string SalesId = get.GetSalesId(strUserName);
            DataTable dt = get.ResellerList(SalesId);
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Alloted Reseller List</h1>";
                rptSalesDetails.Visible = false;
                rptResellerDetails.Visible = true;
                rptResellerDetails.DataSource = dt;
                rptResellerDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no Reseller are added by sales executive yet</h1>";
                rptResellerDetails.Visible = false;
                rptSalesDetails.Visible = false;
            }
        }

        protected void radListSelection_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (radListSelection.SelectedValue == "1")
            {
                BindRepeaterSalesMgr();
            }
            else if (radListSelection.SelectedValue == "2")
            {
                BindRepeaterReseller();
            }
        }
    }

}