﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class currentPlanDetails : System.Web.UI.Page
    {
        string strUpgradedPrice, strPlancode, strValidMonths;
        int status;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        ContentPlaceHolder cphPageContent = (ContentPlaceHolder)Page.PreviousPage.Form.FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageContent.FindControl("hfs")).Value;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();", true);
            }
           // if(hfs.Value!=string.Empty)
                //getCurrentPlanDetails(out strPlancode, out strValidMonths);
        }

        //protected void getCurrentPlanDetails(out string _PlanCode, out string _ValidMonths)
        //{
        //    _PlanCode = "";
        //    _ValidMonths = "";
        //    AddCurrentPlan addCurrentPlan = new AddCurrentPlan();
        //    addCurrentPlan.GetMaxWorkFlow();
        //    CurrentPlanDetail currentPlan = new CurrentPlanDetail(hfs.Value.Split(',')[2]);
        //    currentPlan.Process();
        //    DataTable dtCurrentPlanDtl = currentPlan.PlanDetails;
        //    if (dtCurrentPlanDtl != null)
        //    {
        //        if (dtCurrentPlanDtl.TableName == "Plan")
        //        {
        //            lblWorkflow.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["MAX_WORKFLOW"]);
        //            lblChargePerMonth.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["USER_CHARGE_PM"]);
        //            lblChargeType.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["CHARGE_TYPE"]);
        //            lblMaxUsers.Text = Convert.ToString(dtCurrentPlanDtl.Rows[0]["MAX_USER"]);
        //            _PlanCode = Convert.ToString(dtCurrentPlanDtl.Rows[0]["PLAN_CODE"]);
        //            btnUpgradeUser.Visible = true;
        //            btnNewPlan.Visible = false;
        //            btnAddUser.Visible = true;
        //            btnRenewPlan.Visible = true;
        //            if (lblWorkflow.Text == addCurrentPlan.HighestWorkflow)
        //            {
        //                btnUpgradeUser.Visible = false;
        //            }
        //            GetPlanList objPlanList = new GetPlanList(_PlanCode);
        //            objPlanList.Process();
        //            DataTable dtblPlanDtls = objPlanList.PlanDetails;
        //            if (dtblPlanDtls != null)
        //            {
        //                if (dtblPlanDtls.Rows.Count > 0)
        //                {
        //                    lblCurrentPlanName.Text = Convert.ToString(dtblPlanDtls.Rows[0]["PLAN_NAME"]);
        //                }
        //            }
        //            DateTime purchaseDate = new DateTime(Convert.ToInt64(dtCurrentPlanDtl.Rows[0]["PURCHASE_DATE"].ToString()));
        //            DateTime validPeriod = purchaseDate.AddMonths(Convert.ToInt32(dtCurrentPlanDtl.Rows[0]["VALIDITY"].ToString()));
        //            TimeSpan time = validPeriod.Subtract(DateTime.Now);
        //            _ValidMonths = (time.Days / 30.4368499).ToString("N2");
        //            lblValidityDate.Text = validPeriod.ToLongDateString();
        //        }
        //        else
        //        {
        //            lblCurrentPlanName.Text = "Trial";
        //            lblMaxUsers.Text = ((int)TRIAL_ACCOUNT_VALIDATION.MAX_USER).ToString();
        //            lblWorkflow.Text = ((int)TRIAL_ACCOUNT_VALIDATION.MAX_WORKFLOW).ToString();
        //            DateTime dt = new DateTime(Convert.ToInt64(dtCurrentPlanDtl.Rows[0]["REGISTRATION_DATETIME"])).AddDays((int)TRIAL_ACCOUNT_VALIDATION.VALIDITY);
        //            lblValidityDate.Text = dt.ToLongDateString();
        //            btnAddUser.Visible = false;
        //            btnUpgradeUser.Visible = false;
        //            btnNewPlan.Visible = true;
        //            btnRenewPlan.Visible = false;
        //            lblChargePerMonth.Text = "0";
        //        }
        //    }
        //}

        protected void GetDiscountPrice(string validMonths)
        {
            bool flag = false;
            GetPlanList objPlanList = new GetPlanList();
            objPlanList.PlanDiscounts();
            DataTable dtblPlanDiscounts = objPlanList.PlanDiscountList;
            foreach (DataRow row in dtblPlanDiscounts.Rows)
            {
                if (Convert.ToBoolean(row["ENABLED"].ToString()) == true)
                {
                    string fromMonth = row["FROM_MONTH"].ToString();
                    string toMonth = row["TO_MONTH"].ToString();
                    if (validMonths == fromMonth || validMonths == toMonth)
                    {
                        lblAllowableDiscount.Text = row["DISCOUNT"].ToString();
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            if (!flag)
            {
                lblAllowableDiscount.Text = "0";
            }
        }

        protected void CalculatePayableAmount()
        {
            if (txtAddUsers.Text == "")
            {
                txtAddUsers.Text = "1";
            }
            lblPrice.Text = (Convert.ToDecimal(txtAddUsers.Text) * Convert.ToDecimal(lblChargePerMonth.Text) * Convert.ToDecimal(lblValidMonths.Text)).ToString();
            lblDiscountAmount.Text = "0";
            lblPayableAmount.Text = lblPrice.Text;
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            CurrentPlanDetailsDiv.Visible = false;
            AddUserDiv.Visible = true;
            //getCurrentPlanDetails(out strPlancode, out strValidMonths);
            lblValidMonths.Text = strValidMonths;
            GetDiscountPrice(lblValidMonths.Text);
            CalculatePayableAmount();
        }

        protected void AddCurrentPlanPurchaseHistory(string _planCode, out int status)
        {
            AddCurrentPlanPurchaseHistory addCurrentPlanPurchaseHistory = new AddCurrentPlanPurchaseHistory();
            addCurrentPlanPurchaseHistory.CompanyId = hfs.Value.Split(',')[2];
            addCurrentPlanPurchaseHistory.ChargePM = lblChargePerMonth.Text;
            addCurrentPlanPurchaseHistory.UserChargeType = lblChargeType.Text;
            addCurrentPlanPurchaseHistory.MaxUsers = txtAddUsers.Text;
            addCurrentPlanPurchaseHistory.MaxWorkFlow = lblWorkflow.Text;
            addCurrentPlanPurchaseHistory.Validity = lblValidMonths.Text;
            addCurrentPlanPurchaseHistory.PriceWithoutDiscount = lblPrice.Text;
            addCurrentPlanPurchaseHistory.DiscountPercent = lblAllowableDiscount.Text;
            addCurrentPlanPurchaseHistory.ActualPrice = lblPayableAmount.Text;
            addCurrentPlanPurchaseHistory.TransactionType = "Add Users";
            addCurrentPlanPurchaseHistory.AdminId = hfs.Value.Split(',')[3];
            addCurrentPlanPurchaseHistory.PlanCode = _planCode;
            addCurrentPlanPurchaseHistory.TotalUsers = (Convert.ToInt32(txtAddUsers.Text) + Convert.ToInt32(lblMaxUsers.Text)).ToString();
            addCurrentPlanPurchaseHistory.Process();
            status = addCurrentPlanPurchaseHistory.StatusCode;
        }

        protected void txtAddUsers_TextChanged(object sender, EventArgs e)
        {
            CalculatePayableAmount();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //getCurrentPlanDetails(out strPlancode, out strValidMonths);
            try
            {
                AddCurrentPlanPurchaseHistory(strPlancode, out status);
                if (status == 0)
                {
                    Utilities.showMessage("User Details have been Added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    //getCurrentPlanDetails(out strPlancode,out strValidMonths);
                    AddUserDiv.Visible = false;
                    CurrentPlanDetailsDiv.Visible = true;
                }
                else
                {
                    Utilities.showMessage("User addition unsuccessful.Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
            catch// (Exception ex)
            {
                Utilities.showMessage("User addition unsuccessful.Please try again", this.Page, "second script", DIALOG_TYPE.Warning);

            }
        }

        protected void btnUpgradeUser_Click(object sender, EventArgs e)
        {
            BindPlansDropDown();
            UpgradeUserDiv.Visible = true;
            AddUserDiv.Visible = false;
            CurrentPlanDetailsDiv.Visible = false;
            CalculateUpgradedAmount(out strUpgradedPrice);
        }

        protected void AddPlanPurchaseHistoryForUserUpgrade(string _PlanCode)
        {
            CalculateUpgradedAmount(out strUpgradedPrice);
            AddCurrentPlanPurchaseHistory addCurrentPlanPurchaseHistory = new AddCurrentPlanPurchaseHistory();
            addCurrentPlanPurchaseHistory.CompanyId = hfs.Value.Split(',')[2];
            addCurrentPlanPurchaseHistory.ChargePM = strUpgradedPrice;
            addCurrentPlanPurchaseHistory.UserChargeType = lblChargeType.Text;
            addCurrentPlanPurchaseHistory.MaxUsers = lblMaxUsers.Text;
            addCurrentPlanPurchaseHistory.MaxWorkFlow = lblMaxWorkflow.Text;
            addCurrentPlanPurchaseHistory.Validity = lblMonths.Text;
            addCurrentPlanPurchaseHistory.PlanCode = _PlanCode;
            addCurrentPlanPurchaseHistory.PriceWithoutDiscount = "0";
            addCurrentPlanPurchaseHistory.DiscountPercent = "0";
            addCurrentPlanPurchaseHistory.ActualPrice = lblAmount.Text;
            addCurrentPlanPurchaseHistory.TransactionType = "Upgrade User";
            addCurrentPlanPurchaseHistory.AdminId = hfs.Value.Split(',')[3];
            addCurrentPlanPurchaseHistory.TotalUsers = lblMaxUsers.Text;
            addCurrentPlanPurchaseHistory.Process();
        }

        public void BindPlansDropDown()
        {
            UpgradeCurrentPlan objPlanList = new UpgradeCurrentPlan();
            objPlanList.GetPlanList(lblWorkflow.Text);
            DataTable dt = objPlanList.PlanList;
            ddlPlans.DataSource = dt;
            ddlPlans.DataValueField = "PLAN_CODE";
            ddlPlans.DataTextField = "PLAN_NAME";
            ddlPlans.DataBind();
        }

        public void CalculateUpgradedAmount(out string strPlanPrice)
        {
            strPlanPrice = "";
            //getCurrentPlanDetails(out strPlancode, out strValidMonths);
            lblMonths.Text = strValidMonths;
            GetPlanList objPlanList = new GetPlanList(ddlPlans.SelectedValue);
            objPlanList.Process();
            DataTable dtblPlanDtls = objPlanList.PlanDetails;
            if (dtblPlanDtls != null)
            {
                if (dtblPlanDtls.Rows.Count > 0)
                {
                    lblMaxWorkflow.Text = Convert.ToString(dtblPlanDtls.Rows[0]["MAX_WORKFLOW"]);
                    if (lblChargeType.Text == "INR")
                    {
                        strPlanPrice = Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_INR_PM"]);
                    }
                    else if (lblChargeType.Text == "USD")
                    {
                        strPlanPrice = Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_USD_PM"]);
                    }
                    decimal priceDiff = Convert.ToDecimal(strPlanPrice) - Convert.ToDecimal(lblChargePerMonth.Text);
                    lblAmount.Text = (Convert.ToDecimal(lblMonths.Text) * priceDiff * Convert.ToDecimal(lblMaxUsers.Text)).ToString();
                }
            }
        }

        protected void ddlPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateUpgradedAmount(out strUpgradedPrice);
        }

        protected void btnUpgrade_Click(object sender, EventArgs e)
        {
            CalculateUpgradedAmount(out strUpgradedPrice);
            AddPlanPurchaseHistoryForUserUpgrade(ddlPlans.SelectedValue);
            UpgradeCurrentPlan upgradeUserPlan = new UpgradeCurrentPlan();
            upgradeUserPlan.CompanyId = hfs.Value.Split(',')[2];
            upgradeUserPlan.ChargePM = strUpgradedPrice;
            upgradeUserPlan.MaxWorkFlow = lblMaxWorkflow.Text;
            upgradeUserPlan.PlanCode = ddlPlans.SelectedValue;
            upgradeUserPlan.Process();
            if (upgradeUserPlan.StatusCode == 0)
            {
                Utilities.showMessage("User upgraded successfully", this.Page, "second script", DIALOG_TYPE.Info);
                //getCurrentPlanDetails(out strPlancode, out strValidMonths);
                UpgradeUserDiv.Visible = false;
                CurrentPlanDetailsDiv.Visible = true;
            }
            else
            {
                Utilities.showMessage("User upgradation unsuccessful.Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
            }
        }

        protected void lnkBack_Click(object sender, EventArgs e)
        {
            UpgradeUserDiv.Visible = false;
            CurrentPlanDetailsDiv.Visible = true;
            AddUserDiv.Visible = false;
        }
    }
}