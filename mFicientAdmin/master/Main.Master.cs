﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mFicientAdmin.master
{
    public partial class Main : System.Web.UI.MasterPage
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            try
            {
                hfsValue = context.Items["hfs"].ToString();

                if (string.IsNullOrEmpty(hfsValue)) return;

                strUserName = context.Items["hfs1"].ToString();
                strSessionId = context.Items["hfs2"].ToString();
                strRole = context.Items["hfs3"].ToString();
            }
            catch (Exception ex)
            {
                return;
            }

            hfs.Value = hfsValue;

            if (!Page.IsPostBack)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", "makeSelectedPageMenuActive();", true);
            }

        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            string Role = hfs.Value.Split(',')[2].ToUpper();

            LogoutUsersAdmin objLogout = new LogoutUsersAdmin();
            string ResellerId = objLogout.GetResellerId(hfs.Value.Split(',')[0]);
            string SalesId = objLogout.GetUserId(hfs.Value.Split(',')[0], "S");
            string SMid = objLogout.GetUserId(hfs.Value.Split(',')[0], "SM");
            string SupportId = objLogout.GetUserId(hfs.Value.Split(',')[0], "SUPP");
            string AccountId = objLogout.GetUserId(hfs.Value.Split(',')[0], "ACC");

            if (Role == "R")
            {
                objLogout.Process(ResellerId);
                Response.Redirect("~/Default.aspx");
            }

            else if (Role == "S")
            {
                objLogout.Process(SalesId);
                Response.Redirect("~/Default.aspx");
            }

            else if (Role == "SM")
            {
                objLogout.Process(SMid);
                Response.Redirect("~/Default.aspx");
            }

            else if (Role == "SUPP")
            {
                objLogout.Process(SupportId);
                Response.Redirect("~/Default.aspx");
            }

            else if (Role == "ACC")
            {
                objLogout.Process(AccountId);
                Response.Redirect("~/Default.aspx");
            }

            else if (Role == "A")
            {
                UserLogOutAdmin objUserLogout = new UserLogOutAdmin();
                string UserId = objUserLogout.GetUserId(hfs.Value.Split(',')[0]);
                objUserLogout.Process(UserId, hfs.Value.Split(',')[1]);
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void btnPasswordSave_Click(object sender, EventArgs e)
        {

        }

        protected void btnPasswordCancel_Click(object sender, EventArgs e)
        {

        }
        //Tried to open the modal pop up.Didn't worked
        protected void lbChangePwd_Click(object sender, EventArgs e)
        {
            showPasswordModalPopUp("Change Password");
        }
        void showPasswordModalPopUp(string modalPopUpHeader)
        {
            //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPopUp", @"showModalPopUp('divChangePasswordContainer','" + modalPopUpHeader + "',400);", true);

            ClientScriptManager cs = Page.ClientScript;
            Type cstype = this.GetType();
            String csname1 = "PasswordPopupScript";
            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                String cstext1 = @"showModalPopUp('divChangePasswordContainer','" + modalPopUpHeader + "',400);";
                //String cstext1 = "alert('Hello World');";
                cs.RegisterStartupScript(cstype, csname1, cstext1, true);
            }
        }

        public LinkButton Logout
        {
            get
            {
                return lbLogout;
            }
        }

        protected void lbChangePwd_Click1(object sender, EventArgs e)
        {

        }

        protected void hidMenu_ValueChanged(object sender, EventArgs e)
        {

        }

    }
}