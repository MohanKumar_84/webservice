﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientAdminWS
{
    public class AddmBuzzServerResp
    {
        ResponseStatus _respStatus;
        string _requestId, _serverId;

        public AddmBuzzServerResp(ResponseStatus respStatus, string requestId, string serverId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _serverId = serverId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Adding mBuzzServer Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            AddmBuzzServerResponse objAddmBuzzServerResp = new AddmBuzzServerResponse();
            objAddmBuzzServerResp.func = Convert.ToString((int)FUNCTION_CODES.ADD_MBUZZ_SERVER);
            objAddmBuzzServerResp.rid = _requestId; ;
            objAddmBuzzServerResp.srvid = _serverId;
            objAddmBuzzServerResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<AddmBuzzServerResponse>(objAddmBuzzServerResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class AddmBuzzServerResponse : CommonResponse
    {
        public AddmBuzzServerResponse()
        {

        }
        /// <summary>
        /// Server ID
        /// </summary>
        [DataMember]
        public string srvid { get; set; }
    }
}