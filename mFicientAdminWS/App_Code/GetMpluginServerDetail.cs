﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdminWS
{
    public class GetMpluginServerDetail
    {
        string _statusDescription;
        int _statusCode;
        #region Public Properties
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }


        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        #endregion
        public List<MFEmPluginServer> getAllMpluginServers()
        {
            List<MFEmPluginServer> lstMpluginServers =
                    new List<MFEmPluginServer>();
            try
            {
                string strQuery = this.getAllMpluginServersQuery();
                SqlCommand cmd = this.getCmdTogetAllMpluginServer(strQuery);
                DataSet dsMpluginServer = MSSqlClient.SelectDataFromSqlCommand(cmd);
                if (dsMpluginServer == null)
                    throw new Exception(
                        ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()
                        );
                foreach (DataRow row in dsMpluginServer.Tables[0].Rows)
                {
                    lstMpluginServers.Add(getMpluginServerFromDatarow(row));
                }
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return lstMpluginServers;
        }
        public MFEmPluginServer getDefaultMpluginServer()
        {
            MFEmPluginServer objMpluginServer =
                new MFEmPluginServer();
            try
            {
                string strQuery = this.getDefaultMpluginServerQuery();
                SqlCommand cmd = this.getCmdTogetDefaultMpluginServer(strQuery);
                DataSet dsMpluginServer = MSSqlClient.SelectDataFromSqlCommand(cmd);
                if (dsMpluginServer == null &&
                   dsMpluginServer.Tables[0].Rows.Count == 0)//should get a record
                    throw new Exception(
                        ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()
                        );
                objMpluginServer = this.getMpluginServerFromDatarow(
                    dsMpluginServer.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return objMpluginServer;
        }
        string getAllMpluginServersQuery()
        {
            return @"SELECT * FROM [ADMIN_TBL_MST_MP_SERVER] WHERE IS_ENABLED = 1;";
        }
        SqlCommand getCmdTogetAllMpluginServer(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            return cmd;
        }
        string getDefaultMpluginServerQuery()
        {
            return @"SELECT * FROM [ADMIN_TBL_MST_MP_SERVER]
                     WHERE IS_DEFAULT =@IS_DEFAULT AND IS_ENABLED = 1;";
        }
        SqlCommand getCmdTogetDefaultMpluginServer(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@IS_DEFAULT", 1);
            return cmd;
        }
        MFEmPluginServer getMpluginServerFromDatarow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEmPluginServer objMpluginServer = new MFEmPluginServer();
            objMpluginServer.ServerId = Convert.ToString(row["MP_SERVER_ID"]);
            objMpluginServer.ServerName = Convert.ToString(row["MP_SERVER_NAME"]);
            objMpluginServer.ServerIpAddress = Convert.ToString(row["MP_SERVER_IP_ADDRESS"]);
            objMpluginServer.ServerPort = Convert.ToString(row["MP_SERVER_PORT"]);
            objMpluginServer.IsEnabled = Convert.ToBoolean(row["IS_ENABLED"]);
            objMpluginServer.ServerURL = Convert.ToString(row["MP_HTTP_URL"]);
            objMpluginServer.IsDefault = Convert.ToBoolean(row["IS_DEFAULT"]);
            return objMpluginServer;
        }
    }
}