﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
namespace mFicientAdminWS
{
    public class GetmPluginServerDetails
    {
        GetmPluginServerDetailsReq _companyServerDtlsReq;
        public GetmPluginServerDetails(GetmPluginServerDetailsReq companyServerDtlsReq)
        {
            _companyServerDtlsReq = companyServerDtlsReq;
        }

        public GetmPluginServerDetailsResp Process()
        {
            DataSet dsServerDetails = getServerDetailsByCompanyId();
            ResponseStatus objResponseStatus = new ResponseStatus();
            if (dsServerDetails != null && dsServerDetails.Tables.Count > 0)
            {
                if (dsServerDetails.Tables[0].Rows.Count != 0)
                {
                    objResponseStatus.cd = "0";
                    objResponseStatus.desc = "";
                }
                else
                {
                    //record not found error
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            else
            {
                //record not found error
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            return new GetmPluginServerDetailsResp(objResponseStatus, _companyServerDtlsReq.RequestId, (string)dsServerDetails.Tables[0].Rows[0]["MP_HTTP_URL"], (string)dsServerDetails.Tables[0].Rows[0]["MP_SERVER_PORT"]);
        }

        DataSet getServerDetailsByCompanyId()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_SERVER_MAPPING  AS a INNER JOIN ADMIN_TBL_MST_MP_SERVER AS c
                                            ON a.MPLUGIN_SERVER_ID = c.MP_SERVER_ID WHERE COMPANY_ID = @CompanyId");

            cmd.Parameters.AddWithValue("@CompanyId", _companyServerDtlsReq.CompanyId);
            cmd.CommandType = CommandType.Text;
            DataSet dsServerDtls = Utilities.SelectDataFromSQlCommand(cmd);
            return dsServerDtls;
        }
    }
}