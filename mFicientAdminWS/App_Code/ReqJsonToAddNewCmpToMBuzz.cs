﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    [DataContract]
    public class ReqJsonToAddNewCmpToMBuzz
    {
        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public string mrid { get; set; }

        [DataMember]
        public string type { get; set; }

        [DataMember]
        public ReqJsonToAddNewCmpToMBuzzData data { get; set; }
    }

    public class ReqJsonToAddNewCmpToMBuzzData
    {
        [DataMember]
        public string ennm { get; set; }

        [DataMember]
        public string surl { get; set; }

    }
}