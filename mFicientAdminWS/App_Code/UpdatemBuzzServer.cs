﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdminWS
{
    public class UpdatemBuzzServer
    {
        UpdatemBuzzServerReq updatemBuzzServerReq;

        public UpdatemBuzzServer(UpdatemBuzzServerReq _updatemBuzzServerReq)
        {
            updatemBuzzServerReq = _updatemBuzzServerReq;
        }

        public UpdatemBuzzServerResp Process()
        {
            SqlCommand cmd = null;
            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {
                cmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_MBUZZ_SERVER WHERE MBUZZ_SERVER_NAME = @MBUZZ_SERVER_NAME AND MBUZZ_SERVER_ID != @MBUZZ_SERVER_ID;SELECT * FROM ADMIN_TBL_MBUZZ_SERVER WHERE SERVER_IP = @SERVER_IP AND MBUZZ_SERVER_ID != @MBUZZ_SERVER_ID");

                cmd.Parameters.AddWithValue("@MBUZZ_SERVER_NAME", updatemBuzzServerReq.ServerName);
                cmd.Parameters.AddWithValue("@SERVER_IP", updatemBuzzServerReq.ServerIP);
                cmd.Parameters.AddWithValue("@MBUZZ_SERVER_ID", updatemBuzzServerReq.ServerId);

                DataSet dsServerDtls = Utilities.SelectDataFromSQlCommand(cmd);

                if (dsServerDtls != null && dsServerDtls.Tables[0].Rows.Count > 0)
                {
                    objRespStatus.cd = "210022";
                    objRespStatus.desc = "Server Name Already Exist.";
                }
                else if (dsServerDtls != null && dsServerDtls.Tables[1].Rows.Count > 0)
                {
                    objRespStatus.cd = "210021";
                    objRespStatus.desc = "Server IP Already Exist";

                }
                else
                {
                    cmd = new SqlCommand(@"UPDATE ADMIN_TBL_MBUZZ_SERVER SET MBUZZ_SERVER_NAME = @MBUZZ_SERVER_NAME , SERVER_IP = @SERVER_IP , PORT_NUMBER = @PORT_NUMBER WHERE MBUZZ_SERVER_ID= @MBUZZ_SERVER_ID");
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@MBUZZ_SERVER_ID", updatemBuzzServerReq.ServerId);
                    cmd.Parameters.AddWithValue("@MBUZZ_SERVER_NAME", updatemBuzzServerReq.ServerName);
                    cmd.Parameters.AddWithValue("@SERVER_IP", updatemBuzzServerReq.ServerIP);
                    cmd.Parameters.AddWithValue("@PORT_NUMBER", updatemBuzzServerReq.ServerPort);

                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";

                    if (Utilities.ExecuteNonQueryRecord(cmd) < 0)
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
            return new UpdatemBuzzServerResp(objRespStatus, updatemBuzzServerReq.RequestId);
        }
    }
}