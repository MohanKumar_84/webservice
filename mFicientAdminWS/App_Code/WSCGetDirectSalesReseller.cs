﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdminWS
{
    public class WSCGetDirectSalesReseller
    {
        public static string getDirectSalesResellerId()
        {
            string strResellerId = String.Empty;
            try
            {
                string strQuery = @"SELECT * FROM ADMIN_TBL_RESELLER_DETAIL
                                     WHERE IS_DIRECT_SALES = 1 ";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                DataSet dsResellerDtl = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsResellerDtl == null) throw new Exception("No record found");
                if (dsResellerDtl.Tables[0].Rows.Count > 0)
                {
                    strResellerId = Convert.ToString(dsResellerDtl.Tables[0].Rows[0]["RESELLER_ID"]);
                }
                else
                {
                    strResellerId = String.Empty;
                }
            }
            catch
            {
                strResellerId = String.Empty;
            }
            return strResellerId;
        }
    }
}