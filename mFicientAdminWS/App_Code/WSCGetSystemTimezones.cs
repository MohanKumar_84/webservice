﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
using System.Data;
namespace mFicientAdminWS
{
    public static class WSCGetSystemTimezones
    {
        public static ReadOnlyCollection<TimeZoneInfo> getSystemTimezones()
        {
            ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();
            return zones;
        }
    }
}