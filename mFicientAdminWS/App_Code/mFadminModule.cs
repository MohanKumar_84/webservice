﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
namespace mFicientAdminWS
{
    public class mFadminModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
            /*clean up*/
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(Application_BeginRequest);
            context.EndRequest += new EventHandler(Application_EndRequest);

        }

        #endregion
        //changed on 4/6/2012
        //earlier intFunctionCode was in both application_beginrequest and application_endrequest as a local variable
        //required a global scope as it was used in Application_EndRequest As well
        int intFunctionCode = 0;
        private void Application_BeginRequest(object sender, EventArgs e)
        {
            string strRequestID = string.Empty, strAccessCode = string.Empty, cid = string.Empty;
            string  strResponseXml = string.Empty, dataJson = string.Empty;

            int intHttpStatusCode = (int)HttpStatusCode.OK;

            HttpContext appContext = ((HttpApplication)sender).Context;

            try
            {
                if (appContext.Request.HttpMethod == "GET")
                {
                    dataJson = appContext.Request.QueryString.Get("d");
                }
                else if (appContext.Request.HttpMethod == "POST")
                {
                    dataJson = appContext.Request.Form.Get("d");
                }
                else
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                if (string.IsNullOrEmpty(dataJson))
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }

                //strIPAddress = appContext.Request.UserHostAddress;
                //strHostName = appContext.Request.UserHostName;

                GetAuthenticationParams(dataJson, out intFunctionCode,  out strRequestID, out cid,out strAccessCode);//Check And Get Values Of FunctionCode,RequestId,Email

                if (!Enum.IsDefined(typeof(FUNCTION_CODES), intFunctionCode))
                {
                    intHttpStatusCode = (int)HttpStatusCode.NotImplemented;
                    return;
                }

                object objRequest = GetRequestObject(intFunctionCode, dataJson);

                if (objRequest == null)
                {
                    throw new Exception(Utilities.INVALID_REQUEST_JSON.ToString());
                }
                else
                {
                    if (intFunctionCode.ToString().StartsWith("2"))
                    {
                        if (Utilities.CheckAuthenticationCode(strAccessCode))
                            strResponseXml = DoWebServiceAction(objRequest, intFunctionCode, appContext);
                        else
                            throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
                    }
                    else
                    {
                        strResponseXml = DoWebServiceAction(objRequest, intFunctionCode, appContext);
                    }
                    if (strResponseXml == string.Empty)
                        throw new Exception();
                }
            }
            catch (Exception ex)
            {
                int intParseResult = 0;
                if (int.TryParse(ex.Message, out intParseResult))
                {
                    if (intParseResult == (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    }
                    else if (intParseResult == (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR && intFunctionCode == (int)FUNCTION_CODES.COMPANY_SERVER_DETAIL)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    else if (intParseResult == (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.NoContent;
                    }
                    else if (intParseResult == Utilities.INVALID_REQUEST_JSON)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
                else
                {
                    if (ex.Source == "App_Code.-b38ecmt" || ex.Source == "System.Runtime.Serialization")
                    {
                        intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
            }
            finally
            {
                appContext.Items.Add("HTTP_STATUS_CODE", intHttpStatusCode);
                appContext.Items.Add("RESPONSE_XML", strResponseXml);
            }
        }

        private void Application_EndRequest(object sender, EventArgs e)
        {
            //intFunctionCode was moved to class level scope.
            //Used in both application_beginrequest and application_endrequest
            string strUserID = string.Empty, strRequestID = string.Empty, strResponse = string.Empty;
            int intHttpStatusCode = 500;

            HttpContext appContext = ((HttpApplication)sender).Context;
            intFunctionCode = Convert.ToInt32(appContext.Items["FUNCTION_CODE"]);
            strResponse = appContext.Items["RESPONSE_XML"].ToString();
            try
            {
                intHttpStatusCode = Convert.ToInt32(appContext.Items["HTTP_STATUS_CODE"]);
            }
            catch
            {
                intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
            }

            if (intHttpStatusCode == (int)HttpStatusCode.OK || intHttpStatusCode == (int)HttpStatusCode.Accepted)
            {
               appContext.Response.Write(strResponse);
            }
            appContext.Response.StatusCode = intHttpStatusCode;
        }


        #region PrivateFunctions

        //private void SaveRequestDetail(string _ResponseXml, string _Email, int _FunctionCode, string _RequestID, string _Response)
        //{
        //    long intDateTime = DateTime.Now.Ticks;

        //    SqlCommand objSqlCommand = new SqlCommand();
        //    objSqlCommand.CommandType = CommandType.Text;

        //    objSqlCommand.CommandText = "INSERT INTO ADMIN_TBL_WEB_SERVICE_REQUEST([USER_ID],[LAST_REQUEST_ID],[LAST_REQUEST_DATETIME] ,[LAST_REQUEST_CODE]," +
        //                                "[LAST_REQUEST_XML],[REQUEST_COUNT]) VALUES (@USER_ID ,@LAST_REQUEST_ID ,@LAST_REQUEST_DATETIME " +
        //                                " ,@LAST_REQUEST_CODE ,@LAST_REQUEST_XML,0);";

        //    objSqlCommand.Parameters.AddWithValue("@USER_ID", _Email);
        //    objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_ID", _RequestID);
        //    objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_DATETIME", intDateTime);
        //    objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_CODE", _FunctionCode);
        //    objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_XML", _Response);

        //    Utilities.ExecuteNonQueryRecord(objSqlCommand);
        //}

        //private void UpdateRequestDetail(string _ResponseXml, string _Email, int _FunctionCode, string _RequestID)
        //{
        //    long intDateTime = DateTime.Now.Ticks;

        //    SqlCommand objSqlCommand = new SqlCommand();
        //    objSqlCommand.CommandType = CommandType.Text;

        //    objSqlCommand.CommandText = "update ADMIN_TBL_WEB_SERVICE_REQUEST set [REQUEST_COUNT]=[REQUEST_COUNT]+1 where [EMAIL]=@Email and LAST_REQUEST_ID=@LAST_REQUEST_ID and LAST_REQUEST_CODE=@LAST_REQUEST_CODE";

        //    objSqlCommand.Parameters.AddWithValue("@Email", _Email);
        //    objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_ID", _RequestID);
        //    objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_CODE", _FunctionCode);

        //    Utilities.ExecuteNonQueryRecord(objSqlCommand);
        //}

        //private string CheckRequestIDAndGetPreviousResponse(string _USERID, string _RequestID, int _FunctionCode)
        //{
        //    try
        //    {
        //        SqlCommand objSqlCommand = new SqlCommand();
        //        objSqlCommand.CommandType = System.Data.CommandType.Text;
        //        string strRequestTimeout = (600000000 * (long)CONSTANT_VARIABLES.REQUEST_TIMEOUT).ToString();//within 3 mins

        //        objSqlCommand.CommandText = @"Select LAST_REQUEST_XML from ADMIN_TBL_WEB_SERVICE_REQUEST where LAST_REQUEST_ID=@LAST_REQUEST_ID and LAST_REQUEST_CODE=@LAST_REQUEST_CODE and USER_ID=@USER_ID and @REQUEST_DATETIME > LAST_REQUEST_DATETIME-" + strRequestTimeout;
        //        objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_ID", _RequestID);
        //        objSqlCommand.Parameters.AddWithValue("@LAST_REQUEST_CODE", _FunctionCode);
        //        objSqlCommand.Parameters.AddWithValue("@USER_ID", _USERID);
        //        objSqlCommand.Parameters.AddWithValue("@REQUEST_DATETIME", DateTime.Now.Ticks);

        //        DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
        //        if (objDataSet.Tables[0].Rows.Count > 0)
        //            return objDataSet.Tables[0].Rows[0][0].ToString();
        //        else
        //            return string.Empty;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Invalid Operation [" + ex.Message + "]");
        //    }
        //}

        private object GetRequestObject(int _FunctionCode, string _dataJson)
        {
            try
            {
                switch (_FunctionCode)
                {
                    case (int)FUNCTION_CODES.COMPANY_SERVER_DETAIL:
                        return new GetCompanyServerDetailsReq(_dataJson);

                    case (int)FUNCTION_CODES.MPLUGIN_SERVER_DETAIL:
                        return new GetmPluginServerDetailsReq(_dataJson);
                   
                    case (int)FUNCTION_CODES.GET_LATEST_APP_VERSION:
                        return new GetLatestAppVersionReq(_dataJson);

                    case (int)FUNCTION_CODES.ADD_MBUZZ_SERVER:
                        return new AddmBuzzServerReq(_dataJson);

                    case (int)FUNCTION_CODES.UPDATE_MBUZZ_SERVER:
                        return new UpdatemBuzzServerReq(_dataJson);

                    case (int)FUNCTION_CODES.COUNTRY_LIST:
                        return new GetCountryListReq(_dataJson);

                    case (int)FUNCTION_CODES.TIMEZONE_LIST:
                        return new GetAllTimezonesReq(_dataJson);

                    case (int)FUNCTION_CODES.COMPANY_REGISTRATION:
                        return new CompanyRegistrationReq(_dataJson);
                    
                }
                return null;
            }
            catch
            {
                throw new Exception(Utilities.INVALID_REQUEST_JSON.ToString());
            }
        }

        private string DoWebServiceAction(object _RequestObject, int _FunctionCode, HttpContext _Context)
        {
            switch (_FunctionCode)
            {
                
                case (int)FUNCTION_CODES.COMPANY_SERVER_DETAIL:
                    return Utilities.DoCompanyServerDetail(_RequestObject);

                case (int)FUNCTION_CODES.MPLUGIN_SERVER_DETAIL:
                    return Utilities.DoGetmPluginServerDetails(_RequestObject);
               
                case (int)FUNCTION_CODES.GET_LATEST_APP_VERSION:
                    return Utilities.DoGetLatestAppVersion(_RequestObject);

                case (int)FUNCTION_CODES.ADD_MBUZZ_SERVER:
                    return Utilities.DoAddmBuzzServer(_RequestObject);

                case (int)FUNCTION_CODES.UPDATE_MBUZZ_SERVER:
                    return Utilities.DoUpdatemBuzzServer(_RequestObject);

                case (int)FUNCTION_CODES.COUNTRY_LIST:
                    return Utilities.DoGetCountryList(_RequestObject);
                case (int)FUNCTION_CODES.COMPANY_REGISTRATION:
                    return Utilities.DoCompanyRegistration(_RequestObject, _Context);
                case (int)FUNCTION_CODES.TIMEZONE_LIST:
                    return Utilities.DoGetTimezoneList(_RequestObject);
                
            }
            return string.Empty;
        }

        #endregion
        private void GetAuthenticationParams(string _RequestJson, out int _FunctionCode, out string _RequestID, out string _Cid, out string _Pwd)
        {
            _FunctionCode = 0;
            _RequestID = string.Empty;
            _Cid = string.Empty;
            _Pwd = string.Empty;
            if (_RequestJson.Length > 0)
            {
                RequestJsonParsing<Data> jsonObject = Utilities.DeserialiseJson<RequestJsonParsing<Data>>(_RequestJson);

                _FunctionCode = int.Parse(jsonObject.req.func);
                _RequestID = jsonObject.req.rid != null ? jsonObject.req.rid : "";
                _Pwd = jsonObject.req.pwd != null ? jsonObject.req.pwd : "";
                if (jsonObject.req.data != null)
                {
                    _Cid = jsonObject.req.data.enid != null ? jsonObject.req.data.enid : "";
                }
            }
        }

    }
}