﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="AdditionalProperties.aspx.cs" Inherits="mFicientCP.AdditionalProperties" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <link href="css/jquery.datatables.css" rel="stylesheet" type="text/css" />
    <link href="css/IdeDialog.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #tblAddProp td, #tblDdlOption td, #tblAuthentication td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tblAddProp td:last-child, #tblAuthentication td:last-child
        {
            text-align: right;
            vertical-align: middle;
            width: 45px;
        }
        .SubProcBtnMrgn1
        {
            margin-top: 7px;
            width: 100%;
            text-align: center;
        }
        .Intext
        {
            float: left;
            margin-top: 4px;
        }
        .btnstyle
        {
            float: right;
        }
    </style>
    <script src="Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script type="text/javascript">
        var TabEnum = {
            // AdditionalProreties: 1,
            AthenticationMeta: 2,
            AmazonMeta: 3
        }
        function makeTabAfterPostBack() {
            $('#content').find('div.tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        }

        function storeSelectedTabIndex(index) {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            hidSelectedTabIndex.value = '';
            hidSelectedTabIndex.value = index;
        }
        function showSelectedTabOnPostBack() {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            //this is the first tab and associated div
            //  var lstAdditionalProreties = $('#lstAdditionalProreties');
            // var divAdditionalProreties = $('#AdditionalProreties');
            //this is the second tab and associated div
            var lstAthenticationMeta = $('#lstAthenticationMeta');
            var divAthenticationMeta = $('#AthenticationMetasDiv');
            //this is the third tab and associated div
            var lstAmazonMeta = $('#lstAmazonCredential');
            var divAmazonMeta = $('#AmazonMetaDiv');

            switch (parseInt(hidSelectedTabIndex.value, 10)) {

                case TabEnum.AthenticationMeta:

                    lstAmazonMeta.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

                    lstAthenticationMeta.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");
                    divAmazonMeta.removeClass("ui-tabs-hide");
                    divAthenticationMeta.removeClass("ui-tabs-hide");
                    divAdditionalProreties.addClass("ui-tabs-hide");
                    break;


                case TabEnum.AmazonMeta:
                    lstAthenticationMeta.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstAmazonMeta.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

                    divAthenticationMeta.removeClass("ui-tabs-hide");

                    divAdditionalProreties.removeClass("ui-tabs-hide");

                    divAmazonMeta.addClass("ui-tabs-hide");
                    break;
            }
        } </script>
   
    <script type="text/javascript">
        function UpdateAmazon() {
            $('[id$=<%=btnamazoncredential.ClientID %>]').click();
        }

        function UpdateUpdBtnRowIndex() {
            $('[id$=<%= btnDbConnRowIndex.ClientID %>]').click();
        }


        function UpdateToDatabase(tag, opration) {

            $('[id$=hidAuthenticationData]').val(JSON.stringify(AuthenticationListData));
            $('[id$=hidUpdateTag]').val(tag + "/" + opration + "/1");
            UpdateUpdBtnRowIndex();
        }

        $(document).ready(function () {

            LoadAuthenticatioPage();
            LoadAmazonPage();
            Uniform();
        });

    </script>

    <script src="Scripts/addtionalproperty.js" type="text/javascript"></script>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updContainer" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div id="divAccountSettings">
                    <div class=" widget" id="widget_tabs">
                        <h3 class="handle">
                            Additional Properties Settings</h3>
                        <div id="divTab" class="tab">
                            <ul id="tabList">
                             <%--   <li id="lstAdditionalProreties"><a id="lnkAdditionalProreties" href="#AdditionalProreties"
                                    onclick="storeSelectedTabIndex(TabEnum.DEVICE);">Custom User Properties</a></li>--%>

                                <li id="lstAthenticationMeta"><a id="lnkAthenticationMeta" href="#AthenticationMetaDiv"
                                    onclick="storeSelectedTabIndex(TabEnum.DATA_COMMAND);">Credential</a></li>
                                <li id="lstAmazonCredential"><a id="lntAmazonCredential" href="#AmazonMetaDiv" onclick="storeSelectedTabIndex(TabEnum.AmazonMeta);">
                                    Amazon Credential</a></li>
                            </ul>
                            <asp:UpdatePanel ID="updateaddition" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                <%--    <div id="AdditionalProreties">
                                        <div style="min-height: 300px; margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                            <div id="AddProp">
                                            </div>
                                        </div>
                                    </div>--%>

                                    <div id="AthenticationMetaDiv">
                                        <div style="min-height: 300px; margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                            <div id="divAuthentication">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="AmazonMetaDiv">
                                        <div style="min-height: 300px; margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                            <div id="divAmazon">
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            
            <div>
                <div>
                    <div id="SubProcConfirmBoxMessage">
                        <div>
                            <div>
                                <div class="ConfirmBoxMessage1">
                                    <a id="aCFmessage"></a></a> <a id="aCfmMessage"></a><a id="aCfmmgs"></a>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="SubProcborderDiv">
                                <div class="SubProcBtnMrgn" align="center">
                                    <input id="btnCnfFormSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                                        class="InputStyle" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel runat="server" ID="updDbConnRowIndex" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnDbConnRowIndex" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnamazoncredential" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <asp:Button ID="btnDbConnRowIndex" OnClick="btnDbConnRowIndex_Click" runat="server"
                    CssClass="hide" />
                <asp:Button ID="btnamazoncredential" OnClick="btnDbClick" runat="server" CssClass="hide" />
               <%-- <asp:HiddenField ID="hidData" runat="server" />--%>
                <asp:HiddenField ID="hidUpdateTag" runat="server" />
                <asp:HiddenField ID="hidUserId" runat="server" />
                <asp:HiddenField ID="hidTabSelected" runat="server" />
                <asp:HiddenField ID="hidAuthenticationData" runat="server" />
                <asp:HiddenField ID="hidAmazonData" runat="server" />
                <asp:HiddenField ID="hideditAmazondata" runat="server" />
                <asp:HiddenField ID="hidIsCleanUpRequired" runat="server" />
            </div>
            <div id="divModelCredential" style="display: none">
                <div id="divModelCredentialError">
                </div>
                <div class="modalPopUpDetRowo" style="width: auto">
                    <div class="clear">
                    </div>
                    <div id="divNameCr" class="modalPopUpDetRow" style="width: 97%;">
                        <div class="modalPopUpDetColumn1">
                            Name
                        </div>
                        <div class="modalPopUpDetColumn2">
                            :
                            <asp:Label ID="cr_name" runat="server"></asp:Label><input type="hidden" id="cr_tag" />
                        </div>
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                        <div class="modalPopUpDetColumn1">
                            User Id :</div>
                        <div class="modalPopUpDetColumn2">
                            <input type="text" id="txt_FixedUserId" style="width: 100%" autocomplete="false" />
                        </div>
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                        <div class="modalPopUpDetColumn1">
                            Password :</div>
                        <div class="modalPopUpDetColumn2">
                            <input type="password" name="pwd" style="width: 100%" autocomplete="false" id="txt_FixedPwd">
                        </div>
                    </div>
                    <div class="modalPopUpAction">
                        <div id="div11" class="popUpActionCont">
                            <input id="Button1" onclick="UpdateFixedForAllUserCeredential();" type="button" value="Ok"
                                style="margin-top: 10px" />
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hidusername" runat="server" />
                <asp:HiddenField ID="hidpassword" runat="server" />
            </div>
            <div id="divupdatename" style="display: none; min-height: 70px !important;">
                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 75px">
                        Name :</div>
                    <div class="modalPopUpDetColumn2">
                        <input type="text" id="txtcredentialname" style="width: 100%" autocomplete="false" />
                    </div>
                </div>
                <div class="modalPopUpAction" style="margin-top: 10px">
                    <div id="div4" class="popUpActionCont">
                        <input id="Button2" onclick="UpdateAuthenticationname();" type="button" value="Ok"
                            style="margin-top: 10px" formmethod="post" />
                    </div>
                </div>
            </div>
            
            <div id="divamazonaddrow" style="display: none; min-height: 70px !important;">
                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 127px">
                        Name :</div>
                    <div class="modalPopUpDetColumn2">
                        <input type="text" id="textamazonname" style="width: 100%" autocomplete="false" />
                    </div>
                </div>
                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;" id="extedittextbox">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 127px">
                        Key :</div>
                    <div class="modalPopUpDetColumn2">
                        <input type="text" id="textamazonkey" style="width: 100%" autocomplete="false" />
                    </div>
                </div>

                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px; display:none" id="editkeylablel">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 127px">
                        Key :</div>
                    <div class="modalPopUpDetColumn2">
                         <asp:Label ID="lblamazonkey" runat="server" ></asp:Label>
                    </div>

                    </div>












                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 125px">
                        Type :</div>
                    <div class="modalPopUpDetColumn2">
                        <asp:DropDownList ID="selectamazontype" onchange="AmzonSecretKey(this);" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>


                <div id="divnormaluser">
                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 127px">
                        Access Key :</div>
                    <div class="modalPopUpDetColumn2">
                        <input type="text" id="txtaccesskey" style="width: 100%" autocomplete="false" />
                    </div>
                </div>
                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 127px">
                        Secrate Access Key :</div>
                    <div class="modalPopUpDetColumn2">
                        <input type="text" id="txtsecrectaccesskey" style="width: 100%" autocomplete="false" />
                    </div>
                </div>

                </div>


                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 125px">
                        Service Region :</div>
                    <div class="modalPopUpDetColumn2">
                        <asp:DropDownList ID="DropDownserviceregion" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="divamazonvalue" style="display: none">
                    <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                        <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 125px">
                            Cognito Region :</div>
                        <div class="modalPopUpDetColumn2">
                            <asp:DropDownList ID="DropDownCognitoRegion" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                        <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 125px">
                            Cognito pool Id :</div>
                        <div class="modalPopUpDetColumn2">

                         <input type="text" id="DropDownCognitopoolId" style="width: 100%" autocomplete="false" />

                          
                        </div>
                    </div>
                </div>
                <div class="modalPopUpAction" style="margin-top: 10px">
                    <div id="div3" class="popUpActionCont">
                        <input id="Btnsaveamzon" onclick="SaveAmzonSecreatkey();"
                            type="button" value="Save" style="margin-top: 10px" formmethod="post" />
                    </div>
                </div>
            </div>
            <div id="divWaitBox" class="waitModal">
                <div id="WaitAnim">
                    <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                        AlternateText="Please Wait" BorderWidth="0px" />
                </div>
            </div>
            <script type="text/javascript">
                Sys.Application.add_init(application_init);
                function application_init() {

                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    prm.add_initializeRequest(prm_initializeRequest);
                    prm.add_endRequest(prm_endRequest);
                }
                function prm_initializeRequest() {
                    // $("input").uniform();
                    showWaitModal();
                    isCookieCleanUpRequired('false');
                }
                function prm_endRequest() {
                    //   $("input").uniform();
                    hideWaitModal();
                    isCookieCleanUpRequired('true');
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
