﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.Script.Serialization;

namespace mFicientCP
{
    public partial class AdditionalProperties : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;

            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                AdditionalPropertyTable();
                BindAmazontype();
                BindAazonproperty();
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    AdditionalPropertyTable();
                }
            }
        }


        private void AdditionalPropertyTable()
        {

            GetEnterpriseAdditionalDefinition objGetWsCommand = new GetEnterpriseAdditionalDefinition(hfsPart3);
            if (objGetWsCommand.StatusCode == 0)
            {
                if (objGetWsCommand.ResultTable != null)
                {
                    if (objGetWsCommand.ResultTable.Rows.Count > 0)
                    {
                        hidAuthenticationData.Value = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["AUTHENTICATION_META"]);
                        List<ReplaceUserCredentialForall> lst = Utilities.DeserialiseJson<List<ReplaceUserCredentialForall>>(hidAuthenticationData.Value);
                        hidAuthenticationData.Value = "";
                        if (lst.Count != 0)
                        {
                            foreach (var item in lst)
                            {
                                if (item.typeval == 0)
                                {
                                    item.uid = AesEncryption.AESDecrypt(hfsPart3.ToUpper(), item.uid);
                                    item.pwd = AesEncryption.AESDecrypt(hfsPart3.ToUpper(), item.pwd);
                                    hidusername.Value = item.uid;
                                    hidpassword.Value = item.pwd;
                                }
                                else
                                {
                                    item.uid = string.Empty;
                                    item.pwd = string.Empty;
                                }
                            }
                            var jsonSerialiser = new JavaScriptSerializer();
                            var json = jsonSerialiser.Serialize(lst);
                            hidAuthenticationData.Value = json;
                        }
                        //    string result = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["ADDITIONAL_PARAMETERS_META"]);
                        hidAmazonData.Value = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["AMAZON_ATHENTICATION"]);
                        //   hidData.Value = result;


                    }
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"BindforAll();", true);
                }
                else
                {
                    //  hidData.Value = "";
                    hidAuthenticationData.Value = "";
                }
            }
        }
        protected void btnDbConnRowIndex_Click(object sender, EventArgs e)
        {
            if (this.hidUpdateTag.Value.Length == 0) return;
            string[] stroperation = hidUpdateTag.Value.Split('/');
            if (stroperation[2] == "1" || stroperation[2] == "2")
            {
                if (hidAuthenticationData.Value != "")
                {
                    List<ReplaceUserCredentialForall> lst = Utilities.DeserialiseJson<List<ReplaceUserCredentialForall>>(hidAuthenticationData.Value);
                    hidAuthenticationData.Value = "";
                    if (lst.Count != 0)
                    {
                        foreach (var item in lst)
                        {
                            if (item.typeval == 0)
                            {
                                item.uid = AesEncryption.AESEncrypt(hfsPart3.ToUpper(), item.uid);
                                item.pwd = AesEncryption.AESEncrypt(hfsPart3.ToUpper(), item.pwd);
                            }
                            else
                            {
                                item.uid = string.Empty;
                                item.pwd = string.Empty;
                            }
                        }
                        var jsonSerialiser = new JavaScriptSerializer();
                        var json = jsonSerialiser.Serialize(lst);
                        hidAuthenticationData.Value = json;
                    }
                }
                //     SaveAdditionalProperties(hfsPart3, hidData.Value, hidAuthenticationData.Value, stroperation);
                SaveAdditionalProperties(hfsPart3, "", hidAuthenticationData.Value, stroperation);
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "Uniform();", true);
                // ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "LoadProperyPage();Uniform();", true);
            }
        }

        protected void btnDbClick(object sender, EventArgs e)
        {
            SaveEnterpriseAdditionalDefinition objsave = new SaveEnterpriseAdditionalDefinition(hfsPart3, hidAmazonData.Value, EnterpriseAdditionalDefinitionType.Amazon);
            if (objsave.StatusCode != 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('Data cannot be saved.Internal server error.');$('#aCFmessage').html('');showModalPopUp('SubProcConfirmBoxMessage','Error','350',false); Uniform();", true);
            }
        }

        private void SaveAdditionalProperties(string CompanyId, string _JsonData, string _CrendentialJson, string[] Opration)
        {
            SaveEnterpriseAdditionalDefinition objsave = new SaveEnterpriseAdditionalDefinition(CompanyId, _CrendentialJson, EnterpriseAdditionalDefinitionType.Authentication);
            if (objsave.StatusCode != 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('Data cannot be saved.Internal server error.');$('#aCFmessage').html('');showModalPopUp('SubProcConfirmBoxMessage','Error','350',false);Uniform();", true);
            }
        }

        private void BindAazonproperty()
        {
            foreach (int val in Enum.GetValues(typeof(Amzondropdown)))
            {
                DropDownserviceregion.Items.Add(new ListItem(Enum.GetName(typeof(Amzondropdown), val), val.ToString()));
                DropDownCognitoRegion.Items.Add(new ListItem(Enum.GetName(typeof(Amzondropdown), val), val.ToString()));
            }
        }

        private void BindAmazontype()
        {
            foreach (int val in Enum.GetValues(typeof(AmzonType)))
            {
                selectamazontype.Items.Add(new ListItem(Enum.GetName(typeof(AmzonType), val), val.ToString()));
            }
        }

        public class ReplaceUserCredentialForall
        {
            public string uid { get; set; }
            public string pwd { get; set; }
            public string type { get; set; }
            public int typeval { get; set; }
            public string name { get; set; }
            public string tag { get; set; }
            public string life { get; set; }
            public int lifeval { get; set; }
        }
    }
}