﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace mFicientCP
{
    public class AesEncryption
    {
       public AesEncryption()
        {
        }

       private static string RemoveBom(string p)
       {
           //string BOMMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
           //if (p.StartsWith(BOMMarkUtf8))
           //    p = p.Remove(0, BOMMarkUtf8.Length);
           return p.Replace("\0", "");
       }

       private static string getKeyFromHash(string hash)
       {
           //return string of characters at odd number places
           string strKey = "";
           int index;
           for (index = 0; index <= hash.Length; index++)
           {
               if (index % 2 != 0)
                   strKey += hash.Substring(index, 1);
           }
           //MessageBox.Show("Key " + strKey);
           return strKey;
       }

       private static string getIVFromHash(string hash)
       {
           //return string of characters at even number places
           string strKey = "";
           int index;
           for (index = 0; index <= hash.Length - 1; index++)
           {
               if (index % 2 == 0)
                   strKey += hash.Substring(index, 1);
           }
           //MessageBox.Show("IV " + strKey);
           return strKey;
       }

       public static string AESEncrypt(string key, string _InputString)
       {
           try
           {
               string strKeyHash = Utilities.GetMd5Hash(key.ToLower()).ToLower();
               byte[] ptbytes = Encoding.UTF8.GetBytes(_InputString);
               string strIV = getRandomIV(16);

               MemoryStream ms = new MemoryStream();
               AesManaged AES = new AesManaged();

               AES.KeySize = 128;
               AES.BlockSize = 128;
               AES.Mode = CipherMode.CBC;
               AES.Padding = PaddingMode.PKCS7;

               AES.Key = Encoding.UTF8.GetBytes(strKeyHash);
               AES.IV = Encoding.UTF8.GetBytes(strIV);

               CryptoStream cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write);
               cs.Write(ptbytes, 0, ptbytes.Length);
               cs.Close();

               byte[] ct = ms.ToArray();
               AES.Clear();
               return strIV + Convert.ToBase64String(ct, Base64FormattingOptions.None);
           }
           catch 
           {
               //throw ex;
               return "";
           }
       }

       /// <summary>
       /// Decrypt the encrypted string to plain string
       /// </summary>
       /// <param name="_EncryptedString">encrypted string</param>
       /// <returns>plain string</returns>
       public static string AESDecrypt(string key, string _EncryptedString)
       {
           if (_EncryptedString.Trim().Length <= 16)
               return "";
           try
           {
               string strKeyHash = Utilities.GetMd5Hash(key.ToLower()).ToLower();
               string strIV = _EncryptedString.Substring(0, 16);
               string encryptedString = _EncryptedString.Substring(16, _EncryptedString.Length - 16);
               byte[] bytesArray = Convert.FromBase64String(encryptedString);

               MemoryStream ms = new MemoryStream();
               AesManaged AES = new AesManaged();

               AES.KeySize = 128;
               AES.BlockSize = 128;
               AES.Mode = CipherMode.CBC;
               AES.Padding = PaddingMode.PKCS7;

               AES.Key = Encoding.UTF8.GetBytes(strKeyHash);
               AES.IV = Encoding.UTF8.GetBytes(strIV);

               CryptoStream cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write);

               cs.Write(bytesArray, 0, bytesArray.Length);
               cs.Close();
               byte[] ct = ms.ToArray();

               return Encoding.UTF8.GetString(ct);
           }
           catch 
           {
               //throw ex;
               return "";
           }
       }

       private static string getRandomIV(int length)
       {
           string ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
           Random random = new Random();
           StringBuilder sb = new StringBuilder();
           for (int i = 0; i < length; i++)
               sb.Append(ALLOWED_CHARACTERS[random.Next(ALLOWED_CHARACTERS.Length)]);
           return sb.ToString();
       }
    }
}