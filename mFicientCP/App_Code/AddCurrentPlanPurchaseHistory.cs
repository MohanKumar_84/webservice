﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddCurrentPlanPurchaseHistory
    {
        public AddCurrentPlanPurchaseHistory()
        {

        }
        public void Process()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                objSqlTransaction = objSqlConnection.BeginTransaction();

                string query = @"INSERT INTO TBL_COMPANY_PLAN_PURCHASE_HISTORY(COMPANY_ID,PLAN_CODE,MAX_WORKFLOW,MAX_USER,USER_CHARGE_PM,CHARGE_TYPE,VALIDITY,PRICE_WITHOUT_DISCOUNT,DISCOUNT_PER,ACTUAL_PRICE,PURCHASE_DATE,TRANSACTION_ID,TRANSACTION_TYPE)
                                VALUES(@COMPANY_ID,@PLAN_CODE,@MAX_WORKFLOW,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE,@VALIDITY,@PRICE_WITHOUT_DISCOUNT,@DISCOUNT_PER,@ACTUAL_PRICE,@PURCHASE_DATE,@TRANSACTION_ID,@TRANSACTION_TYPE);";

                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                objSqlCommand.Parameters.AddWithValue("@MAX_USER", Convert.ToInt16(this.MaxUsers));
                objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDecimal(this.ChargePM));
                objSqlCommand.Parameters.AddWithValue("@CHARGE_TYPE", this.UserChargeType);
                objSqlCommand.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(this.Validity));
                objSqlCommand.Parameters.AddWithValue("@PRICE_WITHOUT_DISCOUNT", Convert.ToDecimal(this.PriceWithoutDiscount));
                objSqlCommand.Parameters.AddWithValue("@DISCOUNT_PER", Convert.ToDecimal(this.DiscountPercent));
                objSqlCommand.Parameters.AddWithValue("@ACTUAL_PRICE", Convert.ToDecimal(this.ActualPrice));
                objSqlCommand.Parameters.AddWithValue("@PURCHASE_DATE", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@TRANSACTION_ID", "Transaction");
                objSqlCommand.Parameters.AddWithValue("@TRANSACTION_TYPE", this.TransactionType.Trim());

                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }

                objSqlCommand = new SqlCommand(@"UPDATE TBL_COMPANY_ADMINISTRATOR SET IS_TRIAL = 0 WHERE ADMIN_ID = @ADMIN_ID", objSqlConnection, objSqlTransaction);

                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", AdminId);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }

                objSqlCommand = new SqlCommand(@"UPDATE TBL_COMPANY_CURRENT_PLAN SET MAX_USER = @MAX_USER WHERE COMPANY_ID = @COMPANY_ID", objSqlConnection, objSqlTransaction);

                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                objSqlCommand.Parameters.AddWithValue("@MAX_USER", TotalUsers);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
               
                objSqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }
        public string MaxWorkFlow
        {
            set;
            get;
        }
        public string MaxUsers
        {
            set;
            get;
        }

        public string TotalUsers
        {
            set;
            get;
        }
        public string UserChargeType
        {
            set;
            get;
        }
        public string ChargePM
        {
            set;
            get;
        }
        public string Validity
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string PriceWithoutDiscount
        {
            set;
            get;
        }
        public string DiscountPercent
        {
            set;
            get;
        }
        public string ActualPrice
        {
            set;
            get;
        }
        public string TransactionType
        {
            set;
            get;
        }
    }
}