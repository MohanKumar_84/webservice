﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddODataServiceConnector
    {
       //public AddODataServiceConnector()
       // { 

       // }

       //public AddODataServiceConnector(string _CompanyId, string _SubAdminId, string _ConnectionName, string _EndPointUrl, string _UserName, string _Password, string _ODataContent, string _MpluginAgent, string _Version)
       // {
       //     this.CompanyId = _CompanyId;
       //     this.SubAdminId = _SubAdminId;
       //     this.ConnectionName = _ConnectionName;
       //     this.UserName = _UserName;
       //     this.Password = _Password;
       //     this.EndPointUrl = _EndPointUrl;
       //     this.ODataContent = _ODataContent;
       //     this.MpluginAgent = _MpluginAgent;
       //     this.Version = _Version;
       //     Process();
       // }
       //public AddODataServiceConnector(string _CompanyId, string _SubAdminId, string _ConnectionName, string _EndPointUrl, string _UserName, string _Password, string _HttpAutenticationtype, string _ODataContent, string _MpluginAgent, string _Version)
       //{
       //    this.CompanyId = _CompanyId;
       //    this.SubAdminId = _SubAdminId;
       //    this.ConnectionName = _ConnectionName;
       //    this.UserName = _UserName;
       //    this.Password = _Password;
       //    this.EndPointUrl = _EndPointUrl;
       //    this.ODataContent = _ODataContent;
       //    this.MpluginAgent = _MpluginAgent;
       //    this.Version = _Version;
       //    this.HttpAutenticationtype = _HttpAutenticationtype;
       //    Process1();
       //}

       //public AddODataServiceConnector(string _CompanyId, string _SubAdminId, string _ConnectionName, string _EndPointUrl, string _UserName, string _Password, string _HttpAutenticationtype, string _ODataContent, string _MpluginAgent, string _Version, string _CreatedBy, long _CreatedOn, long _UpdatedOn)
       //{
       //    this.CompanyId = _CompanyId;
       //    this.SubAdminId = _SubAdminId;
       //    this.ConnectionName = _ConnectionName;
       //    this.UserName = _UserName;
       //    this.Password = _Password;
       //    this.EndPointUrl = _EndPointUrl;
       //    this.ODataContent = _ODataContent;
       //    this.MpluginAgent = _MpluginAgent;
       //    this.Version = _Version;
       //    this.HttpAutenticationtype = _HttpAutenticationtype;
       //    this.CreatedBy = _CreatedBy;
       //    this.CreatedOn = _CreatedOn;
       //    this.UpdatedOn = _UpdatedOn;
       //    Process2();
       //}
       public AddODataServiceConnector(string _CompanyId, string _SubAdminId, string _ConnectionName, string _EndPointUrl, string _UserName, string _Password, string _HttpAutenticationtype, string _ODataContent, string _MpluginAgent, string _Version, string _CreatedBy, long _CreatedOn, long _UpdatedOn, string _Cretedtial_Property)
       {
           this.CompanyId = _CompanyId;
           this.SubAdminId = _SubAdminId;
           this.ConnectionName = _ConnectionName;
           this.UserName = _UserName;
           this.Password = _Password;
           this.EndPointUrl = _EndPointUrl;
           this.ODataContent = _ODataContent;
           this.MpluginAgent = _MpluginAgent;
           this.Version = _Version;
           this.HttpAutenticationtype = _HttpAutenticationtype;
           this.CreatedBy = _CreatedBy;
           this.CreatedOn = _CreatedOn;
           this.UpdatedOn = _UpdatedOn;
           this.Cretedtial_Property = _Cretedtial_Property;
           Process3();
       }


//        private void Process()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());
//                string query = @"INSERT INTO TBL_ODATA_CONNECTION(COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,CONNECTION_NAME,
//									ODATA_ENDPOINT,ODATA_CONTENT,MPLUGIN_AGENT,VERSION)
//							VALUES(@COMPANY_ID,@SUBADMIN_ID,@ODATA_CONNECTOR_ID,@CONNECTION_NAME,
//									@ODATA_ENDPOINT,@ODATA_CONTENT,@MPLUGIN_AGENT,@VERSION);";

//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_ENDPOINT", this.EndPointUrl);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_CONTENT", this.ODataContent);
//                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
//                //objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
//                //objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
//                objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
//                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//            }
//        }

//        private void Process1()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());
//                string query = @"INSERT INTO TBL_ODATA_CONNECTION(COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,CONNECTION_NAME,
//									ODATA_ENDPOINT,ODATA_CONTENT,MPLUGIN_AGENT,USER_NAME,PASSWORD,AUTHENTICATION_TYPE,VERSION)
//							VALUES(@COMPANY_ID,@SUBADMIN_ID,@ODATA_CONNECTOR_ID,@CONNECTION_NAME,
//									@ODATA_ENDPOINT,@ODATA_CONTENT,@MPLUGIN_AGENT,@USER_NAME,@PASSWORD,@AUTHENTICATION_TYPE,@VERSION);";

//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_ENDPOINT", this.EndPointUrl);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_CONTENT", this.ODataContent);
//                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
//                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
//                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
//                objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
//                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_TYPE", this.HttpAutenticationtype);
//                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//            }
//        }

//        private void Process2()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());
//                string query = @"INSERT INTO TBL_ODATA_CONNECTION(COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,CONNECTION_NAME,
//									ODATA_ENDPOINT,ODATA_CONTENT,MPLUGIN_AGENT,USER_NAME,PASSWORD,AUTHENTICATION_TYPE,VERSION,CREATED_BY,CREATED_ON,UPDATED_ON)
//							VALUES(@COMPANY_ID,@SUBADMIN_ID,@ODATA_CONNECTOR_ID,@CONNECTION_NAME,
//									@ODATA_ENDPOINT,@ODATA_CONTENT,@MPLUGIN_AGENT,@USER_NAME,@PASSWORD,@AUTHENTICATION_TYPE,@VERSION,@CREATED_BY,@CREATED_ON,@UPDATED_ON);";

//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_ENDPOINT", this.EndPointUrl);
//                objSqlCommand.Parameters.AddWithValue("@ODATA_CONTENT", this.ODataContent);
//                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
//                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
//                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
//                objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
//                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_TYPE", this.HttpAutenticationtype);
//                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
//                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
//                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
//                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//            }
//        }

        private void Process3()
        {
            try
            {
                this.StatusCode = -1000;
                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());
                string query = @"INSERT INTO TBL_ODATA_CONNECTION(COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,CONNECTION_NAME,
									ODATA_ENDPOINT,ODATA_CONTENT,MPLUGIN_AGENT,AUTHENTICATION_TYPE,VERSION,CREATED_BY,CREATED_ON,UPDATED_ON,CREDENTIAL_PROPERTY)
							VALUES(@COMPANY_ID,@SUBADMIN_ID,@ODATA_CONNECTOR_ID,@CONNECTION_NAME,
									@ODATA_ENDPOINT,@ODATA_CONTENT,@MPLUGIN_AGENT,@AUTHENTICATION_TYPE,@VERSION,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@CREDENTIAL_PROPERTY);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                objSqlCommand.Parameters.AddWithValue("@ODATA_ENDPOINT", this.EndPointUrl);
                objSqlCommand.Parameters.AddWithValue("@ODATA_CONTENT", this.ODataContent);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                //objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
                //objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
                objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_TYPE", this.HttpAutenticationtype);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_PROPERTY", this.Cretedtial_Property);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }

        public string CompanyId { get; set; }
        public string SubAdminId { get; set; }
        public string ConnectionId { get; set; }
        public string ConnectionName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EndPointUrl { get; set; }
        public string ODataContent { get; set; }
        public string MpluginAgent { get; set; }
        public int StatusCode { get; set; }
        public string Version { get; set; }
        public string HttpAutenticationtype { get; set; }
        public string CreatedBy { get; set; }
        public long CreatedOn { get; set; }
        public long UpdatedOn { get; set; }
        public string Cretedtial_Property{ get; set; }
    }
}