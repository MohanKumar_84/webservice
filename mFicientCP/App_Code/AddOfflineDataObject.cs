﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddOfflineDataObject
    {
        private string id, companyId, subAdminId, name, description, query, inputParams, outputParams, tablesUsed, errorMessage;
        private bool allowRetry, exitOnError;
        DatabaseCommandType queryType;
        OfflineDataObject objDataObject;

        public AddOfflineDataObject(string _companyId, string _subAdminId, string _name, string _description, string _query, string _inputParams, string _outputParams, string _tablesUsed, DatabaseCommandType _queryType, bool _allowRetry, bool _exitOnError, string _errorMessage)
        {
            companyId = _companyId;
            subAdminId = _subAdminId;
            name = _name;
            description = _description;
            query = _query;
            inputParams = _inputParams;
            outputParams = _outputParams;
            tablesUsed = _tablesUsed;
            queryType = _queryType;
            allowRetry = _allowRetry;
            exitOnError = _exitOnError;
            errorMessage = _errorMessage;

            try
            {
                process();
                StatusCode = 0;
            }
            catch
            {
                StatusCode = -1;
            }
        }

        public AddOfflineDataObject(OfflineDataObject _objDataObject)
        {
            objDataObject = _objDataObject;

            try
            {
                addObject(null);
                StatusCode = 0;
            }
            catch
            {
                StatusCode = -1;
            }
        }

        public AddOfflineDataObject(OfflineDataObject _objDataObject, string _dataObjectName)
        {
            objDataObject = _objDataObject;

            try
            {
                addObject(_dataObjectName);
                StatusCode = 0;
            }
            catch
            {
                StatusCode = -1;
            }
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string ID
        {
            get
            {
                return id;
            }
        }

        private void process()
        {
            try
            {
                id = Utilities.GetMd5Hash(subAdminId + name + DateTime.UtcNow.Ticks.ToString());
                long updatedOn = 0;
                string strQuery = @"INSERT INTO TBL_OFFLINE_OBJECTS (OFFLINE_OBJECT_ID, COMPANY_ID, SUBADMIN_ID, OFFLINE_OBJECT_NAME, DESCRIPTION, 
                                   QUERY_TYPE, INPUT_PARAMETERS, QUERY, OUTPUTS, OFFLINE_TABLES, CREATED_BY, CREATED_ON, UPDATED_ON, ERROR_MESSAGE, ALLOW_RETRY, EXIT_ON_ERROR) VALUES
                                   (@OFFLINE_OBJECT_ID, @COMPANY_ID, @SUBADMIN_ID,  @OFFLINE_OBJECT_NAME, @DESCRIPTION, @QUERY_TYPE, @INPUT_PARAMETERS,
                                   @QUERY, @OUTPUTS, @OFFLINE_TABLES, @CREATED_BY, @CREATED_ON, @UPDATED_ON, @ERROR_MESSAGE, @ALLOW_RETRY, @EXIT_ON_ERROR)";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_ID", id);
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
                cmd.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_NAME", name);
                cmd.Parameters.AddWithValue("@DESCRIPTION", description);
                cmd.Parameters.AddWithValue("@QUERY_TYPE", ((int)queryType).ToString());
                cmd.Parameters.AddWithValue("@INPUT_PARAMETERS", inputParams);
                cmd.Parameters.AddWithValue("@QUERY", query);
                cmd.Parameters.AddWithValue("@OUTPUTS", outputParams);
                cmd.Parameters.AddWithValue("@OFFLINE_TABLES", tablesUsed);
                cmd.Parameters.AddWithValue("@CREATED_BY", subAdminId);
                cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@UPDATED_ON", updatedOn);
                cmd.Parameters.AddWithValue("@ERROR_MESSAGE", errorMessage);
                cmd.Parameters.AddWithValue("@ALLOW_RETRY", allowRetry);
                cmd.Parameters.AddWithValue("@EXIT_ON_ERROR", exitOnError);
                int result = MSSqlClient.ExecuteNonQueryRecord(cmd);
                if (result <= 0)
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void addObject(string _dataObjectName)
        {
            string dataObjName = string.Empty, dataObjId = string.Empty;
            if (!string.IsNullOrEmpty(_dataObjectName))
            {
                dataObjName = _dataObjectName;
                dataObjId = Utilities.GetMd5Hash(subAdminId + name + DateTime.UtcNow.Ticks.ToString());
            }
            else
            {
                dataObjName = objDataObject.Name;
                dataObjId = objDataObject.ID;
            }

            try
            {
                string strQuery = @"INSERT INTO TBL_OFFLINE_OBJECTS (OFFLINE_OBJECT_ID, COMPANY_ID, SUBADMIN_ID, OFFLINE_OBJECT_NAME, DESCRIPTION, 
                                   QUERY_TYPE, INPUT_PARAMETERS,QUERY,OUTPUTS,OFFLINE_TABLES, CREATED_BY, CREATED_ON, UPDATED_ON, ERROR_MESSAGE, ALLOW_RETRY, EXIT_ON_ERROR) VALUES
                                   (@OFFLINE_OBJECT_ID, @COMPANY_ID, @SUBADMIN_ID,  @OFFLINE_OBJECT_NAME, @DESCRIPTION, @QUERY_TYPE, @INPUT_PARAMETERS,
                                   @QUERY, @OUTPUTS, @OFFLINE_TABLES, @CREATED_BY, @CREATED_ON, @UPDATED_ON, @ERROR_MESSAGE, @ALLOW_RETRY, @EXIT_ON_ERROR)";
                long updatedOn = 0;
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_ID", dataObjId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", objDataObject.CompanyId);
                cmd.Parameters.AddWithValue("@SUBADMIN_ID", objDataObject.SubadminId);
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_NAME", dataObjName);
                cmd.Parameters.AddWithValue("@DESCRIPTION", objDataObject.Description);
                cmd.Parameters.AddWithValue("@QUERY_TYPE", ((int)objDataObject.QueryType).ToString());
                cmd.Parameters.AddWithValue("@INPUT_PARAMETERS", objDataObject.InputParameters);
                cmd.Parameters.AddWithValue("@QUERY", objDataObject.Query);
                cmd.Parameters.AddWithValue("@OUTPUTS", objDataObject.OutputParameters);
                cmd.Parameters.AddWithValue("@OFFLINE_TABLES", objDataObject.OfflineTable);
                cmd.Parameters.AddWithValue("@CREATED_BY", objDataObject.SubadminId);
                cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@UPDATED_ON", updatedOn);
                cmd.Parameters.AddWithValue("@ERROR_MESSAGE", objDataObject.ErrorMessage);
                cmd.Parameters.AddWithValue("@ALLOW_RETRY", objDataObject.AllowRetry);
                cmd.Parameters.AddWithValue("@EXIT_ON_ERROR", objDataObject.ExitOnError);
                int result = MSSqlClient.ExecuteNonQueryRecord(cmd);
                if (result <= 0)
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}