﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class BlockUnblockSubAdmin
    {
        public BlockUnblockSubAdmin()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="BlockOrUnblock">Block or UnBlock</param>
        public BlockUnblockSubAdmin(string adminId, string subAdminId, string blockOrUnblock)
        {
            this.AdminId = adminId;
            this.SubAdminId = subAdminId;
            this.BlockOrUnblock = blockOrUnblock;
        }
        public void Process()
        {
            try
            {
                string query = getSqlQuery();

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;

                objSqlCommand.Parameters.AddWithValue("@AdminId", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    StatusCode = -1000;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusCode = -1000;
                }
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="process">Block or Unblock</param>
        string getSqlQuery()
        {
            string sqlQuery;
            if (BlockOrUnblock.ToLower() == "block")
            {
                sqlQuery = @"UPDATE TBL_SUB_ADMIN
                                SET IS_BLOCKED = 1
                                WHERE ADMIN_ID = @AdminId
                                AND SUBADMIN_ID = @SubAdminId;";
            }
            else
            {
                sqlQuery = @"UPDATE TBL_SUB_ADMIN
                                SET IS_BLOCKED = 0
                                WHERE ADMIN_ID = @AdminId
                                AND SUBADMIN_ID = @SubAdminId;";
            }
            return sqlQuery;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string BlockOrUnblock
        {
            set;
            get;
        }
    }
}