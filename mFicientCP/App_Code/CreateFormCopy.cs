﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data.SqlClient;
//using System.Data;
//using System.Web.UI.WebControls;
//using System.Runtime.Serialization;
//using System.IO;
//using System.Text;

//namespace mFicientCP
//{
//    public class CreateFormCopy
//    {
//        public CreateFormCopy()
//        {

//        }
//        public CreateFormCopy(string _CompanyId, string _SubAdminId, string _CopyToAppId, string _AppId, string _CopyFromId, string _FormName)
//        {
//            this.CompanyId = _CompanyId;
//            this.AppId = _AppId;
//            this.SubAdminId = _SubAdminId;
//            this.CopyFromId = _CopyFromId;
//            this.FormName = _FormName;
//            this.CopyToAppId = _CopyToAppId;
//        }

//        public void Process()
//        {
//            SqlConnection objSqlConnection = null;
//            SqlTransaction objSqlTransaction = null;
//            SqlCommand objSqlCommand;
//            try
//            {
//                this.FormId = Utilities.GetMd5Hash(this.SubAdminId + this.FormName + DateTime.Now.Ticks.ToString());
//                this.StatusCode = -1000;
//                string query;
//                string strChildFormId = "", strName = "" ;
//                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
//                query = @"INSERT INTO TBL_FORM_DETAILS(COMPANY_ID,FORM_ID,FORM_NAME,CREATED_BY,MODIFIED_BY,MODIFIED_DATE,
//						IS_ACTIVE,FORM_JSON,FORM_DESCRIPTION,COMMAND_IDS,MASTER_ID,PARENT_ID,
//						IS_REPEATER_ROWCLICK,MODEL_TYPE) 
//                        SELECT @COMPANY_ID,@FORM_ID,@FORM_NAME,@CREATED_BY,MODIFIED_BY,@MODIFIED_DATE,
//						IS_ACTIVE,FORM_JSON,FORM_DESCRIPTION,COMMAND_IDS,MASTER_ID,@PARENT_ID,
//						IS_REPEATER_ROWCLICK,MODEL_TYPE FROM TBL_FORM_DETAILS fd 
//						where fd.FORM_ID=@COPY_FROM_ID AND fd.COMPANY_ID=@COMPANY_ID AND fd.PARENT_ID=@COPY_FROM_APPID;";

//                objSqlTransaction = objSqlConnection.BeginTransaction();
//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.CopyToAppId);
//                objSqlCommand.Parameters.AddWithValue("@FORM_ID", this.FormId);
//                objSqlCommand.Parameters.AddWithValue("@COPY_FROM_ID", this.CopyFromId);
//                objSqlCommand.Parameters.AddWithValue("@COPY_FROM_APPID", this.AppId);
//                objSqlCommand.Parameters.AddWithValue("@FORM_NAME", this.FormName);
//                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubAdminId);
//                objSqlCommand.Parameters.AddWithValue("@MODIFIED_DATE", DateTime.UtcNow.Ticks);
//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                    this.StatusCode = 0;
//                else
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());

//                for (int i = 1; i <= 3; i++)
//                {
//                    if (i == 1) strName = "Add";
//                    else if (i == 2) strName = "Edit";
//                    else if (i == 3) strName = "View";
//                    else strName = "";
//                    strChildFormId = Utilities.GetMd5Hash(this.SubAdminId + strName + DateTime.Now.Ticks.ToString());
//                    query = @"INSERT INTO TBL_CHILD_FORM_DETAILS
//                            ([COMPANY_ID]
//                            ,[FORM_ID]
//                            ,[FORM_NAME]
//                            ,[CREATED_BY]
//                            ,[MODIFIED_BY]
//                            ,[MODIFIED_DATE]
//                            ,[IS_ACTIVE]
//                            ,[FORM_JSON]
//                            ,[FORM_DESCRIPTION]
//                            ,[COMMAND_IDS]
//                            ,[MASTER_ID]
//                            ,[PARENT_ID]
//                            ,[MODEL_TYPE]
//                            ,[FORM_TYPE]
//                            ,[CONTROL_ID]
//                            ,[WF_ID])
//                        SELECT [COMPANY_ID]
//                        ,@FORM_ID
//                        ,[FORM_NAME]
//                        ,@CREATED_BY
//                        ,[MODIFIED_BY]
//                        ,@MODIFIED_DATE
//                        ,[IS_ACTIVE]
//                        ,[FORM_JSON]
//                        ,[FORM_DESCRIPTION]
//                        ,[COMMAND_IDS]
//                        ,[MASTER_ID]
//                        ,@PARENT_ID
//                        ,[MODEL_TYPE]
//                        ,[FORM_TYPE]
//                        ,[CONTROL_ID]
//                        ,[WF_ID]
//                    FROM [TBL_CHILD_FORM_DETAILS] WHERE WF_ID=@COPY_FROM_APPID AND COMPANY_ID=@COMPANY_ID AND PARENT_ID=@COPY_FROM_ID AND FORM_TYPE=@FORM_TYPE;";
//                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@FORM_ID", strChildFormId);
//                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.FormId);
//                    objSqlCommand.Parameters.AddWithValue("@COPY_FROM_ID", this.CopyFromId);
//                    objSqlCommand.Parameters.AddWithValue("@COPY_FROM_APPID", this.AppId);
//                    objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubAdminId);
//                    objSqlCommand.Parameters.AddWithValue("@MODIFIED_DATE", DateTime.UtcNow.Ticks);
//                    objSqlCommand.Parameters.AddWithValue("@FORM_TYPE", i);

//                    objSqlCommand.ExecuteNonQuery();
//                }

//                objSqlTransaction.Commit();
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000; 
//                if (objSqlTransaction != null)
//                    objSqlTransaction.Rollback();
//            }
//            finally
//            {
//                MSSqlClient.SqlConnectionClose(objSqlConnection);
//            }
//        }


//        public int StatusCode { get; set; }

//        public string CompanyId { get; set; }

//        public string AppId { get; set; }

//        public string SubAdminId { get; set; }

//        public string FormId { get; set; }

//        public string FormName { get; set; }

//        public string CopyFromId { get; set; }

//        public string CopyToAppId { get; set; }
//    }


//}