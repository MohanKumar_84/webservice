﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DatabaseCommand
    {
        #region Private Members

        private string commandId, connectorId, enterpriseId, sqlQuery, returnType, commandName, tableName, mPluginAgent, mPluginAgentPwd, parameter,noExecuteCon,imageColumn, parameterJson, coloumns, description, expirycondition, xreferences, querytype;
        private DatabaseCommandType dbCommandType;
        private DataBaseConnector dbConnector;
        private int dbConnectorType,cache,expiryfrequency=0;

        #endregion

        #region Constructor

        public DatabaseCommand()
        { 
        }

        public DatabaseCommand(string _commandId, string _enterpriseId)
        {
            commandId = _commandId;
            enterpriseId = _enterpriseId;
        }

        #endregion

        #region Public Properties
        public string CommandId
        {
            get
            {
                return commandId;
            }
            set
            {
                commandId = value;
            }

        }
        public string ProcessId
        {
            get;
            set;
        }
        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
            set
            {
                connectorId = value;
            }
        }
        public string Parameterjson
        {
            get
            {
                return parameterJson;
            }
            set
            {
                parameterJson = value;
            }
        }

        public string Columns
        {
            get
            {
                return coloumns;
            }
            set
            {
                coloumns = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public int Cache
        {
            get
            {
                return cache;
            }
            set
            {
                cache = value;
            }

        }
        public int Expiryfrequency
        {
            get
            {
                return expiryfrequency;
            }
            set
            {
                expiryfrequency = value;
            }
        }

        public string ExpiryCondtion
        {
            get
            {
                return expirycondition;
            }
            set
            {
                expirycondition = value;
            }
        }

        public string Xreference
        {
            get
            {
                return xreferences;
            }
            set
            {
                xreferences = value;
            }
        }

        public string Querytype
        {
            get
            {
                return querytype;
            }
            set
            {
                querytype = value;
            }
        }

        public DataBaseConnector DbConnector
        {
            get
            {
                try
                {
                    dbConnector = new DataBaseConnector(connectorId, enterpriseId);
                    dbConnector.GetConnector();
                }
                catch
                {
                }
                return dbConnector;
            }
            set
            {
                dbConnector = value;
            }
        }
        public DatabaseCommandType DBConnectorType
        {
            get
            {
                return dbCommandType;
            }
            set
            {
                dbCommandType = value;
            }
        }
        public int DataBaseCommandType
        {
            get
            {
                return dbConnectorType;
            }
            set
            {
                dbConnectorType = value;
            }
        }

        public string SqlQuery
        {
            get
            {
                return sqlQuery;
            }
            set
            {
                sqlQuery = value;
            }
        }

        public string ReturnType
        {
            get
            {
                return returnType;
            }
            set
            {
                returnType = value;
            }
        }

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
            set
            {
                enterpriseId = value;
            }
        }

        public string CommandName
        {
            get
            {
                return commandName;
            }
            set
            {
                commandName = value;
            }
        }

        public string TableName
        {
            get
            {
                return tableName;
            }
            set
            {
                tableName = value;
            }
        }
        public string MPluginAgent
        {
            get
            {
                return mPluginAgent;
            }
            set
            {
                mPluginAgent = value;
            }
        }
        public string MPluginAgentPwd
        {
            get
            {
                return mPluginAgentPwd;
            }
            set
            {
                mPluginAgentPwd = value;
            }
        }
        
        public string Parameter
        {
            get
            {
                return parameter;
            }
            set
            {
                parameter = value;
            }
        }

        public string ImageColumn
        {
            get
            {
                return imageColumn;
            }
            set
            {
                imageColumn = value;
            }
        }

        public string NoExecuteCon
        {
            get
            {
                return noExecuteCon;
            }
            set
            {
                noExecuteCon = value;
            }
        }
        #endregion

        #region Public Method

        public void GetCommandDetails()
        {
            string strQuery = @"SELECT cmd.*,isnull(ag.MP_AGENT_PASSWORD, '') as MP_AGENT_PASSWORD,con.*  FROM TBL_DATABASE_COMMAND as cmd inner join TBL_database_CONNECTION as con on cmd.db_CONNECTOR_ID=con.db_CONNECTOR_ID and cmd.COMPANY_ID=con.COMPANY_ID 
            left outer join TBL_MPLUGIN_AGENT_DETAIL as ag on ag.MP_AGENT_NAME=con.MPLUGIN_AGENT and ag.COMPANY_ID=con.COMPANY_ID 
            WHERE DB_COMMAND_ID = @DB_COMMAND_ID AND con.COMPANY_ID = @COMPANY_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@DB_COMMAND_ID", commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                connectorId = ds.Tables[0].Rows[0]["DB_CONNECTOR_ID"].ToString();
                mPluginAgentPwd = ds.Tables[0].Rows[0]["MP_AGENT_PASSWORD"].ToString();
                mPluginAgent = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();
                dbConnectorType = Convert.ToInt32(ds.Tables[0].Rows[0]["DATABASE_TYPE"].ToString());
                commandName = ds.Tables[0].Rows[0]["DB_COMMAND_NAME"].ToString();
                returnType = ds.Tables[0].Rows[0]["RETURN_TYPE"].ToString();
                dbCommandType = (DatabaseCommandType)Convert.ToInt32(ds.Tables[0].Rows[0]["DB_COMMAND_TYPE"].ToString());
                tableName = ds.Tables[0].Rows[0]["TABLE_NAME"].ToString();
                sqlQuery = ds.Tables[0].Rows[0]["SQL_QUERY"].ToString();
                commandId = ds.Tables[0].Rows[0]["DB_COMMAND_ID"].ToString();
                cache = Convert.ToInt32(ds.Tables[0].Rows[0]["CACHE"].ToString());
                expiryfrequency = Convert.ToInt32(ds.Tables[0].Rows[0]["EXPIRY_FREQUENCY"].ToString());
                expirycondition = ds.Tables[0].Rows[0]["EXPIRY_CONDITION"].ToString();
                xreferences = ds.Tables[0].Rows[0]["X_REFERENCES"].ToString();
                parameter = ds.Tables[0].Rows[0]["PARAMETER"].ToString();
                returnType = ds.Tables[0].Rows[0]["RETURN_TYPE"].ToString();
                parameterJson = ds.Tables[0].Rows[0]["PARAMETER_JSON"].ToString();
                coloumns = ds.Tables[0].Rows[0]["COLUMNS"].ToString();
                description = ds.Tables[0].Rows[0]["DESCRIPTION"].ToString();
                querytype = ds.Tables[0].Rows[0]["QUERY_TYPE"].ToString();
                imageColumn = ds.Tables[0].Rows[0]["IMAGE_COLUMN"].ToString();
                noExecuteCon = ds.Tables[0].Rows[0]["NO_EXECUTE_CONDITION"].ToString();

                this.DbConnector = new DataBaseConnector(connectorId, enterpriseId);
                DbConnector.ConnectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                DbConnector.MpluginAgentName = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();
                DbConnector.HostName = ds.Tables[0].Rows[0]["HOST_NAME"].ToString();
                DbConnector.Timeout = Convert.ToInt32(ds.Tables[0].Rows[0]["TIME_OUT"].ToString());
                DbConnector.DataBaseType = (DatabaseType)Convert.ToInt32(ds.Tables[0].Rows[0]["DATABASE_TYPE"].ToString());
            }
        }

        #endregion
    }
}