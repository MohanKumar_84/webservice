﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class DeleteActvDirSetting
    {
        #region Private members
        string _domainId, _companyId;
        private enum DELETE_SETTINGS_BY
        {
            CompanyId,
            CompanyAndDomainId,
        }
        DELETE_SETTINGS_BY _deleteCondition;
        #endregion

        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }

        public string DomainId
        {
            get { return _domainId; }
            private set { _domainId = value; }
        }
        private DELETE_SETTINGS_BY DeleteCondition
        {
            get { return _deleteCondition; }
            set { _deleteCondition = value; }
        }
        public int StatusCode
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        #endregion

        #region Constructor
        public DeleteActvDirSetting(string domainId,
            string companyId)
        {
            this.CompanyId = companyId;
            this.DomainId = domainId;
            this.DeleteCondition = DELETE_SETTINGS_BY.CompanyAndDomainId;
        }
        public DeleteActvDirSetting(string companyId)
        {
            this.CompanyId = companyId;
            this.DeleteCondition = DELETE_SETTINGS_BY.CompanyId;
        }
        #endregion

        #region Public Methods
        public void Process()
        {
            try
            {
                switch (this.DeleteCondition)
                {
                    case DELETE_SETTINGS_BY.CompanyId:
                        deleteActvDirSettingByCmpId();
                        break;
                    case DELETE_SETTINGS_BY.CompanyAndDomainId:
                        deleteActvDirSettingByCmpAndDomainId();
                        break;
                }
                
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        #endregion

        #region Private Methods
        void deleteActvDirSettingByCmpAndDomainId()
        {
            string strQuery = @"DELETE FROM TBL_ENTERPRISE_DOMAINS
                                WHERE DOMAIN_ID = @DOMAIN_ID
                                AND COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@DOMAIN_ID", this.DomainId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd);
            if (iRowsEffected == 0) throw new Exception();
        }
        void deleteActvDirSettingByCmpId()
        {
            string strQuery = @"DELETE FROM TBL_ENTERPRISE_DOMAINS
                                WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd);
            //if (iRowsEffected == 0) throw new Exception();
        }
        #endregion
    }
}