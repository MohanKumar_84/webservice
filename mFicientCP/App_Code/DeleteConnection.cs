﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteConnection
    {
       public DeleteConnection()
        { 

        }

       public DeleteConnection(string _SubAdminId, string _ConnectionId, Boolean _IsDbConnection,string _CompanyId)
        {
            this.SubAdminId = _SubAdminId;
            this.ConnectionId = _ConnectionId;
            this.IsDbConnection = _IsDbConnection;
            this.CompanyId = _CompanyId;
        }

     
        public void Process()
       {
           this.StatusCode = -1000;
           SqlConnection objSqlConnection = null;
           SqlTransaction objSqlTransaction = null;
           SqlCommand objSqlCommand;
           try
           {
               MSSqlClient.SqlConnectionOpen(out objSqlConnection);
               string query = "";
               if (this.IsDbConnection) 
               query = @"DELETE FROM TBL_DATABASE_CONNECTION WHERE DB_CONNECTOR_ID=@CONNECTION_ID AND   COMPANY_ID =@COMPANY_ID;";

               else query = @"DELETE FROM TBL_WEBSERVICE_CONNECTION WHERE WS_CONNECTOR_ID=@CONNECTION_ID AND COMPANY_ID =@COMPANY_ID;";

               objSqlTransaction = objSqlConnection.BeginTransaction();
               objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
               objSqlCommand.CommandType = CommandType.Text;
               objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
               objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
              // objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);

               if (objSqlCommand.ExecuteNonQuery() > 0) this.StatusCode = 0;

               objSqlTransaction.Commit();
           }
           catch
           {
               this.StatusCode = -1000;
               if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
           }
           finally
           {
               MSSqlClient.SqlConnectionClose(objSqlConnection);
           }
        }

       

        public string SubAdminId
        {
            set;
            get;
        }
        public string ConnectionId
        {
            set;
            get;
        }
        public Boolean IsDbConnection
        {
            set;
            get;
        }
      
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string CompanyId { get; set; }

       
    }
}