﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class DeleteDevcDtlsFromMGram
    {
        string _enterpriseId, _deviceId, _deviceType, _userName;
        int _statusCode;
        string _statusDescription;
        DELETE_PROCESS_BY _deleteProcessBy;
        enum DELETE_PROCESS_BY
        {
            OnlyUserName,
            WithDeviceAndUsername,
        }
        public DeleteDevcDtlsFromMGram(string companyId,
            string deviceId,
            string deviceType, string userName)
        {
            this.EnterpriseId = companyId;
            this.DeviceId = deviceId;
            this.DeviceType = deviceType;
            this.UserName = userName;
            this.DeleteProcessBy = DELETE_PROCESS_BY.WithDeviceAndUsername;
        }
        public DeleteDevcDtlsFromMGram(string companyId,
            string userName)
        {
            this.EnterpriseId = companyId;
            this.DeviceId = String.Empty;
            this.DeviceType = String.Empty;
            this.UserName = userName;
            this.DeleteProcessBy = DELETE_PROCESS_BY.OnlyUserName;
        }

        public void Process()
        {
            try
            {
                switch (this.DeleteProcessBy)
                {
                    case DELETE_PROCESS_BY.OnlyUserName:
                        deleteDeviceDtlsByUserName();
                        break;
                    case DELETE_PROCESS_BY.WithDeviceAndUsername:
                        deleteDeviceDtlsByDeviceAndUserName();
                        break;

                }

            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        void deleteDeviceDtlsByDeviceAndUserName()
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", this.EnterpriseId);
            cmd.Parameters.AddWithValue("@DEVICE_ID", this.DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", this.DeviceType);
            cmd.Parameters.AddWithValue("@USER_NAME", this.UserName);
            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
        }
        void deleteDeviceDtlsByUserName()
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", this.EnterpriseId);
            cmd.Parameters.AddWithValue("@USER_NAME", this.UserName);
            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
        }
        string getQuery()
        {
            string strQuery = String.Empty;
            switch (this.DeleteProcessBy)
            {
                case DELETE_PROCESS_BY.OnlyUserName:
                    strQuery = @"DELETE FROM TBL_USER_DEVICE
                                WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                                AND USERNAME = @USER_NAME";
                    break;
                case DELETE_PROCESS_BY.WithDeviceAndUsername:
                    strQuery = @"DELETE FROM TBL_USER_DEVICE
                                WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                                AND DEVICE_ID = @DEVICE_ID
                                AND DEVICE_TYPE = @DEVICE_TYPE
                                AND USERNAME = @USER_NAME";
                    break;

            }
            return strQuery;

        }

        #region Public Properties
        public string EnterpriseId
        {
            get { return _enterpriseId; }
            private set { _enterpriseId = value; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
            private set { _deviceId = value; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
            private set { _deviceType = value; }
        }
        public string UserName
        {
            get { return _userName; }
            private set { _userName = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        private DELETE_PROCESS_BY DeleteProcessBy
        {
            get { return _deleteProcessBy; }
            set { _deleteProcessBy = value; }
        }
        #endregion
    }
}