﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class DeleteHtml5App
    {
        public DeleteHtml5App(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public void DeleteApp(string appId)
        {
            try
            {
                string strQuery = @"DELETE FROM [TBL_HTML5_APP_DETAIL]
                                WHERE APP_ID = @APP_ID
                                AND SUBADMIN_ID = @SUBADMIN_ID
                                AND COMPANY_ID = @COMPANY_ID";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
                MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }


        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }

        public int StatusCode
        {
            private set;
            get;
        }

        public string StatusDescription
        {
            private set;
            get;
        }
    }
}