﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteOdataCommand
    {
       public DeleteOdataCommand()
        { 
        }

       public DeleteOdataCommand(string _CommandId, string _SubAdminId, string _CompanyId)
        {

            this.CommandId = _CommandId;
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"DELETE FROM TBL_ODATA_COMMAND 
                                WHERE ODATA_COMMAND_ID=@ODATA_COMMAND_ID AND COMPANY_ID=@COMPANY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }

        public string CommandId { get; set; }
        public string ConnactorId { get; set; }
        public int StatusCode { get; set; }
        public string SubAdminId { get; set; }
        public string CompanyId { get; set; }
    }
}