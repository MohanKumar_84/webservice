﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteUserRoleDetails
    {
       public DeleteUserRoleDetails()
        { 

        }

       public DeleteUserRoleDetails(string _AdminId, string _UserId, string _RoleId)
        {
            this.AdminId = _AdminId;
            this.UserId = _UserId;
            this.RoleId = _RoleId;
        }
        public void Process()
        {
            try
            {
                string query = @"DELETE FROM TBL_USER_ROLE WHERE ADMIN_ID=@ADMIN_ID AND  USER_ID=@USER_ID AND ROLE_ID=@ROLE_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@ROLE_ID", this.RoleId);
                
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public string AdminId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string RoleId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}