﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;
//namespace mFicientCP
//{
//    public class DesktopMessengerDeletedRequest
//    {
//        #region Private Members

//        private string strMsgRefId, strJson, strUserName, strDeviceId, strEnterpriseId;
//        private int deviceCount;

//        private DesktopMessengerDeletedRequestJson _desktopMsgerDeletedReqJson;

//        #endregion

//        #region Public Properties

//        public string RequestJson
//        {
//            get
//            {
//                return strJson;
//            }
//        }

//        #endregion

//        #region Constructor

//        public DesktopMessengerDeletedRequest(string _msgRefId, string _userName, string _deviceId, string _enterpriseId, int _deviceCount)
//        {
//            _desktopMsgerDeletedReqJson = new DesktopMessengerDeletedRequestJson();
//            strMsgRefId = _msgRefId;
//            strUserName = _userName;
//            strDeviceId = _deviceId;
//            strEnterpriseId = _enterpriseId;
//            deviceCount = _deviceCount;
//            strJson = CreateRequestJson();
//        }

//        #endregion

//        #region Private Methods

//        private string CreateRequestJson()
//        {
//            _desktopMsgerDeletedReqJson.mrid = strMsgRefId;
//            _desktopMsgerDeletedReqJson.type = ((int)MessageCode.DESKTOP_MESSENGER_DELETED).ToString();
//            _desktopMsgerDeletedReqJson.unm = strUserName;
//            _desktopMsgerDeletedReqJson.did = strDeviceId;
//            _desktopMsgerDeletedReqJson.enid = strEnterpriseId;
//            _desktopMsgerDeletedReqJson.dcnt = deviceCount.ToString();

//            string json = Utilities.SerializeJson<DesktopMessengerDeletedRequestJson>(_desktopMsgerDeletedReqJson);
//            return Utilities.getRequestJson(json);
//        }

//        #endregion
//    }

//    #region Classes

//    [DataContract]
//    public class DesktopMessengerDeletedRequestJson : RequestJson
//    {
//        public DesktopMessengerDeletedRequestJson()
//        { }

//        [DataMember]
//        public string did { get; set; }

//        [DataMember]
//        public string dcnt { get; set; }
//    }

//    #endregion
    
//}