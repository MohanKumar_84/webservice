﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class DeviceSettings
    {

        public DeviceSettings(string companyId, string userIdsAndDeviceCount)
        {
            this.CompanyId = companyId;
            this.UserIdsAndDeviceCount = userIdsAndDeviceCount;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string[] userIdsAndDeviceCount = UserIdsAndDeviceCount.Split(';');
                DataRow[] rows = usersFromListAlreadyHavingMoreRegistedDevices(userIdsAndDeviceCount[0], Convert.ToInt32(userIdsAndDeviceCount[1]));
                if (rows.Length > 0)
                {
                    string strUserNames = "";
                    for (int i = 0; i < rows.Length; i++)
                    {
                        strUserNames += rows[i]["USER_NAME"] + " ";
                    }
                    StatusDescription = strUserNames + " already have more devices registered";
                    return;
                }
                else
                {
                    int iReturnedValue = saveUsersAndDeviceCount(userIdsAndDeviceCount);
                    if (iReturnedValue == 1)
                    {
                        StatusCode = 0;
                        StatusDescription = "";
                    }
                    else
                    {
                        StatusDescription = "Internal Server Error";
                    }
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal Server Error";
            }

        }

        /// <summary>
        /// Provides the users who has more devices already registered than the count that is provided in the current
        /// request.
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="noOfDevicesToRegister"></param>
        /// <returns></returns>
        DataRow[] usersFromListAlreadyHavingMoreRegistedDevices(string userIds, int noOfDevicesToRegister)
        {
            SqlCommand cmd = new SqlCommand();
            string[] strUserIds = userIds.Split(',');
            string[] parameters = new string[strUserIds.Length];
            for (int i = 0; i < strUserIds.Length; i++)
            {
                parameters[i] = "@p" + i;
                cmd.Parameters.AddWithValue(parameters[i], strUserIds[i]);
            }
            //cmd.Parameters.AddWithValue("@UserIDs", strParameterValue);
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            string strQuery = @"SELECT COUNT(RegDev.USER_ID) AS ALREADY_REGISTERED, RegDev.USER_ID,RegDev.COMPANY_ID,DevSetngs.MAX_DEVICE,UsrDtl.USER_NAME
                                FROM TBL_REGISTERED_DEVICE AS RegDev
                                LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS DevSetngs
                                ON RegDev.USER_ID = DevSetngs.USER_ID
                                INNER JOIN TBL_USER_DETAIL UsrDtl
                                ON UsrDtl.USER_ID = RegDev.USER_ID
                                WHERE RegDev.USER_ID IN (";
            
            strQuery +=string.Join(",",parameters)+")";
            strQuery +="AND RegDev.COMPANY_ID = @CompanyId GROUP BY RegDev.USER_ID,RegDev.COMPANY_ID,DevSetngs.MAX_DEVICE,UsrDtl.USER_NAME";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            DataSet dsUserList = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (dsUserList != null && dsUserList.Tables.Count > 0)
            {
                string strFilter = String.Format("ALREADY_REGISTERED > '{0}'", noOfDevicesToRegister);
                return dsUserList.Tables[0].Select(strFilter);
            }
            else
            {
                return null;
            }

        }

        int saveUsersAndDeviceCount(string[] userIdsAndDeviceCount)
        {
            string []strUserIds = userIdsAndDeviceCount[0].Split(new char[]{','});
            string strNoOfDevicesToRegister = userIdsAndDeviceCount[1];
            string strQuery = @"IF EXISTS(SELECT * FROM TBL_USER_DEVICE_SETTINGS WHERE COMPANY_ID = @CompanyId AND USER_ID = @UserId )
                                UPDATE TBL_USER_DEVICE_SETTINGS
                                SET MAX_DEVICE = @MaxDevice
                                WHERE COMPANY_ID = @CompanyId
                                AND USER_ID = @UserId
                                ELSE
                                INSERT INTO TBL_USER_DEVICE_SETTINGS
                                VALUES(@UserId,@MaxDevice,@CompanyId)";

            SqlTransaction transaction = null;
            SqlConnection con  = null;
            
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        try
                        {
                            for (int i = 0; i < strUserIds.Length; i++)
                            {
                                if (!String.IsNullOrEmpty(strUserIds[i]))
                                {
                                    SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
                                    objSqlCommand.CommandType = CommandType.Text;
                                    objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                                    objSqlCommand.Parameters.AddWithValue("@UserId", strUserIds[i]);
                                    objSqlCommand.Parameters.AddWithValue("@MaxDevice", strNoOfDevicesToRegister);
                                    objSqlCommand.ExecuteNonQuery();
                                }
                            }
                            transaction.Commit();
                            StatusCode = 0;

                        }
                        catch
                        {
                            return (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    return (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                return (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch (Exception e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    return (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                return (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
            return 1;
        }

        public string CompanyId
        {
            set;
            get;
        }
        public string UserIdsAndDeviceCount
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}