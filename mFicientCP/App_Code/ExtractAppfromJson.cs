﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace mFicientCP
{
    public class ExtractAppfromJson
    {
        string companyId;
        List<string> appJson;
        List<DatabaseCommand> hstDBCmds;
        List<WebServiceCommand> hstWSCmds;
        List<OdataCommand> hstODataCmds;
        List<OfflineDataObject> hstOfflineDataObjs;
        List<Tasks> lstAllTasks;
        Hashtable hstDBCon;
        Hashtable hstWSCon;
        Hashtable hstODataCon;
        Hashtable hstOfflineDataTbl;
        JObject appJsonObj;
        List<string> lstAppCmdIds;

        public ExtractAppfromJson(List<string> _appJson, string _companyId)
        {
            appJson = _appJson;
            companyId = _companyId;
            hstDBCmds = new List<DatabaseCommand>();
            hstWSCmds = new List<WebServiceCommand>();
            hstODataCmds = new List<OdataCommand>();
            hstOfflineDataObjs = new List<OfflineDataObject>();
            lstAllTasks = new List<Tasks>();
            hstDBCon = new Hashtable();
            hstWSCon = new Hashtable();
            hstODataCon = new Hashtable();
            hstOfflineDataTbl = new Hashtable();
            lstAppCmdIds = new List<string>();
            try
            {
                foreach (string objAppJson in _appJson)
                {
                    appJsonObj = JObject.Parse(objAppJson);
                    processJsonObject();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public Hashtable DbConnections
        {
            get
            {
                return hstDBCon;
            }
        }

        public Hashtable WSConnections
        {
            get
            {
                return hstWSCon;
            }
        }

        public Hashtable ODataConnections
        {
            get
            {
                return hstODataCon;
            }
        }

        public Hashtable OfflineDataTables
        {
            get
            {
                return hstOfflineDataTbl;
            }
        }

        public string DBCommandsJson
        {
            get
            {
                string strDbCmds = JsonConvert.SerializeObject(hstDBCmds);
                return strDbCmds;
            }
        }

        public string WSCommandsJson
        {
            get
            {
                string strWsCmds = JsonConvert.SerializeObject(hstWSCmds);
                return strWsCmds;
            }
        }

        public string ODataCommandsJson
        {
            get
            {
                string strODataCmds = JsonConvert.SerializeObject(hstODataCmds);
                return strODataCmds;
            }
        }

        public string OfflineDataCommandsJson
        {
            get
            {
                string strOfflineDataCmds = JsonConvert.SerializeObject(hstOfflineDataObjs);
                return strOfflineDataCmds;
            }
        }

        private void processJsonObject()
        {
            var objStartupTasks = JObject.Parse(Convert.ToString(appJsonObj["startUpTasks"]));
            Tasks startupTasks = objStartupTasks.ToObject<Tasks>();
            lstAllTasks.Add(startupTasks);

            JArray array = JArray.Parse(Convert.ToString(appJsonObj["views"]));
            List<Appviews> views = new List<Appviews>();
            foreach (var obj in array)
            {
                views.Add(obj.ToObject<Appviews>());
            }

            foreach (Appviews view in views)
            {
                addTask(view.onExitCmds);
                addTask(view.rowClickTasks);
                addTask(view.cancelTasks);
                addTask(view.actionBtn1Tasks);
                addTask(view.actionBtn2Tasks);
                addTask(view.actionBtn3Tasks);
                getViewActionBtnsTasks(view.viewActionBtns);
            }

            getAllCommandsfromTasks();
        }

        private void getViewActionBtnsTasks(List<ViewActionButton> lstViewActionButtons)
        {
            if (lstViewActionButtons != null && lstViewActionButtons.Count > 0)
            {
                foreach (ViewActionButton viewActnBtn in lstViewActionButtons)
                {
                    addTask(viewActnBtn.onExitTasks);
                }
            }
        }

        private void addTask(Tasks task)
        {
            try
            {
                if (task != null && !lstAllTasks.Contains(task))
                    lstAllTasks.Add(task);
            }
            catch (Exception ex)
            {
            }
        }

        private void getAllCommandsfromTasks()
        {
            List<AppCommands> appCommands;
            if (lstAllTasks != null && lstAllTasks.Count > 0)
            {
                foreach (Tasks task in lstAllTasks)
                {
                    appCommands = new List<AppCommands>();
                    appCommands = task.databindingObjs;
                    if (appCommands.Count > 0)
                    {
                        foreach (AppCommands appCmd in appCommands)
                        {
                            addAppCommandtoCollection(appCmd);
                        }
                    }
                }
            }
        }

        private void addAppCommandtoCollection(AppCommands appCmd)
        {
            GetOfflineDataTableDtl getOfflineTbl;
            MFEOfflineDataTable offlineTbl;

            if (appCmd != null && !lstAppCmdIds.Contains(appCmd.id))
            {
                lstAppCmdIds.Add(appCmd.id);
                switch ((DATABINDING_OBJECT_TYPE)(Convert.ToInt32(appCmd.bindObjType)))
                {
                    case DATABINDING_OBJECT_TYPE.Database:
                        DatabaseCommand dbCmd = new DatabaseCommand(appCmd.id, companyId);
                        dbCmd.GetCommandDetails();
                        if (dbCmd != null)
                        {
                            hstDBCmds.Add(dbCmd);
                            if (!hstDBCon.Contains(dbCmd.DbConnector.ConnectorId))
                                hstDBCon.Add(dbCmd.DbConnector.ConnectorId, dbCmd.DbConnector);
                        }
                        break;
                    case DATABINDING_OBJECT_TYPE.Http:
                    case DATABINDING_OBJECT_TYPE.Wsdl:
                    case DATABINDING_OBJECT_TYPE.XmlRpc:
                        WebServiceCommand wsCmd = new WebServiceCommand(appCmd.id, companyId);
                        wsCmd.GetCommandDetails();
                        if (wsCmd != null)
                        {
                            hstWSCmds.Add(wsCmd);
                            if (!hstWSCon.Contains(wsCmd.ConnectorId))
                                hstWSCon.Add(wsCmd.ConnectorId, wsCmd.WSConnector);
                        }
                        break;
                    case DATABINDING_OBJECT_TYPE.OData:
                        OdataCommand oDataCmd = new OdataCommand(appCmd.id, companyId);
                        oDataCmd.GetCommandDetails();
                        if (oDataCmd != null)
                        {
                            hstODataCmds.Add(oDataCmd);
                            if (!hstODataCon.Contains(oDataCmd.ConnectorId))
                                hstODataCon.Add(oDataCmd.ConnectorId, oDataCmd.OdataConnector);
                        }
                        break;
                    case DATABINDING_OBJECT_TYPE.Offline_Data_Obj:
                        GetOfflineDataObject offlineDataCmd = new GetOfflineDataObject(appCmd.id, companyId);
                        if (offlineDataCmd.DataObject != null)
                        {
                            hstOfflineDataObjs.Add(offlineDataCmd.DataObject);
                            if (!string.IsNullOrEmpty(offlineDataCmd.DataObject.OfflineTable))
                            {
                                getOfflineTbl = new GetOfflineDataTableDtl(companyId);
                                offlineTbl = getOfflineTbl.ProcessGetOfflineDTDtlByTblName(offlineDataCmd.DataObject.OfflineTable);
                                if (offlineTbl != null)
                                {
                                    if (!hstOfflineDataTbl.Contains(offlineTbl.TableId))
                                        hstOfflineDataTbl.Add(offlineTbl.TableId, offlineTbl.TableName);
                                    processOfflineTable(offlineTbl);
                                }
                            }
                        }
                        break;
                }
            }
        }

        private void processOfflineTable(MFEOfflineDataTable offlineDataTbl)
        {
            //switch ((COMMAND_TYPE)Convert.ToInt32(offlineDataTbl.TableType))
            //{
            //    case COMMAND_TYPE.DatabaseCommand:
            //        DatabaseCommand dbCmd = new DatabaseCommand(offlineDataTbl.UploadCommandId, companyId);
            //        dbCmd.GetCommandDetails();
            //        if (dbCmd != null)
            //        {
            //            hstDBCmds.Add(dbCmd);
            //            if (!hstDBCon.Contains(dbCmd.DbConnector.ConnectorId))
            //                hstDBCon.Add(dbCmd.DbConnector.ConnectorId, dbCmd.DbConnector);
            //        }
            //        dbCmd = new DatabaseCommand(offlineDataTbl.DownloadCommandId, companyId);
            //        dbCmd.GetCommandDetails();
            //        if (dbCmd != null)
            //        {
            //            hstDBCmds.Add(dbCmd);
            //            if (!hstDBCon.Contains(dbCmd.DbConnector.ConnectorId))
            //                hstDBCon.Add(dbCmd.DbConnector.ConnectorId, dbCmd.DbConnector);
            //        }
            //        break;
            //    case COMMAND_TYPE.Http:
            //    case COMMAND_TYPE.Wsdl:
            //    case COMMAND_TYPE.XmlRpc:
            //        WebServiceCommand wsCmd = new WebServiceCommand(offlineDataTbl.UploadCommandId, companyId);
            //        wsCmd.GetCommandDetails();
            //        if (wsCmd != null)
            //        {
            //            hstWSCmds.Add(wsCmd);
            //            if (!hstWSCon.Contains(wsCmd.ConnectorId))
            //                hstWSCon.Add(wsCmd.ConnectorId, wsCmd.WSConnector);
            //        }
            //        wsCmd = new WebServiceCommand(offlineDataTbl.DownloadCommandId, companyId);
            //        wsCmd.GetCommandDetails();
            //        if (wsCmd != null)
            //        {
            //            hstWSCmds.Add(wsCmd);
            //            if (!hstWSCon.Contains(wsCmd.ConnectorId))
            //                hstWSCon.Add(wsCmd.ConnectorId, wsCmd.WSConnector);
            //        }
            //        break;
            //    case COMMAND_TYPE.Odata:
            //        OdataCommand oDataCmd = new OdataCommand(offlineDataTbl.UploadCommandId, companyId);
            //        oDataCmd.GetCommandDetails();
            //        if (oDataCmd != null)
            //        {
            //            hstODataCmds.Add(oDataCmd);
            //            if (!hstODataCon.Contains(oDataCmd.ConnectorId))
            //                hstODataCon.Add(oDataCmd.ConnectorId, oDataCmd.OdataConnector);
            //        }
            //        oDataCmd = new OdataCommand(offlineDataTbl.DownloadCommandId, companyId);
            //        oDataCmd.GetCommandDetails();
            //        if (oDataCmd != null)
            //        {
            //            hstODataCmds.Add(oDataCmd);
            //            if (!hstODataCon.Contains(oDataCmd.ConnectorId))
            //                hstODataCon.Add(oDataCmd.ConnectorId, oDataCmd.OdataConnector);
            //        }
            //        break;
            //}
        }
    }
}