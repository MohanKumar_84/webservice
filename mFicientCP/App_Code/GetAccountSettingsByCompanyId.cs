﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetAccountSettingsByCompanyId
    {
        int _maxDevicePerUser, _maxMpluginAgents, _autoLogin = 0;
        bool _allowDeleteCommand = false, _allowActiveDirectoryUser = false,
              _isAutoLoginAllowed = false;

        public GetAccountSettingsByCompanyId(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_ACCOUNT_SETTINGS
                                WHERE COMPANY_ID = @CompanyId";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                AccountSettings = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (AccountSettings != null)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                    DataTable dtblAccountSetting = AccountSettings.Tables[0];
                    if (dtblAccountSetting.Rows.Count > 0)
                    {
                        _maxDevicePerUser = Convert.ToInt32(dtblAccountSetting.Rows[0]["MAX_DEVICE_PER_USER"]);
                        _allowDeleteCommand = Convert.ToBoolean(dtblAccountSetting.Rows[0]["ALLOW_DELETE_COMMAND"]);
                        _allowActiveDirectoryUser = Convert.ToBoolean(dtblAccountSetting.Rows[0]["ALLOW_ACTIVE_DIRECTORY_USER"]);
                        _maxMpluginAgents = Convert.ToInt32(dtblAccountSetting.Rows[0]["MAX_MPLUGIN_AGENTS"]);
                        _autoLogin = Convert.ToInt32(dtblAccountSetting.Rows[0]["AUTO_LOGIN"]);
                        if (_autoLogin == 0)
                        {
                            _isAutoLoginAllowed = false;
                        }
                        else
                        {
                            _isAutoLoginAllowed = true;
                        }
                    }
                }
                else
                {
                    StatusCode = -1000;
                    StatusDescription = "Internal server error";
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }

        }


        public string CompanyId
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public DataSet AccountSettings
        {
            get;
            set;
        }
        public int MaxDevicePerUser
        {
            get { return _maxDevicePerUser; }
        }
        public bool AllowDeleteCommand
        {
            get { return _allowDeleteCommand; }
        }
        public bool AllowActiveDirectoryUser
        {
            get { return _allowActiveDirectoryUser; }
        }
        public int MaxMpluginAgents
        {
            get { return _maxMpluginAgents; }
        }
        public int AutoLogin
        {
            get { return _autoLogin; }
        }
        public bool IsAutoLoginAllowed
        {
            get { return _isAutoLoginAllowed; }
        }
    }
}