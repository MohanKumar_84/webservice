﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetAllMgrmEnterpriseGrps
    {
        public GetAllMgrmEnterpriseGrps(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            selectGroupDtls();
        }
        void selectGroupDtls()
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", this.CompanyId);
            DataSet ds =
                MSSqlClient.SelectDataFromDifferentConnection(strConnectionString,
                cmd
                );
            if (ds != null)
            {
                this.ResultTable = ds.Tables[0];
                this.MgramEnterpriseGroups = new List<MFEMgramEnterpriseGroups>();
                if (ds.Tables[0].Rows.Count > 0)
                    this.MgramEnterpriseGroups = getMgrmGrpDtlFromDatatable(ds.Tables[0]);
            }
        }
        List<MFEMgramEnterpriseGroups> getMgrmGrpDtlFromDatatable(DataTable dtblGroupDtl)
        {
            if (dtblGroupDtl == null) throw new ArgumentNullException();
            List<MFEMgramEnterpriseGroups> lstMgrmEnterpriseGrps =
                new List<MFEMgramEnterpriseGroups>();
            foreach (DataRow row in dtblGroupDtl.Rows)
            {
                lstMgrmEnterpriseGrps.Add(getGroupDetailFromDataRow(row));
            }
            return lstMgrmEnterpriseGrps;
        }
        MFEMgramEnterpriseGroups getGroupDetailFromDataRow(DataRow groupDtlRow)
        {
            if (groupDtlRow == null) throw new ArgumentNullException();
            MFEMgramEnterpriseGroups objMgramEnterpriseGroup =
                new MFEMgramEnterpriseGroups();
            objMgramEnterpriseGroup.EnterpriseId = Convert.ToString(groupDtlRow["ENTERPRISE_ID"]);
            objMgramEnterpriseGroup.GroupName = Convert.ToString(groupDtlRow["GROUP_NAME"]);
            objMgramEnterpriseGroup.Users = Convert.ToString(groupDtlRow["USERS"]);
            objMgramEnterpriseGroup.GroupId = Convert.ToString(groupDtlRow["GROUP_ID"]);
            return objMgramEnterpriseGroup;
        }
        string getQuery()
        {
            return @"SELECT * 
                    FROM TBL_ENTERPRISE_GROUPS 
                    WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                    ;";

        }
        #region Public Properties
        public string GroupId
        {
            get;
            private set;
        }
        public string CompanyId
        {
            get;
            private set;
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public List<MFEMgramEnterpriseGroups> MgramEnterpriseGroups
        {
            get;
            private set;
        }
        #endregion
    }
}