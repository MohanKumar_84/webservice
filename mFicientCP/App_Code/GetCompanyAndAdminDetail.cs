﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetCompanyAndAdminDetail
    {
        private string strAdminId;
        public GetCompanyAndAdminDetail(string _adminId)
        {
            strAdminId = _adminId;
        }

        public void Process()
        {
            try
            {
                string strQuery = "select FIRST_NAME+' '+MIDDLE_NAME+' '+LAST_NAME as 'Administration',IS_TRIAL,REGISTRATION_DATETIME,COMPANY_NAME,REGISTRATION_NO,EMAIL_ID from TBL_COMPANY_ADMINISTRATOR a inner join TBL_COMPANY_DETAIL c on a.ADMIN_ID=c.ADMIN_ID where a.ADMIN_ID=@ADMIN_ID;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", strAdminId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    AdminName = objDataSet.Tables[0].Rows[0]["Administration"].ToString();
                    CompanyName = objDataSet.Tables[0].Rows[0]["COMPANY_NAME"].ToString();
                    RegistrationNumber = objDataSet.Tables[0].Rows[0]["REGISTRATION_NO"].ToString();
                    IsTrial = Convert.ToBoolean(objDataSet.Tables[0].Rows[0]["IS_TRIAL"]);
                    RegistrationDateTimeInTicks = Convert.ToInt64(objDataSet.Tables[0].Rows[0]["REGISTRATION_DATETIME"]);
                    Email = objDataSet.Tables[0].Rows[0]["EMAIL_ID"].ToString();
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
        }
        public string AdminName
        {
            set;
            get;
        }
        public string Email
        {
            set;
            get;
        }
        public string CompanyName
        {
            set;
            get;
        }
        public string RegistrationNumber
        {
            set;
            get;
        }
        public Boolean IsTrial
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public long RegistrationDateTimeInTicks
        {
            set;
            get;
        }
    }
}