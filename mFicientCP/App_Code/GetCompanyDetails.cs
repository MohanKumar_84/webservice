﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetCompanyDetails
    {
        enum RUN_QUERY_BY
        {
            AdminAndCompanyId,
            CompanyId
        }
        RUN_QUERY_BY _eRunQueryBy;
        MFECompanyInfo _cmpInfo;


        public GetCompanyDetails(string adminId, string companyId)
        {
            this.AdminId = adminId;
            this.CompanyId = companyId;
            this.ERunQueryBy = RUN_QUERY_BY.AdminAndCompanyId;
        }
        public GetCompanyDetails(string companyId)
        {
            this.AdminId = String.Empty;
            this.CompanyId = companyId;
            this.ERunQueryBy = RUN_QUERY_BY.CompanyId;
        }

        public void Process()
        {
            try
            {
                SqlCommand cmd = getCmdToRun();
                DataSet dsCompanyDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsCompanyDtls != null)
                {
                    this.CmpInfo = new MFECompanyInfo();
                    if (dsCompanyDtls.Tables[0].Rows.Count > 0)
                    {
                        this.ResultTable = dsCompanyDtls.Tables[0];
                        CompanyId = (string)dsCompanyDtls.Tables[0].Rows[0]["COMPANY_ID"];
                        CompanyName = (string)dsCompanyDtls.Tables[0].Rows[0]["COMPANY_NAME"];
                        Address = (string)dsCompanyDtls.Tables[0].Rows[0]["Address"];
                        CompanyLogoName = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["LOGO_IMAGE_NAME"]);
                        this.TimeZoneId = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["TIMEZONE_ID"]);
                        this.IsBlocked = Convert.ToBoolean(dsCompanyDtls.Tables[0].Rows[0]["IS_BLOCKED"]);
                        this.CmpInfo = fillCmpDtlFromDtbl(this.ResultTable);
                        if (dsCompanyDtls.Tables[1].Rows.Count > 0)
                        {
                            this.ResultTable1 = dsCompanyDtls.Tables[1];
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        SqlCommand getCmdToRun()
        {
            SqlCommand cmd = new SqlCommand(getSqlQuery());
            cmd.CommandType = CommandType.Text;
            switch (this.ERunQueryBy)
            {
                case RUN_QUERY_BY.AdminAndCompanyId:
                    cmd.Parameters.AddWithValue("@AdminId", AdminId);
                    break;
                case RUN_QUERY_BY.CompanyId:
                    cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    break;
            }
            return cmd;
        }
        string getSqlQuery()
        {
            string strQuery = String.Empty;
            switch (this.ERunQueryBy)
            {
                case RUN_QUERY_BY.AdminAndCompanyId:
                    strQuery = @"SELECT *,(STREET_ADDRESS1+' '+STREET_ADDRESS2+' '+STREET_ADDRESS3+' '+CITY_NAME+' '+STATE_NAME+' '+ZIP)as Address
                                    FROM TBL_COMPANY_DETAIL
                                    WHERE ADMIN_ID = @AdminId;
                                   select * from dbo.TBL_COMPANY_ADMINISTRATOR WHERE ADMIN_ID = @AdminId;";
                    break;
                case RUN_QUERY_BY.CompanyId:
                    strQuery = @"SELECT *,(STREET_ADDRESS1+' '+STREET_ADDRESS2+' '+STREET_ADDRESS3+' '+CITY_NAME+' '+STATE_NAME+' '+ZIP)as Address
                                    FROM TBL_COMPANY_DETAIL
                                    WHERE COMPANY_ID = @COMPANY_ID;
select * from dbo.TBL_COMPANY_ADMINISTRATOR WHERE COMPANY_ID = @COMPANY_ID;";
                    break;
            }
            return strQuery;
        }
        MFECompanyInfo fillCmpDtlFromDtbl(DataTable compDetail)
        {
            if (compDetail == null) throw new ArgumentNullException();
            MFECompanyInfo objCmpInfo = new MFECompanyInfo();
            objCmpInfo.CompanyId = Convert.ToString(compDetail.Rows[0]["COMPANY_ID"]);
            objCmpInfo.CompanyName = Convert.ToString(compDetail.Rows[0]["COMPANY_NAME"]);
            objCmpInfo.StreetAddress1 = Convert.ToString(compDetail.Rows[0]["STREET_ADDRESS1"]);
            objCmpInfo.StreetAddress2 = Convert.ToString(compDetail.Rows[0]["STREET_ADDRESS2"]);
            objCmpInfo.StreetAddress3 = Convert.ToString(compDetail.Rows[0]["STREET_ADDRESS3"]);
            objCmpInfo.CityName = Convert.ToString(compDetail.Rows[0]["CITY_NAME"]);
            objCmpInfo.StateName = Convert.ToString(compDetail.Rows[0]["STATE_NAME"]);
            objCmpInfo.Zip = Convert.ToString(compDetail.Rows[0]["ZIP"]);
            objCmpInfo.LogoImageName = Convert.ToString(compDetail.Rows[0]["LOGO_IMAGE_NAME"]);
            objCmpInfo.TimezoneId = Convert.ToString(compDetail.Rows[0]["TIMEZONE_ID"]);
            objCmpInfo.IsBlocked = Convert.ToBoolean(compDetail.Rows[0]["IS_BLOCKED"]);
            return objCmpInfo;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string AdminId
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
        public string CompanyName
        {
            get;
            set;
        }
        public string Address
        {
            get;
            set;
        }
        public string CompanyLogoName
        {
            get;
            set;
        }
        private RUN_QUERY_BY ERunQueryBy
        {
            get { return _eRunQueryBy; }
            set { _eRunQueryBy = value; }
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public DataTable ResultTable1
        {
            get;
            private set;
        }


        public string TimeZoneId
        {
            get;
            private set;
        }
        public bool IsBlocked
        {
            get;
            private set;
        }
        public MFECompanyInfo CmpInfo
        {
            get { return _cmpInfo; }
            private set { _cmpInfo = value; }
        }
    }
}