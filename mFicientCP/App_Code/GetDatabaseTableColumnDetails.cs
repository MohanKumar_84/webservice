﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;

namespace mFicientCP
{
    public class GetDatabaseTableColumnDetails
    {
        public GetDatabaseTableColumnDetails()
        {

        }

        public GetDatabaseTableColumnDetails(string _ConnectionString, string _TableName, string _DatabaseType, string _DatabaseName)
        {
            this.ConnectionString = _ConnectionString;
            this.TableName = _TableName;
            this.DbType = _DatabaseType;
            this.DatabaseName = _DatabaseName;
        }
        public void Process()
        {
            this.StatusCode = -1000;
            switch (this.DbType)
            {
                case "1":
                    GetMssqlDatabaseColDetails();
                    break;
                case "2":
                    GetOraclelDatabaseColDetails();
                    break;
                case "3":
                    GetMysqlDatabaseColDetails();
                    break;
                case "4":
                    GetPostgreDatabaseColDetails();
                    break;
            }
        }

        private void GetMssqlDatabaseColDetails()
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable
                                         FROM sys.columns AS c
                                         JOIN sys.types AS t ON c.user_type_id=t.user_type_id
                                         JOIN sysobjects ON c.OBJECT_ID = sysobjects.id
                                         WHERE sysobjects.xtype='U' 
                                        AND OBJECT_NAME(c.OBJECT_ID)=@Table_Name
                                        ORDER BY c.OBJECT_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query, conn);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@Table_Name", this.TableName);
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                conn.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void GetOraclelDatabaseColDetails()
        {
            OracleConnection conn = new OracleConnection();
            try
            {
                conn = new OracleConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE as column_type, NULLABLE as is_nullable from user_tab_columns where TABLE_NAME = :TABLE_NAME";
                OracleCommand objSqlCommand = new OracleCommand(query, conn);
                objSqlCommand.Parameters.Add("TABLE_NAME", this.TableName);
                DataSet ObjDataSet = new DataSet();
                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                conn.Close();
                this.StatusCode = 0;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void GetMysqlDatabaseColDetails()
        {
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT column_name, DATA_TYPE As column_type, is_nullable, COLUMN_DEFAULT, TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TABLE_NAME AND table_schema =@DataBaseName;";
                MySqlCommand objCommand = new MySqlCommand(query, conn);
                objCommand.CommandType = CommandType.Text;
                objCommand.Parameters.AddWithValue("@TABLE_NAME", this.TableName);
                objCommand.Parameters.AddWithValue("@DataBaseName", this.DatabaseName);
                DataSet ObjDataSet = new DataSet();
                MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(objCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                conn.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }

        private void GetPostgreDatabaseColDetails()
        {
            NpgsqlConnection Conn = null;
            try
            {
                Conn = new NpgsqlConnection(this.ConnectionString);
                Conn.Open();
                string strQuery = @" SELECT table_name, column_name, data_type as column_type, is_nullable 
                                        FROM information_schema.columns where table_name = '" + this.TableName.Split('.')[1].Replace("\"", "") + "' and table_schema = '" + this.TableName.Split('.')[0].Replace("\"", "") + "';";


                NpgsqlCommand objCommand = new NpgsqlCommand(strQuery, Conn);
                DataSet ObjDataSet = new DataSet();
                NpgsqlDataAdapter objDataAdapter = new NpgsqlDataAdapter(objCommand);
                objDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
            }
            catch
            {
                this.StatusCode = -1000;
            }
            finally
            {
                if (Conn != null) Conn.Close();
            }
        }

        public string ConnectionString
        {
            set;
            get;
        }
        public string TableName
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string DatabaseType { get; set; }

        public string DatabaseName { get; set; }

        public string DbType { get; set; }
    }
}