﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetDetailsToValidateWfJson
    {
        public GetDetailsToValidateWfJson()
        {

        }

        public GetDetailsToValidateWfJson(string _WfId, string _SubAdminId, string _CompanyId)
        {
            this.WfId = _WfId;
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;

                query = @"SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND PARENT_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID ORDER BY WF_NAME;
                            
                        SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.COMPANY_ID=@COMPANY_ID AND fd.PARENT_ID=@PARENT_ID ORDER BY fd.FORM_NAME;

                        SELECT  cd.WS_COMMAND_ID FROM  TBL_WS_COMMAND cd INNER JOIN TBL_WEBSERVICE_CONNECTION cn ON 
                                    cn.WS_CONNECTOR_ID=cd.WS_CONNECTOR_ID WHERE cd.SUBADMIN_ID=@SUBADMIN_ID ORDER BY cd.WS_COMMAND_NAME;

                        SELECT  cd.DB_COMMAND_ID FROM  TBL_DATABASE_COMMAND cd
                                INNER JOIN TBL_DATABASE_CONNECTION cn on cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID
                                    WHERE cd.SUBADMIN_ID=@SUBADMIN_ID ORDER BY cd.DB_COMMAND_NAME;

                        SELECT * FROM TBL_PRV_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID ORDER BY VERSION desc ;
                         
                        SELECT * FROM TBL_CURR_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID ORDER BY VERSION;";

                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.WfId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.WfResultTable = objDataSet.Tables[0];
                if (objDataSet.Tables[0].Rows.Count > 0)
                    this.StatusCode = 0;
                else
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());

                //this.FrmResultTable = objDataSet.Tables[1];
                this.WsCmdResultTable = objDataSet.Tables[2];
                this.DbCmdResultTable = objDataSet.Tables[3];
                this.WfCommitedResultTable = objDataSet.Tables[4];
                this.WfPublishedTable = objDataSet.Tables[5];
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

        public string WfId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public DataTable WfResultTable
        {
            set;
            get;
        }
        //public DataTable FrmResultTable
        //{
        //    set;
        //    get;
        //}
        public DataTable DbCmdResultTable
        {
            set;
            get;
        }
        public DataTable WsCmdResultTable
        {
            set;
            get;
        }
        public DataTable WfCommitedResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }

        public DataTable WfPublishedTable { get; set; }
    }

    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}