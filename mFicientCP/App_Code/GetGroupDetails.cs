﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetGroupDetails
    {
        public GetGroupDetails(string companyId)
        {
            this.CompanyId = companyId;
        }
        public GetGroupDetails(string companyId, Boolean _IsFindByUserId)
        {
            this.CompanyId = companyId;
            ProcessAutoapprovedGroups();

        }



        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string query = @"SELECT G.*,approval.REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION FROM TBL_USER_GROUP G 
                                left outer JOIN TBL_SUB_ADMIN S ON G.SUBADMIN_ID=S.SUBADMIN_ID
                                inner join  dbo.TBL_ACCOUNT_SETTINGS as approval ON approval.COMPANY_ID=g.COMPANY_ID 
                                WHERE g.COMPANY_ID=@CompanyId ORDER BY GROUP_NAME";//28/8/2012 order by group_name
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = "";
                this.GroupList = new List<MFEGroupDetail>();
                List<List<string>> gps = new List<List<string>>(); 
                if (objDataSet.Tables[0].Rows.Count > 0){
                    this.GroupList = getGrpDtlFromDatatable(objDataSet.Tables[0], out gps);
                    this.Groups = gps;
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public void ProcessAutoapprovedGroups()
        {
            try
            {
                StatusCode = -1000;//for error
                string query = @"select U.GROUP_IDS,G.GROUP_NAME,U.COMPANY_ID,s.REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION from 
                                (SELECT Split.a.value('.', 'VARCHAR(100)') AS GROUP_IDS,COMPANY_ID  FROM  (SELECT [GROUP_IDS], CAST ('<M>' + REPLACE(GROUP_IDS, ',', '</M><M>') + '</M>' AS XML) AS String  
                              ,COMPANY_ID FROM  TBL_USER_DEVICE_AUTOAPPROVE_SETTING) AS A CROSS APPLY String.nodes ('/M') AS Split(a)) U
                              INNER JOIN TBL_USER_GROUP AS G ON  G.GROUP_ID=U.GROUP_IDS 
                              INNER JOIN dbo.TBL_ACCOUNT_SETTINGS as s  on  U.COMPANY_ID =s.COMPANY_ID
                              WHERE U.COMPANY_ID=@CompanyId;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        List<MFEGroupDetail> getGrpDtlFromDatatable(DataTable groupDetailDtbl,out List<List<string>> groups)
        {
            if (groupDetailDtbl == null) throw new ArgumentNullException();
            List<MFEGroupDetail> lstGrpDtl = new List<MFEGroupDetail>();
            groups = new List<List<string>>();
            foreach (DataRow row in groupDetailDtbl.Rows)
            {
                lstGrpDtl.Add(getGrpDtlFromDatarow(row));

                List<string> gps = new List<string>();
                gps.Add(row["GROUP_NAME"].ToString());
                gps.Add(row["GROUP_NAME"].ToString());
                groups.Add(gps);
            }
            return lstGrpDtl;
        }
        MFEGroupDetail getGrpDtlFromDatarow(DataRow groupDetailRow)
        {
            if (groupDetailRow == null) throw new ArgumentNullException();
            MFEGroupDetail objGrpDtl = new MFEGroupDetail();
            objGrpDtl.EnterpriseId = Convert.ToString(groupDetailRow["COMPANY_ID"]);
            objGrpDtl.GroupId = Convert.ToString(groupDetailRow["GROUP_ID"]);
            objGrpDtl.GroupName = Convert.ToString(groupDetailRow["GROUP_NAME"]);
            objGrpDtl.SubadminId = Convert.ToString(groupDetailRow["SUBADMIN_ID"]);
            objGrpDtl.CreatedOn = Convert.ToInt64(groupDetailRow["CREATED_ON"]);
            return objGrpDtl;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public List<MFEGroupDetail> GroupList
        {
            get;
            private set;
        }
        public List<List<string>> Groups
        {
            get;
            private set;
        }
    }
}