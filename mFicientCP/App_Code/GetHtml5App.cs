﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetHtml5App
    {
        public GetHtml5App(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// Sets the Result Table and Html5App if App exists
        /// </summary>
        /// <param name="appName"></param>
        public void GetHtml5AppByName(string appName)
        {
            try
            {
                DataTable dtblHtml5App = getHtml5AppByName(appName);
                this.ResultTable = dtblHtml5App;
                MFEHtml5AppDetail objAppDtl = null;
                if (this.ResultTable.Rows.Count > 0)
                {
                    DataRow row = this.ResultTable.Rows[0];//should get only one record.
                    objAppDtl = getHtml5AppDetailFromDataRow(row);
                }
                this.Html5App = objAppDtl;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        /// <summary>
        /// Sets the result table and Html5App if app Exists
        /// </summary>
        /// <param name="appId"></param>
        public void GetHtml5AppById(string appId)
        {
            try
            {
                DataTable dtblHtml5App = getHtml5AppById(appId);
                this.ResultTable = dtblHtml5App;
                MFEHtml5AppDetail objAppDtl = null;
                if (this.ResultTable.Rows.Count > 0)
                {
                    DataRow row = this.ResultTable.Rows[0];//should get only one record.
                    objAppDtl = getHtml5AppDetailFromDataRow(row);
                }
                this.Html5App = objAppDtl;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        /// <exception cref="MficientException">Thrown when there is some internal error</exception>
        public bool DoesHtml5AppWithAppNameExists(string appName)
        {
            try
            {
                DataTable dtblAppDtl = this.getHtml5AppByName(appName);
                if (dtblAppDtl == null) throw new DataSetNullException();
                if (dtblAppDtl.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw new MficientException("Internal server error.");
            }
        }
        public bool DoesHtml5AppWithAppNameExists(string appName,List<string>ignoreIds)
        {
            try
            {
                string strQuery = "";
                SqlCommand cmd = new SqlCommand();
                if (ignoreIds != null && ignoreIds.Count>0)
                {
                    string[] parameters = new string[ignoreIds.Count];
                    for (int i = 0; i < ignoreIds.Count; i++)
                    {
                        parameters[i] = "@p" + i;
                        cmd.Parameters.AddWithValue(parameters[i], ignoreIds[i]);
                    }
                    strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
                                WHERE APP_NAME = @APP_NAME 
                                AND SUBADMIN_ID = @SUBADMIN_ID
                                AND COMPANY_ID = @COMPANY_ID
                                AND APP_ID NOT IN (";
                    strQuery += string.Join(",", parameters) + ")";
                    cmd.CommandText = strQuery;

                }
                else
                {
                     strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
                                WHERE APP_NAME = @APP_NAME 
                                AND SUBADMIN_ID = @SUBADMIN_ID
                                AND COMPANY_ID = @COMPANY_ID";
                }
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@APP_NAME", appName);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (objDataSet == null) throw new DataSetNullException();
                //this.ResultTable = objDataSet.Tables[0];
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw new MficientException("Internal server error.");
            }
        }
        #region Private Member Region
        DataTable getHtml5AppByName(string appName)
        {
            string strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
                                WHERE APP_NAME = @APP_NAME 
                                AND SUBADMIN_ID = @SUBADMIN_ID
                                AND COMPANY_ID = @COMPANY_ID;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_NAME", appName);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            //this.ResultTable = objDataSet.Tables[0];
            return objDataSet.Tables[0];
            
        }
        DataTable getHtml5AppById(string appId)
        {
            string strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
                                WHERE APP_ID = @APP_ID 
                                AND SUBADMIN_ID = @SUBADMIN_ID
                                AND COMPANY_ID = @COMPANY_ID;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            //this.ResultTable = objDataSet.Tables[0];
            return objDataSet.Tables[0];
        }

        MFEHtml5AppDetail getHtml5AppDetailFromDataRow(DataRow dr)
        {
            if (dr == null) throw new ArgumentNullException();
            MFEHtml5AppDetail objApp = new MFEHtml5AppDetail();
            objApp.AppId = Convert.ToString(dr["APP_ID"]);
            objApp.CompanyId = Convert.ToString(dr["COMPANY_ID"]);
            objApp.AppName = Convert.ToString(dr["APP_NAME"]);
            objApp.Description = Convert.ToString(dr["DESCRIPTION"]);
            objApp.AppJson = Convert.ToString(dr["APP_JSON"]);
            objApp.Icon = Convert.ToString(dr["ICON"]);
            objApp.SubadminId = Convert.ToString(dr["SUBADMIN_ID"]);
            objApp.CreatedBy = Convert.ToString(dr["CREATED_BY"]);
            objApp.UpdatedOn = Convert.ToString(dr["UPDATE_ON"]);
            objApp.BucketFilePath = Convert.ToString(dr["BUCKET_FILE_PATH"]);
            return objApp;
        }
        #endregion
        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public MFEHtml5AppDetail Html5App
        {
            get;
            private set;
        }
    }
}