﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetHtml5AppPublishDetail
    {
        public GetHtml5AppPublishDetail(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// Sets the Result Table and Html5App if App exists
        /// </summary>
        /// <param name="appName"></param>
        public void GetHtml5PreviousPublishedDtls(string appId)
        {
            try
            {
                DataTable dtblHtml5AppPublishDtl = getHtml5AppPrevPublishDtlsById(appId);
                this.ResultTable = dtblHtml5AppPublishDtl;
                MFEHtml5AppPublishDetail objAppPublishDtl = null;
                List<MFEHtml5AppPublishDetail> lstAppPublishDtls = new List<MFEHtml5AppPublishDetail>();
                if (this.ResultTable.Rows.Count > 0)
                {
                    foreach (DataRow row in this.ResultTable.Rows)
                    {
                        objAppPublishDtl = getHtml5AppPublishDtlFromDataRow(row);
                        lstAppPublishDtls.Add(objAppPublishDtl);
                    }
                }
                this.Html5AppPublishDtls = lstAppPublishDtls;
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void GetHtml5CurrentPublishedDtls(string appId)
        {
            try
            {
                DataTable dtblHtml5AppPublishDtl = getHtml5AppCurrPublishDtlsById(appId);
                this.ResultTable = dtblHtml5AppPublishDtl;
                MFEHtml5AppPublishDetail objAppPublishDtl = null;
                List<MFEHtml5AppPublishDetail> lstAppPublishDtls = new List<MFEHtml5AppPublishDetail>();
                if (this.ResultTable.Rows.Count > 0)
                {
                    foreach (DataRow row in this.ResultTable.Rows)
                    {
                        objAppPublishDtl = getHtml5AppPublishDtlFromDataRow(row);
                        lstAppPublishDtls.Add(objAppPublishDtl);
                    }
                }
                this.Html5AppPublishDtls = lstAppPublishDtls;
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void GetLatestHtml5PreviousPublishedDtls(string appId)
        {
            try
            {
                DataTable dtblHtml5AppPublishDtl = getLatestHtml5AppPrevPublishDtls(appId);
                this.ResultTable = dtblHtml5AppPublishDtl;
                MFEHtml5AppPublishDetail objAppPublishDtl = null;
                List<MFEHtml5AppPublishDetail> lstAppPublishDtls = new List<MFEHtml5AppPublishDetail>();
                if (this.ResultTable.Rows.Count > 0)
                {
                    foreach (DataRow row in this.ResultTable.Rows)
                    {
                        objAppPublishDtl = getHtml5AppPublishDtlFromDataRow(row);
                        lstAppPublishDtls.Add(objAppPublishDtl);
                    }
                }
                this.Html5AppPublishDtls = lstAppPublishDtls;
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        /// <exception cref="MficientException">Thrown when there is some internal error</exception>
        //        public bool DoesHtml5AppWithAppNameExists(string appName)
        //        {
        //            try
        //            {
        //                DataTable dtblAppDtl = this.getHtml5AppByName(appName);
        //                if (dtblAppDtl == null) throw new DataSetNullException();
        //                if (dtblAppDtl.Rows.Count > 0)
        //                {
        //                    return true;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //            }
        //            catch
        //            {
        //                throw new MficientException("Internal server error.");
        //            }
        //        }
        //        public bool DoesHtml5AppWithAppNameExists(string appName,List<string>ignoreIds)
        //        {
        //            try
        //            {
        //                string strQuery = "";
        //                SqlCommand cmd = new SqlCommand();
        //                if (ignoreIds != null && ignoreIds.Count>0)
        //                {
        //                    string[] parameters = new string[ignoreIds.Count];
        //                    for (int i = 0; i < ignoreIds.Count; i++)
        //                    {
        //                        parameters[i] = "@p" + i;
        //                        cmd.Parameters.AddWithValue(parameters[i], ignoreIds[i]);
        //                    }
        //                    strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
        //                                WHERE APP_NAME = @APP_NAME 
        //                                AND SUBADMIN_ID = @SUBADMIN_ID
        //                                AND COMPANY_ID = @COMPANY_ID
        //                                AND APP_ID NOT IN (";
        //                    strQuery += string.Join(",", parameters) + ")";
        //                    cmd.CommandText = strQuery;

        //                }
        //                else
        //                {
        //                     strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
        //                                WHERE APP_NAME = @APP_NAME 
        //                                AND SUBADMIN_ID = @SUBADMIN_ID
        //                                AND COMPANY_ID = @COMPANY_ID";
        //                }
        //                cmd.CommandType = CommandType.Text;
        //                cmd.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
        //                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
        //                cmd.Parameters.AddWithValue("@APP_NAME", appName);
        //                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
        //                if (objDataSet == null) throw new DataSetNullException();
        //                //this.ResultTable = objDataSet.Tables[0];
        //                if (objDataSet.Tables[0].Rows.Count > 0)
        //                {
        //                    return true;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //            }
        //            catch
        //            {
        //                throw new MficientException("Internal server error.");
        //            }
        //        }
        #region Private Member Region

        DataTable getHtml5AppPrevPublishDtlsById(string appId)
        {
            string strQuery = @"SELECT * FROM TBL_HTML5_APP_PRV_WF_DETAIL
                                WHERE COMPANY_ID = @COMPANY_ID
                                AND APP_ID = @APP_ID
                                AND SUBADMIN_ID = @SUBADMIN_ID;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            return objDataSet.Tables[0];
        }
        DataTable getHtml5AppCurrPublishDtlsById(string appId)
        {
            string strQuery = @"SELECT * FROM TBL_HTML5_APP_CURR_WF_DETAIL
                                WHERE COMPANY_ID = @COMPANY_ID
                                AND APP_ID = @APP_ID
                                AND SUBADMIN_ID = @SUBADMIN_ID;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            return objDataSet.Tables[0];
        }
        DataTable getLatestHtml5AppPrevPublishDtls(string appId)
        {
            string strQuery = @"SELECT * FROM TBL_HTML5_APP_PRV_WF_DETAIL
                                WHERE COMPANY_ID = @COMPANY_ID
                                AND APP_ID = @APP_ID
                                ORDER BY ""VERSION"" DESC";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            return objDataSet.Tables[0];
        }
        MFEHtml5AppPublishDetail getHtml5AppPublishDtlFromDataRow(DataRow dr)
        {
            if (dr == null) throw new ArgumentNullException();
            MFEHtml5AppPublishDetail objAppPublishDtl = new MFEHtml5AppPublishDetail();
            objAppPublishDtl.AppId = Convert.ToString(dr["APP_ID"]);
            objAppPublishDtl.CompanyId = Convert.ToString(dr["COMPANY_ID"]);
            objAppPublishDtl.AppName = Convert.ToString(dr["APP_NAME"]);
            objAppPublishDtl.Description = Convert.ToString(dr["APP_DESCRIPTION"]);
            objAppPublishDtl.AppJson = Convert.ToString(dr["APP_JSON"]);
            objAppPublishDtl.Icon = Convert.ToString(dr["ICON"]);
            objAppPublishDtl.SubadminId = Convert.ToString(dr["SUBADMIN_ID"]);
            objAppPublishDtl.Version = Convert.ToString(dr["VERSION"]);
            objAppPublishDtl.WorkedBy = Convert.ToString(dr["WORKED_BY"]);
            objAppPublishDtl.PublishedOn = Convert.ToInt64(dr["PUBLISHED_ON"]);
            objAppPublishDtl.CommittedOn = Convert.ToInt64(dr["COMMITTED_ON"]);
            objAppPublishDtl.VersionHistory = Convert.ToString(dr["VERSION_HISTORY"]);
            objAppPublishDtl.BucketFilePath = Convert.ToString(dr["BUCKET_FILE_PATH"]);
            return objAppPublishDtl;
        }
        #endregion
        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public List<MFEHtml5AppPublishDetail> Html5AppPublishDtls
        {
            get;
            private set;
        }
    }
}