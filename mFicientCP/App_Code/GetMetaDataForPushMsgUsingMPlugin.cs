﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Net;
using System.Data;
namespace mFicientCP
{
    public class GetMetaDataForPushMsgUsingMPlugin
    {
        string _companyId, _connString, _query, _mPluginAgentName;
        int _dbType;
        List<QueryParameters> _paramLst;
        string _statusDescription;
        int _statusCode;
        DataSet _metaDataDS;


        public GetMetaDataForPushMsgUsingMPlugin(string companyId,
            string connString, string query,
            int dbType, string mpluginAgntName,
            List<QueryParameters> paramsList)
        {
            this.CompanyId = companyId;
            this.ConnString = connString;
            this.Query = query;
            this.DbType = dbType;
            this.MPluginAgentName = mpluginAgntName;
            this.ParamLst = paramsList;
        }
        public void Process()
        {
            try
            {
                mPluginAgents objAgent = new mPluginAgents();
                string strUrl = String.Empty;
                GetMetaDataForPUshMsgReqFields objReqFields =
                    new GetMetaDataForPUshMsgReqFields();
                objReqFields.rid = Utilities.GetMd5Hash(this.CompanyId + DateTime.Now.Ticks.ToString()).Substring(0, 15);
                objReqFields.eid = this.CompanyId;
                objReqFields.agtnm = this.MPluginAgentName;
                try
                {
                    objReqFields.agtpwd = objAgent.GetMpluginAgentPassword(this.CompanyId, this.MPluginAgentName);
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower() == "Agent not found".ToLower())
                    {
                        throw new MficientException("Agent not found");
                    }
                    else
                    {
                        throw ex;
                    }
                }
                objReqFields.constr = this.ConnString;
                objReqFields.query = this.Query;
                objReqFields.dbtp = this.DbType.ToString();
                objReqFields.lp = this.ParamLst;
                GetMetaDataForPushMsgReq objRequst =
                    new GetMetaDataForPushMsgReq();
                objRequst.req = objReqFields;
                string strReqJsonWithDetails = Utilities.SerializeJson<GetMetaDataForPushMsgReq>(objRequst);

                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(this.CompanyId);
                objServerUrl.Process();
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (objServerUrl.ServerUrl.Length > 0)
                    {
                        strUrl = strUrl + "/CPPushMsgTableMetadata.aspx?d=" + Utilities.UrlEncode(strReqJsonWithDetails);
                        HTTP oHttp = new HTTP(strUrl);
                        oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                        HttpResponseStatus oResponse = oHttp.Request();
                        if (oResponse.StatusCode == HttpStatusCode.OK)
                        {
                            DataSet dsMetaDataOfTable = new DataSet();
                            MP_GetDatabseMetadataResp objParsedResp = new MP_GetDatabseMetadataResp(oResponse.ResponseText);
                            if (objParsedResp.Code == "0")
                            {
                                dsMetaDataOfTable = objParsedResp.Data;
                                if (dsMetaDataOfTable == null || dsMetaDataOfTable.Tables.Count == 0)
                                    throw new MficientException("Table not found.");
                                this.MetaDataDS = dsMetaDataOfTable;
                            }
                            else
                            {
                                throw new MficientException("There was some error in connection.");   
                            }
                        }
                        else if (oResponse.StatusCode == HttpStatusCode.NotFound)
                        {
                            throw new MficientException("mplugin server not found.");
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        #region Public Properties
        public int DbType
        {
            get { return _dbType; }
            private set { _dbType = value; }
        }
        public string Query
        {
            get { return _query; }
            private set { _query = value; }
        }
        public string ConnString
        {
            get { return _connString; }
            private set { _connString = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public List<QueryParameters> ParamLst
        {
            get { return _paramLst; }
            private set { _paramLst = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        public string MPluginAgentName
        {
            get { return _mPluginAgentName; }
            private set { _mPluginAgentName = value; }
        }
        public DataSet MetaDataDS
        {
            get { return _metaDataDS; }
            private set { _metaDataDS = value; }
        }
        #endregion
    }

    public class GetMetaDataForPushMsgReq
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public GetMetaDataForPUshMsgReqFields req { get; set; }
    }
    public class GetMetaDataForPUshMsgReqFields
    {
        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        ///company id
        /// </summary>
        [DataMember]
        public string eid { get; set; }
        /// <summary>
        /// agent name
        /// </summary>
        [DataMember]
        public string agtnm { get; set; }
        /// <summary>
        /// agent password
        /// </summary>
        [DataMember]
        public string agtpwd { get; set; }
        /// <summary>
        /// connection string
        /// </summary>
        [DataMember]
        public string constr { get; set; }
        /// <summary>
        /// Query to run
        /// </summary>
        [DataMember]
        public string query { get; set; }

        /// <summary>
        /// Database Type
        /// </summary>
        [DataMember]
        public string dbtp { get; set; }

        /// <summary>
        /// Parameters for query
        /// </summary>
        [DataMember]
        public List<QueryParameters> lp { get; set; }

    }
    [DataContract]
    public class QueryParameters
    {
        [DataMember]
        public string para { get; set; }

        [DataMember]
        public string val { get; set; }
    }
}