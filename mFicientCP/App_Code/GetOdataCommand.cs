﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetOdataCommand
    {
        public GetOdataCommand()
        {

        }

        public GetOdataCommand(Boolean _IsSelectAll, Boolean _IsCmdEdit, string _SubAdminId, string _CommandId, string _CommandName, string _CompanyId)
        {
            this.IsSelectAll = _IsSelectAll;
            this.SubAdminId = _SubAdminId;
            this.CommandId = _CommandId;
            this.CommandName = _CommandName;
            this.IsCmdEdit = _IsCmdEdit;
            this.CompanyId = _CompanyId;
            Process();
        }

        private void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT  cd.X_REFERENCES,cd.ODATA_CONNECTOR_ID,cd.ODATA_COMMAND_ID,cn.CONNECTION_NAME,cn.VERSION,cn.ODATA_ENDPOINT,cd.ODATA_COMMAND_NAME, cd.RESOURCE_TYPE,isnull(cd.CREATED_BY,'') as CREATED_BY,isnull(cd.CREATED_ON,0) as CREATED_ON,isnull(cd.UPDATED_ON,0) as UPDATED_ON,SC.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,
                              cd.RESOURCE_PATH,cd.HTTP_TYPE,cd.SUBADMIN_ID,cd.HTTP_DATA,cd.QUERY_OPTION,cd.INPUT_PARAMETER_JSON,
                               cd.OUTPUT_PARAMETER_JSON,cd.FUNCTION_TYPE,cd.RESPONSE_FORMAT,cd.DATA_JSON,cd.RETURN_TYPE,cd.DATASET_PATH,
                               cd.ADDITIONAL_RESOURCE_PATH,cd.DESCRIPTION,cn.COMPANY_ID,cd.CACHE,cd.EXPIRY_FREQUENCY,cd.EXPIRY_CONDITION,cn.CREDENTIAL_PROPERTY
                                FROM  TBL_ODATA_COMMAND cd INNER JOIN TBL_ODATA_CONNECTION cn                              
                                on cn.ODATA_CONNECTOR_ID=cd.ODATA_CONNECTOR_ID
                                Left outer join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=cd.SUBADMIN_ID 
                            Left outer  join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID 
                            WHERE  cd.COMPANY_ID=@COMPANY_ID ORDER BY cn.CONNECTION_NAME,cd.ODATA_COMMAND_NAME;";   
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                }
                else
                {
                    if (this.CommandName.Length == 0)
                    {
                        query = @"SELECT  cd.ODATA_CONNECTOR_ID,
                                    cd.ODATA_COMMAND_ID,
                                    cn.CONNECTION_NAME,
                                    cn.VERSION,
                                    cn.ODATA_ENDPOINT,
                                    cd.ODATA_COMMAND_NAME,
                                    cd.RESOURCE_TYPE,
                                    cd.RESOURCE_PATH,
                                    cd.HTTP_TYPE,
                                    cd.SUBADMIN_ID,
                                    cd.HTTP_DATA,
                                    cd.QUERY_OPTION,
                                    cd.INPUT_PARAMETER_JSON,
                                    cd.OUTPUT_PARAMETER_JSON,
                                    cd.FUNCTION_TYPE,
                                    cd.RESPONSE_FORMAT,
                                    cd.DATA_JSON,
                                    cd.RETURN_TYPE,
                                    cd.DATASET_PATH,
                                    cd.ADDITIONAL_RESOURCE_PATH,
                                    cd.DESCRIPTION,cd.DESCRIPTION,
                                    cd.CREATED_BY,cd.CREATED_ON,cd.UPDATED_ON,
                                   cd.X_REFERENCES,
                                   ss.FULL_NAME,st.FULL_NAME as CreatedBY,
                                cd.CACHE,cd.EXPIRY_FREQUENCY,cd.EXPIRY_CONDITION,cn.CREDENTIAL_PROPERTY
                                    FROM  TBL_ODATA_COMMAND cd
                                    LEFT JOIN TBL_ODATA_CONNECTION cn 
                                    on cn.ODATA_CONNECTOR_ID=cd.ODATA_CONNECTOR_ID
                                    left outer join  dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=cd.SUBADMIN_ID left outer join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID
                                    WHERE cd.ODATA_COMMAND_ID=@ODATA_COMMAND_ID and cd.COMPANY_ID=@COMPANY_ID
                                    ORDER BY cd.ODATA_COMMAND_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                    else
                    {
                        if (IsCmdEdit)
                        {

                            query = @"SELECT  * FROM  TBL_ODATA_COMMAND 
                                        WHERE ODATA_COMMAND_NAME=@ODATA_COMMAND_NAME 
                                        AND ODATA_COMMAND_ID !=@ODATA_COMMAND_ID 
                                        AND COMPANY_ID=@COMPANY_ID 
                                        ORDER BY ODATA_COMMAND_NAME;";
                            objSqlCommand = new SqlCommand(query);
                            objSqlCommand.CommandType = CommandType.Text;
                        
                            objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_NAME", this.CommandName);
                            objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
                            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                        }
                        else
                        {

                            query = @"SELECT  * FROM  TBL_ODATA_COMMAND 
                                        WHERE 
                                        ODATA_COMMAND_NAME=@ODATA_COMMAND_NAME AND 
                                        COMPANY_ID=@COMPANY_ID 
                                        ORDER BY ODATA_COMMAND_NAME;";
                            objSqlCommand = new SqlCommand(query);
                            objSqlCommand.CommandType = CommandType.Text;
                     
                            objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_NAME", this.CommandName);
                            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                        }
                    }
                }

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable != null)
                    this.StatusCode = 0;
                else this.ResultTable = new DataTable();
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }
        public List<IdeOdataCmdsForJson> getOdataCommandDetail()
        {
            DataTable dtblOdataCmds = this.ResultTable;
            List<IdeOdataCmdsForJson> lstOdataCmds = new List<IdeOdataCmdsForJson>();
                
            if (dtblOdataCmds != null)
            {
                foreach (DataRow row in dtblOdataCmds.Rows)
                {
                    IdeOdataCmdsForJson objOdataCmd = new IdeOdataCmdsForJson();
                    objOdataCmd.compId = Convert.ToString(row["COMPANY_ID"]);
                    objOdataCmd.connectorId = Convert.ToString(row["ODATA_CONNECTOR_ID"]);
                    objOdataCmd.connectionName = Convert.ToString(row["CONNECTION_NAME"]);
                    objOdataCmd.endPoint = Convert.ToString(row["ODATA_ENDPOINT"]);
                    objOdataCmd.serviceVer = Convert.ToString(row["VERSION"]);
                    objOdataCmd.commandId = Convert.ToString(row["ODATA_COMMAND_ID"]);
                    objOdataCmd.commandName = Convert.ToString(row["ODATA_COMMAND_NAME"]);
                    objOdataCmd.resourceType = Convert.ToString(row["RESOURCE_TYPE"]);
                    objOdataCmd.resourcePath = Convert.ToString(row["RESOURCE_PATH"]);
                    objOdataCmd.httpType = Convert.ToString(row["HTTP_TYPE"]);
                    objOdataCmd.httpData = Convert.ToString(row["HTTP_DATA"]);
                    objOdataCmd.queryOption = Convert.ToString(row["QUERY_OPTION"]);
                    objOdataCmd.inputParamJson = Convert.ToString(row["INPUT_PARAMETER_JSON"]);
                    objOdataCmd.outputParamJson = Convert.ToString(row["OUTPUT_PARAMETER_JSON"]);
                    objOdataCmd.functionType = Convert.ToString(row["FUNCTION_TYPE"]);
                    objOdataCmd.responseFormat = Convert.ToString(row["RESPONSE_FORMAT"]);
                    objOdataCmd.dataJson = Convert.ToString(row["DATA_JSON"]);
                    objOdataCmd.datasetPath = Convert.ToString(row["DATASET_PATH"]);
                    objOdataCmd.returnType = Convert.ToString(row["RETURN_TYPE"]);
                    objOdataCmd.additionalResourcePath = Convert.ToString(row["ADDITIONAL_RESOURCE_PATH"]);
                    objOdataCmd.description = Convert.ToString(row["DESCRIPTION"]);
                    lstOdataCmds.Add(objOdataCmd);
                }
            }
            return lstOdataCmds;
        }

        public GetOdataCommand(string _CommandId, string _CommandName)
        {
           this.CommandId = _CommandId;
          this.CommandName = _CommandName;
          GetProcess();
            
        }

        public void GetProcess()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"select * from dbo.TBL_ODATA_COMMAND where ODATA_COMMAND_NAME =@ODATA_COMMAND_NAME";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_NAME", this.CommandName);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable != null)
                    if (this.ResultTable.Rows.Count != 0)
                        this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }




        private Boolean IsSelectAll { get; set; }

        public DataTable ResultTable { get; set; }


   
        public int StatusCode { get; set; }

        private string SubAdminId { get; set; }

        private string CommandId { get; set; }

        private string CommandName { get; set; }

        private bool IsCmdEdit { get; set; }

        private string CompanyId { get; set; }
    }
}