﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class GetOfflineDataTableDtl
    {
        public GetOfflineDataTableDtl(string companyId)
        {
            this.CompanyId = companyId;
            this.offlineDatatables = new List<MFEOfflineDataTable>();
        }
        public void Process()
        {
            try
            {
                string strQuery = getSqlQueryForOfflineDTDtl();
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (ds != null)
                {
                    this.ResultTble = ds.Tables[0];
                    this.offlineDatatables = getOfflineDatatableDtl(ds.Tables[0]);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void ProcessGetOfflineDTDtlWithCommandDtl()
        {

        }
        List<MFEOfflineDataTable> getOfflineDatatableDtl(DataTable offlineDtbl)
        {
            if (offlineDtbl == null) throw new ArgumentNullException();
            List<MFEOfflineDataTable> lstOfflineDatatbls = new List<MFEOfflineDataTable>();
            foreach (DataRow row in offlineDtbl.Rows)
            {
                MFEOfflineDataTable objOfflineDtbl = new MFEOfflineDataTable();
                objOfflineDtbl.TableId = Convert.ToString(row["TABLE_ID"]);
                objOfflineDtbl.CompanyId = Convert.ToString(row["COMPANY_ID"]);
                objOfflineDtbl.TableName = Convert.ToString(row["TABLE_NAME"]);
                objOfflineDtbl.TableType = Convert.ToInt32(row["TABLE_TYPE"]);
                //objOfflineDtbl.TableSyncOn = Convert.ToInt32(row["TABLE_SYNC_ON"]);
                //objOfflineDtbl.ConfirmationForSync = Convert.ToByte(row["CONFIRMATION_FOR_SYNC"]);
                //objOfflineDtbl.CommandType = Convert.ToInt32(row["COMMAND_TYPE"]);
                //objOfflineDtbl.UploadCommandId = Convert.ToString(row["UPLOAD_COMMAND_ID"]);
                //objOfflineDtbl.DownloadCommandId = Convert.ToString(row["DOWNLOAD_COMMAND_ID"]);
                objOfflineDtbl.TableJson = Convert.ToString(row["TABLE_JSON"]);
                objOfflineDtbl.SyncJson = Convert.ToString(row["SYNC_JSON"]);
                objOfflineDtbl.UpdateDatetime = Convert.ToInt64(row["UPDATE_DATETIME"]);
                //objOfflineDtbl.ConnectionId = Convert.ToString(row["CONNECTION_ID"]);
                //objOfflineDtbl.Apps = Convert.ToString(row["APPS"]);
                lstOfflineDatatbls.Add(objOfflineDtbl);
            }
            return lstOfflineDatatbls;
        }
        public MFEOfflineDataTable ProcessGetOfflineDTDtlByTblId(string tableId)
        {
            MFEOfflineDataTable objOfflineDt = new MFEOfflineDataTable();
            //appIdsSelected = new List<string>();
            try
            {
                string strQuery = getSqlQueryForOfflineTblByTblId();
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@TABLE_ID", tableId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (ds != null)
                {
                    this.ResultTble = ds.Tables[0];
                    if (this.ResultTble.Rows.Count == 0)
                    {
                        throw new MficientException("Offline Datatable does not exist.");
                    }
                    objOfflineDt = getOfflineDatatableDtl(ds.Tables[0])[0];
                    //foreach (DataRow row in ds.Tables[1].Rows)
                    //{
                    //    appIdsSelected.Add(Convert.ToString(row["WORKFLOW_ID"]));
                    //}
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objOfflineDt;
        }

        public MFEOfflineDataTable ProcessGetOfflineDTDtlByTblName(string tableName)
        {
            MFEOfflineDataTable objOfflineDt = new MFEOfflineDataTable();
            try
            {
                string strQuery = "SELECT OfflnTbl.* FROM TBL_OFFLINE_DATA_TABLE AS OfflnTbl WHERE TABLE_NAME = @TABLE_NAME AND COMPANY_ID = @COMPANY_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@TABLE_NAME", tableName);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (ds != null)
                {
                    this.ResultTble = ds.Tables[0];
                    objOfflineDt = getOfflineDatatableDtl(ds.Tables[0])[0];
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objOfflineDt;
        }
        string getSqlQueryForOfflineDTDtl()
        {
            return @"SELECT OfflnTbl.* FROM TBL_OFFLINE_DATA_TABLE AS OfflnTbl WHERE COMPANY_ID = @COMPANY_ID";
            //            return @"SELECT OfflnTbl.*,
            //                    DbCommand1.DB_COMMAND_NAME AS DbUploadCmdName,DbCommand2.DB_COMMAND_NAME AS DbDownloadCmdName,
            //                    WsCommand1.WS_COMMAND_NAME AS WsUploadCmdName,WsCommand2.WS_COMMAND_NAME AS WsDownloadCmdName,
            //                    OdataCommand1.ODATA_COMMAND_NAME AS OdUploadCmdName,OdataCommand2.ODATA_COMMAND_NAME AS OdDownloadCmdName
            //                    FROM TBL_OFFLINE_DATA_TABLE AS OfflnTbl
            //
            //                    LEFT OUTER JOIN TBL_DATABASE_COMMAND AS DbCommand1
            //                    ON OfflnTbl.UPLOAD_COMMAND_ID = DbCommand1.DB_COMMAND_ID 
            //                    LEFT OUTER JOIN TBL_DATABASE_COMMAND AS DbCommand2 
            //                    ON OfflnTbl.DOWNLOAD_COMMAND_ID = DbCommand2.DB_COMMAND_ID
            //
            //                    LEFT OUTER JOIN TBL_WS_COMMAND AS WsCommand1
            //                    ON OfflnTbl.UPLOAD_COMMAND_ID = WsCommand1.WS_COMMAND_ID
            //                    LEFT OUTER JOIN TBL_WS_COMMAND AS WsCommand2
            //                    ON OfflnTbl.DOWNLOAD_COMMAND_ID = WsCommand2.WS_COMMAND_ID
            //
            //                    LEFT OUTER JOIN TBL_ODATA_COMMAND AS OdataCommand1
            //                    ON OfflnTbl.UPLOAD_COMMAND_ID = OdataCommand1.ODATA_COMMAND_ID
            //                    LEFT OUTER JOIN TBL_ODATA_COMMAND AS OdataCommand2
            //                    ON OfflnTbl.DOWNLOAD_COMMAND_ID = OdataCommand2.ODATA_COMMAND_ID
            //
            //                    WHERE COMPANY_ID = @COMPANY_ID AND SUB_ADMIN_ID = @SUB_ADMIN_ID";
        }
        string getSqlQueryForOfflineDTDtlWithCommand()
        {
            return @"SELECT * FROM TBL_OFFLINE_DATA_TABLE WHERE COMPANY_ID = @COMPANY_ID";
        }
        string getSqlQueryForOfflineTblByTblId()
        {
            return @"SELECT * FROM TBL_OFFLINE_DATA_TABLE
                    WHERE COMPANY_ID = @COMPANY_ID 
                    AND TABLE_ID = @TABLE_ID;
";
        }
        #region Public Properties
        public string CompanyId
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public DataTable ResultTble
        {
            get;
            private set;
        }
        public List<MFEOfflineDataTable> offlineDatatables
        {
            get;
            private set;
        }
        #endregion
    }
}