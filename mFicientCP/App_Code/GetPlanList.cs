﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class GetPlanList
    {
        /// <summary>
        /// not for trial plan
        /// </summary>
        /// <param name="currency"></param>
        public GetPlanList(string currency)
        {
            this.Currency = currency;
            this.Months = int.MinValue;
            this.IsTrial = false;

            this.MaxWorkFlow = String.Empty;
        }
        /// <summary>
        /// Not for Trial Plan
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="months"></param>
        public GetPlanList(string currency, int months)
        {
            this.Currency = currency;
            this.Months = months;
            this.IsTrial = false;

            this.MaxWorkFlow = String.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="planCode"></param>
        /// <param name="currency"></param>
        /// <param name="months"></param>
        public GetPlanList(string planCode, string currency, int months, bool isTrial)
        {
            PlanCode = planCode;
            this.Currency = currency;
            this.Months = months;
            this.IsTrial = isTrial;

            this.MaxWorkFlow = String.Empty;
        }
        //added on 13-8-2012.to get plan list whose workflow is greater than or less than the current plan
        /// <summary>
        /// 
        /// </summary>
        /// <param name="planCode"></param>
        /// <param name="isUpgradedList">true if we want to get the list whose work flows are greater than the workflow provided</param>
        public GetPlanList(string maxWorkFlow, bool isUpgradedList, string currency, int months)
        {
            MaxWorkFlow = maxWorkFlow;
            IsUpgradedList = isUpgradedList;
            this.Currency = currency;
            this.Months = months;
            this.IsTrial = false;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string strQuery = getSqlQuery();

                SqlCommand cmd = new SqlCommand(strQuery);
                if (!String.IsNullOrEmpty(PlanCode))
                {
                    cmd.Parameters.AddWithValue("@PlanCode", PlanCode);
                }
                if (!String.IsNullOrEmpty(MaxWorkFlow))
                {
                    cmd.Parameters.AddWithValue("@MAX_WORKFLOW", MaxWorkFlow);
                }
                if (this.Months != int.MinValue)
                {
                    cmd.Parameters.AddWithValue("@Month", this.Months);
                }
                cmd.Parameters.AddWithValue("@Currency", this.Currency);
                cmd.CommandType = CommandType.Text;
                DataSet dsPlanDtls = MSSqlClient.SelectDataFromSQlCommandAdminData(cmd);
                if (dsPlanDtls != null)
                {

                    PlanDetails = dsPlanDtls.Tables[0];
                    if (dsPlanDtls.Tables[0].Rows.Count > 0)
                    {
                        this.MinNoOfUsers = Convert.ToInt32(dsPlanDtls.Tables[0].Rows[0]["MIN_USERS"]);
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public void PlanDiscounts()
        {
            try
            {
                string strQuery = getDisountSqlQuery();

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                DataSet dsPlanDiscounts = MSSqlClient.SelectDataFromSQlCommandAdminData(cmd);
                if (dsPlanDiscounts != null)
                {

                    PlanDiscountList = dsPlanDiscounts.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }

            }
        }
        //        string getSqlQuery()
        //        {
        //            string strQuery;
        //            if (String.IsNullOrEmpty(MaxWorkFlow) 
        //                && String.IsNullOrEmpty(PlanCode) && 
        //                this.Months == int.MinValue)//setting int.minvlue at the constructor if the requesst does not invove months.
        //            {
        //                  strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
        //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
        //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
        //                                WHERE Plans.IS_ENABLED =1
        //                                AND PRICE.CURRENCY = @Currency";

        //            }//added on 13-08-2012 this first condition check
        //            else if (String.IsNullOrEmpty(MaxWorkFlow))
        //            {
        //                if (String.IsNullOrEmpty(PlanCode))
        //                {
        //                    //old query changed on 25/20/2012.Tables were changed and we have to get the price from a new table.Admin_tbl_plan_price
        //                    //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE IS_ENABLED =1";//changed on 17/9/2012 Added admin_//WHERE IS_ENABLED =1 18/9/2012
        //                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
        //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
        //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
        //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
        //                                AND Plans.IS_ENABLED =1
        //                                AND PRICE.CURRENCY = @Currency";
        //                }
        //                else
        //                {
        //                    if (!IsTrial)
        //                    {
        //                        strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
        //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
        //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
        //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
        //                                AND Plans.IS_ENABLED =1
        //                                AND PRICE.CURRENCY = @Currency
        //                                AND Plans.PLAN_CODE = @PlanCode";
        //                    }
        //                    else
        //                    {
        //                        strQuery = @"SELECT * FROM ADMIN_TBL_TRIAL_PLANS
        //                                     WHERE PLAN_CODE =  @PlanCode";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (IsUpgradedList)
        //                {
        //                    //old query
        //                    //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE MAX_WORKFLOW > @MAX_WORKFLOW AND IS_ENABLED =1";//changed on 17/9/2012 Added admin_//AND IS_ENABLED =1 18/9/2012
        //                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
        //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
        //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
        //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
        //                                AND Plans.IS_ENABLED =1
        //                                AND PRICE.CURRENCY = @Currency
        //                                AND MAX_WORKFLOW > @MAX_WORKFLOW;";
        //                }
        //                else
        //                {
        //                    //old query
        //                    //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE MAX_WORKFLOW < @MAX_WORKFLOW AND IS_ENABLED =1";//changed on 17/9/2012 Added admin_//AND IS_ENABLED =1 18/9/2012
        //                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
        //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
        //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
        //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
        //                                AND Plans.IS_ENABLED =1
        //                                AND PRICE.CURRENCY = @Currency
        //                                AND MAX_WORKFLOW < @MAX_WORKFLOW;";
        //                }
        //            }
        //            return strQuery;
        //        }
        string getSqlQuery()
        {
            string strQuery = String.Empty;
            if (String.IsNullOrEmpty(MaxWorkFlow))
            {
                if (this.Months == int.MinValue)
                {
                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency";
                }
                else
                {
                    if (String.IsNullOrEmpty(this.PlanCode))
                    {
                        strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency";
                    }
                    else
                    {
                        if (!IsTrial)
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                        INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                        ON Plans.PLAN_CODE=Price.PLAN_CODE
                                        WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                        AND Plans.IS_ENABLED =1
                                        AND PRICE.CURRENCY = @Currency
                                        AND Plans.PLAN_CODE = @PlanCode";
                        }
                        else
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_TRIAL_PLANS
                                         WHERE PLAN_CODE =  @PlanCode";
                        }
                     }
                }
            }
            else
            {
                if (IsUpgradedList)
                {

                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND MAX_WORKFLOW > @MAX_WORKFLOW;";
                }
                else
                {

                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND MAX_WORKFLOW < @MAX_WORKFLOW;";
                }
            }
            return strQuery;
            #region Previous Code
            //            if (String.IsNullOrEmpty(MaxWorkFlow)
            //                && String.IsNullOrEmpty(PlanCode) &&
            //                this.Months == int.MinValue)//setting int.minvlue at the constructor if the requesst does not invove months.
            //            {
            //                strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
            //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
            //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
            //                                WHERE Plans.IS_ENABLED =1
            //                                AND PRICE.CURRENCY = @Currency";

            //            }//added on 13-08-2012 this first condition check
            //            else if (String.IsNullOrEmpty(MaxWorkFlow))
            //            {
            //                if (String.IsNullOrEmpty(PlanCode))
            //                {
            //                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
            //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
            //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
            //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
            //                                AND Plans.IS_ENABLED =1
            //                                AND PRICE.CURRENCY = @Currency";
            //                }
            //                else
            //                {
            //                    if (!IsTrial)
            //                    {
            //                        strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
            //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
            //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
            //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
            //                                AND Plans.IS_ENABLED =1
            //                                AND PRICE.CURRENCY = @Currency
            //                                AND Plans.PLAN_CODE = @PlanCode";
            //                    }
            //                    else
            //                    {
            //                        strQuery = @"SELECT * FROM ADMIN_TBL_TRIAL_PLANS
            //                                     WHERE PLAN_CODE =  @PlanCode";
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                if (IsUpgradedList)
            //                {

            //                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
            //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
            //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
            //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
            //                                AND Plans.IS_ENABLED =1
            //                                AND PRICE.CURRENCY = @Currency
            //                                AND MAX_WORKFLOW > @MAX_WORKFLOW;";
            //                }
            //                else
            //                {

            //                    strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
            //                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
            //                                ON Plans.PLAN_CODE=Price.PLAN_CODE
            //                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
            //                                AND Plans.IS_ENABLED =1
            //                                AND PRICE.CURRENCY = @Currency
            //                                AND MAX_WORKFLOW < @MAX_WORKFLOW;";
            //                }
            //            }

            #endregion

        }


        string getDisountSqlQuery()
        {
            string strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_DISCOUNT WHERE ENABLED = 1";//changed on 17/9/2012 Added admin_
            return strQuery;
        }
        public DataTable PlanDetails
        {
            private set;
            get;
        }

        public DataTable PlanDiscountList
        {
            private set;
            get;
        }

        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public string PlanCode
        {
            get;
            private set;
        }
        public string MaxWorkFlow
        {
            get;
            private set;
        }
        public bool IsUpgradedList
        {
            get;
            private set;
        }
        public int Months
        {
            get;
            private set;
        }
        public string Currency
        {
            get;
            private set;
        }
        public int MinNoOfUsers
        {
            get;
            private set;
        }
        public bool IsTrial
        {
            get;
            private set;
        }
    }
}