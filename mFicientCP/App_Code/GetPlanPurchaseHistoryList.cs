﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetPlanPurchaseHistoryList
    {
        public GetPlanPurchaseHistoryList(string companyId)
        {
            CompanyId = companyId;
        }
        public GetPlanPurchaseHistoryList()
        {

        }
        public void Process()
        {
            try
            {
                string strQuery = @"SELECT PLAN_CODE,ACTUAL_PRICE,PURCHASE_DATE,TRANSACTION_TYPE,PLAN_NAME
		                            FROM TBL_COMPANY_PLAN_PURCHASE_HISTORY
                                    WHERE COMPANY_ID = @CompanyId
                                    ORDER BY PURCHASE_DATE DESC;";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", CompanyId);

                cmd.CommandType = CommandType.Text;
                DataSet dsPlanPurchaseHistoryDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsPlanPurchaseHistoryDtls != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    PlanPurchaseHistoryDetails = dsPlanPurchaseHistoryDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public DataTable PlanPurchaseHistoryDetails
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            get;
            set;
        }
    }
}