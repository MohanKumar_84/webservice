﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP

{
    public class GetPublishCategoryAndVersionDtlsByWorkflowid
    {
        public GetPublishCategoryAndVersionDtlsByWorkflowid(string workflowId)
        {
            this.WorkFlowId = workflowId;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string query = @"SELECT * FROM TBL_WORKFLOW_DETAIL  WHERE WF_ID = @WorkFlowId;
                               select DISTINCT v.WF_ID,v.VERSION,v1.VERSION as Cversion from dbo.TBL_PRV_WORKFLOW_DETAIL as v LEFT OUTER join dbo.TBL_CURR_WORKFLOW_DETAIL as v1 on v.WF_ID=v1.WF_ID where v.WF_ID=@WorkFlowId  order by v.VERSION   asc;
                                SELECT * FROM TBL_PRV_WORKFLOW_DETAIL WHERE WF_ID=@WorkFlowId  ORDER BY VERSION;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WorkFlowId", this.WorkFlowId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null && objDataSet.Tables.Count > 0)
                {
                    this.ResultTables = objDataSet;
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }
        public string WorkFlowId
        {
            set;
            get;
        }


        public DataSet ResultTables
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

    }
}