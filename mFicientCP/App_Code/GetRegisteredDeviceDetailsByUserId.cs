﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetRegisteredDeviceDetailsByUserId
    {
        public GetRegisteredDeviceDetailsByUserId(string companyId, string subAdminId, string userId)
        {
            SubAdminId = subAdminId;
            CompanyId = companyId;
            UserId = userId;
        }

        public void Process()
        {
            try
            {

                StatusCode = -1000;

                string strQuery = @" SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                                     FROM TBL_REGISTERED_DEVICE device
                                     INNER JOIN TBL_USER_DETAIL as usr
                                     ON usr.USER_ID = device.USER_ID
                                     WHERE device.COMPANY_ID =@CompanyId
                                     AND device.SUBADMIN_ID=@SubAdminId
                                     AND usr.USER_ID = @UserID;
                                     ";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@UserID", this.UserId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    ResultTables = objDataSet;
                    if (ResultTables.Tables[0] != null)
                    {
                        CommaSeparatedDevIdAndDevType = new List<string>();
                        this.RegDeviceDtlsDevIdAndDevType = new List<MFEDevice>();
                        foreach (DataRow row in ResultTables.Tables[0].Rows)
                        {
                            CommaSeparatedDevIdAndDevType.Add(row["DEVICE_ID"] + "," + row["DEVICE_TYPE"]);
                        }
                        foreach (DataRow row in ResultTables.Tables[0].Rows)
                        {
                            MFEDevice objMFEDevice = new MFEDevice();
                            objMFEDevice.DeviceId = Convert.ToString(row["DEVICE_ID"]);
                            objMFEDevice.DeviceType = Convert.ToString(row["DEVICE_TYPE"]);
                            objMFEDevice.DevicePushMsgId = Convert.ToString(row["DEVICE_PUSH_MESSAGE_ID"]);
                            objMFEDevice.UserId = Convert.ToString(row["USER_ID"]);
                            objMFEDevice.CompanyId = Convert.ToString(row["COMPANY_ID"]);
                            objMFEDevice.RegistrationDate = Convert.ToInt64(row["REGISTRATION_DATE"]);
                            objMFEDevice.SubAdminId = Convert.ToString(row["SUBADMIN_ID"]);
                            objMFEDevice.DeviceModel = Convert.ToString(row["DEVICE_MODEL"]);
                            objMFEDevice.OsVersion = Convert.ToString(row["OS_VERSION"]);
                            objMFEDevice.MficientKey = Convert.ToString(row["MFICIENT_KEY"]);
                            objMFEDevice.DeviceSize = Convert.ToInt32(row["DEVICE_SIZE"]);
                            this.RegDeviceDtlsDevIdAndDevType.Add(objMFEDevice);
                        }
                    }
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                StatusCode = -1000;
            }
        }

        public DataSet ResultTables
        {
            set;
            get;
        }
        public string UserId
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public List<string> CommaSeparatedDevIdAndDevType
        {
            get;
            set;
        }
        public List<MFEDevice> RegDeviceDtlsDevIdAndDevType
        {
            get;
            set;
        }
    }
}