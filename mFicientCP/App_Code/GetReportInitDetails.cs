﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetReportInitDetails
    {
        public GetReportInitDetails()
        {

        }
        public GetReportInitDetails(string _PageInitId, string _ReportId, string _CompanyId)
        {
            this.PageInitId = _PageInitId;
            this.ReportId = _ReportId;
            this.CompanyId = _CompanyId;
        }
        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"SELECT  * FROM  TBL_REPORT_PAGE_INITIALIZATION WHERE REPORT_ID=@REPORT_ID AND COMPANY_ID=@COMPANY_ID AND REPORT_PAGE_INT_ID=@REPORT_PAGE_INT_ID;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@REPORT_ID", this.ReportId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@REPORT_PAGE_INT_ID", this.PageInitId);

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }

            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public string PageInitId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string ReportId
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }



    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}