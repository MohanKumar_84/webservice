﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetSubAdminGroups
    {
        public GetSubAdminGroups(string companyId,string subAdminId)
        {
            this.SubAdminId = subAdminId;
            this.CompanyId = companyId;
        }

        public GetSubAdminGroups(string companyId)
        {
            this.SubAdminId = string.Empty;
            this.CompanyId = companyId;

        }
       
        /// <summary>
        /// Executes the command using only company Id
        /// </summary>
        public void ProcessByCompanyId()
        {
            try
            {
                string strQuery = @"SELECT UsrGrp.GROUP_ID,UsrGrp.GROUP_NAME,UsrGrp.SUBADMIN_ID,
                                    COUNT(UsrGrpLnk.GROUP_ID) AS USER_COUNT FROM TBL_USER_GROUP AS UsrGrp
                                    LEFT OUTER JOIN TBL_USER_GROUP_LINK AS UsrGrpLnk
                                    ON UsrGrp.GROUP_ID = UsrGrpLnk.GROUP_ID
                                    WHERE  UsrGrp.COMPANY_ID = @CompanyId
                                    GROUP BY UsrGrp.GROUP_ID,UsrGrp.GROUP_NAME,UsrGrp.SUBADMIN_ID order by GROUP_NAME asc";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsGroupsDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupsDtls != null)
                {

                    GroupDetails = dsGroupsDtls.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public void GetAdminGroupList()
        {
            try
            {
                string strQuery = @"SELECT UsrGrp.GROUP_ID,UsrGrp.GROUP_NAME, COUNT(UsrGrpLnk.GROUP_ID) AS USER_COUNT FROM TBL_USER_GROUP AS UsrGrp
                                    LEFT OUTER JOIN TBL_USER_GROUP_LINK AS UsrGrpLnk
                                    ON UsrGrp.GROUP_ID = UsrGrpLnk.GROUP_ID
                                    WHERE UsrGrp.COMPANY_ID = @CompanyId
                                    GROUP BY UsrGrp.GROUP_ID,UsrGrp.GROUP_NAME  order by GROUP_NAME asc";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsGroupsDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupsDtls != null)
                {

                    GroupDetails = dsGroupsDtls.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        
        public DataTable GroupDetails
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SubAdminId
        {
            get;
            set;
        }
        public string GroupId
        {
            get;
            set;
        }
        public string CompanyId
        {
            get;
            set;
        }
    }
}