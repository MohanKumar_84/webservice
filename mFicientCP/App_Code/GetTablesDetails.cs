﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using Npgsql;
using System.Collections;
using Oracle.ManagedDataAccess.Client;

namespace mFicientCP
{
    public class GetTablesDetails
    {
        public GetTablesDetails()
        {

        }

        public GetTablesDetails(string _ConnectionString, DatabaseType _DatabaseType, string _SchemaName)
        {
            this.ConnectionString = _ConnectionString;
            this.DbType = _DatabaseType;
            this.SchemaName = _SchemaName;
        }
        public void Process()
        {
            this.StatusCode = -1000;
            switch (this.DbType)
            {
                case DatabaseType.MSSQL:
                    GetMssqlTablesDetails();
                    break;
                case DatabaseType.ORACLE:
                    GetOracleTablesDetails();
                    break;
                case DatabaseType.MYSQL:
                    GetMysqlTablesDetails();
                    break;
                case DatabaseType.POSTGRESQL:
                    GetPostgreTablesDetails();
                    break;
            }
        }

        public DataSet getOracleDatabseTableAndcolumn()
        {
            this.StatusCode = -1000;
            OracleConnection conn = new OracleConnection();
            try
            {
                conn = new OracleConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT table_name as TABLE_NAME FROM user_tables;
                                SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE as column_type, NULLABLE as is_nullable from user_tab_columns;";
                OracleCommand objSqlCommand = new OracleCommand(query, conn);
                DataSet ObjDataSet = new DataSet();
                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                conn.Close();
                this.StatusCode = 0;
                return ObjDataSet;
            }
            catch
            {
                this.StatusCode = -1000;
                conn.Close();
                this.ResultTable = null;
                return null;
            }
        }

        public void ProcessStoredProcedure()
        {
            this.StatusCode = -1000;
            switch (this.DbType)
            {
                case DatabaseType.MSSQL:
                    GetMssqlStoredProcedureList();
                    break;
                case DatabaseType.ORACLE:
                    GetOracleStoredProcedureList();
                    break;
                case DatabaseType.MYSQL:
                    GetMysqlStoredProcedureList();
                    break;
                case DatabaseType.POSTGRESQL:
                    GetPostgreStoredProcedureList();
                    break;
            }
        }
        public void GetOracleStoredProcedures()
        {
            DataSet dssp = null;
            OracleConnection conn = new OracleConnection();
            try
            {
                conn = new OracleConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT OBJECT_NAME as routine_name FROM user_procedures";
                OracleCommand objSqlCommand = new OracleCommand(query, conn);
                dssp = new DataSet();
                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(dssp);
                this.Dsprocedure = dssp;
                this.StatusCode = 0;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void GetOracleStoredProcedureList()
        {
            OracleConnection conn = new OracleConnection();
            try
            {
                string query = @"SELECT proc.OBJECT_NAME as routine_name, arg.ARGUMENT_NAME as parameter_name, arg.DATA_TYPE as parameter_type, arg.DATA_LENGTH as parameter_maxLength, arg.IN_OUT as parameter_mode
                                 FROM user_arguments  arg INNER JOIN user_procedures proc on proc.OBJECT_NAME = arg.OBJECT_NAME";
                OracleCommand objSqlCommand = new OracleCommand(query, conn);
                DataSet dssp = new DataSet();
                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(objSqlCommand);
                this.Dsprocedure = dssp;
                conn.Close();
                this.StatusCode = 0;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        private void GetMssqlTablesDetails()
        {
            this.StatusCode = -1000;
            SqlConnection conn = new SqlConnection();
            try
            {
                conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type 
                                         FROM INFORMATION_SCHEMA.TABLES";
                SqlCommand objSqlCommand = new SqlCommand(query, conn);
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];

                conn.Close();
                this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
                conn.Close();
                this.ResultTable = null;
            }
        }


        private void GetOracleTablesDetails()
        {
            this.StatusCode = -1000;
            OracleConnection conn = new OracleConnection();
            try
            {
                conn = new OracleConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT table_name as TABLE_NAME FROM user_tables";
                OracleCommand objSqlCommand = new OracleCommand(query, conn);
                DataSet ObjDataSet = new DataSet();
                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                conn.Close();
                this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
                conn.Close();
                this.ResultTable = null;
            }
        }
        private void GetMssqlStoredProcedureList()
        {
            this.StatusCode = -1000;
            SqlConnection conn = new SqlConnection();
            try
            {
                conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"WITH CTE (ObjectName, ParameterName, ParameterDataType) AS( SELECT  SO.name AS [ObjectName] ,REPLACE(P.name,'@','') AS [ParameterName],TYPE_NAME(P.user_type_id) AS [ParameterDataType]
 FROM sys.objects AS SO  left outer JOIN sys.parameters AS P ON SO.OBJECT_ID = P.OBJECT_ID
WHERE SO.OBJECT_ID IN ( SELECT OBJECT_ID FROM sys.objects WHERE TYPE IN ('P','FN'))
)
select ObjectName,ParameterName,ParameterDataType from (
select ss.ObjectName,STUFF((select ',' + T.ParameterName from CTE as T where ss.ObjectName=T.ObjectName for xml path('')),1,1,'') as ParameterName ,
  STUFF((select ',' + T.ParameterDataType from CTE as T where ss.ObjectName=T.ObjectName for xml path('')),1,1,'') as ParameterDataType
                 from CTE  ss )t group by ObjectName,ParameterName,ParameterDataType;";
                SqlCommand objSqlCommand = new SqlCommand(query, conn);
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.Dsprocedure = ObjDataSet;
                conn.Close();
                this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
                conn.Close();
                this.ResultTable = null;
            }
        }


        private void GetMysqlTablesDetails()
        {
            this.StatusCode = -1000;
            MySqlConnection conn = new MySqlConnection();

            try
            {
                conn = new MySqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_schema =@DataBaseName";
                MySqlCommand objCommand = new MySqlCommand(query, conn);
                objCommand.Parameters.AddWithValue("@DataBaseName", this.SchemaName);
                DataSet ObjDataSet = new DataSet();
                MySqlDataAdapter objDataAdapter = new MySqlDataAdapter(objCommand);
                objDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
            finally
            {
                conn.Close();
            }
        }

        private List<string> getSchemas()
        {
            List<string> schemas = new List<string>();
            string strSchemas = string.Empty;
            try
            {
                DataTable dtSchema = ExecutePostGreSelectQuery(@"select schema_name from information_schema.schemata where schema_name <> 'information_schema' and schema_name !~ E'^pg_';");
                if (dtSchema != null && dtSchema.Rows.Count > 0)
                {
                    foreach (DataRow row in dtSchema.Rows)
                    {
                        schemas.Add(row["schema_name"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return schemas;
        }


        private void GetPostgreTablesDetails()
        {
            try
            {
                DataSet dsMetaData = new DataSet();
                string strQuery = string.Empty;
                string tableName = string.Empty;
                DataTable dt;
                List<string> schemas = getSchemas();
                DataTable objTable = new DataTable("MetaData");
                objTable.Columns.Add("TABLE_SCHEMA");
                objTable.Columns.Add("TABLE_NAME");
                dsMetaData.Tables.Add(objTable);
                foreach (string schema in schemas)
                {
                    dt = new DataTable();
                    strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where table_type = 'BASE TABLE' and TABLE_SCHEMA = '" + schema + "';";
                    dt = ExecutePostGreSelectQuery(strQuery);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["TABLE_SCHEMA"].ToString() != "public")
                                objTable.Rows.Add(row["TABLE_SCHEMA"], "\"" + row["TABLE_SCHEMA"] + "\".\"" + row["TABLE_NAME"] + "\"");
                            else
                                objTable.Rows.Add(row["TABLE_SCHEMA"], row["TABLE_SCHEMA"] + ".\"" + row["TABLE_NAME"] + "\"");
                        }
                    }
                }
                this.ResultTable = objTable;
            }
            catch
            {
            }
        }

        private void GetMysqlStoredProcedureList()
        {
            this.StatusCode = -1000;
            MySqlConnection conn = new MySqlConnection();

            try
            {
                conn = new MySqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT d.ROUTINE_NAME as ObjectName,GROUP_CONCAT(PARAMETER_NAME) as ParameterName,GROUP_CONCAT(DATA_TYPE) as ParameterDataType FROM (
         select SPECIFIC_NAME,PARAMETER_NAME ,DATA_TYPE from information_schema.parameters as rout   where rout.SPECIFIC_SCHEMA = @DataBaseName AND  
          rout.PARAMETER_MODE!='OUT'  group BY SPECIFIC_NAME,PARAMETER_NAME ,DATA_TYPE)M
           right outer join (select ROUTINE_NAME FROM INFORMATION_SCHEMA.ROUTINES where ROUTINE_SCHEMA=@DataBaseName)d
                            on(M.SPECIFIC_NAME = d.ROUTINE_NAME) group BY SPECIFIC_NAME;";
                MySqlCommand objCommand = new MySqlCommand(query, conn);
                objCommand.Parameters.AddWithValue("@DataBaseName", this.SchemaName);
                DataSet ObjDataSet = new DataSet();
                MySqlDataAdapter objDataAdapter = new MySqlDataAdapter(objCommand);
                objDataAdapter.Fill(ObjDataSet);
                this.Dsprocedure = ObjDataSet;
                this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
            finally
            {
                conn.Close();
            }
        }

        private DataTable ExecutePostGreSelectQuery(string _SelectStatement)
        {
            DataTable dt = new DataTable();
            NpgsqlConnection Conn = new NpgsqlConnection(this.ConnectionString);
            Conn.Open();
            try
            {
                NpgsqlCommand objCommand = new NpgsqlCommand(_SelectStatement, Conn);

                DataSet ObjDataSet = new DataSet();
                NpgsqlDataAdapter objDataAdapter = new NpgsqlDataAdapter(objCommand);
                objDataAdapter.Fill(ObjDataSet);
                dt = ObjDataSet.Tables[0];
            }
            catch 
            {
            }
            finally
            {
                Conn.Close();
            }
            return dt;
        }

        private void GetPostgreStoredProcedureList()
        {
            DataTable dt = new DataTable();
            DataTable objTable = new DataTable();
            //DataTable dt2 = new DataTable();
            DataTable result = new DataTable();
            result.Columns.Add("ObjectName", typeof(string));
            result.Columns.Add("ParameterName", typeof(string));
            result.Columns.Add("ParameterDataType", typeof(string));
            string strQuery;
            try
            {
                List<string> schemas = getSchemas();
                objTable.Columns.Add("schema_name");
                objTable.Columns.Add("routine_name");
                objTable.Columns.Add("parameter_name");
                objTable.Columns.Add("parameter_type");
                objTable.Columns.Add("parameter_mode");
                objTable.Columns.Add("parameter_maxLength");
                foreach (string schema in schemas)
                {
                    strQuery = @"SELECT rout.specific_schema, rout.routine_name,para.parameter_name, para.data_type as parameter_type, para.parameter_mode, para.character_maximum_length as parameter_maxLength
                                 FROM INFORMATION_SCHEMA.routines as rout inner join information_schema.parameters as para on para.specific_name = rout.specific_name
                                 WHERE rout.specific_schema = '" + schema + "';";

                    dt = ExecutePostGreSelectQuery(strQuery);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["specific_schema"].ToString() != "public")
                                objTable.Rows.Add(row["specific_schema"], "\"" + row["specific_schema"] + "\".\"" + row["routine_name"] + "\"", row["parameter_name"], row["parameter_type"], row["parameter_mode"], row["parameter_maxLength"]);
                            else
                                objTable.Rows.Add(row["specific_schema"], row["specific_schema"] + ".\"" + row["routine_name"] + "\"", row["parameter_name"], row["parameter_type"], row["parameter_mode"], row["parameter_maxLength"]);
                        }
                    }
                }

                foreach (DataRow row in objTable.Rows)
                {
                    string strobjectname = "";
                    string strparametername = "";
                    string strparameterdattype = "";
                    string strroutinename = Convert.ToString(row["routine_name"]);
                    DataRow[] rowsroutinename = objTable.Select("routine_name = '" + strroutinename + "'");
                    for (int i = 0; i <= rowsroutinename.Length - 1; i++)
                    {
                        if (String.IsNullOrEmpty(strparametername))
                        {
                            strobjectname = Convert.ToString(rowsroutinename[i]["routine_name"]);
                            strparametername = Convert.ToString(rowsroutinename[i]["parameter_name"]);
                            strparameterdattype = Convert.ToString(rowsroutinename[i]["parameter_type"]);
                        }
                        else
                        {

                            strparametername += "," + Convert.ToString(rowsroutinename[i]["parameter_name"]);
                            strparameterdattype += "," + Convert.ToString(rowsroutinename[i]["parameter_type"]);
                        }
                    }
                    DataRow rowToAdd = result.NewRow();
                    rowToAdd["ObjectName"] = strobjectname;
                    rowToAdd["ParameterName"] = strparametername;
                    rowToAdd["ParameterDataType"] = strparameterdattype;
                    result.Rows.Add(rowToAdd);
                }
                this.Dsprocedure = new DataSet();
                this.Dsprocedure.Tables.Add(DeleteDuplicateFromDataTable(result, "ObjectName"));
            }
            catch (Exception ex)
            {
                this.Dsprocedure = new DataSet();
                throw ex;
            }
        }
        private DataTable DeleteDuplicateFromDataTable(DataTable dtDuplicate,
                                                       string columnName)
        {
            Hashtable hashT = new Hashtable();
            ArrayList arrDuplicate = new ArrayList();
            foreach (DataRow row in dtDuplicate.Rows)
            {
                if (hashT.Contains(row[columnName]))
                    arrDuplicate.Add(row);
                else
                    hashT.Add(row[columnName], string.Empty);
            }
            foreach (DataRow row in arrDuplicate)
                dtDuplicate.Rows.Remove(row);

            return dtDuplicate;
        }

        public string ConnectionString
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public DataTable ResultTableSP
        {
            set;
            get;
        }
        public DataSet Dsprocedure
        {
            set;
            get;
        }

        public DatabaseType DbType
        {
            set;
            get;
        }
        public string SchemaName
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
    }
}