﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetUsersofGroup
    {
        public GetUsersofGroup()
        {

        }
        public void Process()
        {
            try
            {
                string strQuery = @"SELECT userDetail.*, userGroup.* FROM TBL_USER_GROUP_LINK userGroup INNER JOIN
                                    TBL_USER_DETAIL userDetail ON userGroup.USER_ID = userDetail.USER_ID WHERE userGroup.GROUP_ID  = @GROUP_ID;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@GROUP_ID", GroupId);

                cmd.CommandType = CommandType.Text;
                DataSet dsGroupUsersDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupUsersDtls != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    GroupUsers = dsGroupUsersDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public void ProcessByGroupIds()
        {
            try
            {
                string strQuery = @"SELECT distinct userDetail.* FROM TBL_USER_GROUP_LINK userGroup INNER JOIN
                                    TBL_USER_DETAIL userDetail ON userGroup.USER_ID = userDetail.USER_ID WHERE userGroup.GROUP_ID in (" + GroupId + ");";
                SqlCommand cmd = new SqlCommand(strQuery);

                cmd.CommandType = CommandType.Text;
                DataSet dsGroupUsersDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupUsersDtls != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    GroupUsers = dsGroupUsersDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public void GetOtherUsers()
        {
            try
            {
                string strQuery = @"SELECT FIRST_NAME,LAST_NAME,EMAIL_ID,USER_NAME,USER_ID FROM TBL_USER_DETAIL WHERE USER_ID not in 
                                    (SELECT USER_ID FROM TBL_USER_GROUP_LINK WHERE GROUP_ID = @GROUP_ID)
                                    AND COMPANY_ID = @CompanyId";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@GROUP_ID", GroupId);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsGroupUsersDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupUsersDtls != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    GroupUsers = dsGroupUsersDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public string GetUsersName()
        {
            try
            {
                string strQuery = @" SELECT SUBSTRING((select ' , '+ d.FIRST_NAME+' '+d.LAST_NAME+' ( '+d.USER_NAME +' )' from TBL_USER_GROUP g inner join TBL_USER_GROUP_LINK u
                on g.GROUP_ID=u.GROUP_ID inner join TBL_USER_DETAIL d on u.USER_ID=d.USER_ID where g.GROUP_ID=@GROUP_ID FOR XML PATH('')),3,3000)as USERS";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@GROUP_ID", GroupId);

                cmd.CommandType = CommandType.Text;
                DataSet dsGroupUsersDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupUsersDtls != null && dsGroupUsersDtls.Tables[0].Rows.Count > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    return dsGroupUsersDtls.Tables[0].Rows[0][0].ToString();
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return "";
        }

        public string GetAllUser(string CompanyId)
        {
            try
            {
                string strQuery = @"select distinct USER_ID,USER_NAME from  TBL_USER_DETAIL where COMPANY_ID=@COMPANY_ID;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsGroupUsersDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupUsersDtls != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    GroupUsers = dsGroupUsersDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return "";
        }

       

        public DataTable GroupUsers
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string GroupId
        {
            get;
            set;
        }
        public string CompanyId
        {
            get;
            set;
        }
    }
}