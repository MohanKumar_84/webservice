﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetWFDetailWithVersion
    {
        string _subAdminId;
        string _companyId;
        DataTable _wfDetails;

        


        public GetWFDetailWithVersion(string companyId)
        {
            this.SubAdminId = String.Empty;
            this.CompanyId = companyId;
        }
        public GetWFDetailWithVersion(string subAdminId, string companyId)
        {
            this.SubAdminId = subAdminId;
            this.CompanyId = companyId;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string strQuery = String.Empty;
                if (!String.IsNullOrEmpty(this.SubAdminId))
                {
                    strQuery = @"SELECT distinct WFDet.COMPANY_ID,WFDet.WF_ID,WFDet.WF_NAME,
                                WFDet.SUBADMIN_ID,CurrWFDet.VERSION,
                                SubAdmin.FULL_NAME,WFDet.WF_DESCRIPTION
                                FROM TBL_WORKFLOW_DETAIL AS WFDet
                                LEFT OUTER JOIN TBL_CURR_WORKFLOW_DETAIL AS CurrWFDet
                                ON WFDet.WF_ID = CurrWFDet.WF_ID
                                INNER JOIN TBL_SUB_ADMIN AS SubAdmin
                                ON  WFDet.SUBADMIN_ID = SubAdmin.SUBADMIN_ID
                                WHERE WFDet.SUBADMIN_ID = @SubAdminId
                                AND WFDet.COMPANY_ID = @CompanyId";
                }
                else
                {
                    strQuery = @"SELECT distinct WFDet.COMPANY_ID,WFDet.WF_ID,WFDet.WF_NAME,
                                WFDet.SUBADMIN_ID,CurrWFDet.VERSION,
                                SubAdmin.FULL_NAME,WFDet.WF_DESCRIPTION
                                FROM TBL_WORKFLOW_DETAIL AS WFDet
                                LEFT OUTER JOIN TBL_CURR_WORKFLOW_DETAIL AS CurrWFDet
                                ON WFDet.WF_ID = CurrWFDet.WF_ID
                                INNER JOIN TBL_SUB_ADMIN AS SubAdmin
                                ON  WFDet.SUBADMIN_ID = SubAdmin.SUBADMIN_ID
                                WHERE WFDet.COMPANY_ID = @CompanyId";
                }
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;

                if (!String.IsNullOrEmpty(this.SubAdminId))
                {
                    cmd.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                }
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                DataSet dsWFDetails = MSSqlClient.SelectDataFromSQlCommand(cmd);

                if (dsWFDetails != null)
                {
                    this.WfDetails = dsWFDetails.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    StatusCode = -1000;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch
            {
                StatusCode = -1000;
            }
        }




        public string SubAdminId
        {
            get { return _subAdminId; }
            private set { _subAdminId = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public DataTable WfDetails
        {
            get { return _wfDetails; }
            private set { _wfDetails = value; }
        }
    }
}