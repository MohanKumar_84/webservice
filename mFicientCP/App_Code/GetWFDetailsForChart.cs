﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetWFDetailsForChart
    {
        #region private members
        enum WF_SEARCH_CRITERIA
        {
            CompanyId,
            CmpIdAndMenuId,
            CmpIdMenuIdSubAdmin,
        }
        string _companyId, _menuId, _subAdminId;
        WF_SEARCH_CRITERIA eSearchCriteria = WF_SEARCH_CRITERIA.CmpIdAndMenuId;
        #endregion
        #region Constructors
        public GetWFDetailsForChart(string companyId)
        {
            eSearchCriteria = WF_SEARCH_CRITERIA.CompanyId;
            this.CompanyId = companyId;
            this.MenuId = String.Empty;
            this.SubAdminId = String.Empty;
        }
        public GetWFDetailsForChart(string companyId, string menuId)
        {
            eSearchCriteria = WF_SEARCH_CRITERIA.CmpIdAndMenuId;
            this.CompanyId = companyId;
            this.MenuId = menuId;
            this.SubAdminId = String.Empty;
        }
        public GetWFDetailsForChart(string companyId, string menuId, string subAdminId)
        {
            eSearchCriteria = WF_SEARCH_CRITERIA.CmpIdMenuIdSubAdmin;
            this.CompanyId = companyId;
            this.MenuId = menuId;
            this.SubAdminId = subAdminId;
        }
        #endregion
        #region Public Methods
        public void Process(bool getCountOnly)
        {
            try
            {
                string strQuery = String.Empty;
                if (getCountOnly)
                {
                    strQuery = getSqlQueryForCount();
                }
                else
                {
                    strQuery = getSqlQuery();
                }
                SqlCommand cmd = getSqlCommandToRun(strQuery);
                DataSet dsWFDetails = MSSqlClient.SelectDataFromSQlCommand(cmd);

                if (dsWFDetails == null) { throw new Exception("Internal server error."); };
                this.WFDetails = dsWFDetails.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public void Process(bool getCountOnly, long fromDate, long toDate)
        {
            try
            {
                string strQuery = String.Empty;
                if (getCountOnly)
                {
                    strQuery = getSqlQueryForCountWithDateCondition();
                }
                else
                {
                    strQuery = getSqlQueryWithDateCondition();
                }
                SqlCommand cmd = getSqlCommandToRunWithDateParams(strQuery, fromDate, toDate);
                DataSet dsWFDetails = MSSqlClient.SelectDataFromSQlCommand(cmd);

                if (dsWFDetails == null) { throw new Exception("Internal server error."); };
                this.WFDetails = dsWFDetails.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        #endregion
        #region Private Methods
        string getSqlQuery()
        {
            string strQuery = String.Empty;
            switch (ESearchCriteria)
            {
                case WF_SEARCH_CRITERIA.CompanyId:
                    strQuery = @"SELECT DISTINCT(WFLog.WF_ID) As C_WF_ID,WFLog.WF_ID,WFLog.PROCESS_DATETIME,WFDet.WF_NAME,
                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLog
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLog.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLog.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLog.COMPANY_ID =@COMPANY_ID 
                                AND WFLog.WF_ID=WFDet.PARENT_ID
                                ORDER BY WFDet.WF_NAME;";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdAndMenuId:
                    strQuery = @"SELECT DISTINCT(WFLog.WF_ID) As C_WF_ID,WFLog.WF_ID,WFLog.PROCESS_DATETIME,WFDet.WF_NAME,
                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLog
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLog.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLog.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLog.COMPANY_ID =@COMPANY_ID
                                AND MenuCat.CATEGORY_ID =@MenuCatId 
                                AND WFLog.WF_ID=WFDet.PARENT_ID
                                ORDER BY WFDet.WF_NAME;";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdMenuIdSubAdmin:
                    strQuery = @"SELECT DISTINCT(WFLog.WF_ID) As C_WF_ID,WFLog.WF_ID,WFLog.PROCESS_DATETIME,WFDet.WF_NAME,
                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLog
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLog.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLog.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLog.COMPANY_ID =@COMPANY_ID 
                                AND MenuCat.CATEGORY_ID =@MenuCatId
                                AND WF.SUBADMIN_ID =@SubAdminId 
                                AND WFLog.WF_ID=WFDet.PARENT_ID
                                ORDER BY WFDet.WF_NAME;";
                    break;
            }
            return strQuery;
        }
        string getSqlQueryForCount()
        {
            string strQuery = String.Empty;
            switch (ESearchCriteria)
            {
                case WF_SEARCH_CRITERIA.CompanyId:
                    strQuery = @"SELECT COUNT(WFLOG.WF_ID) As COUNT_WF,WFLOG.WF_ID,WFDet.WF_NAME
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLOG
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLOG.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLOG.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLOG.COMPANY_ID =@COMPANY_ID
                                AND WFLOG.WF_ID=WFDet.WF_ID 
                                GROUP BY WFLOG.WF_ID,WFDet.WF_NAME";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdAndMenuId:
                    strQuery = @"SELECT COUNT(WFLOG.WF_ID) As COUNT_WF,WFLOG.WF_ID,WFDet.WF_NAME
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLOG
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLOG.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLOG.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLOG.COMPANY_ID =@COMPANY_ID
                                AND MenuCat.MENU_CATEGORY_ID =@MenuCatId
                                AND WFLOG.WF_ID=WFDet.WF_ID 
                                GROUP BY WFLOG.WF_ID,WFDet.WF_NAME";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdMenuIdSubAdmin:
                    strQuery = @"SELECT COUNT(WFLOG.WF_ID) As COUNT_WF,WFLOG.WF_ID,WFDet.WF_NAME
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLOG
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLOG.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLOG.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLOG.COMPANY_ID =@COMPANY_ID
                                AND MenuCat.MENU_CATEGORY_ID =@MenuCatId
                                AND WF.SUBADMIN_ID =@SubAdminId 
                                AND WFLOG.WF_ID=WFDet.WF_ID 
                                GROUP BY WFLOG.WF_ID,WFDet.WF_NAME";
                    break;
            }
            return strQuery;
        }
        string getSqlQueryWithDateCondition()
        {
            string strQuery = String.Empty;
            switch (ESearchCriteria)
            {
                case WF_SEARCH_CRITERIA.CompanyId:
                    strQuery = @"SELECT DISTINCT(WFLog.WF_ID) As C_WF_ID,WFLog.WF_ID,WFLog.PROCESS_DATETIME,WFDet.WF_NAME,
                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLog
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLog.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLog.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLog.COMPANY_ID =@COMPANY_ID 
                                AND WFLog.WF_ID=WFDet.PARENT_ID
                                AND WFLOG.PROCESS_DATETIME BETWEEN @FROM_DATE AND @TO_DATE
                                ORDER BY WFDet.WF_NAME;";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdAndMenuId:
                    strQuery = @"SELECT DISTINCT(WFLog.WF_ID) As C_WF_ID,WFLog.WF_ID,WFLog.PROCESS_DATETIME,WFDet.WF_NAME,
                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLog
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLog.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLog.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLog.COMPANY_ID =@COMPANY_ID
                                AND MenuCat.CATEGORY_ID =@MenuCatId 
                                AND WFLog.WF_ID=WFDet.PARENT_ID
                                AND WFLOG.PROCESS_DATETIME BETWEEN @FROM_DATE AND @TO_DATE
                                ORDER BY WFDet.WF_NAME;";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdMenuIdSubAdmin:
                    strQuery = @"SELECT DISTINCT(WFLog.WF_ID) As C_WF_ID,WFLog.WF_ID,WFLog.PROCESS_DATETIME,WFDet.WF_NAME,
                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLog
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLog.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLog.WF_ID = WFCat.WORKFLOW_ID
                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
                                WHERE  WFLog.COMPANY_ID =@COMPANY_ID 
                                AND MenuCat.CATEGORY_ID =@MenuCatId
                                AND WF.SUBADMIN_ID =@SubAdminId 
                                AND WFLog.WF_ID=WFDet.PARENT_ID
                                AND WFLOG.PROCESS_DATETIME BETWEEN @FROM_DATE AND @TO_DATE
                                ORDER BY WFDet.WF_NAME;";
                    break;
            }
            return strQuery;
        }
        string getSqlQueryForCountWithDateCondition()
        {
            string strQuery = String.Empty;
            switch (ESearchCriteria)
            {
                case WF_SEARCH_CRITERIA.CompanyId:
                    strQuery = @"SELECT COUNT(WFLOG.WF_ID) As COUNT_WF,WFLOG.WF_ID,WFDet.WF_NAME
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLOG
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLOG.WF_ID
                                WHERE  WFLOG.COMPANY_ID =@COMPANY_ID
                                AND WFLOG.WF_ID=WFDet.WF_ID 
                                AND WFLOG.PROCESS_DATETIME BETWEEN @FROM_DATE AND @TO_DATE
                                GROUP BY WFLOG.WF_ID,WFDet.WF_NAME";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdAndMenuId:
                    strQuery = @"SELECT COUNT(WFLOG.WF_ID) As COUNT_WF,WFLOG.WF_ID,WFDet.WF_NAME
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLOG
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLOG.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLOG.WF_ID = WFCat.WORKFLOW_ID
                                WHERE  WFLOG.COMPANY_ID =@COMPANY_ID
                                AND  WFCat.CATEGORY_ID =@MenuCatId
                                AND WFLOG.WF_ID=WFDet.WF_ID 
                                AND WFLOG.PROCESS_DATETIME BETWEEN @FROM_DATE AND @TO_DATE
                                GROUP BY WFLOG.WF_ID,WFDet.WF_NAME";
                    break;
                case WF_SEARCH_CRITERIA.CmpIdMenuIdSubAdmin:
                    strQuery = @"SELECT COUNT(WFLOG.WF_ID) As COUNT_WF,WFLOG.WF_ID,WFDet.WF_NAME
                                FROM TBL_WORKFLOW_USAGE_LOG AS WFLOG
                                INNER JOIN TBL_WORKFLOW_DETAIL AS WFDet
                                ON WFDet.WF_ID = WFLOG.WF_ID
                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
                                ON WFLOG.WF_ID = WFCat.WORKFLOW_ID
                                WHERE  WFLOG.COMPANY_ID =@COMPANY_ID
                                AND WFCat.CATEGORY_ID  =@MenuCatId
                                AND WFDet.SUBADMIN_ID =@SubAdminId 
                                AND WFLOG.WF_ID=WFDet.WF_ID 
                                AND WFLOG.PROCESS_DATETIME BETWEEN @FROM_DATE AND @TO_DATE
                                GROUP BY WFLOG.WF_ID,WFDet.WF_NAME";
                    break;
            }
            return strQuery;
        }
        SqlCommand getSqlCommandToRun(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            switch (ESearchCriteria)
            {
                case WF_SEARCH_CRITERIA.CompanyId:
                    cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    break;
                case WF_SEARCH_CRITERIA.CmpIdAndMenuId:
                    cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    cmd.Parameters.AddWithValue("@MenuCatId", this.MenuId);
                    break;
                case WF_SEARCH_CRITERIA.CmpIdMenuIdSubAdmin:
                    cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    cmd.Parameters.AddWithValue("@MenuCatId", this.MenuId);
                    cmd.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                    break;
            }
            return cmd;
        }
        SqlCommand getSqlCommandToRunWithDateParams(string query, long fromDate, long toDate)
        {
            SqlCommand cmd = getSqlCommandToRun(query);
            cmd.Parameters.AddWithValue("@FROM_DATE", fromDate);
            cmd.Parameters.AddWithValue("@TO_DATE", toDate);
            return cmd;
        }
        #endregion
        #region Properties

        public string SubAdminId
        {
            get { return _subAdminId; }
            set { _subAdminId = value; }
        }

        public string MenuId
        {
            get { return _menuId; }
            set { _menuId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        private WF_SEARCH_CRITERIA ESearchCriteria
        {
            get { return eSearchCriteria; }
            set { eSearchCriteria = value; }
        }
        public DataTable WFDetails
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }

        #endregion
    }


}