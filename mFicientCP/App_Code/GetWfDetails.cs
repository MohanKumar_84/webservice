﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetWfDetails
    {
        public GetWfDetails()
        {

        }

        public GetWfDetails(Boolean _IsSelectAll, Boolean _IsCheckName, Boolean _IsMain, string _WfId, string _SubAdminId, string _WfName, string _ParentId, string _CompanyId, string _IsAppForTablet, Boolean _BindRpt)
        {
            this.WfId = _WfId;
            this.WfName = _WfName;
            this.SubAdminId = _SubAdminId;
            this.IsSelectAll = _IsSelectAll;
            this.IsCheckName = _IsCheckName;
            this.ParentId = _ParentId;
            this.IsMain = _IsMain;
            this.IsAppForTablet = _IsAppForTablet;
            this.CompanyId = _CompanyId;
            this.BindRpt = _BindRpt;


        }
//        public void Process()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                string query;
//                DataSet objDataSet;
//                SqlCommand objSqlCommand;
//                if (this.IsSelectAll == true)
//                {
//                    //                    query = @"SELECT Wf.*,wd.count from (SELECT  * FROM  TBL_WORKFLOW_DETAIL
//                    //                                    WHERE SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID AND MODEL_TYPE=@MODEL_TYPE AND WF_ID =PARENT_ID) AS Wf JOIN
//                    //                                    (SELECT COUNT(WF_ID) count,WF_ID FROM TBL_WORKFLOW_DETAIL  GROUP BY WF_ID) AS  wd  ON Wf.WF_ID=wd.WF_ID ORDER BY Wf.WF_NAME;";
//                    if (this.BindRpt)
//                    {

//                        query = @"SELECT Wf.*,wd.count from (SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='1'
//                                            AND WF_ID not in (SELECT  WF_ID FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='0')
//                                            AND SUBADMIN_ID=@SUBADMIN_ID  AND COMPANY_ID=@COMPANY_ID
//                                            UNION
//                                            SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='0'
//                                            AND SUBADMIN_ID=@SUBADMIN_ID  AND COMPANY_ID=@COMPANY_ID) AS Wf JOIN
//                                            (SELECT COUNT(WF_ID) count,WF_ID FROM TBL_WORKFLOW_DETAIL  GROUP BY WF_ID) AS  wd  
//                                             ON Wf.WF_ID=wd.WF_ID where wf.APP_JS_JSON=''  ORDER BY Wf.WF_NAME;";

//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    }
//                    else
//                    {
//                        query = @"SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID AND MODEL_TYPE=@MODEL_TYPE ORDER BY WF_NAME;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                        objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", this.IsAppForTablet);
//                    }
//                    //objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", this.IsAppForTablet);
//                }
//                else if (IsCheckName)
//                {
//                    if (!this.IsMain)
//                    {
//                        query = @"SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE WF_ID !=@WF_ID AND WF_NAME =@WF_NAME AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID AND MODEL_TYPE=@MODEL_TYPE AND PARENT_ID !=@PARENT_ID  ORDER BY WF_NAME;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
//                        objSqlCommand.Parameters.AddWithValue("@WF_NAME", this.WfName);
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                        objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", this.IsAppForTablet);
//                    }
//                    else
//                    {
//                        query = @"SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE WF_ID !=@WF_ID AND WF_NAME =@WF_NAME AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID AND MODEL_TYPE=@MODEL_TYPE AND WF_ID=PARENT_ID ORDER BY WF_NAME;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
//                        objSqlCommand.Parameters.AddWithValue("@WF_NAME", this.WfName);
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                        objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", this.IsAppForTablet);
//                    }
//                }
//                else
//                {
//                    if (!this.IsMain)
//                    {
//                        query = @"SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID AND MODEL_TYPE=@MODEL_TYPE ORDER BY WF_NAME;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                        objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", this.IsAppForTablet);
//                    }
//                    else
//                    {
//                        query = @"SELECT  wd.* FROM  TBL_WORKFLOW_DETAIL  wd
//			                        WHERE wd.WF_ID=@WF_ID AND wd.SUBADMIN_ID=@SUBADMIN_ID AND wd.COMPANY_ID=@COMPANY_ID ORDER BY wd.MODEL_TYPE;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    }
//                }
//                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                this.ResultTable = objDataSet.Tables[0];
//                if (objDataSet.Tables[0].Rows.Count > 0) this.StatusCode = 0;
//                else this.StatusCode = -1000;
//            }
//            catch
//            {
//                this.StatusCode = -1000;
//            }
//        }
//        public static DataTable getWFAndCatLinkDtlByWfId(string workFlowId)
//        {
//            DataTable dtblWFAndCatLinkDtl = new DataTable();
//            try
//            {
//                string query = @"SELECT * FROM TBL_WORKFLOW_ADDITIONAL_DETAIL
//                                WHERE WORKFLOW_ID = @WorkFlowId;";
//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WorkFlowId", workFlowId);
//                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                if (objDataSet != null)
//                {
//                    dtblWFAndCatLinkDtl = objDataSet.Tables[0];
//                }
//                else
//                {
//                    dtblWFAndCatLinkDtl = null;
//                }

//            }
//            catch
//            {
//                dtblWFAndCatLinkDtl = null;
//            }
//            return dtblWFAndCatLinkDtl;
//        }
        public static DataTable getAllWfAndAdditionalDtlForCmp(string subAdminId, string companyId)
        {
            DataTable dtblWFAndCatLinkDtl = new DataTable();
            try
            {
                string query = @"SELECT Wf.*,ADD_DET.WF_ICON,wd.WF_COUNT FROM 
                                (SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='1'
                                AND WF_ID not in (SELECT  WF_ID FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='0')
                                AND SUBADMIN_ID=@SUBADMIN_ID  AND COMPANY_ID=@COMPANY_ID
                                UNION
                                SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='0'
                                AND SUBADMIN_ID=@SUBADMIN_ID  AND COMPANY_ID=@COMPANY_ID) AS Wf JOIN
                                (SELECT COUNT(WF_ID) WF_COUNT,WF_ID FROM TBL_WORKFLOW_DETAIL  GROUP BY WF_ID) AS  wd
                                ON Wf.WF_ID=wd.WF_ID 
                                LEFT OUTER JOIN TBL_WORKFLOW_ADDITIONAL_DETAIL AS ADD_DET 
                                ON ADD_DET.WORKFLOW_ID = wd.WF_ID ORDER BY Wf.WF_NAME;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    dtblWFAndCatLinkDtl = objDataSet.Tables[0];
                }
                else
                {
                    dtblWFAndCatLinkDtl = null;
                }

            }
            catch
            {
                dtblWFAndCatLinkDtl = null;
            }
            return dtblWFAndCatLinkDtl;
        }
        public static DataTable getWorkflowDtlById(string wfId, string subAdminId, string companyId)
        {
            DataTable dtblWorkflowIdDtl = new DataTable();
            try
            {
                string query = @"SELECT * FROM TBL_WORKFLOW_DETAIL
                                WHERE WF_ID = @WF_ID AND SUBADMIN_ID = @SUBADMIN_ID AND COMPANY_ID = @COMPANY_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", wfId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    dtblWorkflowIdDtl = objDataSet.Tables[0];
                }
                else
                {
                    dtblWorkflowIdDtl = null;
                }

            }
            catch
            {
                dtblWorkflowIdDtl = null;
            }
            return dtblWorkflowIdDtl;
        }
        public static DataTable getWorkflowDtlByName(string wfName, string subAdminId, string companyId)
        {
            DataTable dtblWorkflowDtl = new DataTable();
            try
            {
                string query = @"Select WF_NAME from TBL_WORKFLOW_DETAIL where WF_NAME=@WF_NAME and COMPANY_ID=@COMPANY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_NAME", wfName);
                //objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    dtblWorkflowDtl = objDataSet.Tables[0];
                }
                else
                {
                    dtblWorkflowDtl = null;
                }

            }
            catch
            {
                dtblWorkflowDtl = null;
            }
            return dtblWorkflowDtl;
        }

        public static DataTable getWorkflowDetailByWfId(string wfId, string companyId)
        {
            DataTable dtblWorkflowIdDtl = new DataTable();
            try
            {
                string query = @"SELECT * FROM TBL_WORKFLOW_DETAIL
                                WHERE WF_ID = @WF_ID AND COMPANY_ID = @COMPANY_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", wfId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    dtblWorkflowIdDtl = objDataSet.Tables[0];
                }
                else
                {
                    dtblWorkflowIdDtl = null;
                }

            }
            catch
            {
                dtblWorkflowIdDtl = null;
            }
            return dtblWorkflowIdDtl;
        }

        public static DataTable getWorkflowDetailByCompId(string companyId)
        {
            DataTable dtblWorkflowIdDtl = new DataTable();
            try
            {
                string query = @"SELECT * FROM TBL_WORKFLOW_DETAIL
                                WHERE COMPANY_ID = @COMPANY_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    dtblWorkflowIdDtl = objDataSet.Tables[0];
                }
                else
                {
                    dtblWorkflowIdDtl = null;
                }

            }
            catch
            {
                dtblWorkflowIdDtl = null;
            }
            return dtblWorkflowIdDtl;
        }

        public string WfId
        {
            set;
            get;
        }
        public string WfName
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }

        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }

        public string ParentId { get; set; }

        public bool IsMain { get; set; }

        public string IsAppForTablet { get; set; }

        public string CompanyId { get; set; }

        public bool IsCheckName { get; set; }

        public bool BindRpt { get; set; }
    }

    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}