﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class HTML5AppFile
    {
        private string fileName, fileContent, fileExtension;

        public HTML5AppFile(string _fileName, string _fileContent, string _fileExtension)
        {
            fileName = _fileName;
            fileContent = _fileContent;
            fileExtension = _fileExtension;
        }

        public HTML5AppFile()
        {

        }

        public string FileName
        {
            get
            {
                return fileName;
            }
        }

        public string FileContent
        {
            get
            {
                return fileContent;
            }
        }

        public string FileExtension
        {
            get
            {
                return fileExtension;
            }
        }
    }

    [DataContract]
    public class Html5AppJson
    {
        public Html5AppJson()
        {
            files = new FileJson();
        }

        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public string appid { get; set; }

        [DataMember]
        public string appnm { get; set; }

        [DataMember]
        public FileJson files { get; set; }

    }

    [DataContract]
    public class FileJson
    {
        public FileJson()
        {
            image = new List<FileObject>();
            style = new List<FileObject>();
            script = new List<FileObject>();
            root = new List<FileObject>();
        }

        [DataMember]
        public List<FileObject> image { get; set; }

        [DataMember]
        public List<FileObject> style { get; set; }

        [DataMember]
        public List<FileObject> script { get; set; }

        [DataMember]
        public List<FileObject> root { get; set; }
    }
}