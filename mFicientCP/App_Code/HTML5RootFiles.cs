﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace mFicientCP
{
    public class HTML5RootFiles : ICollection<HTML5AppFile>
    {
        private static List<HTML5AppFile> rootFiles;
        private object collectionLock;

        public HTML5RootFiles()
        {
            rootFiles = new List<HTML5AppFile>();
            collectionLock = new object();
        }

        int ICollection<HTML5AppFile>.Count
        {
            get
            {
                return rootFiles.Count;
            }
        }

        public List<HTML5AppFile> Files
        {
            get
            {
                return rootFiles;
            }
        }

        public void Add(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (!rootFiles.Contains(item))
                        rootFiles.Add(item);
                }
                catch
                {
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void AddRange(List<HTML5AppFile> files)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    rootFiles.AddRange(files);
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        bool ICollection<HTML5AppFile>.Remove(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (rootFiles.Contains(item))
                    {
                        rootFiles.Remove(item);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    rootFiles.RemoveAt(index);
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void RemoveAll(Predicate<HTML5AppFile> files)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    rootFiles.RemoveAll(files);
                }
                catch
                {
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void Clear()
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    rootFiles.Clear();
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public bool Contains(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (rootFiles.Contains(item))
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return false;
        }

        public HTML5AppFile GetFile(string filename)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    foreach (HTML5AppFile file in rootFiles)
                    {
                        if (file.FileName == filename)
                        {
                            return file;
                        }
                    }
                }
                catch
                {
                    return null;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return null;
        }

        public void CopyTo(HTML5AppFile[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerator<HTML5AppFile> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}