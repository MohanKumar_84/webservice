﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class Html5App
    {
        private HTML5ImageFiles imageFiles;
        private HTML5ScriptFiles scriptFiles;
        private HTML5StyleFiles styleFiles;
        private HTML5RootFiles rootFiles;
        private string appId, companyId, description, appName;

        public Html5App()
        {
            imageFiles = new HTML5ImageFiles();
            scriptFiles = new HTML5ScriptFiles();
            styleFiles = new HTML5StyleFiles();
            rootFiles = new HTML5RootFiles();
            appId = string.Empty;
            companyId = string.Empty;
            description = string.Empty;
        }

        public string AppId
        {
            get
            {
                return appId;
            }
            set
            {
                appId = value;
            }
        }

        public string AppName   {
            get
            {
                return appName;
            }
            set
            {
                appName = value;
            }
        }

        public string CompanyId
        {
            get
            {
                return companyId;
            }
            set
            {
                companyId = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public HTML5ImageFiles Imagefiles
        {
            get
            {
                return imageFiles;
            }
            set
            {
                imageFiles = value;
            }
        }

        public HTML5ScriptFiles Scriptfiles
        {
            get
            {
                return scriptFiles;
            }
            set
            {
                scriptFiles = value;
            }
        }

        public HTML5StyleFiles Stylefiles
        {
            get
            {
                return styleFiles;
            }
            set
            {
                styleFiles = value;
            }
        }

        public HTML5RootFiles Rootfiles
        {
            get
            {
                return rootFiles;
            }
            set
            {
                rootFiles = value;
            }
        }

        public string GetAppFilesJson()
        {
            try
            {
                AppJson appJson = new AppJson();
                List<FileObject> lstImageFiles = new List<FileObject>();
                List<FileObject> lstScriptFiles = new List<FileObject>();
                List<FileObject> lstStyleFiles = new List<FileObject>();
                List<FileObject> lstRootFiles = new List<FileObject>();

                if (imageFiles.Count() > 0)
                {
                    foreach (HTML5AppFile file in imageFiles)
                    {
                        FileObject objImageFile = new FileObject();
                        objImageFile.ext = file.FileExtension;
                        objImageFile.fileid = file.FileName;
                        if (file.FileName.Length > 15)
                            objImageFile.filenm = file.FileName.Substring(0, 15) + "...." + file.FileExtension;
                        else
                            objImageFile.filenm = file.FileName;
                        if (!lstImageFiles.Contains(objImageFile))
                            lstImageFiles.Add(objImageFile);
                    }
                }

                if (scriptFiles.Count() > 0)
                {
                    foreach (HTML5AppFile file in scriptFiles)
                    {
                        FileObject objScriptFile = new FileObject();
                        objScriptFile.ext = file.FileExtension;
                        objScriptFile.fileid = file.FileName;
                        if (file.FileName.Length > 15)
                            objScriptFile.filenm = file.FileName.Substring(0, 15) + "...." + file.FileExtension;
                        else
                            objScriptFile.filenm = file.FileName;
                        if (!lstScriptFiles.Contains(objScriptFile))
                            lstScriptFiles.Add(objScriptFile);
                    }
                }

                if (styleFiles.Count() > 0)
                {
                    foreach (HTML5AppFile file in styleFiles)
                    {
                        FileObject objStyleFile = new FileObject();
                        objStyleFile.ext = file.FileExtension;
                        objStyleFile.fileid = file.FileName;
                        if (file.FileName.Length > 15)
                            objStyleFile.filenm = file.FileName.Substring(0, 15) + "...." + file.FileExtension;
                        else
                            objStyleFile.filenm = file.FileName;
                        if (!lstStyleFiles.Contains(objStyleFile))
                            lstStyleFiles.Add(objStyleFile);
                    }
                }

                if (rootFiles.Count() > 0)
                {
                    foreach (HTML5AppFile file in rootFiles)
                    {
                        FileObject objRootFile = new FileObject();
                        objRootFile.ext = file.FileExtension;
                        objRootFile.fileid = file.FileName;
                        if (file.FileName.Length > 15)
                            objRootFile.filenm = file.FileName.Substring(0, 15) + "...." + file.FileExtension;
                        else
                            objRootFile.filenm = file.FileName;
                        if (!lstRootFiles.Contains(objRootFile))
                            lstRootFiles.Add(objRootFile);
                    }
                }

                appJson.img = lstImageFiles;
                appJson.script = lstScriptFiles;
                appJson.style = lstStyleFiles;
                appJson.root = lstRootFiles;

                return Utilities.SerializeJson<AppJson>(appJson);
            }
            catch
            {
            }
            return string.Empty;
        }

        public void Save()
        {

        }
    }

    [DataContract]
    public class AppJson
    {
        public AppJson()
        {
            img = new List<FileObject>();
            script = new List<FileObject>();
            style = new List<FileObject>();
            root = new List<FileObject>();
        }

        [DataMember]
        public List<FileObject> img { get; set; }

        [DataMember]
        public List<FileObject> script { get; set; }

        [DataMember]
        public List<FileObject> style { get; set; }

        [DataMember]
        public List<FileObject> root { get; set; }
    }

    [DataContract]
    public class FileObject
    {
        [DataMember]
        public string fileid { get; set; }

        [DataMember]
        public string filenm { get; set; }

        [DataMember]
        public string ext { get; set; }
    }
}