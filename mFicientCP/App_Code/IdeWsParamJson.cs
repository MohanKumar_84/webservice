﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Runtime.Serialization;
namespace mFicientCP
{
    //[DataContract]
    //public class IdeWsParamJson
    //{
    //    [DataMember]
    //    public List<IdeWsParam> wsParam { get; set; }
    //}

    public class IdeWsParam
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string path { get; set; }
     }
    public class IdeOdataInputParam
    {
        [DataMember]
        public string para { get; set; }
        [DataMember]
        public string typ { get; set; }
    }
}