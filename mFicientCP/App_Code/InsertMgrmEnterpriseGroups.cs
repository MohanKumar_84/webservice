﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace mFicientCP
{
    public class InsertMgrmEnterpriseGroups
    {
        public InsertMgrmEnterpriseGroups()
        {
        }
        #region FOr Inserting SIngle Record
        public void Process(MFEMgramEnterpriseGroups mGramEnterpriseGroups)
        {
            insertGroupDtls(mGramEnterpriseGroups);
        }
        public void Process(SqlConnection con,
            SqlTransaction transaction,
            MFEMgramEnterpriseGroups mGramEnterpriseGroups)
        {
            insertGroupDtls(con, 
                transaction,
                mGramEnterpriseGroups
                );
        }
        void insertGroupDtls(MFEMgramEnterpriseGroups mGramEnterpriseGroups)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mGramEnterpriseGroups.EnterpriseId);
            cmd.Parameters.AddWithValue("@GROUP_ID", mGramEnterpriseGroups.GroupId);
            cmd.Parameters.AddWithValue("@GROUP_NAME", mGramEnterpriseGroups.GroupName);
            cmd.Parameters.AddWithValue("@USERS", mGramEnterpriseGroups.Users);

            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }
        void insertGroupDtls(SqlConnection con,
            SqlTransaction transaction,
            MFEMgramEnterpriseGroups mGramEnterpriseGroups
            )
        {
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mGramEnterpriseGroups.EnterpriseId);
            cmd.Parameters.AddWithValue("@GROUP_ID", mGramEnterpriseGroups.GroupId);
            cmd.Parameters.AddWithValue("@GROUP_NAME", mGramEnterpriseGroups.GroupName);
            cmd.Parameters.AddWithValue("@USERS", mGramEnterpriseGroups.Users);

            int iRowsEffected = cmd.ExecuteNonQuery();
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }
        #endregion
        #region FOr INserting Multiple records
        public void Process(List< MFEMgramEnterpriseGroups> mGramEnterpriseGroups)
        {
            insertGroupDtls(mGramEnterpriseGroups);
        }
        public void Process(SqlConnection con,
            SqlTransaction transaction,
           List< MFEMgramEnterpriseGroups> mGramEnterpriseGroups)
        {
            insertGroupDtls(con,
                transaction,
                mGramEnterpriseGroups
                );
        }
        void insertGroupDtls(List< MFEMgramEnterpriseGroups> mGramEnterpriseGroups)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            foreach (MFEMgramEnterpriseGroups mGramEnterpriseGrps in mGramEnterpriseGroups)
            {
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mGramEnterpriseGrps.EnterpriseId);
                cmd.Parameters.AddWithValue("@GROUP_ID", mGramEnterpriseGrps.GroupId);
                cmd.Parameters.AddWithValue("@GROUP_NAME", mGramEnterpriseGrps.GroupName);
                cmd.Parameters.AddWithValue("@USERS", mGramEnterpriseGrps.Users);

                int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
                if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
        }
        void insertGroupDtls(SqlConnection con,
            SqlTransaction transaction,
           List< MFEMgramEnterpriseGroups> mGramEnterpriseGroups
            )
        {
            string strQuery = getQuery();
            foreach (MFEMgramEnterpriseGroups mGramEnterpriseGrps in mGramEnterpriseGroups)
            {
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mGramEnterpriseGrps.EnterpriseId);
                cmd.Parameters.AddWithValue("@GROUP_ID", mGramEnterpriseGrps.GroupId);
                cmd.Parameters.AddWithValue("@GROUP_NAME", mGramEnterpriseGrps.GroupName);
                cmd.Parameters.AddWithValue("@USERS", mGramEnterpriseGrps.Users);

                int iRowsEffected = cmd.ExecuteNonQuery();
                if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
        }
        #endregion
        string getQuery()
        {
            return @"INSERT INTO TBL_ENTERPRISE_GROUPS(ENTERPRISE_ID,GROUP_NAME,USERS,GROUP_ID)
                    VALUES(@ENTERPRISE_ID,@GROUP_NAME,@USERS,@GROUP_ID);";

        }
        #region Public Properties
        public List<MFEMgramEnterpriseGroups> MgramEnterpriseGroups
        {
            get;
            private set;
        }
        public MFEMgramEnterpriseGroups MgramEnterpriseGroup
        {
            get;
            private set;
        }
        #endregion
    }
}