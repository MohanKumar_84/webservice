﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
namespace mFicientCP
{
    public class MP_GetOdbcDsnListResp
    {
        //{resp:{\"cd\":\"" + _statusCode + "\",\"desc\":\"" + _description + "\",\"data\":\"" + _data + "\"}}
        string _code, _description;

        public string Description
        {
            get { return _description; }
        }

        public string Code
        {
            get { return _code; }
        }
        DataTable _data;
        public DataTable Data
        {
            get { return _data; }
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public MP_GetOdbcDsnListResp(string json)
        {
            //json="{\"resp\":{\"cd\":\"0\",\"data\":{\"dsn\":[{\"driver\":\"Driver do Microsoft Access (*.mdb)\",\"dsName\":\"MSAcessDSNTest\"},{\"driver\":\"SQL Server\",\"dsName\":\"DSNfromCode\"},{\"driver\":\"SQL Server\",\"dsName\":\"SQLTest\"}]},\"desc\":\"\"}}";
            this.StatusCode = -1000;
            try
            {
                MPGetDSNDataResponse objReps = DeserialiseJson<MPGetDSNDataResponse>(json);
                _code = objReps.resp.cd;
                _description = objReps.resp.desc;
                if (Convert.ToInt32(_code) == 0)
                {
                    _data = new DataTable();
                    DataColumn dc1 = new DataColumn("Driver");
                    DataColumn dc2 = new DataColumn("DsName");
                    _data.Columns.Add(dc1);
                    _data.Columns.Add(dc2);
                    DataRow dr;
                    foreach (MPDSNList item in objReps.resp.data.dsn)
                    {
                        dr = _data.NewRow();
                        dr["Driver"] = item.driver;
                        dr["DsName"] = item.dsName;
                        _data.Rows.Add(dr);
                    }
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }


        T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }
    }

    [DataContract]
    public class MPGetDSNDataResponse
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPGetDSNRespFields resp { get; set; }
    }

    [DataContract]
    public class MPGetDSNRespFields
    {

        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public MPDSNData data { get; set; }
    }

    [DataContract]
    public class MPDSNData
    {
        [DataMember]
        public List<MPDSNList> dsn { get; set; }
    }

    [DataContract]
    public class MPDSNList
    {
        [DataMember]
        public string driver { get; set; }

        [DataMember]
        public string dsName { get; set; }
    }
}