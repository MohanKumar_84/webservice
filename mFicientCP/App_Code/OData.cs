﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Data.Edm;
using Microsoft.Data.OData;
using System.Xml;
using System.IO;

namespace mFicientCP
{
    public enum FunctionType
    {
        WEBGET = 1,
        WEBINVOKE = 2,
        ACTION = 3
    }

    public enum ODataRequestType
    {
        EntityType = 0,
        Function = 1
    }

    public class ODATA
    {
        private IEdmModel edmModel;

        #region Constructor

        public ODATA(string _metadata)
        {
            try
            {
                IEnumerable<Microsoft.Data.Edm.Validation.EdmError> errors = null;
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(_metadata);
                MemoryStream ms = new System.IO.MemoryStream(bytes);
                XmlReader reader = XmlReader.Create(ms);
                Microsoft.Data.Edm.Csdl.EdmxReader.TryParse(reader,out edmModel,out errors);
            }
            catch (Exception ex)
            {
                throw new Exception(@"Invalid CSDL", ex);
            }
        }

        #endregion

        #region Public Properties

        public IEdmModel EdmModel
        {
            get
            {
                return edmModel;
            }
        }

        public string MajorVersion
        {
            get
            {
                return edmModel.GetEdmVersion().Major.ToString();
            }
        }

        public string MinorVersion
        {
            get
            {
                return edmModel.GetEdmVersion().Minor.ToString();
            }
        }

        public IEnumerable<IEdmEntityContainer> EntityContainers
        {
            get
            {
                return edmModel.EntityContainers();
            }
        }

        public IEnumerable<IEdmSchemaElement> EntityTypes
        {
            get
            {
                return edmModel.SchemaElements;
            }
        }

        public IEnumerable<IEdmSchemaElement> SchemaElements
        {
            get
            {
                return edmModel.SchemaElements;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get all function (web methods, actions, bindable, non-bindable) of a service
        /// </summary>
        /// <param name="container">EDM Model container</param>
        /// <returns>List of all functions</returns>
        public IEnumerable<IEdmFunctionImport> GetFunctionImports(IEdmEntityContainer container)
        {
            try
            {
                return container.FunctionImports();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get a function import
        /// </summary>
        /// <param name="container">EDM Model container</param>
        /// <param name="functionName">function name</param>
        /// <returns>list of Function Import with specific given function name</returns>
        public List<IEdmFunctionImport> GetFunctionImport(IEdmEntityContainer container, string functionName)
        {
            List<IEdmFunctionImport> reqFunctions = new List<IEdmFunctionImport>();
            try
            {
                IEnumerable<IEdmFunctionImport> functions = GetFunctionImports(container);
                foreach (IEdmFunctionImport func in functions)
                {
                    if (func.Name == functionName)
                    {
                        reqFunctions.Add(func);
                        break;
                    }
                }
                return reqFunctions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get return type of the function
        /// </summary>
        /// <param name="function">Function Import</param>
        /// <param name="kind">kind of the function return(Entity, entity reference, collection, primitive kind etc.)</param>
        /// <param name="structuralProperties">list of structural properties(columns of a Database table) of the function return</param>
        /// <returns>return type of the function</returns>
        public string GetFunctionReturnType(IEdmFunctionImport function, out Microsoft.Data.Edm.EdmTypeKind kind, out IEnumerable<IEdmStructuralProperty> structuralProperties)
        {
            structuralProperties = null;
            try
            {
                if (function.ReturnType != null)
                {
                    kind = function.ReturnType.TypeKind();
                    if (kind == EdmTypeKind.Collection || kind == EdmTypeKind.Entity || kind == EdmTypeKind.EntityReference)
                    {
                        IEdmEntitySet entitySet = null;
                        function.TryGetStaticEntitySet(out entitySet);
                        IEdmEntityType typ = entitySet.ElementType;
                        structuralProperties = GetStructuralProperties(typ);
                        return typ.Name;
                    }
                    else
                    {
                        return function.ReturnType.Definition.ToString().Split('.')[1];
                    }
                }
                else
                {
                    kind = EdmTypeKind.None;
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return string.Empty;
        }

        /// <summary>
        /// Get Http Method type of the function import
        /// </summary>
        /// <param name="function">Function Import</param>
        /// <param name="functionType">out parameter - Function Type(webget, webinvoke, action)</param>
        /// <returns>Http Method type(Get, post, put , patch, merge, delete)</returns>
        public string GetHttpMethodType(IEdmFunctionImport function, out FunctionType functionType)
        {
            string methodType = string.Empty;
            functionType = FunctionType.ACTION;
            try
            {
                IEnumerable<Microsoft.Data.Edm.Annotations.IEdmDirectValueAnnotation> annotations = edmModel.DirectValueAnnotations(function);
                foreach (Microsoft.Data.Edm.Annotations.IEdmDirectValueAnnotation objAnnotaion in annotations)
                {
                    Microsoft.Data.Edm.Library.Values.EdmStringConstant val = (Microsoft.Data.Edm.Library.Values.EdmStringConstant)objAnnotaion.Value;
                    methodType = val.Value;
                    if (val.Value == "GET")
                        functionType = FunctionType.WEBGET;
                    else
                        functionType = FunctionType.WEBINVOKE;
                    break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (string.IsNullOrEmpty(methodType))
                methodType = "POST";
            return methodType;
        }

        /// <summary>
        /// Get Entity Sets
        /// </summary>
        /// <param name="container">EDM Model container</param>
        /// <returns>List of Entity Sets</returns>
        public IEnumerable<IEdmEntitySet> GetEntitySets(IEdmEntityContainer container)
        {
            try
            {
                return container.EntitySets();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Entity Type
        /// </summary>
        /// <param name="container">EDM Model container</param>
        /// <param name="entitySet">Entity Set</param>
        /// <returns>Entity Type</returns>
        public IEdmEntityType GetEntityType(IEdmEntityContainer container, IEdmEntitySet entitySet)
        {
            try
            {
                if (entitySet.Container == container)
                    return entitySet.ElementType;
                else
                    throw new Exception(@"IEdmEntityContainer is different from the given entityset container!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Primary Key of an Entity Type(Database Table)
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        /// <returns>Primary Key of an Entity Type</returns>
        public IEnumerable<IEdmStructuralProperty> GetEntityTypeKey(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredKey;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Navigation Properties(Relational Properties) Mapping
        /// </summary>
        /// <param name="container">EDM Model container</param>
        /// <param name="entitySet">Entity Set</param>
        /// <returns>List of Navigation Properties</returns>
        public IEnumerable<IEdmNavigationTargetMapping> GetNavigationPropertyMapping(IEdmEntityContainer container, IEdmEntitySet entitySet)
        {
            try
            {
                if (entitySet.Container == container)
                    return entitySet.NavigationTargets;
                else
                    throw new Exception(@"IEdmEntityContainer is different from the given entityset container!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Structural Properties(columns of a Database table)
        /// </summary>
        /// <param name="entityType">Entity Type(Database Table)</param>
        /// <returns>List of Structural Properties</returns>
        public IEnumerable<IEdmStructuralProperty> GetStructuralProperties(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredStructuralProperties();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get All Properties(Structural and Navigation Properties)
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        /// <returns>List of All Properties</returns>
        public IEnumerable<IEdmProperty> GetAllProperties(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredProperties;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Navigation Properties
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        /// <returns>List of Navigation Properties</returns>
        public IEnumerable<IEdmNavigationProperty> GetNavigationProperties(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredNavigationProperties();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Property Details
        /// </summary>
        /// <param name="propertyName">property name</param>
        /// <param name="entityType">Entity Type</param>
        /// <returns>IEdmProperty object</returns>
        public IEdmProperty GetProperty(string propertyName, IEdmEntityType entityType)
        {
            try
            {
                return entityType.FindProperty(propertyName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Property Type(Structural or Navigation Property)
        /// </summary>
        /// <param name="property">IEdmProperty object</param>
        /// <param name="kind">kind of propery</param>
        /// <returns>Property Type</returns>
        public string GetPropertyType(IEdmProperty property, out Microsoft.Data.Edm.EdmTypeKind kind)
        {
            try
            {
                kind = property.Type.TypeKind();
                return property.Type.FullName();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Target Entity Set of the Navigation Property
        /// </summary>
        /// <param name="container">EDM Model container</param>
        /// <param name="entitySet">Entity Set</param>
        /// <param name="navigationProperty">IEdmNavigationProperty object</param>
        /// <returns>Target IEdmEntitySet object</returns>
        public IEdmEntitySet GetNavigationPropertyTarget(IEdmEntityContainer container, IEdmEntitySet entitySet, IEdmNavigationProperty navigationProperty)
        {
            try
            {
                if (entitySet.Container == container)
                    return entitySet.FindNavigationTarget(navigationProperty);
                else
                    throw new Exception(@"IEdmEntityContainer is different from the given entityset container!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}