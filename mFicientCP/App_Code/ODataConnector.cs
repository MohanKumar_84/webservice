﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class ODataConnector
    {
        #region Private Members

        private string enterpriseId, connectorId, connectionName, endPoint, userId, password, mPluginAgentName, dataServiceVersion;
        private string httpAuthenticationType;

        #endregion

        #region Constructor

        internal ODataConnector(string _connectorId, string _enterpriseId)
        {
            connectorId = _connectorId;
            enterpriseId = _enterpriseId;
        }

        #endregion

        #region Public Properties

        public string EnterpriseId
        {
           
            get
            {
                return enterpriseId;
            }
            set
            {
                enterpriseId = value;
            }
        }

        public string ConnectorId
        {
            
            get
            {
                return connectorId;
            }
            set
            {
                connectorId = value;
            }
        }

        public string ConnectionName
        {
            
            get
            {
                return connectionName;
            }
            set
            {
                connectionName = value;
            }
        }

        public string ServiceEndPoint
        {
            get
            {
                return endPoint;
            }
            set
            {
                endPoint = value;
            }


        }

        public string UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
            }
            
        }

        public string Password
        {
           
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public string MpluginAgentName
        {
            get
            {
                return mPluginAgentName;
            }
            set
            {
                mPluginAgentName = value;
            }
           
        }

        public string DataServiceVersion
        {
           
            get
            {
                return dataServiceVersion;
            }
            set
            {
                dataServiceVersion = value;
            }
        }

        public string HttpAuthenticationType
        {
             get
            {
                return httpAuthenticationType;
            }
            set
            {
                httpAuthenticationType = value;
            }


        }

        #endregion

        public void GetConnector()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_ODATA_CONNECTION as odatacon left join TBL_MPLUGIN_AGENT_DETAIL as mplugin on odatacon.COMPANY_ID = mplugin.COMPANY_ID
                                    AND mplugin.MP_AGENT_ID= odatacon.MPLUGIN_AGENT WHERE odatacon.COMPANY_ID = @COMPANY_ID AND odatacon.ODATA_CONNECTOR_ID = @ODATA_CONNECTOR_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                cmd.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", connectorId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    connectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                    endPoint = ds.Tables[0].Rows[0]["ODATA_ENDPOINT"].ToString();

                    userId = ds.Tables[0].Rows[0]["USER_NAME"].ToString();
                    if (!String.IsNullOrEmpty(userId))
                        userId = AesEncryption.AESDecrypt(this.enterpriseId.ToUpper(), userId);

                    mPluginAgentName = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();

                    password = ds.Tables[0].Rows[0]["PASSWORD"].ToString();
                    if (!String.IsNullOrEmpty(password))
                        password = AesEncryption.AESDecrypt(this.enterpriseId.ToUpper(), password);

                    dataServiceVersion = ds.Tables[0].Rows[0]["VERSION"].ToString();
                    httpAuthenticationType = Convert.ToString(ds.Tables[0].Rows[0]["AUTHENTICATION_TYPE"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}