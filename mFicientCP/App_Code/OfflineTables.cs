﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
namespace mFicientCP
{
    public class OfflineTables
    {
        string _subAdminId = "";
        public OfflineTables()
        {

        }
        public OfflineTables(string companyId, string subAdminId)
        {
            this.CompanyId = companyId;
            this.SubAdminId = subAdminId;
        }   
        
        public void InsertRecordInOfflineTables(MFEOfflineDataTable mfeOfflineTbl)
        {
            try
            {
                if (mfeOfflineTbl == null) throw new ArgumentNullException();

                bool isExist = doesTableNameExistsForCompany(this.CompanyId, mfeOfflineTbl.TableName);


                if (!isExist)
                {
                    this.TABLE_ID = Utilities.GetMd5Hash(this.CompanyId + mfeOfflineTbl.TableName + DateTime.UtcNow.Ticks.ToString());
                    JObject jobj = JObject.Parse(mfeOfflineTbl.TableJson);
                    JObject jobjTbl = (JObject)jobj["tbl"];
                    jobjTbl["id"] = this.TABLE_ID;
                    mfeOfflineTbl.TableJson = jobj.ToString();
                    string query = @"INSERT INTO [TBL_OFFLINE_DATA_TABLE]([TABLE_ID],[COMPANY_ID],[TABLE_NAME],[TABLE_TYPE],[TABLE_SYNC_ON]
                                    ,[TABLE_JSON],[SYNC_JSON],[UPDATE_DATETIME],[SUB_ADMIN_ID])
                            VALUES(
                                @TABLE_ID,@COMPANY_ID,@TABLE_NAME,@TABLE_TYPE,@TABLE_SYNC_ON,
                                @TABLE_JSON,@SYNC_JSON,@UPDATE_DATETIME,@SUB_ADMIN_ID
                            );";

                    SqlCommand objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@TABLE_ID", this.TABLE_ID);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", mfeOfflineTbl.TableName);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_TYPE", mfeOfflineTbl.TableType);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_SYNC_ON", 0);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_JSON", mfeOfflineTbl.TableJson);
                    objSqlCommand.Parameters.AddWithValue("@SYNC_JSON", mfeOfflineTbl.SyncJson);
                    objSqlCommand.Parameters.AddWithValue("@UPDATE_DATETIME", DateTime.UtcNow.Ticks / 10000000);
                    objSqlCommand.Parameters.AddWithValue("@SUB_ADMIN_ID", this.SubAdminId);
                    
                    MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new MficientException("Table name already exists");
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }

        }
        string Get_tbl_json(string _TABLE_NAME, int _TABLE_TYPE, int _TABLE_SYNC_ON,
            bool _CONFIRMATION_FOR_SYNC, List<Off_columns> _lstCol, List<ListParameters> _lstdlpara,
            List<ListParameters> _lstuppara, int _SequenceType, string noOfRecordsInBatch)
        {
            OfflineDataTableJson objOfflineDataTableJson = new OfflineDataTableJson();
            OfflineTable objOfflineTable = new OfflineTable();
            objOfflineTable.col = _lstCol;
            //objOfflineTable.down = _lstdlpara;
            //objOfflineTable.up = _lstuppara;
            objOfflineTable.id = TABLE_ID;
            objOfflineTable.nm = _TABLE_NAME;
            objOfflineTable.prmt = _CONFIRMATION_FOR_SYNC.ToString();
            objOfflineTable.seq = _SequenceType.ToString();
            //objOfflineTable.sync = _TABLE_SYNC_ON.ToString();
            objOfflineTable.typ = _TABLE_TYPE.ToString();
            objOfflineTable.nor = noOfRecordsInBatch;
            objOfflineDataTableJson.tbl = objOfflineTable;
            return Utilities.SerializeJson<OfflineDataTableJson>(objOfflineDataTableJson);
        }

        bool doesTableNameExistsForCompany(string _COMPANY_ID, string tableName)
        {
            bool blnIsExists = true;
            try
            {
                DataSet objDataSet;
                SqlCommand objSqlCommand;

                string query = @"SELECT [TABLE_NAME]
                                FROM TBL_OFFLINE_DATA_TABLE
                                WHERE COMPANY_ID = @COMPANY_ID
                                AND TABLE_NAME = @TABLE_NAME";

                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _COMPANY_ID);
                objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", tableName);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        blnIsExists = true;
                    }
                    else
                    {
                        blnIsExists = false;
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnIsExists;
        }
        bool doesTableNameExistsForCompany(string _COMPANY_ID, string tableName, string tableIdToIgnore)
        {
            bool blnIsExists = true;
            try
            {
                DataSet objDataSet;
                SqlCommand objSqlCommand;

                string query = @"SELECT [TABLE_NAME]
                                FROM TBL_OFFLINE_DATA_TABLE
                                WHERE COMPANY_ID = @COMPANY_ID
                                AND TABLE_NAME = @TABLE_NAME
                                AND TABLE_ID != @TABLE_ID";

                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _COMPANY_ID);
                objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", tableName);
                objSqlCommand.Parameters.AddWithValue("@TABLE_ID", tableIdToIgnore);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        blnIsExists = true;
                    }
                    else
                    {
                        blnIsExists = false;
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnIsExists;
        }
        public void UpdateRecordInOfflineTables(MFEOfflineDataTable offlineDt)
        {
            try
            {
                bool isExist = doesTableNameExistsForCompany(this.CompanyId, offlineDt.TableName, offlineDt.TableId);

                if (!isExist)
                {
                    string query = @"UPDATE [TBL_OFFLINE_DATA_TABLE] SET [TABLE_TYPE] = @TABLE_TYPE,[TABLE_SYNC_ON] = @TABLE_SYNC_ON,
                             [TABLE_JSON] = @TABLE_JSON,[SYNC_JSON]=@SYNC_JSON,[UPDATE_DATETIME] = @UPDATE_DATETIME
                             ,[SUB_ADMIN_ID] = @SUB_ADMIN_ID,[TABLE_NAME]=@TABLE_NAME WHERE [TABLE_ID] = @TABLE_ID AND [COMPANY_ID] = @COMPANY_ID";

                    SqlCommand objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@TABLE_ID", offlineDt.TableId);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_TYPE", offlineDt.TableType);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", offlineDt.TableName);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_SYNC_ON", 0);
                    objSqlCommand.Parameters.AddWithValue("@TABLE_JSON", offlineDt.TableJson);
                    objSqlCommand.Parameters.AddWithValue("@SYNC_JSON", offlineDt.SyncJson);
                    objSqlCommand.Parameters.AddWithValue("@UPDATE_DATETIME", DateTime.UtcNow.Ticks / 10000000);
                    objSqlCommand.Parameters.AddWithValue("@SUB_ADMIN_ID", this.SubAdminId);
                    MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new MficientException("Table name already exists");
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }

        public void DeleteOfflineTables(string _TABLE_ID, string _COMPANY_ID)
        {
            try
            {
                string query = @"Delete from [TBL_OFFLINE_DATA_TABLE] where TABLE_ID=@TABLE_ID and COMPANY_ID=@COMPANY_ID ";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@TABLE_ID", _TABLE_ID);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _COMPANY_ID);
                MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
       
        public string TABLE_ID
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        public string SubAdminId
        {
            private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
    }
}