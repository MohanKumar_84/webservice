﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class PlanRenewOrChangeReqByReqType
    {
        public PlanRenewOrChangeReqByReqType(string companyId, string requestType, string planCode, string noOfUsers, string months, string currency)
        {
            this.CompanyId = companyId;
            RequestType = requestType;
            PlanCode = planCode;
            NoOfUsers = noOfUsers;
            Months = months;
            Currency = currency;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                if (!anyRequestAlreadyExists())
                {
                    //status added on 18/9/2012//currency added on 21/9/2012
                    string query = @"INSERT INTO [ADMIN_TBL_PLAN_ORDER_REQUEST]
                                       ([COMPANY_ID]
                                       ,[REQUEST_TYPE]
                                       ,[PLAN_CODE]
                                       ,[MOBILE_USERS]
                                       ,[NUMBER_MONTH]
                                       ,[REQUEST_DATETIME]
                                        ,[STATUS]
                                        ,[CURRENCY_TYPE]
                                        ,[REMARKS])
                                 VALUES
                                       (@COMPANY_ID
                                       ,@REQUEST_TYPE
                                       ,@PLAN_CODE
                                       ,@MOBILE_USERS
                                       ,@NUMBER_MONTH
                                       ,@REQUEST_DATETIME
                                        ,@STATUS
                                        ,@CURRENCY_TYPE
                                        ,@Remarks);";

                    SqlCommand objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@REQUEST_TYPE", this.RequestType);
                    objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                    objSqlCommand.Parameters.AddWithValue("@MOBILE_USERS", Convert.ToInt32(this.NoOfUsers));
                    objSqlCommand.Parameters.AddWithValue("@NUMBER_MONTH", double.Parse(this.Months));
                    objSqlCommand.Parameters.AddWithValue("@REQUEST_DATETIME", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@STATUS", Convert.ToInt16(0));
                    objSqlCommand.Parameters.AddWithValue("@CURRENCY_TYPE", Currency);
                    objSqlCommand.Parameters.AddWithValue("@Remarks", "");
                    if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    {
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                    else
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                    StatusDescription = "A request of this type is already pending";
                }
                else
                {
                    StatusCode = -1000;
                    StatusDescription = "A request of this type is already pending";
                }
            }
            catch (Exception ex)
            {
                StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusDescription = "Database Connection Error";
                }
                this.StatusDescription = "Record insert error";
            }

        }
        bool anyRequestAlreadyExists()
        {
            try
            {
                //string query = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REQUEST
                //WHERE COMPANY_ID = @CompanyId
                //AND PLAN_CODE = @PlanCode
                //AND REQUEST_TYPE = @RequestType
                //AND STATUS = 0;";
                string query = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REQUEST
                WHERE COMPANY_ID = @CompanyId
                AND STATUS = 0;";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CompanyID", this.CompanyId);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@RequestType", this.RequestType);

                DataSet dsExistingRequest = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsExistingRequest.Tables.Count > 0)
                {
                    if (dsExistingRequest.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new Exception("No record found");
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public string CompanyId
        {
            set;
            get;
        }

        public string RequestType
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }
        public string NoOfUsers
        {
            set;
            get;
        }
        public string Months
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string Currency
        {
            set;
            get;
        }
    }
}