﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;

namespace mFicientCP
{
    public class PostgreSqlClient
    {
        public static bool TestConnection(string connectionString)
        {
            bool isConnect = false;
            NpgsqlConnection Conn;
            try
            {
                Conn = new NpgsqlConnection(connectionString);
            }
            catch
            {
                throw new Exception(@"Invalid Connection");
            }
            try
            {
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
                isConnect = true;
            }
            catch (NpgsqlException ex)
            {
                Conn = null;
            }
            catch {
                Conn = null;
            }
            return isConnect;
        }

        public static void OpenConnection(out NpgsqlConnection Conn, string connectionString)
        {
            try
            {
                Conn = new NpgsqlConnection(connectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        public static void CloseConnection(NpgsqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open)) Conn.Close();
            }
            catch
            {
            }
        }

        public static DataSet SelectDataFromCommand(NpgsqlCommand _NpgsqlCommand, string ConString)
        {
            NpgsqlConnection Conn = null;
            try
            {
                OpenConnection(out Conn, ConString);
            }
            catch 
            {
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }

            DataSet ds = null;
            try
            {
                _NpgsqlCommand.Connection = Conn;
                ds = new DataSet();

                NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(_NpgsqlCommand);

                objSqlDataAdapter.Fill(ds);
            }
            catch (Exception ex)
            {
                ds = null;
                throw ex;
            }
            finally
            {
                CloseConnection(Conn);
            }
            return ds;
        }

        public static DataSet SelectDataUsingTransaction(NpgsqlCommand _NpgsqlCommand, NpgsqlConnection connection, NpgsqlTransaction transaction)
        {
            _NpgsqlCommand.Connection = connection;
            _NpgsqlCommand.Transaction = transaction;
            DataSet ds = new DataSet();

            NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(_NpgsqlCommand);

            objSqlDataAdapter.Fill(ds);

            return ds;

        }

        public static int ExecuteNonQueryRecord(NpgsqlCommand cmd, string ConString)
        {
            NpgsqlConnection Conn = null;
            try
            {
                OpenConnection(out Conn, ConString);
                cmd.Connection = Conn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection(Conn);
            }
        }

        public static int ExecuteNonQueryUsingTransaction(NpgsqlCommand _NpgsqlCommand, NpgsqlConnection connection, NpgsqlTransaction transaction)
        {
            _NpgsqlCommand.Connection = connection;
            _NpgsqlCommand.Transaction = transaction;
            try
            {
                return _NpgsqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet getMetaDataByType(MetadataType metadataType,
           string connectionString,
           string databaseName,
           string tableName,
          out string errorMsg)
        {
            errorMsg = String.Empty;
            DataSet dsMetaData = new DataSet();
            string strQuery = string.Empty;
            string strTableNameWithoutSchema = string.Empty;
            string strSchema = String.Empty;
            DataTable objdt = null;
            NpgsqlCommand cmd;
            try
            {
                switch (metadataType)
                {
                    case MetadataType.TABLES:
                        try
                        {
                            DataTable dt = getTableMetadata(connectionString, MetadataType.TABLES);
                            dsMetaData.Tables.Add(dt.Copy());
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        break;

                    case MetadataType.COLUMNS:

                        DataTable dtColumnMetadata = new DataTable("MetaData");
                        dtColumnMetadata.Columns.Add("table_name");
                        dtColumnMetadata.Columns.Add("column_name");
                        dtColumnMetadata.Columns.Add("column_type");
                        dtColumnMetadata.Columns.Add("is_nullable");
                        dsMetaData.Tables.Add(dtColumnMetadata);

                        
                        strTableNameWithoutSchema = tableName.Split('.')[1].Replace("\"", "");
                        strSchema = tableName.Split('.')[0].Replace("\"", "");
                        strQuery = @"SELECT table_name, column_name, data_type as column_type, is_nullable 
                                        FROM information_schema.columns 
                                        where table_name = @TableName and table_schema = @ShemaName;";

                        cmd = new NpgsqlCommand(strQuery);
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@TableName", NpgsqlTypes.NpgsqlDbType.Varchar);
                        cmd.Parameters.Add("@ShemaName", NpgsqlTypes.NpgsqlDbType.Varchar);

                        // DataTable objdt = ExecuteSelectQuery(strQuery, connectionString, null).Tables[0];
                        objdt = SelectDataFromCommand(cmd, connectionString).Tables[0];
                        if (objdt != null && objdt.Rows.Count > 0)
                        {
                            foreach (DataRow row in objdt.Rows)
                            {
                                if (tableName.Split('.')[0].Replace("\"", "").Trim() != "public")
                                    dtColumnMetadata.Rows.Add("\"" + tableName.Split('.')[0].Replace("\"", "") + "\".\"" + row["table_name"] + "\"", row["column_name"], row["column_type"], row["is_nullable"]);
                                else
                                    dtColumnMetadata.Rows.Add(tableName.Split('.')[0].Replace("\"", "") + ".\"" + row["table_name"] + "\"", row["column_name"], row["column_type"], row["is_nullable"]);
                            }
                        }
                       
                        break;

                    case MetadataType.ALL:
                        try
                        {
                            DataTable dtTablesMetadata = getTableMetadata(connectionString, MetadataType.ALL);
                            dsMetaData.Tables.Add(dtTablesMetadata.Copy());


                            List<string> tableSchemas = getSchemas(connectionString);

                            DataTable dtMetadata = new DataTable("ColumnMetaData");
                            dtMetadata.Columns.Add("table_schema");
                            dtMetadata.Columns.Add("table_name");
                            dtMetadata.Columns.Add("column_name");
                            dtMetadata.Columns.Add("column_type");
                            dtMetadata.Columns.Add("is_nullable");
                            dsMetaData.Tables.Add(dtMetadata);

                            foreach (string schema in tableSchemas)
                            {
                                objdt = new DataTable();
                                strQuery = @" SELECT table_name, column_name, data_type as column_type, is_nullable 
                                        FROM information_schema.columns where table_schema =@Schema;";
                                cmd = new NpgsqlCommand(strQuery);
                                cmd.CommandType = CommandType.Text;
                                cmd.Parameters.Add("@Schema", NpgsqlTypes.NpgsqlDbType.Varchar);
                                objdt = SelectDataFromCommand(cmd, connectionString).Tables[0];
                                if (objdt != null && objdt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in objdt.Rows)
                                    {
                                        if (schema.Trim() != "public")
                                            dtMetadata.Rows.Add(schema, row["table_name"], row["column_name"], row["column_type"], row["is_nullable"]);
                                        else
                                            dtMetadata.Rows.Add(schema, row["table_name"], row["column_name"], row["column_type"], row["is_nullable"]);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        break;
                }
                return dsMetaData;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    errorMsg = "There was some error in connection.";
                }
                else
                {
                    errorMsg = "Internal server error.";
                }
                return dsMetaData = null;
            }
        }


        private static DataTable getTableMetadata(string connectionString, MetadataType metaDatatype)
        {
            DataTable dtblTableSchema = new DataTable();
            DataTable objTable = new DataTable("MetaData");
            string strQuery;
            try
            {
                List<string> schemas = getSchemas(connectionString);

                objTable.Columns.Add("TABLE_SCHEMA");
                objTable.Columns.Add("TABLE_NAME");

                foreach (string schema in schemas)
                {
                    dtblTableSchema = new DataTable();
                    strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = '" + schema + "';";
                    NpgsqlCommand cmd = new NpgsqlCommand(
                                    strQuery
                                    );
                    cmd.CommandType = CommandType.Text;
                    DataSet dsTableSchema = SelectDataFromCommand(cmd, connectionString);
                    dtblTableSchema = dsTableSchema.Tables[0];
                    if (dtblTableSchema != null && dtblTableSchema.Rows.Count > 0)
                    {
                        switch (metaDatatype)
                        {
                            case MetadataType.TABLES:
                                foreach (DataRow row in dtblTableSchema.Rows)
                                {
                                    if (row["TABLE_SCHEMA"].ToString() != "public")
                                        objTable.Rows.Add(row["TABLE_SCHEMA"], "\"" + row["TABLE_SCHEMA"] + "\".\"" + row["TABLE_NAME"] + "\"");
                                    else
                                        objTable.Rows.Add(row["TABLE_SCHEMA"], row["TABLE_SCHEMA"] + ".\"" + row["TABLE_NAME"] + "\"");
                                }
                                break;

                            case MetadataType.ALL:
                                foreach (DataRow row in dtblTableSchema.Rows)
                                {
                                    objTable.Rows.Add(row["TABLE_SCHEMA"], row["TABLE_NAME"]);
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTable;
        }

        private static List<string> getSchemas(string connectionString)
        {
            List<string> schemas = new List<string>();
            string strSchemas = string.Empty;
            NpgsqlConnection con = new NpgsqlConnection();
            try
            {
                NpgsqlCommand cmd = new NpgsqlCommand(
                                    @"select schema_name from information_schema.schemata where schema_name <> 'information_schema' and schema_name !~ E'^pg_';"
                                    );
                cmd.CommandType = CommandType.Text;
                DataSet dsSchema = SelectDataFromCommand(cmd, connectionString);
                DataTable dtblSchema;
                if (dsSchema == null) throw new Exception("Internal server error.");
                dtblSchema = dsSchema.Tables[0];
                if (dtblSchema.Rows.Count > 0)
                {
                    foreach (DataRow row in dtblSchema.Rows)
                    {
                        schemas.Add(row["schema_name"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return schemas;
        }
        public static string getMetaDataQueryForTable()
        {
            return  @"SELECT table_name, column_name, data_type as column_type, is_nullable 
                                        FROM information_schema.columns 
                                        where table_name = @TableName and table_schema = @ShemaName;";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <param name="timeout">For default timeout pass 0 or value less than 0</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static string getConnectionString(string hostName,
            string databaseName,
            string userId,
            string password,
            uint timeout)
        {
            if (String.IsNullOrEmpty(hostName) ||
               String.IsNullOrEmpty(databaseName) ||
                String.IsNullOrEmpty(userId) ||
                String.IsNullOrEmpty(password)) throw new ArgumentNullException();

            NpgsqlConnectionStringBuilder pgsqlConnectionString = new NpgsqlConnectionStringBuilder();
            pgsqlConnectionString.Host = hostName;
            pgsqlConnectionString.Database = databaseName;
            pgsqlConnectionString.UserName = userId;
            pgsqlConnectionString.Password = password;
            if (Convert.ToInt32(timeout) > 0)
                pgsqlConnectionString.CommandTimeout = Convert.ToInt32(timeout);

            return pgsqlConnectionString.ConnectionString;

        }
    }
}