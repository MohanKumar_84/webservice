﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class QueriesCP
    {
        public enum GET_RESOLVED_QUERY_TYPE
        {
            All,
            SubAdmin,
            UserId,
        }

        public DataTable GetMobileQueries(string SUBADMIN_ID, string COMPANY_ID)
        {
            SqlCommand objCmd = new SqlCommand(@"select u.First_NAME +' '+u.Last_name as NAME,q.USER_ID, q.TOKEN_NO,q.QUERY_SUBJECT,q.POSTED_ON,qt.QUERY_TYPE from ADMIN_TBL_QUERY q 
                                                inner join TBL_user_detail u on q.user_id=u.user_id and q.COMPANY_ID=u.COMPANY_ID
                                                left outer join TBL_SUB_ADMIN  s on u.SUBADMIN_ID=s.SUBADMIN_ID and s.COMPANY_ID=u.COMPANY_ID
                                                inner join ADMIN_TBL_QUERY_type  qt on qt.QUERY_CODE=q.QUERY_TYPE
                                                where q.RESOLVED_BY_CREATOR=0 and s.COMPANY_ID=@COMPANY_ID
                                                ORDER BY q.POSTED_ON DESC");

            objCmd.Parameters.AddWithValue("@SUBADMIN_ID", SUBADMIN_ID);
            objCmd.Parameters.AddWithValue("@COMPANY_ID", COMPANY_ID);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }

        public DataTable GetMyQueries(string subAdminId)
        {
            SqlCommand objCmd = new SqlCommand(@"SELECT Query.*,SubAdmin.FULL_NAME AS NAME FROM ADMIN_TBL_QUERY AS Query
                                                INNER JOIN TBL_SUB_ADMIN AS SubAdmin
                                                ON Query.USER_ID = SubAdmin.SUBADMIN_ID
                                                where Query.RESOLVED_BY_CREATOR =0 and Query.USER_ID=@userid
                                                ORDER BY Query.POSTED_ON DESC");
            objCmd.Parameters.AddWithValue("@userid", subAdminId);

            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectAll"></param>
        /// <param name="userId">Pass user id if not selecting all</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error.Datatable returned is null</exception>
        public DataTable getResolvedQueries(GET_RESOLVED_QUERY_TYPE queryType
                                            , bool queryWithTokenNo
                                           , string userId, string subAdminId,
                                            string tokenNo)
        {
            string strQueryForSubAdmin = @"SELECT Query.*,SubAdmin.FULL_NAME AS NAME
                                            FROM ADMIN_TBL_QUERY AS Query
                                            INNER JOIN TBL_SUB_ADMIN AS SubAdmin
                                            ON Query.USER_ID = SubAdmin.SUBADMIN_ID
                                            WHERE Query.RESOLVED_BY_CREATOR =1
                                            ";
            string strQueryForUser = @"SELECT Query.*,(UsrDtl.FIRST_NAME+' '+UsrDtl.LAST_NAME) AS NAME
                                        FROM ADMIN_TBL_QUERY AS Query
                                        INNER JOIN TBL_USER_DETAIL AS UsrDtl
                                        ON Query.USER_ID = UsrDtl.USER_ID
                                        WHERE Query.RESOLVED_BY_CREATOR =1 ";
            if (queryWithTokenNo)
            {
                strQueryForUser += " " + "AND Query.TOKEN_NO LIKE '%'+@TokenNo+'%'";
                strQueryForSubAdmin += " " + "AND Query.TOKEN_NO LIKE '%'+@TokenNo+'%'";
            }
            string strQueryForCmd = String.Empty;
            switch (queryType)
            {
                case GET_RESOLVED_QUERY_TYPE.All:
                    strQueryForCmd = @"SELECT * FROM (" + strQueryForSubAdmin + " " +
                        " UNION " + " " + strQueryForUser +
                        " ) AS RESULT_TABLE ORDER BY RESULT_TABLE.POSTED_ON DESC" + ";";
                    break;
                case GET_RESOLVED_QUERY_TYPE.SubAdmin:
                    strQueryForUser += " " + "AND Query.USER_ID=@userid ORDER BY Query.POSTED_ON DESC";
                    strQueryForCmd = strQueryForSubAdmin + ";";
                    break;
                case GET_RESOLVED_QUERY_TYPE.UserId:
                    strQueryForUser += " " + "AND Query.USER_ID=@userid ORDER BY Query.POSTED_ON DESC";
                    strQueryForCmd = strQueryForUser + ";";
                    break;
            }
            SqlCommand objCmd = new SqlCommand(strQueryForCmd);
            if (queryType != GET_RESOLVED_QUERY_TYPE.All)
            {
                objCmd.Parameters.AddWithValue("@userid", userId);
            }
            objCmd.Parameters.AddWithValue("@SubAdminID", subAdminId);
            if (queryWithTokenNo)
                objCmd.Parameters.AddWithValue("@TokenNo", tokenNo);

            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objCmd);
            if (ds == null)
            {
                throw new Exception("Record not found");
            }
            return ds.Tables[0];
        }

        public DataTable GetClosedQueries(string UserId)
        {
            SqlCommand objCmd = new SqlCommand(@"select l.ADMIN_NAME as NAME, q.USER_ID,q. TOKEN_NO,q.QUERY_SUBJECT,q.POSTED_ON
                                                  from ADMIN_TBL_QUERY q inner join ADMIN_TBL_ADMIN_LOGIN l
                                                   on q.USER_ID=l.ADMIN_ID
                                                   where QUERY_TYPE=@querytype and USER_ID=@userid and RESOLVED_BY_CREATOR=@resolved
                                                    ORDER BY q.POSTED_ON DESC");
            objCmd.Parameters.AddWithValue("@userid", UserId);
            objCmd.Parameters.AddWithValue("@querytype", 0);
            objCmd.Parameters.AddWithValue("@resolved", 1);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }
        /// <summary>
        /// Use it for finding the full name of sub admin.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public string UserNameCP(string UserId, out string fullName)
        {
            string strUsername = String.Empty;
            fullName = String.Empty;
            SqlCommand ObjSqlCmd = new SqlCommand(@"select USER_NAME,FULL_NAME from TBL_SUB_ADMIN where SUBADMIN_ID=@id");
            ObjSqlCmd.CommandType = CommandType.Text;
            ObjSqlCmd.Parameters.AddWithValue("@id", UserId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(ObjSqlCmd);
            if (objDataSet != null && objDataSet.Tables.Count > 0 && objDataSet.Tables[0].Rows.Count > 0)
            {
                strUsername = Convert.ToString(objDataSet.Tables[0].Rows[0]["USER_NAME"]);
                fullName = Convert.ToString(objDataSet.Tables[0].Rows[0]["FULL_NAME"]);
            }
            return strUsername;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="companyId"></param>
        /// <param name="fullName"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when data is not found</exception>
        public string getUsernameOfUser(string userId, string subAdminId, string companyId, out string fullName)
        {
            fullName = String.Empty;
            string strUserName = String.Empty;
            GetUserDetail objUserDetail = new GetUserDetail(false,
                true, subAdminId, userId, String.Empty, companyId);
            objUserDetail.Process();
            if (objUserDetail.StatusCode == 0)
            {
                if (objUserDetail.ResultTable == null) throw new Exception();
                fullName = Convert.ToString(objUserDetail.ResultTable.Rows[0]["FIRST_NAME"]) + " " +
                    Convert.ToString(objUserDetail.ResultTable.Rows[0]["LAST_NAME"]);
                strUserName = Convert.ToString(objUserDetail.ResultTable.Rows[0]["USER_NAME"]);
            }
            return strUserName;
        }

        public void QueryDetails(string tokenNo)
        {
            SqlCommand ObjSqlCmd = new SqlCommand(@"select QUERY_SUBJECT,USER_ID,QUERY from ADMIN_TBL_QUERY where TOKEN_NO=@TokenNo");
            ObjSqlCmd.CommandType = CommandType.Text;
            ObjSqlCmd.Parameters.AddWithValue("@TokenNo", tokenNo);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(ObjSqlCmd);
            if (objDataSet.Tables[0].Rows.Count > 0)
            {
                ResultTable = objDataSet.Tables[0];
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }

        }

        public string UserName(string UserId)
        {
            SqlCommand ObjSqlCmd = new SqlCommand(@"select USER_NAME from TBL_SUB_ADMIN where SUBADMIN_ID=@subadminid");
            ObjSqlCmd.CommandType = CommandType.Text;
            ObjSqlCmd.Parameters.AddWithValue("@subadminid", UserId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(ObjSqlCmd);
            string name = Convert.ToString(objDataSet.Tables[0].Rows[0]["USER_NAME"]);
            return name;
        }
        /// <summary>
        /// use it for sub admin
        /// </summary>
        /// <param name="tokennumber"></param>
        /// <returns></returns>
        public DataTable GetCommentsList(string tokennumber)
        {
            SqlCommand objCmd = new SqlCommand(@"SELECT SubAdmin.USER_NAME,SubAdmin.FULL_NAME,Thread.COMMENT ,Thread.COMMENTED_ON
                                                from TBL_SUB_ADMIN AS SubAdmin INNER JOIN ADMIN_TBL_QUERY_THREAD Thread 
                                                on SubAdmin.SUBADMIN_ID=Thread.COMMENT_BY
                                                WHERE Thread.TOKEN_NO=@tokenno and Thread.USER_TYPE=2
                                                
                                                UNION
                                                SELECT TblAdmin.USER_NAME,TblAdmin.LOGIN_NAME+'(Support)',Thread.COMMENT ,Thread.COMMENTED_ON
                                                from ADMIN_TBL_LOGIN_USER_DETAIL AS TblAdmin INNER JOIN ADMIN_TBL_QUERY_THREAD Thread 
                                                on TblAdmin.LOGIN_ID=Thread.COMMENT_BY
                                                WHERE Thread.TOKEN_NO=@tokenno and Thread.USER_TYPE=3
                                                ORDER BY Thread.COMMENTED_ON DESC");
            objCmd.Parameters.AddWithValue("@tokenno", tokennumber);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }

        public DataTable getCommentsListForUsers(string tokennumber)
        {
            SqlCommand objCmd = new SqlCommand(@"SELECT UsrDtl.USER_NAME,(UsrDtl.FIRST_NAME+' '+UsrDtl.LAST_NAME)AS FULL_NAME,Thread.COMMENT,
                                                Thread.COMMENTED_ON,Thread.USER_TYPE
                                                FROM TBL_USER_DETAIL AS UsrDtl
                                                INNER JOIN 
                                                ADMIN_TBL_QUERY_THREAD AS Thread
                                                ON UsrDtl.USER_ID=Thread.COMMENT_BY
                                                WHERE Thread.TOKEN_NO=@tokenno and Thread.USER_TYPE=1
                                               
                                                UNION
                                                SELECT SubAdmin.USER_NAME,SubAdmin.FULL_NAME,Thread.COMMENT ,Thread.COMMENTED_ON,
                                                Thread.USER_TYPE
                                                from TBL_SUB_ADMIN AS SubAdmin INNER JOIN ADMIN_TBL_QUERY_THREAD Thread 
                                                on SubAdmin.SUBADMIN_ID=Thread.COMMENT_BY
                                                WHERE Thread.TOKEN_NO=@tokenno and Thread.USER_TYPE=2
                                                
                                                UNION
                                                SELECT TblAdmin.USER_NAME,TblAdmin.LOGIN_NAME+'(Support)',Thread.COMMENT ,Thread.COMMENTED_ON,
                                                Thread.USER_TYPE
                                                from ADMIN_TBL_LOGIN_USER_DETAIL AS TblAdmin INNER JOIN ADMIN_TBL_QUERY_THREAD Thread 
                                                on TblAdmin.LOGIN_ID=Thread.COMMENT_BY
                                                WHERE Thread.TOKEN_NO=@tokenno and Thread.USER_TYPE=3
                                                ORDER BY Thread.COMMENTED_ON DESC
                                                ");
            objCmd.Parameters.AddWithValue("@tokenno", tokennumber);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }


        public void AddStatusChange(string CompanyId, string TokenNo)
        {
            SqlCommand cmd = new SqlCommand("update  ADMIN_TBL_QUERY set RESOLVED_BY_CREATOR='true',RESOLVED_ON=@RESOLVED_ON where TOKEN_NO=@TOKEN_NO AND  COMPANY_ID=@COMPANY_ID");
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@TOKEN_NO", TokenNo);
            cmd.Parameters.AddWithValue("@RESOLVED_ON", System.DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
            MSSqlClient.ExecuteNonQueryRecord(cmd);
        }
        public void AddSubadminQuery(string CompanyId, string subAdminId, int Querytype, string SubjectQuery, string Query)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO ADMIN_TBL_QUERY([TOKEN_NO],[COMPANY_ID],[USER_ID],[QUERY_TYPE],[QUERY_SUBJECT],[QUERY],[POSTED_ON],[IS_RESOLVED],[RESOLVED_BY_CREATOR],[RESOLVED_ON],IS_FORWARD_TO_ADMIN)" +
                "VALUES (@TOKEN_NO,@COMPANY_ID,@USER_ID,@QUERY_TYPE,@QUERY_SUBJECT,@QUERY,@POSTED_ON,@IS_RESOLVED,@RESOLVED_BY_CREATOR,@RESOLVED_ON,'false')");
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@TOKEN_NO", getTokenID(CompanyId, subAdminId));
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", subAdminId);
                cmd.Parameters.AddWithValue("@QUERY_TYPE", Querytype);
                cmd.Parameters.AddWithValue("@POSTED_ON", System.DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@QUERY_SUBJECT", SubjectQuery);
                cmd.Parameters.AddWithValue("@QUERY", Query);
                cmd.Parameters.AddWithValue("@IS_RESOLVED", false);
                cmd.Parameters.AddWithValue("@RESOLVED_BY_CREATOR", false);
                cmd.Parameters.AddWithValue("@RESOLVED_ON", 0);

                if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch
            {
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error.Or ds returned is null</exception>
        public DataTable getQueryTypeForSubAdmin()
        {
            string strSqlCmd = @"SELECT * FROM ADMIN_TBL_QUERY_TYPE
                                 WHERE CATEGORY = 2;";
            SqlCommand cmd = new SqlCommand(strSqlCmd);
            cmd.CommandType = CommandType.Text;
            DataSet dsQueryType = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (dsQueryType != null)
            {
                return dsQueryType.Tables[0];
            }
            else
            {
                throw new Exception("No record found");
            }
        }


        private string getTokenID(string CompanyId, string UserID)
        {
            string strTokenNo = Utilities.GetMd5Hash(CompanyId + UserID + "1" + System.DateTime.UtcNow.Ticks);
            int index = 0;
            Boolean blnIsSessionIdValid = false;
            string strNewTokenId = string.Empty;
            do
            {
                strNewTokenId = strTokenNo.Substring(index, 10);
                blnIsSessionIdValid = isTokenValid(strNewTokenId, CompanyId);
                index = index + 1;
            }
            while (blnIsSessionIdValid == false);

            return strNewTokenId;
        }
        private Boolean isTokenValid(string strTokenNo, string CompanyId)
        {
            Boolean blnIsSessionIdValid = true;

            SqlCommand objSqlCommand = new SqlCommand("select token_no from ADMIN_TBL_QUERY where token_no=@TOKEN_NO AND  COMPANY_ID=@COMPANY_ID");
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@TOKEN_NO", strTokenNo);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet.Tables[0].Rows.Count > 0)
                blnIsSessionIdValid = false;
            return blnIsSessionIdValid;
        }
        public void AddCommentQuery(string TokenNumber, string CommentId,
            string CommentBy, int UserType,
            string Comment,
            string subAdminId,
            string companyId,
            bool isResolved)
        {
            SqlTransaction transaction = null;
            SqlConnection con;
            MSSqlClient.SqlConnectionOpen(out con);
            try
            {
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        long lngCommentedOn = DateTime.UtcNow.Ticks;
                        string strQuery = @"insert into ADMIN_TBL_QUERY_THREAD values(@tokennumber,@commentid,@commentby,@usertype,@comment,@commentedon);";
                        SqlCommand objSqlCmd = new SqlCommand(strQuery,
                            con, transaction);
                        objSqlCmd.Parameters.AddWithValue("@tokennumber", TokenNumber);
                        objSqlCmd.Parameters.AddWithValue("@commentid", CommentId);
                        objSqlCmd.Parameters.AddWithValue("@commentby", CommentBy);
                        objSqlCmd.Parameters.AddWithValue("@usertype", UserType);
                        objSqlCmd.Parameters.AddWithValue("@comment", Comment);
                        objSqlCmd.Parameters.AddWithValue("@commentedon", lngCommentedOn);
                        if (objSqlCmd.ExecuteNonQuery() == 0) throw new Exception();
                        if (isResolved)
                        {
                            strQuery = @"UPDATE ADMIN_TBL_QUERY
                                    SET IS_RESOLVED = 1,
                                    RESOLVED_BY_CREATOR = 1,
                                    RESOLVED_ON = @ResolvedDatetime,
                                    RESOLVED_BY = @SubadminId
                                    WHERE TOKEN_NO = @TokenNo
                                    AND COMPANY_ID = @CompanyId
                                    ";
                            objSqlCmd = new SqlCommand(strQuery,
                                con, transaction);
                            objSqlCmd.Parameters.AddWithValue("@ResolvedDatetime", lngCommentedOn);
                            objSqlCmd.Parameters.AddWithValue("@SubadminId", subAdminId);
                            objSqlCmd.Parameters.AddWithValue("@TokenNo", TokenNumber);
                            objSqlCmd.Parameters.AddWithValue("@CompanyId", companyId);

                            if (objSqlCmd.ExecuteNonQuery() == 0) throw new Exception();
                        }

                        transaction.Commit();
                        //make the response status
                    }
                }

            }
            catch (SqlException e)
            {
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch 
            {
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
            #region Previous Code
            //try
            //{
            //    SqlCommand objSqlCmd = new SqlCommand(@"insert into ADMIN_TBL_QUERY_THREAD values(@tokennumber,@commentid,@commentby,@usertype,@comment,@commentedon)");

            //    objSqlCmd.Parameters.AddWithValue("@tokennumber", TokenNumber);
            //    objSqlCmd.Parameters.AddWithValue("@commentid", CommentId);
            //    objSqlCmd.Parameters.AddWithValue("@commentby", CommentBy);
            //    objSqlCmd.Parameters.AddWithValue("@usertype", UserType);
            //    objSqlCmd.Parameters.AddWithValue("@comment", Comment);
            //    objSqlCmd.Parameters.AddWithValue("@commentedon", DateTime.UtcNow.Ticks);

            //    if (MSSqlClient.ExecuteNonQueryRecord(objSqlCmd) > 0)
            //    {
            //        StatusCode = 0;
            //        StatusDescription = "";
            //    }
            //    else
            //    {
            //        StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            //    }
            //}
            //catch
            //{
            //    StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            //}
            #endregion
        }


        public string GetTokenNo(string commentby)
        {
            SqlCommand ObjSqlCmd = new SqlCommand(@"select TOKEN_NO from ADMIN_TBL_QUERY_THREAD where COMMENT_BY=@commentby");
            ObjSqlCmd.CommandType = CommandType.Text;
            ObjSqlCmd.Parameters.AddWithValue("@commentby", commentby);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(ObjSqlCmd);
            if (objDataSet.Tables[0].Rows.Count > 0)
            {
                string name = Convert.ToString(objDataSet.Tables[0].Rows[0]["TOKEN_NO"]);
                return name;
            }
            else
            {
                return "";
            }
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }


        public DataTable ResultTable
        {
            set;
            get;
        }

    }
}