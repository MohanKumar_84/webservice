﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;
//namespace mFicientCP
//{
//    public class ResetMobileUserPassword
//    {
//        public ResetMobileUserPassword(string userName, string companyId, string userId, string emailId)
//        {
//            this.UserName = userName;
//            this.CompanyId = companyId;
//            this.UserId = userId;
//            this.EmailId = emailId;
//        }

//        public void Process()
//        {
//            StatusCode = -1000;
//            SqlTransaction transaction = null;
//            SqlConnection con = null;
//            try
//            {
//                MSSqlClient.SqlConnectionOpen(out con);
//                using (con)
//                {
//                    using (transaction = con.BeginTransaction())
//                    {
//                        string strPassword;
//                        updateIsExpiredStatus(con, transaction);
//                        saveResetPassword(con, transaction, out strPassword);
//                        saveInfoInEmailOutbox(con, transaction, strPassword);
//                        transaction.Commit();
//                        StatusCode = 0;
//                    }
//                }
//            }
//            catch
//            {
//                //transaction.Rollback();
//                this.StatusCode = -1000;
//                this.StatusDescription = "Internal server error";
//            }
//            finally
//            {
//                if (con != null)
//                {
//                    con.Dispose();
//                }
//                if (transaction != null)
//                {
//                    transaction.Dispose();
//                }
//            }

//        }
//        /// <summary>
//        /// Returns the MD5 Password
//        /// the output parameter is used for getting the original password
//        /// </summary>
//        /// <param name="_UserId"></param>
//        /// <param name="password"></param>
//        /// <returns></returns>
//        private string GetPassword(string _UserId, out string password)
//        {
//            string strGuid, strMd5Password;
//            do
//            {
//                Guid objGuid = new Guid();
//                objGuid = Guid.NewGuid();
//                strGuid = Convert.ToString(objGuid);
//                string[] stringArray = { "0", "0", "O", "o", "I", "L", "1", "l", "s", "S", "5", "9", "Q", "q", "-" };
//                foreach (string strItem in stringArray)
//                {
//                    strGuid = strGuid.Replace(strItem, "");
//                }
//            }
//            while (strGuid.Length < 12);

//            Boolean bolHashCodeNotEmpty = false;
//            password = strGuid.Substring(0, 6);
//            do
//            {
//                strMd5Password = Utilities.GetMd5Hash(password);
//                if (strMd5Password.Length > 0)
//                    bolHashCodeNotEmpty = true;
//            }
//            while (bolHashCodeNotEmpty == false);
//            return strMd5Password;
//        }

//        void saveResetPassword(SqlConnection sqlConnection, SqlTransaction sqlTransaction, out string password)
//        {
//            string strMd5Password = GetPassword(UserId, out password);
//            string strQuery = @"INSERT INTO TBL_RESET_PASSWORD_LOG(COMPANY_ID,USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED) 
//                                VALUES(@COMPANY_ID,@USER_ID,@ACCESS_CODE,@ACCESSCODE_RESET_DATETIME,0,0);";
//            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@USER_ID", UserId);
//            cmd.Parameters.AddWithValue("@ACCESS_CODE", strMd5Password);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
//            cmd.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);
//            cmd.ExecuteNonQuery();
//        }
//        void saveInfoInEmailOutbox(SqlConnection sqlConnection, SqlTransaction sqlTransaction, string password)
//        {
//            string strURL = MficientConstants.ENTERPRISE_URL+"/resetPassword.aspx?" + Utilities.EncryptString(UserName + "~" + CompanyId);

//            SqlCommand objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_EMAIL_OUTBOX(EMAIL_ID,EMAIL,SUBJECT,BODY,POST_TIME,SEND_TIME)
//                            VALUES(@EMAIL_ID,@EMAIL,@SUBJECT,@BODY,@POST_TIME,@SEND_TIME)");
//            objSqlCommand.CommandType = CommandType.Text;
//            objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", Utilities.GetMd5Hash((EmailId + DateTime.UtcNow.Ticks.ToString())));
//            objSqlCommand.Parameters.AddWithValue("@EMAIL", EmailId);
//            objSqlCommand.Parameters.AddWithValue("@SUBJECT", "Mficient Credentials");
//            objSqlCommand.Parameters.AddWithValue("@POST_TIME", DateTime.UtcNow.Ticks);
//            objSqlCommand.Parameters.AddWithValue("@BODY", "Your New Password: " + " " + password + "<br/><br/>" + "Click here to reset your password" + " " + "<a href=" + strURL + ">value</a>");
//            objSqlCommand.Parameters.AddWithValue("@SEND_TIME", 0);
//            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
           
//        }

//        void updateIsExpiredStatus(SqlConnection sqlConnection, SqlTransaction sqlTransaction)
//        {
//            string strQuery = @"UPDATE TBL_RESET_PASSWORD_LOG
//                                SET IS_EXPIRED = 1
//                                WHERE USER_ID = @UserId";
//            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@UserId", UserId);
//            cmd.ExecuteNonQuery();
//        }
//        public DataTable ResultTable
//        {
//            set;
//            get;
//        }
//        public int StatusCode
//        {
//            set;
//            get;
//        }
//        public string StatusDescription
//        {
//            set;
//            get;
//        }
//        public string UserName
//        {
//            set;
//            get;
//        }
//        public string CompanyId
//        {
//            set;
//            get;
//        }
//        public string UserId
//        {
//            set;
//            get;
//        }
//        public string EmailId
//        {
//            set;
//            get;
//        }
//    }
//}