﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class ResetPasword
    {
        /// <summary>
        /// For Admin And SubAdmin
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="companyId"></param>
        /// <param name="context"></param>
        public ResetPasword(string userName,
            string companyId,
            HttpContext context)
        {
            this.UserName = userName;
            this.CompanyID = companyId;
            this.SubAdminId = String.Empty;
            this.PageContext = context;
        }
        /// <summary>
        /// For Mobile User
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="companyId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="context"></param>
        public ResetPasword(string userName,
            string companyId, string subAdminId,
            HttpContext context)
        {
            this.UserName = userName;
            this.CompanyID = companyId;
            this.SubAdminId = subAdminId;
            this.PageContext = context;
        }
        int intStatusCode;
        public void Process()
        {
            try
            {
                int intExecuteRecordsCount = -1;

                string strResetCode = GetPassword(UserName);

                SqlCommand objSqlCommand = new SqlCommand(@"select userID,EMAIL,USER_TYPE,USER_NAME  
                                                        from (select a.SUBADMIN_ID as userID,a.USER_NAME,b.COMPANY_ID,a.EMAIL,a.ADMIN_ID ,'" +
                                                        (int)RESET_PASSWORD_REQUEST_BY.SubAdmin +
                                                        @"'AS USER_TYPE
                                                        from TBL_SUB_ADMIN a inner join TBL_COMPANY_DETAIL b on a.ADMIN_ID=b.ADMIN_ID
                                                        union
                                                        select a.ADMIN_ID as userID,a.EMAIL_ID as USER_NAME,b.COMPANY_ID,a.EMAIL_ID as EMAIL,a.ADMIN_ID,'" +
                                                        (int)RESET_PASSWORD_REQUEST_BY.Admin + @"'AS USER_TYPE
                                                        from TBL_COMPANY_ADMINISTRATOR a inner join TBL_COMPANY_DETAIL b on a.ADMIN_ID=b.ADMIN_ID ) a 
                                                       where USER_NAME=@USER_NAME and a.COMPANY_ID=@COMPANY_ID
                                                        ORDER BY USER_TYPE;;");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", UserName);

                DataTable dt = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //changed on 14-8-2012.IsExpired Added Mohan
                    objSqlCommand = new SqlCommand(@"INSERT INTO TBL_RESET_PASSWORD_LOG(COMPANY_ID,USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED) 
                        VALUES(@COMPANY_ID,@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,0,0);");

                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", Utilities.GetMd5Hash(strResetCode));
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["userID"].ToString());
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);

                    intExecuteRecordsCount = MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        SaveEmailInfo.cpResetPassword(
                            Convert.ToString(dt.Rows[0]["USER_NAME"]),
                            Convert.ToString(dt.Rows[0]["EMAIL"]),
                            this.CompanyID, Convert.ToString(dt.Rows[0]["USER_TYPE"]),
                            strResetCode, this.PageContext
                            );

                        intStatusCode = 0;
                    }
                    else
                    {
                        intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    }
                }
                else
                {
                    intStatusCode = -1000;//for error
                }
            }
            catch
            {
                intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        public void ProcessForMobileUser()
        {
            try
            {
                int intExecuteRecordsCount = -1;

                string strResetCode = GetPassword(UserName);

                SqlCommand objSqlCommand = new SqlCommand(@"SELECT *,'" +
                                                         (int)RESET_PASSWORD_REQUEST_BY.User +
                                                         @"' AS USER_TYPE FROM TBL_USER_DETAIL
                                                        WHERE USER_NAME = @UserName
                                                        AND SUBADMIN_ID = @SubAdminId
                                                         AND COMPANY_ID =@COMPANY_ID");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@UserName", this.UserName);
                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);

                DataTable dt = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    objSqlCommand = new SqlCommand(@"INSERT INTO TBL_RESET_PASSWORD_LOG(COMPANY_ID,USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED) 
                        VALUES(@COMPANY_ID,@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,0,0);");

                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", Utilities.GetMd5Hash(strResetCode));
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["USER_ID"].ToString());
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);

                    intExecuteRecordsCount = MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        SaveEmailInfo.cpResetPassword(
                           Convert.ToString(dt.Rows[0]["USER_NAME"]),
                           Convert.ToString(dt.Rows[0]["EMAIL_ID"]),
                           this.CompanyID, Convert.ToString(dt.Rows[0]["USER_TYPE"]),
                           strResetCode, this.PageContext
                           );
                        intStatusCode = 0;
                    }
                    else
                    {
                        intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    }
                }
                else
                {
                    intStatusCode = -1000;
                }
            }
            catch
            {
                intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        private string GetPassword(string _UserId)
        {
            string strGuid, strPassword;
            do
            {
                Guid objGuid = new Guid();
                objGuid = Guid.NewGuid();
                strGuid = Convert.ToString(objGuid);
                string[] stringArray = { "0", "0", "O", "o", "I", "L", "1", "l", "s", "S", "5", "9", "Q", "q", "-" };
                foreach (string strItem in stringArray)
                {
                    strGuid = strGuid.Replace(strItem, "");
                }
            }
            while (strGuid.Length < 12);

            //Boolean bolHashCodeNotEmpty = false;
            strPassword = strGuid.Substring(0, 6);
            //do
            //{
            //    strPassword = Utilities.GetMd5Hash(strPassword);
            //    if (strPassword.Length > 0)
            //        bolHashCodeNotEmpty = true;
            //}
            //while (bolHashCodeNotEmpty == false);
            return strPassword;
        }
        public int StatusCode
        {
            get
            {
                return intStatusCode;
            }
        }
        public string UserName
        {
            get;
            set;
        }
        public string CompanyID
        {
            get;
            set;
        }

        public string SubAdminId
        {
            get;
            set;
        }
        public HttpContext PageContext
        {
            get;
            private set;
        }

    }
}