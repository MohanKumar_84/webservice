﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data.SqlClient;
//using System.Data;

//namespace mFicientCP
//{
//    public class RevertAppVersion
//    {
//       public RevertAppVersion()
//        { 

//        }

//       public RevertAppVersion(string _WorkFlowId, string _Version, string _SubAdminId, string _PrvVersion)
//        {
//            this.WorkFlowId = _WorkFlowId;
//            this.Version = _Version;
//            this.SubAdminId = _SubAdminId;
//            this.PrvVersion = _PrvVersion;
//        }   
//        public void Process()
//       {
//           this.StatusCode = -1000;
//           SqlConnection objSqlConnection = null;
//           SqlTransaction objSqlTransaction = null;
//           SqlCommand objSqlCommand;
//           try
//           {
//                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
//                objSqlTransaction = objSqlConnection.BeginTransaction();
//                string query;
//                query = @"INSERT INTO TBL_PRV_WORKFLOW_DETAIL SELECT WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,VERSION,WORKED_BY,@PUBLISH_ON,ALL_GROUPS,SUBADMIN_ID,FORMS,WF_ICON FROM TBL_CURR_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID;";
//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
//                objSqlCommand.Parameters.AddWithValue("@PUBLISH_ON", DateTime.UtcNow.Ticks);
//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                {
//                }
//                else
//                {
//                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
//                }
//                query = @"DELETE FROM TBL_CURR_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID; ";

//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);

//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                {
//                    this.StatusCode = 0;
//                }
//                else
//                {
//                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
//                }
//                query = @"DELETE FROM TBL_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID;";

//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);

//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                {
//                }
//                else
//                {
//                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
//                }

//                query = @"INSERT INTO TBL_CURR_WORKFLOW_DETAIL SELECT WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,@VERSION,WORKED_BY,@PUBLISH_ON,ALL_GROUPS,SUBADMIN_ID,FORMS,WF_ICON 
//                                        FROM TBL_PRV_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND VERSION=@PRV_VERSION;
//
//                              INSERT INTO TBL_WORKFLOW_DETAIL SELECT WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,WORKED_BY,@PUBLISH_ON,ALL_GROUPS,SUBADMIN_ID,FORMS,WF_ICON,@IS_PUBLISHED 
//                                        FROM TBL_PRV_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND VERSION=@PRV_VERSION;";

//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
//                objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
//                objSqlCommand.Parameters.AddWithValue("@PRV_VERSION", this.PrvVersion);
//                objSqlCommand.Parameters.AddWithValue("@PUBLISH_ON", DateTime.UtcNow.Ticks);
//                objSqlCommand.Parameters.AddWithValue("@IS_PUBLISHED", true);
//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                {
//                }
//                else
//                {
//                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
//                }

                


//                query = @"DELETE FROM TBL_PRV_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID AND VERSION=@PRV_VERSION;";

//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
//                objSqlCommand.Parameters.AddWithValue("@PRV_VERSION", this.PrvVersion);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);

//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                {
//                    this.StatusCode = 0;
//                }
//                else
//                {
//                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
//                }
//                objSqlTransaction.Commit();
//                this.StatusCode = 0;
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//                objSqlTransaction.Rollback();
//                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//                }
//            }
//           finally
//           {
//               MSSqlClient.SqlConnectionClose(objSqlConnection);
//           }
//        }
        
//        public string WorkFlowId
//        {
//            set;
//            get;
//        }
//        public string Version
//        {
//            set;
//            get;
//        }

//        public int StatusCode
//        {
//            set;
//            get;
//        }

//        public bool InsertInTblCurr { get; set; }

//        public string SubAdminId { get; set; }

//        public string PrvVersion { get; set; }

//        public bool IsMaxVersion { get; set; }
//    }
//}