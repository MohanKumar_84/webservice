﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public enum EnterpriseAdditionalDefinitionType
    {
        Properties = 01,
        Amazon = 2,
        Authentication = 3,        
        TraceDevice = 4,
    }
    public class SaveEnterpriseAdditionalDefinition
    {
        public SaveEnterpriseAdditionalDefinition(string _CompanyId, string _DefinitionJSON, EnterpriseAdditionalDefinitionType type)
        {
            this.CompanyId = _CompanyId;
            this.CredentialsDefinitionJson = "";
            this.amazondefinfinationJson = "";
            this.AdditionalParameterDefinitionJson = "";
            this.trackLocation = "";
            switch (type)
            {
                case EnterpriseAdditionalDefinitionType.Properties:
                    this.AdditionalParameterDefinitionJson = _DefinitionJSON;
                    break;
                case EnterpriseAdditionalDefinitionType.Amazon:
                    this.amazondefinfinationJson = _DefinitionJSON;
                    break;
                case EnterpriseAdditionalDefinitionType.Authentication:
                     this.CredentialsDefinitionJson = _DefinitionJSON;
                    break;
                case EnterpriseAdditionalDefinitionType.TraceDevice:
                    this.trackLocation = _DefinitionJSON;
                    break;
            }
            ProcessSaveAuthectication(type);
        }
        private void ProcessSaveAuthectication(EnterpriseAdditionalDefinitionType type)
        {
            this.StatusCode = -1000;
            SqlConnection objSqlConnection = null;
            SqlCommand objSqlCommand;
            string query;
            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                query = @"if not exists( select 'true' from TBL_ENTERPRISE_ADDITIONAL_DEFINITION where COMPANY_ID=@CompanyId)
	            INSERT INTO dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION(COMPANY_ID,ADDITIONAL_PARAMETERS_META,AUTHENTICATION_META,AMAZON_ATHENTICATION,TRACE_LOCATION)
                VALUES(@CompanyId,@ADDITIONAL_PARAMETERS_META,@AUTHENTICATION_META,@AMAZON_ATHENTICATION,@TRACE_LOCATION)
                else";
                switch (type)
                {
                    case EnterpriseAdditionalDefinitionType.Properties:
                        query += " Update dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION  set ADDITIONAL_PARAMETERS_META=@ADDITIONAL_PARAMETERS_META  where COMPANY_ID=@CompanyId";
                        break;
                    case EnterpriseAdditionalDefinitionType.Amazon:
                        query += " Update dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION  set AMAZON_ATHENTICATION=@AMAZON_ATHENTICATION where COMPANY_ID=@CompanyId";
                        break;
                    case EnterpriseAdditionalDefinitionType.Authentication:
                        query += " Update dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION  set AUTHENTICATION_META=@AUTHENTICATION_META  where COMPANY_ID=@CompanyId";
                        break;
                    case EnterpriseAdditionalDefinitionType.TraceDevice:
                        query += " Update dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION  set TRACE_LOCATION=@TRACE_LOCATION  where COMPANY_ID=@CompanyId";
                        break;
                }

                objSqlCommand = new SqlCommand(query, objSqlConnection);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_PARAMETERS_META", this.AdditionalParameterDefinitionJson);
                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_META", this.CredentialsDefinitionJson);
                objSqlCommand.Parameters.AddWithValue("@AMAZON_ATHENTICATION", this.amazondefinfinationJson);
                objSqlCommand.Parameters.AddWithValue("@TRACE_LOCATION", this.trackLocation);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";

            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        private string AdditionalParameterDefinitionJson { get; set; }
        private string amazondefinfinationJson { get; set; }
        private string CredentialsDefinitionJson { get; set; }
        private string CompanyId { get; set; }
        public string StatusDescription { get; set; }
        public int StatusCode { get; set; }
        private string trackLocation { get; set; }

    }
}