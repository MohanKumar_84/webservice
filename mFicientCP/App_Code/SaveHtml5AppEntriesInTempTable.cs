﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class SaveHtml5AppEntriesInTempTable
    {
        public SaveHtml5AppEntriesInTempTable(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public void InsertAppEntries(List<Html5AppEntryMetaData> appEntries, MFEHtml5AppDetail appInfo, string sessionId)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        foreach (Html5AppEntryMetaData appEntry in appEntries)
                        {
                            insertAppEntry(appEntry, appInfo.AppId, sessionId, transaction, con);
                        }
                        transaction.Commit();
                    }
                }

            }
            catch (SqlException e)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }

        }
        public void UpdateAppEntry(Html5AppEntryMetaData appEntry, MFEHtml5AppDetail appInfo, string sessionId)
        {
            try
            {
                updateAppEntry(appEntry, appInfo.AppId, sessionId);
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }

        }
        public void UpdateAppEntryFilePath(List<Html5AppFileMoveDataForUI> fileMoveData, string appId, string sessionId)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        foreach (Html5AppFileMoveDataForUI moveData in fileMoveData)
                        {
                            updateAppEntryFilePath(moveData, appId, sessionId, con, transaction);
                        }
                        transaction.Commit();
                    }
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        //        public void UpdateApp(MFEHtml5AppDetail appDetail)
        //        {
        //            string strQuery = @"UPDATE [TBL_HTML5_APP_DETAIL]
        //                               SET [APP_NAME] = @APP_NAME
        //                                  ,[DESCRIPTION] = @DESCRIPTION
        //                                  ,[APP_JSON] = @APP_JSON
        //                                  ,[ICON] = @ICON
        //                                  ,[UPDATE_ON] = @UPDATE_ON
        //      
        //                             WHERE APP_ID = @APP_ID
        //                             AND COMPANY_ID = @COMPANY_ID
        //                             AND SUBADMIN_ID = @SUBADMIN_ID";

        //            SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //            objSqlCommand.CommandType = CommandType.Text;
        //            objSqlCommand.Parameters.AddWithValue("@APP_NAME", appDetail.AppName);
        //            objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", appDetail.Description);
        //            objSqlCommand.Parameters.AddWithValue("@APP_JSON", appDetail.AppJson);
        //            objSqlCommand.Parameters.AddWithValue("@ICON", appDetail.Icon);
        //            objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
        //            objSqlCommand.Parameters.AddWithValue("@APP_ID", appDetail.AppId);
        //            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
        //            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
        //            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);

        //        }
        //        public MFEHtml5AppDetail SaveAsApp(string appName, MfHtml5App appToCopy)
        //        {
        //            //string strNewAppId = String.Empty;
        //            MfHtml5App objHtml5App = null;
        //            MFEHtml5AppDetail objMfeHtml5AppDtl = null;
        //            try
        //            {
        //                string strAppId = Utilities.GetMd5Hash(this.SubadminId + appName + this.CompanyId + DateTime.UtcNow.Ticks.ToString());
        //                objHtml5App = new MfHtml5App();
        //                objHtml5App.id = strAppId;
        //                objHtml5App.name = appName;
        //                objHtml5App.icon = appToCopy.icon;
        //                objHtml5App.startupPage = appToCopy.startupPage;
        //                objHtml5App.description = appToCopy.description;
        //                objMfeHtml5AppDtl = insertNewApp(objHtml5App);
        //                //strNewAppId = strAppId;
        //                StatusCode = 0;
        //                this.StatusDescription = "";
        //            }
        //            catch (Exception)
        //            {
        //                this.StatusCode = -1000;
        //                this.StatusDescription = "Internal server error";
        //            }
        //            return objMfeHtml5AppDtl;
        //        }

        void insertAppEntry(Html5AppEntryMetaData appEntry,
                            string appId,
                            string sessionId,
                            SqlTransaction transaction,
                            SqlConnection conn)
        {
            string strQuery = @"INSERT INTO [TBL_TEMP_HTML5_APP]
                                   ([COMPANY_ID],[SUBADMIN_ID],[SESSION_ID]
                                   ,[APP_ID],[OPEN_DATETIME],[CDN_PATH]
                                   ,[FILE_PATH],[FILE_META],[FILE_CONTENT])
                             VALUES
                                   (@COMPANY_ID,@SUBADMIN_ID,@SESSION_ID
                                   ,@APP_ID,@OPEN_DATETIME,@CDN_PATH
                                   ,@FILE_PATH,@FILE_META,@FILE_CONTENT)";


            SqlCommand objSqlCommand = new SqlCommand(strQuery, conn, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@OPEN_DATETIME", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@CDN_PATH", String.Empty);
            objSqlCommand.Parameters.AddWithValue("@FILE_PATH", appEntry.FilePath);
            objSqlCommand.Parameters.AddWithValue("@FILE_META", Utilities.SerializeJson<Html5AppEntryMetaData>(appEntry));
            objSqlCommand.Parameters.AddWithValue("@FILE_CONTENT", appEntry.FileContent);
            objSqlCommand.ExecuteNonQuery();

        }
        //        MfHtml5App insertNewApp(string appId, string appName)
        //        {
        //            string strQuery = @"INSERT INTO [TBL_HTML5_APP_DETAIL]
        //                                ([COMPANY_ID],[APP_ID],[APP_NAME]
        //                                ,[DESCRIPTION],[APP_JSON],[ICON]
        //                                ,[SUBADMIN_ID],[CREATED_BY],[UPDATE_ON]
        //                                ,[BUCKET_FILE_PATH])
        //                                VALUES
        //                                (@COMPANY_ID,@APP_ID,@APP_NAME,
        //                                    @DESCRIPTION,@APP_JSON,@ICON,
        //                                    @SUBADMIN_ID,@CREATED_BY,@UPDATE_ON,
        //                                    @BUCKET_FILE_PATH)";

        //            MfHtml5App objHtml5App = new MfHtml5App();
        //            objHtml5App.id = appId;
        //            objHtml5App.name = appName;
        //            objHtml5App.icon = MficientConstants.IDE_APPLICATION_DEFAULT_ICON;
        //            objHtml5App.startupPage = String.Empty;
        //            objHtml5App.description = String.Empty;

        //            string strS3BucketFilePath = String.Empty;
        //            strS3BucketFilePath = S3Bucket.GetWorkingFolderFilePath(this.SubadminId, this.CompanyId, appId, appId + ".zip");
        //            SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //            objSqlCommand.CommandType = CommandType.Text;
        //            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
        //            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
        //            objSqlCommand.Parameters.AddWithValue("@APP_NAME", appName);
        //            objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", "");
        //            objSqlCommand.Parameters.AddWithValue("@APP_JSON", Utilities.SerializeJson<MfHtml5App>(objHtml5App));
        //            objSqlCommand.Parameters.AddWithValue("@ICON", MficientConstants.IDE_APPLICATION_DEFAULT_ICON);
        //            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
        //            objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubadminId);
        //            objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
        //            objSqlCommand.Parameters.AddWithValue("@BUCKET_FILE_PATH", strS3BucketFilePath);
        //            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);

        //            return objHtml5App;
        //        }
        void updateAppEntry(Html5AppEntryMetaData appEntry,
                            string appId,
                            string sessionId)
        {
            string strQuery = @"UPDATE TBL_TEMP_HTML5_APP
                               SET FILE_META = @FILE_META
                               ,FILE_CONTENT = @FILE_CONTENT
                               ,OPEN_DATETIME = @OPEN_DATETIME
                               WHERE COMPANY_ID = @COMPANY_ID
                               AND SUBADMIN_ID = @SUBADMIN_ID
                               AND SESSION_ID = @SESSION_ID
                               AND APP_ID = @APP_ID
                               AND FILE_PATH = @FILE_PATH";


            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@FILE_META", Utilities.SerializeJson<Html5AppEntryMetaData>(appEntry));
            objSqlCommand.Parameters.AddWithValue("@FILE_CONTENT", appEntry.FileContent);
            objSqlCommand.Parameters.AddWithValue("@OPEN_DATETIME", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@FILE_PATH", appEntry.FilePath);
            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
        }

        void updateAppEntryFilePath(Html5AppFileMoveDataForUI fileMoveData,
                            string appId,
                            string sessionId,
                            SqlConnection con,
                            SqlTransaction transaction)
        {

            string strQuery = @"UPDATE TBL_TEMP_HTML5_APP
                               SET FILE_PATH = @FILE_NEW_PATH,
                                FILE_META = @FILE_META
                               WHERE COMPANY_ID = @COMPANY_ID
                               AND SUBADMIN_ID = @SUBADMIN_ID
                               AND SESSION_ID = @SESSION_ID
                               AND APP_ID = @APP_ID
                               AND FILE_PATH = @FILE_OLD_PATH";


            SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@FILE_NEW_PATH", fileMoveData.newPath);
            objSqlCommand.Parameters.AddWithValue("@FILE_OLD_PATH", fileMoveData.oldPath);
            objSqlCommand.Parameters.AddWithValue("@FILE_META", fileMoveData.metaData);
            objSqlCommand.ExecuteNonQuery();
        }
        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
    }
}