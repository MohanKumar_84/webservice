﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class SaveHtml5AppFile
    {
        private HTML5AppFile appFile;
        private FileType fileType;
        private string appId, companyId;

        public SaveHtml5AppFile(HTML5AppFile _appFile, FileType _fileType, string _appId, string _companyId)
        {
            appFile = _appFile;
            fileType = _fileType;
            appId = _appId;
            companyId = _companyId;
        }

        public void Process(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string tableName = getTableName();
                string query = @"IF NOT EXISTS(SELECT * FROM " + tableName + @" WHERE FILENAME = @FILENAME AND APP_ID = @APP_ID AND COMPANY_ID = @COMPANY_ID AND FILE_EXTENSION = @FILE_EXTENSION) 
                             INSERT INTO " + tableName + @" (FILENAME, APP_ID, COMPANY_ID, FILE_CONTENT, FILE_EXTENSION, CREATED_ON, UPDTAED_ON) VALUES (@FILENAME, @APP_ID, @COMPANY_ID, @FILE_CONTENT, @FILE_EXTENSION, @CREATED_ON, @UPDTAED_ON)";

                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@FILENAME", appFile.FileName);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                objSqlCommand.Parameters.AddWithValue("@FILE_CONTENT", appFile.FileContent);
                objSqlCommand.Parameters.AddWithValue("@FILE_EXTENSION", appFile.FileExtension);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@UPDTAED_ON", DateTime.UtcNow.Ticks);
                int iRowsEffected = objSqlCommand.ExecuteNonQuery();
                if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string getTableName()
        {
            string tableName = string.Empty;
            switch (fileType)
            {
                case FileType.Image:
                    tableName = "TBL_HTML5_IMAGE_FILES";
                    break;

                case FileType.Script:
                    tableName = "TBL_HTML5_SCRIPT_FILES";
                    break;

                case FileType.Style:
                    tableName = "TBL_HTML5_STYLE_FILES";
                    break;

                case FileType.Root:
                    tableName = "TBL_HTML5_ROOT_FILES";
                    break;
            }
            return tableName;
        }
    }
}