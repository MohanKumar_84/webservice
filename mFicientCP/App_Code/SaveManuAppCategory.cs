﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class SaveManuAppCategory
    {
        
        public SaveManuAppCategory(string companyId, string menuCategoryName, int categoryDisplayIndex, string groups, bool isForAllGroups, string menuCategoryId, string subAdminId, List<AppAndCategoryLink> _lst1, string addedapp, string existingapp)
        {
            this.CompanyId = companyId;
            this.MenuCategoryName = menuCategoryName;
            this.CategoryDisplayIndex = categoryDisplayIndex;
            this.Groups = groups;
            this.IsForAllGroups = isForAllGroups;
            this.MenuCategoryId = menuCategoryId;
            this.SubAdminId = subAdminId;
            this.lstAppAndCategoryLink = _lst1;
            this.Existingapp = existingapp;
            this.Addedapp = addedapp;

        }
        public void Process()
        {
            StatusCode = -1000;
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                GetMenuCategoryDtlsByMenuCatId objGetMenuCategorydtl = new GetMenuCategoryDtlsByMenuCatId(this.CompanyId, this.MenuCategoryId);
                objGetMenuCategorydtl.Process();

                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        if (!String.IsNullOrEmpty(MenuCategoryId))
                        {
                            RemoveExistingApps(transaction, con);
                            foreach (AppAndCategoryLink lst in lstAppAndCategoryLink)
                            {
                                AddNewApps(transaction, con, lst);
                            }
                            transaction.Commit();
                        }
                        AddIntoUpdationRequiredForMenu obj_up = new AddIntoUpdationRequiredForMenu();
                        if (objGetMenuCategorydtl.StatusCode == 0)
                        {
                            if (!IsForAllGroups)
                                obj_up.CategoryUpdationForSelectedGroup(CompanyId,  objGetMenuCategorydtl.ResultTables);
                            else
                                obj_up.CategoryUpdationForAllGroup(CompanyId);
                        }
                        StatusCode = 0;
                        StatusDescription = "";
                        if (Addedapp != "")
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.CATEGORY_MODIFIED_APPADDED, this.CompanyId, this.SubAdminId, this.MenuCategoryId, this.MenuCategoryName, "", "", "", "", "", Addedapp);
                        if (Existingapp != "")
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.CATEGORY_MODIFIED_APP_DELETED, this.CompanyId, this.SubAdminId, this.MenuCategoryId, this.MenuCategoryName, "", "", "", "", "", Existingapp);
                    }
                }
            }
            catch
            {
                if (transaction != null) transaction.Rollback();
                StatusCode = -1000;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        /// <summary>
        /// Update Existing Selected Apps
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="connection"></param>
        void RemoveExistingApps(SqlTransaction transaction, SqlConnection connection)
        {
            //            string strQuery = @"update TBL_WORKFLOW_AND_CATEGORY_LINK set 
            //                              CATEGORY_ID=''  where WORKFLOW_ID =@WORKFLOW_ID and COMPANY_ID=@CompanyId";
            //            string strQuery = @"DELETE FROM TBL_WORKFLOW_AND_CATEGORY_LINK 
            //                               where WORKFLOW_ID =@WORKFLOW_ID AND CATEGORY_ID = @CATEGORY_ID and COMPANY_ID=@CompanyId";

            string strQuery = @"DELETE FROM TBL_WORKFLOW_AND_CATEGORY_LINK 
                               where CATEGORY_ID = @CATEGORY_ID and COMPANY_ID=@CompanyId";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            //cmd.Parameters.AddWithValue("@WORKFLOW_ID", appSelected.WORKFLOW_ID);
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@CATEGORY_ID", this.MenuCategoryId);
            //cmd.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
            cmd.ExecuteNonQuery();
        }

        void AddNewApps(SqlTransaction transaction, SqlConnection connection, AppAndCategoryLink appSelected1)
        {

            //            string strQuery = @"update dbo.TBL_WORKFLOW_AND_CATEGORY_LINK Set CATEGORY_ID=@CATEGORY_ID
            //                                      where WORKFLOW_ID =@WORKFLOW_ID  and COMPANY_ID=@COMPANY_ID ;";
            string strQuery = @"INSERT INTO TBL_WORKFLOW_AND_CATEGORY_LINK VALUES (@COMPANY_ID ,@WORKFLOW_ID,@CATEGORY_ID);";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
            cmd.Parameters.AddWithValue("@CATEGORY_ID", this.MenuCategoryId);
            cmd.Parameters.AddWithValue("@WORKFLOW_ID", appSelected1.WORKFLOW_ID);
            //cmd.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
            cmd.ExecuteNonQuery();
        }

        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string MenuCategoryName
        {
            set;
            get;
        }

        public int CategoryDisplayIndex
        {
            set;
            get;
        }

        public string Groups
        {
            set;
            get;
        }

        public bool IsForAllGroups
        {
            set;
            get;
        }

        public string MenuCategoryId
        {
            set;
            get;
        }

        public string StatusDescription
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }

        public string Addedapp
        {
            set;
            get;
        }
        public string Existingapp
        {
            set;
            get;
        }

        //public string CategoryIcon { get; set; }
        //public List<SelectetdList> Slst { get; set; }
        public List<AppAndCategoryLink> lstAppAndCategoryLink { get; set; }

    }
    public class AppAndCategoryLink
    {
        public string WORKFLOW_ID { get; set; }
        public string Category_ID { get; set; }
        public string WORKFLOW_NAME { get; set; }
    }
}