﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class SavePushMsgOutboxInMgram
    {
        public enum NOTIFICATION_TYPE
        {
            /**
             * MF_DEVICE_BLK = 0,
               MF_DEVICE_REJ = 1,
               MF_DEVICE_APP = 2,
             * Values used at MGRAM END
             * **/
            MF_USER_BLOCK = 0,
            MF_DEVICE_REJECT = 1,
            MF_DEVICE_APPRV = 2,
        }
        string _enterpriseId, _userName;
        int _statusCode, _badgeCount;
        string _deviceID, _devTokenID, _devType, _info;
        NOTIFICATION_TYPE _eNotificationType;

        public SavePushMsgOutboxInMgram(
            string companyId,
            string userName,
            string deviceID,
            string devTokenID,
            string devType,
            NOTIFICATION_TYPE notificationType,string info)
        {
            this._enterpriseId = companyId;
            this._userName = userName;
            this._deviceID = deviceID;
            this._devTokenID = devTokenID;
            this._eNotificationType = notificationType;
            this._devType = devType;
            this._info = info;
        }
        public void Process()
        {
            try
            {
                savePushMsgOutboxDtls();
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                //this.StatusDescription = "Internal server error.";
                if (ex.Message == (((int)DATABASE_ERRORS.RECORD_INSERT_ERROR)).ToString())
                {
                    //this.StatusDescription = "Record Insertion error.";
                }
            }
        }
        void savePushMsgOutboxDtls()
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            SqlCommand cmd = getSqlCommand(getQuery());
            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }

        string getQuery()
        {
            return @"INSERT INTO [TBL_PUSHMESSAGE_OUTBOX]
           ([PUSH_NOTIFICATION_ID],[ENTERPRISE_ID]
           ,[BADGE_COUNT],[USERNAME]
           ,[FROM_USER_FIRSTNAME],[SOURCE]
           ,[NOTIFICATION_TYPE],[PUSH_DATETIME],
            [DEVICE_TOKEN_ID],[DEVICE_TYPE],[DEVICE_ID],[INFO])
     VALUES
           (@PUSH_NOTIFICATION_ID,@ENTERPRISE_ID
           ,@BADGE_COUNT,@USERNAME
           ,@FROM_USER_FIRSTNAME,@SOURCE
           ,@NOTIFICATION_TYPE,
           @PUSH_DATETIME
           ,@DEVICE_TOKEN_ID
           ,@DEVICE_TYPE,@DEVICE_ID,@INFO);";

        }
        SqlCommand getSqlCommand(string query)
        {

            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue(
                "@PUSH_NOTIFICATION_ID",
                Utilities.GetMd5Hash(this._enterpriseId + DateTime.UtcNow.Ticks + this._userName)
                );
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", this._enterpriseId);
            cmd.Parameters.AddWithValue("@BADGE_COUNT", 1);
            cmd.Parameters.AddWithValue("@USERNAME", this._userName);
            cmd.Parameters.AddWithValue("@FROM_USER_FIRSTNAME", String.Empty);
            cmd.Parameters.AddWithValue("@SOURCE",0);
            cmd.Parameters.AddWithValue("@NOTIFICATION_TYPE", Convert.ToInt16((int)_eNotificationType));
            cmd.Parameters.AddWithValue("@PUSH_DATETIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@DEVICE_TOKEN_ID", this._devTokenID);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", this._devType);
            cmd.Parameters.AddWithValue("@DEVICE_ID", this._deviceID);
            cmd.Parameters.AddWithValue("@INFO", this._info);
            return cmd;
        }
        #region Public Properties
        //public string EnterpriseId
        //{
        //    get { return _enterpriseId; }
        //    private set { _enterpriseId = value; }
        //}

        //public string UserName
        //{
        //    get { return _userName; }
        //    private set { _userName = value; }
        //}
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        //public string StatusDescription
        //{
        //    get { return _statusDescription; }
        //    set { _statusDescription = value; }
        //}
        //public string PushNotificationId
        //{
        //    get { return _pushNotificationId; }
        //    private set { _pushNotificationId = value; }
        //}
        //public int BadgeCount
        //{
        //    get { return _badgeCount; }
        //    private set { _badgeCount = value; }
        //}
        //public NOTIFICATION_TYPE ENotificationType
        //{
        //    get { return _eNotificationType; }
        //    private set { _eNotificationType = value; }
        //}
        //public string DeviceID
        //{
        //    get { return _deviceID; }
        //    private set { _deviceID = value; }
        //}
        //public string DevTokenID
        //{
        //    get { return _devTokenID; }
        //    private set { _devTokenID = value; }
        //}
        //public string DevType
        //{
        //    get { return _devType; }
        //    private set { _devType = value; }
        //}
        #endregion
    }
}