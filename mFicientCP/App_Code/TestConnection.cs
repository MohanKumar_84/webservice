﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Net;
using System.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace mFicientCP
{
    public class TestConnection
    {
        #region Private Members

        private string enterpriseId, connectionString, mPluginAgentName;
        DatabaseType dbType;
        #endregion

        #region Constructor

        public TestConnection(string _databaseType, string _host, string _databaseName, string _userId, string _password, int _timeout, string _additionalString, string _enterpriseId, string _mPluginAgentName)
        {
            enterpriseId = _enterpriseId;
            mPluginAgentName = _mPluginAgentName;
            dbType = (DatabaseType)Convert.ToInt32(_databaseType);
            connectionString = Utilities.getConnectionString(_host, _databaseName, _userId, _password, _additionalString, _timeout.ToString(), dbType);
        }

        #endregion

        #region Public Methods

        public bool Process()
        {
            if (string.IsNullOrEmpty(mPluginAgentName))
            {
                return testDataBaseConnection();
            }
            else
            {
                return getMpluginResponse();
            }
        }

        #endregion

        #region Private Methods

        private bool getMpluginResponse()
        {
            bool isConnected = false;
            string strUrl = string.Empty;
            try
            {
                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(enterpriseId);
                objServerUrl.Process();
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (!string.IsNullOrEmpty(strUrl))
                    {
                        strUrl = strUrl + "/TestConnection.aspx?d=" + Utilities.UrlEncode(ReplaceBackSlash(getMpluginRequest()));
                        isConnected = callHttpWebService(strUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //strUrl = "http://localhost:50995/TestConnection.aspx?d=" +Utilities.UrlEncode(GetMpluginRequest());
            return isConnected;
        }

        private string getMpluginRequest()
        {
            string strRqst = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objAgent = new mPluginAgents();
            strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + enterpriseId + "\",\"data\":{\"rtyp\":\"2\",\"con\":\"" + connectionString + "\",\"dbtyp\":\"" + ((int)dbType).ToString() + "\",\"agtid\":\"" + mPluginAgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(enterpriseId, mPluginAgentName) + "\"}}}";
            return strRqst;
        }
        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            return Regex.Replace(_Input, @"\\", @"\\");
        }
        private bool callHttpWebService(string _WebServiceURL)
        {
            bool response = false;
            try
            {
                HTTP oHttp = new HTTP(_WebServiceURL);
                oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                HttpResponseStatus oResponse = oHttp.Request();
                if (oResponse.StatusCode == HttpStatusCode.OK)
                {
                    response = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        private bool testDataBaseConnection()
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception(@"Invalid connection string.");
            }
            switch (dbType)
            {
                case DatabaseType.MSSQL:
                    return MSSqlClient.TestMsSQLConnection(connectionString);
                case DatabaseType.MYSQL:
                    return MySQLClient.TestMySQLConnection(connectionString);
                case DatabaseType.ORACLE:
                    return OracleClientHelper.TestOracleConnection(connectionString);
                case DatabaseType.POSTGRESQL:
                    return PostgreSqlClient.TestConnection(connectionString);
            }
            return false;
        }

        #endregion

    }
}