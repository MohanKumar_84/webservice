﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace mFicientCP
{
    public class TransferAppCommands
    {
        private DatabaseCommand dbCommand;
        private WebServiceCommand wsCommand;
        private OdataCommand oDataCommand;
        private OfflineDataObject offlineDataObject;
        private string subAdminId, companyId, connectionId, newCommandId;
        private DataTable dt;

        public TransferAppCommands(DatabaseCommand _dbCommand, string _subAdminId, string _companyId, string _connectionId)
        {
            dt = new DataTable();
            newCommandId = string.Empty;
            dbCommand = _dbCommand;
            subAdminId = _subAdminId;
            companyId = _companyId;
            connectionId = _connectionId;
            processDbCommand();
        }

        public TransferAppCommands(WebServiceCommand _wsCommand, string _subAdminId, string _companyId, string _connectionId)
        {
            dt = new DataTable();
            newCommandId = string.Empty;
            wsCommand = _wsCommand;
            subAdminId = _subAdminId;
            companyId = _companyId;
            connectionId = _connectionId;
            processWSCommand();
        }

        public TransferAppCommands(OdataCommand _oDataCommand, string _subAdminId, string _companyId, string _connectionId)
        {
            dt = new DataTable();
            newCommandId = string.Empty;
            oDataCommand = _oDataCommand;
            subAdminId = _subAdminId;
            companyId = _companyId;
            connectionId = _connectionId;
            processODataCommand();
        }

        public TransferAppCommands(OfflineDataObject _offlineDataObject, string _subAdminId, string _companyId, string _connectionId)
        {
            dt = new DataTable();
            newCommandId = string.Empty;
            offlineDataObject = _offlineDataObject;
            subAdminId = _subAdminId;
            companyId = _companyId;
            connectionId = _connectionId;
            processOfflineDataCommand();
        }

        public string NewCommandId
        {
            get
            {
                return newCommandId;
            }
        }

        private void processDbCommand()
        {
         
            if (dbCommand != null)
            {
                //comment due to error
                //AddNewDbCommand addDbNewCmd11 = new AddNewDbCommand(subAdminId, dbCommand.CommandName, connectionId, dbCommand.SqlQuery ,dbCommand.Parameter
                //                                ,dbCommand.ReturnType,dbCommand.DataBaseCommandType,dbCommand.TableName,
                //                                 dbCommand.Querytype,
                //                               dbCommand.Parameterjson, dbCommand.Columns , companyId,dbCommand.Description, );

                //newCommandId = addDbNewCmd11.CommandId;
            }
        }

        private void processWSCommand()
        {
          
            if (wsCommand != null)
            {
                long updatedOn = 0;
                AddNewWsCommand addNewWSCmd = new AddNewWsCommand(subAdminId, wsCommand.CommandName, connectionId,wsCommand.Url, wsCommand.Parameter,
                wsCommand.ReturnType, wsCommand.RecordPath, wsCommand.DataSetPath, wsCommand.Tag, wsCommand.Service, wsCommand.PortType,wsCommand.Method,wsCommand.WSType.ToString(),
                "",wsCommand.OperationAction,
                "", Convert.ToInt32(wsCommand.HTTPType) , wsCommand.HttpRequest, companyId, wsCommand.Description
                , wsCommand.Cache, wsCommand.Expiryfrequency, wsCommand.ExpiryCondtion, wsCommand.Xreference, subAdminId,
                DateTime.UtcNow.Ticks, updatedOn,"[]");

                newCommandId = addNewWSCmd.CommandId;
            }
        }

        private void processODataCommand()
        { 
           
            if (oDataCommand != null)
            {
                AddOdataCommand addNewOdataCmd = new AddOdataCommand(companyId, subAdminId, connectionId, oDataCommand.CommandName,
                   oDataCommand.Resourcetype.ToString(), oDataCommand.ResourcePath, oDataCommand.AddtionalResourcepath , Convert.ToInt32(oDataCommand.Httptype).ToString(),
                                                                      oDataCommand.HttpData ,oDataCommand.QueryOptions,oDataCommand.InputParajson,
                                                                    oDataCommand.OutputParaJson,
                                                             Convert.ToInt32(oDataCommand.Functiontype).ToString(), Convert.ToInt32(oDataCommand.Responseformat).ToString(), oDataCommand.Datajson
                                                               , oDataCommand.ReturnType, oDataCommand.DataSetPath, oDataCommand.Description,
                                                                    oDataCommand.Cache,oDataCommand.Expiryfrequency,oDataCommand.ExpiryCondtion
                                                                  , subAdminId,
                                                                    DateTime.UtcNow.Ticks, 0, oDataCommand.Xreference);

                newCommandId = addNewOdataCmd.CommandId;
            }
        }

        private void processOfflineDataCommand()
        {
            try
            {
                if (offlineDataObject != null)
                {
                    //AddOfflineDataObject addOfflineDataCmd = new AddOfflineDataObject(companyId, subAdminId, offlineDataObject.Name, offlineDataObject.Description, offlineDataObject.Query,
                    //                                                                  offlineDataObject.InputParameters, offlineDataObject.OutputParameters, offlineDataObject.OfflineTable,
                    //                                                                  offlineDataObject.QueryType);


                    //newCommandId = addOfflineDataCmd.ID;
                }
            }
            catch
            { 
            }
        }
    }
}
