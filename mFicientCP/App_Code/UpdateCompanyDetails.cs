﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateCompanyDetails
    {
       private string strAdminId,strCompanyId,
           strCompanyName, strCompanyRegNo, strCompanyStreetAddress1,strCompanyStreetAddress2,strCompanyStreetAddress3,
                            strCompanyCityName,strCompanyStateName,strCompanyCountryCode,strCompanyZip;

       //private Int64 intDateOfBirth;

       //public UpdateCompanyDetails(string _AdminId, string _CompanyId, string _CompanyName, string _CompanyRegNo, string _CompanyStreetAddress1, string _CompanyStreetAddress2, string _CompanyStreetAddress3,
       //    string _CompanyCityName, string _CompanyStateName, string _CompanyCountryCode, string _CompanyZip)
       // {
       //     strAdminId = _AdminId; 
       //     //strCompanyId = _CompanyId;
       //     //strCompanyName=_CompanyName;
       //     //strCompanyRegNo = _CompanyRegNo;
       //     strCompanyStreetAddress1=_CompanyStreetAddress1;
       //     strCompanyStreetAddress2=_CompanyStreetAddress2;
       //     strCompanyStreetAddress3 = _CompanyStreetAddress3;
       //     strCompanyCityName = _CompanyCityName;
       //     strCompanyStateName = _CompanyStateName;
       //     //strCompanyCountryCode = _CompanyCountryCode;
       //     strCompanyZip = _CompanyZip;
           
       // }

       public UpdateCompanyDetails(string _AdminId,string _CompanyId,string _CompanyStreetAddress1, string _CompanyStreetAddress2, string _CompanyStreetAddress3,
         string _CompanyCityName, string _CompanyStateName,string _CompanyZip)
       {
           strAdminId = _AdminId;
           strCompanyId = _CompanyId;
           //strCompanyName=_CompanyName;
           //strCompanyRegNo = _CompanyRegNo;
           strCompanyStreetAddress1 = _CompanyStreetAddress1;
           strCompanyStreetAddress2 = _CompanyStreetAddress2;
           strCompanyStreetAddress3 = _CompanyStreetAddress3;
           strCompanyCityName = _CompanyCityName;
           strCompanyStateName = _CompanyStateName;
           //strCompanyCountryCode = _CompanyCountryCode;
           strCompanyZip = _CompanyZip;

       }

       public UpdateCompanyDetails()
       {
           strAdminId = this.AdminId;
           strCompanyId = this.CompanyId;
           strCompanyName = this.CompanyName;
           strCompanyRegNo = this.CompanyRegNo;
           strCompanyStreetAddress1 = this.CompanyStreetAddress1;
           strCompanyStreetAddress2 = this.CompanyStreetAddress2;
           strCompanyStreetAddress3 = this.CompanyStreetAddress3;
           strCompanyCityName = this.CompanyCityName;
           strCompanyStateName = this.CompanyStateName;
           strCompanyCountryCode = this.CompanyCountryCode;
           strCompanyZip = this.CompanyZip;

       }

       public void Process()
       {
           //int intStatusCode = 0;
           //string strStatusDescription = "";
           
               try
               {
//                   string query = @"UPDATE TBL_COMPANY_DETAIL SET 
//                                            COMPANY_NAME=@COMPANY_NAME,
//                                            REGISTRATION_NO=@REGISTRATION_NO,
//                                            STREET_ADDRESS1=@STREET_ADDRESS1,
//                                            STREET_ADDRESS2=@STREET_ADDRESS2,
//                                            STREET_ADDRESS3=@STREET_ADDRESS3,
//                                            CITY_NAME=@CITY_NAME,
//                                            STATE_NAME=@STATE_NAME,
//                                            COUNTRY_CODE=@COUNTRY_CODE,
//                                            ZIP=@ZIP
//                                       WHERE
//                                            COMPANY_ID=@COMPANY_ID 
//                                            AND 
//                                            ADMIN_ID=ADMIN_ID";

                   string query = @"UPDATE TBL_COMPANY_DETAIL SET 
                                            STREET_ADDRESS1=@STREET_ADDRESS1,
                                            STREET_ADDRESS2=@STREET_ADDRESS2,
                                            STREET_ADDRESS3=@STREET_ADDRESS3,
                                            CITY_NAME=@CITY_NAME,
                                            STATE_NAME=@STATE_NAME
                                       WHERE
                                            COMPANY_ID=@COMPANY_ID 
                                            AND 
                                            ADMIN_ID=ADMIN_ID";

                   SqlCommand objSqlCommand = new SqlCommand(query);

                   objSqlCommand.CommandType = CommandType.Text;

                   objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", strCompanyId);
                   objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", strAdminId);
                   //objSqlCommand.Parameters.AddWithValue("@COMPANY_NAME", strCompanyName);
                   //objSqlCommand.Parameters.AddWithValue("@REGISTRATION_NO", strCompanyRegNo);
                   objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS1", strCompanyStreetAddress1);
                   objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS2", strCompanyStreetAddress2);
                   objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS3", strCompanyStreetAddress3);
                   objSqlCommand.Parameters.AddWithValue("@CITY_NAME", strCompanyCityName);
                   objSqlCommand.Parameters.AddWithValue("@STATE_NAME", strCompanyStateName);
                   //objSqlCommand.Parameters.AddWithValue("@COUNTRY_CODE", strCompanyCountryCode);
                   objSqlCommand.Parameters.AddWithValue("@ZIP", strCompanyZip);

                   if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                   {
                       //string strMessageBody = "Details updated suceessfully.";
                       StatusCode = 0;
                   }
                   else
                   {
                       StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                       throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                   }
               }
               catch (Exception ex)
               {
                   if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                   {
                       StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                       //throw ex;
                   }
                   StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                 // throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
               }
       }

     

       private bool isValidDateOfBirth(string _Dob)
       {
           if (!string.IsNullOrEmpty(_Dob))
           {
               try
               {
                   DateTime dt = new DateTime(int.Parse(_Dob.Substring(0, 4)), int.Parse(_Dob.Substring(4, 2)), int.Parse(_Dob.Substring(6, 2)));
                   if (dt < DateTime.UtcNow.Date)
                   {
                       return true;
                   }
               }
               catch
               {
               }
           }
           return false;
       }

      
       public string AdminId
       {
           get;
           set;
       }
       public string CompanyId
       {
           get;
           set;
       }
       public string CompanyName
       {
           get;
           set;
       }
       public string CompanyRegNo
       {
           get;
           set;
       }
       public string CompanyStreetAddress1
       {
           get;
           set;
       }
       public string CompanyStreetAddress2
       {
           get;
           set;
       }
       public string CompanyStreetAddress3
       {
           get;
           set;
       }
       public string CompanyCityName
       {
           get;
           set;

       }
       public string CompanyStateName
       {
           get;
           set;
       }
       public string CompanyCountryCode
       {
           get;
           set;
       }
       public string CompanyZip
       {
           get;
           set;
       }

       public int StatusCode
       {
           get;
           set;
       }
    }
}