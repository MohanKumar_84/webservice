﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateDbConnection
    {
        public UpdateDbConnection()
        {

        }

        public UpdateDbConnection(string _SubAdminId, string _ConnectionId, string _HostName, string _Database, string _UserId, string _Password, string _TimeOut, string _AdditinalString, string _MpluginAgent)
        {
            this.SubAdminId = _SubAdminId;
            this.UserId = _UserId;
            this.Password = _Password;
            this.HostName = _HostName;
            this.Database = _Database;
            this.ConnectionId = _ConnectionId;
            this.AdditinalString = _AdditinalString;
            this.TimeOut = _TimeOut;
            this.MpluginAgent = _MpluginAgent;
        }
         //string _ReferenceJson
        public UpdateDbConnection(string _SubAdminId, string _ConnectionId, string _HostName, string _Database,  string _TimeOut, string _AdditinalString, string _MpluginAgent, long _UpdatedOn, string _CompanyId,string _Credential)
           
        {
            this.SubAdminId = _SubAdminId;
            this.HostName = _HostName;
            this.Database = _Database;
            this.ConnectionId = _ConnectionId;
            this.AdditinalString = _AdditinalString;
            this.TimeOut = _TimeOut;
            this.MpluginAgent = _MpluginAgent;
            this.UpdatedOn = _UpdatedOn;
            this.CompanyID = _CompanyId;
            this.Credential = _Credential;
        }

        public void Process()
        {
            try
            {
                this.StatusCode = -1000;

                string query = @"UPDATE TBL_DATABASE_CONNECTION SET 
                                            HOST_NAME=@HOST_NAME,
                                            DATABASE_NAME=@DATABASE_NAME,
                                            USER_ID=@USER_ID,
                                            PASSWORD=@PASSWORD,
                                            TIME_OUT=@TIME_OUT,
                                            ADDITIONAL_STRING=@ADDITIONAL_STRING,
                                            MPLUGIN_AGENT=@MPLUGIN_AGENT
                                WHERE DB_CONNECTOR_ID=@DB_CONNECTOR_ID AND SUBADMIN_ID=@SUBADMIN_ID";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@HOST_NAME", this.HostName);
                objSqlCommand.Parameters.AddWithValue("@DATABASE_NAME", this.Database);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
                objSqlCommand.Parameters.AddWithValue("@DB_CONNECTOR_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@TIME_OUT", this.TimeOut);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_STRING", this.AdditinalString);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
               
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
                else this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }

        public void Process1()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"UPDATE TBL_DATABASE_CONNECTION SET 
                                            HOST_NAME=@HOST_NAME,
                                            DATABASE_NAME=@DATABASE_NAME,
                                            TIME_OUT=@TIME_OUT,
                                            SUBADMIN_ID=@SUBADMIN_ID,
                                            UPDATED_ON=@UPDATED_ON,
                                             CREDENTIAL_PROPERTY=@CREDENTIAL_PROPERTY,
                                            ADDITIONAL_STRING=@ADDITIONAL_STRING,
                                            MPLUGIN_AGENT=@MPLUGIN_AGENT
                                       WHERE DB_CONNECTOR_ID=@DB_CONNECTOR_ID and COMPANY_ID=@COMPANY_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@HOST_NAME", this.HostName);
                objSqlCommand.Parameters.AddWithValue("@DATABASE_NAME", this.Database);
                objSqlCommand.Parameters.AddWithValue("@DB_CONNECTOR_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@TIME_OUT", this.TimeOut);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_STRING", this.AdditinalString);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_PROPERTY", this.Credential);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
                else this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

        public string SubAdminId
        {
            set;
            get;
        }
        public string Database
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string HostName
        {
            set;
            get;
        }
        public string ConnectionId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }

        public string AdditinalString { get; set; }

        public string TimeOut { get; set; }

        public bool UseMplugin { get; set; }

        public string MpluginAgent { get; set; }

        public string CompanyID { get; set; }

        public long UpdatedOn { get; set; }

        public string ReferenceJson { get; set; }
        public string Credential
        {
            set;
            get;
        }


    }
}