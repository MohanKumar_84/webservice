﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class UpdateGroupDetailsNameAndUsers
    {

        //update the name
        //add users in list
        //delete users provided in list

        public UpdateGroupDetailsNameAndUsers(string groupId, string groupName
            , DataTable userIdsToAdd, DataTable userIdsToRemove,
            string subAdminIdModifyingDtls, string companyId, string subAdminIdWhoCreatedGrp, string adduser, string removeuser)
        {
            GroupId = groupId;
            this.GroupName = groupName;
            this.UserIdsToAdd = userIdsToAdd;
            this.UserIdsToRemove = userIdsToRemove;
            this.SubAdminIdModifyingDtls = subAdminIdModifyingDtls;
            this.SubAdminIdWhoCreatedGrp = subAdminIdWhoCreatedGrp;
            this.CompanyId = companyId;
            this.Addnewuser = adduser;
            this.Removeuser = removeuser;
        }
        public void Process()
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        updateGroupName(con, transaction);
                        deleteAlreadyExistingUsers(con, transaction);
                        addUsersToGroup(con, transaction);
                        deleteUsersFromGroup(con, transaction);
                        addDetailsInMGramDatabase();
                        transaction.Commit();
                        StatusDescription = "";
                        StatusCode = 0;
                        if (this.Addnewuser != "")
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_USER_ADDED, this.CompanyId, this.SubAdminIdModifyingDtls, this.GroupId, this.GroupName, "", "", "", "", "", this.Addnewuser);
                        if (this.Removeuser != "")
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_USER_DELETED, this.CompanyId, this.SubAdminIdModifyingDtls, this.GroupId, this.GroupName, "", "", "", "", "", this.Addnewuser);
                    }
                }
            }
            catch (SqlException e)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
            catch (Exception e)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        void updateGroupName(SqlConnection con, SqlTransaction transaction)
        {
            string query = @"UPDATE TBL_USER_GROUP SET GROUP_NAME=@GROUP_NAME WHERE GROUP_ID =@GROUP_ID AND SUBADMIN_ID=@SUBADMIN_ID AND  COMPANY_ID=@COMPANY_ID;";

            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminIdWhoCreatedGrp);
            objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
            objSqlCommand.Parameters.AddWithValue("@GROUP_NAME", this.GroupName);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.ExecuteNonQuery();
        }
        void addUsersToGroup(SqlConnection con, SqlTransaction transaction)
        {
            foreach (DataRow row in UserIdsToAdd.Rows)
            {
                string query = @"INSERT INTO TBL_USER_GROUP_LINK(GROUP_ID,USER_ID,CREATED_ON,COMPANY_ID) VALUES(@GROUP_ID,@USER_ID,@CREATED_ON,@COMPANY_ID);";
                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", row["USER_ID"]);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();
            }
        }
        void deleteUsersFromGroup(SqlConnection con, SqlTransaction transaction)
        {
            foreach (DataRow row in UserIdsToRemove.Rows)
            {
                string query = @"DELETE FROM TBL_USER_GROUP_LINK WHERE GROUP_ID=@GROUP_ID AND USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", row["USER_ID"]);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();
            }
        }
        void deleteAlreadyExistingUsers(SqlConnection con, SqlTransaction transaction)
        {
            string query = @"DELETE FROM TBL_USER_GROUP_LINK WHERE GROUP_ID=@GROUP_ID AND  COMPANY_ID=@COMPANY_ID;";
            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.ExecuteNonQuery();
        }
        void addDetailsInMGramDatabase()
        {
            List<string> lstUserNames = new List<string>();
            foreach (DataRow row in this.UserIdsToAdd.Rows)
            {
                lstUserNames.Add(Convert.ToString(row["USER_NAME"]));
            }
            SaveGrpDtlsInMGramInsertUpdate objSaveDtlsInMGram =
                new SaveGrpDtlsInMGramInsertUpdate(this.CompanyId,
                    this.GroupName,
                    this.GroupId,
                    lstUserNames);
            objSaveDtlsInMGram.Process();
            if (objSaveDtlsInMGram.StatusCode != 0)
                throw new Exception("Error in saving in MGram");
        }
        public string GroupId
        {
            get;
            set;
        }
        public string GroupName
        {
            get;
            set;
        }
        public DataTable UserIdsToAdd
        {
            get;
            set;
        }
        public DataTable UserIdsToRemove
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public string SubAdminIdModifyingDtls
        {
            get;
            set;
        }
        public string SubAdminIdWhoCreatedGrp
        {
            get;
            set;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string Addnewuser
        {
            set;
            get;
        }
        public string Removeuser
        {
            set;
            get;
        }



    }
}