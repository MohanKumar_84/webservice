﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

namespace mFicientCP
{
    public class UpdateUserCompleteDetail
    {
        public UpdateUserCompleteDetail()
        {

        }
        //changed on 20-8-2012 byte _AllowDesktopMessenger,string _RequestedBy added. Mohan
        public UpdateUserCompleteDetail(string _Email, string _FirstName,
                           string _LastName, string _Mobile, string _CompanyId,
                             string _DateOfBirth, string _SubAdminId, string _RegionId, string _LocationId,
            string _DesignationId, string _EmployeeNo, string _DivisionId, Boolean _IsActive, byte _AllowMessenger, byte _AllowOfflineWork, string _UserId, byte _AllowDesktopMessenger
            , string _RequestedBy, MFICIENT_USER_TYPE userType, string domainId, List<string> userGroupIds, string userName, string _CustomPro, string _Authentication)
        {
            this.Email = _Email;
            this.FirstName = _FirstName;
            this.LastName = _LastName;
            this.Mobile = _Mobile;
            this.CompanyId = _CompanyId;
            this.DateOfBirth = _DateOfBirth;
            this.SubAdminId = _SubAdminId;
            this.RegionId = _RegionId == "-1" ? "" : _RegionId;
            this.LocationId = _LocationId == "-1" ? "" : _LocationId;
            this.DivisionId = _DivisionId == "-1" ? "" : _DivisionId;
            this.DesignationId = _DesignationId == "-1" ? "" : _DesignationId;
            this.EmployeeNo = _EmployeeNo;
            this.UserId = _UserId;
            this.IsActive = _IsActive;
            this.AllowMessenger = _AllowMessenger;
            this.AllowOfflineWork = _AllowOfflineWork;
            this.AllowDesktopWork = _AllowDesktopMessenger;
            this.RequestedBy = _RequestedBy;
            this.UserType = userType;
            this.DomainId = domainId;
            this.UserGroupIds = userGroupIds;
            this.UserName = userName;


            List<CredentialProperties> Crds = Utilities.DeserialiseJson<List<CredentialProperties>>(_Authentication);
            foreach (CredentialProperties Cr in Crds)
            {
                Cr.unm = AesEncryption.AESEncrypt(_CompanyId.ToUpper(), Cr.unm);
                if (!string.IsNullOrEmpty(Cr.pwd))
                    Cr.pwd = AesEncryption.AESEncrypt(_CompanyId.ToUpper(), Cr.pwd);
            }

            this.CedentiailProperties = Utilities.SerializeJson<List<CredentialProperties>>(Crds);
            this.Customproperties = _CustomPro;
        }

        public UpdateUserCompleteDetail(string _CompanyId, string _userId, Boolean _Istester)
        {
            this.CompanyId = _CompanyId;
            this.Istester = _Istester;
            this.UserId = _userId;
            ProcessTesterUser();
        }

        public void ProcessTesterUser()
        {

            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            try
            {

                StatusCode = -1000;
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"UPDATE dbo.TBL_USER_DETAIL SET IS_TESTER='0' WHERE COMPANY_ID=@COMPANY_ID";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }


                query = @"UPDATE dbo.TBL_USER_DETAIL SET IS_TESTER='1' WHERE COMPANY_ID=@COMPANY_ID AND USER_ID=@USER_ID";
                //    objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                query = @"DELETE FROM dbo.TBL_USER_GROUP_LINK WHERE COMPANY_ID=COMPANY_ID AND USER_ID=@USER_ID";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();
                if (objSqlCommand.ExecuteNonQuery() == 0 || objSqlCommand.ExecuteNonQuery() >= 1)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }
                objSqlTransaction.Commit();
                objSqlConnection.Close();
            }
            catch (Exception)
            {
                objSqlTransaction.Rollback();
                StatusCode = -1000;
            }

            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public void Process()
        {
            StatusCode = -1000;//For error
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            try
            {
                List<MFEMgramEnterpriseGroups> lstAllMgrmEntprsGrps = getMgrmEnterpriseGroup();
                List<MFEMgramEnterpriseGroups> lstExistingGrps;
                List<MFEMgramEnterpriseGroups> lstNonExistingGrpsINMgram;
                //Suppose this Group already exist and the user has removed this from the selected group
                //we have to remove the user name from existing user list
                List<MFEMgramEnterpriseGroups> lstOtherGroupsThatNeedsUpdate;

                compareIfUserGroupSelectedAlreadyExistInMGram(
                    lstAllMgrmEntprsGrps,
                    out lstExistingGrps,
                    out lstNonExistingGrpsINMgram,
                    out lstOtherGroupsThatNeedsUpdate);

                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"UPDATE TBL_USER_DETAIL SET 
                                            EMAIL_ID=@EMAIL_ID,
                                            FIRST_NAME=@FIRST_NAME,
                                            LAST_NAME=@LAST_NAME,
                                            MOBILE=@MOBILE,
                                            COMPANY_ID=@COMPANY_ID,
                                            DATE_OF_BIRTH=@DATE_OF_BIRTH,
                                            REGISTRATION_DATETIME=@REGISTRATION_DATETIME,
                                            REGION_ID=@REGION_ID,
                                            LOCATION_ID=@LOCATION_ID,
                                            DESIGNATION_ID=@DESIGNATION_ID,
                                            EMPLOYEE_NO=@EMPLOYEE_NO,
                                            IS_ACTIVE =@IS_ACTIVE,
                                            ALLOW_MESSENGER=@AllowMessenger,
                                            IS_OFFLINE_WORK=@IsOfflineWork,
                                            UPDATE_ON = @UpdatedOn,
                                            DESKTOP_MESSENGER =@DesktopMessenger,
                                            REQUESTED_BY  =@RequestedBy,
                                            SUBADMIN_ID=@SUBADMIN_ID,
                                            DOMAIN_ID = @DomainId,
                                            CUSTOM_PROPERTIES= @CUSTOM_PROPERTIES,
                                            CREDENTIAL_DETAIL = @CREDENTIAL_DETAIL          
                                WHERE
                                            USER_ID=@USER_ID 
                                            AND COMPANY_ID=@COMPANY_ID;";
                //AND SUBADMIN_ID=@SUBADMIN_ID
                objSqlTransaction = objSqlConnection.BeginTransaction();

                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", this.Email);
                objSqlCommand.Parameters.AddWithValue("@FIRST_NAME", this.FirstName);
                objSqlCommand.Parameters.AddWithValue("@LAST_NAME", this.LastName);
                objSqlCommand.Parameters.AddWithValue("@MOBILE", this.Mobile);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@DATE_OF_BIRTH", this.DateOfBirth);
                objSqlCommand.Parameters.AddWithValue("@REGISTRATION_DATETIME", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@REGION_ID", this.RegionId);
                objSqlCommand.Parameters.AddWithValue("@LOCATION_ID", this.LocationId);
                objSqlCommand.Parameters.AddWithValue("@DESIGNATION_ID", this.DesignationId);
                objSqlCommand.Parameters.AddWithValue("@EMPLOYEE_NO", this.EmployeeNo);
                objSqlCommand.Parameters.AddWithValue("@IS_ACTIVE", this.IsActive ? 1 : 0);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@AllowMessenger", this.AllowMessenger);
                objSqlCommand.Parameters.AddWithValue("@IsOfflineWork", this.AllowOfflineWork);
                objSqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@DesktopMessenger", this.AllowDesktopWork);
                objSqlCommand.Parameters.AddWithValue("@RequestedBy", this.RequestedBy);
                objSqlCommand.Parameters.AddWithValue("@DomainId", this.DomainId);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_DETAIL", this.CedentiailProperties);
                objSqlCommand.Parameters.AddWithValue("@CUSTOM_PROPERTIES", this.Customproperties);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }

                //query = @" DELETE FROM TBL_USER_DIVISION_DEP  WHERE USER_ID=@USER_ID AND SUBADMIN_ID=@SUBADMIN_ID AND  COMPANY_ID=@COMPANY_ID;";
                query = @" DELETE FROM TBL_USER_DIVISION_DEP  WHERE USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";

                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                // objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();

                string[] strArrayDivDepId = this.DivisionId.Split('&');
                foreach (string str in strArrayDivDepId)
                {
                    if (str.Length != 0)
                    {
                        string strDivId = str.Split('@')[1];
                        string strRelatedDepId = str.Split('@')[0];
                        string[] strArrayDepId = strRelatedDepId.Split('#');
                        foreach (string strDepId in strArrayDepId)
                        {
                            if (strDepId.Length != 0)
                            {
                                query = @"INSERT INTO TBL_USER_DIVISION_DEP(USER_ID,SUBADMIN_ID,DIVISION_ID,DEPARTMENT_ID,COMPANY_ID) VALUES(@USER_ID,@SUBADMIN_ID,@DIVISION_ID,@DEPARTMENT_ID,@COMPANY_ID);";
                                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                                objSqlCommand.Parameters.AddWithValue("@DIVISION_ID", strDivId);
                                objSqlCommand.Parameters.AddWithValue("@DEPARTMENT_ID", strDepId);
                                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                                objSqlCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
                //these process added on 4/9/2012
                int status = deletePreviousUpdateReqRecords(this.CompanyId, this.UserId, objSqlTransaction, objSqlConnection);
                if (status == -1000)
                {
                    throw new Exception();
                }
                else
                {
                    status = saveUpdateRequiredInfo(this.CompanyId, this.UserId, objSqlTransaction, objSqlConnection);
                    if (status == -1000)
                    {
                        throw new Exception();
                    }
                }
                deleteAllUserGroupLink(objSqlConnection, objSqlTransaction, this.UserGroupIds);
                insertUserGroupLinkDetails(objSqlConnection, objSqlTransaction);

                insertMgramGroups(lstExistingGrps,
                    lstNonExistingGrpsINMgram,
                    lstOtherGroupsThatNeedsUpdate);

                //SubAdminLog.saveSubAdminActivityLog("S", this.SubAdminId, this.UserId, this.CompanyId, SUBADMIN_ACTIVITY_LOG.MOBILE_USER_UPDATE, DateTime.UtcNow.Ticks, "", "", "", "", objSqlConnection, objSqlTransaction);
                objSqlTransaction.Commit();

                Utilities.saveActivityLog(objSqlConnection, mFicientCommonProcess.ACTIVITYENUM.USER_MODIFIED, this.CompanyId, this.SubAdminId, this.UserId, this.UserName, this.FirstName + ' ' + this.LastName, "", "", "", "", "");
                objSqlConnection.Close();
            }
            catch (Exception)
            {
                objSqlTransaction.Rollback();
                StatusCode = -1000;
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public static int deletePreviousUpdateReqRecords(string companyId, string userId, SqlTransaction transaction, SqlConnection con)
        {
            //int StatusCode = -1000,for error.
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE FROM TBL_UPDATION_REQUIRED_DETAIL WHERE COMPANY_ID = @CompanyId AND UPDATION_TYPE=@UpdateType AND USER_ID = @UserId", con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UpdateType", UPDATE_TYPE.USER_DETAILS_UPDATE);
                cmd.Parameters.AddWithValue("@CompanyId", companyId);
                cmd.Parameters.AddWithValue("@UserId", userId);
                return cmd.ExecuteNonQuery();
            }
            catch
            {
                return -1000;
            }
        }

        public static int saveUpdateRequiredInfo(string companyId, string userId, SqlTransaction transaction, SqlConnection con)
        {
            //int StatusCode = -1000,for error.
            try
            {
                SqlCommand cmd = new SqlCommand(@"INSERT INTO TBL_UPDATION_REQUIRED_DETAIL(COMPANY_ID,USER_ID,DEVICE_ID,DEVICE_TYPE,UPDATED_ON,UPDATION_TYPE)
                                                (
                                                SELECT usr.COMPANY_ID,usr.USER_ID,regDev.DEVICE_ID,regDev.DEVICE_TYPE,@UpdatedOn,@UpdateType FROM TBL_USER_DETAIL AS usr
                                                INNER JOIN TBL_REGISTERED_DEVICE AS regDev
                                                ON usr.USER_ID = regDEV.USER_ID
                                                WHERE usr.COMPANY_ID = @CompanyId
                                                AND usr.USER_ID =@UserId
                                                );", con, transaction);

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@UpdateType", UPDATE_TYPE.USER_DETAILS_UPDATE);
                cmd.Parameters.AddWithValue("@CompanyId", companyId);
                cmd.Parameters.AddWithValue("@UserId", userId);
                return cmd.ExecuteNonQuery();

            }
            catch
            {
                return -1000;
            }
        }
        #region Process Function
        void insertUserGroupLinkDetails(SqlConnection con, SqlTransaction transaction)
        {
            if (this.UserGroupIds != null)
            {
                List<MFEUserGroupLink> lstUserGrpLink = new List<MFEUserGroupLink>();
                foreach (string groupId in this.UserGroupIds)
                {
                    MFEUserGroupLink objUserGrpLink =
                        new MFEUserGroupLink();
                    objUserGrpLink.EnterpriseId = this.CompanyId;
                    objUserGrpLink.UserId = this.UserId;
                    objUserGrpLink.GroupId = groupId;
                    objUserGrpLink.CreatedOn = DateTime.UtcNow.Ticks;

                    lstUserGrpLink.Add(objUserGrpLink);
                }
                InsertUserGroupLink objInsertUsrGrpLink =
                        new InsertUserGroupLink();
                objInsertUsrGrpLink.Process(con, transaction, lstUserGrpLink);
            }
        }

        void insertMgramGroups(
            List<MFEMgramEnterpriseGroups> lstSelectedGrpsExistingInMgram,
            List<MFEMgramEnterpriseGroups> lstNonExistingGrpsInMgram,
            List<MFEMgramEnterpriseGroups> lstOtherGrpsThatNeedsUpdate
                )
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
                MSSqlClient.SqlConnectionOpen(out con, strConnectionString);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        if (lstSelectedGrpsExistingInMgram != null)
                        {
                            //Add this Username to existing list of user name in mGram Database
                            foreach (MFEMgramEnterpriseGroups mGramEnterPriseGrps in lstSelectedGrpsExistingInMgram)
                            {
                                //Check if user name already exist.If not then add.
                                if (!String.IsNullOrEmpty(mGramEnterPriseGrps.Users) &&
                                    !mGramEnterPriseGrps.Users.Contains(this.UserName))
                                {

                                    mGramEnterPriseGrps.Users = mGramEnterPriseGrps.Users + "," + this.UserName;
                                }
                            }
                            UpdateMgrmEnterpriseGroupsByGrpId objUpdateMgrmEntrprseGrps =
                                new UpdateMgrmEnterpriseGroupsByGrpId();
                            objUpdateMgrmEntrprseGrps.Process(con, transaction,
                                lstSelectedGrpsExistingInMgram
                                );
                        }
                        if (lstNonExistingGrpsInMgram != null)
                        {
                            InsertMgrmEnterpriseGroups objInsertMgrmEnterpriseGrps =
                                new InsertMgrmEnterpriseGroups();
                            objInsertMgrmEnterpriseGrps.Process(
                                con, transaction,
                                lstNonExistingGrpsInMgram
                                );
                        }
                        if (lstOtherGrpsThatNeedsUpdate != null)
                        {
                            UpdateMgrmEnterpriseGroupsByGrpId objUpdateMgrmEntrprseGrps =
                                new UpdateMgrmEnterpriseGroupsByGrpId();
                            objUpdateMgrmEntrprseGrps.Process(con, transaction,
                                lstOtherGrpsThatNeedsUpdate
                                );
                        }
                        transaction.Commit();
                    }
                }

            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }

        }

        void deleteAllUserGroupLink(SqlConnection con,
            SqlTransaction transaction,
            List<string> groupIds)
        {
            DeleteGroupandGroupUsers objDelUserGrpLink =
                new DeleteGroupandGroupUsers();
            objDelUserGrpLink.DeleteAllUsersGroupLink(con,
                    transaction,
                    this.UserId,
                    this.CompanyId
                    );
            if (objDelUserGrpLink.StatusCode != 0) throw new Exception();


        }
        #endregion

        #region Helper Functions also used in AddNewUserCompleteDetail
        List<MFEMgramEnterpriseGroups> getMgrmEnterpriseGroup()
        {
            GetAllMgrmEnterpriseGrps objMgrmEnterpriseGroups =
                new GetAllMgrmEnterpriseGrps(this.CompanyId);
            objMgrmEnterpriseGroups.Process();
            return objMgrmEnterpriseGroups.MgramEnterpriseGroups;
        }
        /// <summary>
        /// From All the Groups in Mgram table
        /// we have to remove user name if the group was deselected(which would requuire a update)
        /// if the Group is selected the we have to add the user name to existing users(getting groupsSelectedExistsInMgram for that)
        /// Suppose the user is updated after a new group was added but user are not added to the group yet.
        /// In that case if Group was selected then we hvar to add the group in Mgrm
        /// (getting groupsNotInMgram for the above purpose)
        /// </summary>
        /// <param name="mGramEnterpriseGroups"></param>
        /// <param name="groupsSelectedExistsInMgram"></param>
        /// <param name="groupsNotInMgram"></param>
        /// <param name="otherGroupsThatNeedsUpdate"></param>
        void compareIfUserGroupSelectedAlreadyExistInMGram(
            List<MFEMgramEnterpriseGroups> mGramEnterpriseGroups
            , out List<MFEMgramEnterpriseGroups> groupsSelectedExistsInMgram
            , out List<MFEMgramEnterpriseGroups> groupsNotInMgram
            , out List<MFEMgramEnterpriseGroups> otherGroupsThatNeedsUpdate)
        {
            groupsSelectedExistsInMgram = new List<MFEMgramEnterpriseGroups>();
            groupsNotInMgram = new List<MFEMgramEnterpriseGroups>();
            otherGroupsThatNeedsUpdate = new List<MFEMgramEnterpriseGroups>();
            List<MFEGroupDetail> lstAllGroupsDtl = getGroupDtlbyCompany();
            foreach (string groupId in this.UserGroupIds)
            {
                bool blnExists = false;
                MFEMgramEnterpriseGroups objMgramEnterpriseGroup =
                        new MFEMgramEnterpriseGroups();
                foreach (MFEMgramEnterpriseGroups mgramEnterpriseGroup in mGramEnterpriseGroups)
                {
                    if (mgramEnterpriseGroup.GroupId == groupId)
                    {
                        blnExists = true;
                        objMgramEnterpriseGroup = mgramEnterpriseGroup;
                        break;
                    }
                }
                if (blnExists)
                {
                    groupsSelectedExistsInMgram.Add(objMgramEnterpriseGroup);
                }
                else
                {
                    objMgramEnterpriseGroup = new MFEMgramEnterpriseGroups();
                    foreach (MFEGroupDetail grpDtl in lstAllGroupsDtl)
                    {
                        if (grpDtl.GroupId == groupId)
                        {
                            objMgramEnterpriseGroup.EnterpriseId = grpDtl.EnterpriseId;
                            objMgramEnterpriseGroup.GroupId = grpDtl.GroupId;
                            objMgramEnterpriseGroup.GroupName = grpDtl.GroupName;
                            objMgramEnterpriseGroup.Users = this.UserName;
                            break;
                        }
                    }

                    groupsNotInMgram.Add(objMgramEnterpriseGroup);
                }
            }
            //Get Other Groups that needs Update
            foreach (MFEMgramEnterpriseGroups mgramEntprsGrp in mGramEnterpriseGroups)
            {
                bool isGrpSelected = false;
                foreach (MFEMgramEnterpriseGroups mgramEntprsGrpSelected in groupsSelectedExistsInMgram)
                {
                    if (mgramEntprsGrpSelected.GroupId == mgramEntprsGrp.GroupId)
                    {
                        isGrpSelected = true;
                        break;
                    }
                }
                if (!isGrpSelected)
                {
                    if (!String.IsNullOrEmpty(mgramEntprsGrp.Users))
                    {
                        if (mgramEntprsGrp.Users.Contains(this.UserName))
                        {
                            otherGroupsThatNeedsUpdate.Add(mgramEntprsGrp);

                            string[] aryUserNames = mgramEntprsGrp.Users.Split(',');
                            string strNewUserNameList = String.Empty;
                            if (aryUserNames.Length > 0)
                            {
                                for (int i = 0; i <= aryUserNames.Length - 1; i++)
                                {
                                    if (aryUserNames[i].ToLower().Trim() ==
                                        this.UserName.ToLower().Trim())//Don't add the user name
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(strNewUserNameList))
                                        {
                                            strNewUserNameList += aryUserNames[i];
                                        }
                                        else
                                        {
                                            strNewUserNameList += "," + aryUserNames[i];
                                        }
                                    }
                                }
                                mgramEntprsGrp.Users = strNewUserNameList;
                            }
                        }

                    }

                }
                //end adding OtherGropusThat needs update
            }
        }
        List<MFEGroupDetail> getGroupDtlbyCompany()
        {
            GetGroupDetails objGrpDtls =
                new GetGroupDetails(this.CompanyId);
            objGrpDtls.Process();
            if (objGrpDtls.StatusCode == 0)
            {
                return objGrpDtls.GroupList;
            }
            else
            {
                throw new Exception();
            }
        }
        #endregion
        public Boolean IsActive
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string UserId
        {
            get;
            set;
        }
        public string SubAdminId
        {
            get;
            set;
        }
        public string RegionId
        {
            get;
            set;
        }
        public string DivisionId
        {
            get;
            set;
        }
        public string LocationId
        {
            get;
            set;
        }
        public string DesignationId
        {
            get;
            set;
        }
        public string EmployeeNo
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string AccessCode
        {
            get;
            set;
        }
        public string FirstName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        public string Gender
        {
            get;
            set;
        }
        public string Mobile
        {
            get;
            set;

        }
        public string CompanyId
        {
            get;
            set;
        }
        public string DateOfBirth
        {
            get;
            set;
        }
        public byte AllowMessenger
        {
            get;
            set;
        }
        public byte AllowOfflineWork
        {
            get;
            set;
        }

        public string RequestedBy
        {
            get;
            set;
        }
        public byte AllowDesktopWork
        {
            get;
            set;
        }
        public MFICIENT_USER_TYPE UserType
        {
            get;
            private set;
        }
        public string DomainId
        {
            get;
            private set;
        }
        public List<string> UserGroupIds
        {
            get;
            private set;
        }
        public string Customproperties
        {
            get;
            private set;
        }
        public string CedentiailProperties
        {
            get;
            private set;
        }

        public Boolean Istester
        {
            get;
            set;
        }
    }
}