﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class UpdateWFSubAdminId
    {
        string _appId, _companyId, _newSubAdminId, _oldSubAdminId;
        bool _isUpdateAll;

        public UpdateWFSubAdminId(bool isUpdateAll,string companyId, string newSubAdminId,
           List<MFEWorkFlowDetail>appIdsToUpdate)
        {
            this.CompanyId = companyId;
            this.NewSubAdminId = newSubAdminId;
            AppIdsToUpdate = appIdsToUpdate;
            this.IsUpdateAll = isUpdateAll;
        }
        public void ProcessForMultipleAppIds()
        {
            this.StatusCode = -1000;
            try
            {
                SqlTransaction transaction = null;
                SqlConnection con;
                MSSqlClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            if (AppIdsToUpdate.Count > 0)
                            {
                                foreach (MFEWorkFlowDetail wfDetail in AppIdsToUpdate)
                                {
                                    updateTblCurrWFDetail(this.IsUpdateAll, wfDetail.WorkflowId,
                                       this.CompanyId, this.NewSubAdminId, wfDetail.SubAdminId, con, transaction);

                                    updateTblPrevWFDetail(this.IsUpdateAll, wfDetail.WorkflowId,
                                       this.CompanyId, this.NewSubAdminId, wfDetail.SubAdminId, con, transaction);

                                    updateTblWorkFlowDetail(this.IsUpdateAll, wfDetail.WorkflowId,
                                       this.CompanyId, this.NewSubAdminId, wfDetail.SubAdminId, con, transaction);
                                }
                            }
                            
                            transaction.Commit();
                            this.StatusCode = 0;
                            this.StatusDescription = String.Empty;

                        }
                    }

                }
                catch (SqlException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

       

        void updateTblCurrWFDetail(bool updateAll, string wfId,
                               string companyId, string newSubAdminId, string oldSubAdminId,
                               SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = String.Empty;
            if (updateAll)
            {
                strQuery = @"UPDATE TBL_CURR_WORKFLOW_DETAIL
                            SET SUBADMIN_ID = @NewSubAdminId
                            WHERE COMPANY_ID = @CompanyId
                            AND SUBADMIN_ID = @OldSubAdminId;";
            }
            else
            {
                strQuery = @"UPDATE TBL_CURR_WORKFLOW_DETAIL
                            SET SUBADMIN_ID = @NewSubAdminId
                            WHERE COMPANY_ID = @CompanyId
                            AND SUBADMIN_ID = @OldSubAdminId
                            AND WF_ID = @WFID;";
            }

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@NewSubAdminId", newSubAdminId);
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            cmd.Parameters.AddWithValue("@OldSubAdminId", oldSubAdminId);
            if (!updateAll)
                cmd.Parameters.AddWithValue("@WFID", wfId);

            cmd.ExecuteNonQuery();
        }

        void updateTblPrevWFDetail(bool updateAll, string wfId,
                               string companyId, string newSubAdminId, string oldSubAdminId,
                               SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = String.Empty;
            if (updateAll)
            {
                strQuery = @"UPDATE TBL_PRV_WORKFLOW_DETAIL
                            SET SUBADMIN_ID = @NewSubAdminId
                            WHERE COMPANY_ID = @CompanyId
                            AND SUBADMIN_ID = @OldSubAdminId;";
            }
            else
            {
                strQuery = @"UPDATE TBL_PRV_WORKFLOW_DETAIL
                            SET SUBADMIN_ID = @NewSubAdminId
                            WHERE COMPANY_ID = @CompanyId
                            AND SUBADMIN_ID = @OldSubAdminId
                            AND WF_ID = @WFID;";
            }

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@NewSubAdminId", newSubAdminId);
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            cmd.Parameters.AddWithValue("@OldSubAdminId", oldSubAdminId);
            if (!updateAll)
                cmd.Parameters.AddWithValue("@WFID", wfId);

            cmd.ExecuteNonQuery();
        }

        void updateTblWorkFlowDetail(bool updateAll, string wfId,
                               string companyId, string newSubAdminId, string oldSubAdminId,
                               SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = String.Empty;
            if (updateAll)
            {
                strQuery = @"UPDATE TBL_WORKFLOW_DETAIL
                            SET SUBADMIN_ID = @NewSubAdminId
                            WHERE COMPANY_ID = @CompanyId
                            AND SUBADMIN_ID = @OldSubAdminId;";
            }
            else
            {
                strQuery = @"UPDATE TBL_WORKFLOW_DETAIL
                            SET SUBADMIN_ID = @NewSubAdminId
                            WHERE COMPANY_ID = @CompanyId
                            AND SUBADMIN_ID = @OldSubAdminId
                            AND WF_ID = @WFID;";
            }

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@NewSubAdminId", newSubAdminId);
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            cmd.Parameters.AddWithValue("@OldSubAdminId", oldSubAdminId);
            if (!updateAll)
                cmd.Parameters.AddWithValue("@WFID", wfId);

            cmd.ExecuteNonQuery();
        }



        public string OldSubAdminId
        {
            get { return _oldSubAdminId; }
            private set { _oldSubAdminId = value; }
        }

        public string NewSubAdminId
        {
            get { return _newSubAdminId; }
            private set { _newSubAdminId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }

        public string AppId
        {
            get { return _appId; }
            private set { _appId = value; }
        }


        public bool IsUpdateAll
        {
            get { return _isUpdateAll; }
            private set { _isUpdateAll = value; }
        }
        public int StatusCode
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public List<MFEWorkFlowDetail> AppIdsToUpdate
        {
            get;
            private set;
        }
    }
}