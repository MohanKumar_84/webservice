﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections;
using System.Web.Caching;
using Newtonsoft.Json.Linq;
namespace mFicientCP
{
    public class Utilities
    {
        public static Hashtable hstHtml5Apps = new Hashtable();
        public static Byte[] currentZipFile = new Byte[0];
        #region Fields Validation
        public static bool IsValidEmail(string _email)
        {
            if (!string.IsNullOrEmpty(_email))
            {
                string strRegex = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                        [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                            + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                        [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_email))
                    return (true);
                else
                    return (false);
            }
            else
                return false;
        }

        public static bool IsValidString(string _InputString, bool _a_z, bool _A_Z, bool _0_9, string _SpecialChars, int _MinLength, int _MaxLength, bool _StartSpaceAllowed, bool _EndSpaceAllowed)
        {
            try
            {
                if ((!_StartSpaceAllowed && _InputString.StartsWith(" ")) || (!_EndSpaceAllowed && _InputString.EndsWith(" ")))
                {
                    return false;
                }

                if ((_MaxLength < _MinLength) || (_MinLength >= 0 && _InputString.Length < _MinLength) || (_MaxLength >= 0 && _InputString.Length > _MaxLength))
                {
                    return false;
                }

                /* Create a regex expression based on _a-z and _A_Z and _0_9 and allowedCharacters */

                if (string.IsNullOrEmpty(_InputString))
                {
                    return true;
                }

                string strRegex = "";

                if (_a_z)
                {
                    strRegex += "a-z";
                }

                if (_A_Z)
                {
                    strRegex += "A-Z";
                }

                if (_0_9)
                {
                    strRegex += "0-9";
                }

                string strSpecialCharacters = _SpecialChars.Replace(@"\", @"\\");

                if (strSpecialCharacters.Contains("]"))
                {
                    strRegex = "]" + strRegex;
                    strSpecialCharacters = strSpecialCharacters.Replace("]", "");
                }

                if (strSpecialCharacters.Contains("^"))
                {
                    strRegex += "^";
                    strSpecialCharacters = strSpecialCharacters.Replace("^", "");
                }

                if (strSpecialCharacters.Contains("-"))
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters.Replace("-", "") + "-" + "]+$";
                }
                else
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters + "]+$";
                }

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_InputString))
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        public static bool isValidUserName(string userName,int width,bool checkForActiveDierectoryUser)
        {
            if (String.IsNullOrEmpty(userName))
            {
                return false;
            }
            if (checkForActiveDierectoryUser)
            {
                return checkForActvDirUserNameValidChars(userName);
            }
            else
            {
                string strFirstCharacter = userName.Substring(0, 1);
                bool blnUserNmBeginWithNmOrAllowedSymbol = false;
                blnUserNmBeginWithNmOrAllowedSymbol = checkCloudUserBeginWithNumberOrAllowedSymbol(userName);
                if (blnUserNmBeginWithNmOrAllowedSymbol)
                {
                    return false;
                }
                else
                {
                    return IsValidString(userName, true, true, true, @"!#$%^*()@_.-", 1, width, false, false);
                }
            }
        }
        private static bool checkForActvDirUserNameValidChars(string userName)
        {
            //Invalid characters "/\[]:;|=,+*?<>
            bool isValid = true;
            if (String.IsNullOrEmpty(userName))
            {
                isValid = false;
            }
            else
            {
                char[] aryInvalidCharacters = getInvalidCharsForActDirUsrNm();
                foreach (char charToChek in aryInvalidCharacters)
                {
                    if (userName.Contains(charToChek))
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            return isValid;
        }
        private static char[] getInvalidCharsForActDirUsrNm()
        {
            return new char[15] { '"', '/', '\\', '[', ']', ':', ';', '|', '=', ',', '+', '*', '?', '<', '>' };
        }
        private static bool checkCloudUserBeginWithNumberOrAllowedSymbol(string userName)
        {
            bool isValid = true;
            if (String.IsNullOrEmpty(userName))
            {
                isValid = false;
            }
            else
            {
                string strFirstCharacter = userName.Substring(0, 1);
                bool blnFirstCharacterIsInt = true;
                int iParseResult;
                blnFirstCharacterIsInt = int.TryParse(strFirstCharacter, out iParseResult);
                if (blnFirstCharacterIsInt)
                {
                    isValid = false;
                }
                else
                {
                    isValid = isFirstCharOfCldUsernameAnyValidChars(Convert.ToChar(strFirstCharacter));
                }
            }
            return isValid;
        }
        private static bool isFirstCharOfCldUsernameAnyValidChars(char charToCheck)
        {
            bool result = false;
            char[] validChars = getValidCharsForCldUsrNm();
            foreach (char chr in validChars)
            {
                if (charToCheck == chr)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
        private static char[] getValidCharsForCldUsrNm()
        {
            //!#$%^*()_-
            return new char[10] { '!', '#', '$', '%', '^', '*', '(', ')', '_', '-' };
        }
        public static bool IsValidDateOfBirth(string _Dob)
        {
            if (!string.IsNullOrEmpty(_Dob))
            {
                try
                {
                    DateTime dt = new DateTime(int.Parse(_Dob.Substring(0, 4)), int.Parse(_Dob.Substring(4, 2)), int.Parse(_Dob.Substring(6, 2)));
                    if (dt < DateTime.UtcNow.Date)
                    {
                        return true;
                    }
                }
                catch
                {
                }
            }
            return false;
        }
        #endregion

        #region common Funtion use in cp
        //public static void BindRegionDropDown(DropDownList _ddlDropdownId, string Company_id)
        //{
        //    GetRegion getregion = new GetRegion(true, "", Company_id);
        //    getregion.Process();
        //    DataTable dt = getregion.ResultTable;
        //    if (dt != null)
        //    {
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "REGION_ID";
        //        _ddlDropdownId.DataTextField = "REGION_NAME";
        //        _ddlDropdownId.DataBind();
        //        _ddlDropdownId.Items.Insert(0, new ListItem("Select", "-1"));
        //    }
        //    else
        //    {
        //        dt = new DataTable();
        //        DataColumn dc1 = new DataColumn("REGION_ID");
        //        DataColumn dc2 = new DataColumn("REGION_NAME");
        //        dt.Columns.Add(dc1);
        //        dt.Columns.Add(dc2);
        //        dt.Rows.Add("", "Region not available");
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "REGION_ID";
        //        _ddlDropdownId.DataTextField = "REGION_NAME";
        //        _ddlDropdownId.DataBind();
        //    }
        //}

        //public static void BindLocationDropDown(DropDownList _ddlDropdownId, string _RegionId, string _CompanyId)
        //{
        //    GetLocations getlocations = new GetLocations(true, _RegionId, "", _CompanyId);
        //    getlocations.Process();
        //    DataTable dt = getlocations.ResultTable;
        //    if (dt != null)
        //    {
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "LOCATION_ID";
        //        _ddlDropdownId.DataTextField = "LOCATION_NAME";
        //        _ddlDropdownId.DataBind();
        //        _ddlDropdownId.Items.Insert(0, new ListItem("Select", "-1"));
        //    }
        //    else
        //    {
        //        dt = new DataTable();
        //        DataColumn dc1 = new DataColumn("LOCATION_ID");
        //        DataColumn dc2 = new DataColumn("LOCATION_NAME");
        //        dt.Columns.Add(dc1);
        //        dt.Columns.Add(dc2);
        //        dt.Rows.Add("", "Location not available");
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "LOCATION_ID";
        //        _ddlDropdownId.DataTextField = "LOCATION_NAME";
        //        _ddlDropdownId.DataBind();
        //    }
        //}

        //public static void BindDivisionDropDown(DropDownList _ddlDropdownId, string _AdminId, string _CompanyId)
        //{
        //    GetDivision getdivision = new GetDivision(true, _AdminId, "", _CompanyId);
        //    getdivision.Process();
        //    DataTable dt = getdivision.ResultTable;
        //    if (dt != null)
        //    {
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "DIVISION_ID";
        //        _ddlDropdownId.DataTextField = "DIVISION_NAME";
        //        _ddlDropdownId.DataBind();
        //        _ddlDropdownId.Items.Insert(0, new ListItem("Select Division", "-1"));
        //    }
        //    else
        //    {
        //        dt = new DataTable();
        //        DataColumn dc1 = new DataColumn("DIVISION_ID");
        //        DataColumn dc2 = new DataColumn("DIVISION_NAME");
        //        dt.Columns.Add(dc1);
        //        dt.Columns.Add(dc2);
        //        dt.Rows.Add("", "Division not available");
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "DIVISION_ID";
        //        _ddlDropdownId.DataTextField = "DIVISION_NAME";
        //        _ddlDropdownId.DataBind();
        //    }
        //}

        //public static void BindDesignationDropDown(DropDownList _ddlDropdownId, string _AdminId, string _CompanyID)
        //{
        //    GetDesignation getdesignation = new GetDesignation(true, _AdminId, "", _CompanyID);
        //    getdesignation.Process();
        //    DataTable dt = getdesignation.ResultTable;
        //    if (dt != null)
        //    {
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "DESIGNATION_ID";
        //        _ddlDropdownId.DataTextField = "DESIGNATION_NAME";
        //        _ddlDropdownId.DataBind();
        //        _ddlDropdownId.Items.Insert(0, new ListItem("Select", "-1"));
        //    }
        //    else
        //    {
        //        dt = new DataTable();
        //        DataColumn dc1 = new DataColumn("DESIGNATION_ID");
        //        DataColumn dc2 = new DataColumn("DESIGNATION_NAME");
        //        dt.Columns.Add(dc1);
        //        dt.Columns.Add(dc2);
        //        dt.Rows.Add("", "Designation not available");
        //        _ddlDropdownId.DataSource = dt;
        //        _ddlDropdownId.DataValueField = "DESIGNATION_ID";
        //        _ddlDropdownId.DataTextField = "DESIGNATION_NAME";
        //        _ddlDropdownId.DataBind();
        //    }
        //}

        public static string getBrowserWindowUniqueId()
        {
            return GetMd5Hash((Guid.NewGuid().ToString() + DateTime.UtcNow.Ticks).ToString());
        }
        public static bool AuthenticateUserSessionFromCache(string userId, string adminId, string companyId, string sessionId, string deviceId, System.Web.Caching.Cache cache, out bool deviceIdExistsInCookie, out bool cookieExists)
        {
            deviceIdExistsInCookie = false;
            string strBrowserId = "";
            cookieExists = false;
            string strDeviceId = deviceId;

            HttpCookie msidCookie = HttpContext.Current.Request.Cookies[MficientConstants.COOKIE_MSID];

            if (msidCookie != null)
            {
                string msidCookieValue = HttpUtility.UrlDecode(msidCookie.Value);

                if (!string.IsNullOrEmpty(msidCookieValue))
                {
                    cookieExists = true;
                    string[] aryCookieValue = Convert.ToString(msidCookieValue).Split('|');
                    strBrowserId = aryCookieValue.Length > 1 ? aryCookieValue[1] : "";
                    if (aryCookieValue.Length > 2)
                    {
                        //value stored as UserId;SessionId+"|"+strDeviceId+("|"+strDeviceId(if different browser window))
                        for (int i = 2; i <= aryCookieValue.Length - 1; i++)
                        {
                            if (strDeviceId == aryCookieValue[i])
                            {
                                deviceIdExistsInCookie = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(strBrowserId))
            {
                SessionUser objSession = null;
                string cacheKey = CacheManager.GetKey(CacheManager.CacheType.mFicientSession, companyId, userId, strBrowserId, MficientConstants.WEB_DEVICE_TYPE_AND_ID, sessionId, "");
                try
                {
                    objSession = CacheManager.Get<SessionUser>(cacheKey);


                    if (objSession == null)
                        return false;

                    try
                    {
                        if (objSession.CheckSessionAndUpdateLastActivity())
                        {
                            return true;
                        }

                        return false;
                    }
                    catch (UnauthorizedAccessException uex)
                    {
                        objSession = null;
                        return false;
                    }
                }
                catch 
                {
                    if(cacheKey!=null)
                        cache.Remove(cacheKey);
                    objSession = null;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool SendInfoEmail(string _Email, string _EmailBody, string _EmailSubject)
        {
            try
            {
                string strPostDateTime = DateTime.UtcNow.Ticks.ToString();
                string strEmailID = Utilities.GetMd5Hash(_Email + strPostDateTime);
                SqlCommand objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_EMAIL_OUTBOX(EMAIL_ID,EMAIL,SUBJECT,BODY,POST_TIME,SEND_TIME)
                            VALUES(@EMAIL_ID,@EMAIL,@SUBJECT,@BODY,@POST_TIME,@SEND_TIME)");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", strEmailID);
                objSqlCommand.Parameters.AddWithValue("@EMAIL", _Email);
                objSqlCommand.Parameters.AddWithValue("@SUBJECT", _EmailSubject);
                objSqlCommand.Parameters.AddWithValue("@POST_TIME", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@BODY", _EmailBody);
                objSqlCommand.Parameters.AddWithValue("@SEND_TIME", 0);
                int intResult = MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                if (intResult > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendInfoEmail(string _EmailIdOfTheUser, string _EmailSubject, string templateName, Hashtable parametersToReplace, System.Web.Caching.Cache cache)
        {
            try
            {
                string strPostDateTime = DateTime.UtcNow.Ticks.ToString();
                string strEmailBody = "";
                string strEmailDBUniqueID = Utilities.GetMd5Hash(_EmailIdOfTheUser + strPostDateTime);
                FileInfo fileEmailTemplate = null;
                if (cache != null)
                {
                    if (cache[templateName] != null)
                    {
                        fileEmailTemplate = (FileInfo)cache[templateName];
                    }
                    else
                    {
                        fileEmailTemplate = new FileInfo(@"~/EmailTemplates/" + templateName + ".txt");
                        cache.Add(templateName, fileEmailTemplate, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, MficientConstants.EMAIL_TEMPLATE_CACHE_SECONDS), CacheItemPriority.NotRemovable, null);
                        //cache[templateName] = fileEmailTemplate;
                    }

                }
                if (fileEmailTemplate != null)
                {
                    Hashtable hashKeyPositions = new Hashtable();
                    using (StreamReader sreader = fileEmailTemplate.OpenText())
                    {
                        // Use the StreamReader object...
                        string strfileContent = sreader.ReadToEnd();
                        //find the index of the parameters to replace
                        foreach (DictionaryEntry hashEntry in parametersToReplace)
                        {
                            int iIndexOf = strfileContent.IndexOf("<%" + hashEntry.Key + "%>");
                            EmailTemplateParameterNameValue objParameterNameValue = new EmailTemplateParameterNameValue();
                            objParameterNameValue.ParameterName = "<%" + hashEntry.Key + "%>";
                            objParameterNameValue.ParameterValue = Convert.ToString(hashEntry.Value);
                            hashKeyPositions.Add(iIndexOf, objParameterNameValue);//complete tag in the html text=<%UserName%>
                        }
                        //insert the value of the parameters at the particular position.and remove the parameter in the string
                        foreach (DictionaryEntry hashEntry in hashKeyPositions)
                        {
                            EmailTemplateParameterNameValue parameterNameValue = (EmailTemplateParameterNameValue)hashEntry.Value;
                            strfileContent = strfileContent.Insert(Convert.ToInt32(hashEntry.Key), Convert.ToString(parameterNameValue.ParameterValue));
                            strfileContent = strfileContent.Remove(Convert.ToInt32(hashEntry.Key) + parameterNameValue.ParameterValue.Length, parameterNameValue.ParameterName.Length);
                        }

                        //if for some reason some of the parameters are not replaced then find that pattern and replace it with empty string.
                        //string strPattern = @"\b<%[A-Za-z0-9 ]%>\b";
                        string strPattern = @"<%[A-Za-z0-9 ]*%>";
                        bool isLoopCompleted = false;
                        do
                        {
                            foreach (DictionaryEntry hashEntry in hashKeyPositions)
                            {
                                MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(strfileContent, strPattern);
                                EmailTemplateParameterNameValue parameterNameValue = (EmailTemplateParameterNameValue)hashEntry.Value;
                                foreach (Match match in matches)
                                {
                                    if (parameterNameValue.ParameterName != match.Value)
                                    {
                                        strfileContent = strfileContent.Remove(match.Index, match.Value.Length);//because when thee characters are deleted then others position changes.
                                        break;
                                    }
                                }
                            }
                            int iCountOfMatchesAfterReplacing = 0;
                            foreach (DictionaryEntry hashEntry in hashKeyPositions)
                            {
                                MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(strfileContent, strPattern);
                                iCountOfMatchesAfterReplacing = iCountOfMatchesAfterReplacing + matches.Count;
                            }
                            if (iCountOfMatchesAfterReplacing > 0)
                            {
                                isLoopCompleted = false;
                            }
                            else
                            {
                                isLoopCompleted = true;
                            }
                        } while (isLoopCompleted == false);
                        strEmailBody = strfileContent;
                    }
                    SqlCommand objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_EMAIL_OUTBOX(EMAIL_ID,EMAIL,SUBJECT,BODY,POST_TIME,SEND_TIME)
                            VALUES(@EMAIL_ID,@EMAIL,@SUBJECT,@BODY,@POST_TIME,@SEND_TIME)");
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", strEmailDBUniqueID);
                    objSqlCommand.Parameters.AddWithValue("@EMAIL", _EmailIdOfTheUser);
                    objSqlCommand.Parameters.AddWithValue("@SUBJECT", _EmailSubject);
                    objSqlCommand.Parameters.AddWithValue("@POST_TIME", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@BODY", strEmailBody);
                    objSqlCommand.Parameters.AddWithValue("@SEND_TIME", 0);
                    int intResult = MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                    if (intResult > 0)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Json Serialization Deserialization

        public static string getFinalJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"resp\":" + jsonWithDetails + "}";
            return strFinalJson;
        }

        public static string SerializeJson<T>(object obj)
        {
            T objRequest = (T)obj;
            MemoryStream stream = new MemoryStream();
            StreamReader streamReader = null;
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objRequest.GetType());
                serializer.WriteObject(stream, objRequest);
                stream.Position = 0;
                streamReader = new StreamReader(stream);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            if(streamReader!=null)
                return streamReader.ReadToEnd();
            else return "";
        }

        public static string getRequestJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"req\":" + jsonWithDetails + "}";
            return strFinalJson;
        }

        public static T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            if (!string.IsNullOrEmpty(json))
            {
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
                obj = (T)serialiser.ReadObject(ms);
                ms.Close();
            }
            return obj;
        }

        public static string GetMsgRefID()
        {
            Random random = new Random();
            string strMsgRefID = random.Next(989, 9999999).ToString() + DateTime.Now.Ticks.ToString();
            return strMsgRefID;
        }
        public static string getFinalJsonForAutoCompleteTextBox(string json)
        {
            return "[" + json.Split('[')[1].Split(']')[0] + "]";
        }
        #endregion

        #region Soap protocal definition
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="action">the target namespace of the schema</param>
        /// <param name="functionName"></param>
        /// <param name="functionNamespace"></param>
        /// <param name="inputParameters"></param>
        /// <returns></returns>
        public static string getSoapRequest(string url, string action, string functionName, string functionNamespace, string[] inputParameters)
        {
            string strSoapEnvelope =
                        @"<soap:Envelope
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                            xmlns:xsd='http://www.w3.org/2001/XMLSchema'
                            xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
                        <soap:Body></soap:Body></soap:Envelope>";
            return createCompleteSoapRequest(getBodyOfSoapRequest(functionName, functionNamespace, inputParameters), strSoapEnvelope).ToString();
        }

        static string getBodyOfSoapRequest(string functionName, string functionNamespace, string[] inputParameters)
        {
            string strBodyXML = "<" + functionName + " " + "xmlns=\"" + functionNamespace + "\"" + ">";
            for (int i = 0; i < inputParameters.Length; i++)
            {
                strBodyXML += "<" + inputParameters[i] + ">" + "@@" + inputParameters[i] + "@@" + "</" + inputParameters[i] + ">";
            }
            strBodyXML += "</" + functionName + ">";
            return strBodyXML;
        }

        private static XmlDocument createCompleteSoapRequest(string content, string soapEnvelope)
        {
            StringBuilder sb = new StringBuilder(soapEnvelope);
            sb.Insert(sb.ToString().IndexOf("</soap:Body>"), content);

            // create an empty soap envelope
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(sb.ToString());

            return soapEnvelopeXml;
        }
        #endregion

        #region ModelpopUps Functions
        public static int getModalPopUpWidth(MODAL_POP_UP_NAME name)
        {
            int iWidth = 0;
            switch (name)
            {
                case MODAL_POP_UP_NAME.USER_DETAIL:
                    iWidth = 650;
                    break;
                case MODAL_POP_UP_NAME.DEVICE_DETAILS_REPEATER:
                    iWidth = 600;
                    break;
                case MODAL_POP_UP_NAME.SUB_ADMIN_DETAILS:
                    iWidth = 400;
                    break;
                case MODAL_POP_UP_NAME.MANAGE_GROUP_POPUPS:
                    iWidth = 300;
                    break;
                case MODAL_POP_UP_NAME.POST_BACK_CONFIRMATION:
                    iWidth = 320;
                    break;
                case MODAL_POP_UP_NAME.CHANGE_PASSWORD:
                    iWidth = 320;
                    break;
            }
            return iWidth;
        }

        public static void closeModalPopUp(string divIdToClose, System.Web.UI.UpdatePanel updPanel)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "CloseModal", @"$('#" + divIdToClose + "').dialog('close')", true);
        }
        public static void closeModalPopUp(string divIdToClose, System.Web.UI.UpdatePanel updPanel, string keyForScript)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"$('#" + divIdToClose + "').dialog('close')", true);
        }
        public static void closeModalPopUp(string divIdToClose, System.Web.UI.UpdatePanel updPanel, string keyForScript, string divForRemovingImg)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"$('#" + divIdToClose + "').dialog('close');removeDialogImage(" + "'" + divForRemovingImg + "'" + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.UpdatePanel updPanel, DIALOG_TYPE dialogType)
        {
            if (updPanel == null) return;
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "Message", @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.UpdatePanel updPanel, string keyForScript, DIALOG_TYPE dialogType)
        {
            if (updPanel == null) return;
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        public static void showMessage(string message, System.Web.UI.Page page, string keyForScript, DIALOG_TYPE dialogType)
        {
            if (page == null) return;
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), keyForScript, @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        /// <summary>
        /// The pop up is not resizable
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="headerText"></param>
        /// <param name="width">Pass empty string for default width</param>
        /// <param name="showCloseButton"></param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string headerText, string width, bool showCloseButton)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (showCloseButton)
            {
                //ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + "false" + ")", true);
                ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + "false" + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + "false" + ")", true);
            }
        }

        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string headerText, string width, bool showCloseButton, bool isResizable)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="keyForScript"></param>
        /// <param name="headerText"></param>
        /// <param name="width">Pass empty string for default width</param>
        /// <param name="showCloseButton"></param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string keyForScript, string headerText, string width, bool showCloseButton)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + ")", true);
            }
        }
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string keyForScript, string headerText, string width, bool showCloseButton, bool isResizable)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ")", true);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="headerText"></param>
        /// <param name="width"></param>
        /// <param name="showCloseButton"></param>
        /// <param name="dialogType"></param>
        /// <param name="div">Div whose header has to be prepended with image.</param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string headerText, string width, bool showCloseButton, bool isResizable, DIALOG_TYPE dialogType, string div)
        {
            if (updPanel == null) return;
            string strTrueFalse = "";
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            string showDialogImageType = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    break;
                case DIALOG_TYPE.Warning:
                    showDialogImageType = "DialogType.Warning";
                    break;
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "showModal", @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="divIdToShow"></param>
        /// <param name="page"></param>
        /// <param name="keyForScript"></param>
        /// <param name="headerText"></param>
        /// <param name="width"></param>
        /// <param name="showCloseButton"></param>
        /// <param name="isResizable"></param>
        /// <param name="dialogType"></param>
        /// <param name="div">Div whose header has to be prepended with image.</param>
        public static void showModalPopup(string divIdToShow, System.Web.UI.UpdatePanel updPanel, string keyForScript, string headerText, string width, bool showCloseButton, bool isResizable, DIALOG_TYPE dialogType, string div)
        {
            if (updPanel == null) return;
            if (String.IsNullOrEmpty(width))
            {
                width = "525";
            }
            string strTrueFalse = "";
            if (isResizable)
            {
                strTrueFalse = "true";
            }
            else
            {
                strTrueFalse = "false";
            }
            string showDialogImageType = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    break;
                case DIALOG_TYPE.Warning:
                    showDialogImageType = "DialogType.Warning";
                    break;
            }
            if (showCloseButton)
            {
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUp('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }
            else
            {
                //this is for popup without the header close button
                ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), keyForScript, @"showModalPopUpWithOutHeader('" + divIdToShow + "','" + headerText + "'," + width + "," + strTrueFalse + ");showDialogImage(" + showDialogImageType + ",'" + div + "'" + ");", true);
            }
        }

        public static string getPostBackControlName(System.Web.UI.Page page)
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                return control.ID;
            else return "";
        }

        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.UpdatePanel updPanel)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), "AlertMessage", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.UpdatePanel updPanel, string scriptKey)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), scriptKey, @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        public static void showAlert(string message, string divIdToShowMsg, System.Web.UI.Page page, string scriptKey)
        {
            ScriptManager.RegisterStartupScript(page, typeof(Page), scriptKey, @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        public static void showAlertOnPageStartUp(string message, string divIdToShowMsg, System.Web.UI.Page page, string scriptKey)
        {
            page.ClientScript.RegisterStartupScript(
                page.GetType(),
                scriptKey,
                @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptToRun">; separated scripts,functionName.Pass as verbatim string</param>
        /// <param name="page"></param>
        /// <param name="scriptKey"></param>
        public static void runPostBackScript(string scriptToRun, System.Web.UI.UpdatePanel updPanel, string scriptKey)
        {
            if (updPanel == null) return;
            ScriptManager.RegisterClientScriptBlock(updPanel, typeof(UpdatePanel), scriptKey, scriptToRun, true);
        }
        public static void runPostBackScript(string scriptToRun, System.Web.UI.Page page, string scriptKey)
        {
            ScriptManager.RegisterClientScriptBlock(page, typeof(Page), scriptKey, scriptToRun, true);
        }
        public static void runPageStartUpScript(System.Web.UI.Page page, string key,
                                                string scriptToRun)
        {
            page.ClientScript.RegisterStartupScript(page.GetType(), key, scriptToRun, true);
        }
        public static void showMessageUsingPageStartUp(System.Web.UI.Page page, string key, string message, DIALOG_TYPE dialogType)
        {
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            page.ClientScript.RegisterStartupScript(page.GetType(), key,
                  @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        #endregion

        #region Data Encription

        public static string EncryptString(string s)
        {
            string strEnc = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strEnc;
                }

                int key1 = (DateTime.UtcNow.Second % 10) + 2;
                int key2 = ((key1 + DateTime.UtcNow.Minute) % 10) + 4;
                string strThisAscii = "";

                for (int i = 0; i < s.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key1 - key2);
                    }
                    else
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key2 - key1);
                    }

                    do
                    {
                        strThisAscii = "0" + strThisAscii;

                    } while (strThisAscii.Length < 4);

                    strEnc = strThisAscii + strEnc;
                }

                strEnc = (Hex(key1 * 10) + strEnc + Hex(key2 * 10)).ToLower();

                strEnc = strEnc.Replace("a", "g").Replace("b", "i").Replace("c", "k").Replace("d", "m").Replace("e", "o").Replace("f", "q").Replace("1", "s").Replace("2", "u");
                strEnc = strEnc.Replace("3", "v").Replace("4", "t").Replace("5", "r").Replace("6", "p").Replace("7", "n").Replace("8", "l").Replace("9", "j").Replace("0", "h");
            }
            catch
            {
                strEnc = "";
            }
            return strEnc;
        }

        public static string DecryptString(string s)
        {
            string strDec = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strDec;
                }

                s = s.Replace("h", "0").Replace("j", "9").Replace("l", "8").Replace("n", "7").Replace("p", "6").Replace("r", "5").Replace("t", "4").Replace("v", "3");
                s = s.Replace("u", "2").Replace("s", "1").Replace("q", "f").Replace("o", "e").Replace("m", "d").Replace("k", "c").Replace("i", "b").Replace("g", "a");

                int key1 = (int)HexToDec(s.Substring(0, 2)) / 10;
                int key2 = (int)HexToDec(s.Substring(s.Length - 2, 2)) / 10;

                s = s.Substring(2, s.Length - 4);

                string strThisChar = "";
                bool oddChar = true;

                for (int i = s.Length - 4; i >= 0; i -= 4)
                {
                    if (oddChar)
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key1 + key2);
                        oddChar = false;
                    }
                    else
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key2 + key1);
                        oddChar = true;
                    }

                    strDec += strThisChar;
                }

                return strDec;
            }
            catch
            {
            }
            return "";
        }

        public static string Hex(int n)
        {
            try
            {
                return n.ToString("X");
            }
            catch
            {
            }
            return "";
        }

        public static int HexToDec(string s)
        {
            int intResult;
            try
            {
                intResult = Convert.ToInt32(s, 16);
            }
            catch
            {
                intResult = 0;
            }
            return intResult;
        }

        public static string StringReverse(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            StringBuilder rs = new StringBuilder(s.Length);
            for (int i = s.Length - 1; i >= 0; i--)
            {
                rs.Append(s.Substring(i, 1));
            }
            return rs.ToString();
        }

        private static short Asc(string String)
        {
            return Encoding.Default.GetBytes(String)[0];
        }

        private static string Chr(int CharCode)
        {
            if (CharCode > 255)
            {
                return "";
            }
            return Encoding.Default.GetString(new[] { (byte)CharCode });
        }

        public static string UrlEncode(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return HttpUtility.UrlEncode(s).Replace(@"'", @"%27");
        }

        /// <summary>
        /// Description :Create 32 bit Hash Code Encoding.UTF8
        /// </summary>
        public static string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }

        #endregion

        #region VALIDATE SQL QUERY
        public static Boolean ValidateSqlQuery(string _SqlQuery)
        {
            Boolean bolIsValid = true;
            if (Utilities.IsValidString(_SqlQuery.ToLower(), true, false, true, @"()[],;@#$%&^-_=+||}{:<>?/ ", 10, 5000, true, true))
            {
                bool flag = true;
                string str = _SqlQuery.ToLower();
                while (flag)
                {
                    if (str.Contains("  "))
                    {
                        flag = true;
                        str = str.Replace("  ", " ");
                    }
                    else
                        flag = false;
                }
                if (str.Contains("delete from") || str.Contains("trancate from") || str.Contains("drop table"))
                    bolIsValid = false;
                else
                    bolIsValid = true;
            }
            else
                bolIsValid = false;
            return bolIsValid;
        }

        #endregion

        #region fetch menu detail from database
        public static string SelectForm(string _SubAdminId)
        {
            SqlCommand cm = new SqlCommand("Select ROLE_ID from TBL_SUBADMIN_ROLE where SUBADMIN_ID=@SUBADMIN_ID");
            cm.Parameters.AddWithValue("@SUBADMIN_ID", _SubAdminId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cm);
            string roles = "";
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    roles = roles + dr[0] + ",";
                }
                return roles.Substring(0, roles.LastIndexOf(','));
            }
            return "";
        }

        public static DataSet GetMainMenuList(string strRoles, out bool gotDtlsFromCache)
        {
            gotDtlsFromCache = false;
            DataSet dsNavDtls = null;// CacheManager.Get<DataSet>(CacheManager.GetKey(CacheManager.CacheType.mFicientNavigation,
                                                            //String.Empty,
                                                            //strRoles == "A" ? MficientConstants.CACHE_ADMIN_ROLE : MficientConstants.CACHE_SUB_ADMIN_ROLE
                                                            //, String.Empty, String.Empty, String.Empty, String.Empty));
            if (dsNavDtls != null)
            {
                gotDtlsFromCache = true;
                return dsNavDtls;
            }
            else
            {
                gotDtlsFromCache = false;
                return MSSqlClient.SelectDataFromSQlCommand(
                new SqlCommand("SELECT * from TBL_MENU_ROLE_WISE WHERE  ROLE in(" + "'" + strRoles.Replace(",", "','") + "'" + ") ORDER BY ID;")
                );
            }
            //return MSSqlClient.SelectDataFromSQlCommand(
            //    new SqlCommand("select id,Link,PAGE,CLASS,HEADER,HELP_TEXT from TBL_MENU_ROLE_WISE where PARENT_ID=0 And role in(" + strRoles + ") order by id")
            //    );
        }
        public static DataSet GetSubMenuList(string Pagename, string ParentID)
        {
            return MSSqlClient.SelectDataFromSQlCommand(
                new SqlCommand("select id,Link,PAGE,CLASS,HEADER,HELP_TEXT from TBL_MENU_ROLE_WISE where PARENT_ID=" + ParentID + ";select id from TBL_MENU_ROLE_WISE where PAGE='" + Pagename + "' and PARENT_ID=" + ParentID + " order by id")
                );
        }
        #endregion

        #region Format date according to timezone
        public static string getFormattedDateForUI(DateTime date)
        {
            return date.ToString("dd-MMM-yyyy");
        }

        public static string getFormattedDateTimeForUI(DateTime date)
        {
            return date.ToString("dd-MMM-yyyy HH:mm:ss");
        }

        public static string getFullFormattedDateForUI(DateTime date)
        {
            return date.ToString("F",
                  System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }
        /// <summary>
        /// Interprets year, month, and day as a year, month, and day in the Gregorian calendar
        /// Gives the date in dd-MMM-yyyy format.
        /// </summary>
        /// <param name="date">The format of date should be dd-mm-yyyy</param>
        /// <param name="separator">eg. For dd-mm-yyyy. separator is -</param>
        /// <returns></returns>
        public static string getFormattedDateForUI(
            string dateString,
            char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            DateTime dtSelected =
                new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0])
                );
            return getFormattedDateForUI(dtSelected);
        }
        //public static string getCompanyLocalFormattedDate(string timeZoneInString, long ticksInUTC)
        //{
        //    DateTime dtInUTC = new DateTime(ticksInUTC, DateTimeKind.Utc);
        //    string strTimeZone = timeZoneInString;
        //    string[] aryTimeZone = strTimeZone.Split(':');
        //    bool isPositive = aryTimeZone[0].Substring(0, 1) == "+" ? true : false;
        //    int iTimeOffset = (Convert.ToInt32(aryTimeZone[0].Substring(1)) * 60) + Convert.ToInt32(aryTimeZone[1]);
        //    //int iTimeOffset = Convert.ToInt32(hidTimeOffsetOfClient.Value);

        //    int iTimeOffsetHrs = -(iTimeOffset / 60);
        //    int iTimeOffsetMin = -(iTimeOffset % 60);
        //    //if (isPositive)//getTimezoneOffset() returns -330 for india.Time zone for india is GMT+5:30
        //    //{
        //    //    iTimeOffsetHrs = -iTimeOffsetHrs;
        //    //    iTimeOffsetMin = -iTimeOffsetMin;
        //    //}
        //    DateTime dtInLocalTimezone = dtInUTC.AddHours(iTimeOffsetHrs).AddMinutes(iTimeOffsetMin);
        //    return getFormattedDateForUI(dtInLocalTimezone);
        //}
        //public static string getCompanyLocalFormattedDate(int timeZoneOffset, long ticksInUTC)
        //{
        //    DateTime dtInUTC = new DateTime(ticksInUTC, DateTimeKind.Utc);
        //    int iTimeOffsetHrs = -(timeZoneOffset / 60);
        //    int iTimeOffsetMin = -(timeZoneOffset % 60);//getTimezoneOffset() returns -330 for india.Time zone for india is GMT+5:30
        //    DateTime dtInLocalTimezone = dtInUTC.AddHours(iTimeOffsetHrs).AddMinutes(iTimeOffsetMin);
        //    return getFormattedDateForUI(dtInLocalTimezone);
        //}
        /// <summary>
        /// Returns empty if  Timezone id does not exist.
        /// </summary>
        /// <param name="timeZoneId"></param>
        /// <param name="ticksInUTC"></param>
        /// <returns></returns>
        //public static string getCompanyLocalFormattedDate(string timeZoneId,
        //    long ticksInUTC,out bool isConversionSuccessful)
        //{
        //    TimeZoneInfo tzi;
        //    DateTime dtInLocalTimezone = DateTime.MinValue;
        //    isConversionSuccessful = false;
        //    try
        //    {
        //        tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
        //        isConversionSuccessful = true;
        //    }
        //    catch (TimeZoneNotFoundException)
        //    {
        //        tzi = TimeZoneInfo.Local;
        //    }
        //    catch (InvalidTimeZoneException)
        //    {
        //        tzi = TimeZoneInfo.Local;
        //    }
        //    dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
        //        new DateTime(ticksInUTC, DateTimeKind.Utc),
        //        tzi
        //        );
        //    return getFormattedDateForUI(dtInLocalTimezone);
        //}
        public static string getCompanyLocalFormattedDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(  new DateTime(ticksInUTC, DateTimeKind.Utc), tzi );
            return getFormattedDateForUI(dtInLocalTimezone);
        }
        public static string getCompanyLocalFormattedDateTime(TimeZoneInfo tzi, long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(new DateTime(ticksInUTC, DateTimeKind.Utc), tzi);
            return getFormattedDateTimeForUI(dtInLocalTimezone);
        }
        public static string getFullCompanyLocalFormattedDate(TimeZoneInfo tzi, long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(new DateTime(ticksInUTC, DateTimeKind.Utc), tzi);
            return getFullFormattedDateForUI(dtInLocalTimezone);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateString">The date format should be dd-mm-yyyy</param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <param name="millisec"></param>
        /// <param name="separator">eg. For dd-mm-yyyy. separator is -</param>
        /// <returns></returns>
        public static DateTime getTimeInUtc(string dateString,
            int hrs,
            int min,
            int sec,
            char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            return new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0]),
                hrs,
                min,
                sec
                ).ToUniversalTime();
        }
        public static DateTime getCompanyLocalDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return dtInLocalTimezone;
        }
        #endregion

        #region Company Plan
        internal static double roundOfAmountByCurrencyType(double amount, CURRENCY_TYPE currencyType)
        {
            double dblFinalAmount = 0;
            //for inr round to nearest 10 rs and for usd round to nearest 0.1 $
            switch (currencyType)
            {
                case CURRENCY_TYPE.INR:
                    int iAmount = Convert.ToInt32(Math.Ceiling(amount));
                    if ((iAmount % 10) >= 5)
                    {
                        iAmount = iAmount + (10 - (iAmount % 10));
                    }
                    else
                    {
                        iAmount = iAmount - (iAmount % 10);
                    }
                    dblFinalAmount = iAmount;
                    break;
                case CURRENCY_TYPE.USD:
                    dblFinalAmount = Math.Round(amount, 1);
                    break;
            }
            return dblFinalAmount;
        }
        #endregion

        #region Input Uniform After PostBack
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="key"></param>
        /// <param name="isPostBack"></param>
        /// <param name="otherScripts">pass semicolon separated scripts</param>
        public static void makeInputFieldsUniform(System.Web.UI.Page page, string key, bool isPostBack, string otherScripts)
        {
            if (isPostBack)
            {
                ScriptManager.RegisterStartupScript(page, typeof(Page), key,
                    "$(\"input\").uniform();makeSelectUniformOnPostBack();" + otherScripts, true);
            }
            else
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), key,
                    "$(\"input\").uniform();makeSelectUniformOnPostBack();" + otherScripts,
                    true);
            }
        }
        public static void makeInputFieldsUniformIfMenuClickedAgain(System.Web.UI.Page page, string otherScripts)
        {
            ScriptManager.RegisterStartupScript(page, typeof(Page),
                "MakeInputUniformInMenuIsClickedAgain",
                "$(\"input\").uniform();" + otherScripts, true);
        }
        #endregion


        public static void removeCurrentCookie(string deviceId)
        {
            if (String.IsNullOrEmpty(deviceId))
            {
                HttpContext.Current.Response.Cookies[MficientConstants.COOKIE_MSID].Value = "";
            }
            else
            {
                /**
                 * Consider
                 *  if subadmin logs in and then admin logs in.Then the previous cookie will be removed and new cookie will be added with admin browser id
                 *  if after login as admin then the user try a process in sub admin then again all the cookie will be deleted if we are only changing the value to ""
                 *  as a result then admin will also not able to do any work.
                 * **/
                HttpCookie reqCookie = HttpContext.Current.Request.Cookies[MficientConstants.COOKIE_MSID];
                if (reqCookie != null && reqCookie.Value.Length > 0)
                {
                    string[] aryCookieValue = HttpUtility.UrlDecode(reqCookie.Value).Split('|');//remove the device id from the cookie
                    if (aryCookieValue.Length > 0)
                    {
                        for (int i = 0; i <= aryCookieValue.Length - 1; i++)
                        {
                            if (aryCookieValue[i] == reqCookie.Value)
                            {
                                aryCookieValue[i] = String.Empty;
                            }
                        }
                    }

                    string cookieValue = "";
                    for (int i = 0; i <= aryCookieValue.Length - 1; i++)
                    {
                        if (aryCookieValue[i] != "")
                        {
                            cookieValue += cookieValue.Length == 0 ? aryCookieValue[i] : "|" + aryCookieValue[i];
                        }
                    }
                    if (cookieValue.Split('|').Length == 1)
                    {
                        HttpContext.Current.Response.Cookies[MficientConstants.COOKIE_MSID].Value = "";
                    }
                    else
                    {
                        HttpContext.Current.Response.Cookies[MficientConstants.COOKIE_MSID].Value = cookieValue;
                    }
                }
                else
                {
                    HttpContext.Current.Response.Cookies[MficientConstants.COOKIE_MSID].Value = "";
                }
            }
        }

        public static void addSessionValueInContext(HttpContext context, string key, object value)
        {
            context.Items.Add(key, value);
        }

        public static string getCompanyLogoUrl(string companyId, string companyLogoName)
        {
            return MficientConstants.COMPANY_LOGO_IMAGE_URL_PREFIX + companyId + "/logo/" + companyLogoName;
        }

        public static void SaveInContext(HttpContext _context, string strhfs, string strhfbid,out string SubAdminid,out string strSessionId,out string CompanyId,out string Adminid)
        {
            SubAdminid = "";
            strSessionId = "";
            CompanyId="";
            Adminid="";
            if (string.IsNullOrEmpty(strhfs)) return;
            _context.Items["hfs"] = strhfs;
            _context.Items["hfbid"] = strhfbid;

            string[] hfsParts = Utilities.DecryptString(strhfs).Split(',');
            SubAdminid = hfsParts[0];
            strSessionId = hfsParts[1];
            if (hfsParts[0] == hfsParts[2])
            {
                Adminid = hfsParts[3];
                CompanyId = hfsParts[2];
            }
            else
            {
                Adminid = hfsParts[2];
                CompanyId = hfsParts[3];
            }

            _context.Items["hfs1"] = SubAdminid;
            _context.Items["hfs2"] = strSessionId;
            _context.Items["hfs3"] = Adminid;
            _context.Items["hfs4"] = CompanyId;
        }

        #region Html5App

        public static void AddHtml5AppToCollection(Html5App app)
        {
            try
            {
                if (!hstHtml5Apps.Contains(app.AppId.ToLower().Trim()))
                    hstHtml5Apps.Add(app.AppId.ToLower().Trim(), app);
            }
            catch
            {
            }
        }
        
        public static void RemoveHtml5AppFromCollection(string appId)
        {
            try
            {
                if (hstHtml5Apps.Contains(appId.ToLower().Trim()))
                    hstHtml5Apps.Remove(appId.ToLower().Trim());
            }
            catch
            {
            }
        }

        public static Html5App GetHtml5AppFromCollection(string appId)
        {
            try
            {
                if (hstHtml5Apps.Contains(appId.ToLower().Trim()))
                    return (Html5App)hstHtml5Apps[appId.ToLower().Trim()];
            }
            catch
            {
            }
            return null;
        }

        public static string ConvertdatetabletoString(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serilezer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> listrows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Drow;
            foreach (DataRow dr in dt.Rows)
            {
                Drow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Drow.Add(col.ColumnName, dr[col]);
                }
                listrows.Add(Drow);
            }
            string strresultjson = serilezer.Serialize(listrows);

            return strresultjson;
        }

        public static void makedropdownFieldsUniform(System.Web.UI.Page page, string key, bool isPostBack, string otherScripts)
        {
            if (isPostBack)
            {
                ScriptManager.RegisterStartupScript(page, typeof(Page), key,
                    "makeSelectUniformOnPostBack();" + otherScripts, true);
            }
            else
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), key,
                    "makeSelectUniformOnPostBack();" + otherScripts,
                    true);
            }
        }





        #endregion

        #region S3 Bucket
        
        #endregion

        public static string testObjectRequest(string _CompanyId, string _CommandId, string _ctypType, JArray lp, string _userName, string _password,out string _error)
        {
            _error = "";
            string strRqst, strUrl = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objAgent = new mPluginAgents();
            JObject jodata = new JObject();
            jodata.Add("enid", _CompanyId.ToLower());
            jodata.Add("cmd", _CommandId);
            jodata.Add("ctyp", _ctypType);
            jodata.Add("lp", lp);
            JObject jocr = new JObject();
            jocr.Add("unm", _userName);
            jocr.Add("pwd", _password);
            jodata.Add("cr", jocr);
            strRqst = jodata.ToString();

            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {

                    //strUrl =@"http://localhost:50995/execobject.aspx?d=" + Utilities.UrlEncode(strRqst);
                    strUrl = strUrl + "/execobject.aspx?d=" + Utilities.UrlEncode(strRqst);
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return oResponse.ResponseText;
                    }
                    else
                    {
                        _error = ((int)oResponse.StatusCode).ToString();
                        return "";
                    }
                }
                else
                    return "";
            }
            else
                return "";
        }
        public static string getConnectionString(string _hostName, string _databaseName, string _userId, string _password, string _addString, string _timeout, DatabaseType _dbType)
        {
            string connectionString = "";
            switch (_dbType)
            {
                case DatabaseType.MSSQL:
                    SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
                    connectionStringBuilder.DataSource = _hostName;
                    connectionStringBuilder.InitialCatalog = _databaseName;
                    connectionStringBuilder.UserID = _userId;
                    connectionStringBuilder.Password = _password;
                    if (Convert.ToInt32(_timeout) > 0)
                        connectionStringBuilder.ConnectTimeout = Convert.ToInt32(_timeout);

                    if (_addString.Length > 0)
                        connectionString = connectionStringBuilder.ToString() + _addString + ";";
                    else
                        connectionString = connectionStringBuilder.ToString();
                    break;
                case DatabaseType.MYSQL:
                    connectionString = "SERVER=" + _hostName + ";" + "DATABASE=" + _databaseName + ";" + "UID=" + _userId + ";" + "PASSWORD=" + _password + ";";
                    if (!string.IsNullOrEmpty(_addString))
                        connectionString = connectionString + _addString + ";";
                    break;
                case DatabaseType.ORACLE:
                    connectionString = "";
                    //Use System.Data.OracleClient as database provider
                    Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder builder = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder();
                    builder.DataSource = _hostName + "/" + _databaseName;
                    builder.UserID = _userId;
                    builder.Password = _password;
                    connectionString = builder.ToString();
                    if (_addString.Length > 0)
                        connectionString = connectionString + _addString + ";";
                    break;
                case DatabaseType.POSTGRESQL:
                    connectionString = "Server=" + _hostName + ";" + "Database=" + _databaseName + ";" + "User Id=" + _userId + ";" + "Password=" + _password + ";";
                    if (!string.IsNullOrEmpty(_addString))
                        connectionString = connectionString + _addString + ";";
                    break;
                case DatabaseType.ODBCDSN:
                    connectionString = "dsn=" + _databaseName + ";";
                    break;
            }
            return connectionString;
        }
        public static JArray decriptAllCredential(string strAuthentication, string companyId)
        {
            JArray lst = new JArray();
            if (!string.IsNullOrEmpty(strAuthentication))
            {
                lst = JArray.Parse(strAuthentication);
                foreach (JObject item in lst)
                {
                    if (Convert.ToString(item["uid"]) != "")
                        item["uid"] = AesEncryption.AESDecrypt(companyId, Convert.ToString(item["uid"]));
                    if (Convert.ToString(item["pwd"]) != "")
                        item["pwd"] = AesEncryption.AESDecrypt(companyId, Convert.ToString(item["pwd"]));
                }
            }
            return lst;
        }
        public static void saveActivityLog(SqlConnection objSqlConnection, mFicientCommonProcess.ACTIVITYENUM type, string CompanyId, string SubAdminid, string para1, string para2, string para3, string para4, string para5, string para6, string para7, string para8)
        {
            try
            {
                if (objSqlConnection == null)
                    MSSqlClient.SqlConnectionOpen(out objSqlConnection);

                List<mFicientCommonProcess.Activity> objActivity = new List<mFicientCommonProcess.Activity>();
                objActivity.Add(new mFicientCommonProcess.Activity(type, CompanyId, DateTime.UtcNow.Ticks, SubAdminid, para1, para2, para3, para4, para5, para6, para7, para8, mFicientCommonProcess.SOURCE.CP));
                mFicientCommonProcess.SaveActivityLog.ActivityPerform(objSqlConnection, objActivity, null);
            }
            catch { }
        }
        public static void SendNotificationMenuUpdate(string companyId, string info)
        {
            SqlConnection con;
            MSSqlClient.SqlConnectionOpen(out con, MSSqlClient.getConnectionStringFromWebConfig(MSSqlClient.CONNECTION_STRING_FOR_DB.MFICIENT));
            mFicientCommonProcess.getUserDetail obj = new mFicientCommonProcess.getUserDetail(companyId, con);
            DataSet ds = obj.AllUserDetailProcess();
            MSSqlClient.SqlConnectionClose(con);

            MSSqlClient.SqlConnectionOpen(out con, MSSqlClient.getConnectionStringFromWebConfig(MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM));
            if (ds != null && ds.Tables[0] != null)
            {
                foreach (DataRow usrDevice in ds.Tables[0].Rows)
                {
                  int i= mFicientCommonProcess.SendNotification.SaveNotificationTable(con, null, companyId, Convert.ToString(usrDevice["USER_NAME"]),"",0, mFicientCommonProcess.NOTIFICATION_TYPE.MENU_CHANGE, Convert.ToString(usrDevice["DEVICE_PUSH_MESSAGE_ID"]), Convert.ToString(usrDevice["DEVICE_TYPE"]), Convert.ToString(usrDevice["DEVICE_ID"]), info,1);
                  int b = i;
                }
            }
        }

        public static List<List<string>> objectUsage(DataRow[] drUsedApp, string commandId,string type)
        {
            List<string> apps = new List<string>();
            List<List<string>> appsusage = new List<List<string>>();
            foreach (DataRow dr in drUsedApp)
            {
                List<ObjectXRefrence> lstRef = Utilities.DeserialiseJson<List<ObjectXRefrence>>(Convert.ToString(dr["Command_ids"]));
                foreach (ObjectXRefrence xrf in lstRef)
                    if (xrf.ID == type + commandId)
                    {
                        string views = "";
                        foreach (string v in xrf.Views) views += "," + v;
                        if (views.Length > 0) views = views.Substring(1);
                        else views = "-";
                        string forms = "";
                        foreach (string f in xrf.Forms) forms += "," + f;
                        if (forms.Length > 0) forms = forms.Substring(1);
                        else forms = "-";

                        string app = Convert.ToString(dr["WF_NAME"]);
                        if (!apps.Contains(app))
                        {
                            List<string> appusage = new List<string>();
                            appusage.Add(app);
                            appusage.Add(views);
                            appusage.Add(forms);
                            appsusage.Add(appusage);
                            apps.Add(app);
                        }
                    }
            }
            return appsusage;
        }

        public static string getFormOfflineInputParamJsonForXmlRpc(string inputParam)
        {
            //we willl get a comma separated list of input param
            OfflineInputOutputParamForXmlRpc objInParam = new OfflineInputOutputParamForXmlRpc();
            List<OfflineInputOutputParamDetail> lstParamDtl = new List<OfflineInputOutputParamDetail>();
            if (!String.IsNullOrEmpty(inputParam))
            {
                string[] aryInParams = inputParam.Split(',');
                foreach (string param in aryInParams)
                {
                    OfflineInputOutputParamDetail objParamDtl = new OfflineInputOutputParamDetail();
                    objParamDtl.name = param;
                    objParamDtl.typ = "";
                    lstParamDtl.Add(objParamDtl);
                }
            }
            objInParam.param = lstParamDtl;
            return Utilities.SerializeJson<OfflineInputOutputParamForXmlRpc>(objInParam);
        }
        public static  string getFormOfflineOutputParamJsonForXmlRpc(string outputParam)
        {
            //eg root(paramName,paramName)
            OfflineInputOutputParamForXmlRpc objInParam = new OfflineInputOutputParamForXmlRpc();
            List<OfflineInputOutputParamDetail> lstParamDtl = new List<OfflineInputOutputParamDetail>();
            if (!String.IsNullOrEmpty(outputParam))
            {
                string strCommaSepOutParam = String.Empty;
                string[] aryParams = outputParam.Split('(');
                string strRoot = aryParams[0];
                if (aryParams.Length > 1)
                {
                    strCommaSepOutParam = aryParams[1].Split(')')[0];
                }
                if (strCommaSepOutParam.Length > 0)
                {
                    foreach (string param in strCommaSepOutParam.Split(','))
                    {
                        OfflineInputOutputParamDetail objParamDtl = new OfflineInputOutputParamDetail();
                        objParamDtl.name = strRoot + "." + param;
                        objParamDtl.typ = "";
                        lstParamDtl.Add(objParamDtl);
                    }
                }
            }
            objInParam.param = lstParamDtl;
            return Utilities.SerializeJson<OfflineInputOutputParamForXmlRpc>(objInParam);
        }

        public static string ParseRpcParaJson(string _Json, string _ParaType, out string _ParaDetails)
        {
            _ParaDetails = "";
            string strParaDetail = "", strInStructParaDetail = "", strInArrayParaDetail = "";
            if (_Json.Trim().Length == 0) return "";
            XmlRpcParser obj = new XmlRpcParser();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(_Json));
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = serializer.ReadObject(ms) as XmlRpcParser;
            ms.Close();

            string strMarginLeft = "";
            string strLevel = "0";
            string strHtml = "";
            string ItemType = "";
            string strHeaderHtml = "";
            foreach (param paramItem in obj.param)
            {
                ItemType = GetRpcParamType(paramItem.typ.Trim());
                if (ItemType != "struct" && ItemType.Split('-')[0] != "array")
                {
                    strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').append('<div id=\"" + _ParaType + "Para_" + paramItem.name + "\" class=\"rpcContText\" " + strMarginLeft + ">" +
                                                                                    paramItem.name +
                                                                              "</div>" +
                                                                              "<a id=\"aLevel" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\"  type=\"level\">" + strLevel + "</a>" +
                                                                              "<a id=\"aPre" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\"></a>" +
                                                                              "<div class=\"clear\"></div>');"
                             + "$('#rpc" + _ParaType + "ParaContDiv2').append('<div id=\"" + _ParaType + "PType_" + paramItem.name + "\"  class=\"rpcContType\">" +
                                                                                    "<div class=\"FLeft\">" + ItemType + "</div>" +
                                                                                    "<div class=\"rpcContDelete\"><img id=\"Img_" + _ParaType + "_" + paramItem.name + "\" src=\"//enterprise.mficient.com/images/cross.png\" alt=\"Delete\" onclick=\"DeleteRpcParamClick(this);\" /></div>" +
                                                                               "</div>" +
                                                                               "<div id=\"SepType" + _ParaType + "PType_" + paramItem.name + "\" class=\"clear\" class=\"clear\"></div>');";
                    if (strParaDetail.Length > 0)
                        strParaDetail += ",";
                    else { }
                    strParaDetail += paramItem.name;
                }
                else if (ItemType.Split('-')[0] == "struct")
                {
                    strInStructParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, "", "struct", out strInStructParaDetail);
                    var test = strInStructParaDetail;
                }
                else if (ItemType.Split('-')[0] == "array")
                {
                    strInArrayParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, "", "array", out strInArrayParaDetail);
                }
                strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').show();$('#rpc" + _ParaType + "ParaContDiv2').show();";
            }
            if (strParaDetail.Length > 0)
            {
                if (strInStructParaDetail.Length > 0)
                    strParaDetail += ",";
                else { }
            }
            else { }
            strParaDetail += strInStructParaDetail;
            if (strInArrayParaDetail.Length > 0 && strParaDetail.Length > 0)
                strParaDetail += ",";
            else { }
            strParaDetail += strInArrayParaDetail;
            _ParaDetails = strParaDetail;
            return strHtml + strHeaderHtml;
        }


        static string GetRpcParaHtmlForStruct(string _StructName, param _Inpara, string _ParaType, string _Level, string _ParentPara, string _ParentType, out string _ParaDetails)
        {

            string strParaDetail = "", strInnerParaDetail = "", strInnerStructParaDetail = "", strInArrayParaDetail = "";
            string strLevel = (Convert.ToInt32(_Level) + 1).ToString();
            string strHtml = "";
            string ItemType = "";
            string strMarginLeft = "style=\"margin-left:" + (Convert.ToInt32(strLevel) * 20).ToString() + "px;\"";
            strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').append('<div id=\"" + _ParaType + "Para_" + _StructName + "\" class=\"rpcContText\" " + "style=\"margin-left:" + (Convert.ToInt32(_Level) * 20).ToString() + "px;\"" + ">" +
                                                                            _StructName +
                                                                      "</div>" +
                                                                      "<a id=\"aLevel" + _ParaType + "Para_" + _StructName + "\" class=\"hide\"  type=\"level\">" + _Level + "</a>" +
                                                                      "<a id=\"aPre" + _ParaType + "Para_" + _StructName + "\" class=\"hide\">" + _ParentPara + "</a>" +
                                                                      "<div class=\"clear\"></div>');"
                     + "$('#rpc" + _ParaType + "ParaContDiv2').append('<div id=\"" + _ParaType + "PType_" + _StructName + "\"  class=\"rpcContType\">" +
                                                                            "<div class=\"FLeft\">" + _ParentType + "</div>" +
                                                                            "<div class=\"rpcContDelete\"><img id=\"Img_" + _ParaType + "_" + _StructName + "\" src=\"//enterprise.mficient.com/images/cross.png\" alt=\"Delete\" onclick=\"DeleteRpcParamClick(this);\" /></div>" +
                                                                            "<div class=\"rpcContAdd\"><img id=\"ImgAdd_" + _ParaType + "_" + _StructName + "\" src=\"//enterprise.mficient.com/images/add.png\" alt=\"Add\"  onclick=\"AddRpcParamClick(this);\" /></div>" +
                                                                      "</div>" +
                                                                      "<div id=\"SepType" + _ParaType + "PType_" + _StructName + "\" class=\"clear\"></div>');";

            if (strParaDetail.Length > 0)
                strParaDetail += ",";
            strParaDetail += _StructName + "(";
            foreach (param paramItem in _Inpara.inparam)
            {
                ItemType = GetRpcParamType(paramItem.typ.Trim());
                if (ItemType != "struct" && ItemType != "array")
                {
                    strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').append('<div id=\"" + _ParaType + "Para_" + paramItem.name + "\" class=\"rpcContText\" " + strMarginLeft + ">" +
                                                                                paramItem.name +
                                                                             "</div>" +
                                                                             "<a id=\"aLevel" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\"  type=\"level\">" + strLevel + "</a>" +
                                                                             "<a id=\"aPre" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\">" + _StructName + "</a>" +
                                                                             "<div class=\"clear\"></div>');"
                            + "$('#rpc" + _ParaType + "ParaContDiv2').append('<div id=\"" + _ParaType + "PType_" + paramItem.name + "\"  class=\"rpcContType\">" +
                                                                                    "<div class=\"FLeft\">" + ItemType + "</div>" +
                                                                                    "<div class=\"rpcContDelete\"><img id=\"Img_" + _ParaType + "_" + paramItem.name + "\" src=\"//enterprise.mficient.com/images/cross.png\" alt=\"Delete\" onclick=\"DeleteRpcParamClick(this);\" /></div>" +

                                                                            "</div>" +
                                                                            "<div id=\"SepType" + _ParaType + "PType_" + paramItem.name + "\" class=\"clear\"></div>');";

                    if (strInnerParaDetail.Length > 0)
                        strInnerParaDetail += ",";
                    else { }
                    strInnerParaDetail += paramItem.name;
                }
                else if (ItemType.Split('-')[0] == "struct")
                {
                    strInnerStructParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, _StructName, "struct", out strInnerStructParaDetail);
                }
                else if (ItemType.Split('-')[0] == "array")
                {
                    strInArrayParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, _StructName, "array", out strInArrayParaDetail);
                }
            }
            strParaDetail += strInnerParaDetail;
            if (strInnerParaDetail.Trim().Length > 0)
                if (strInnerStructParaDetail.Length > 0)
                    strParaDetail += ",";
            strParaDetail += strInnerStructParaDetail;
            if (strInnerStructParaDetail.Length > 0 || strInnerParaDetail.Length > 0)
                if (strInArrayParaDetail.Length > 0)
                    strParaDetail += ",";
            strParaDetail += strInArrayParaDetail;
            strParaDetail += ")";
            _ParaDetails = strParaDetail;
            return strHtml;
        }

        static string GetRpcParamType(string _Type)
        {
            var CurrType = "";
            switch (_Type)
            {
                case "0":
                    CurrType = "i4";
                    break;
                case "1":
                    CurrType = "base64";
                    break;
                case "2":
                    CurrType = "boolean";
                    break;
                case "3":
                    CurrType = "date/time";
                    break;
                case "4":
                    CurrType = "double";
                    break;
                case "5":
                    CurrType = "integer";
                    break;
                case "6":
                    CurrType = "string";
                    break;
                case "7":
                    CurrType = "struct";
                    break;
                case "8":
                    CurrType = "array";
                    break;
            }
            return CurrType;
        }
    }
    
}