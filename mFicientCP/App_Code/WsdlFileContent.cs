﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class WsdlFileContent
    {
       public WsdlFileContent()
        { 

        }

       public WsdlFileContent(Boolean _UploadContent,string _Id, string _Content,string _ContentType,string _SubAdminId,string _CompanyId)
        {
            this.Id = _Id;
            this.Content = _Content;
            this.UploadContent = _UploadContent;
            this.ContentType = _ContentType;//wsdl or dpOption
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
           
        }
        public void Process()
       {
           
           SqlCommand objSqlCommand;
           string query; this.StatusCode = -1000;
           if (UploadContent)
           {
               SqlConnection objSqlConnection = null;
               SqlTransaction objSqlTransaction = null;
               try
               {
                   MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                   query = @"DELETE FROM TBL_UPLOAD_FILE WHERE TYPE=@TYPE AND COMPANY_ID=@COMPANY_ID AND SUBADMIN_ID=@SUBADMIN_ID;";

                   objSqlTransaction = objSqlConnection.BeginTransaction();
                   objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                   objSqlCommand.CommandType = CommandType.Text;
                   objSqlCommand.Parameters.AddWithValue("@TYPE", this.ContentType);
                   objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                   objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                   objSqlCommand.ExecuteNonQuery();

                   query = @"INSERT INTO TBL_UPLOAD_FILE(FILE_ID,FILE_CONTENT,TYPE,COMPANY_ID,SUBADMIN_ID) VALUES(@FILE_ID,@FILE_CONTENT,@TYPE,@COMPANY_ID,@SUBADMIN_ID);";
                   objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                   objSqlCommand.CommandType = CommandType.Text;
                   objSqlCommand.Parameters.AddWithValue("@FILE_ID", this.Id);
                   objSqlCommand.Parameters.AddWithValue("@FILE_CONTENT", this.Content);
                   objSqlCommand.Parameters.AddWithValue("@TYPE", this.ContentType);
                   objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                   objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                   if (objSqlCommand.ExecuteNonQuery() > 0) this.StatusCode = 0;

                   objSqlTransaction.Commit();
               }
               catch
               {
                   this.StatusCode = -1000;
                   if (objSqlTransaction != null)
                        objSqlTransaction.Rollback();
               }
               finally
               {
                   MSSqlClient.SqlConnectionClose(objSqlConnection);
               }
           }
           else
           {
               try
               {
                   query = @"SELECT  * FROM  TBL_UPLOAD_FILE WHERE FILE_ID=@FILE_ID AND TYPE=@TYPE AND COMPANY_ID=@COMPANY_ID AND SUBADMIN_ID=@SUBADMIN_ID;";
                   objSqlCommand = new SqlCommand(query);
                   objSqlCommand.CommandType = CommandType.Text;
                   objSqlCommand.Parameters.AddWithValue("@FILE_ID", this.Id);
                   objSqlCommand.Parameters.AddWithValue("@TYPE", this.ContentType);
                   objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                   objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                   DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                   this.ResultTable = objDataSet.Tables[0];
                   if (objDataSet.Tables[0].Rows.Count > 0) this.StatusCode = 0;
               }
               catch 
               {
                   this.StatusCode = -1000;
               }
           }
            
        }
        public int StatusCode
        {
            set;
            get;
        }

        public string Id { get; set; }

        public bool UploadContent { get; set; }

        public string Content { get; set; }

        public DataTable ResultTable { get; set; }

        public string ContentType { get; set; }

        public string SubAdminId { get; set; }

        public string CompanyId { get; set; }
    }
}