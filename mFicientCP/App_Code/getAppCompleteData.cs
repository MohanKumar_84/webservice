﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web.UI;
using System.Xml;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace mFicientCP
{
    public class GetAppCompleteData
    {
        string CompanyId,SubadminId;  TimeZoneInfo companyTimezone;
        public GetAppCompleteData(string _CompanyId,string _subadminId)
        {
            this.CompanyId = _CompanyId;
            this.SubadminId = _subadminId;
            this.companyTimezone = CompanyTimezone.getTimezoneInfo(_CompanyId);
        }

        public string GetAppsList()
        {
            string query = @"SELECT DISTINCT Wf.WF_ID,Wf.WF_NAME,'Standard' AS APP_TYPE,Wf.WF_DESCRIPTION,convert(varchar,Wf.UPDATE_ON) as UPDATE_ON,
            CASE wd.count when 1  then Wf.MODEL_TYPE ELSE wd.count END AS MTYPE,isnull(Wf.icon,'GYAR0389.png') as icon,
            CASE  WHEN PublishedApp.WF_ID is null  then 0 ELSE 1 END AS IS_PUBLISHED
            from (SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='1'
            AND WF_ID not in (SELECT  WF_ID FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='0')
            AND COMPANY_ID=@COMPANY_ID UNION
            SELECT  * FROM  TBL_WORKFLOW_DETAIL WHERE MODEL_TYPE='0'
            AND COMPANY_ID=@COMPANY_ID) AS Wf JOIN
            (SELECT COUNT(WF_ID) count,WF_ID FROM TBL_WORKFLOW_DETAIL  GROUP BY WF_ID) AS  wd  
            ON Wf.WF_ID=wd.WF_ID and Wf.WORKED_BY=@SubadminId 
            LEFT OUTER JOIN
            TBL_CURR_WORKFLOW_DETAIL PublishedApp ON PublishedApp.WF_ID = Wf.WF_ID
            where WF.APP_JS_JSON !=''  ORDER BY Wf.WF_NAME;";

            SqlCommand objSqlCommand = new SqlCommand(query);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@SubadminId", this.SubadminId);

            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            ArrayList rows = new ArrayList();
            if (objDataSet != null)
            {
                foreach (DataRow appdr in objDataSet.Tables[0].Rows)
                {
                    string str = Utilities.getCompanyLocalDate(companyTimezone, Convert.ToInt64(appdr["UPDATE_ON"])).ToString("dd-MMM-yyyy");
                    appdr["UPDATE_ON"] = str;
                    rows.Add(appdr.ItemArray);
                }
            } 
            return Utilities.SerializeJson<ArrayList>(rows);
        }
        public JArray GetAppDetailAsJArray(string _appId)
        {
            JArray _apps = new JArray();
            JObject _app;
            try
            {
                //PREVIOUS QUERY
                //SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.PARENT_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID ORDER BY fd.FORM_NAME;
                // select * from TBL_MASTER_PAGE_DETAILS fd WHERE fd.PARENT_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID 
                StatusCode = 0;//for error
                string query = @"SELECT WF.*,isnull(WF.ICON,'') as WF_ICON FROM TBL_WORKFLOW_DETAIL AS WF WHERE WF.COMPANY_ID =@COMPANY_ID AND WF.WF_ID=@WF_ID ORDER BY UPDATE_ON";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", _appId);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                int ROW_COUNT = objDataSet.Tables[0].Rows.Count;

                foreach (DataRow dr_App in objDataSet.Tables[0].Rows)
                {
                    _app = new JObject();
                    _app.Add("compId", CompanyId);
                    string modelType = Convert.ToString(dr_App["MODEL_TYPE"]);
                    _app.Add("modelType", modelType);
                    if (ROW_COUNT == 1)
                    {
                        if (modelType == "0")
                        {
                            _app.Add("runsOn", "0");
                            _app.Add("diffInterfaceForTab", "0");
                        }
                        else
                        {
                            _app.Add("runsOn", "1");
                            _app.Add("diffInterfaceForTab", "1");
                        }
                    }
                    else if (ROW_COUNT == 2)
                    {
                        _app.Add("runsOn", "1");
                        _app.Add("diffInterfaceForTab", "0");
                    }

                    _app.Add("icon", Convert.ToString(dr_App["WF_ICON"]));
                    _app.Add("noOfRecordsInDb", ROW_COUNT.ToString());
                    _app.Add("parentId", Convert.ToString(dr_App["PARENT_ID"]));
                    _app.Add("wfDescription", Convert.ToString(dr_App["WF_DESCRIPTION"]));
                    _app.Add("wfid", _appId);
                    _app.Add("wfJson", Convert.ToString(dr_App["WF_JSON"]));
                    _app.Add("wfName", Convert.ToString(dr_App["WF_NAME"]));
                    if (dr_App["APP_JS_JSON"].ToString().Trim().Length > 0)
                    {
                        JObject tempAppJson = JObject.Parse(Convert.ToString(dr_App["APP_JS_JSON"]));
                        JArray views = JArray.Parse(Convert.ToString(tempAppJson["views"]));
                        foreach (JObject obj in views)
                        {
                            obj["xReference"] = null;
                        }
                        tempAppJson["views"] = views;
                        _app.Add("appJsJson", tempAppJson);

                        //jqueryAppClass objappjson = Utilities.DeserialiseJson<jqueryAppClass>();
                        //_app.appJsJson = objappjson;
                    }
                    _apps.Add(_app);
                }
            }
            catch
            {
                StatusCode = -1000;
            }
            return _apps;
        }
        public string GetAppJson(string _appId)
        {
            JArray _apps = new JArray();
            JObject _app;
            try
            {
                //PREVIOUS QUERY
                //SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.PARENT_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID ORDER BY fd.FORM_NAME;
                 // select * from TBL_MASTER_PAGE_DETAILS fd WHERE fd.PARENT_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID 
                StatusCode = 0;//for error
                string query = @"SELECT WF.*,isnull(WF.ICON,'') as WF_ICON FROM TBL_WORKFLOW_DETAIL AS WF WHERE WF.COMPANY_ID =@COMPANY_ID AND WF.WF_ID=@WF_ID ORDER BY UPDATE_ON";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", _appId);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                int ROW_COUNT = objDataSet.Tables[0].Rows.Count;

                foreach (DataRow dr_App in objDataSet.Tables[0].Rows)
                {
                    _app = new JObject();
                    _app.Add("compId", CompanyId);
                    string modelType=Convert.ToString(dr_App["MODEL_TYPE"]);
                    _app.Add("modelType", modelType);
                    if (ROW_COUNT == 1)
                    {
                        if (modelType == "0")
                        {
                            _app.Add("runsOn","0");
                            _app.Add("diffInterfaceForTab","0");
                        }
                        else
                        {
                            _app.Add("runsOn","1");
                            _app.Add("diffInterfaceForTab","1");
                        }
                    }
                    else if (ROW_COUNT == 2)
                    {
                        _app.Add("runsOn", "1");
                        _app.Add("diffInterfaceForTab", "0");
                    }

                    _app.Add("icon",  Convert.ToString(dr_App["WF_ICON"]));
                    _app.Add("noOfRecordsInDb",  ROW_COUNT.ToString());
                    _app.Add("parentId",  Convert.ToString(dr_App["PARENT_ID"]));
                    _app.Add("wfDescription",  Convert.ToString(dr_App["WF_DESCRIPTION"]));
                     _app.Add("wfid",  _appId);
                     _app.Add("wfJson",  Convert.ToString(dr_App["WF_JSON"]));
                     _app.Add("wfName",  Convert.ToString(dr_App["WF_NAME"]));
                    if (dr_App["APP_JS_JSON"].ToString().Trim().Length > 0)
                    {
                        JObject tempAppJson = JObject.Parse(Convert.ToString(dr_App["APP_JS_JSON"]));
                        JArray views = new JArray();
                        var varView=tempAppJson["views"];
                        if (varView != null) views = JArray.Parse(varView.ToString());
                        foreach (JObject obj in views)
                        {
                            obj["xReference"] = null;
                        }
                        tempAppJson["views"] = views;
                        _app.Add("appJsJson", tempAppJson);
                        
                        //jqueryAppClass objappjson = Utilities.DeserialiseJson<jqueryAppClass>();
                        //_app.appJsJson = objappjson;
                    }
                    _apps.Add(_app);
                }
            }
            catch
            {
                StatusCode = -1000;
            }
            return _apps.ToString();
        }
        public string GetArchivedAppJson(string _appId, string version)
        {
            JArray _apps = new JArray();
            JObject _app;
            try
            {
                //PREVIOUS QUERY
                //SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.PARENT_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID ORDER BY fd.FORM_NAME;
                // select * from TBL_MASTER_PAGE_DETAILS fd WHERE fd.PARENT_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID 
                StatusCode = 0;//for error
                string query = @"SELECT WF.*,isnull(WF.ICON,'') as WF_ICON FROM TBL_PRV_WORKFLOW_DETAIL AS WF WHERE WF.COMPANY_ID =@COMPANY_ID AND WF.WF_ID=@WF_ID AND WF.VERSION = @VERSION";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", _appId);
                objSqlCommand.Parameters.AddWithValue("@VERSION", version);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                int ROW_COUNT = objDataSet.Tables[0].Rows.Count;

                foreach (DataRow dr_App in objDataSet.Tables[0].Rows)
                {
                    _app = new JObject();
                    _app.Add("compId", CompanyId);
                    string modelType = Convert.ToString(dr_App["MODEL_TYPE"]);
                    _app.Add("modelType", modelType);
                    if (ROW_COUNT == 1)
                    {
                        if (modelType == "0")
                        {
                            _app.Add("runsOn", "0");
                            _app.Add("diffInterfaceForTab", "0");
                        }
                        else
                        {
                            _app.Add("runsOn", "1");
                            _app.Add("diffInterfaceForTab", "1");
                        }
                    }
                    else if (ROW_COUNT == 2)
                    {
                        _app.Add("runsOn", "1");
                        _app.Add("diffInterfaceForTab", "0");
                    }

                    _app.Add("icon", Convert.ToString(dr_App["WF_ICON"]));
                    _app.Add("noOfRecordsInDb", ROW_COUNT.ToString());
                    _app.Add("parentId", Convert.ToString(dr_App["PARENT_ID"]));
                    _app.Add("wfDescription", Convert.ToString(dr_App["WF_DESCRIPTION"]));
                    _app.Add("wfid", _appId);
                    _app.Add("wfJson", Convert.ToString(dr_App["WF_JSON"]));
                    _app.Add("wfName", Convert.ToString(dr_App["WF_NAME"]));
                    if (dr_App["APP_JS_JSON"].ToString().Trim().Length > 0)
                    {
                        JObject tempAppJson = JObject.Parse(Convert.ToString(dr_App["APP_JS_JSON"]));
                        JArray views = JArray.Parse(Convert.ToString(tempAppJson["views"]));
                        foreach (JObject obj in views)
                        {
                            obj["xReference"] = null;
                        }
                        tempAppJson["views"] = views;
                        _app.Add("appJsJson", tempAppJson);

                        //jqueryAppClass objappjson = Utilities.DeserialiseJson<jqueryAppClass>();
                        //_app.appJsJson = objappjson;
                    }
                    _apps.Add(_app);
                }
            }
            catch
            {
                StatusCode = -1000;
            }
            return _apps.ToString();
        }
        public string GetArchivedAppsList()
        {
            string query = @"

                            SELECT wf.WF_ID,wf.WF_NAME,COMPANY_ID,Versions,MODEL_TYPES,
                            (	CASE 
	                            WHEN (wf.APP_TYPE IS NULL OR CONVERT(VARCHAR(MAX),wf.APP_TYPE) ='0') THEN 'Standard' 
	                            WHEN CONVERT(VARCHAR(MAX),wf.APP_TYPE) ='1' THEN 'Offline'
	                            END
                            ) AS APP_TYPE
                            FROM
                            (
	                            (SELECT DISTINCT prevWf.WF_ID,prevWf.COMPANY_ID,
	                              STUFF((SELECT distinct ',' + CONVERT(VARCHAR(MAX),tempWf.[Version])
			                             FROM TBL_PRV_WORKFLOW_DETAIL tempWf
			                             WHERE prevWf.WF_ID = tempWf.WF_ID
				                            FOR XML PATH(''), TYPE
				                            ).value('.', 'NVARCHAR(MAX)')
			                            ,1,1,'') Versions,
	                              STUFF((SELECT distinct ',' + CONVERT(VARCHAR(MAX),tempWf.[MODEL_TYPE])
			                             FROM TBL_PRV_WORKFLOW_DETAIL tempWf
			                             WHERE prevWf.WF_ID = tempWf.WF_ID
				                            FOR XML PATH(''), TYPE
				                            ).value('.', 'NVARCHAR(MAX)')
			                            ,1,1,'') MODEL_TYPES
	                            FROM TBL_PRV_WORKFLOW_DETAIL prevWf
	                            WHERE prevWf.COMPANY_ID = @COMPANY_ID
	                            AND SUBADMIN_ID = @SUBADMIN_ID)AS wfVersions

	                            INNER JOIN 
	
	                            (SELECT DISTINCT WF_ID,APP_TYPE,WF_NAME
	                            FROM TBL_WORKFLOW_DETAIL
	                            WHERE COMPANY_ID = @COMPANY_ID
	                            AND SUBADMIN_ID = @SUBADMIN_ID
	                            )AS wf
	
	                            ON wfVersions.WF_ID = wf.WF_ID
                            )";

            SqlCommand objSqlCommand = new SqlCommand(query);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);

            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            List<IdeArchivedAppsForListingUI> rows = new List<IdeArchivedAppsForListingUI>();

            IdeArchivedAppsForListingUI objAppsDtls = null;
            if (objDataSet != null)
            {
                foreach (DataRow appdr in objDataSet.Tables[0].Rows)
                {
                    objAppsDtls = new IdeArchivedAppsForListingUI();
                    objAppsDtls.Name = Convert.ToString(appdr["WF_NAME"]);
                    objAppsDtls.Id = Convert.ToString(appdr["WF_ID"]);
                    objAppsDtls.Type = Convert.ToString(appdr["APP_TYPE"]);
                    objAppsDtls.Versions = Convert.ToString(appdr["Versions"]);
                    objAppsDtls.ModelTypes = Convert.ToString(appdr["MODEL_TYPES"]);
                    rows.Add(objAppsDtls);
                }
            }
            return Utilities.SerializeJson<List<IdeArchivedAppsForListingUI>>(rows);
        }
        public string StatusDescription { get; set; }
        public int StatusCode { get; set; }
    }
}