﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class DeviceLog : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;


            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;


            if (!Page.IsPostBack)
            {
                bindSubAdminDropDown();
                bindActivityLogProcessType();
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd-MM-yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
                bindActivityRepeater();
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "makeDatePickerWithMonthYear();", true);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    bindSubAdminDropDown();
                    bindActivityLogProcessType();
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "makeDatePickerWithMonthYear();");
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "makeDatePickerWithMonthYear();");
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", "$(\"select:not(div.selector select)\").uniform();$(\"input:not(div.checker input)\").uniform();makeDatePickerWithMonthYear();", true);
            }
        }

        void bindActivityRepeater()
        {
            string toDateClientId = txtToDate.ClientID;
            string fromDateClientId = txtFromDate.ClientID;
            if (txtToDate.Text != "" && txtFromDate.Text != "")
            {
                DateTime fromDate =
                    new DateTime(Convert.ToInt32(txtFromDate.Text.Split('-')[2]),
                        Convert.ToInt32(txtFromDate.Text.Split('-')[1]),
                        Convert.ToInt32(txtFromDate.Text.Split('-')[0]));//0 hrs 0 mins

                DateTime toDate =
                    new DateTime(Convert.ToInt32(txtToDate.Text.Split('-')[2]),
                        Convert.ToInt32(txtToDate.Text.Split('-')[1]),
                        Convert.ToInt32(txtToDate.Text.Split('-')[0])).AddHours(23).AddMinutes(59);//23 hrs 59 min

                if (fromDate > toDate)
                {
                    Utilities.showAlert("Please enter valid date.From Date cannot be after to date.", "divAlert", upd);
                    return;
                }
                DataTable dtblActivityLog = new DataTable();

                DataSet dsSubAdminActivityLog = SubadminDeviceLog.getSubAdminDeviceActivityLogModified(strUserId, hfsPart4, (SUBADMIN_Device_LOG)Enum.Parse(typeof(SUBADMIN_Device_LOG), ddlLogType.SelectedValue), fromDate.ToUniversalTime().Ticks, toDate.ToUniversalTime().Ticks);
                if (dsSubAdminActivityLog != null)
                {
                    dtblActivityLog = dsSubAdminActivityLog.Tables[0];
                }
                else
                {
                    Utilities.showMessage("Internal server error", upd, DIALOG_TYPE.Error);
                }
                if (dtblActivityLog.Rows.Count > 0)
                {
                    rptActivityLogDtls.DataSource = dtblActivityLog;
                    rptActivityLogDtls.DataBind();
                    lblHeaderInfo.Text = "<h1>Activity Log</h1>";
                }
                else
                {
                    lblHeaderInfo.Text = "<h1>No activity log found</h1>";
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    rptActivityLogDtls.DataSource = null;
                    rptActivityLogDtls.DataBind();
                }
            }
            else
            {
                string strErrorMsg = "";
                if (txtFromDate.Text == "" && txtToDate.Text != "")
                {
                    strErrorMsg = "Please enter from date";
                }
                else if (txtFromDate.Text != "" && txtToDate.Text == "")
                {
                    strErrorMsg = "Please enter to date";
                }
                else if (txtFromDate.Text == "" && txtToDate.Text == "")
                {
                    strErrorMsg = "Please enter from date </br> Please enter to date";
                }

                Utilities.showAlert(strErrorMsg, "divAlert", upd);
            }
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void bindSubAdminDropDown()
        {
            //GetSubAdminDetail objSubAdminDetail = new GetSubAdminDetail(true, false, hfsPart4, "", "", hfsPart3);
            //objSubAdminDetail.Process();
            GetSubAdminDetail objGetSubAdminDetail =
                new GetSubAdminDetail( hfsPart3,hfsPart4);
            objGetSubAdminDetail.getAllSubAdminsOfCmpByAdminId();
            DataTable dtblSubAdmins = objGetSubAdminDetail.ResultTable;
            if (dtblSubAdmins != null && dtblSubAdmins.Rows.Count > 0)
            {
                ddlSubAdmin.Items.Clear();
                ddlSubAdmin.DataSource = dtblSubAdmins;
                ddlSubAdmin.DataValueField = "SUBADMIN_ID";
                ddlSubAdmin.DataTextField = "FULL_NAME";
                ddlSubAdmin.DataBind();
                ddlSubAdmin.Items.Insert(0, new ListItem("All", String.Empty));
            }

        }
        void bindActivityLogProcessType()
        {
            ddlLogType.Items.Clear();
            ddlLogType.Items.Add(new ListItem("Device All", ((int)SUBADMIN_Device_LOG.ALL).ToString()));
            ddlLogType.Items.Add(new ListItem("Devices Approved", ((int)SUBADMIN_Device_LOG.MOBILE_DEVICE_APPROVE).ToString()));
            ddlLogType.Items.Add(new ListItem("Device Deleted", ((int)SUBADMIN_Device_LOG.MOBILE_DEVICE_DELETE).ToString()));
            ddlLogType.Items.Add(new ListItem("Device Denied", ((int)SUBADMIN_Device_LOG.MOBILE_DEVICE_DENY).ToString()));
            ddlLogType.Items.Add(new ListItem("Device Delete Approved", ((int)SUBADMIN_Device_LOG.MOBILE_DEVICE_DELETE_APPROVE).ToString()));
        }
        protected void rptActivityLogDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void rdlLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindActivityRepeater();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            bindActivityRepeater();
        }
    }
}