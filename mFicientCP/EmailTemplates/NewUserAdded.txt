﻿Dear <%UserFullname%>
You have been added as a mobile user by subadmin <%SubAdminName%> on <%Date%>.
Your username is <%UsersUserName%>
In case of any further queries, please contact support@mficient.com