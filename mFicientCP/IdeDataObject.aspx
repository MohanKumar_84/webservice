﻿<%@ Page Title="mStudio | Data Objects" Language="C#" MasterPageFile="~/master/IdeMaster.Master"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="IdeDataObject.aspx.cs"
    Inherits="mFicientCP.IdeDataObject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery.datatables.css" rel="stylesheet" type="text/css" />
    <style>
        #tableobj thead{
            visibility: hidden;
        }
        .dataTables_filter
        {
            padding-bottom : 0px !important;
        }
        .ui-icon-closethick
        {
            margin: 0px;
        }
        .selector
        {
            float: left;
        }
        
        .DbCmdRightDivtest
        {
            float: left;
            width: 98%;
            margin-top: 15px;
        }
        .DbCmdRightDivtest1
        {
            margin-top: -35px;
            margin-left: 35px;
        }
        .CmdDescInputStyle12
        {
            width: 80%;
            border: 1px solid;
            border-color: #bbbbbb;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            background-color: #ffffff;
            min-height: 2px;
            margin-top: -15px;
            margin-left: 50px;
        }
        
        .hidediv
        {
            display: none;
        }
        .DbCmdLeftDiv
        {
            float: left;
            padding-top: 5px;
            width: 20%;
            margin-top: 7px;
        }
        .selects
        {
            margin-top: 9px !important;
        }
        
        .repeaterTable tbody :hover
        {
            background-color: White !important;
        }
        
        .QPContentText1
        {
            padding-left: 3px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 48px;
            float: left;
            height: 20px;
        }
        .QPContentType1
        {
            padding-left: 13px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 190px;
            float: left;
            height: 20px;
        }
        
        .QPDeletethird
        {
            font-size: 12px;
            font-weight: 700;
            width: 150px;
            float: left;
            padding-top: 3px;
            height: 20px;
        }
        .QPTextHeader1
        {
            width: 119.2px;
            float: left;
            border-right: solid 1px silver;
            border-bottom: solid 1px silver;
            border-left: solid 1px silver;
            padding-left: 3px;
            padding-top: 3px;
            height: 20px;
        }
        
        .QPDelete1
        {
            font-size: 12px;
            font-weight: 700;
            width: 106px;
            float: left;
            padding-top: 3px;
            height: 20px;
        }
        .QPTypeHeader1
        {
            font-size: 12px;
            font-weight: 700;
            width: 106px;
            float: left;
            padding-top: 3px;
            padding-left: 3px;
            border-right: solid 1px silver;
            height: 20px;
        }
        
        .Qtemp
        {
            height: 20px;
            float: left;
            width: 125px;
            word-wrap: break-word;
        }
        .imagecontent
        {
            padding-left: 3px;
            padding-top: 3px;
            padding-bottom: 3px;
            float: left;
            border-bottom: solid 1px silver;
            border-right: solid 1px silver;
            height: 20px;
            width: 120px;
        }
        
        .imagecontentType
        {
            padding-left: 3px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 120px;
            float: left;
            border-bottom: solid 1px silver;
            border-right: solid 1px silver;
            height: 20px;
            width: 120px;
        }
        
        
        .ImageQPContentRow
        {
            float: left;
        }
    </style>
    <link href="css/IdeToken-input-facebook.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.atwho.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/Jquerydatatablerowclick.css" />
    <script src="Scripts/IdeDataObjects.js" type="text/javascript"></script>
    <script src="Scripts/jquery.atwho.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.caret.js" type="text/javascript"></script>
    <%--<script src="Scripts/IdeTokeninput201409161055.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        var loadQueryBuilder = false;
        var backButton = false;
        var backButtonId = '';
        //Database connector edit
        function DataCredentialBy(_bol) {
            if (_bol) {
                showModalPopUp('divCredentialBymobile', 'Enter Credential', 300);
                $('[id$=txtUsername]').val('');
                $('[id$=txtPassword]').val('');
            }
            else {
                $('#divCredentialBymobile').dialog('close');
            }
        }
        function Credentialvalidation() {
            var user = $('[id$=txtUsername]').val();
            var password = $('[id$=txtPassword]').val();
            if (user == '') {
                alert('Please enter username');
            }
            else if (password == '') {
                alert('Please enter password');
            }
            if (user != '' && password != '') {
                DataCredentialBy(false);

                return true;
            }
            else {
                return false;
            }
        }

        function Btnstoreprocedure() {

            $('[id$=<%= btnstoreprocedureID.ClientID %>]').click();


        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div id="CompletePage"  style="padding: 10px;">
        <section style="margin: 5px 5px 5px 3px;">
            <asp:Panel ID="Paneldbobjects" CssClass="repeaterBox" runat="server">
                <asp:Panel ID="PaneldbobjectsHeader" CssClass="repeaterBox-header" runat="server">
                    <div>
                        <asp:Label ID="lbldbobject" runat="server" Text="<h1>Database Objects</h1>">
                        </asp:Label>
                    </div>
                    <div style="position: relative; top: 10px; right: 15px;">
                        <asp:LinkButton ID="lnkadd" runat="server" Text="Add" CssClass="repeaterLink fr"
                            OnClientClick="EditIndex = undefined; isCookieCleanUpRequired('false');AddIdedata();return Adddiscardtype();" OnClick="lnkAddNewDbCommand_Click" ></asp:LinkButton>                          
                    </div>
                </asp:Panel>
                <div id="divdbobjectComplete" style="padding-top: 5px">
                    <asp:UpdatePanel runat="server" ID="updbobject" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divdbobjleft" class="g5" style="width:47%;padding:0 !important">
                                <div id="divdbdataobject" class="g12" style="padding:0 !important;" >
                            

                            
                                </div>
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" ID="updAddDbCommand" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnDbCmdSwitchToAdv" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <div id="divright" runat="server" class="g7" style="width:49% !important">
                                    <div style="width: 98%; padding: 0px;">
                                        <div id="DbCommandDetails">
                                            <div id="viewinfo" class="hide">
                                                <div class="modalPopUpDetHeaderDiv" id="ideheaderobj">
                                                    <div class="modalPopUpDetHeader" id="divmsgheader">
                                                        <asp:Label ID="lblDbCmd_Name" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <div style="float: right;">
                                                        <div style="float: left;">
                                                            <asp:UpdatePanel runat="server" ID="updDeleteDbCmd" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                <div class="FLeft">
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Copy" 
                                                                    OnClientClick="isCookieCleanUpRequired('false');opencopyobject(true); return false;"></asp:LinkButton>
                                                                </div>
                                                                <div style="float: left;"> &nbsp;|&nbsp;</div>
                                                                <div class="FLeft">
                                                                    <asp:LinkButton ID="lnkeditbtn" runat="server" Text="Edit" OnClick="lnkeditbtn_Click"
                                                                        OnClientClick="isCookieCleanUpRequired('false');ImageDivClear();"></asp:LinkButton>
                                                                         <asp:HiddenField ID="hideditdone" runat="server" />
                                                                </div>
                                                                <div style="float: left;"> &nbsp;|&nbsp;</div>
                                                                   <div class="FLeft">
                                                                   <asp:Button ID="savebtn" runat="server" OnClick="savebtn_Click" style="display:none"/>
                                                                    <asp:LinkButton ID="lnktest" runat="server" Text="Test" 
                                                                        OnClientClick="isCookieCleanUpRequired('false');dialogtest();return false;"></asp:LinkButton>
                                                                        <asp:HiddenField ID="hdflp" runat="server" />
                                                                </div>
                                                                
                                                                <div style="float: left;">&nbsp;|&nbsp;  </div>
                                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClick="lnkDelete_Click" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modalPopUpDetRow" id="ideviewinfo" style="border: none; padding: 0px 0px 0px 0px;">
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Description</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbCmdDesc" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Source</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbCmd_ConnName" runat="server" Text=""></asp:Label>
                                                            <asp:HiddenField ID="hidconid" runat="server" />
                                                            <asp:HiddenField ID="hidname" runat="server" />
                                                            <asp:HiddenField ID="hidperconnectionname" runat="server" />
                                                            <asp:HiddenField ID="hdfDbType1" runat="server" />
                                                            <asp:HiddenField ID="hidlblinputpara" runat="server" />                                                          
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Command Type</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbCmd_CmdType" runat="server" Text=""></asp:Label>
                                                            <asp:HiddenField ID="hdfDbType" runat="server" />                                                           
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>                                              
                                                  
                                                            <asp:Label ID="lblDbCmd_TableName" CssClass="hide" runat="server" Text=""></asp:Label>
                                                     
                                                    </div>
                                                    <div class="clear"> </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Input(s)
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblinput" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Sql Query</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbCmd_Query" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="clear"></div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Output(s) </div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lbloutput" runat="server" Text=""></asp:Label>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>                                                   
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Created By</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblcreatedby" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Updated By</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblupdatedby" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>   
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">Data Cache</div>
                                                        <div class="modalPopUpDetColumn2">:</div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDataCache" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>                                                    
                                                    <div >
                                                        <div class="modalPopUpDetRow">
                                                           <%-- <div class="FLeft">
                                                                <img id="imgDbCmdApp" alt="Expand" src="//enterprise.mficient.com/images/expand.png"
                                                                    style="cursor: pointer;" title="Expand" onclick="imgDbCmdAppClick(this);" /></div>
                                                            <div class="modalPopUpDetColumn1" style="padding-top: 0px; width: 200px;">--%>
                                                                Apps using this object :
                                                            <%--</div>--%>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div class="CmdAppRoot" style="overflow:hidden;">
                                                            <div id="DbCmdAppDiv"  style="overflow:hidden;">
                                                                This object is not used any where.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="EditDbCommandDiv">
                                            <div class="DbCmdDiv">
                                                <div class="Headerfontsize" id="add">Add New Data Object</div>
                                                <div class="Headernewfontsize" id="Edit">Edit Data Object</div>
                                                <div class="DbConnRightDiv"></div>
                                                <div class="clear">
                                                </div>
                                                <div id="EditDbcmd1" class="DbCmdLeftDiv" runat="server">
                                                    Object Name
                                                </div>
                                                <div id="EditDbcmd2" class="DbCmdRightSmallDiv" runat="server">
                                                    <asp:TextBox ID="txtDbCommand_CommandId" runat="server" Visible="false"></asp:TextBox>
                                                    <asp:TextBox ID="txtDbCommand_CommandName" runat="server" class="InputStyle" Width="180px"></asp:TextBox>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="editCmdDescDiv" runat="server" class="DbCmdRow">
                                                    <div class="DbCmdLeftDiv">
                                                        Description
                                                    </div>
                                                    <div class="DbCmdRightDescDiv">
                                                        <asp:TextBox ID="txtDBCmdDesc" runat="server" TextMode="MultiLine" class="CmdDescInputStyle1"
                                                            Rows="2"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                               
                                               
                                                <div id="EditDbcmdConnDiv" runat="server" class="DbCmdRow">
                                                    <div class="DbCmdLeftDiv selects">
                                                        Source
                                                    </div>
                                                    <div class="DbCmdRightDiv SelectDiv">
                                                         <asp:DropDownList ID="ddl_DbConnactor" runat="server" class="UseUniformCss" onchange="DBCmd_Conn_SelectedIndexChange(); return false;">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                                 <div id="ddlCommandTypeDiv" runat="server"  style="width: 100%; float: left; display:none">
                                                    <div class="DbCmdLeftDiv selects">
                                                        Command Type
                                                    </div>
                                                    <div class="DbCmdRightDiv SelectDiv">
                                                        <asp:DropDownList ID="ddlCommandType" runat="server" class="UseUniformCss" onchange="CommandType('1');">
                                                            <asp:ListItem Value="-1">Select Command Type</asp:ListItem>
                                                            <asp:ListItem Value="1">SELECT</asp:ListItem>
                                                            <asp:ListItem Value="2">INSERT</asp:ListItem>
                                                            <asp:ListItem Value="3">UPDATE</asp:ListItem>
                                                            <asp:ListItem Value="4">DELETE</asp:ListItem>
                                                            <asp:ListItem Value="5">STORE PROCEDURE</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="hidstoreproc" runat="server" />
                                                        <asp:HiddenField ID="hdConnection" runat="server" />
                                                        <asp:HiddenField ID="hidWsAuthencationMeta" runat="server" />                                                        
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div id="divstoreprocedure" runat="server"  style="width: 100%; float: left; display:none">
                                                    <div class="DbCmdLeftDiv selects">
                                                        Stored Procedure 
                                                    </div>
                                                    <div class="DbCmdRightDiv SelectDiv">
                                                        <asp:DropDownList ID="drpstoreprocedure" runat="server" class="UseUniformCss"  
                                                            AutoPostBack="true" 
                                                            onselectedindexchanged="drpstoreprocedure_SelectedIndexChanged">
                                                        </asp:DropDownList>                                                        
                                                    </div>
                                                </div>
                                                <div class="clear" id="divsp" runat="server">
                                                </div>
                                                <div id="btnDbcmdParaAddDiv" runat="server"   class="DbCmdRow" style="margin-top: 10px; display:none">
                                                    <div class="DbCmdLeftDiv">
                                                        Input Parameters
                                                    </div>
                                                    <div class="DbCmdRightDiv">
                                                        <asp:LinkButton ID="lnkInputButton"  runat="server" OnClientClick="SubProcDbCmdAddPara(true);AddParameterInDbCommand();UnformDatabaseObject();return false;"
                                                            CssClass="lnkNoBorder"><img src="css/images/icons/dark/pencil.png" alt="Edit" title="Edit"/></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div id="clearDivAddParameter" runat="server"  class="clear">
                                                </div>

                                                <div id="lblInputParametersDiv"  class="InputStyle" runat="server"
                                                    style="padding-top: 5px; padding-bottom: 5px; margin-top: 5px; float: left; overflow-y:scroll; display:none">
                                                    <label id="lblInputParameters">
                                                        No Parameters defined yet</label>
                                                </div>
                                                <div id="divParameterInfo" runat="server" style="display:none;padding-left:5px;padding-top:2px; font-size:10px; font-style:italic;" class="DbCmdRow">
                                                </div>

                                                 <div id="clearDivInputParaDiv" runat="server">
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="clear">      </div>
                                                <div id="divSelectSqlQueryLabel" runat="server"  class="DbCmdRow">
                                                    <div class="DbCmdLeftDiv">
                                                        Query
                                                    </div>
                                                    <div class="DbCmdRightLinkDiv" id="divadvacequerybuilder" style="display:none">
                                                        <asp:LinkButton ID="btnDbCmdSwitchToAdv" runat="server" CssClass="LinkMouseOver"
                                                            Text="Advanced Query Builder" OnClick="btnDbCmdSwitchToAdv_Click"
                                                            Font-Size="Smaller" OnClientClick="BtnAdvQbClick();" />
                                                        <div style="display: none">
                                                            <asp:Button ID="btnDbCmdSwitchToAdvhidden"   PostBackUrl="~/querybuilder/AdvancedQueryBuilder1.aspx"
                                                                runat="server" Text="Advanced Query Builder" />                                                              
                                                        </div>
                                                    </div>
                                                    <div class="DbCmdRightLinkDiv" id="QueryLink" style="display:none">
                                                        <asp:LinkButton ID="lnkQueryDefined" runat="server" CssClass="LinkMouseOver" OnClientClick="OpenPanelInsertCommands(true);AddParameterInDbCommand();"
                                                            Text="Query Builder" Font-Size="Smaller" onclick="lnkQueryDefined_Click"  />
                                                     </div>

                                                </div>
                                                <div class="clear"></div>
                                            

                                                <div id="divInsertSqlQueryLabel" runat="server" style="display:none"  class="DbCmdRow">
                                                    <div class="DbCmdLeftDiv">
                                                    
                                                    </div>
                                                </div>

                                                <div class="clear"></div>                                               
                                                <div class="DbCmdCompleteDiv" id="divtxtdbsqquery" style="margin-top:0px;">
                                                    <asp:UpdatePanel runat="server" ID="updInsertSqlQryTxt" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtDbCommand_SqlQuery" runat="server"  TextMode="MultiLine"  class="InputStyle"
                                                                 Height="80px" onchange="txtDbCommandSqlQueryChange();" Width="99%"></asp:TextBox>
                                                            <asp:HiddenField ID="hdfInsertQueryPara" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="clear"></div>
                                                <div id="SqlQueryOutputDiv" runat="server"   style="width: 100%; float: left;display:none">
                                                    <asp:UpdatePanel runat="server" ID="updRefreshOutPut" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div id="Div71" runat="server" class="DbCmdRow">
                                                                <div class="DbCmdLeftDiv" style="padding-top:10px;">
                                                                    Output Columns
                                                                </div>
                                                                <div class="DbCmdRightDiv"  id="divoutputquerylinks"  style="display:none">
                                                                 <asp:HiddenField ID="hidimageoutput" runat="server" />
                                                                    <asp:LinkButton ID="lnRefButton" runat="server" OnClientClick="GetAllAutoboxMValue();isCookieCleanUpRequired('false');"
                                                                        OnClick="imgRefreshPara_Click" CssClass="lnkNoBorder"><img src="images/refresh.png" class="imgRefresh" alt="Refresh" title="Refresh"/></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="DbCmdCompleteDiv"  style="margin-top:0px;">
                                                                <asp:TextBox ID="txtDbCommand_SqlQueryPara" runat="server" class="InputStyle" Width="99%"></asp:TextBox>
                                                            </div>
                                                            <div class="clear" >
                                                            </div>
                                                            <div id="Div8" runat="server" class="DbCmdRow" style="display:none">
                                                               <div class="DbCmdLeftDiv">
                                                                   Image Columns
                                                                </div>
                                                            <div style="float:left;width:auto;margin-top:12px"> <asp:Label ID="lblImageColumns" runat="server" ></asp:Label> </div>
                                                            <div class="DbCmdRightDiv" style="width:14% !important">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="imageoutputparameter();return false;"
                                                                CssClass="lnkNoBorder"><img src="css/images/icons/dark/pencil.png" alt="Edit" title="Edit"/></asp:LinkButton>
                                                               <asp:HiddenField ID="hidimagecoloums" runat="server" />
                                                               <asp:HiddenField ID="hidlblImageColumns" runat="server" />
                                                               <asp:HiddenField ID="hidcoloumtype" runat="server" />
                                                               <asp:HiddenField ID="hidnoexecutecondtion"  runat="server" />
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </div>
                                                <div class="clear"></div> 
                                              

                                                   <div id="divdonotexecute" runat="server" style="display:none" class="DbCmdRow">
                                                        <div class="DbCmdLeftDiv">Do Not Execute</div>
                                                        <div style="float:left;width:auto;margin-top:12px"> <asp:Label ID="Labeldonotexecut" runat="server" ></asp:Label> </div>
                                                        <div class="DbCmdRightDiv" style="width:14% !important">
                                                           <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="DonotExecute();return false;"
                                                                CssClass="lnkNoBorder"><img src="css/images/icons/dark/pencil.png" alt="Edit" title="Edit"/></asp:LinkButton>
                                                    </div>
                                                    </div>

                                                      <div class="clear"></div>

                                                     <div id="divcatchedetails" style="display:none">

                                                    <div id="Catchmgrtime">
                                                        <div class="DbCmdLeftDiv selects">Data Cache</div>
                                                        <div class="DbCmdRightDiv SelectDiv">
                                                            <asp:DropDownList ID="dd1DataCatch" runat="server" onchange="Hide(this)"  class="UseUniformCss">
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes (Absolute Time)</asp:ListItem>
                                                                <asp:ListItem Value="2">Yes (Relative Time)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div id="CatchEvery" class="hidediv">
                                                        <div class="DbCmdLeftDiv selects">Frequency</div>
                                                        <div class="DbCmdRightDiv SelectDiv">
                                                            <asp:DropDownList ID="dd1AbMonth" runat="server" onchange=" Abmonth(this)"  EnableViewState="true" class="UseUniformCss">
                                                            <asp:ListItem Value="0">Hour</asp:ListItem>
                                                            <asp:ListItem Value="1">Day</asp:ListItem>
                                                            <asp:ListItem Value="2">Week</asp:ListItem>
                                                            <asp:ListItem Value="3">Month</asp:ListItem>
                                                            <asp:ListItem Value="4">Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <div id="Catcmin" class="hidediv" style="float:left;width:inherit !important" >
                                                                <div style="float:left; margin-top:10px;margin-right:10px;">At Min</div>
                                                                 <div class="SelectDiv" style="float:left;">
                                                                    <asp:DropDownList ID="dd1AbHr" runat="server" class="UseUniformCss">
                                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                                        <asp:ListItem Value="10">10</asp:ListItem>
                                                                        <asp:ListItem Value="15">15</asp:ListItem>
                                                                        <asp:ListItem Value="30">30</asp:ListItem>
                                                                        <asp:ListItem Value="45">45</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="AbEvDayDiv" class="hidediv" style="float:left;width:inherit !important">
                                                                <div  style="float:left; margin-top:10px;margin-right:10px;">At</div>
                                                                <div class="SelectDiv"  style="float:left;">                     
                                                                    <asp:DropDownList ID="dd1Abhh" runat="server" class="UseUniformCss">                                                       
                                                                    </asp:DropDownList>
                                                                <div style="float:left; margin-top:10px;margin-right:10px;" >Hr. :</div>
                                                                    <asp:DropDownList ID="ddlAbMM" runat="server" class="UseUniformCss">                                                      
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div  style="float:left; margin-top:10px;margin-right:10px;">Min.</div>
                                                            </div>
                                                            <div id="AbEvWkDiv" class="hidediv"  style="float:left;width:inherit !important">
                                                                <div  style="float:left; margin-top:10px;margin-right:10px;">On </div>
                                                                <div class="SelectDiv" style="float:left;">                                                     
                                                                    <asp:DropDownList ID="ddlAbDay" runat="server" class="UseUniformCss">
                                                                        <asp:ListItem Value="1">MON</asp:ListItem>
                                                                        <asp:ListItem Value="2">TUE</asp:ListItem>
                                                                        <asp:ListItem Value="3">WED</asp:ListItem>
                                                                        <asp:ListItem Value="4">THU</asp:ListItem>
                                                                        <asp:ListItem Value="5">FRI</asp:ListItem>
                                                                        <asp:ListItem Value="6">SAT</asp:ListItem>
                                                                        <asp:ListItem Value="7">SUN</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="AbEvMnDiv" class="hidediv"  style="float:left;width:inherit !important">
                                                                <div  style="float:left; margin-top:10px;margin-right:10px;">On Date</div>
                                                                <div class="SelectDiv" style="float:left;">
                                                                    <asp:DropDownList ID="dd1AbMnDay" runat="server" class="UseUniformCss">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="AbEvYrDiv"  class="hidediv"  style="float:left;width:inherit !important">
                                                                <div  style="float:left; margin-top:10px;margin-right:10px;">On</div>
                                                                <div class="SelectDiv" style="float:left;">                                                       
                                                                    <asp:DropDownList ID="dd1AbMnthDay" runat="server" class="UseUniformCss">   
                                                                    </asp:DropDownList>
                                                                    <div style="float:left; margin-top:10px;margin-right:10px;" >Day /</div> 
                                                                    <asp:DropDownList ID="ddlAbMnth" class="UseUniformCss"   runat="server" style="width: 90px;
                                                                    margin-left: 5px;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                 <div  style="float:left; margin-top:10px;margin-right:10px;"> Month</div>
                                                             </div>
                                                        </div>
                                                    </div>                                                    
                                                    <div id="RltDiv" class="hidediv">
                                                        <div id="divcatch" class="DbCmdLeftDiv selects">Every</div>
                                                        <div class="DbCmdRightDiv SelectDiv">    
                                                            <div id="RltDivmin"  class="hidediv">
                                                        
                                                                <div class="SelectDiv" style="float:left;">
                                                                   <asp:DropDownList id="drprltmin"  class="UseUniformCss" runat="server"   style="width: 65px">                                                       
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="RltHour" class="hidediv">
                                                           
                                                                <div class="SelectDiv" style="float:left;">
                                                                   <asp:DropDownList id="Drprlthour"  class="UseUniformCss" runat="server"   style="width: 65px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="RltDay" class="hidediv">
                                                       
                                                                <div class="SelectDiv" style="float:left;">
                                                                   <asp:DropDownList id="rltdayd"  class="UseUniformCss" runat="server"   style="width: 65px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="RltWeek" class="hidediv">
                                                               
                                                                <div class="SelectDiv" style="float:left;">
                                                                   <asp:DropDownList id="raltweeks"  class="UseUniformCss" runat="server"   style="width: 65px">                                                        
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div id="RltMonth" class="hidediv">
                                                                
                                                                <div class="SelectDiv" style="float:left;">
                                                                   <asp:DropDownList id="drprltmonth"  class="UseUniformCss" runat="server"   style="width: 65px">                                                        
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>    
                                                                                                            
                                                            <asp:DropDownList id="ddlRlt"  class="UseUniformCss" runat="server"  onchange="Relative(this)">
                                                            <asp:ListItem Value="0">Minute</asp:ListItem>
                                                            <asp:ListItem Value="1">Hour</asp:ListItem>
                                                            <asp:ListItem Value="2">Day</asp:ListItem>
                                                            <asp:ListItem Value="3">Week</asp:ListItem>
                                                            <asp:ListItem Value="4">Month</asp:ListItem>
                                                            </asp:DropDownList> 
                                                        </div>
                                                    </div> </div>                             
                                            
                                               <asp:HiddenField ID="hidcatchexfrequency" runat="server" />
                                               <asp:HiddenField ID="hidcatchexcondition" runat="server" />
                                               <asp:HiddenField ID="hidCatchdetails" runat="server" />
                                            <div class="clear"></div>
                                            <div class="SubProcborderDiv">
                                                    <div class="SubProcBtnDiv" align="center">
                                                        <asp:HiddenField ID="hiddbobjdataset" runat="server" />
                                                        <asp:HiddenField ID="hiddbobjdatasource" runat="server" />
                                                        <asp:Button ID="btnDbCommand_Save" runat="server" Text="  Save  " OnClientClick="return dataobjectvalidation();" OnClick="btnDbCommand_Save_Click"
                                                            class="InputStyle"  />
                                                  
                                                          <asp:Button ID="btnDbCommand_Reset" runat="server"  Text="  Cancel  "       class="InputStyle" OnClick="btnDbCommand_Reset_Click" />
                                                    
                                                       <asp:Button ID="btnDbBackClick" runat="server" Text="  Cancel  " Visible="false"  CssClass="InputStyle"  OnClientClick="PreventCancel();$('[id$=hideditdone]').val('');return false;" />
                                                </div>
                                                <asp:HiddenField ID="hdfDbCmdType" runat="server" />
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel runat="server" ID="updDbCmdParaJson" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdfDbCmdPara" runat="server" />
                        <asp:HiddenField ID="hdfIsinsertQB" runat="server" /> 
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <div id="divCredentialBymobile" class="hide">
                <asp:UpdatePanel ID="updatecredentialwithoutsave" runat="server" UpdateMode="Conditional">
                    <ContentTemplate><asp:Literal ID="ltCrd" Visible="false" runat="server"></asp:Literal>
                        <div class="DbConnLeftDiv " id="div2">
                            Username
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 12px" id="div3">
                            <asp:TextBox ID="txtUsername" runat="server" Width="90%" class="InputStyle"
                                MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="DbConnLeftDiv" id="div5">
                            Password
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 12px" id="div6">
                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="90%"
                                class="InputStyle" MaxLength="50"></asp:TextBox>
                                
                        </div>
                        <div class="clear">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:Button ID="btnSaveCredential" runat="server" Text="  Next  " OnClientClick="return Credentialvalidation();" OnClick="btnSaveCredential_Click"
                                    class="InputStyle" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:HiddenField ID="hdfOutCol" runat="server" />        
            <asp:HiddenField ID="hdfAutoboxMannulPara" Value="9" runat="server" />
            <asp:HiddenField ID="hfs" runat="server" />
            <asp:HiddenField ID="hfbid" runat="server" />
            <asp:HiddenField ID="hdfSql" runat="server" />
            <asp:HiddenField ID="hdfIsMplugIn" runat="server" />
            <asp:HiddenField ID="hiddbobjectsucess" runat="server" />
            <input type="hidden" id="hfsQB" name="hfsQB" />
       
            <input type="hidden" id="hdfOutColQB" name="hdfOutColQB" />
            <input type="hidden" id="hfDbCmdSqlQueryQB" name="hfDbCmdSqlQueryQB" />
            <input type="hidden" id="hfDbCon" name="hfDbCon" />
            <input type="hidden" id="hfDbType" name="hfDbType" />
            <div>
                <asp:Button ID="brnBackToUiDetails" PostBackUrl="~/Home.aspx" runat="server" CssClass="hide" />
            </div>
            <div id="SubProcDeleteCmdConfrmation">
                <div>
                    <div>
                        <asp:UpdatePanel runat="server" ID="UpdCmdDelCnf" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:HiddenField ID="hdfCmdDelId" runat="server" />
                                <asp:HiddenField ID="hiddbtypetrpe" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="MarginT10"></div>
                    <div class="MessageDiv"><a id="aMsgCmdDelCmf"></a></div>
                    <div class="MarginT10"></div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn" align="center">
                            <asp:UpdatePanel runat="server" ID="UpdDeleteCommand" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="Button23" runat="server" Text="  Yes  " OnClientClick="SubProcDeleteCmdConfrmation(false,'');EditIndex=undefined;"
                                        OnClick="btnCmdDelYes_Click" CssClass="InputStyle" />
                                    <input type="button" value="  No  " onclick="SubProcDeleteCmdConfrmation(false,'');"
                                        class="InputStyle" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

<asp:Button ID="btnstoreprocedureID" OnClick="btn_storeprocedureID" runat="server" style="visibility: hidden; display: none;" />

            <div id="SqlQueryBuilding" class="hide">
            <div id="divtablename" runat="server" style="width: 100%; float: left;padding-bottom:1em;">
            <div class="DbCmdLeftDiv">Table Name</div>
            <div class="DbCmdRightDiv SelectDiv">
                <%--<asp:HiddenField ID="hidintellegence" runat="server" />--%>
                <asp:UpdatePanel ID="updatetable" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DropDownList ID="Drppoptable" runat="server" class="UseUniformCss" OnSelectedIndexChanged="Drppoptable_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="-1">Select Table Name</asp:ListItem>
                    </asp:DropDownList>
                </ContentTemplate>
                </asp:UpdatePanel>     
            </div>                     
            <div class="clear"> </div>
            <div class=" DbCmdRightDivtest" id="divrepeterupdate">
                <asp:UpdatePanel runat="server" ID="updDbCmdRptInsert" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlDbCmdColRpt" CssClass="popUprepeaterBox" runat="server" Visible="false">
                            <asp:Panel ID="Panel16" CssClass="popUprepeaterBox-header" runat="server">
                                <div>
                                    <asp:Label ID="Label29" runat="server" Text="<h1>Table Columns</h1>"></asp:Label>
                                </div>
                                <div style="position: relative; top: 12px; right: 15px;">
                                    <asp:LinkButton ID="lnkGenrateQuery" runat="server" Text="Generate Query" CssClass="repeaterLink fr"
                                        OnClick="lnkGenrateQuery_Click" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlCmdTabl" CssClass="DbTblColRptDiv" runat="server">
                                <asp:Repeater ID="rptDbCommandTblCol" runat="server" OnItemDataBound="rptDbCommandTblCol_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterPopUpTable" id="tblrep">
                                            <thead>
                                                <tr>
                                                  
                                                    <th>
                                                        Column Name
                                                    </th>
                                                    <th>
                                                        Parameter
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                
                                                <td>
                                                    <asp:Literal ID="lit_Col" runat="server" Text='<%# Eval("column_name") %>' />
                                                    <asp:Literal ID="lit_ColType" runat="server" />
                                                     <asp:Literal ID="lit_Type" runat="server" Visible="false" Text='<%# Eval("column_type") %>' />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_Prarameter" CssClass="testy" runat="server" Width="120px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                              
                                                <td>
                                                    <asp:Literal ID="lit_Col" runat="server" Text='<%# Eval("column_name") %>' />
                                                    <asp:Literal ID="lit_ColType" runat="server" />
                                                    <asp:Literal ID="lit_Type" runat="server" Visible="false" Text='<%# Eval("column_type") %>' />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_Prarameter" CssClass="testy" runat="server" Width="120px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="clear"> </div>
            <div></div>
    <asp:UpdatePanel runat="server" ID="UpdatePanel32" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="DbCmdLeftDiv" id="divwhere" runat="server" visible="false"> WHERE</div>
            <div class="clear"></div>
            <div class="DbCmdRightDivtest22"  id="divctlwhere"  runat="server"  visible="false">
                <asp:TextBox ID="txtDbCommand_SqlQueryWH" runat="server" TextMode="MultiLine" CssClass="CmdDescInputStyle12" Rows="2"  >
                </asp:TextBox>  
             </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="clear"></div>
    </div>
             <div class="clear">
             </div>
            </div>
            <input type="hidden" id="hdfMannualOutColQB" name="hdfOutColQB" />
            <div id="SubProcDbCmdAddPara">
                <div style="margin-bottom:15px !important">
                    <div id="AddParameter" class="AddParameter">
                        <div style="">
                            <div class="QPHeader">
                                <div class="QPTextHeader" align="center">
                                    <div class="QPHeaderText">
                                        Parameter
                                    </div>
                                </div>
                                <div class="QPTypeHeader hide">
                                    <div class="QPHeaderText" align="center">
                                        Select Type
                                    </div>
                                </div>
                                <div class="QPDelete">
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="AddParaDiv" class="QPContent">
                            </div>
                            <div class="clear">
                            </div>
                            <div class="QPContent">
                                <div class="QPContentRow">
                                    <div class="QPContentText">
                                        <input id="txtAddParaName" type="text" class="txtQPOption" maxlength="30" />
                                    </div>
                                    <div class="QPContentType hide">
                                        <select id="ddlAddParaType" class="ddlQPOption">
                                            <option value="String">String</option>
                                            </select>
                                    </div>
                                    <div class="QPContentDelete" align="center">
                                        <img id="imgAddQueryPara" alt="" src="//enterprise.mficient.com/images/add.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ImageColumnPopup" class="hide">
                <div style="margin-bottom:15px !important" id="divimageproperties">
                    <div class="QPHeader" id="divheader" style="width:300px">
                                <div class="QPTextHeader" align="center" style="width:120px !important">
                                    <div class="QPHeaderText">
                                       Image Parameter
                                    </div>
                                </div>
                                <div class="QPTypeHeader">
                                    <div class="QPHeaderText" align="center">
                                         Format
                                    </div>
                                </div>
                                <div class="QPDelete">
                                </div>
                            </div>
                    <div class="QPContent" id="divcontent" style="border-top:0px !important">
                                <div class="QPContentRow">
                                    <div class="QPContentText" style="width:120px !important">
                                         <select id="drpimageparameter" class="ddlQPOption" style="width:118px !important">
                                         </select>
                                    </div>
                                    <div class="QPContentType">
                                        <select id="drpselectimageparameter" class="ddlQPOption" style="width:118px !important">
                                            <option value="1" selected="selected">Public URL </option>
                                              <option value="2">Local URL </option>
                                               <option value="3">Local File Path </option>
                                                <option value="4">Base 64 </option>
                                            </select>
                                    </div>
                                    <div class="QPContentDelete" align="center" style="margin-right: 2px !important">
                                        <img id="img1" alt="" src="//enterprise.mficient.com/images/add.png" onclick="ImageAddbuttonClick();" />
                                    </div>
                                </div>
                            </div>
                    <div class="SubProcborderDiv">
                   <div class="SubProcBtnMrgn" style="text-align:center">
                   <button type="button"  class="InputStyle"  onclick="displayimageparameter();ImageColumnPopup(false);">Save</button>
                   </div>
                   </div>

                </div>
                <div id="divimagepropertiesnoparameter"></div>
            </div>

            <div id="dialogPara">
                <div id="ShowDnCmdParaDiv">
                </div>
           </div>
           
            <div id="ShowDelete" class="dataTables_wrapper no-footer"  style="display:none;">
                <asp:UpdatePanel ID="UPDEL" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="Div4">
                        <p>This data object is using at following places</p>
                        </div>
                        <div id="divoverflow" style="max-height:150px; margin-bottom:10px; overflow-y:auto !important">
                        <asp:Repeater ID="RepDelete" runat="server">
                            <HeaderTemplate>
                                <table class="display dataTable no-footer">
                                    <thead>
                                        <tr>                                      
                                            <th>
                                                App
                                            </th>
                                            <th>
                                                Views
                                            </th>
                                            <th>
                                                Forms
                                            </th>
                                                                                      
                                        </tr>
                                    </thead>

                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="odd">                   
                                        <td >
                                            <asp:Literal ID="litApp" runat="server" Text='<%# Eval("app") %>' />
                                        </td>                                  
                                        <td>
                                            <asp:Literal ID="litOpertion" runat="server" Text='<%# Eval("inuse") %>'  />
                                        </td>                                  
                                        <td>
                                            <asp:Literal ID="litform" runat="server" Text='<%# Eval("forms") %>' />
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tbody>
                                    <tr class="even">       
                                        <td>                                     
                                            <asp:Literal ID="litApp" runat="server"  Text='<%# Eval("app") %>' />
                                        </td>
                                        <td>
                                            <asp:Literal ID="litOpertion" runat="server" Text='<%# Eval("inuse") %>'  />
                                        </td>
                                        <td>
                                            <asp:Literal ID="litform" runat="server" Text='<%# Eval("forms") %>'  />
                                        </td>                                     
                                    </tr>
                                </tbody>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </div>                                    
            <div id="SubProcBoxMessage">
                <div>
                    <div class="MessageDiv">
                        <a id="a2">Please check following points : </a>
                        <br />
                    </div>
                    <div style="margin-top: 5px">
                     <a id="aMessage"></a>
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                            <input id="btnOkErrorMsg" type="button" value="  OK  " onclick="SubProcBoxMessage(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="divWaitBox" class="waitModal">
                <div id="WaitAnim">
                    <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                        AlternateText="Please Wait" BorderWidth="0px" />
                </div>
            </div>
            <div id="SubProcConfirmBoxMessage">
                <div>
                    <div>
                        <div class="ConfirmBoxMessage1">
                            <a id="aCFmessage"></a><a id="aCfmMessage"></a><a id="aCfmUpdateMessage"></a>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn" align="center">
                            <input id="btnCnfFormSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="divcopyobject" class="hide" style="height:100px">
                <div id="Div1">
                    <div id="divcopysameobject" class="DbCmdRow">
                    <div class="DbConnLeftDiv">Object Name </div>
                        <div class="DbConnRightDiv">
                        <asp:TextBox ID="txtcopyobject" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn" align="center">
                            <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                <ContentTemplate>
                                <asp:Button ID="Button1" runat="server" Text="  Yes  " OnClientClick="return userDataValid();"  OnClick="CommandCopy_Save_Click" CssClass="InputStyle" />
                                <input type="button" value="  No  " onclick="opencopyobject(false);"
                                    class="InputStyle" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="SubProobjectChange" class="hide">
                <asp:UpdatePanel ID="updatediscard" runat="server">
                <ContentTemplate>
                    <div class="MessageDiv">
                            <a id="aFormChangeMsg" class="DefaultCursor">
                             Discard any changes made?
                             </a>
                        </div>
                    <div class="SubProcborderDiv" style="margin-top:30px !important;">
                        <div class="SubProcBtnMrgn" align="center">
                            <asp:Button ID="btnfrmDiscard" runat="server" text="  Yes  " OnClientClick="$('#SubProobjectChange').dialog('close');return discardadd();" OnClick="btndiscardyesclick"
                                class="InputStyle" />
                            <asp:Button ID="btnsaveCurrentobject" runat="server"  class="InputStyle" text="  No  " OnClientClick="$('#SubProobjectChange').dialog('close');" OnClick="btnSaveDb_Click" />
                            <input id="btnCancelSaveCurrentForm" type="button" value="  Cancel " onclick="ChangeSelection();$('#SubProobjectChange').dialog('close');"
                                class="InputStyle" />
                        </div>
                    </div>
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div id="dialogtestPara">
                <div style="margin-bottom:15px !important" id="divdetails">
                   
                </div>
           </div>
           <div id="dialogdynamictable">
                <div >
                    <div id="Div7" class="AddParameter">
                            <table id="resulttbl"></table>
                            </div>
                    <div class="clear"></div>
                </div>
           </div>

           <div id="divdonotexecutedetails" style="display:none" >
             <div style="margin-bottom:15px !important" id="div9">
             <table id="tabexe" class="CntrlInits">
             
             
             </table>
             </div>
             <div class="SubProcBtnMrgn" align="center">
                  <asp:Button ID="btnsavenoexecuteprameter" runat="server" text="  Save  " OnClientClick="return DefinedexecutePaameter();" 
                                class="InputStyle"  />
                          </div>

           </div>
        </section>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
            prm.add_pageLoaded(prm_pageLoaded);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {

            hideWaitModal();
            isCookieCleanUpRequired('true');
            UnformDatabaseObject();
        }


        function prm_pageLoaded(sender, args) {
            if (loadQueryBuilder == true) {
                $('[id$=<%= btnDbCmdSwitchToAdvhidden.ClientID %>]').click();

                $('#shadow').show();
            }
            if (backButton == true) {
                $('[id$=<%= brnBackToUiDetails.ClientID %>]').click();
                $('#shadow').show();
            }
        }
    </script>
    <asp:HiddenField ID="hidIsCleanUpRequired" runat="server" />
    <asp:HiddenField ID="hdfPostBackPageMode" runat="server" />
    <asp:HiddenField ID="hidBackButtonClientId" runat="server" />
    <asp:HiddenField ID="hiddiscardcommadtype" runat="server" />
</asp:Content>
