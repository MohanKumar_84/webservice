﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using DataQueryParser;
using System.Globalization;
using ActiveDatabaseSoftware.ActiveQueryBuilder.Web.Control;
using ActiveDatabaseSoftware.ActiveQueryBuilder;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.IO;

namespace mFicientCP
{
    public partial class IdeDataObject : System.Web.UI.Page
    {
        string strhfs, strhfbid, SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "", strPostbackPageMode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPage mainMaster = Page.Master;

            HttpContext context = HttpContext.Current;
            bool IsDbCmpOpen = false;
            if (Page.IsPostBack)
            {
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                if (((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value != "")
                {
                    strPostbackPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value;
                    if (strPostbackPageMode == "dbobject")
                        ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value = "";
                }
            }
            else
            {
                if (((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value != "")
                {
                    ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value = "";
                }
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;
                string FileName = this.PreviousPage.AppRelativeVirtualPath;
                FileName = FileName.Substring(FileName.LastIndexOf("/") + 1);

                if (FileName == "AdvancedQueryBuilder1.aspx")
                {
                    IsDbCmpOpen = true;
                    hdfSql.Value = ((HiddenField)previousPageForm.FindControl("hfSql")).Value;
                    hdfInsertQueryPara.Value = ((HiddenField)previousPageForm.FindControl("hfSqlPara")).Value;
                    hdfOutCol.Value = ((HiddenField)previousPageForm.FindControl("hfOutCol")).Value;
                    hdfDbCmdPara.Value = ((HiddenField)previousPageForm.FindControl("hdfInputParam")).Value;
                    hdfAutoboxMannulPara.Value = ((HiddenField)previousPageForm.FindControl("hdfMannualParam")).Value;
                    hidWsAuthencationMeta.Value = ((HiddenField)previousPageForm.FindControl("hidWsAuthencationMeta")).Value;
                }
                else
                {

                }

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;


                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid))
            {
                if (String.IsNullOrEmpty(strhfs))
                {
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect(@"Default.aspx");
                    return;
                }
                return;
            }

            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            Utilities.SaveInContext(context, strhfs, strhfbid, out SubAdminid, out Sessionid, out CompanyId, out Adminid);

            //if (!string.IsNullOrEmpty(strhfs))
            //{
            //    string str = Utilities.DecryptString(strhfs);
            //    SubAdminid = str.Split(',')[0];
            //    CompanyId = str.Split(',')[3].ToLower();
            //    Sessionid = str.Split(',')[1];
            //    Adminid = str.Split(',')[2];
            //}
            DBcmdObjectTable();
            string strScript = "";
            if (!IsPostBack)
            {
                hdfIsinsertQB.Value = "0";
                BindDatabaseConnector(out strScript);
                CacheDropdownBind();
                if (hidWsAuthencationMeta.Value == "")
                    bindCreadential();

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AutoCompleteTextbox('');", true);
            }
            if (!IsPostBack)
                strPostbackPageMode = hdfPostBackPageMode.Value;
            else
                hdfPostBackPageMode.Value = strPostbackPageMode;
            if (hdfPostBackPageMode.Value == "dbobject")
            {
                Clear();
                hidconid.Value = "";
                ddl_DbConnactor_SelectedIndexChanged(null, null);
                hdfPostBackPageMode.Value = "";
            }
            if (IsDbCmpOpen) OpenObjectAfterQueryBuilder();

            if (strScript.Length > 0)
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), "dbConectiondetail=" + strScript + ";", true);
        }

        #region BindDataTable JSON
        private void DBcmdObjectTable()
        {
            try
            {
                GetDbCommand objGetDbCommand = new GetDbCommand(true, false, this.SubAdminid, "", "", this.CompanyId, "1");
                UpdateObjectXreference objref = new UpdateObjectXreference();
                DataTable dtApps = objref.GetAppsListForObjectReff(this.CompanyId);
                DataTable dt = new DataTable();
                ArrayList rows = new ArrayList();

                if (objGetDbCommand.StatusCode == 0)
                {
                    dt = objGetDbCommand.ResultTable;
                    dt.Columns.Add("DateTime", typeof(string));
                    dt.Columns.Add("Used", typeof(string));
                    foreach (DataRow row in dt.Rows)
                    {
                        row["CONNECTION_NAME"] = Convert.ToString(row["CONNECTION_NAME"]);
                        row["DB_COMMAND_NAME"] = Convert.ToString(row["DB_COMMAND_NAME"]);
                        row["DB_COMMAND"] = Convert.ToString(row["DB_COMMAND"]);
                        row["COLUMNS"] = Convert.ToString(row["COLUMNS"]);
                        row["PARAMETER"] = Convert.ToString(row["PARAMETER"]);
                        row["DateTime"] = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(row["UPDATED_ON"]));
                        row["Used"] = Utilities.SerializeJson<List<List<string>>>(Utilities.objectUsage(dtApps.Select("Command_ids like '%DB_" + row["DB_COMMAND_id"] + "%'"), row["DB_COMMAND_id"].ToString(), "DB_"));
                    }
                    DataTable dtSelectedColumns = dt.DefaultView.ToTable(false, "DB_COMMAND_NAME", "CONNECTION_NAME", "DB_COMMAND");
                    foreach (DataRow dataRow in dtSelectedColumns.Rows)
                    {
                        rows.Add(dataRow.ItemArray);
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(updbobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + MficientConstants.ERROR_DATABASE_CONNECTION + "');", true);
                hiddbobjdatasource.Value = Utilities.SerializeJson<ArrayList>(rows);
                hiddbobjdataset.Value = Utilities.ConvertdatetabletoString(dt);

                ScriptManager.RegisterStartupScript(updbobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"LoadDatabaseObjectData();UnformDatabaseObject();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(updbobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }
        }
        #endregion

        #region Save DB Command / DB Catch/Save Button Click
        protected void SaveDbCommand()
        {

            if (!SqlQueryValid())
            {
                return;
            }
            else
            {
                if (hidconid.Value.Length == 0)
                {
                    GetDbCommand objGetDbCommand = new GetDbCommand(false, false, this.SubAdminid, "", txtDbCommand_CommandName.Text, this.CompanyId, "1");
                    if (objGetDbCommand.StatusCode == 0 && objGetDbCommand.ResultTable.Rows.Count > 0)
                    {
                        if (ddlCommandType.SelectedValue == "5")
                            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Database object name already exist.');UnformDatabaseObject();SelectedStoreProcedure('" + drpstoreprocedure.SelectedValue + "');catchtype('" + dd1DataCatch.SelectedValue + "');absolute('" + dd1AbMonth.SelectedValue + "');relativetcatch('" + ddlRlt.SelectedValue + "');", true);
                        else
                            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Database object name already exist.');UnformDatabaseObject();catchtype('" + dd1DataCatch.SelectedValue + "');absolute('" + dd1AbMonth.SelectedValue + "');relativetcatch('" + ddlRlt.SelectedValue + "');", true);
                        return;
                    }
                    else
                    {
                        if (ddlCommandType.SelectedValue == "5")
                            txtDbCommand_SqlQuery.Text = drpstoreprocedure.SelectedItem.ToString();

                        AddNewDbCommand objAddNewDbCommand = new AddNewDbCommand(this.SubAdminid, txtDbCommand_CommandName.Text, ddl_DbConnactor.SelectedValue, txtDbCommand_SqlQuery.Text, hdfInsertQueryPara.Value, "MULTIPLE", Convert.ToInt32(ddlCommandType.SelectedValue), "", "", hdfDbCmdPara.Value, txtDbCommand_SqlQueryPara.Text, this.CompanyId, txtDBCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value, this.SubAdminid, Convert.ToInt64(DateTime.UtcNow.Ticks), Convert.ToInt64(DateTime.UtcNow.Ticks), hidimagecoloums.Value, hidnoexecutecondtion.Value);
                        if (objAddNewDbCommand.StatusCode == 0)
                        {
                            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcConfirmBoxMessageDB(true,'Saved',250);$('#aCfmMessage').html('" + "* Database object saved succsessfully." + "');$('#aCfmUpdateMessage').html('');$('#aCFmessage').html('');", true);
                            UpdateAllUpdatePanel();
                            AfterSave(true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');UnformDatabaseObject();", true);
                            return;
                        }
                    }
                }
                else
                {
                    GetDbCommand objGetDbCommand = new GetDbCommand(false, true, this.SubAdminid, hidconid.Value, txtDbCommand_CommandName.Text, this.CompanyId, "1");
                    if (objGetDbCommand.StatusCode == 0 && objGetDbCommand.ResultTable.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('Database object Name already exist.');UnformDatabaseObject();", true);
                        return;
                    }
                    else
                    {
                        if (ddlCommandType.SelectedValue == "3" || ddlCommandType.SelectedValue == "4")
                            txtDbCommand_SqlQuery.Text = txtDbCommand_SqlQuery.Text + (txtDbCommand_SqlQueryWH.Text.Trim().Length > 0 ? (" WHERE " + txtDbCommand_SqlQueryWH.Text) : "");
                        else if (ddlCommandType.SelectedValue == "5")
                            txtDbCommand_SqlQuery.Text = drpstoreprocedure.SelectedItem.ToString();
                        else { }

                        UpdateDbCommand objUpdateDbCommand = new UpdateDbCommand(hidconid.Value, this.SubAdminid, txtDbCommand_CommandName.Text, ddl_DbConnactor.SelectedValue, txtDbCommand_SqlQuery.Text, hdfInsertQueryPara.Value, "MULTIPLE", ddlCommandType.SelectedValue, "", "", hdfDbCmdPara.Value, txtDbCommand_SqlQueryPara.Text, this.CompanyId, txtDBCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value, Convert.ToInt64(DateTime.UtcNow.Ticks), hidimagecoloums.Value, hidnoexecutecondtion.Value);
                        if (objUpdateDbCommand.StatusCode == 0)
                        {
                            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcConfirmBoxMessageDB(true,'Saved',250);$('#aCfmUpdateMessage').html('" + "* Database object update succsessfully." + "');$('#aCfmMessage').html('');$('#aCFmessage').html('');", true);
                            UpdateAllUpdatePanel();
                            AfterSave(false);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');UnformDatabaseObject();", true);
                            return;
                        }
                    }
                }

            }
        }
        protected void SaveCache()
        {
            if (dd1DataCatch.SelectedValue.ToString() == "0")
            {
                hidcatchexfrequency.Value = "0";
                hidcatchexcondition.Value = "1";
            }
            else if (dd1DataCatch.SelectedValue.ToString() == "1")
            {
                if (dd1AbMonth.SelectedValue.ToString() == "0")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    hidcatchexcondition.Value = dd1AbHr.SelectedValue.ToString();
                }
                else if (dd1AbMonth.SelectedValue.ToString() == "1")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    string strabhh = dd1Abhh.SelectedValue.ToString();
                    string strabmm = ddlAbMM.SelectedValue.ToString();
                    string strabfinal = strabhh + ":" + strabmm;
                    hidcatchexcondition.Value = strabfinal;
                }
                else if (dd1AbMonth.SelectedValue.ToString() == "2")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    hidcatchexcondition.Value = ddlAbDay.SelectedValue.ToString();
                }
                else if (dd1AbMonth.SelectedValue.ToString() == "3")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    hidcatchexcondition.Value = dd1AbMnDay.SelectedValue.ToString();
                }
                else
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    string strabmnday = dd1AbMnthDay.SelectedValue.ToString();
                    string strabmn = ddlAbMnth.SelectedValue.ToString();
                    string strabfinalres = strabmnday + "/" + strabmn;
                    hidcatchexcondition.Value = strabfinalres;
                }

            }
            else
            {
                if (ddlRlt.SelectedValue.ToString() == "0")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = drprltmin.SelectedValue.ToString();
                }
                else if (ddlRlt.SelectedValue.ToString() == "1")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = Drprlthour.SelectedValue.ToString();

                }
                else if (ddlRlt.SelectedValue.ToString() == "2")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = rltdayd.SelectedValue.ToString();
                }
                else if (ddlRlt.SelectedValue.ToString() == "3")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = raltweeks.SelectedValue.ToString();
                }
                else
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = drprltmonth.SelectedValue.ToString();
                }
            }
        }
        protected void btnDbCommand_Save_Click(object sender, EventArgs e)
        {
            bool flag1 = ValidatDbCommand();

            if (flag1)
            {
                if (hiddbobjectsucess.Value == "1")
                {
                    hiddbobjectsucess.Value = "";
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ChangeSelection();", true);
                }
                return;
            }
            else
            {
                try
                {
                    SaveCache();
                    SaveDbCommand();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(updbobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
                }
            }
            if (hiddbobjectsucess.Value == "1")
            {
                hiddbobjectsucess.Value = "";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"discardedit();$('[id$=hideditdone]').val('');SubModifiction(false);$('[id$=divdonotexecute]').hide();", true);
            }
        }
        #endregion

        #region UpdatePanelUpdate
        private void UpdateAllUpdatePanel()
        {
            UpdatePanel1.Update();
            updAddDbCommand.Update();
            updDeleteDbCmd.Update();
            updDbCmdRptInsert.Update();
            updInsertSqlQryTxt.Update();
            updRefreshOutPut.Update();
            UpdatePanel32.Update();
            updDbCmdParaJson.Update();

        }
        #endregion

        #region Clear Ctrl
        private void Clear()
        {
            hdfIsinsertQB.Value = "0";
            ddlCommandType.SelectedIndex = -1;
            txtDbCommand_SqlQueryWH.Text = "";
            Drppoptable.SelectedIndex = -1;
            //hidconid.Value = "";
            txtDbCommand_CommandName.Text = "";
            txtDBCmdDesc.Text = "";
            txtDbCommand_CommandId.Text = "";
            txtcopyobject.Text = "";
            txtDbCommand_SqlQueryPara.Text = "";
            ddl_DbConnactor.SelectedIndex = -1;
            dd1DataCatch.SelectedValue = "0";
            dd1AbMonth.SelectedIndex = 0;
            dd1AbHr.SelectedIndex = 0;
            dd1Abhh.SelectedIndex = 0;
            ddlAbMM.SelectedIndex = 0;
            ddlAbDay.SelectedIndex = 0;
            dd1AbMnDay.SelectedIndex = 0;
            dd1AbMnthDay.SelectedIndex = 0;
            ddlRlt.SelectedIndex = 0;
            drprltmin.SelectedIndex = 0;
            Drprlthour.SelectedIndex = 0;
            rltdayd.SelectedIndex = 0;
            raltweeks.SelectedIndex = 0;
            drprltmonth.SelectedIndex = 0;
            hidcatchexfrequency.Value = "";
            hidcatchexcondition.Value = "";
            hdfDbCmdPara.Value = "";
            txtDbCommand_SqlQuery.Text = "";
            updatetable.Update();
            BindTableColumn(null);
            hideditdone.Value = "";
            hdfDbCmdPara.Value = "";
            lblImageColumns.Text = "";
            hidimageoutput.Value = "";
            hidlblImageColumns.Value = "";
            hidcoloumtype.Value = "";
            updDbCmdParaJson.Update();
            hidnoexecutecondtion.Value = "";
        }
        private void DBClearAllDetails()
        {
            divSelectSqlQueryLabel.Visible = true;
            UpdateAllUpdatePanel();
            BindTableColumn(null);
            BindTableDropdown(null, false);
            Clear();
            hidconid.Value = "";
            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AddParameterInDbCommand();UnformDatabaseObject();catchtype('0');", true);
        }
        protected void ClearDbCommandDetails()
        {
            txtDbCommand_CommandId.Text = "";
            txtDbCommand_CommandName.Text = "";
            txtDbCommand_SqlQuery.Text = "";
            txtDBCmdDesc.Text = "";
            lblImageColumns.Text = "";
            hidimageoutput.Value = "";
            hidlblImageColumns.Value = "";
            hidcoloumtype.Value = "";

            ddlCommandType.SelectedValue = "-1";
            ddl_DbConnactor.SelectedValue = "-1";
            hidimageoutput.Value = "";
            hdfDbCmdPara.Value = "";
            updDbCmdParaJson.Update();
            updRefreshOutPut.Update();
        }
        private void AfterSave(bool isNew)
        {
            DBcmdObjectTable();
            updbobject.Update();
            Clear();
            btnDbBackClick.Visible = false;
            btnDbCommand_Reset.Visible = true;
            updDbCmdRptInsert.Update();
            if (isNew)
                ScriptManager.RegisterStartupScript(updbobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"BindDatabaseObjectdata();DataobjectClear();UnformDatabaseObject();", true); //
            else
                ScriptManager.RegisterStartupScript(updbobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"BindDatabaseObjectdata();PreventCancel();UnformDatabaseObject();", true); //DataobjectClear
        }
        #endregion

        #region All Validation Methods

        protected Boolean ValidatDbCommand()
        {
            if (txtDbCommand_CommandName.Text.Trim().Length > 50)
            {
                if (ddlCommandType.SelectedValue == "5")
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('Database object name cannot be more than 50 characters.');UnformDatabaseObject();AddParameterInDbCommand();HideSqlQueryhide();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('Database object name cannot be more than 50 characters.');UnformDatabaseObject();AddParameterInDbCommand();", true);
                }
                return true;
            }
            else if (txtDbCommand_CommandName.Text.Trim().Length == 0)
            {
                if (ddlCommandType.SelectedValue == "5")
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please enter object Name.');UnformDatabaseObject();AddParameterInDbCommand();HideSqlQueryhide();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please enter object Name.');UnformDatabaseObject();AddParameterInDbCommand();", true);
                }
                return true;
            }
            else if (ddl_DbConnactor.SelectedValue == "-1")
            {
                if (ddlCommandType.SelectedValue == "5")
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please select connector.');UnformDatabaseObject();AddParameterInDbCommand();HideSqlQueryhide();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please select connector.');UnformDatabaseObject();AddParameterInDbCommand();", true);
                }
                return true;
            }
            else if (ddlCommandType.SelectedValue == "-1")
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please select command type.');UnformDatabaseObject();AddParameterInDbCommand();", true);
                return true;
            }

            else if (txtDbCommand_SqlQuery.Text.Trim().Length == 0 && ddlCommandType.SelectedValue != "5")
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please enter sql query.');UnformDatabaseObject();AddParameterInDbCommand();", true);
                return true;
            }

            else if (ddlCommandType.SelectedValue == "2" || ddlCommandType.SelectedValue == "3" || ddlCommandType.SelectedValue == "4" || ddlCommandType.SelectedValue == "5")
            {
                if (ddlCommandType.SelectedValue == "5" && drpstoreprocedure.SelectedValue == "-1")
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please select any StoreProcedure.');UnformDatabaseObject();HideSqlQueryhide();", true);
                    return true;
                }
                else if (ddlCommandType.SelectedValue == "2" || ddlCommandType.SelectedValue == "3")
                {
                    if (hidlblinputpara.Value == "No Parameters defined yet")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Undefined input parameter');UnformDatabaseObject();", true);
                        return true;
                    }
                }
                else
                {
                }
            }
            return false;
        }



        protected Boolean ValidateCopyObject()
        {
            if (txtcopyobject.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Please enter object Name.');", true);
                return true;
            }
            else if (txtcopyobject.Text.Trim().Length < 3)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Object name cannot be less than 3 charecters.');", true);
                return true;
            }
            else if (txtcopyobject.Text.Trim().Length > 50)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Database object name cannot be more than 50 characters.');", true);
                return true;
            }
            return false;
        }

        public static Boolean IsSqlQueryValid(string _SqlQuery)
        {
            Boolean bolIsValid = true;
            string str = _SqlQuery.ToLower();
            if (str.Contains("delete from") || str.Contains("trancate from") || str.Contains("drop table") || str.Contains("select *"))
                bolIsValid = false;
            else
                bolIsValid = true;
            return bolIsValid;
        }

        protected Boolean SqlQueryValid()
        {
            Boolean bolIsValid = true;
            if (txtDbCommand_CommandName.Text.Length == 0)
            {
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please enter object name.');UnformDatabaseObject();", true);
                return false;
            }
            else if (txtDbCommand_SqlQuery.Text.Length == 0)
            {
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please enter sql query.');UnformDatabaseObject();", true);
                return false;
            }
            if (ddlCommandType.SelectedValue != "4")
            {
                if (!IsSqlQueryValid(txtDbCommand_SqlQuery.Text))
                {
                    ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('* Please enter valid sql query..');UnformDatabaseObject();", true);
                    return false;
                }
            }
            return bolIsValid;
        }

        #endregion

        #region Advance Query Builder

        protected void btnDbCmdSwitchToAdv_Click(object sender, EventArgs e)
        {
            string strIds = "";
            string[] strHfs;
            strHfs = Utilities.DecryptString(strhfs).Split(',');

            for (int index = 0; index < 4; index++)
            {
                if (strIds.Length > 0) strIds += ",";
                strIds += strHfs[index];
            }

            if (txtDbCommand_CommandId.Text.Trim().Length == 0)
                strIds += ",0," + ddl_DbConnactor.SelectedValue;
            else
                strIds += ",1," + ddl_DbConnactor.SelectedValue;
            //  adavce Query Builder uncomment
            strIds += "," + txtDbCommand_CommandName.Text + "," + txtDbCommand_CommandId.Text + "," + "MULTIPLE" + "," + txtDBCmdDesc.Text + "," + hidconid.Value + "," + dd1DataCatch.SelectedValue + "," + hidcatchexfrequency.Value + "," + hidcatchexcondition.Value + "," + lblImageColumns.Text + "," + hidimageoutput.Value + "," + hidcoloumtype.Value;
            hfs.Value = Utilities.EncryptString(strIds);
            hfbid.Value = strhfbid;
            string hfsQB = hfs.Value;
            string hdfInsertQueryParaQB = hdfInsertQueryPara.Value;
            string hfDbCmdSqlQueryQB = txtDbCommand_SqlQuery.Text;
            string hfDbType = string.Empty;
            string hfDbCon = string.Empty;
            string hdfMannualOutColQB = hdfAutoboxMannulPara.Value;
            StringBuilder sb = new StringBuilder();
            string[] ids = strIds.Split(',');
            GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, ids[5], "", this.CompanyId);

            if (objGetDatabaseConnection != null)
            {
                hfDbType = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]);
                if (Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["MPLUGIN_AGENT"]).Trim().Length > 0)
                {
                    sb.Append("$('[id$=hdfIsMplugIn]').val('1');");
                }
                else
                {
                    sb.Append("$('[id$=hdfIsMplugIn]').val('0');");
                }
                DatabaseType db;
                JObject crdtial = getCradential(Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim());
                sb.Append("loadQueryBuilder = false;");
                if (crdtial != null)
                {
                    if (Convert.ToString(crdtial["uid"]) != "" && Convert.ToString(crdtial["pwd"]) != "")
                    {
                        hfDbCon = GetConnectionString(objGetDatabaseConnection.ResultTable, Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]), out db);
                        sb.Append("loadQueryBuilder = true;");
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Invalid credential.');", true);
                }
            }
            else
            {
            }

            sb.Append("$('#" + hfs.ClientID + "').val('" + hfs.Value + "');");
            sb.Append("$('#" + hfbid.ClientID + "').val('" + hfbid.Value + "');");
            sb.Append("$('#hfsQB').val('" + hfsQB + "');");
            sb.Append("$('#hfDbCmdSqlQueryQB').val('" + ReplaceSingleCourseAndBackSlash(hfDbCmdSqlQueryQB) + "');");
            sb.Append("$('#hfDbType').val('" + hfDbType + "');");
            sb.Append("$('#hfDbCon').val('" + ReplaceSingleCourseAndBackSlash(hfDbCon) + "');");
            sb.Append("$('#hdfMannualOutColQB').val('" + hdfMannualOutColQB + "');");
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "UnformDatabaseObject();$('[id$=ddlCommandTypeDiv]').show();CommandType('uuu');", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), sb.ToString(), true);

        }

        protected void OpenObjectAfterQueryBuilder()
        {
            string strHfs = Utilities.DecryptString(strhfs);
            txtDbCommand_CommandId.Text = Convert.ToString(strHfs.Split(',')[7]);
            txtDBCmdDesc.Text = Convert.ToString(strHfs.Split(',')[9]);
            hidconid.Value = Convert.ToString(strHfs.Split(',')[10]);
            hidcatchexfrequency.Value = Convert.ToString(strHfs.Split(',')[12]);
            hidcatchexcondition.Value = Convert.ToString(strHfs.Split(',')[13]);
            txtDbCommand_CommandName.Text = Convert.ToString(strHfs.Split(',')[6]);
            txtDbCommand_SqlQuery.Text = Convert.ToString(hdfSql.Value);

            ddl_DbConnactor.SelectedIndex = ddl_DbConnactor.Items.IndexOf(ddl_DbConnactor.Items.FindByValue(strHfs.Split(',')[5]));
            ddlCommandType.SelectedIndex = ddlCommandType.Items.IndexOf(ddlCommandType.Items.FindByValue("1"));

            ddlCommandTypeDiv.Visible = true;
            if (ddlCommandType.SelectedValue == "2")
            {
                string strAgent = "", strDbType, strDbName;
                BindDbCmdTableDropDown(false, ddl_DbConnactor.SelectedValue, "", strHfs.Split(',')[3], out strAgent, out strDbType, out strDbName, false);
            }
            else { }
            btnDbCmdSwitchToAdv.Visible = true;
            SqlQueryOutputDiv.Visible = true;
            if (txtDbCommand_CommandId.Text.Trim().Length != 0)
            {
                lblDbCmd_Name.Text = txtDbCommand_CommandName.Text;
                lblDbCmd_ConnName.Text = ddl_DbConnactor.Items.FindByValue(ddl_DbConnactor.SelectedValue).Text;
                lblDbCmd_CmdType.Text = ddlCommandType.Items.FindByValue(ddlCommandType.SelectedValue).Text;

                lblDbCmd_Query.Text = txtDbCommand_SqlQuery.Text;

                EditDbcmdConnDiv.Visible = false;
            }
            else { }
            CreateInputParameterByQueryBuilder();

            if (hidconid.Value != "")
            {
                EditDbcmdConnDiv.Visible = true;
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UnformDatabaseObject();AddParameterInDbCommand();EditDBobject();CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
                ShowSelectOutParam(updAddDbCommand);
                hfs.Value = Utilities.EncryptString(strHfs.Split(',')[0] + "," + strHfs.Split(',')[1] + "," + strHfs.Split(',')[2] + "," + strHfs.Split(',')[3]);
            }
            else
            {
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UnformDatabaseObject();AddParameterInDbCommand();CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
                ShowSelectOutParam(updAddDbCommand);
                hfs.Value = Utilities.EncryptString(strHfs.Split(',')[0] + "," + strHfs.Split(',')[1] + "," + strHfs.Split(',')[2] + "," + strHfs.Split(',')[3]);
            }
        }
        void CreateInputParameterByQueryBuilder()
        {
            hdfDbCmdPara.Value = "";
            if (hdfInsertQueryPara.Value.Trim().Length > 0 && hdfDbCmdPara.Value.Trim().Length == 0)
            {
                string[] strParaArray = hdfInsertQueryPara.Value.Split(',');
                for (int index = 0; index < strParaArray.Length; index++)
                {
                    if (hdfDbCmdPara.Value.Length > 0)
                        hdfDbCmdPara.Value += ",";
                    else { }
                    hdfDbCmdPara.Value += "{\"para\":\"" + strParaArray[index] + "\",\"typ\":\"String\"}";
                }
                hdfDbCmdPara.Value = "[" + hdfDbCmdPara.Value + "]";
            }
            else { }
        }

        protected void lnkDbCmd_Back_Click(object sender, EventArgs e)
        {
            if (txtDbCommand_CommandId.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcAddDbCommandDiv(false);", true);
                return;
            }

            EditDBCommand(this.SubAdminid, this.CompanyId, txtDbCommand_CommandId.Text, updAddDbCommand);
            ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UnformDatabaseObject();", true);
        }

        #endregion

        #region BindRepeter On Table Seleletion Basis
        protected void BindDbCommandTableColumnRepeaterTab(string _ConnectionString, string _TableName, string _CompanyId, string _ConnectorId, string _MetaDataType, string _AgentName, string _DatabaseType, string _DatabaseName, string _UserName, string _Password)
        {
            if (_AgentName.Trim().Length != 0)
                BindColumnRepeaterFromMpluginMD(_CompanyId, _ConnectorId, _MetaDataType, _TableName, _AgentName, _DatabaseType, _UserName, _Password);
            else
            {
                if (ddlCommandType.SelectedValue == "2" || ddlCommandType.SelectedValue == "3")
                {
                    GetDatabaseTableColumnDetails objGetDatabaseTableColumnDetails = new GetDatabaseTableColumnDetails(_ConnectionString, _TableName, _DatabaseType, _DatabaseName);
                    objGetDatabaseTableColumnDetails.Process();
                    if (objGetDatabaseTableColumnDetails.ResultTable != null)
                    {
                        BindTableColumn(objGetDatabaseTableColumnDetails.ResultTable);
                        //rptDbCommandTblCol.DataSource = objGetDatabaseTableColumnDetails.ResultTable;
                        //rptDbCommandTblCol.DataBind();
                        pnlDbCmdColRpt.Visible = true;
                        //updDbCmdRptInsert.Update();
                    }
                    else
                        return;
                }
                else
                    return;
            }
        }
        #endregion

        void BindTableColumn(DataTable dt)
        {
            rptDbCommandTblCol.DataSource = dt;
            rptDbCommandTblCol.DataBind();
            updDbCmdRptInsert.Update();
            if (dt == null)
                pnlDbCmdColRpt.Visible = false;
            else
                pnlDbCmdColRpt.Visible = true;
        }

        #region Dropdown Connector/ Index Changed
        protected void ddl_DbConnactor_SelectedIndexChanged(object sender, EventArgs e)
        {

            txtDbCommand_SqlQuery.Text = "";
            ddlCommandType.SelectedIndex = -1;
            ddlCommandTypeDiv.Visible = true;
            ////btnDbCmdSwitchToAdv.Visible = false;

            ////SqlQueryOutputDiv.Visible = false;
            ////dispalyInputPara(false, false);

            ////divInsertSqlQueryLabel.Visible = false;
            ////btnDbCmdSwitchToAdv.Visible = false;
            ////divSelectSqlQueryLabel.Visible = true;
            ////pnlDbCmdColRpt.Visible = false;
            ////btnDbCmdSwitchToAdv.Visible = false;
            BindTableDropdown(null, false);
            //   divstoreprocedure.Visible = false;
            BindStoredProcedureDropdown(null);
            if (ddl_DbConnactor.SelectedValue == "-1")
            {
                // ddlCommandTypeDiv.Visible = false;
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();UnformDatabaseObject();catchtype('0');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();UnformDatabaseObject();", true);
            }
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"catchtype('0');ImageDivClear();$('[id$=divdonotexecute]').hide();", true);
        }
        private void BindDatabaseConnector(out string strScript)
        {
            GetWscommandDropdown objGetDbCommand = new GetWscommandDropdown(CompanyId);
            // DataTable dt = objGetDbCommand.ResultTable;
            JArray ja = new JArray();
            foreach (DataRow dr in objGetDbCommand.ResultTable.Rows)
            {
                JObject jo = new JObject();//DB_CONNECTOR_ID,DATABASE_TYPE
                jo.Add("id", Convert.ToString(dr["DB_CONNECTOR_ID"]));
                jo.Add("typ", Convert.ToString(dr["DATABASE_TYPE"]));
                ja.Add(jo);
            }
            strScript = ja.ToString();
            //ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"dbConectiondetail=" + ja.ToString(), true);
            ddl_DbConnactor.DataSource = objGetDbCommand.ResultTable;
            ddl_DbConnactor.DataTextField = "CONNECTION_NAME";
            ddl_DbConnactor.DataValueField = "DB_CONNECTOR_ID";
            ddl_DbConnactor.DataBind();
            ddl_DbConnactor.Items.Insert(0, new ListItem("Select", "-1"));
        }
        #endregion

        #region DB Command Select Index Changed
        private void dispalyInputPara(bool visibility, bool InputEditBtn)
        {
            btnDbcmdParaAddDiv.Visible = visibility;
            clearDivInputParaDiv.Visible = visibility;
            clearDivAddParameter.Visible = visibility;
            lblInputParametersDiv.Visible = visibility;
            divParameterInfo.Visible = visibility;
            lnkInputButton.Visible = InputEditBtn;
        }

        #endregion

        #region  Genrate Inserst/update/delete Query

        private void GenrateInsertQuery()
        {
            string strCol = "", strPara = "", strParaMeter = "", strInputPara = "";
            string strKey = (hdfDbCmdType.Value == "4" || hdfDbCmdType.Value == "2" ? ":" : "@");
            foreach (RepeaterItem r in rptDbCommandTblCol.Items)
            {
                DataRowView drv = (DataRowView)r.DataItem;
                Literal lit_Col = (Literal)r.FindControl("lit_Col");
                TextBox txt_Prarameter = (TextBox)r.FindControl("txt_Prarameter");
                if (txt_Prarameter.Text.Trim().Length > 0)
                {
                    if (strCol.Length > 0)
                    {
                        strCol += ",";
                    }
                    else { }
                    strCol += (hdfDbCmdType.Value == "4" ? "\"" + lit_Col.Text + "\"" : lit_Col.Text);
                    string isInputPara = "";
                    if (IsValidParameterOrValue(txt_Prarameter.Text.Trim(), out isInputPara, strKey))
                    {
                        strPara += "," + txt_Prarameter.Text;
                        strInputPara += isInputPara.Length > 0 ? ("," + isInputPara) : "";
                    }
                    else
                    {
                        strParaMeter += "," + lit_Col.Text.Trim();
                    }
                }
            }
            if (strParaMeter.Length <= 0)
            {
                txtDbCommand_SqlQuery.Text = "INSERT INTO " + Drppoptable.SelectedValue + "(" + strCol + ") VALUES(" + (strPara.Length > 0 ? strPara.Substring(1) : "") + ");";
                hdfInsertQueryPara.Value = strInputPara.Length > 0 ? strInputPara.Substring(1) : "";
                CreateInputParameterByQueryBuilder();
                updInsertSqlQryTxt.Update();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"OpenPanelInsertCommands(false);UnformDatabaseObject();AddParameterInDbCommand();CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
            }
            else
            {
                updInsertSqlQryTxt.Update();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html('Please correct value for column(s) [" + strParaMeter.Substring(1) + "]');SubProcBoxMessage(true);CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
            }
        }

        private bool IsValidParameterOrValue(string strInput, out string strInputPara, string key)
        {
            strInputPara = "";
            double dblResult; bool blnResult;
            if (strInput.StartsWith("'") && strInput.EndsWith("'"))
                return true;
            else if (double.TryParse(strInput, out dblResult) || bool.TryParse(strInput, out blnResult))
                return true;
            else if (strInput.StartsWith(key))
            {
                Regex re = new Regex("^[a-zA-Z][a-zA-Z0-9]*$");
                strInput = strInput.Substring(1);
                if (re.IsMatch(strInput))
                {
                    strInputPara = strInput;
                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        private void GenrateUpdateQuery()
        {
            StringBuilder sbUpdateCol = new StringBuilder();
            string strParaMeter = "", strInputPara = "";
            string strKey = (hdfDbCmdType.Value == "4" || hdfDbCmdType.Value == "2" ? ":" : "@");
            foreach (RepeaterItem r in rptDbCommandTblCol.Items)
            {
                DataRowView drv = (DataRowView)r.DataItem;
                Literal lit_Col = (Literal)r.FindControl("lit_Col");
                TextBox txt_Prarameter = (TextBox)r.FindControl("txt_Prarameter");
                if (txt_Prarameter.Text.Trim().Length > 0)
                {
                    string isInputPara = "";
                    if (IsValidParameterOrValue(txt_Prarameter.Text.Trim(), out isInputPara, strKey))
                    {
                        sbUpdateCol.Append(sbUpdateCol.ToString().Length > 0 ? "," : "");
                        if (hdfDbCmdType.Value == "4")
                        {
                            sbUpdateCol.Append("\"" + lit_Col.Text + "\"" + " = " + "" + txt_Prarameter.Text + "");
                        }
                        else
                        {
                            sbUpdateCol.Append(lit_Col.Text + " = " + "" + txt_Prarameter.Text + "");
                            strInputPara += isInputPara.Length > 0 ? ("," + isInputPara) : "";
                        }
                    }
                    else
                    {
                        strParaMeter += "," + lit_Col.Text.Trim();
                    }
                }
            }
            Regex rr = new Regex(@"(?<Parameter>" + strKey + @"\w*)", RegexOptions.Compiled);
            string[] parameters = rr.Matches(txtDbCommand_SqlQueryWH.Text).Cast<Match>().Select<Match, string>(x => x.Value).Distinct<string>().ToArray<string>();
            foreach (string para in parameters)
            {
                strInputPara += "," + para.Substring(1).Trim();
            }
            if (strParaMeter.Length <= 0)
            {
                if (sbUpdateCol.ToString().Trim().Length == 0)
                {
                    updInsertSqlQryTxt.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html('Please select atleast one column from database table.');SubProcBoxMessage(true);CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
                    return;
                }
                else
                {
                    txtDbCommand_SqlQuery.Text = "UPDATE " + Drppoptable.SelectedValue + " SET " + sbUpdateCol.ToString() +
                        (txtDbCommand_SqlQueryWH.Text.Trim().Length > 0 ? (" Where " + txtDbCommand_SqlQueryWH.Text) : "");
                    hdfInsertQueryPara.Value = strInputPara.Length > 0 ? strInputPara.Substring(1) : "";
                    CreateInputParameterByQueryBuilder();
                    updInsertSqlQryTxt.Update(); ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"OpenPanelInsertCommands(false);UnformDatabaseObject();AddParameterInDbCommand();CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html('Please correct value for column(s) [" + strParaMeter.Substring(1) + "]');SubProcBoxMessage(true);", true);
                updInsertSqlQryTxt.Update();
            }
        }

        #endregion

        #region Refersh OutputParameter Button Click
        protected void imgRefreshPara_Click(object sender, EventArgs e)
        {
            ShowSelectOutParam(updRefreshOutPut);
            updRefreshOutPut.Update();

            if (lblImageColumns.Text != "" || hidimagecoloums.Value != "")
            {
                if (hidconid.Value != "")
                {
                    ScriptManager.RegisterStartupScript(updRefreshOutPut, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"imageoutputparameter();displayimageparameter();ImageColumnPopup(false);", true);
                }
                else ScriptManager.RegisterStartupScript(updRefreshOutPut, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('[id$=hidimagecoloums]').val('');$('#drpselectimageparameter option:eq(0)').attr('selected', true);$('.ImageQPContentRow').remove();", true);
            }
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CommandType('ggg');", true);
        }
        #endregion

        #region Bind  AllTable in  DropDown and StoreProcedure

        protected bool BindDbCommandTableDropdownPopup(string _ConnectionString, DatabaseType _DataBaseType, string _DataBaseName, int _type)
        {
            bool flag = false;
            if (_type == 1)
            {
                if (hdConnection.Value.ToLower() != ddl_DbConnactor.SelectedItem.Text)
                {
                    BindTableDropdown(GetMsSqlDataBaseTables(_ConnectionString, _DataBaseType, _DataBaseName), true);
                }
            }
            else
            {
                if (hdConnection.Value.ToLower() != ddl_DbConnactor.SelectedItem.Text || drpstoreprocedure.Items.Count == 0)
                    flag = GetStoredProcedureList(_ConnectionString, _DataBaseType, _DataBaseName);
            }
            return flag;
        }
        protected bool GetStoredProcedureList(string _ConnectionString, DatabaseType _DataBaseType, string _DataBaseName)
        {
            DataSet objDataTable1 = GetMsSqlDataBaseStoredProcedure(_ConnectionString, _DataBaseType, _DataBaseName);
            if (objDataTable1 != null)
            {
                if (_DataBaseType == DatabaseType.ORACLE)
                {
                    GetTablesDetails objGetTablesDetails = new GetTablesDetails(_ConnectionString, _DataBaseType, _DataBaseName);
                    objGetTablesDetails.GetOracleStoredProcedures();
                    BindStoreProcedureDropdownFromMplugin(objGetTablesDetails.Dsprocedure, objDataTable1, "2");
                }
                else
                {
                    BindStoredProcedureDropdown(objDataTable1.Tables[0]);
                }
                hidstoreproc.Value = Utilities.ConvertdatetabletoString(objDataTable1.Tables[0]);
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UnformDatabaseObject();Getallstoreprocedure();", true);
                return true;
            }
            else
            {
                BindStoredProcedureDropdown(null);
                //drpstoreprocedure.Items.Insert(0, new ListItem("Select StoreProcedure", "-1"));
            }
            return false;
        }
        void BindStoredProcedureDropdown(DataTable dt)
        {
            drpstoreprocedure.Items.Clear();
            drpstoreprocedure.DataSource = dt;
            drpstoreprocedure.DataTextField = "ObjectName";
            drpstoreprocedure.DataValueField = "ObjectName";
            drpstoreprocedure.DataBind();
            drpstoreprocedure.Items.Insert(0, new ListItem("Select Stored Procedure", "-1"));
        }
        private void BindTableDropdown(DataTable ds, Boolean isBind)
        {
            Drppoptable.DataSource = null;
            Drppoptable.DataBind();
            Drppoptable.Items.Clear();
            if (isBind)
            {
                if (ds != null)
                {
                    Drppoptable.DataSource = ds;
                    Drppoptable.DataTextField = "TABLE_NAME";
                    Drppoptable.DataValueField = "TABLE_NAME";
                    Drppoptable.DataBind();
                    Drppoptable.Items.Insert(0, new ListItem("Select Table Name", "-1"));
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();UnformDatabaseObject();DBCmd_Conn_SelectedIndexChange();CommandType('0');", true);
                }
                else
                {
                    pnlDbCmdColRpt.Visible = false;
                    Drppoptable.Items.Insert(0, new ListItem("Select Table Name", "-1"));
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessageDataConection(true,'Connection Issue');$('#aMessage').html('Database connection cannot be established.<br />May be some error in database connector or invalid credential');UnformDatabaseObject();DBCmd_Conn_SelectedIndexChange();CommandType('0');", true);
                }
            }
        }
        private void BindStoreProcedureDropdownFromMplugin(DataSet dssp, DataSet dspara, string databasetype)
        {
            try
            {
                if (dssp != null && dssp.Tables.Count > 0)
                {
                    DataTable dtFinal = new DataTable();
                    dtFinal.Columns.Add("ObjectName");
                    dtFinal.Columns.Add("ParameterName");
                    dtFinal.Columns.Add("ParameterDataType");
                    string paranm = "";
                    string paratyp = "";

                    foreach (DataRow dr in dssp.Tables[0].Rows)
                    {
                        string filter = "";
                        if (databasetype == "3")
                            filter = String.Format("routine_name ='{0}' and  parameter_mode <> 'OUT'  ", Convert.ToString(dr["routine_name"]), Convert.ToString(dr["parameter_mode"]));
                        else
                            filter = String.Format("routine_name ='{0}' ", Convert.ToString(dr["routine_name"]));

                        DataRow nrow = dtFinal.NewRow();
                        nrow["ObjectName"] = Convert.ToString(dr["routine_name"]);
                        if (dspara != null)
                        {
                            DataRow[] dwr = dspara.Tables[0].Select(filter);
                            paranm = "";
                            paratyp = "";
                            foreach (DataRow drt in dwr)
                            {
                                paranm += (paranm.Length > 0 ? "," : "") + Convert.ToString(drt["parameter_name"]);
                                paratyp += (paratyp.Length > 0 ? "," : "") + Convert.ToString(drt["parameter_type"]);
                            }
                            paranm = paranm.Replace("@", "");
                        }
                        nrow["ParameterName"] = paranm;
                        nrow["ParameterDataType"] = paratyp;
                        dtFinal.Rows.Add(nrow);
                    }
                    dtFinal = DeleteDuplicateFromDataTable(dtFinal, "ObjectName");

                    if (dtFinal.Rows.Count > 0)
                    {
                        BindStoredProcedureDropdown(dtFinal);
                        hidstoreproc.Value = Utilities.ConvertdatetabletoString(dtFinal);
                        ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"Getallstoreprocedure();DBCmd_Conn_SelectedIndexChange();CommandType('0');", true);
                    }
                }
                else
                {
                    BindStoredProcedureDropdown(null);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');DBCmd_Conn_SelectedIndexChange();CommandType('0');", true);
            }
        }

        public string BindDbCmdTableDropDown(Boolean _IsSelectAll, string _ConnectorId, string _Name, string _ComapnyId, out string _AgentName, out string _DbType, out string _DbName, bool _isMeta)
        {
            string strConnectionString = ""; _DbName = ""; _DbType = "";
            _AgentName = "";
            try
            {
                GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(_IsSelectAll, _ConnectorId, _Name, _ComapnyId);
                //DatabaseType db;
                if (objGetDatabaseConnection != null)
                {
                    _DbType = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]).Trim();
                    _DbName = AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"]).Trim());
                    hdfDbType.Value = _DbType;
                    if (_isMeta)
                    {
                        JObject crdtial = getCradential(Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim());
                        if (crdtial != null)
                        {
                            if (Convert.ToString(crdtial["uid"]) != "" && Convert.ToString(crdtial["pwd"]) != "")
                            {
                                int MetaType = (ddlCommandType.SelectedValue == "5" ? 2 : 1);
                                GetMeta(_IsSelectAll, _ConnectorId, _ComapnyId, out _AgentName, _DbType, _DbName, objGetDatabaseConnection.ResultTable, Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]), MetaType);
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Invalid credential.');", true);
                            strConnectionString = "Error";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
                strConnectionString = "Error";
            }
            return strConnectionString;
        }
        JObject getCradential(string _credentialproperty)
        {
            ltCrd.Text = _credentialproperty;
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == _credentialproperty)
                        return crd;
            }
            return null;
        }
        protected void btnSaveCredential_Click(object sender, EventArgs e)
        {
            string _DbName = "", _DbType = "", strAgent;

            if (ddlCommandType.SelectedIndex > 0 && (ddlCommandType.SelectedValue != "1"))
                try
                {
                    GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, ddl_DbConnactor.SelectedValue, "", this.CompanyId);
                    if (objGetDatabaseConnection != null)
                    {
                        _DbType = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]).Trim();
                        _DbName = AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"]).Trim());
                        hdfDbType.Value = _DbType;
                        int MetaType = (ddlCommandType.SelectedValue == "5" ? 2 : 1);
                        bool flag = GetMeta(false, ddl_DbConnactor.SelectedValue, this.CompanyId, out strAgent, _DbType, _DbName, objGetDatabaseConnection.ResultTable, txtUsername.Text.Trim(), txtPassword.Text, MetaType);
                        if (flag)
                        {
                            saveCredentialInHidden(Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim());
                        }
                        if (hidconid.Value.Length == 0)
                            updAddDbCommand.Update();
                        else
                        {
                            if (flag)
                                EditDBCommand(this.SubAdminid, this.CompanyId, txtDbCommand_CommandId.Text, updAddDbCommand);
                            ScriptManager.RegisterStartupScript(updDeleteDbCmd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "UnformDatabaseObject();EditDBobject();disabletextobject();", true);
                        }
                    }

                    if (ddlCommandType.SelectedValue == "5")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('[id$=ddlCommandTypeDiv]').show();CommandTypeDetail('rr');", true);
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AddParameterInDbCommand();", true);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AddParameterInDbCommand();SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
                }
            else if (ddlCommandType.SelectedValue == "1")
            {
                GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, ddl_DbConnactor.SelectedValue, "", this.CompanyId);
                if (objGetDatabaseConnection != null)
                    saveCredentialInHidden(Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim());
                btnDbCmdSwitchToAdv_Click(sender, e);
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AddParameterInDbCommand();", true);
            }
            else
                if (ShowTestResult(txtUsername.Text.Trim(), txtPassword.Text))
                {
                    saveCredentialInHidden(ltCrd.Text);
                };
        }
        void saveCredentialInHidden(string credentialproperty)
        {
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == credentialproperty)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(crd["uid"])))
                        {
                            crd["uid"] = txtUsername.Text.Trim();
                            crd["pwd"] = txtPassword.Text;
                            break;
                        }
                    }
                hidWsAuthencationMeta.Value = crds.ToString();
            }
        }
        private string GetConnectionString(DataTable _dbConnection, string _uid, string _pwd, out DatabaseType db)
        {
            db = (DatabaseType)(Convert.ToInt32(_dbConnection.Rows[0]["DATABASE_TYPE"]));
            return Utilities.getConnectionString(Convert.ToString(_dbConnection.Rows[0]["HOST_NAME"]),
                AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(_dbConnection.Rows[0]["DATABASE_NAME"])),
                _uid, _pwd, Convert.ToString(_dbConnection.Rows[0]["ADDITIONAL_STRING"]), Convert.ToString(_dbConnection.Rows[0]["TIME_OUT"]),
                db);
        }
        private bool GetMeta(Boolean _IsSelectAll, string _ConnectorId, string _ComapnyId, out string _AgentName, string _DbType, string _DbName, DataTable _dbConnection, string _uid, string _pwd, int _type)
        {
            bool flag = false;
            _AgentName = "";

            _AgentName = Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]);
            if (Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]).Length != 0)
            {
                if (_type == 1)
                {
                    if (hdConnection.Value.ToLower() != ddl_DbConnactor.SelectedItem.Text)
                    {
                        DataSet ds = GetMetaData(_ComapnyId, _ConnectorId, ((int)Different_Type_Meta.TABLES).ToString(), "", Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]), _DbType, AesEncryption.AESEncrypt(_ComapnyId, _uid), AesEncryption.AESEncrypt(_ComapnyId, _pwd));
                        if (ds != null)
                        {
                            BindTableDropdown(ds.Tables[0], true);
                            flag = true;
                        }
                        else
                        {
                            BindTableDropdown(null, true);
                            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessageDataConection(true,'Connection Issue');$('#aMessage').html('Database connection cannot be established.<br />May be some error in database connector or invalid credential');UnformDatabaseObject();DBCmd_Conn_SelectedIndexChange();CommandType('0');", true);
                        }
                        updatetable.Update();
                    }
                }
                else
                {
                    if (hdConnection.Value.ToLower() != ddl_DbConnactor.SelectedItem.Text || drpstoreprocedure.Items.Count == 0)
                    {
                        DataSet dssp = GetMetaData(_ComapnyId, _ConnectorId, ((int)Different_Type_Meta.STORED_PROCEDURES).ToString(), "", Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]), _DbType, AesEncryption.AESEncrypt(_ComapnyId, _uid), AesEncryption.AESEncrypt(_ComapnyId, _pwd));
                        DataSet dspara = GetMetaData(_ComapnyId, _ConnectorId, ((int)Different_Type_Meta.STORED_PROCEDURE_PARAMS).ToString(), "", Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]), _DbType, AesEncryption.AESEncrypt(_ComapnyId, _uid), AesEncryption.AESEncrypt(_ComapnyId, _pwd));
                        if (dssp != null)
                        {
                            BindStoreProcedureDropdownFromMplugin(dssp, dspara, _DbType);
                            flag = true;
                        }
                        else
                        {
                            BindStoredProcedureDropdown(null);
                            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessageDataConection(true,'Connection Issue');$('#aMessage').html('Database connection cannot be established.<br />May be some error in database connector or invalid credential');UnformDatabaseObject();DBCmd_Conn_SelectedIndexChange();CommandType('0');", true);
                        }
                    }
                    updatetable.Update();
                }
            }
            else
            {
                DatabaseType db;
                flag = BindDbCommandTableDropdownPopup(GetConnectionString(_dbConnection, _uid, _pwd, out db), db, _DbName, _type);
                updatetable.Update();
            }
            hdConnection.Value = ddl_DbConnactor.SelectedItem.Text;
            return flag;
        }
        private DataTable DeleteDuplicateFromDataTable(DataTable dtDuplicate, string columnName)
        {
            Hashtable hashT = new Hashtable();
            ArrayList arrDuplicate = new ArrayList();
            foreach (DataRow row in dtDuplicate.Rows)
            {
                if (hashT.Contains(row[columnName]))
                    arrDuplicate.Add(row);
                else
                    hashT.Add(row[columnName], string.Empty);
            }
            foreach (DataRow row in arrDuplicate)
                dtDuplicate.Rows.Remove(row);

            return dtDuplicate;
        }

        #endregion

        #region GetConnection
        private DataTable GetMsSqlDataBaseTables(string _ConnectionString, DatabaseType _DataBaseType, string _DatabaseName)
        {
            GetTablesDetails objGetTablesDetails = new GetTablesDetails(_ConnectionString, _DataBaseType, _DatabaseName);
            objGetTablesDetails.Process();
            return objGetTablesDetails.ResultTable;
        }
        private DataSet GetMsSqlDataBaseStoredProcedure(string _ConnectionString, DatabaseType _DataBaseType, string _DatabaseName)
        {
            GetTablesDetails objGetTablesDetails = new GetTablesDetails(_ConnectionString, _DataBaseType, _DatabaseName);
            objGetTablesDetails.ProcessStoredProcedure();
            return objGetTablesDetails.Dsprocedure;
        }
        #endregion

        #region mpluginRequest MetaData
        private DataSet GetMetaData(string _CompanyId, string _ConnectorId, string _MetaDataType, string _TableName, string _AgentName, string _DbType, string _userName, string _password)
        {
            string strRqst, strUrl = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objAgent = new mPluginAgents();
            strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + _CompanyId + "\",\"agtid\":\"" + _AgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(this.CompanyId, _AgentName) + "\",\"connid\":\"" + _ConnectorId + "\",\"mttyp\":\"" + _MetaDataType + "\",\"tblnm\":\"" + ReplaceDoubleCourseAndBackSlash(_TableName) + "\",\"qrytp\":\"" + (_DbType == "5" ? "0" : "1") + "\",\"unm\":\"" + _userName + "\",\"pwd\":\"" + _password + "\"}}";

            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    strUrl = strUrl + "/MPGetDBMetadata.aspx?d=" + Utilities.UrlEncode(strRqst);
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        MP_GetDatabseMetadataResp obj = new MP_GetDatabseMetadataResp(oResponse.ResponseText);
                        if (obj.Code == "0")
                            return obj.Data;
                        else
                            return null;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return null;
        }
        #endregion

        #region Replacement Inputs
        private string ReplaceDoubleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceDoubleCourse(ReplaceBackSlash(_Input));
            }
        }
        private string ReplaceSingleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"'", @"\'");
            }
        }

        private string ReplaceDoubleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, "\"", "\\\"");
            }
        }


        private string ReplaceSingleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceSingleCourse(ReplaceBackSlash(_Input));
            }
        }
        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"\\", @"\\");
            }
        }
        #endregion

        #region Delete Object
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            DeleteDbCommand();
        }
        protected void DeleteDbCommand()
        {
            hdfCmdDelId.Value = "DB" + "_" + txtDbCommand_CommandId.Text;
            UpdateObjectXreference obj = new UpdateObjectXreference();
            DataSet ds = obj.GetObjectXreference(this.CompanyId, hidconid.Value, 1);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Database object already used in apps .<br>" + "');", true);
            else
                ScriptManager.RegisterStartupScript(updDeleteDbCmd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMsgCmdDelCmf').html('" + "Do you want to delete this object ?" + "');SubProcDeleteCmdConfrmation(true,'Delete Object');UnformDatabaseObject();", true);
        }

        protected void btnCmdDelYes_Click(object sender, EventArgs e)
        {
            DeleteDbWsCommand(this.SubAdminid, this.CompanyId);
            DBcmdObjectTable();
            updAddDbCommand.Update();
            updbobject.Update();
            DBClearAllDetails();
        }
        protected void DeleteDbWsCommand(string _SubAdminId, string _CompanyId)
        {
            hdfCmdDelId.Value = "DB";
            DeleteDbCommand objDeleteDbCommand = new DeleteDbCommand(hidconid.Value, _SubAdminId, _CompanyId);
            objDeleteDbCommand.Process();
            if (objDeleteDbCommand.StatusCode != 0)
            {
                ScriptManager.RegisterStartupScript(UpdDeleteCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Internal server error.<br>Database object cannot be deleted.');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdDeleteCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcConfirmBoxMessageDB(true,'Deleted',250);$('#aCFmessage').html('" + "* Database object deleted Succsessfully." + "');$('#aCfmMessage').html('');$('#aCfmUpdateMessage').html('');SubProcDeleteCmdConfrmation(false);", true);
            }
        }


        private string GetCommandRelateApp(DataTable _dt, DataTable _dtApp, string _CmdId)
        {
            string strFormName = "", strAppName = "";
            List<string> objApps = new List<string>();
            Hashtable ObjHsForms = new Hashtable();

            Hashtable ObjHsVExit = new Hashtable();
            Hashtable ObjHsVRowClk = new Hashtable();
            Hashtable ObjHsVCancel = new Hashtable();

            string[] strArray, strInnerArray;
            foreach (DataRow dr in _dtApp.Rows)
            {
                strAppName = Convert.ToString(dr["WF_NAME"]);
                strArray = Convert.ToString(dr["COMMAND_IDS"]).Split(',');
                foreach (string objStr in strArray)
                {
                    strInnerArray = objStr.Split('_');
                    if (strInnerArray[0] == _CmdId)
                    {
                        switch (strInnerArray[3])
                        {
                            case "0":
                                AddItemInHash(ObjHsVExit, objApps, strAppName, strInnerArray[2], " (Exit)");
                                break;
                            case "1":
                                AddItemInHash(ObjHsVExit, objApps, strAppName, strInnerArray[2], " (RowCick)");
                                break;
                            case "2":
                                AddItemInHash(ObjHsVExit, objApps, strAppName, strInnerArray[2], " (Cancel)");
                                break;
                        }
                    }
                    else { }
                }
            }

            if (_dt != null)
            {
                foreach (DataRow dr in _dt.Rows)
                {
                    if (!ObjHsForms.ContainsKey(Convert.ToString(dr["WF_NAME"])))
                    {
                        if (objApps.Contains(Convert.ToString(dr["WF_NAME"]))) { }
                        else objApps.Add(Convert.ToString(dr["WF_NAME"]));
                        ObjHsForms.Add(Convert.ToString(dr["WF_NAME"]), Convert.ToString(dr["FORM_NAME"]));
                    }
                    else
                    {
                        strFormName = Convert.ToString(ObjHsForms[Convert.ToString(dr["WF_NAME"])]);
                        if (strFormName.Length >= 0) strFormName = strFormName + ", " + Convert.ToString(dr["FORM_NAME"]);
                        ObjHsForms[Convert.ToString(dr["WF_NAME"])] = strFormName;
                    }
                }
            }
            string strHtml = "";
            if (objApps.Count > 0)
            {

                foreach (string str in objApps)
                {
                    strHtml += "<div class=\"ClCmdDMHeader\" style=\"width:440px;background-color:#cdc9c9;height: 17px;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + str + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
                    if (ObjHsForms.ContainsKey(str))
                    {
                        strHtml += "<div class=\"FLeft modalPopUpDetRow\" style=\"width: 440px;\">"
                              + "<div class=\"FLeft modalPopUpDetHeader\"  style=\"font-size:12px;margin-left:10px;width: 90px;\">Forms   :</div>"
                              + "<div class=\"FLeft\" style=\"margin-left:3px;margin-right:3px;word-wrap: break-word;width: 330px;\">" + ObjHsForms[str] + "</div>"
                        + "</div>"
                        + "<div class=\"clear\"></div>";
                    }
                    if (ObjHsVExit.ContainsKey(str))
                    {
                        strHtml += "<div class=\"FLeft modalPopUpDetRow\" style=\"width: 440px;\">"
                                        + "<div class=\"FLeft modalPopUpDetHeader\"  style=\"font-size:12px;margin-left:10px;width: 90px;\">Views   :</div>"
                                        + "<div class=\"FLeft\" style=\"margin-left:3px;margin-right:3px;word-wrap: break-word;width: 330px;\">" + ObjHsVExit[str] + "</div>"
                                 + "</div>"
                                 + "<div class=\"clear\"></div>";
                    }
                }
            }

            strHtml = (strHtml.Length == 0 ? "<div class=\"FLeft\" style=\"margin-left: 10px;\">This object is not used any where.</div>" : "<div class=\"FLeft\" style=\"border: solid 1px #c0c0c0;width:99%;padding:2px;width: 450px;\">" + strHtml + "</div>");

            return strHtml;
        }
        private void AddItemInHash(Hashtable _Hs, List<string> _Lst, string _strKey, string _strValue, string _strSuff)
        {
            string strFormName = "";
            if (!_Hs.ContainsKey(_strKey))
            {
                if (_Lst.Contains(_strKey)) { }
                else _Lst.Add(_strKey);
                _Hs.Add(_strKey, _strValue + _strSuff);
            }
            else
            {
                strFormName = Convert.ToString(_Hs[_strKey]);
                if (strFormName.Length >= 0) strFormName = strFormName + ", " + _strValue + _strSuff;
                _Hs[_strKey] = strFormName;
            }
        }
        #endregion

        #region Add /Cancel Button Click
        protected void lnkAddNewDbCommand_Click(object sender, EventArgs e)
        {
            if (hideditdone.Value == "5")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubModifiction(true);", true);
            }
            else
            {
                hdfDbCmdPara.Value = "";
                DBClearAllDetails();
                ddlCommandType.SelectedValue = "-1";
                btnDbBackClick.Visible = false;
                btnDbCommand_Reset.Visible = true;
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('[id$=divdonotexecute]').hide();CommandType('-1');AutoCompleteTextbox('');", true);
            }
        }
        protected void lnkAddNewDbCancel_Click(object sender, EventArgs e)
        {
            try
            {
                EditDbcmd1.Visible = true;
                EditDbcmd2.Visible = true;
                EditDbcmdConnDiv.Visible = true;
                hdfDbCmdPara.Value = "";
                btnDbcmdParaAddDiv.Visible = false;
                lblInputParametersDiv.Visible = false;
                divParameterInfo.Visible = false;
                ddlCommandTypeDiv.Visible = false;
                btnDbBackClick.Visible = false;
                pnlDbCmdColRpt.Visible = false;
                divstoreprocedure.Visible = false;
                divInsertSqlQueryLabel.Visible = false;
                hidconid.Value = "";
                Clear();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AddParameterInDbCommand();UnformDatabaseObject();Enabletextobject();ViewDBDisplay('" + hidname.Value + "')", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }

        }
        #endregion

        #region Bind Repeter Table Selection/repeter Data Bound/Repeter Button Click

        protected void Drppoptable_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strAgent, strDbType = "", strDbName = "";
            txtDbCommand_SqlQuery.Text = "";
            hdfDbType.Value = "";
            if (Drppoptable.SelectedValue != "-1")
            {
                GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, ddl_DbConnactor.SelectedValue, "", this.CompanyId);
                if (objGetDatabaseConnection != null && objGetDatabaseConnection.ResultTable.Rows.Count > 0)
                {
                    strDbType = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]).Trim();
                    strDbName = AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"])).Trim();
                    string credentialproperty = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim();
                    JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                    JObject crdtial = new JObject();
                    foreach (JObject crd in crds)
                        if (Convert.ToString(crd["tag"]) == credentialproperty)
                        {
                            crdtial = crd;
                            break;
                        }
                    DatabaseType db;
                    strAgent = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["MPLUGIN_AGENT"]);
                    int particulartable = (int)Different_Type_Meta.TABLE_COLUMNS;
                    BindDbCommandTableColumnRepeaterTab(GetConnectionString(objGetDatabaseConnection.ResultTable, Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]), out db), Drppoptable.SelectedValue, this.CompanyId, ddl_DbConnactor.SelectedValue, particulartable.ToString(), strAgent, strDbType, strDbName, Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]));
                    pnlDbCmdColRpt.Visible = true;
                }
            }
            else
            {
                pnlDbCmdColRpt.Visible = false;
                BindTableColumn(null);
                txtDbCommand_SqlQueryWH.Text = "";
                UpdatePanel32.Update();
            }

            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UnformDatabaseObject();", true);
        }

        private void BindColumnRepeaterFromMpluginMD(string _CompanyId, string _ConnectorId, string _MetaDataType, string _TableName, string _AgentName, string _DbType, string _UserName, string _Password)
        {
            if (ddlCommandType.SelectedValue == "2" || ddlCommandType.SelectedValue == "3")
            {
                DataSet ds = GetMetaData(_CompanyId, _ConnectorId, _MetaDataType, _TableName, _AgentName, _DbType, AesEncryption.AESEncrypt(_CompanyId, _UserName), AesEncryption.AESEncrypt(_CompanyId, _Password));
                if (ds != null)
                {
                    if (ds.Tables != null)
                    {
                        BindTableColumn(ds.Tables[0]);
                        pnlDbCmdColRpt.Visible = true;
                        pnlCmdTabl.Visible = true;
                    }
                    else
                        return;
                }
            }
            else
                return;
        }
        protected void rptDbCommandTblCol_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) ||
                (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                Literal lit_Type = (Literal)e.Item.FindControl("lit_Type");
                Literal lit_ColType = (Literal)e.Item.FindControl("lit_ColType");
                lit_ColType.Text = " (" + lit_Type.Text + ")";
            }
        }
        protected void lnkGenrateQuery_Click(object sender, EventArgs e)
        {
            hdfIsinsertQB.Value = "1";
            updDbCmdParaJson.Update();
            switch (ddlCommandType.SelectedValue)
            {
                case "2":
                    GenrateInsertQuery();
                    break;
                case "3":
                    GenrateUpdateQuery();
                    break;
            }

        }
        #endregion

        #region GetParameter Where Condtion
        private string GetParameterFromWhereCnd(string SqlQuery, out Hashtable _HS, out string _Query, out string _WhereCond)
        {
            _WhereCond = "";
            string strPara = SqlQuery;
            int i = 0;
            if (!strPara.Contains(" WHERE "))
            {
                _Query = strPara.Substring(("UPDATE " + Drppoptable.SelectedValue + "SET ").Length, strPara.Length - ("UPDATE " + Drppoptable.SelectedValue + "SET ").Length);
                _HS = new Hashtable();
                return "";
            }
            i = 0;
            foreach (string str in strPara.Split(new string[] { " WHERE " }, StringSplitOptions.None))
            {
                _WhereCond += (_WhereCond.Trim().Length > 0 ? " WHERE " : "");
                if (i != 0)
                    _WhereCond += str;
                i = i + 1;

            }
            strPara = strPara.Substring(("UPDATE " + Drppoptable.SelectedValue + "SET ").Length, strPara.Length - ("UPDATE " + Drppoptable.SelectedValue + "SET ").Length);
            i = 0;
            string strIndex = "";
            foreach (char c in strPara.ToCharArray())
            {
                if (c.ToString().Equals("'"))
                {
                    if (strIndex.Length > 0)
                    {
                        strIndex += ",";
                    }
                    else { }
                    strIndex += i.ToString();
                }
                else { }
                i = i + 1;
            }
            string[] strAll = strIndex.Split(',');
            Hashtable hs = new Hashtable();
            if (strIndex.Trim().Length > 0)
            {
                if (strAll.Length > 0)
                {
                    int index = 1;
                    for (i = 0; i <= strAll.Length; i++)
                    {
                        if (!(i >= strAll.Length))
                        {
                            try
                            {
                                hs.Add("A" + index, strPara.Substring(Convert.ToInt32(strAll[i]) + 1, Convert.ToInt32(strAll[i + 1]) - Convert.ToInt32(strAll[i]) - 1));
                            }
                            catch
                            {
                                hs.Add("A" + index, "");
                            }
                            index = index + 1;
                        }
                        else { }
                        i = i + 1;
                    }
                }
                else { }
            }
            else { }
            foreach (var s in hs.Keys)
            {
                strPara = strPara.Replace("'" + Convert.ToString(hs[s]) + "'", s.ToString());
            }
            string[] strParaArray = strPara.Replace(" WHERE ", ";").Split(';');

            _Query = "";
            if (strParaArray[0].Length > 0)
                _Query = strParaArray[0];
            else { }
            string strparameters = "";
            _HS = hs;
            return strparameters;
        }
        #endregion

        #region Edit Object
        protected void lnkeditbtn_Click(object sender, EventArgs e)
        {
            if (this.CompanyId.Length == 0) return;
            //   ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('.ImageQPContentRow').remove();" + "');", true);
            if (hidconid.Value.Trim().Length != 0)
                EditDBCommand(this.SubAdminid, this.CompanyId, hidconid.Value, updDeleteDbCmd);
            else
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Internal server error.<br>" + "');", true);
            ScriptManager.RegisterStartupScript(updDeleteDbCmd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "UnformDatabaseObject();EditDBobject();disabletextobject();", true);
            hideditdone.Value = "5";

            btnDbCommand_Reset.Visible = false;
        }

        protected void EditDBCommand(string _SubAdminId, string _CompanyId, string _CommandId, UpdatePanel _Upd)
        {
            string addtionalJquery = "", strDbType = "";
            GetDbCommand objGetDbCommand = new GetDbCommand(false, false, _SubAdminId, _CommandId, hdfDbType1.Value, _CompanyId, "1");
            DataTable dt = objGetDbCommand.ResultTable;
            var IsDbConnected = true;
            try
            {
                if (objGetDbCommand.StatusCode == 0)
                {
                    string strHtml = "";
                    hiddbtypetrpe.Value = Convert.ToString(dt.Rows[0]["DB_COMMAND_TYPE"]).Trim();
                    hidnoexecutecondtion.Value = Convert.ToString(dt.Rows[0]["NO_EXECUTE_CONDITION"]).Trim();
                    hdfDbCmdType.Value = Convert.ToString(dt.Rows[0]["DATABASE_TYPE"]);
                    lblcreatedby.Text = Convert.ToString(dt.Rows[0]["CREATEDBY"]);
                    string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(dt.Rows[0]["UPDATED_ON"]));
                    lblupdatedby.Text = Convert.ToString(dt.Rows[0]["FULL_NAME"]) + " On " + strtime;
                    ClearDbCommandDetails();
                    FillDbCommandCtrl(dt);
                    if (IsDbConnected)
                    {

                        switch (ddlCommandType.SelectedValue)
                        {
                            case "1":
                                addtionalJquery = EditSelectCommand(dt);
                                if (strDbType == "5") btnDbCmdSwitchToAdv.Visible = false;
                                break;
                            case "2":
                                EditInsertCommand(dt, _SubAdminId, _CompanyId);
                                break;
                            case "3":
                                EditUpdateCommand(dt, _SubAdminId, _CompanyId);
                                break;
                            case "4":
                                EditDeleteCommand(dt, _SubAdminId, _CompanyId);
                                break;
                            case "5":
                                EditstoreProceCommand(dt, _SubAdminId, _CompanyId);
                                break;
                        }
                        btnDbBackClick.Visible = true;
                    }
                    else
                    {
                    }


                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CommandType('0');$('[id$=ddlCommandTypeDiv]').show();", true);
                    hdfIsinsertQB.Value = "0";
                    updAddDbCommand.Update();
                    CatchBind(dt);
                    updDbCmdParaJson.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(),
                                                                                   @"AddParameterInDbCommand();" + "$('#DbCmdAppDiv').html('" + strHtml + "');$('[id$=divdonotexecute]').show();Noexecuteparameter();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), addtionalJquery, true);
                    ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();", true);

                }
                else { }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }
        }
        private void EditInsertCommand(DataTable _DataTable, string _SubAdminId, string _CompanyId)
        {
            txtDbCommand_SqlQuery.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);
            lblDbCmd_ConnName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
            //lblcreatedby.Text = Convert.ToString(_DataTable.Rows[0]["CREATEDBY"]);
            //string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(_DataTable.Rows[0]["UPDATED_ON"]));
            hdfOutCol.Value = Convert.ToString(_DataTable.Rows[0]["COLUMNS"]);
            //lblupdatedby.Text = Convert.ToString(_DataTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
            txtDbCommand_SqlQueryPara.Text = Convert.ToString(_DataTable.Rows[0]["COLUMNS"]);
            hdfInsertQueryPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER"]);
            dd1DataCatch.SelectedValue = Convert.ToString(_DataTable.Rows[0]["CACHE"]);
            updInsertSqlQryTxt.Update();
        }
        private void EditDeleteCommand(DataTable _DataTable, string _SubAdminId, string _CompanyId)
        {
            hdfInsertQueryPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER"]);
            updInsertSqlQryTxt.Update();
            //lblcreatedby.Text = Convert.ToString(_DataTable.Rows[0]["CREATEDBY"]);
            //string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(_DataTable.Rows[0]["UPDATED_ON"]));
            //lblupdatedby.Text = Convert.ToString(_DataTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
            hdfDbCmdPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER_JSON"]);
            updDbCmdParaJson.Update();

            lblDbCmd_ConnName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
            dd1DataCatch.SelectedValue = Convert.ToString(_DataTable.Rows[0]["CACHE"]);
            txtDbCommand_SqlQuery.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);
        }
        private string EditSelectCommand(DataTable _DataTable)
        {
            string strScript = "";

            // lblcreatedby.Text = Convert.ToString(_DataTable.Rows[0]["CREATEDBY"]);
            //string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(_DataTable.Rows[0]["UPDATED_ON"]));
            //lblupdatedby.Text = Convert.ToString(_DataTable.Rows[0]["FULL_NAME"]) + " On " + strtime;

            hdfDbCmdPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER_JSON"]);
            updDbCmdParaJson.Update();
            hdfOutCol.Value = Convert.ToString(_DataTable.Rows[0]["COLUMNS"]);
            strScript = "setValInHdfDataTrans('','" + ReplaceSingleCourseAndBackSlash(Convert.ToString(_DataTable.Rows[0]["COLUMNS"])) + "');";
            txtDbCommand_SqlQuery.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);
            lblDbCmd_ConnName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
            strScript += GetDbCmdOutPataScript(Convert.ToString(_DataTable.Rows[0]["COLUMNS"]));
            return strScript;
        }
        private string EditstoreProceCommand(DataTable _DataTable, string _SubAdminId, string _CompanyId)
        {
            string strScript = "";

            // lblcreatedby.Text = Convert.ToString(_DataTable.Rows[0]["CREATEDBY"]);
            //string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(_DataTable.Rows[0]["UPDATED_ON"]));
            //lblupdatedby.Text = Convert.ToString(_DataTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
            hdfDbCmdPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER_JSON"]);
            updDbCmdParaJson.Update();
            hdfOutCol.Value = Convert.ToString(_DataTable.Rows[0]["COLUMNS"]);
            strScript = "setValInHdfDataTrans('','" + ReplaceSingleCourseAndBackSlash(Convert.ToString(_DataTable.Rows[0]["COLUMNS"])) + "');";
            txtDbCommand_SqlQuery.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);

            lblDbCmd_ConnName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
            dd1DataCatch.SelectedValue = Convert.ToString(_DataTable.Rows[0]["CACHE"]);

            hidCatchdetails.Value = Convert.ToString(_DataTable.Rows[0]["CACHE"]);
            hidcatchexfrequency.Value = Convert.ToString(_DataTable.Rows[0]["EXPIRY_FREQUENCY"]);
            hidcatchexcondition.Value = Convert.ToString(_DataTable.Rows[0]["EXPIRY_CONDITION"]);

            hdfDbCmdPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER_JSON"]);
            string strConnectionString1, strAgent, strDbType, strDbName = "";
            string conand = Convert.ToString(_DataTable.Rows[0]["DB_CONNECTOR_ID"]);

            ddlCommandType.SelectedValue = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_TYPE"]);
            stroreprocedure();

            strScript += GetDbCmdOutPataScript(Convert.ToString(_DataTable.Rows[0]["COLUMNS"]));
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript, true);
            strScript = "";
            strConnectionString1 = BindDbCmdTableDropDown(false, conand, "", this.CompanyId, out strAgent, out strDbType, out strDbName, true);
            drpstoreprocedure.SelectedIndex = drpstoreprocedure.Items.IndexOf(drpstoreprocedure.Items.FindByValue(Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"])));
            return strScript;
        }
        private void EditUpdateCommand(DataTable _DataTable, string _SubAdminId, string _CompanyId)
        {
            try
            {
                //lblcreatedby.Text = Convert.ToString(_DataTable.Rows[0]["CreatedBY"]);
                //string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(_DataTable.Rows[0]["UPDATED_ON"]));
                //lblupdatedby.Text = Convert.ToString(_DataTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
                txtDbCommand_SqlQuery.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);
                txtDbCommand_SqlQueryWH.Text = "";
                hdfInsertQueryPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER"]);
                lblDbCmd_ConnName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
                updInsertSqlQryTxt.Update();

                hdfDbCmdPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER_JSON"]);
                updDbCmdParaJson.Update();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }
            updDbCmdRptInsert.Update();
        }
        private void CatchBind(DataTable dt)
        {
            hidnoexecutecondtion.Value = Convert.ToString(dt.Rows[0]["NO_EXECUTE_CONDITION"]);

            object objcoloumcheck = Convert.ToString(dt.Rows[0]["IMAGE_COLUMN"]);
            string strnullcoloumcheck = objcoloumcheck.ToString();
            if (strnullcoloumcheck != "")
            {
                string stritemcoloum = "";
                string imgtype = "";
                JArray lst = JArray.Parse(Convert.ToString(dt.Rows[0]["IMAGE_COLUMN"]));
                if (lst.Count != 0)
                    foreach (JArray item in lst)
                    {
                        if (stritemcoloum == "")
                        {
                            stritemcoloum = item[0].ToString();
                            imgtype = item[1].ToString();
                        }
                        else
                        {
                            stritemcoloum += ',' + item[0].ToString();
                            imgtype += ',' + item[1].ToString();
                        }
                    }

                hidimagecoloums.Value = Convert.ToString(dt.Rows[0]["IMAGE_COLUMN"]);
                lblImageColumns.Text = stritemcoloum;

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"EditImageSource();", true);
            }
            dd1DataCatch.SelectedValue = Convert.ToString(dt.Rows[0]["CACHE"]);
            hdfDbCmdPara.Value = Convert.ToString(dt.Rows[0]["PARAMETER_JSON"]);
            hidimageoutput.Value = Convert.ToString(dt.Rows[0]["COLUMNS"]);
            string strcatchtype = Convert.ToString(dt.Rows[0]["CACHE"]);
            if (strcatchtype == "1")
            {
                dd1AbMonth.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_FREQUENCY"]);

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"catchtype('" + strcatchtype + "'), absolute('" + dd1AbMonth.SelectedValue + "')", true);
                if (dd1AbMonth.SelectedValue == "0")
                    dd1AbHr.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else if (dd1AbMonth.SelectedValue == "1")
                {
                    string strexp = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                    string[] strhhmm = strexp.Split(':');
                    dd1Abhh.SelectedValue = strhhmm[0];
                    ddlAbMM.SelectedValue = strhhmm[1];
                }
                else if (dd1AbMonth.SelectedValue == "2")
                    ddlAbDay.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else if (dd1AbMonth.SelectedValue == "3")
                    dd1AbMnDay.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else if (dd1AbMonth.SelectedValue == "4")
                {
                    string strexpyear = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                    string[] strhhmm = strexpyear.Split('/');
                    dd1AbMnthDay.SelectedValue = strhhmm[0];
                    ddlAbMnth.SelectedValue = strhhmm[1];
                }
                else
                {
                    dd1AbMnthDay.SelectedIndex = 0;
                    dd1AbMnDay.SelectedIndex = 0;
                    ddlAbDay.SelectedIndex = 0;
                    dd1Abhh.SelectedIndex = 0;
                    ddlAbMM.SelectedIndex = 0;
                    dd1AbHr.SelectedIndex = 0;
                }
            }
            else if (strcatchtype == "2")
            {
                ddlRlt.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_FREQUENCY"]);
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"catchtype(" + strcatchtype + "), relativetcatch('" + ddlRlt.SelectedValue + "')", true);
                if (ddlRlt.SelectedValue == "1")
                    Drprlthour.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else if (ddlRlt.SelectedValue == "2")
                    rltdayd.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else if (ddlRlt.SelectedValue == "3")
                    raltweeks.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else if (ddlRlt.SelectedValue == "0")
                    drprltmin.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                else
                    drprltmonth.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
            }
        }

        private void ShowSelectOutParam(UpdatePanel _Upd)
        {
            try
            {
                SqlParser myParser = new SqlParser();
                myParser.Parse(txtDbCommand_SqlQuery.Text);
                ArrayList lst = myParser.GetOutputColumn();
                JArray strJson = new JArray();
                if (lst != null && lst.Count > 0)
                {
                    foreach (string str in lst)
                    {
                        JObject jo = new JObject();
                        jo.Add("id", str);
                        jo.Add("name", str);
                        strJson.Add(jo);
                    }
                }
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AutoCompleteTextbox('" + strJson.ToString(Newtonsoft.Json.Formatting.None) + "');", true);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AutoCompleteTextbox('');", true);
            }
        }
        private void GetSelectQueryOutPutColumn(string _Sql)
        {
            string strParam = "";
            if (ddlCommandType.SelectedValue == "1")
            {
                try
                {
                    _Sql = _Sql.TrimStart().Substring(6, _Sql.Length - 6).TrimStart();
                    _Sql = _Sql.Substring(0, _Sql.ToUpper().IndexOf(" FROM "));
                    foreach (string str in _Sql.Split(','))
                    {
                        if (str.ToUpper().IndexOf(" AS ") != -1)
                        {
                            strParam += (strParam.Length > 0 ? "," : "");
                            strParam += str.Substring(str.ToUpper().IndexOf(" AS ") + 4, str.Length - (str.ToUpper().IndexOf(" AS ") + 4));
                        }
                        else
                        {
                            strParam += (strParam.Length > 0 ? "," : "");
                            strParam += str;
                        }
                    }
                    hdfOutCol.Value = strParam;
                }
                catch
                {
                    hdfOutCol.Value = "";
                }
            }
            else
            {
                hdfOutCol.Value = "";
            }
        }

        private void stroreprocedure()
        {
            dispalyInputPara(true, false);
            SqlQueryOutputDiv.Visible = true;
            pnlDbCmdColRpt.Visible = false;
            lnRefButton.Visible = false;
            divSelectSqlQueryLabel.Visible = false;
            divwhere.Visible = true;
            divctlwhere.Visible = true;
            UpdatePanel32.Update();
            divInsertSqlQueryLabel.Visible = false;
            divstoreprocedure.Visible = true;
            divsp.Visible = true;
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"HideSqlQueryhide();", true);
        }
        private void FillDbCommandCtrl(DataTable _DataTable)
        {
            txtDbCommand_CommandId.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_ID"]);
            txtDbCommand_CommandName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
            txtDbCommand_SqlQuery.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);
            lblcreatedby.Text = Convert.ToString(_DataTable.Rows[0]["CREATEDBY"]);
            string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(_DataTable.Rows[0]["UPDATED_ON"]));
            lblupdatedby.Text = Convert.ToString(_DataTable.Rows[0]["FULL_NAME"]) + " On " + strtime;

            hdfInsertQueryPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER"]);
            hdfDbCmdPara.Value = Convert.ToString(_DataTable.Rows[0]["PARAMETER"]);
            updInsertSqlQryTxt.Update();
            ddl_DbConnactor.SelectedIndex = ddl_DbConnactor.Items.IndexOf(ddl_DbConnactor.Items.FindByValue(Convert.ToString(_DataTable.Rows[0]["DB_CONNECTOR_ID"]).Trim()));
            ddlCommandType.SelectedIndex = ddlCommandType.Items.IndexOf(ddlCommandType.Items.FindByValue(Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_TYPE"]).Trim()));

            lblDbCmd_Name.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);
            lblDbCmd_CmdType.Text = ddlCommandType.Items.FindByValue(Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_TYPE"]).Trim()).Text;
            lblDbCmd_Query.Text = Convert.ToString(_DataTable.Rows[0]["SQL_QUERY"]);

            lblDbCmdDesc.Text = Convert.ToString(_DataTable.Rows[0]["DESCRIPTION"]);
            txtDBCmdDesc.Text = Convert.ToString(_DataTable.Rows[0]["DESCRIPTION"]);
            lblDbCmd_ConnName.Text = Convert.ToString(_DataTable.Rows[0]["DB_COMMAND_NAME"]);

            lblDbCmd_TableName.Text = Convert.ToString(_DataTable.Rows[0]["TABLE_NAME"]).Trim();

        }


        protected void savebtn_Click(object sender, EventArgs e)
        {

            GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, "", hidperconnectionname.Value, this.CompanyId);
            //DatabaseType db;
            if (objGetDatabaseConnection != null)
            {
                JObject crdtial = getCradential(Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim());
                if (crdtial != null)
                {
                    if (Convert.ToString(crdtial["uid"]) != "" && Convert.ToString(crdtial["pwd"]) != "")
                    {
                        ShowTestResult(Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]));
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Invalid credential.');", true);
                }
            }
        }

        bool ShowTestResult(string _unm, string _pwd)
        {
            bool flag = false;
            string outError = "";
            string st = Utilities.testObjectRequest(this.CompanyId, hidconid.Value, "1", (
                (hdflp.Value.Length <= 0) ? new JArray() : JArray.Parse(hdflp.Value))
                , AesEncryption.AESEncrypt(this.CompanyId, _unm), AesEncryption.AESEncrypt(this.CompanyId, _pwd), out outError);
            if (st.Length > 0)
            {
                JObject dataObject = JObject.Parse(st);
                JObject dataObjectresp = JObject.Parse(dataObject["resp"].ToString());
                JObject dataObjectRespStatus = JObject.Parse(dataObjectresp["status"].ToString());

                int statuscode = Convert.ToInt32(dataObjectRespStatus["cd"].ToString());

                if (statuscode == 0)
                {
                    flag = true;
                    JObject dataObjectRespData = JObject.Parse(dataObjectresp["data"].ToString());
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CreateDynamicTable(" + dataObjectRespData["dt"].ToString() + ");", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"opendialogtest(false);SubProcBoxMessageDataConection(true,'Error');$('#aMessage').html('" + dataObjectRespStatus["desc"].ToString() + "Error Code :" + statuscode.ToString() + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"opendialogtest(false);SubProcBoxMessageDataConection(true,'Error');$('#aMessage').html('Http Error Code :" + outError + "');", true);
            }
            return flag;
        }

        #endregion

        #region GetAll Input/Output Parameter
        private void GetInsertCmdParam(string _Sql)
        {
            string strCols = _Sql.Substring(_Sql.IndexOf('(') + 1, ((_Sql.IndexOf(')')) - (_Sql.IndexOf('(')) - 1));
            string str = _Sql.Substring(_Sql.IndexOf(')') + 1, (_Sql.Length - _Sql.IndexOf(')') - 1));
            string strPara = "";
            string strKey = (hdfDbCmdType.Value == "4" || hdfDbCmdType.Value == "2" ? ":" : "@");
            if (pnlDbCmdColRpt.Visible)
            {
                strPara = str.Substring(str.IndexOf('(') + 1, (str.LastIndexOf(')') - str.IndexOf('(') - 1));
            }
            else { }
            int i = 0;
            string strIndex = "", strCompCol = "";
            foreach (char c in strPara.ToCharArray())
            {
                if (c.ToString().Equals("'"))
                {
                    if (strIndex.Length > 0)
                    {
                        strIndex += ",";
                    }
                    else { }
                    strIndex += i.ToString();
                }
                else { }
                i = i + 1;
            }
            string[] strAll = strIndex.Split(',');
            Hashtable hs = new Hashtable();
            if (strAll.Length > 0)
            {
                int index = 1;
                for (i = 0; i <= strAll.Length; i++)
                {
                    if (!(i >= strAll.Length))
                    {
                        if (strAll[i].Trim().Length > 0)
                        {
                            try
                            {
                                hs.Add("A" + index, strPara.Substring(Convert.ToInt32(strAll[i]) + 1, Convert.ToInt32(strAll[i + 1]) - Convert.ToInt32(strAll[i]) - 1));
                            }
                            catch
                            {
                            }
                        }
                        else { }
                        index = index + 1;
                    }
                    else { }
                    i = i + 1;
                }
            }
            foreach (var s in hs.Keys)
            {
                strPara = strPara.Replace("'" + Convert.ToString(hs[s]) + "'", s.ToString());
            }
            string[] strParaArray = strPara.Split(',');
            foreach (RepeaterItem r in rptDbCommandTblCol.Items)
            {
                DataRowView drv = (DataRowView)r.DataItem;
                Literal lit_Col = (Literal)r.FindControl("lit_Col");
                TextBox txt_Prarameter = (TextBox)r.FindControl("txt_Prarameter");
                //CheckBox chk_Select = (CheckBox)r.FindControl("chk_Select");
                i = 0;
                foreach (var v in strCols.Split(','))
                {
                    strCompCol = (hdfDbCmdType.Value == "4" ? v.Replace("\"", "") : v.Split('=')[0]);
                    if (lit_Col.Text == strCompCol)
                    {
                        //chk_Select.Checked = true;
                        if (i < strParaArray.Length)
                        {
                            try
                            {
                                if (strParaArray[i].IndexOf(strKey) != -1)
                                {
                                    txt_Prarameter.Text = strKey + strParaArray[i].Replace(strKey, "");
                                }
                                else
                                {
                                    if (hs[strParaArray[i]] == null)
                                    {
                                        txt_Prarameter.Text = strParaArray[i];
                                    }
                                    else
                                    {
                                        txt_Prarameter.Text = "'" + Convert.ToString(hs[strParaArray[i]]) + "'";
                                    }
                                }
                            }
                            catch
                            {
                                txt_Prarameter.Text = "";
                            }
                        }
                        else { }
                    }
                    else { }
                    i = i + 1;
                }
            }
        }
        private string GetDbCmdOutPataScript(string _Value)
        {
            string strJson = "";
            string[] strOut = _Value.Split(',');
            foreach (string str in strOut)
            {
                strJson = (strJson.Length > 0 ? (strJson + ",") : strJson);
                strJson += "{ \"id\": \"" + ReplaceSingleCourseAndBackSlash(str) + "\", \"name\": \"" + ReplaceSingleCourseAndBackSlash(str.Replace(";", "")) + "\" }";
            }
            if (strJson.Length > 0) strJson = "[" + strJson + "]";

            return "try{AutoCompleteTextbox('" + strJson + "');}catch(err){AutoCompleteTextbox('');}";
        }
        #endregion

        #region Datacatch Index Change Methods

        private void CacheDropdownBind()
        {
            DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(null);
            for (int i = 0; i <= 59; i++)
            {
                ddlAbMM.Items.Add(new ListItem(i.ToString(), i.ToString()));
                if (i > 0)
                    drprltmin.Items.Add(new ListItem(i.ToString(), i.ToString()));
                if (i > 0 && i <= 52)//week
                    raltweeks.Items.Add(new ListItem(i.ToString(), i.ToString()));
                if (i <= 23)//hour
                {
                    dd1Abhh.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    if (i > 0)
                        Drprlthour.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                if (i > 0 && i <= 12)//month
                {
                    ddlAbMnth.Items.Add(new ListItem(info.GetMonthName(i), i.ToString()));
                    drprltmonth.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                if (i > 0 && i <= 31)//day
                {
                    rltdayd.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    dd1AbMnthDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    dd1AbMnDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
        }

        #endregion

        #region StoreProcedure Index Changed
        protected void drpstoreprocedure_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hidconid.Value != "" && drpstoreprocedure.SelectedValue != "-1")
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"EditDBobject();AutoCompleteTextbox('');SelectedStoreProcedure('" + drpstoreprocedure.SelectedValue + "');UnformDatabaseObject();", true);
            else if (drpstoreprocedure.SelectedValue != "-1")
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AutoCompleteTextbox('');SelectedStoreProcedure('" + drpstoreprocedure.SelectedValue + "');UnformDatabaseObject();", true);
            else
                ScriptManager.RegisterStartupScript(updAddDbCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UnformDatabaseObject();ClearProc();AutoCompleteTextbox('');", true);
        }
        #endregion

        protected void CommandCopy_Save_Click(object sender, EventArgs e)
        {
            if (ValidateCopyObject()) return;
            else
            {
                try
                {
                    SaveCopyCommand(hidconid.Value, txtcopyobject.Text);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
                }
            }
        }

        private void SaveCopyCommand(string DbCommandId, string COMMAND_NAME)
        {
            GetDbCommand objGetDbCommand = new GetDbCommand(false, false, this.SubAdminid, "", COMMAND_NAME, this.CompanyId, "1");
            if (objGetDbCommand.StatusCode == 0 && objGetDbCommand.ResultTable.Rows.Count > 0)
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Database object Name already exist.');UnformDatabaseObject();", true);
            else
            {
                AddNewDbCommand objAddNewDbCommand = new AddNewDbCommand(this.SubAdminid, COMMAND_NAME, DbCommandId, this.CompanyId, Convert.ToInt64(DateTime.UtcNow.Ticks));
                if (objAddNewDbCommand.StatusCode == 0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcConfirmBoxMessageDB(true,'Saved',250);$('#aCfmMessage').html('" + "* Database Object Saved Succsessfully." + "');$('#aCfmUpdateMessage').html('');$('#aCFmessage').html('');opencopyobject(false);", true);
                    UpdateAllUpdatePanel();
                    AfterSave(true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowDbCmdDivEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');UnformDatabaseObject();", true);
                    return;
                }
            }
        }

        protected void btnDbCommand_Reset_Click(object sender, EventArgs e)
        {
            DBClearAllDetails();
            divstoreprocedure.Visible = false;
        }

        protected void bindCreadential()
        {
            hidWsAuthencationMeta.Value = "";
            GetEnterpriseAdditionalDefinition objAuthMeta = new GetEnterpriseAdditionalDefinition(this.CompanyId);
            if (objAuthMeta.StatusCode == 0)
            {
                if (objAuthMeta.StatusCode == 0 && objAuthMeta.ResultTable.Rows.Count > 0)
                {
                    JArray lst = Utilities.decriptAllCredential(Convert.ToString(objAuthMeta.ResultTable.Rows[0]["AUTHENTICATION_META"]), this.CompanyId);
                    hidWsAuthencationMeta.Value = lst.ToString();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Internal Server Error" + "');", true);
            }
        }

        protected void lnkQueryDefined_Click(object sender, EventArgs e)
        {
            string strAgent = "", strDbType = "", strDbName = "";
            if (ddlCommandType.SelectedValue == "2")
            {
                divwhere.Visible = false;
                divctlwhere.Visible = false;
                UpdatePanel32.Update();
            }
            else
            {
                divwhere.Visible = true;
                divctlwhere.Visible = true;
                UpdatePanel32.Update();
            }
            if (hdfIsinsertQB.Value == "0")
            {
                BindDbCmdTableDropDown(false, ddl_DbConnactor.SelectedValue, "", this.CompanyId, out strAgent, out strDbType, out strDbName, true);
                BindTableColumn(null);
            }
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"AddParameterInDbCommand();$('[id$=ddlCommandTypeDiv]').show();CommandType('0');AutoCompleteTextbox('');", true);

        }

        protected void btnSaveDb_Click(object sender, EventArgs e)
        {
            hiddbobjectsucess.Value = "1";
            btnDbCommand_Save_Click(sender, e);
        }

        protected void btndiscardyesclick(object sender, EventArgs e)
        {
            hdfDbCmdPara.Value = "";
            updDbCmdParaJson.Update();
            DBClearAllDetails();

            btnDbBackClick.Visible = false;
            btnDbCommand_Reset.Visible = true;
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"BindDatabaseObjectdata();", true);
        }

        protected void btn_storeprocedureID(object sender, EventArgs e)
        {

            string _DbName = "", _DbType = "", strAgent, strusername, strpassword;

            GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, ddl_DbConnactor.SelectedValue, "", this.CompanyId);
            if (objGetDatabaseConnection != null)
            {
                _DbType = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]).Trim();
                _DbName = AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"]).Trim());
                strusername = AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["USER_ID"]).Trim());
                strpassword = AesEncryption.AESDecrypt(this.CompanyId, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["PASSWORD"]).Trim());
                hdfDbType.Value = _DbType;

                if (strusername == "" || strpassword == "" || strusername == "" && strpassword == "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('[id$=ddlCommandTypeDiv]').show();CommandTypeDetail('rr');DataCredentialBy(true);", true);
                    return;
                }
                else
                {
                    int MetaType = (ddlCommandType.SelectedValue == "5" ? 2 : 1);
                    bool flag = spGetMeta(false, ddl_DbConnactor.SelectedValue, this.CompanyId, out strAgent, _DbType, _DbName, objGetDatabaseConnection.ResultTable, strusername, strpassword, MetaType);
                }
            }

            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('[id$=ddlCommandTypeDiv]').show();CommandTypeDetail('rr');", true);


        }

        private bool spGetMeta(Boolean _IsSelectAll, string _ConnectorId, string _ComapnyId, out string _AgentName, string _DbType, string _DbName, DataTable _dbConnection, string _uid, string _pwd, int _type)
        {
            bool flag = false;
            _AgentName = "";

            _AgentName = Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]);
            if (Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]).Length != 0)
            {
                if (hdConnection.Value.ToLower() != ddl_DbConnactor.SelectedItem.Text || drpstoreprocedure.Items.Count == 0)
                {
                    DataSet dssp = GetMetaData(_ComapnyId, _ConnectorId, ((int)Different_Type_Meta.STORED_PROCEDURES).ToString(), "", Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]), _DbType, _uid, _pwd);
                    DataSet dspara = GetMetaData(_ComapnyId, _ConnectorId, ((int)Different_Type_Meta.STORED_PROCEDURE_PARAMS).ToString(), "", Convert.ToString(_dbConnection.Rows[0]["MPLUGIN_AGENT"]), _DbType, _uid, _pwd);
                    if (dssp != null)
                    {
                        BindStoreProcedureDropdownFromMplugin(dssp, dspara, _DbType);
                        flag = true;
                    }
                    else
                    {
                        BindStoredProcedureDropdown(null);

                    }
                }
                updatetable.Update();
            }
            return flag;
        }
    }
}