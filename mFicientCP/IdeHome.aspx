﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/IdeMaster.Master" AutoEventWireup="true"
    CodeBehind="IdeHome.aspx.cs" Inherits="mFicientCP.IdeHome" EnableEventValidation="false"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script type="text/javascript">        
    function isCookieCleanUpRequired(value) {
            if (typeof value === "undefined") {
                $('#' + '<%=hidIsCleanUpRequired.ClientID %>').val('false'); 
            }
            else {
                $('#' + '<%=hidIsCleanUpRequired.ClientID %>').val(value);
            }
        }
        </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainCanvas" runat="server">   
    <asp:HiddenField ID="hidIsCleanUpRequired" runat="server" />
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
    </script>
</asp:Content>
