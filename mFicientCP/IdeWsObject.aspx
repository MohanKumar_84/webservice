﻿<%@ Page Title="mStudio | Web-Service Object" Language="C#" MasterPageFile="~/master/IdeMaster.Master"
    AutoEventWireup="true" CodeBehind="IdeWsObject.aspx.cs" Inherits="mFicientCP.IdeWsObject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery.datatables.css" rel="stylesheet" type="text/css" />
    <style>
        .ui-icon-closethick
        {
            margin: 0px;
        }
        .selector
        {
            float: left;
        }
        .addform
        {
            float: right;
            margin-left: 8px;
            margin-right: 7px;
            width: 49%;
            min-width: 450px;
        }
        .datatablebind
        {
            width: 49%;
            float: left;
            margin-left: 7px;
        }
        .DbConnLeftDiv
        {
            float: left;
            padding-top: 5px;
            padding-left: 10px;
            width: 20%;
            margin-top: 9px;
            cursor: default;
        }
        .selects
        {
            margin-top: 9px !important;
        }
        .inpst
        {
            margin-left: 2px !important;
        }
        .catchctrl
        {
            margin-top: 17px !important;
        }
        .selected
        {
            background-repeat: repeat-x;
            background-color: #13aae1 !important;
            filter: none;
            color: #ffffff;
            text-shadow: 0px -1px 0 rgba(0,0,0,0.3);
        }
        .QPContentText1
        {
            padding-left: 3px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 48px;
            float: left; /*  border-bottom: solid 1px silver;
    border-right: solid 1px silver; */
            height: 20px;
        }
        .QPContentType1
        {
            padding-left: 13px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 190px;
            float: left; /*  border-bottom: solid 1px silver;
    border-right: solid 1px silver; */
            height: 20px;
        }
        .hidediv
        {
            display: none;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="css/Jquerydatatablerowclick.css" />
    <script src="Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/Idewsobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        function btnImportClick() {
            SubProcIframe(false);
            $('[id$=btnImport]').click();
        }
        function UpdBtnRowIndex() {
            $('[id$=<%= lnkadd.ClientID %>]').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div style="padding: 10px;">
        <section style="margin: 5px 5px 5px 3px;">
            <asp:Panel ID="Paneldbobjects" CssClass="repeaterBox" runat="server">
                <asp:Panel ID="PaneldbobjectsHeader" CssClass="repeaterBox-header" runat="server">
                    <div>
                        <asp:Label ID="lblwsobject" runat="server" Text="<h1>Web Service Objects</h1>">
                        </asp:Label>
                    </div>
                    <div style="position: relative; top: 10px; right: 15px;">
                        <asp:LinkButton ID="lnkadd" runat="server" Text="Add" CssClass="repeaterLink fr"
                            OnClientClick="isCookieCleanUpRequired('false');AddWsObject();return addbtnclick();RemoveStyle();"
                            OnClick="lnkadd_Click"></asp:LinkButton>
                    </div>
                </asp:Panel>
                <div id="divwsobjectComplete" style="padding-top: 5px">
                    <asp:UpdatePanel runat="server" ID="upwsobject" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divwsobjleft" class="g5" style="width: 47%; padding: 0 !important">
                                <div id="divwsdataobject" class="g12" style="padding: 0 !important">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div id="AddWsCommandDiv" runat="server" class="g7" style="width: 49% !important">
                        <asp:UpdatePanel ID="updatedbright" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin: 0px auto;">
                                    <div class="ProcPriview" style="width: 98%; padding: 5px;">
                                        <asp:UpdatePanel runat="server" ID="updAddWsCommand" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div id="viewinfo">
                                                    <div id="WsCmdDetailsDiv" class="modalPopUpDetails hide" style="width: 97%;">
                                                        <div class="modalPopUpDetHeaderDiv">
                                                            <div class="modalPopUpDetHeader">
                                                                <asp:Label ID="lblWsCmd_Name" runat="server"></asp:Label>
                                                            </div>
                                                            <div style="float: right;">
                                                                <div style="float: left;">
                                                                    <asp:UpdatePanel runat="server" ID="updDelWsCmd" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <div class="FLeft">
                                                                                <asp:LinkButton ID="LinkButton2" runat="server" Text="Copy" OnClientClick="isCookieCleanUpRequired('false');openwscopyobject(true); return false;"></asp:LinkButton>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                &nbsp;|&nbsp;
                                                                            </div>
                                                                            <div class="FLeft">
                                                                                <asp:LinkButton ID="lnkeditbtn" runat="server" Text="Edit" OnClick="btnwsobject_Click"
                                                                                    OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                                                <asp:HiddenField ID="hideditdone" runat="server" />
                                                                            </div>
                                                                            <div class="FLeft">
                                                                                <asp:Button ID="savebtn" runat="server" OnClick="savebtn_Click" Style="display: none" />
                                                                                <asp:LinkButton ID="lnktest" runat="server" Text="Test" OnClientClick="isCookieCleanUpRequired('false');dialogtest();return false;"></asp:LinkButton>
                                                                                <asp:HiddenField ID="hdflp" runat="server" />
                                                                                <div style="float: left;">
                                                                                    &nbsp;|&nbsp;</div>
                                                                            </div>
                                                                            <div style="float: left;">
                                                                                &nbsp;|&nbsp;</div>
                                                                            <asp:LinkButton ID="lnkWsCmdDelete" runat="server" Text="Delete" OnClientClick="isCookieCleanUpRequired('false');"
                                                                                OnClick="lnkWsCmdDelete_Click"></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div id="WsCmdDetailsDiv1" class="modalPopUpDetRow" style="border: none; padding: 0px;">
                                                            <div class="modalPopUpDetRow">
                                                                <div class="modalPopUpDetColumn1">
                                                                    Description
                                                                </div>
                                                                <div class="modalPopUpDetColumn2">
                                                                    :
                                                                </div>
                                                                <div class="modalPopUpDetColumn3">
                                                                    <asp:Label ID="lblWsCmdDesc" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hidwsconname" runat="server" />
                                                                    <asp:HiddenField ID="hidwscondtls" runat="server" />
                                                                    <asp:HiddenField ID="hidwebdatasource" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="modalPopUpDetRow">
                                                                <div class="modalPopUpDetColumn1">
                                                                    Service Type
                                                                </div>
                                                                <div class="modalPopUpDetColumn2">
                                                                    :
                                                                </div>
                                                                <div class="modalPopUpDetColumn3">
                                                                    <asp:Label ID="lblWsCmd_ServiceType" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div id="WsWsdlCmdDetailsDiv" runat="server" class="FLeft" style="width: 100%">
                                                                <div class="modalPopUpDetRow" id="wsservcicenm">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Service Name
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_WeSrc" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="modalPopUpDetRow hide" id="mport">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Port Type
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_PortType" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="modalPopUpDetRow" id="hmethod">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Method
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_Method" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="WsHttpCmdDetailsDiv" runat="server" class="modalPopUpDetRow" style="padding-left: 0px;
                                                                border: 0;">
                                                                <div class="modalPopUpDetRow" id="wshttpcmddtlddiv">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Host Name
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_InHost" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="modalPopUpDetRow" id="wshttpcmddtlddiv2">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Url Template
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_InPara" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="modalPopUpDetRow" id="wshttpcmddatadtlddiv2">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Data Template
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_DataPara" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="modalPopUpDetRow" id="wshttpcmddtlddiv3">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Dataset Path
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_DataSetPath" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                               <%--  <div class="modalPopUpDetRow" id="contantType">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Contant Type
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblContantType" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>--%>
                                                                <div class="modalPopUpDetRow" id="wshttpcmddt1div4">
                                                                    <div class="modalPopUpDetColumn1">
                                                                        Output Elements
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn2">
                                                                        :
                                                                    </div>
                                                                    <div class="modalPopUpDetColumn3">
                                                                        <asp:Label ID="lblWsCmd_OutPara" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modalPopUpDetRow  hide" id="viewRpcparameterout">
                                                                <div class="modalPopUpDetColumn1">
                                                                    Input Parameters
                                                                </div>
                                                                <div class="modalPopUpDetColumn2">
                                                                    :
                                                                </div>
                                                                <div class="modalPopUpDetColumn3">
                                                                    <asp:Label ID="lblWsCmd_Inputpara" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="modalPopUpDetRow hide" id="viewRpcparameterout2">
                                                                <div class="modalPopUpDetColumn1">
                                                                    Output Parameters
                                                                </div>
                                                                <div class="modalPopUpDetColumn2">
                                                                    :
                                                                </div>
                                                                <div class="modalPopUpDetColumn3">
                                                                    <asp:Label ID="lblWsCmd_Outputpara" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="modalPopUpDetRow">
                                                                <div class="modalPopUpDetColumn1">
                                                                    Created By
                                                                </div>
                                                                <div class="modalPopUpDetColumn2">
                                                                    :
                                                                </div>
                                                                <div class="modalPopUpDetColumn3">
                                                                    <asp:Label ID="lblcreatedby" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="modalPopUpDetRow">
                                                                <div class="modalPopUpDetColumn1">
                                                                    Updated By
                                                                </div>
                                                                <div class="modalPopUpDetColumn2">
                                                                    :
                                                                </div>
                                                                <div class="modalPopUpDetColumn3">
                                                                    <asp:Label ID="lblupdatedby" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="modalPopUpDetRow">
                                                                     Apps using this object :
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                                <div class="CmdAppRoot" style="overflow: hidden;">
                                                                    <div id="DbCmdAppDiv" style="overflow: hidden;">
                                                                        This object is not used any where.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="AddDiv" class="DbCmdRow">
                                                    <div id="EditDbCommandDiv" style="padding-top: 10px">
                                                        <div id="EditDbcmdlblDiv" class="modalPopUpDetHeaderDiv" visible="false" runat="server">
                                                            <div class="modalPopUpDetHeader">
                                                                <asp:Label ID="lblEditDbCmd_Name" runat="server" Text=""></asp:Label>
                                                                <asp:HiddenField ID="hdfEditWsType" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <asp:HiddenField ID="hidwsselected" runat="server" />
                                                        <asp:HiddenField ID="hidwsdataset" runat="server" />
                                                        <div id="EditWsCmdDetailsDiv" runat="server">
                                                            <div id="EditDbCommandDivtest" class="hide">
                                                                <div id="EditWsCmdDetailHeadDiv" class="modalPopUpDetHeaderDiv" visible="false" runat="server">
                                                                    <div class="modalPopUpDetHeader">
                                                                        <asp:Label ID="lblWsCmdEdit_Name" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                    <div style="float: right;">
                                                                        <div style="float: left;">
                                                                            <asp:LinkButton ID="lnkWsCmd_Back" runat="server" Text="Back" OnClientClick="isCookieCleanUpRequired('false');WsConnectorBackClick();"></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div id="wsheaderconnector" class="DbCmdRow">
                                                                <div class="Headerfontsize" id="adddivheader">
                                                                    Add WebService Object
                                                                </div>
                                                                <div class="Headerfontsize hide" id="DiveditDBobject">
                                                                    Edit WebService Object
                                                                </div>
                                                            </div>
                                                            <div class="DbConnRightDiv">
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="DbCmdDiv">
                                                                <div id="Div3" class="modalPopUpDetHeader" runat="server">
                                                                    <div class="modalPopUpDetHeader">
                                                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                                <div id="Div" runat="server" class="DbCmdRow">
                                                                    <div class="DbConnLeftDiv selects">
                                                                        Source
                                                                    </div>
                                                                    <div class="DbConnRightDiv SelectDiv">
                                                                        <asp:DropDownList ID="ddl_WsConnactor" runat="server" CssClass="UseUniformCss" OnSelectedIndexChanged="ddl_WsConnactor_SelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                    <div id="Div2">
                                                                        <div id="WsdltxtCmdDiv" class="DbCmdRow">
                                                                            <div class="DbConnLeftDiv">
                                                                                Object Name
                                                                            </div>
                                                                            <div class="DbConnRightDiv">
                                                                                <asp:TextBox ID="txtWsdl_CmdName" runat="server" class="InputStyle inpst" Width="80%"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdfWsCommand_CommandId" runat="server" />
                                                                                <asp:HiddenField ID="hidparameter" runat="server" />
                                                                                <asp:HiddenField ID="hidservicetype" runat="server" />
                                                                                <asp:HiddenField ID="hidwsconnectorid" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="editWsdlCmdDiv" runat="server">
                                                                            <div class="DbConnLeftDiv">
                                                                                Description
                                                                            </div>
                                                                            <div class="DbConnRightDiv">
                                                                                <asp:TextBox ID="txtWsdlCmdDesc" runat="server" TextMode="MultiLine" Width="80%"
                                                                                    class="CmdDescInputStyle inpst"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                    <div id="WsdlCmdDiv" class="hide">
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="ddlWsConnServicesDiv" class="DbCmdRow">
                                                                            <div class="DbConnLeftDiv selects">
                                                                                Service Name
                                                                            </div>
                                                                            <div class="DbConnRightDiv SelectDiv">
                                                                                <asp:DropDownList ID="ddlWsConnServices" runat="server" class="UseUniformCss" AutoPostBack="true"
                                                                                    OnSelectedIndexChanged="ddlWsConnServices_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="-1">Select Services</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="ddlWsConnMethodDiv" class="DbCmdRow">
                                                                            <div class="DbConnLeftDiv">
                                                                                Methods
                                                                            </div>
                                                                            <div class="DbConnRightDiv SelectDiv">
                                                                                <asp:DropDownList ID="ddl_WsConnectorMethod" runat="server" class="UseUniformCss"
                                                                                    onchange="WsdlMethodChange(this);">
                                                                                    <asp:ListItem Value="-1">Select Web service Method</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="WsdlOutParamDiv" class="FLeft" style="width: 100%;">
                                                                            <div class="clear">
                                                                            </div>
                                                                            <div class="solidLine">
                                                                            </div>
                                                                            <div class="DbConnLeftDiv" style="padding-top: 10px;">
                                                                                Output Type
                                                                            </div>
                                                                            <div class="DbConnRightDiv SelectDiv">
                                                                                <asp:DropDownList ID="ddlWsdlCommad_DataReturned" runat="server" class="UseUniformCss"
                                                                                    onchange="SetWsDatasetPathChange(this);">
                                                                                    <asp:ListItem Value="0">Single Record</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Multiple Records</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="clear">
                                                                            </div>
                                                                            <div id="DivWsdlCmd_DsPathHead" class="DbConnLeftDiv hide">
                                                                                Dataset Path(Plural)
                                                                            </div>
                                                                            <div id="DivWsdlCmd_DsPathCtrl" class="DbConnRightDiv hide">
                                                                                <asp:TextBox ID="txtWsdlCommand_DatasetPath" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                                                                            </div>
                                                                            <div class="clear">
                                                                            </div>
                                                                            <div class="DbConnLeftDiv">
                                                                                Output Properties
                                                                            </div>
                                                                            <div class="DbConnRightDiv">
                                                                                <input id="btnWsdlCmdOutput" type="button" value="  Edit Properties  " class="InputStyle"
                                                                                    onclick="EditWsCmdPara(true);" />
                                                                                <asp:HiddenField ID="hdfWsdlOutParamPfix" runat="server" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                    </div>
                                                                    <div id="SimpleHttpCmdPropDiv" class="hide">
                                                                        <div id="HttptxtCmdDiv" class="DbCmdRow">
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="wsCmdDesc" runat="server" class="DbCmdRow">
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv">
                                                                            Http Method
                                                                        </div>
                                                                        <div class="DbConnRightDiv">
                                                                            <asp:RadioButton ID="rdbWsHttpGet" runat="server" Text="  Get  " GroupName="httpType"
                                                                                Checked="true"></asp:RadioButton>
                                                                            <asp:RadioButton ID="rdbWsHttpPost" GroupName="httpType" runat="server" Text="  Post  ">
                                                                            </asp:RadioButton>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="solidLine">
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv">
                                                                            Host Name
                                                                        </div>
                                                                        <div class="DbConnRightDiv" style="word-wrap: break-word; padding-top: 4px;">
                                                                            <asp:Label ID="lblUrl" runat="server" Text=""></asp:Label>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv">
                                                                            Url Template
                                                                        </div>
                                                                        <div class="DbConnRightDiv" style="word-wrap: break-word;">
                                                                            <asp:TextBox ID="txtWsCommand_Url" runat="server" Rows="4" class="InputStyle" Width="80%"></asp:TextBox>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv">
                                                                            Data Template
                                                                        </div>
                                                                        <div class="DbConnRightDiv" style="word-wrap: break-word;">
                                                                            <asp:TextBox ID="txthttpdatatemplate" runat="server" TextMode="MultiLine" Rows="4"
                                                                                class="InputStyle" Width="80%"></asp:TextBox>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv selects">
                                                                            Content Type
                                                                        </div>
                                                                        <div class="DbConnRightDiv SelectDiv">
                                                                            <asp:DropDownList ID="ddlContantType" runat="server" CssClass="UseUniformCss" >
                                                                            <asp:ListItem Value="0"  >multipart-form-data</asp:ListItem>
                                                                            <asp:ListItem Value="1" Selected="True" >application/x-www-form-urlencoded</asp:ListItem>
                                                                            <asp:ListItem Value="2" >application/json</asp:ListItem>
                                                                            <asp:ListItem Value="3" >application/xml</asp:ListItem> 
                                                                            <asp:ListItem Value="4" >application/base64</asp:ListItem>
                                                                            <asp:ListItem Value="5" >application/octet-stream</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="solidLine">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv" style="padding-top: 10px;">
                                                                            Output Type
                                                                        </div>
                                                                        <div class="DbConnRightDiv SelectDiv ">
                                                                            <asp:DropDownList ID="ddlWsCommad_DataReturned" runat="server" class="UseUniformCss"
                                                                                onchange="SetWsDatasetPathChange(this);">
                                                                                <asp:ListItem Value="0">Single Record</asp:ListItem>
                                                                                <asp:ListItem Value="1">Multiple Records</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="DivWsCmd_DsPathHead" class="DbConnLeftDiv hide">
                                                                            <asp:Label ID="Label32" runat="server" Text="Dataset Path(Plural)"></asp:Label>
                                                                        </div>
                                                                        <div id="DivWsCmd_DsPathCtrl" class="DbConnRightDiv hide">
                                                                            <asp:TextBox ID="txtWsCommand_DatasetPath" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="hide">
                                                                            <asp:Label ID="Label40" runat="server" Text="Parameters "></asp:Label>
                                                                        </div>
                                                                        <div class="hide">
                                                                            <asp:TextBox ID="txtWsCommand_Para" runat="server" class="InputStyle" Width="80%"
                                                                                ReadOnly="true"></asp:TextBox>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="DbConnLeftDiv">
                                                                            Output Properties
                                                                        </div>
                                                                        <div class="DbConnRightDiv">
                                                                            <input id="btnWsHttpCmdOutput" type="button" value="  Edit Properties  " class="InputStyle"
                                                                                onclick="EditWsCmdPara();" />
                                                                        </div>
                                                                    </div>
                                                                    <div id="RpcCmdPropDiv" class="hide">
                                                                        <div id="RpcTxtCmdDiv" class="DbCmdRow">
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div id="editRPCCmdDiv" runat="server" class="DbCmdRow">
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                    <div id="RpcMethodDiv" class="DbCmdRow hide">
                                                                        <div class="DbConnLeftDiv">
                                                                            Method Name
                                                                        </div>
                                                                        <div class="DbConnRightDiv">
                                                                            <asp:TextBox ID="txtRpc_MethodName" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                    <div class="rpcOuterCont hide" id="rpcout">
                                                                        <div class="rpcInputPara">
                                                                            <div class="FLeft rpcParaHeader">
                                                                                <div class="FLeft">
                                                                                    <img id="imgRpcInPara" alt="Collapse" src="//enterprise.mficient.com/images/collapse.png"
                                                                                        onclick="rpcParaCollapse(this);" style="cursor: pointer;" /></div>
                                                                                <div class="FLeft rpcHeaderText">
                                                                                    Input Parameters</div>
                                                                            </div>
                                                                            <div class="clear">
                                                                            </div>
                                                                            <div id="DivRpcInPara" class="rpcParaInner">
                                                                                <div id="rpcInParaHeadDiv">
                                                                                    <div class="rpcHeader">
                                                                                        <div class="rpcTextHeader">
                                                                                            <div class="rpcHeaderText FLeft" style="width: 35%;">
                                                                                                Parameter</div>
                                                                                            <div style="width: 60%; cursor: pointer;" class="FLeft" onclick="AddRpcInPara();">
                                                                                                ( Click to add Parameters )</div>
                                                                                        </div>
                                                                                        <div class="rpcTypeHeader">
                                                                                            <div class="rpcHeaderText">
                                                                                                Type</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clear">
                                                                                </div>
                                                                                <div id="rpcInParaContDiv" class="rpcParaCont">
                                                                                    <div class="rpcContRow">
                                                                                        <div id="rpcInParaContDiv1" class="rpcParaContLeft">
                                                                                        </div>
                                                                                        <div id="rpcInParaContDiv2" class="rpcParaContRight">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <div class="rpcOutputPara">
                                                                            <div class="FLeft rpcParaHeader">
                                                                                <div class="FLeft">
                                                                                    <img id="imgRpcOutPara" alt="Collapse" src="//enterprise.mficient.com/images/collapse.png"
                                                                                        onclick="rpcParaCollapse(this);" style="cursor: pointer;" /></div>
                                                                                <div class="FLeft rpcHeaderText">
                                                                                    Output Parameters</div>
                                                                            </div>
                                                                            <div class="clear">
                                                                            </div>
                                                                            <div id="DivRpcOutPara" class="rpcParaInner">
                                                                                <div id="rpcOutParaHeadDiv">
                                                                                    <div class="rpcHeader">
                                                                                        <div class="rpcTextHeader">
                                                                                            <div class="rpcHeaderText FLeft" style="width: 35%;">
                                                                                                Parameter</div>
                                                                                            <div style="width: 60%; cursor: pointer;" class="FLeft" onclick="AddRpcOutPara();">
                                                                                                ( Click to add Parameters )</div>
                                                                                        </div>
                                                                                        <div class="rpcTypeHeader">
                                                                                            <div class="rpcHeaderText">
                                                                                                Type</div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="clear">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clear">
                                                                                </div>
                                                                                <div id="rpcOutParaContDiv" class="rpcParaCont">
                                                                                    <div class="rpcContRow">
                                                                                        <div id="rpcOutParaContDiv1" class="rpcParaContLeft">
                                                                                        </div>
                                                                                        <div id="rpcOutParaContDiv2" class="rpcParaContRight">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clear">
                                                                </div>
                                                                <asp:Panel runat="server" ID="pnlCache">
                                                                    <div id="Catchmgrtime">
                                                                        <div class="DbConnLeftDiv selects">
                                                                            Data Cache</div>
                                                                        <div class="DbConnRightDiv SelectDiv">
                                                                            <asp:DropDownList ID="dd1DataCatch" runat="server" onchange="Hide(this)" class="UseUniformCss">
                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                <asp:ListItem Value="1">Yes (Absolute Time)</asp:ListItem>
                                                                                <asp:ListItem Value="2">Yes (Relative Time)</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                    <div id="CatchEvery" class="hidediv">
                                                                        <div class="DbConnLeftDiv selects">
                                                                            Frequency Every</div>
                                                                        <div class="DbConnRightDiv SelectDiv">
                                                                            <asp:DropDownList ID="dd1AbMonth" runat="server" onchange=" Abmonth(this)" EnableViewState="true"
                                                                                class="UseUniformCss">
                                                                                <asp:ListItem Value="0">Hour</asp:ListItem>
                                                                                <asp:ListItem Value="1">Day</asp:ListItem>
                                                                                <asp:ListItem Value="2">Week</asp:ListItem>
                                                                                <asp:ListItem Value="3">Month</asp:ListItem>
                                                                                <asp:ListItem Value="4">Year</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <div id="Catcmin" class="hidediv" style="float: left; width: inherit !important">
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    At Min</div>
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="dd1AbHr" runat="server" class="UseUniformCss">
                                                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                                                        <asp:ListItem Value="10">10</asp:ListItem>
                                                                                        <asp:ListItem Value="15">15</asp:ListItem>
                                                                                        <asp:ListItem Value="30">30</asp:ListItem>
                                                                                        <asp:ListItem Value="45">45</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="AbEvDayDiv" class="hidediv" style="float: left; width: inherit !important">
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    At</div>
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="dd1Abhh" runat="server" class="UseUniformCss">
                                                                                    </asp:DropDownList>
                                                                                    <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                        Hr. :</div>
                                                                                    <asp:DropDownList ID="ddlAbMM" runat="server" class="UseUniformCss">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    Min.</div>
                                                                            </div>
                                                                            <div id="AbEvWkDiv" class="hidediv" style="float: left; width: inherit !important">
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    On
                                                                                </div>
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="ddlAbDay" runat="server" class="UseUniformCss">
                                                                                        <asp:ListItem Value="1">MON</asp:ListItem>
                                                                                        <asp:ListItem Value="2">TUE</asp:ListItem>
                                                                                        <asp:ListItem Value="3">WED</asp:ListItem>
                                                                                        <asp:ListItem Value="4">THU</asp:ListItem>
                                                                                        <asp:ListItem Value="5">FRI</asp:ListItem>
                                                                                        <asp:ListItem Value="6">SAT</asp:ListItem>
                                                                                        <asp:ListItem Value="7">SUN</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="AbEvMnDiv" class="hidediv" style="float: left; width: inherit !important">
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    On Date</div>
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="dd1AbMnDay" runat="server" class="UseUniformCss">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="AbEvYrDiv" class="hidediv" style="float: left; width: inherit !important">
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    On</div>
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="dd1AbMnthDay" runat="server" class="UseUniformCss">
                                                                                    </asp:DropDownList>
                                                                                    <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                        Day /</div>
                                                                                    <asp:DropDownList ID="ddlAbMnth" class="UseUniformCss" runat="server" Style="width: 90px;
                                                                                        margin-left: 5px;">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div style="float: left; margin-top: 10px; margin-right: 10px;">
                                                                                    Month</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="RltDiv" class="hidediv">
                                                                        <div id="divcatch" class="DbConnLeftDiv selects">
                                                                            Frequency Every</div>
                                                                        <div class="DbConnRightDiv SelectDiv">
                                                                            <div id="RltDivmin" class="hidediv">
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="drprltmin" class="UseUniformCss" runat="server" Style="width: 65px">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="RltHour" class="hidediv">
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="Drprlthour" class="UseUniformCss" runat="server" Style="width: 65px">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="RltDay" class="hidediv">
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="rltdayd" class="UseUniformCss" runat="server" Style="width: 65px">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="RltWeek" class="hidediv">
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="raltweeks" class="UseUniformCss" runat="server" Style="width: 65px">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="RltMonth" class="hidediv">
                                                                                <div class="SelectDiv" style="float: left;">
                                                                                    <asp:DropDownList ID="drprltmonth" class="UseUniformCss" runat="server" Style="width: 65px">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <asp:DropDownList ID="ddlRlt" class="UseUniformCss" runat="server" onchange="Relative(this)"
                                                                                Style="width: 65px">
                                                                                <asp:ListItem Value="0">Minute</asp:ListItem>
                                                                                <asp:ListItem Value="1">Hour</asp:ListItem>
                                                                                <asp:ListItem Value="2">Day</asp:ListItem>
                                                                                <asp:ListItem Value="3">Week</asp:ListItem>
                                                                                <asp:ListItem Value="4">Month</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:HiddenField ID="hidcatchexfrequency" runat="server" />
                                                                <asp:HiddenField ID="hidcatchexcondition" runat="server" />
                                                                <asp:HiddenField ID="hidSelectcatch" runat="server" />
                                                                <div class="clear">
                                                                </div>
                                                                <div class="solidLine">
                                                                </div>
                                                                <div id="btnWsCmdSaveDiv" style="vertical-align: bottom;">
                                                                    <asp:UpdatePanel runat="server" ID="updWsCommandSave" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <div style="margin-top: 10px; margin-left: 45%;">
                                                                                <asp:Button ID="btnWsCommand_Save" runat="server" Text="   Save   " OnClientClick="InCaseOfRPC();return wsobjectvalidation();"
                                                                                    OnClick="btnWsCommand_Save_Click" class="InputStyle" />
                                                                                <asp:Button ID="btnreset" runat="server" Text="   Cancel   " OnClick="btnWsCommand_Reset_Click"
                                                                                    class="InputStyle" />
                                                                                <asp:Button ID="btncancel" runat="server" Text="   Cancel   " Visible="false" OnClick="btnWsCommand_Cancel_Click"
                                                                                    class="InputStyle" />
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                                <div>
                                                                    <asp:HiddenField ID="hdfWsContent" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div>
                    </div>
                    <div id="SubProcImageDelConfirnmation">
                        <div>
                            <div>
                                <asp:HiddenField ID="hdfDelId" runat="server" />
                            </div>
                            <div>
                                <asp:HiddenField ID="hdfDelType" runat="server" />
                            </div>
                            <div class="MarginT10">
                            </div>
                            <div class="MessageDiv">
                                <a id="aDelCfmMsg"></a>
                            </div>
                            <div class="MarginT10">
                            </div>
                            <div class="SubProcborderDiv">
                                <div class="SubProcBtnMrgn" align="center">
                                    <asp:UpdatePanel runat="server" ID="UpdDeleteConfirm" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnDeleteCfm" runat="server" Text="  Yes  " OnClientClick="SubProcImageDelConfirnmation(false,'');"
                                                CssClass="InputStyle" />
                                            <input id="Button13" type="button" value="  No  " onclick="SubProcImageDelConfirnmation(false,'');"
                                                class="InputStyle" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SubWsIntls" class="hide">
                    <div id="SubWsInnrIntls" style="max-height: 200px; overflow-y: auto;">
                    </div>
                </div>
                <div id="SubProcDeleteCmdConfrmation">
                    <div>
                        <div>
                            <asp:UpdatePanel runat="server" ID="UpdCmdDelCnf" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:HiddenField ID="hdfCmdDelId" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="MarginT10">
                        </div>
                        <div class="MessageDiv">
                            <a id="aMsgCmdDelCmf"></a>
                        </div>
                        <div class="MarginT10">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:UpdatePanel runat="server" ID="UpdDeleteCommand" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="Button23" runat="server" Text="  Yes  " OnClientClick="SubProcDeleteCmdConfrmation(false,'');"
                                            OnClick="btnCmdDelYes_Click" CssClass="InputStyle" />
                                        <input type="button" value="  No  " onclick="SubProcDeleteCmdConfrmation(false,'');"
                                            class="InputStyle" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SubProcBoxMessage">
                    <div>
                        <div class="MessageDiv">
                            <a id="a2">Please check following points : </a>
                            <br />
                        </div>
                        <div style="margin-top: 5px">
                            <a id="aMessage"></a>
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnDiv" align="center">
                                <input id="btnOkErrorMsg" type="button" value="  OK  " onclick="SubProcBoxMessage(false);"
                                    class="InputStyle" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SubProcWsCmdOutputPara" class="hide">
                    <div>
                        <div id="Div40" runat="server" class="ProcCanvasAddDpOption" visible="true">
                            <div class="AddDpOptionInner">
                                <div class="DpHeader">
                                    <div class="DpTextHeader">
                                        <div class="DpHeaderText">
                                            Name
                                        </div>
                                    </div>
                                    <div class="DpValueHeader">
                                        <div class="DpHeaderText">
                                            Path</div>
                                    </div>
                                    <div class="DpOptionHeaderDelete">
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="WsParaContent" class="DpContent">
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnDiv">
                                <input id="Button84" type="button" value="  Save  " onclick="SaveWsPara();" class="InputStyle" />
                                <input id="Button86" type="button" value="  Cancel  " onclick="SubProcWsCmdOutputPara(false);"
                                    class="InputStyle" />
                                <asp:HiddenField ID="hdfWsCmdPara" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SubProcConfirmBoxMessage">
                    <div>
                        <div>
                            <div class="ConfirmBoxMessage1">
                                <a id="aCfmMessage"></a>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <input id="btnCnfFormSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                                    class="InputStyle" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ShowDelete" class="dataTables_wrapper no-footer" style="display: none">
                    <asp:UpdatePanel ID="UPDEL" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="Div4">
                                <p>
                                    This data object is using at following places</p>
                            </div>
                            <div id="divoverflow" style="max-height: 150px; overflow-y: auto !important">
                                <asp:Repeater ID="RepDelete" runat="server">
                                    <HeaderTemplate>
                                        <table class="display dataTable no-footer">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        App
                                                    </th>
                                                    <th>
                                                        Views
                                                    </th>
                                                    <th>
                                                        Forms
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="odd">
                                                <td>
                                                    <asp:Literal ID="litApp" runat="server" Text='<%# Eval("app") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litOpertion" runat="server" Text='<%# Eval("inuse") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litform" runat="server" Text='<%# Eval("forms") %>' />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="even">
                                                <td>
                                                    <asp:Literal ID="litApp" runat="server" Text='<%# Eval("app") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litOpertion" runat="server" Text='<%# Eval("inuse") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litform" runat="server" Text='<%# Eval("forms") %>' />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </div>
                <div id="SubProcAddRpcPara" class="hide">
                    <div>
                        <div class="DbConnLeftDiv">
                            Parameter Name :
                        </div>
                        <div class="DbConnRightDiv" style="padding-left: 4px">
                            <asp:TextBox ID="txtRpc_ParaName" runat="server" CssClass="InputStyle" Width="44%"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="DbConnLeftDiv">
                            Parameter Type :
                        </div>
                        <div class="DbConnRightDiv">
                            <select id="ddlRpc_ParaType" class="UseUniformCss">
                                <option value="-1">Select Parameter Type</option>
                                <option value="array">array</option>
                                <option value="base64">base64</option>
                                <option value="boolean">boolean</option>
                                <option value="date/time">date/time</option>
                                <option value="double">double</option>
                                <option value="i4">i4</option>
                                <option value="integer">integer</option>
                                <option value="string">string</option>
                                <option value="struct">struct</option>
                            </select>
                        </div>
                        <div class="clear">
                        </div>
                        <div>
                            <input id="hdfRpcParamLevel" type="hidden" />
                            <input id="hdfRpcParamParent" type="hidden" />
                            <input id="hdfIsInnerParam" type="hidden" />
                            <asp:HiddenField ID="hdfOutRpcParam" runat="server" />
                            <asp:HiddenField ID="hdfRpcParam" runat="server" />
                        </div>
                        <div class="FLeft" style="width: 10%; margin-left: 40%; margin-top: 20px;">
                            <input id="btnRpc_AddPara" type="button" value="  Add  " class="InputStyle" onclick="BtnRpcAddParaClick();" />
                        </div>
                    </div>
                </div>
                <div id="SubProcDeleteRpcPram" class="hide">
                    <div class="ProcBoxMessage">
                        <div style="margin-top: 10px">
                        </div>
                        <div class="MessageDiv">
                            <a class="DefaultCursor">Do you want to delete this parameter ?</a>
                        </div>
                        <div style="margin-top: 10px">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnDiv" align="center">
                                <input id="Button6" type="button" value="  Yes  " onclick="DeleteRpcParamYes();"
                                    class="InputStyle" />
                                <input id="Button7" type="button" value="  No  " onclick="SubProcDeleteRpcPram(false);"
                                    class="InputStyle" />
                            </div>
                            <input id="hdfParamDelIds" type="hidden" />
                        </div>
                    </div>
                </div>
                <div id="divWaitBox" class="waitModal">
                    <div id="WaitAnim">
                        <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                            AlternateText="Please Wait" BorderWidth="0px" />
                    </div>
                </div>
                <div id="divwscopyobject" class="hide" style="height: 100px">
                    <div id="Div1">
                        <div id="divcopysameobject" class="DbCmdRow">
                            <div class="DbConnLeftDiv">
                                Object Name
                            </div>
                            <div class="DbConnRightDiv">
                                <asp:TextBox ID="txtwscopyobject" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="Button1" runat="server" Text="  Yes  " OnClientClick="return userwsValid();"
                                            OnClick="WSCommandCopy_Save_Click" CssClass="InputStyle" />
                                        <input type="button" value="  No  " onclick="openwscopyobject(false);" class="InputStyle" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div id="SubProobjectChange" class="hide">
                    <asp:UpdatePanel ID="updatediscard" runat="server">
                        <ContentTemplate>
                            <div class="MessageDiv">
                                <a id="aFormChangeMsg" class="DefaultCursor">Discard any changes made? </a>
                            </div>
                            <div class="SubProcborderDiv" style="margin-top: 30px !important;">
                                <div class="SubProcBtnMrgn" align="center">
                                    <asp:Button ID="btnfrmDiscard" runat="server" Text="  Yes  " OnClientClick="$('#SubProobjectChange').dialog('close');"
                                        OnClick="lnkdiscardyes_Click" class="InputStyle" />
                                    <asp:Button ID="btnsaveCurrentobject" runat="server" class="InputStyle" Text="  No  "
                                        OnClick="btnSaveDb_Click" />
                                    <input id="btnCancelSaveCurrentForm" type="button" value="  Cancel " onclick="$('#SubProobjectChange').dialog('close');"
                                        class="InputStyle" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="dialogtestPara">
                    <div style="margin-bottom: 15px !important" id="divdetails">
                    </div>
                </div>
                <div id="dialogdynamictable">
                    <div style="margin-bottom: 15px !important">
                        <div id="Div7" class="AddParameter">
                            <table id="resulttbl">
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div id="divCredentialBymobile" class="hide">
                    <asp:UpdatePanel ID="updatecredentialwithoutsave" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Literal ID="Literal1" Visible="false" runat="server"></asp:Literal>
                            <div class="DbConnLeftDiv " id="div5">
                                Username
                            </div>
                            <div class="DbConnRightDiv " style="padding-left: 12px" id="div6">
                                <asp:TextBox ID="txtUsername" runat="server" Width="90%" class="InputStyle" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="DbConnLeftDiv" id="div8">
                                Password
                            </div>
                            <div class="DbConnRightDiv " style="padding-left: 12px" id="div9">
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="90%" class="InputStyle"
                                    MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="SubProcborderDiv">
                                <div class="SubProcBtnMrgn" align="center">
                                    <asp:Button ID="btnSaveCredential" runat="server" Text="  Next  " OnClientClick="return Credentialvalidation();"
                                        OnClick="btnSaveCredential_Click" class="InputStyle" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:HiddenField ID="hiddiscardsuccess" runat="server" />
                <asp:HiddenField ID="hiddiscardcommadtype" runat="server" />
                <asp:HiddenField ID="hdfPostBackPageMode" runat="server" />
                <asp:HiddenField ID="hidWsAuthencationMeta" runat="server" />
                <asp:HiddenField ID="ltCrd" runat="server" />
            </asp:Panel>
        </section>
        <script type="text/javascript">
            Sys.Application.add_init(application_init);
            function application_init() {

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_initializeRequest(prm_initializeRequest);
                prm.add_endRequest(prm_endRequest);
            }
            function prm_initializeRequest() {

                showWaitModal();
                isCookieCleanUpRequired('false');
            }
            function prm_endRequest() {
                CallUniformCssForWsCmd();
                hideWaitModal();
                isCookieCleanUpRequired('true');

            }
        </script>
    </div>
</asp:Content>
