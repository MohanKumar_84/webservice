﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Net;

namespace mFicientCP
{
    public partial class IdeWsObject : System.Web.UI.Page
    {
        string strhfs, strhfbid, SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "", strPageMode = "", strPostbackPageMode = "";
        private WSDL _WSDL;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {

                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                strPostbackPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value;
            }
            else
            {
                strPageMode = ((HiddenField)Page.PreviousPage.Master.FindControl("hidAppNewOrEdit")).Value;
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;
                hdfPostBackPageMode.Value = ((HiddenField)previousPage.Master.FindControl("hidAppNewOrEdit")).Value;
                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid))
            {
                if (String.IsNullOrEmpty(strhfs))
                {
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect(@"Default.aspx");
                    return;
                }
                return;
            }

            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            Utilities.SaveInContext(context, strhfs, strhfbid, out SubAdminid, out Sessionid, out CompanyId, out Adminid);
            
            WscmdObjectTable();
            if (!IsPostBack)
            {
                CacheDropdownBind();
                BindWsConnactorDropdown(ddl_WsConnactor);
                bindCreadential();
            }
            if (!IsPostBack)
                strPostbackPageMode = hdfPostBackPageMode.Value;
            else
                hdfPostBackPageMode.Value = strPostbackPageMode;

            if (hdfPostBackPageMode.Value == "wsobject")
            {
                ClearAfterAdd();
            }
        }

        #region DatatableJsonCreated
        private void WscmdObjectTable()
        {
            try
            {
                GetWsCommand objGetWsCommand = new GetWsCommand(true, false, this.SubAdminid, "", "", this.CompanyId, "1");
                objGetWsCommand.Process(); 
                UpdateObjectXreference objref = new UpdateObjectXreference();                
                DataTable dtApps = objref.GetAppsListForObjectReff(this.CompanyId);                 
                ArrayList rows = new ArrayList();
                DataTable dt = new DataTable();
                if (objGetWsCommand.StatusCode == 0)
                {
                    string InParaHtml = "";
                    string OutParaHtml = "";
                    string strParaDetails1 = "";
                    dt = objGetWsCommand.ResultTable;
                    dt.Columns.Add("DateTime", typeof(string));
                    dt.Columns.Add("RPCIN", typeof(string));
                    dt.Columns.Add("RPCOUT", typeof(string));
                    dt.Columns.Add("Used", typeof(string));
                    foreach (DataRow row in dt.Rows)
                    {
                        row["WS_COMMAND_NAME"] = Convert.ToString(row["WS_COMMAND_NAME"]);
                        row["CONNECTION_NAME"] = Convert.ToString(row["CONNECTION_NAME"]);
                        row["DateTime"] = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(row["UPDATED_ON"]));
                        row["Used"] = Utilities.SerializeJson<List<List<string>>>(Utilities.objectUsage(dtApps.Select("Command_ids like '%WS_" + row["WS_COMMAND_id"] + "%'"), row["WS_COMMAND_id"].ToString(), "WS_"));
                    
                        string strnametype = (string)(row["WEBSERVICE_TYPE"]);
                        string strn = strnametype.Trim();
                        if (strn == "rpc")
                        {
                            bool stre = Convert.ToString(row["PARAMETER"]) == "";
                            if (stre == true)
                            {
                                row["RPCIN"] = "";
                            }
                            else
                            {
                                InParaHtml = ParseRpcParaJson(Convert.ToString(row["PARAMETER"]), "In", out strParaDetails1);
                                row["RPCIN"] = strParaDetails1;

                            }
                            bool stre1 = Convert.ToString(row["TAG"]) == "";
                            if (stre1 == true)
                            {
                                row["RPCOUT"] = "";
                            }
                            else
                            {
                                strParaDetails1 = "";
                                OutParaHtml = ParseRpcParaJson(Convert.ToString(row["TAG"]), "Out", out strParaDetails1);
                                row["RPCOUT"] = strParaDetails1;
                            }
                        }
                        else
                        {
                        }
                    }
                    DataTable dtSelectedColumns = dt.DefaultView.ToTable(false, "WS_COMMAND_NAME", "CONNECTION_NAME");
                    foreach (DataRow dataRow in dtSelectedColumns.Rows)
                    {
                        rows.Add(dataRow.ItemArray);
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(upwsobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + MficientConstants.ERROR_DATABASE_CONNECTION + "');", true);
                hidwsselected.Value = Utilities.SerializeJson<ArrayList>(rows);
                hidwsdataset.Value = Utilities.ConvertdatetabletoString(dt);
                ScriptManager.RegisterStartupScript(upwsobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"OnloadWSObjectData();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(upwsobject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }
        }
        #endregion

        #region BindWebservice connector
        private void BindWsConnactorDropdown(DropDownList _ddl)
        {
            GetWebserviceConnection objGetWebserviceConnection = new GetWebserviceConnection(true, this.SubAdminid, "", "", this.CompanyId);
            objGetWebserviceConnection.Process1();
            if (objGetWebserviceConnection.StatusCode == 0)
            {
                _ddl.DataSource = objGetWebserviceConnection.ResultTable;
                _ddl.DataTextField = "CONNECTION_NAME";
                _ddl.DataValueField = "WS_CONNECTOR_ID";
                _ddl.DataBind();
                _ddl.Items.Insert(0, new ListItem("Select web service Connector", "-1"));
            }
            else
            {
                _ddl.Items.Insert(0, new ListItem("Select Database Connector", "-1"));
            }
        }

        #endregion

        #region Delete Commands
        protected void lnkWsCmdDelete_Click(object sender, EventArgs e)
        {
            DeleteWsCommand(this.CompanyId, this.SubAdminid, updDelWsCmd);
        }
        protected void DeleteWsCommand(string _CompanyId, string _SubAdminId, UpdatePanel _Upd)
        {
            hdfCmdDelId.Value = "WS" + "_" + hidwsconname.Value;
            UpdateObjectXreference obj = new UpdateObjectXreference();
            DataSet ds = obj.GetObjectXreference(this.CompanyId, hdfWsCommand_CommandId.Value, 2);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Web-service object already used in apps .<br>" + "');", true);
            else
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMsgCmdDelCmf').html('" +  "Do you want to delete this web object ?" + "');SubProcDeleteCmdConfrmation(true,'Delete Object');", true);
        }
        protected void btnCmdDelYes_Click(object sender, EventArgs e)
        {
            DeleteDbWsCommand(this.SubAdminid, this.CompanyId);
            WscmdObjectTable();
            updatedbright.Update();
            wsobjectClear();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"OnloadWSObjectData();", true);

        }
        protected void DeleteDbWsCommand(string _SubAdminId, string _CompanyId)
        {

            DeleteWsCommand objDeleteWsCommand = new DeleteWsCommand(hdfWsCommand_CommandId.Value, _SubAdminId, _CompanyId);
            objDeleteWsCommand.Process();
            if (objDeleteWsCommand.StatusCode != 0)
            {
                ScriptManager.RegisterStartupScript(UpdDeleteCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Internal server error.<br>Web service object cannot be deleted.');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdDeleteCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object deleted successfully.');SubProcConfirmBoxMessageWo(true,'Deleted',350);SubProcDeleteCmdConfrmation(false,'');", true);
            }
        }
        protected void DeleteWsConnector(string _SubAdminId, string _ConnectionId, UpdatePanel _Upd, string _CompanyId)
        {
            GetWsCommand objCmd = new GetWsCommand(true, false, _SubAdminId, "", "", this.CompanyId);
            objCmd.Process();
            if (objCmd.StatusCode == 0)
            {
                string str = "WS_CONNECTOR_ID='" + _ConnectionId + "'";
                DataRow[] dr = objCmd.ResultTable.Select(str);
                if (dr != null)
                {
                    if (dr.Length > 0)
                    {

                        return;
                    }
                    else { }
                }
                else { }
            }
            else { }
            DeleteConnection objDeleteConnection = new DeleteConnection(_SubAdminId, _ConnectionId, false, _CompanyId);
            objDeleteConnection.Process();
            if (objDeleteConnection.StatusCode == 0)
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('Web service connector deleted successfully.');SubProcConfirmBoxMessage(true,'  Deleted',250);AddNewWebSrcConnector(false);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ShowWsConnDivOnEdit();CallUniformCssForWsCtr();$('#aMessage').html('Data cannot be deleted.Internal server error.');SubProcBoxMessage(true);", true);
            }
        }
        public class UseDetailsObjects
        {
            public string app { get; set; }
            public string inuse { get; set; }
            public List<string> forms { get; set; }
        }
        #endregion

        #region Div/Hide Show basedon Service selection
        protected void ddl_WsConnactor_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strScript = "";
            lblUrl.Text = "";
            txtWsCommand_Url.Text = "";
            if (ddl_WsConnactor.SelectedValue == "-1")
            {
                updAddWsCommand.Update();
                ScriptManager.RegisterStartupScript(updAddWsCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), (sender == null ? "" : "WsCommandEditClick();") + "", true);
                return;
            }
            GetWebserviceConnection objGetWebserviceConnection = new GetWebserviceConnection(false, this.SubAdminid, ddl_WsConnactor.SelectedValue, "", this.CompanyId);
            objGetWebserviceConnection.Process();
            if (objGetWebserviceConnection.StatusCode == 0)
            {
                if ((objGetWebserviceConnection.ResultTable.Rows[0]["WEBSERVICE_TYPE"]).ToString().Contains("wsdl"))
                {
                    hdfEditWsType.Value = "0";
                    BindServiceDropDown(objGetWebserviceConnection.ResultTable);
                    // strScript += " HideWsCmdEditDiv('0');HideDivWsConnChnage(1);AppActiveCtrl['CmdType']='0';";
                    strScript += " HideWsCmdEditDiv('0');AppActiveCtrl['CmdType']='0';";
                }
                else if ((objGetWebserviceConnection.ResultTable.Rows[0]["WEBSERVICE_TYPE"]).ToString().Contains("http"))
                {
                    txtWsCommand_Para.Text = "";
                    hdfEditWsType.Value = "1";
                    lblUrl.Text = (objGetWebserviceConnection.ResultTable.Rows[0]["WEBSERVICE_URL"]).ToString();
                    strScript += " HideWsCmdEditDiv('1');AppActiveCtrl['CmdType']='1';";
                }
                else if ((objGetWebserviceConnection.ResultTable.Rows[0]["WEBSERVICE_TYPE"]).ToString().Contains("rpc"))
                {
                    hdfEditWsType.Value = "2";
                    txtRpc_MethodName.Text = "";
                    strScript += " HideWsCmdEditDiv('2');AppActiveCtrl['CmdType']='2';";
                }
            }
            ScriptManager.RegisterStartupScript(updAddWsCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), (sender == null ? "" : "WsCommandEditClick();") + "SetWsDatasetPathVisible(false);" + strScript, true);
            updAddWsCommand.Update();

        }

        #endregion
        #region ServiceIndexChanged
        protected void ddlWsConnServices_SelectedIndexChanged(object sender, EventArgs e)
        {

            string strScript = "", strAdditionalScript = "", strOutParamHash = ";wsdlOutParamHash = {};"; ;

            if (hdfWsCommand_CommandId.Value.Length == 0) strScript = "HideWsCmdAddNameDiv('1');" + "HideDivWsConnChnage('2');";
            else strScript = "HideWsCmdAddNameDiv('0');" + "HideDivWsConnChnage('2');";
            if (ddlWsConnServices.SelectedValue == "-1")
            {
                ddl_WsConnectorMethod.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(updAddWsCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), (sender == null ? "" : "WsCommandEditClick();") + "HideWsCmdEditDiv('0');" + strScript, true);
                return;
            }
            DataTable objTable = new DataTable();
            objTable.Columns.Add("Port", typeof(String));
            objTable.Columns.Add("Binding", typeof(String));

            foreach (WSDL.Port objPort in ((WSDL.Service)(this.Wsdl.Services.findKey(ddlWsConnServices.SelectedValue))).Ports)
                objTable.Rows.Add(objPort.Name, objPort.Binding);

            if (this.Wsdl.PortTypes.Contains(ddlWsConnServices.SelectedValue + "Soap"))
            {
                ShowOperationOnServiceChange(ddlWsConnServices.SelectedValue + "Soap");

                foreach (ListItem item in ddl_WsConnectorMethod.Items)
                {
                    strOutParamHash += "wsdlOutParamHash['" + item.Value + "'] = ['" + GetWebServiceOutParam(this.Wsdl, ddlWsConnServices.SelectedValue + "Soap", item.Value) + "'];";
                }
            }
            else
            {
                strAdditionalScript = "CommonErrorMessages(66);";
            }
            updAddWsCommand.Update();
            ScriptManager.RegisterStartupScript(updAddWsCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), (sender == null ? "" : "WsCommandEditClick();") + "HideWsCmdEditDiv('0');" + strScript + strAdditionalScript + strOutParamHash, true);
        }
        #endregion
        #region Cancel/Reset And Clearmethods
        private void wsobjectClear()
        {
            updatedbright.Update();
            hdfWsCommand_CommandId.Value = "";
            txtRpc_MethodName.Text = "";
            txtRpc_ParaName.Text = "";
            txtWsCommand_DatasetPath.Text = "";
            txtWsCommand_Para.Text = "";
            txtWsCommand_Url.Text = "";
            txtWsdl_CmdName.Text = "";
            txtWsdlCmdDesc.Text = "";
            txtWsdlCommand_DatasetPath.Text = "";
            ddl_WsConnactor.SelectedValue = "-1";
            txthttpdatatemplate.Text = "";
            dd1DataCatch.SelectedValue = "0";
            hidSelectcatch.Value = "0";
        }


        protected void btnWsCommand_Cancel_Click(object sender, EventArgs e)
        {

            WsWsdlCmdDetailsDiv.Visible = true;
            WsHttpCmdDetailsDiv.Visible = true;
            btncancel.Visible = false;
            ddl_WsConnactor.Enabled = true;
            txtWsdl_CmdName.Enabled = true;
            hideditdone.Value = "";
            if (lblWsCmd_ServiceType.Text == "WSDL")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ViewWSDL();", true);
            }
            else if (lblWsCmd_ServiceType.Text == "HTTP")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ViewHttp();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ViewRpc();", true);
            }
            updatedbright.Update();
            updAddWsCommand.Update();
            updWsCommandSave.Update();
            updDelWsCmd.Update();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"EditCancelupdate();", true);
        }

        protected void btnWsCommand_Reset_Click(object sender, EventArgs e)
        {
            wsobjectClear();

        }
        private void CatchClear()
        {

            dd1DataCatch.SelectedIndex = 0;
            dd1AbMonth.SelectedIndex = 0;
            ddlAbMM.SelectedIndex = 0;
            drprltmin.SelectedIndex = 0;
            raltweeks.SelectedIndex = 0;
            dd1Abhh.SelectedIndex = 0;
            Drprlthour.SelectedIndex = 0;
            ddlAbMnth.SelectedIndex = 0;
            drprltmonth.SelectedIndex = 0;
            rltdayd.SelectedIndex = 0;
            dd1AbMnthDay.SelectedIndex = 0;
            dd1AbMnDay.SelectedIndex = 0;
            hidcatchexfrequency.Value = "";
            hidcatchexcondition.Value = "";
            hdfWsCommand_CommandId.Value = "";
            txthttpdatatemplate.Text = "";
        }
        private void ClearAfterAdd()
        {
            SucessfullyUpdateDatatable();
            wsobjectClear();
            CatchClear();
            dd1DataCatch.SelectedValue = "0";
            ddl_WsConnactor.SelectedValue = "-1";
            ddl_WsConnactor_SelectedIndexChanged(null, null);
            updAddWsCommand.Update();
            btncancel.Visible = false;
            ddl_WsConnactor.Enabled = true;
        }
        void resetdivs()
        {
            btnreset.Visible = true;
            updAddWsCommand.Update();
            ddl_WsConnactor.SelectedValue = "-1";
            ddlWsdlCommad_DataReturned.SelectedValue = "0";
            txtwscopyobject.Text = "";
            CatchClear();
            txtWsdlCmdDesc.Text = "";
            hidSelectcatch.Value = "0";
            WsWsdlCmdDetailsDiv.Visible = true;
            WsHttpCmdDetailsDiv.Visible = true;
            rdbWsHttpGet.Checked = true;
            rdbWsHttpPost.Checked = false;
            btncancel.Visible = false;
            ddl_WsConnactor.Enabled = true;
            txtWsdl_CmdName.Text = "";
            txtWsdl_CmdName.Enabled = true;
        }

        void Reset()
        {
            WsWsdlCmdDetailsDiv.Visible = true;
            WsHttpCmdDetailsDiv.Visible = true;
            btncancel.Visible = false;
            ddl_WsConnactor.Enabled = true;
            txtWsdl_CmdName.Enabled = true;
            updatedbright.Update();
            updAddWsCommand.Update();

            updWsCommandSave.Update();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"OnloadWSObjectData();", true);
        }
        private void CacheDropdownBind()
        {
            DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(null);
            for (int i = 0; i <= 59; i++)
            {

                ddlAbMM.Items.Add(new ListItem(i.ToString(), i.ToString()));
                if (i > 0)
                    drprltmin.Items.Add(new ListItem(i.ToString(), i.ToString()));
                if (i > 0 && i <= 52)//week
                    raltweeks.Items.Add(new ListItem(i.ToString(), i.ToString()));
                if (i <= 23)//hour
                {
                    dd1Abhh.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    if (i > 0)
                        Drprlthour.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                if (i > 0 && i <= 12)//month
                {
                    ddlAbMnth.Items.Add(new ListItem(info.GetMonthName(i), i.ToString()));
                    drprltmonth.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                if (i > 0 && i <= 31)//day
                {
                    rltdayd.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    dd1AbMnthDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                    dd1AbMnDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
        }

        #endregion
        #region WSDL  Service Methods
        private string GetWebServiceInputParameter(WSDL _WSDL, string _BindingName, string _OperationName, out string _OutParaJson)
        {
            string strInputParaJson = "";
            _OutParaJson = "";
            string st = "";
            string ct = "";
            foreach (WSDL.PortType objPortTypes in _WSDL.PortTypes)
            {
                if (objPortTypes.Name == ((WSDL.Binding)((WSDL.BindingCollection)_WSDL.Bindings).findKey(_BindingName)).PortType)
                {
                    foreach (WSDL.Operation objOperation in objPortTypes.Operations)
                    {
                        if (objOperation.Name == _OperationName)
                        {
                            strInputParaJson = "";
                            if (objOperation.InputMessageParts != null)
                            {
                                st = "";
                                ct = "";
                                foreach (WSDL.Part objPart in objOperation.InputMessageParts)
                                {
                                    if (objPart.Type == null)
                                    {
                                        if (st.Length > 0) st += ",";
                                        st += "{\"S_Name\":\"" + objPart.Name + "\"}";
                                    }
                                    else
                                    {
                                        if (objPart.Type.GetType().Equals(typeof(WSDL.ComplexData)))
                                        {
                                            WSDL.ComplexData objComplexData = (WSDL.ComplexData)objPart.Type;
                                            if (st.Length > 0) st += ",";
                                            st += GetJsonOfSimpleTypeElement(objComplexData.SimpleDataTypes);
                                            if (ct.Length > 0) ct += ",";
                                            ct += GetJsonComplexTypeElement(objComplexData.ComplexDataTypes);
                                        }
                                    }
                                }
                                strInputParaJson += "{" + ("\"S_Types\" :" + (st.Length > 0 ? "[" + st + "]" : "\"\"")) + "," + "\"C_Types\" :" + (ct.Length > 0 ? "[" + ct + "]" : "\"\"") + "}";
                            }
                            else
                            {
                                strInputParaJson = "";
                            }
                            _OutParaJson = "";
                            if (objOperation.OutputMessageParts != null)
                            {
                                st = "";
                                ct = "";
                                foreach (WSDL.Part objPart in objOperation.OutputMessageParts)
                                {
                                    if (objPart.Type == null)
                                    {
                                        if (st.Length > 0) st += ",";
                                        st += "{\"S_Name\":\"" + objPart.Name + "\"}";
                                    }
                                    else
                                    {
                                        WSDL.ComplexData objComplexData = (WSDL.ComplexData)objPart.Type;
                                        if (st.Length > 0) st += ",";
                                        st += GetJsonOfSimpleTypeElement(objComplexData.SimpleDataTypes);
                                        if (ct.Length > 0) ct += ",";
                                        ct += GetJsonComplexTypeElement(objComplexData.ComplexDataTypes);
                                    }
                                }
                                _OutParaJson += "{" + ("\"S_Types\" :" + (st.Length > 0 ? "[" + st + "]" : "\"\"")) + "," + "\"C_Types\" :" + (ct.Length > 0 ? "[" + ct + "]" : "\"\"") + "}";
                            }
                        }
                    }
                }
            }
            return strInputParaJson;
        }
        private string GetJsonComplexTypeElement(WSDL.ComplexDataCollection _cd)
        {
            string strCd = "";
            foreach (WSDL.ComplexData objCd in _cd)
            {
                string strSmipleTypes = GetJsonOfSimpleTypeElement(objCd.SimpleDataTypes);
                string strComplexTypes = GetJsonComplexTypeElement(objCd.ComplexDataTypes);
                strComplexTypes = (strComplexTypes.Length > 0) ? "\"C_Types\" : [" + strComplexTypes + "]" : "\"C_Types\" : \"\"";
                if (strCd.Length > 0)
                    strCd += ",";
                strCd += (strSmipleTypes.Length > 0) ? "{\"C_Name\":\"" + objCd.Name + "\",\"S_Types\":[" + strSmipleTypes + "]" + "," + strComplexTypes + "}" : "{\"C_Name\":\"" + objCd.Name + "\",\"S_Types\" :\"\"" + "," + strComplexTypes + "}";
            }
            return strCd;
        }
        private string GetJsonOfSimpleTypeElement(WSDL.SimpleDataCollection _sd)
        {
            string strSimpleType = "";
            foreach (WSDL.SimpleData objSimpleData in _sd)
            {
                if (objSimpleData.IsArray == false)
                    strSimpleType += (strSimpleType.Length > 0 ? "," + "{\"S_Name\":\"" + objSimpleData.Name + "\"}" : "{\"S_Name\":\"" + objSimpleData.Name + "\"}");
                else
                    strSimpleType += (strSimpleType.Length > 0 ? "," + "{\"S_Name\":\"" + objSimpleData.Name + "[]\"}" : "{\"S_Name\":\"" + objSimpleData.DataType + "[]\"}");

            }
            return strSimpleType;
        }

        private string GetWebServiceOutParam(WSDL _WSDL, string _BindingName, string _OperationName)
        {
            String strOutParaJson = "";
            string st = "";
            string ct = "";
            string ctName = "";
            foreach (WSDL.PortType objPortTypes in _WSDL.PortTypes)
            {
                if (objPortTypes.Name == ((WSDL.Binding)((WSDL.BindingCollection)_WSDL.Bindings).findKey(_BindingName)).PortType)
                {
                    foreach (WSDL.Operation objOperation in objPortTypes.Operations)
                    {
                        if (objOperation.Name == _OperationName)
                        {
                            strOutParaJson = "";
                            if (objOperation.OutputMessageParts != null)
                            {
                                st = "";
                                ct = "";

                                foreach (WSDL.Part objPart in objOperation.OutputMessageParts)
                                {
                                    if (objPart.Type == null)
                                    {
                                        if (st.Length > 0) st += ",";
                                        st += "{\"S_Name\":\"" + objPart.Name + "\"}";
                                    }
                                    else
                                    {
                                        WSDL.ComplexData objComplexData = (WSDL.ComplexData)objPart.Type;
                                        if (st.Length > 0) st += ",";
                                        st += GetJsonInSimpleType(objComplexData.SimpleDataTypes);
                                        if (ct.Length > 0) ct += ",";
                                        ct += GetJsonOutComplexType(objComplexData.ComplexDataTypes);
                                        ctName = objComplexData.Name;
                                    }
                                }
                                strOutParaJson += "{" + ("\"C_Name\":\"" + ctName + "\",\"S_Types\" :" + (st.Length > 0 ? "[" + st + "]" : "\"\"")) + "," + "\"C_Types\" :" + (ct.Length > 0 ? "[" + ct + "]" : "\"\"") + "}";
                            }
                        }
                    }
                }
            }
            return strOutParaJson;
        }
        private string GetJsonOutComplexType(WSDL.ComplexDataCollection _cd)
        {
            string strCd = "";
            foreach (WSDL.ComplexData objCd in _cd)
            {
                string strSmipleTypes = GetJsonInSimpleType(objCd.SimpleDataTypes);
                string strComplexTypes = GetJsonOutComplexType(objCd.ComplexDataTypes);
                strComplexTypes = (strComplexTypes.Length > 0) ? "\"C_Types\" : [" + strComplexTypes + "]" : "\"C_Types\" : \"\"";
                if (strCd.Length > 0)
                    strCd += ",";
                strCd += (strSmipleTypes.Length > 0) ? "{\"C_Name\":\"" + objCd.Name + "\",\"Is_Array\":\"" + Convert.ToInt32(objCd.IsArray) + "\",\"S_Types\":[" + strSmipleTypes + "]" + "," + strComplexTypes + "}" : "{\"C_Name\":\"" + objCd.Name + "\",\"Is_Array\":\"" + Convert.ToInt32(objCd.IsArray) + "\",\"S_Types\" :\"\"" + "," + strComplexTypes + "}";
            }
            return strCd;
        }

        private string GetJsonInSimpleType(WSDL.SimpleDataCollection _sd)
        {
            string strSimpleType = "";
            foreach (WSDL.SimpleData objSimpleData in _sd)
            {
                if (objSimpleData.IsArray == false)
                    strSimpleType += (strSimpleType.Length > 0 ? "," + "{\"S_Name\":\"" + objSimpleData.Name + "\",\"Is_Array\":\"" + Convert.ToInt32(objSimpleData.IsArray) + "\"}" : "{\"S_Name\":\"" + objSimpleData.Name + "\",\"Is_Array\":\"" + Convert.ToInt32(objSimpleData.IsArray) + "\"}");
                else
                    strSimpleType += (strSimpleType.Length > 0 ? "," + "{\"S_Name\":\"" + objSimpleData.Name + "[]\",\"Is_Array\":\"" + Convert.ToInt32(objSimpleData.IsArray) + "\"}" : "{\"S_Name\":\"" + objSimpleData.DataType + "[]\",\"Is_Array\":\"" + Convert.ToInt32(objSimpleData.IsArray) + "\"}");

            }
            return strSimpleType;
        }

        protected void bindCreadential()
        {
            hidWsAuthencationMeta.Value = "";
            GetEnterpriseAdditionalDefinition objAuthMeta = new GetEnterpriseAdditionalDefinition(this.CompanyId);
            if (objAuthMeta.StatusCode == 0)
            {
                if (objAuthMeta.StatusCode == 0 && objAuthMeta.ResultTable.Rows.Count > 0)
                {
                    JArray lst = Utilities.decriptAllCredential(Convert.ToString(objAuthMeta.ResultTable.Rows[0]["AUTHENTICATION_META"]), this.CompanyId);
                    hidWsAuthencationMeta.Value = lst.ToString();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Internal Server Error" + "');", true);
            }
        }


        public WSDL Wsdl
        {
            get
            {
                if (ViewState["wsdl"] != null)
                {
                    _WSDL = (WSDL)ViewState["wsdl"];
                }
                return _WSDL;
            }
            set
            {
                ViewState["wsdl"] = value;
            }
        }
        private void ShowOperationOnServiceChange(string _PortType)
        {
            string sender = null;
            string strScript = "";
            if (hdfWsCommand_CommandId.Value.Length == 0) strScript = "HideWsCmdAddNameDiv('1');" + "HideDivWsConnChnage('3');";
            else strScript = "HideWsCmdAddNameDiv('0');" + "HideDivWsConnChnage('3');";

            if (_PortType == "-1")
            {
                ScriptManager.RegisterStartupScript(updAddWsCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), (sender == null ? "" : "WsCommandEditClick();") + "HideWsCmdEditDiv('0');" + strScript, true);
                return;
            }

            List<string> objOperationList = new List<string>();
            foreach (WSDL.Operation objOperation in this.Wsdl.PortTypes.findKey(this.Wsdl.Bindings.findKey(_PortType).PortType).Operations)
                objOperationList.Add(objOperation.Name);

            ddl_WsConnectorMethod.DataSource = objOperationList;
            ddl_WsConnectorMethod.DataBind();
            ddl_WsConnectorMethod.Items.Insert(0, new ListItem("Select web service Method", "-1"));
            updAddWsCommand.Update();
            ScriptManager.RegisterStartupScript(updAddWsCommand, typeof(UpdatePanel), Guid.NewGuid().ToString(), (sender == null ? "" : "WsCommandEditClick();") + "HideWsCmdEditDiv('0');" + strScript, true);

        }

        private void BindServiceDropDown(DataTable _dt)
        {
            if (_dt.Rows.Count > 0)
            {
                if (Convert.ToString(_dt.Rows[0]["WSDL_CONTENT"]).Length > 0)
                {
                    WSDL objWSDL = new WSDL(Convert.ToString(_dt.Rows[0]["WSDL_CONTENT"]));
                    this.Wsdl = objWSDL;
                    List<string> lstServices = new List<string>();
                    foreach (WSDL.Service objServices in objWSDL.Services)
                        lstServices.Add(objServices.Name);
                    if (lstServices.Count > 0)
                    {
                        ddlWsConnServices.DataSource = lstServices;
                        ddlWsConnServices.DataBind();
                        ddlWsConnServices.Items.Insert(0, new ListItem("Select Services", "-1"));
                        ddlWsConnServices.Visible = true;
                    }
                }
            }
        }

        #endregion
        #region Discardobjects
        private void DiscarSaveWsCommand(UpdatePanel update)
        {
            if (hdfEditWsType.Value == "1")
            {
                if (WebserviceHttpCommandValidation(updatediscard))
                {
                    string strparafinal = "";
                    string strPara = GetWebserviceParameters1(txtWsCommand_Url.Text, updatediscard);
                    string strparadatatemplate = GetWebserviceParameters1(txthttpdatatemplate.Text, updatediscard);
                    if (strparadatatemplate != "")
                    {
                        strparafinal = strPara + "," + strparadatatemplate;
                    }
                    else
                    {
                        strparafinal = strPara;
                    }
                    if (strparafinal == null) return;
                    DisacardSaveSimpleHttpCommand(strparafinal, updatediscard);


                }
            }
            else if (hdfEditWsType.Value == "0")
            {
                if (WebserviceWsdlCommandValidation(updatediscard))
                {
                    string strOutPara = "";
                    string strOutPutPartName = "";
                    string strPara = "";
                    string strSoapRequest = "";
                    string strOperationAction = "";
                    try
                    {
                        strOutPutPartName = this.Wsdl.PortTypes.findKey(this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").PortType).Operations.findKey(ddl_WsConnectorMethod.SelectedValue).OutputMessageParts[0].TypeName;
                    }
                    catch { }
                    try
                    {
                        strPara = GetWebServiceInputParameter(this.Wsdl, ddlWsConnServices.SelectedValue + "Soap", ddl_WsConnectorMethod.SelectedValue, out strOutPara);
                    }
                    catch { }
                    try
                    {
                        strSoapRequest = this.Wsdl.getSoapRequest(ddl_WsConnectorMethod.SelectedValue, this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").PortType, this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").TargetNameSpace, ((this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").Style == "RPC") ? true : false), this.Wsdl.PortTypes, this.Wsdl.SimpleDataTypes);
                    }
                    catch { }
                    try
                    {
                        strOperationAction = this.Wsdl.Services.findKey(ddlWsConnServices.SelectedValue).Ports.findKey(this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").PortType).Address + "," + this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").OperationSoapActions.findKey(ddl_WsConnectorMethod.SelectedValue).SoapAction + "," + strOutPutPartName;
                    }
                    catch { }
                    DiscardSaveWsdlCommand(strPara, strOutPara, strSoapRequest, strOperationAction, updatediscard);

                }
            }
            else
            {
                DiscardSaveXmlRpc();
            }

        }
        private void DiscardSaveWsdlCommand(string _Parameter, string _Element, string _SoapRequest, string _OperationAction, UpdatePanel _Upd)
        {
            string strDsPath = "";
            txtWsdlCommand_DatasetPath.Text = (ddlWsdlCommad_DataReturned.SelectedValue == "0") ? "" : txtWsdlCommand_DatasetPath.Text;
            strDsPath = (txtWsdlCommand_DatasetPath.Text.Trim().Length == 0 ? "" : (hdfWsdlOutParamPfix.Value.Trim().Length == 0 ? txtWsdlCommand_DatasetPath.Text.Trim() : hdfWsdlOutParamPfix.Value.Trim() + '.' + txtWsdlCommand_DatasetPath.Text.Trim()));

            UpdateWsCommand objUpdateWsCommand = new UpdateWsCommand(hdfWsCommand_CommandId.Value, this.SubAdminid, txtWsCommand_Url.Text, _Parameter,
            ddlWsdlCommad_DataReturned.SelectedValue, "", strDsPath, hdfWsCmdPara.Value,
            (ddlWsConnServices.SelectedValue != "-1" ? ddlWsConnServices.SelectedValue : ""), 
            (ddlWsConnServices.SelectedValue != "-1" ? ddlWsConnServices.SelectedValue + "Soap" : ""), 
            (ddl_WsConnectorMethod.SelectedValue != "-1" ? ddl_WsConnectorMethod.SelectedValue : ""),
            "wsdl", _SoapRequest, _OperationAction, null, Convert.ToInt32(HTTP_TYPE.POST), this.CompanyId, txtWsdlCmdDesc.Text,
            Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value,
            Convert.ToInt64(DateTime.UtcNow.Ticks), "[]", "",CreateHeader()); 
            objUpdateWsCommand.Process1();
            if (objUpdateWsCommand.StatusCode == 0)
            {
                WscmdObjectTable();
                updatedbright.Update();
                updAddWsCommand.Update();
                ClearAfterAdd();
                Reset();
                hiddiscardsuccess.Value = "1";
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "AfterSuccessfullUpdate();AddWsCommandDiv(false);", true);


            }
            else
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                return;
            }

        }
        private void DisacardSaveSimpleHttpCommand(string _Parameter, UpdatePanel _Upd)
        {
            string strRecordPath = "", strDatsetPath = "";
            if (ddlWsCommad_DataReturned.SelectedValue == "0")
            {
            }
            else
            {
                strDatsetPath = txtWsCommand_DatasetPath.Text;
            }
            UpdateWsCommand objUpdateWsCommand = new UpdateWsCommand(hdfWsCommand_CommandId.Value, this.SubAdminid, txtWsCommand_Url.Text, _Parameter,
            ddlWsCommad_DataReturned.SelectedValue, strRecordPath, strDatsetPath, hdfWsCmdPara.Value, "", "", "", "http", "", "", null,
            (rdbWsHttpGet.Checked ? Convert.ToInt32(HTTP_TYPE.GET) : Convert.ToInt32(HTTP_TYPE.POST)), this.CompanyId, txtWsdlCmdDesc.Text,
            Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value,
            Convert.ToInt64(DateTime.UtcNow.Ticks), "[]", txthttpdatatemplate.Text,CreateHeader());

            objUpdateWsCommand.Process2();
            if (objUpdateWsCommand.StatusCode == 0)
            {
                WscmdObjectTable();
                updatedbright.Update();
                updAddWsCommand.Update();
                ClearAfterAdd();
                Reset();
                hiddiscardsuccess.Value = "1";
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "AfterSuccessfullUpdate();", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                return;
            }

        }
        private void DiscardSaveXmlRpc()
        {
            if (ddl_WsConnactor.SelectedValue == "-1")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Please Select Connector.');", true);
                return;
            }

            if (txtWsdl_CmdName.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Please enter Object Name.');", true);
                return;
            }

            if (txtRpc_MethodName.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Please enter Method Name.');", true);
                return;
            }
            UpdateWsCommand objUpdateWsCommand = new UpdateWsCommand(hdfWsCommand_CommandId.Value, this.SubAdminid, "", hdfRpcParam.Value,
            "", "", "", hdfOutRpcParam.Value, "", "", txtRpc_MethodName.Text, "rpc", "", "", "", Convert.ToInt32(HTTP_TYPE.POST),
            this.CompanyId, txtWsdlCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value),
            hidcatchexcondition.Value, Convert.ToInt64(DateTime.UtcNow.Ticks), "[]","", CreateHeader());
            objUpdateWsCommand.Process1();
            if (objUpdateWsCommand.StatusCode == 0)
            {
                WscmdObjectTable();
                updatedbright.Update();
                ClearAfterAdd();
                Reset();
                updAddWsCommand.Update();
                hiddiscardsuccess.Value = "1";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "AfterSuccessfullUpdate();AddWsCommandDiv(false);", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                return;
            }

        }
        protected void lnkdiscardyes_Click(object sender, EventArgs e)
        {
            resetdivs();
            if (hideditdone.Value != "" && hiddiscardcommadtype.Value == "2")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"BindDatabaseObjectdata();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"discardedit();", true);
            }
            hiddiscardcommadtype.Value = "";
            hideditdone.Value = "";
        }



        #endregion
        #region Save Type of Servies
        protected void btnWsCommand_Save_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCatch();
                SaveWsCommand();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }
        }
        private void SaveSimpleHttpCommand(string _Parameter, UpdatePanel _Upd)
        {
            string strRecordPath = "", strDatsetPath = "";
            if (ddlWsCommad_DataReturned.SelectedValue == "0")
            {
            }
            else
            {
                strDatsetPath = txtWsCommand_DatasetPath.Text;
            }
            if (hdfWsCommand_CommandId.Value.Length == 0)
            {
                GetWsCommand objGetWsCommand = new GetWsCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
                objGetWsCommand.Process();
                if (objGetWsCommand.StatusCode == 0 && objGetWsCommand.ResultTable.Rows.Count > 0)
                {
                    string filter = String.Format("WS_COMMAND_NAME = '{0}'", txtWsdl_CmdName.Text.Trim());
                    DataRow[] rows = objGetWsCommand.ResultTable.Select(filter);
                    if (rows.Length > 0)
                    {
                        ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#a2').html('Object name already exists.');", true);
                        return;
                    }
                }
                AddNewWsCommand objAddNewWsCommand = new AddNewWsCommand(this.SubAdminid, txtWsdl_CmdName.Text.Trim(), ddl_WsConnactor.SelectedValue, txtWsCommand_Url.Text, _Parameter,
                ddlWsCommad_DataReturned.SelectedValue, strRecordPath, strDatsetPath, hdfWsCmdPara.Value, "", "", "", "http", "", "", 
                null, (rdbWsHttpGet.Checked ? Convert.ToInt32(HTTP_TYPE.GET) : Convert.ToInt32(HTTP_TYPE.POST)), 
                txthttpdatatemplate.Text, this.CompanyId, txtWsdlCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), 
                Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value, "[]", this.SubAdminid, Convert.ToInt64(DateTime.UtcNow.Ticks),
                Convert.ToInt64(DateTime.UtcNow.Ticks),CreateHeader());

                objAddNewWsCommand.Process3();
                if (objAddNewWsCommand.StatusCode == 0)
                {
                    ClearAfterAdd();
                    Reset();
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object saved successfully.');SubProcConfirmBoxMessageWo(true,'Saved',250);AddWsCommandDiv(false);AfterSuccessfullUpdate();", true);
                }

                else
                {
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#a2').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
            else
            {
                UpdateWsCommand objUpdateWsCommand = new UpdateWsCommand(hdfWsCommand_CommandId.Value, this.SubAdminid, txtWsCommand_Url.Text, _Parameter,
                ddlWsCommad_DataReturned.SelectedValue, strRecordPath, strDatsetPath, hdfWsCmdPara.Value, "", "", "", "http", "", "", null,
                (rdbWsHttpGet.Checked ? Convert.ToInt32(HTTP_TYPE.GET) : Convert.ToInt32(HTTP_TYPE.POST)), this.CompanyId, txtWsdlCmdDesc.Text,
                Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value,
                Convert.ToInt64(DateTime.UtcNow.Ticks), "[]", txthttpdatatemplate.Text,CreateHeader());

                objUpdateWsCommand.Process2();
                if (objUpdateWsCommand.StatusCode == 0)
                {
                    WscmdObjectTable();
                    updatedbright.Update();
                    ClearAfterAdd();
                    Reset();
                    hideditdone.Value = "";
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object Updated successfully.');SubProcConfirmBoxMessageWo(true,'Saved',350);AfterSuccessfullUpdate();OnloadWSObjectData();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
            updAddWsCommand.Update();
        }

        private void SaveWsdlCommand(string _Parameter, string _Element, string _SoapRequest, string _OperationAction, UpdatePanel _Upd)
        {
            string strDsPath = "";
            if (hdfWsCommand_CommandId.Value.Length == 0)
            {
                GetWsCommand objGetWsCommand = new GetWsCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
                objGetWsCommand.Process();
                if (objGetWsCommand.StatusCode == 0)
                {
                    string filter = String.Format("WS_COMMAND_NAME = '{0}'", txtWsdl_CmdName.Text.Trim());
                    DataRow[] rows = objGetWsCommand.ResultTable.Select(filter);
                    if (rows.Length > 0)
                    {
                        ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Object name already exists.');", true);
                        return;
                    }
                }
                txtWsdlCommand_DatasetPath.Text = (ddlWsdlCommad_DataReturned.SelectedValue == "0") ? "" : txtWsdlCommand_DatasetPath.Text;
                strDsPath = (txtWsdlCommand_DatasetPath.Text.Trim().Length == 0 ? "" : (hdfWsdlOutParamPfix.Value.Trim().Length == 0 ? txtWsdlCommand_DatasetPath.Text.Trim() : hdfWsdlOutParamPfix.Value.Trim() + '.' + txtWsdlCommand_DatasetPath.Text.Trim()));

                
                AddNewWsCommand objAddNewWsCommand = new AddNewWsCommand(this.SubAdminid, txtWsdl_CmdName.Text.Trim(),
                                                        ddl_WsConnactor.SelectedValue, txtWsCommand_Url.Text, _Parameter,
                                                        ddlWsdlCommad_DataReturned.SelectedValue, "",
                                                        strDsPath, hdfWsCmdPara.Value,
                                                        (ddlWsConnServices.SelectedValue != "-1" ? ddlWsConnServices.SelectedValue : ""),
                                                        (ddlWsConnServices.SelectedValue != "-1" ? ddlWsConnServices.SelectedValue + "Soap" : ""), 
                                                        (ddl_WsConnectorMethod.SelectedValue != "-1" ? ddl_WsConnectorMethod.SelectedValue : ""),
                                                        "wsdl", _SoapRequest, _OperationAction, null, Convert.ToInt32(HTTP_TYPE.POST), "",
                                                        this.CompanyId, txtWsdlCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), 
                                                        Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value, "[]", this.SubAdminid,
                                                        Convert.ToInt64(DateTime.UtcNow.Ticks), Convert.ToInt64(DateTime.UtcNow.Ticks), CreateHeader());
                objAddNewWsCommand.Process2();
                if (objAddNewWsCommand.StatusCode == 0)
                {
                    ClearAfterAdd();
                    Reset();
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object saved successfully.');SubProcConfirmBoxMessageWo(true,'Saved',250);AddWsCommandDiv(false);AfterSuccessfullUpdate();intializewsobject();", true);


                }
                else
                {
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
            else
            {
                txtWsdlCommand_DatasetPath.Text = (ddlWsdlCommad_DataReturned.SelectedValue == "0") ? "" : txtWsdlCommand_DatasetPath.Text;
                strDsPath = (txtWsdlCommand_DatasetPath.Text.Trim().Length == 0 ? "" : (hdfWsdlOutParamPfix.Value.Trim().Length == 0 ? txtWsdlCommand_DatasetPath.Text.Trim() : hdfWsdlOutParamPfix.Value.Trim() + '.' + txtWsdlCommand_DatasetPath.Text.Trim()));

                UpdateWsCommand objUpdateWsCommand = new UpdateWsCommand(hdfWsCommand_CommandId.Value, this.SubAdminid, txtWsCommand_Url.Text, _Parameter,
                                                    ddlWsdlCommad_DataReturned.SelectedValue, "", strDsPath, hdfWsCmdPara.Value, 
                                                    (ddlWsConnServices.SelectedValue != "-1" ? ddlWsConnServices.SelectedValue : ""), 
                                                    (ddlWsConnServices.SelectedValue != "-1" ? ddlWsConnServices.SelectedValue + "Soap" : ""),
                                                    (ddl_WsConnectorMethod.SelectedValue != "-1" ? ddl_WsConnectorMethod.SelectedValue : ""),
                                                    "wsdl", _SoapRequest, _OperationAction, null, Convert.ToInt32(HTTP_TYPE.POST), this.CompanyId,
                                                    txtWsdlCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), 
                                                    Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value,
                                                    Convert.ToInt64(DateTime.UtcNow.Ticks), "[]", "",CreateHeader()) ;

                objUpdateWsCommand.Process1();
                if (objUpdateWsCommand.StatusCode == 0)
                {
                    WscmdObjectTable();
                    updatedbright.Update();
                    ClearAfterAdd();
                    Reset();
                    hideditdone.Value = "";
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object Updated successfully.');SubProcConfirmBoxMessageWo(true,'Saved',350);AfterSuccessfullUpdate();AddWsCommandDiv(false);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);

                    return;
                }
            }
        }
        private string CreateHeader()
        {
            JArray jaHeaders = new JArray();
            JArray jaHeader = new JArray();
            jaHeader.Add("ct");
            jaHeader.Add(ddlContantType.SelectedValue);
            jaHeaders.Add(jaHeader);

            return jaHeaders.ToString();
        }
        private void SaveXmlRpc()
        {

            if (ddl_WsConnactor.SelectedValue == "-1")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Please Select Connector.');", true);
                return;
            }

            if (txtWsdl_CmdName.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Please enter Object Name.');", true);
                return;
            }

            if (txtRpc_MethodName.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Please enter Method Name.');", true);
                return;
            }
            if (hdfWsCommand_CommandId.Value.Length == 0)
            {
                GetWsCommand objGetWsCommand = new GetWsCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
                objGetWsCommand.Process();
                if (objGetWsCommand.StatusCode == 0)
                {
                    string filter = String.Format("WS_COMMAND_NAME = '{0}'", txtWsdl_CmdName.Text.Trim());
                    DataRow[] rows = objGetWsCommand.ResultTable.Select(filter);
                    if (rows.Length > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Object name already exists.');", true);
                        return;
                    }
                }
                AddNewWsCommand objAddNewWsCommand = new AddNewWsCommand(this.SubAdminid, txtWsdl_CmdName.Text.Trim(), ddl_WsConnactor.SelectedValue, "", hdfRpcParam.Value,
                "", "", "", hdfOutRpcParam.Value, "", "", txtRpc_MethodName.Text, "rpc", "", "", "",
                Convert.ToInt32(HTTP_TYPE.POST), "", this.CompanyId, txtWsdlCmdDesc.Text,
                Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), 
                hidcatchexcondition.Value, "[]", this.SubAdminid, Convert.ToInt64(DateTime.UtcNow.Ticks),
                Convert.ToInt64(DateTime.UtcNow.Ticks),CreateHeader());
                objAddNewWsCommand.Process2();
                if (objAddNewWsCommand.StatusCode == 0)
                {
                    ClearAfterAdd();
                    Reset();

                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object saved successfully.');SubProcConfirmBoxMessageWo(true,'Saved',250);AddWsCommandDiv(false);AfterSuccessfullUpdate();", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
            else
            {
                UpdateWsCommand objUpdateWsCommand = new UpdateWsCommand(hdfWsCommand_CommandId.Value, this.SubAdminid, "", hdfRpcParam.Value,
                "", "", "", hdfOutRpcParam.Value, "", "", txtRpc_MethodName.Text, "rpc", "", "", "", Convert.ToInt32(HTTP_TYPE.POST), this.CompanyId,
                txtWsdlCmdDesc.Text, Convert.ToInt32(dd1DataCatch.SelectedValue.ToString()), Convert.ToInt32(hidcatchexfrequency.Value), hidcatchexcondition.Value,
                Convert.ToInt64(DateTime.UtcNow.Ticks), "[]","",CreateHeader());
                objUpdateWsCommand.Process1();
                if (objUpdateWsCommand.StatusCode == 0)
                {
                    WscmdObjectTable();
                    updatedbright.Update();
                    ClearAfterAdd();
                    Reset();
                    hideditdone.Value = "";
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object Updated successfully.');SubProcConfirmBoxMessageWo(true,'Saved',350);AfterSuccessfullUpdate();AddWsCommandDiv(false);", true);
                    // ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#a2').html('');$('#aMessage').html('Object Updated successfully.');SubProcConfirmBoxMessage(true,' Saved',250);SubProcDeleteCmdConfrmation(false);AddWsCommandDiv(false);AfterSuccessfullUpdate();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
        }

        private void SaveWsCommand()
        {

            if (hdfEditWsType.Value == "1")
            {
                if (WebserviceHttpCommandValidation(updWsCommandSave))
                {
                    //string strparafinal = "";
                    string strparafinal = GetWebserviceParameters1(txtWsCommand_Url.Text + txthttpdatatemplate.Text, updWsCommandSave);
                    //string strparadatatemplate = GetWebserviceParameters1(, updWsCommandSave);
                   
                    SaveSimpleHttpCommand(strparafinal, updWsCommandSave);
                }
            }
            else if (hdfEditWsType.Value == "0")
            {
                if (WebserviceWsdlCommandValidation(updWsCommandSave))
                {
                    string strOutPara = "";
                    string strOutPutPartName = "";
                    string strPara = "";
                    string strSoapRequest = "";
                    string strOperationAction = "";
                    try
                    {
                        strOutPutPartName = this.Wsdl.PortTypes.findKey(this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").PortType).Operations.findKey(ddl_WsConnectorMethod.SelectedValue).OutputMessageParts[0].TypeName;
                    }
                    catch { }
                    try
                    {
                        strPara = GetWebServiceInputParameter(this.Wsdl, ddlWsConnServices.SelectedValue + "Soap", ddl_WsConnectorMethod.SelectedValue, out strOutPara);
                    }
                    catch { }
                    try
                    {
                        strSoapRequest = this.Wsdl.getSoapRequest(ddl_WsConnectorMethod.SelectedValue, this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").PortType, this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").TargetNameSpace, ((this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").Style == "RPC") ? true : false), this.Wsdl.PortTypes, this.Wsdl.SimpleDataTypes);
                    }
                    catch { }
                    try
                    {
                        strOperationAction = this.Wsdl.Services.findKey(ddlWsConnServices.SelectedValue).Ports.findKey(this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").PortType).Address + "," + this.Wsdl.Bindings.findKey(ddlWsConnServices.SelectedValue + "Soap").OperationSoapActions.findKey(ddl_WsConnectorMethod.SelectedValue).SoapAction + "," + strOutPutPartName;
                    }
                    catch { }

                    SaveWsdlCommand(strPara, strOutPara, strSoapRequest, strOperationAction, updWsCommandSave);
                }
            }
            else
            {
                SaveXmlRpc();
            }

        }


        void SaveDicardcommand()
        {
            SaveCatch();
            DiscarSaveWsCommand(updatediscard);
        }
        // Save Cache
        protected void SaveCatch()
        {
            if (dd1DataCatch.SelectedValue.ToString() == "0")
            {
                hidcatchexfrequency.Value = "0";
                hidcatchexcondition.Value = "1";
            }
            else if (dd1DataCatch.SelectedValue.ToString() == "1")
            {
                if (dd1AbMonth.SelectedValue.ToString() == "0")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    hidcatchexcondition.Value = dd1AbHr.SelectedValue.ToString();
                }
                else if (dd1AbMonth.SelectedValue.ToString() == "1")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    string strabhh = dd1Abhh.SelectedValue.ToString();
                    string strabmm = ddlAbMM.SelectedValue.ToString();
                    string strabfinal = strabhh + ":" + strabmm;
                    hidcatchexcondition.Value = strabfinal;
                }
                else if (dd1AbMonth.SelectedValue.ToString() == "2")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    hidcatchexcondition.Value = ddlAbDay.SelectedValue.ToString();

                }
                else if (dd1AbMonth.SelectedValue.ToString() == "3")
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    hidcatchexcondition.Value = dd1AbMnDay.SelectedValue.ToString();
                }
                else
                {
                    hidcatchexfrequency.Value = dd1AbMonth.SelectedValue.ToString();
                    string strabmnday = dd1AbMnthDay.SelectedValue.ToString();
                    string strabmn = ddlAbMnth.SelectedValue.ToString();
                    string strabfinalres = strabmnday + "/" + strabmn;
                    hidcatchexcondition.Value = strabfinalres;
                }
            }
            else
            {
                if (ddlRlt.SelectedValue.ToString() == "0")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = drprltmin.SelectedValue.ToString();
                }
                else if (ddlRlt.SelectedValue.ToString() == "1")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = Drprlthour.SelectedValue.ToString();
                }
                else if (ddlRlt.SelectedValue.ToString() == "2")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = rltdayd.SelectedValue.ToString();
                }
                else if (ddlRlt.SelectedValue.ToString() == "3")
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = raltweeks.SelectedValue.ToString();
                }
                else
                {
                    hidcatchexfrequency.Value = ddlRlt.SelectedValue.ToString();
                    hidcatchexcondition.Value = drprltmonth.SelectedValue.ToString();
                }

            }

        }


        private void SucessfullyUpdateDatatable()
        {
            WscmdObjectTable();
            upwsobject.Update();
            ddlWsConnServices.SelectedValue = "-1";
            ddl_WsConnectorMethod.SelectedValue = "-1";
            ScriptManager.RegisterStartupScript(updWsCommandSave, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"OnloadWSObjectData();Cleartext();intializewsobject();", true);
        }

        #endregion
        #region GetServieParameter

        protected string GetFormatedStringDbType(object _InputString)
        {
            string strOutput = "";
            switch (Convert.ToString(_InputString))
            {
                case "1":
                    strOutput = "MS SQL Server";
                    break;
                case "2":
                    strOutput = "Oracle";
                    break;
                case "3":
                    strOutput = "MySQL";
                    break;
                case "4":
                    strOutput = "PostgreSQL";
                    break;
                case "5":
                    strOutput = "DSN (ODBC)";
                    break;
            }
            return strOutput;
        }
        //private string GetWebserviceParameters(string _Url, UpdatePanel _Upd)
        //{
        //    Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9]*$");
        //    _Url = _Url.Replace(" ", string.Empty);
        //    _Url = _Url.Replace("=@@", "#");
        //    _Url = _Url.Replace("@@", "@");

        //    var parameterCont = from e in _Url.Split('#') where e.Contains('@') select e.Split('@').First();
        //    string strParameter = "";
        //    foreach (var parameter in parameterCont)
        //    {
        //        if (strParameter.Length > 0)
        //            strParameter += ",";
        //        strParameter += parameter;
        //        if (!objAlphaPattern.IsMatch(parameter))
        //        {
        //            ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Web service object cannot be saved. Please enter valid input parameter url');", true);
        //            return null;
        //        }
        //    }
        //    return strParameter;
        //}
        private string GetWebserviceParameters1(string _Url, UpdatePanel _Upd)
        {
            string strParameter, s1 = "";
            if (_Url != "" && _Url.Contains("@@"))
            {
                //Regex objAlphaPattern1 = new Regex(@"^[-_!#$a-zA-Z0-9]*$");
                List<string> lst = new List<string>();
                //s1 = _Url.Replace(" ", string.Empty);
                //s1 = _Url.Replace("@@", "/");
                //string[] ara = s1.Split('/');
                string str2 = _Url;
                while (str2.Contains("@@"))
                {
                    string str = str2.Substring(0, str2.LastIndexOf("@@"));
                    lst.Add(str.Substring(str.LastIndexOf("@@")+2));
                    str2 = str.Substring(0, str.LastIndexOf("@@"));
                }
                //{
                //    if (parameter != "")
                //    {
                //        bool sts = objAlphaPattern1.IsMatch(parameter);
                //        if (sts != false)
                //        {
                //            lst.Add(parameter);
                //        }
                //    }
                //}
                strParameter = string.Join(",", lst.ToArray());
            }
            else
            {
                strParameter = "";
            }
            return strParameter;
        }
        #endregion
        #region EditServices
        protected void btnwsobject_Click(object sender, EventArgs e)
        {
            if (hdfWsCommand_CommandId.Value.Trim().Length == 0) return;
            if (hdfWsCommand_CommandId.Value.Trim().Length != 0)
            {
                hideditdone.Value = "1";
                EditWsCommand(this.SubAdminid, hdfWsCommand_CommandId.Value);
                btncancel.Visible = true;
                btnreset.Visible = false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Internal Server Error');", true);
            }

            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "ShowHideHeader();disabletextwsobject();", true);
        }

        private void EditWsCommand(string _SubAdminId, string _CommandId)
        {
            string strInParaHtml = "";
            string strOutParaHtml = "", strAdditionalScript = "$('[id$=hdfWsCmdPara]').val('');WsParaHash['0'] = ['[]'];";
            hdfWsCmdPara.Value = "";

            GetWsCommand objGetWsCommand = new GetWsCommand(false, true, _SubAdminId, _CommandId, hidwsconname.Value, this.CompanyId, "1");
            objGetWsCommand.Process1();
            if (objGetWsCommand.StatusCode != 0) return;

            if (objGetWsCommand.ResultTable.Rows.Count == 0) return;


            else if (Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["WEBSERVICE_TYPE"]).Trim() == "wsdl")
            {
                strAdditionalScript += EditWsdlCommand(objGetWsCommand.ResultTable);

                lblcreatedby.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["CreatedBY"]);
                string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(objGetWsCommand.ResultTable.Rows[0]["UPDATED_ON"]));
                lblupdatedby.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
                lblWsCmd_Name.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["WS_COMMAND_NAME"]);
                if (ddlWsdlCommad_DataReturned.SelectedValue == "1")
                {
                    strAdditionalScript += "SetWsDatasetPathVisible(true);";

                }
                else
                {

                    strAdditionalScript += "SetWsDatasetPathVisible(false);";
                }
                ScriptManager.RegisterStartupScript(updDelWsCmd, typeof(UpdatePanel), Guid.NewGuid().ToString(), strAdditionalScript, true);
            }

            else if (Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["WEBSERVICE_TYPE"]).Trim() == "http")
            {
                strAdditionalScript += EditHttpCommand(objGetWsCommand.ResultTable);
                lblcreatedby.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["CreatedBY"]);
                string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(objGetWsCommand.ResultTable.Rows[0]["UPDATED_ON"]));
                lblupdatedby.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
                lblWsCmd_DataPara.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["HTTP_REQUEST"]);
                string strPara = "";
                DataTable objTagDatatable = ParseWsCommandParam(Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["TAG"]).Trim());
                if (objTagDatatable != null)
                {
                    foreach (DataRow dr in objTagDatatable.Rows)
                    {
                        if (strPara.Length > 0) strPara += ",";
                        strPara += Convert.ToString(dr["TAG"]);
                    }
                }

                lblWsCmd_OutPara.Text = strPara;
                if (ddlWsCommad_DataReturned.SelectedValue == "1")
                {
                    strAdditionalScript += "SetWsDatasetPathVisible(true);";
                }
                else
                {

                    strAdditionalScript += "SetWsDatasetPathNotVisible();";
                }
                ScriptManager.RegisterStartupScript(updDelWsCmd, typeof(UpdatePanel), Guid.NewGuid().ToString(), strAdditionalScript, true);
            }

            else if (Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["WEBSERVICE_TYPE"]).Trim() == "rpc")
            {
                EditXmlRpcCommand(objGetWsCommand.ResultTable, out strInParaHtml, out strOutParaHtml);
                lblcreatedby.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["CreatedBY"]);
                string strtime = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(objGetWsCommand.ResultTable.Rows[0]["UPDATED_ON"]));
                lblupdatedby.Text = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["FULL_NAME"]) + " On " + strtime;
                strAdditionalScript += "ViewRpc()";
            }
            else
            {
                hdfWsCommand_CommandId.Value = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["WS_COMMAND_ID"]);
            }
            CatchBind(objGetWsCommand.ResultTable);
            ScriptManager.RegisterStartupScript(updDelWsCmd, typeof(Page), Guid.NewGuid().ToString(), strAdditionalScript, true);

        }

        private string EditWsdlCommand(DataTable _dt)
        {
            hdfWsCommand_CommandId.Value = Convert.ToString(_dt.Rows[0]["WS_COMMAND_ID"]);
            ddl_WsConnactor.SelectedIndex = ddl_WsConnactor.Items.IndexOf(ddl_WsConnactor.Items.FindByValue(Convert.ToString(_dt.Rows[0]["WS_CONNECTOR_ID"]).Trim()));
            ddl_WsConnactor_SelectedIndexChanged(null, null);
            ddlWsConnServices.SelectedIndex = ddlWsConnServices.Items.IndexOf(ddlWsConnServices.Items.FindByValue(Convert.ToString(_dt.Rows[0]["SERVICE"]).Trim()));
            ddlWsConnServices_SelectedIndexChanged(null, null);
            ShowOperationOnServiceChange(ddlWsConnServices.SelectedValue + "Soap");
            ddl_WsConnectorMethod.SelectedIndex = ddl_WsConnectorMethod.Items.IndexOf(ddl_WsConnectorMethod.Items.FindByValue(Convert.ToString(_dt.Rows[0]["METHOD"]).Trim()));
            ddlWsdlCommad_DataReturned.SelectedIndex = ddlWsdlCommad_DataReturned.Items.IndexOf(ddlWsdlCommad_DataReturned.Items.FindByValue(Convert.ToString(_dt.Rows[0]["RETURN_TYPE"]).Trim()));
            txtWsdlCommand_DatasetPath.Text = Convert.ToString(_dt.Rows[0]["DATASET_PATH"]);
            txtWsdl_CmdName.Text = Convert.ToString(_dt.Rows[0]["WS_COMMAND_NAME"]).Trim();
            lblWsCmd_ServiceType.Text = "WSDL";
            if (ddlWsConnServices.Items.FindByValue(Convert.ToString(_dt.Rows[0]["SERVICE"]).Trim()) != null)
                lblWsCmd_WeSrc.Text = ddlWsConnServices.Items.FindByValue(Convert.ToString(_dt.Rows[0]["SERVICE"]).Trim()).Text;
            else lblWsCmd_WeSrc.Text = "Undefined";

            if (ddl_WsConnectorMethod.Items.FindByValue(Convert.ToString(_dt.Rows[0]["METHOD"]).Trim()) != null)
                lblWsCmd_Method.Text = ddl_WsConnectorMethod.Items.FindByValue(Convert.ToString(_dt.Rows[0]["METHOD"]).Trim()).Text;
            else lblWsCmd_Method.Text = "Undefined";

            txtWsdlCmdDesc.Text = Convert.ToString(_dt.Rows[0]["DESCRIPTION"]).Trim();
            hdfWsCmdPara.Value = ReplaceSingleCourseAndBackSlash(Convert.ToString(_dt.Rows[0]["TAG"]));
            lblWsCmdDesc.Text = Convert.ToString(_dt.Rows[0]["DESCRIPTION"]).Trim();
            txtWsdlCmdDesc.Text = Convert.ToString(_dt.Rows[0]["DESCRIPTION"]).Trim();
            WsWsdlCmdDetailsDiv.Visible = true;
            WsHttpCmdDetailsDiv.Visible = false;
            return ("WsParaHash['0'] = ['" + (Convert.ToString(_dt.Rows[0]["TAG"]).Trim().Length == 0 ? "[]" : ReplaceSingleCourseAndBackSlash(Convert.ToString(_dt.Rows[0]["TAG"])))
                                 + "'];WsdlOutParamHideShow(); wsdlMethodValue ='" + ddl_WsConnectorMethod.SelectedValue
                                 + "';WsdlEditCmdParam();$('[id$=hdfWsCmdPara]').val('" + ReplaceSingleCourseAndBackSlash(Convert.ToString(_dt.Rows[0]["TAG"])) + "');"
                                 + "AppActiveCtrl['CmdType']='0';$('[id$=ddlWsdlCommad_DataReturned]').change();IntWsdlDsPath();");
        }

        private string EditHttpCommand(DataTable _dt)
        {
            hdfWsCommand_CommandId.Value = Convert.ToString(_dt.Rows[0]["WS_COMMAND_ID"]);

            ddl_WsConnactor.SelectedIndex = ddl_WsConnactor.Items.IndexOf(ddl_WsConnactor.Items.FindByValue(Convert.ToString(_dt.Rows[0]["WS_CONNECTOR_ID"]).Trim()));
            ddl_WsConnactor_SelectedIndexChanged(null, null);

            txtWsdl_CmdName.Text = Convert.ToString(_dt.Rows[0]["WS_COMMAND_NAME"]);
            if (Convert.ToInt32(_dt.Rows[0]["HTTP_TYPE"]) == 1)
            {
                rdbWsHttpGet.Checked = true;
                rdbWsHttpPost.Checked = false;
            }
            else
            {
                rdbWsHttpPost.Checked = true;
                rdbWsHttpGet.Checked = false;
            }
            txtWsCommand_DatasetPath.Text = Convert.ToString(_dt.Rows[0]["DATASET_PATH"]);
            ddlWsCommad_DataReturned.SelectedIndex = ddlWsCommad_DataReturned.Items.IndexOf(ddlWsCommad_DataReturned.Items.FindByValue(Convert.ToString(_dt.Rows[0]["RETURN_TYPE"]).Trim()));
            txtWsCommand_Url.Text = Convert.ToString(_dt.Rows[0]["URL"]);
            hdfWsCmdPara.Value = ReplaceSingleCourseAndBackSlash(Convert.ToString(_dt.Rows[0]["TAG"]));
            lblWsCmd_Name.Text = hidwebdatasource.Value;
            lblWsCmd_ServiceType.Text = "HTTP";
            lblWsCmd_InHost.Text = lblUrl.Text;
            lblWsCmd_InPara.Text = Convert.ToString(_dt.Rows[0]["URL"]).Trim();
            lblWsCmd_DataSetPath.Text = Convert.ToString(_dt.Rows[0]["DATASET_PATH"]).Trim();
            lblWsCmd_OutPara.Text = "";
            lblWsCmdDesc.Text = Convert.ToString(_dt.Rows[0]["DESCRIPTION"]).Trim();
            txtWsdlCmdDesc.Text = Convert.ToString(_dt.Rows[0]["DESCRIPTION"]).Trim();
            txthttpdatatemplate.Text = Convert.ToString(_dt.Rows[0]["HTTP_REQUEST"]).Trim();
            string strPara = "";
            DataTable objTagDatatable = ParseWsCommandParam(Convert.ToString(_dt.Rows[0]["TAG"]).Trim());
            if (objTagDatatable != null)
            {
                foreach (DataRow dr in objTagDatatable.Rows)
                {
                    if (strPara.Length > 0) strPara += ",";
                    strPara += Convert.ToString(dr["TAG"]);
                }
            }
            string headers = Convert.ToString(_dt.Rows[0]["Headers"]).Trim();
            if (headers.Length > 0)
            {
                JArray jaHeaders = JArray.Parse(headers);
                ddlContantType.SelectedIndex = 1;
                foreach (JArray joHeader in jaHeaders)
                {
                    switch (joHeader[0].ToString())
                    {
                        case "ct":
                            ddlContantType.SelectedIndex = ddlContantType.Items.IndexOf(ddlContantType.Items.FindByValue(joHeader[1].ToString()));
                            break;
                    }
                }
            }
           
            lblWsCmd_OutPara.Text = strPara;
            WsWsdlCmdDetailsDiv.Visible = false;
            WsHttpCmdDetailsDiv.Visible = true;
            return ("WsParaHash['0'] = ['" + (Convert.ToString(_dt.Rows[0]["TAG"]).Trim().Length == 0 ? "[]" : ReplaceSingleCourseAndBackSlash(Convert.ToString(_dt.Rows[0]["TAG"])))
                                     + "'];$('[id$=hdfWsCmdPara]').val('" + ReplaceSingleCourseAndBackSlash(Convert.ToString(_dt.Rows[0]["TAG"])) + "');AppActiveCtrl['CmdType']='1';");
        }

        private void EditXmlRpcCommand(DataTable _dt, out string _InParaHtml, out string _OutParaHtml)
        {
            string strParaDetails = "";
            _InParaHtml = "";
            _OutParaHtml = "";
            hdfWsCommand_CommandId.Value = Convert.ToString(_dt.Rows[0]["WS_COMMAND_ID"]);
            ddl_WsConnactor.SelectedIndex = ddl_WsConnactor.Items.IndexOf(ddl_WsConnactor.Items.FindByValue(Convert.ToString(_dt.Rows[0]["WS_CONNECTOR_ID"]).Trim()));
            ddl_WsConnactor_SelectedIndexChanged(null, null);
            txtWsdl_CmdName.Text = Convert.ToString(_dt.Rows[0]["WS_COMMAND_NAME"]);
            hdfRpcParam.Value = Convert.ToString(_dt.Rows[0]["WS_COMMAND_NAME"]);
            hdfOutRpcParam.Value = Convert.ToString(_dt.Rows[0]["WS_COMMAND_NAME"]);
            txtRpc_MethodName.Text = Convert.ToString(_dt.Rows[0]["METHOD"]);
            _InParaHtml = ParseRpcParaJson(Convert.ToString(_dt.Rows[0]["PARAMETER"]), "In", out strParaDetails);
            lblWsCmd_Inputpara.Text = strParaDetails;
            strParaDetails = "";
            _OutParaHtml = ParseRpcParaJson(Convert.ToString(_dt.Rows[0]["TAG"]), "Out", out strParaDetails);
            lblWsCmd_Outputpara.Text = strParaDetails;
            _InParaHtml += "$('[id$=hdfRpcParam]').val('" + Convert.ToString(_dt.Rows[0]["PARAMETER"]) + "');";
            _OutParaHtml += "$('[id$=hdfOutRpcParam]').val('" + Convert.ToString(_dt.Rows[0]["TAG"]) + "');";

            WsWsdlCmdDetailsDiv.Visible = false;
            WsHttpCmdDetailsDiv.Visible = false;
            lblWsCmd_Name.Text = hidwebdatasource.Value;
            lblWsCmd_ServiceType.Text = "XML-RPC";
            txtWsdlCmdDesc.Text = Convert.ToString(_dt.Rows[0]["DESCRIPTION"]).Trim();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), _InParaHtml + _OutParaHtml, true);
        }
        private void CatchBind(DataTable dt)
        {
            dd1DataCatch.SelectedValue = Convert.ToString(dt.Rows[0]["CACHE"]);
            lblWsCmdDesc.Text = Convert.ToString(dt.Rows[0]["DESCRIPTION"]);
            string strcatchtype = Convert.ToString(dt.Rows[0]["CACHE"]);

            if (strcatchtype == "1")
            {
                dd1AbMonth.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_FREQUENCY"]);

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"catchtype('" + strcatchtype + "'), absolute('" + dd1AbMonth.SelectedValue + "')", true);
                if (dd1AbMonth.SelectedValue == "0")
                {
                    dd1AbHr.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else if (dd1AbMonth.SelectedValue == "1")
                {
                    string strexp = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                    string[] strhhmm = strexp.Split(':');
                    dd1Abhh.SelectedValue = strhhmm[0];
                    ddlAbMM.SelectedValue = strhhmm[1];
                }
                else if (dd1AbMonth.SelectedValue == "2")
                {
                    ddlAbDay.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else if (dd1AbMonth.SelectedValue == "3")
                {
                    dd1AbMnDay.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else if (dd1AbMonth.SelectedValue == "4")
                {
                    string strexpyear = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                    string[] strhhmm = strexpyear.Split('/');
                    dd1AbMnthDay.SelectedValue = strhhmm[0];
                    ddlAbMnth.SelectedValue = strhhmm[1];

                }
                else
                {
                    dd1AbMnthDay.SelectedValue = "-1";
                    dd1AbMnDay.SelectedValue = "-1";
                    ddlAbDay.SelectedValue = "-1";
                    dd1Abhh.SelectedValue = "0";
                    ddlAbMM.SelectedValue = "0";
                    dd1AbHr.SelectedValue = "-1";
                }
            }
            else if (strcatchtype == "2")
            {
                ddlRlt.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_FREQUENCY"]);
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"catchtype(" + strcatchtype + "), relativetcatch('" + ddlRlt.SelectedValue + "')", true);
                if (ddlRlt.SelectedValue == "1")
                {
                    Drprlthour.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else if (ddlRlt.SelectedValue == "2")
                {
                    rltdayd.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else if (ddlRlt.SelectedValue == "3")
                {
                    raltweeks.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else if (ddlRlt.SelectedValue == "0")
                {
                    drprltmin.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
                else
                {
                    drprltmonth.SelectedValue = Convert.ToString(dt.Rows[0]["EXPIRY_CONDITION"]);
                }
            }
        }

        #endregion
        #region RPC Create Table And parameter Calculation
        private string ParseRpcParaJson(string _Json, string _ParaType, out string _ParaDetails)
        {
            _ParaDetails = "";
            string strParaDetail = "", strInStructParaDetail = "", strInArrayParaDetail = "";
            if (_Json.Trim().Length == 0) return "";
            XmlRpcParser obj = new XmlRpcParser();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(_Json));
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = serializer.ReadObject(ms) as XmlRpcParser;
            ms.Close();

            string strMarginLeft = "";
            string strLevel = "0";
            string strHtml = "";
            string ItemType = "";
            string strHeaderHtml = "";

            foreach (param paramItem in obj.param)
            {
                ItemType = GetRpcParamType(paramItem.typ.Trim());
                if (ItemType != "struct" && ItemType.Split('-')[0] != "array")
                {
                    strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').append('<div id=\"" + _ParaType + "Para_" + paramItem.name + "\" class=\"rpcContText\" " + strMarginLeft + ">" +
                                                                                    paramItem.name +
                                                                              "</div>" +
                                                                              "<a id=\"aLevel" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\"  type=\"level\">" + strLevel + "</a>" +
                                                                              "<a id=\"aPre" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\"></a>" +
                                                                              "<div class=\"clear\"></div>');"
                             + "$('#rpc" + _ParaType + "ParaContDiv2').append('<div id=\"" + _ParaType + "PType_" + paramItem.name + "\"  class=\"rpcContType\">" +
                                                                                    "<div class=\"FLeft\">" + ItemType + "</div>" +
                                                                                    "<div class=\"rpcContDelete\"><img id=\"Img_" + _ParaType + "_" + paramItem.name + "\" src=\"//enterprise.mficient.com/images/cross.png\" alt=\"Delete\" onclick=\"DeleteRpcParamClick(this);\" /></div>" +
                                                                               "</div>" +
                                                                               "<div id=\"SepType" + _ParaType + "PType_" + paramItem.name + "\" class=\"clear\" class=\"clear\"></div>');";
                    if (strParaDetail.Length > 0)
                        strParaDetail += ",";
                    else { }
                    strParaDetail += paramItem.name;
                }
                else if (ItemType.Split('-')[0] == "struct")
                {
                    strInStructParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, "", "struct", out strInStructParaDetail);
                    var test = strInStructParaDetail;
                }
                else if (ItemType.Split('-')[0] == "array")
                {
                    strInArrayParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, "", "array", out strInArrayParaDetail);
                }
                strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').show();$('#rpc" + _ParaType + "ParaContDiv2').show();";
            }
            if (strParaDetail.Length > 0)
            {
                if (strInStructParaDetail.Length > 0)
                    strParaDetail += ",";
                else { }
            }
            else { }
            strParaDetail += strInStructParaDetail;
            if (strInArrayParaDetail.Length > 0 && strParaDetail.Length > 0)
                strParaDetail += ",";
            else { }
            strParaDetail += strInArrayParaDetail;
            _ParaDetails = strParaDetail;
            return strHtml + strHeaderHtml;
        }
        private string GetRpcParaHtmlForStruct(string _StructName, param _Inpara, string _ParaType, string _Level, string _ParentPara, string _ParentType, out string _ParaDetails)
        {

            string strParaDetail = "", strInnerParaDetail = "", strInnerStructParaDetail = "", strInArrayParaDetail = "";
            string strLevel = (Convert.ToInt32(_Level) + 1).ToString();
            string strHtml = "";
            string ItemType = "";
            string strMarginLeft = "style=\"margin-left:" + (Convert.ToInt32(strLevel) * 20).ToString() + "px;\"";
            strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').append('<div id=\"" + _ParaType + "Para_" + _StructName + "\" class=\"rpcContText\" " + "style=\"margin-left:" + (Convert.ToInt32(_Level) * 20).ToString() + "px;\"" + ">" +
                                                                            _StructName +
                                                                      "</div>" +
                                                                      "<a id=\"aLevel" + _ParaType + "Para_" + _StructName + "\" class=\"hide\"  type=\"level\">" + _Level + "</a>" +
                                                                      "<a id=\"aPre" + _ParaType + "Para_" + _StructName + "\" class=\"hide\">" + _ParentPara + "</a>" +
                                                                      "<div class=\"clear\"></div>');"
                     + "$('#rpc" + _ParaType + "ParaContDiv2').append('<div id=\"" + _ParaType + "PType_" + _StructName + "\"  class=\"rpcContType\">" +
                                                                            "<div class=\"FLeft\">" + _ParentType + "</div>" +
                                                                            "<div class=\"rpcContDelete\"><img id=\"Img_" + _ParaType + "_" + _StructName + "\" src=\"//enterprise.mficient.com/images/cross.png\" alt=\"Delete\" onclick=\"DeleteRpcParamClick(this);\" /></div>" +
                                                                            "<div class=\"rpcContAdd\"><img id=\"ImgAdd_" + _ParaType + "_" + _StructName + "\" src=\"//enterprise.mficient.com/images/add.png\" alt=\"Add\"  onclick=\"AddRpcParamClick(this);\" /></div>" +
                                                                      "</div>" +
                                                                      "<div id=\"SepType" + _ParaType + "PType_" + _StructName + "\" class=\"clear\"></div>');";

            if (strParaDetail.Length > 0)
                strParaDetail += ",";
            strParaDetail += _StructName + "(";
            foreach (param paramItem in _Inpara.inparam)
            {
                ItemType = GetRpcParamType(paramItem.typ.Trim());
                if (ItemType != "struct" && ItemType != "array")
                {
                    strHtml += "$('#rpc" + _ParaType + "ParaContDiv1').append('<div id=\"" + _ParaType + "Para_" + paramItem.name + "\" class=\"rpcContText\" " + strMarginLeft + ">" +
                                                                                paramItem.name +
                                                                             "</div>" +
                                                                             "<a id=\"aLevel" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\"  type=\"level\">" + strLevel + "</a>" +
                                                                             "<a id=\"aPre" + _ParaType + "Para_" + paramItem.name + "\" class=\"hide\">" + _StructName + "</a>" +
                                                                             "<div class=\"clear\"></div>');"
                            + "$('#rpc" + _ParaType + "ParaContDiv2').append('<div id=\"" + _ParaType + "PType_" + paramItem.name + "\"  class=\"rpcContType\">" +
                                                                                    "<div class=\"FLeft\">" + ItemType + "</div>" +
                                                                                    "<div class=\"rpcContDelete\"><img id=\"Img_" + _ParaType + "_" + paramItem.name + "\" src=\"//enterprise.mficient.com/images/cross.png\" alt=\"Delete\" onclick=\"DeleteRpcParamClick(this);\" /></div>" +

                                                                            "</div>" +
                                                                            "<div id=\"SepType" + _ParaType + "PType_" + paramItem.name + "\" class=\"clear\"></div>');";

                    if (strInnerParaDetail.Length > 0)
                        strInnerParaDetail += ",";
                    else { }
                    strInnerParaDetail += paramItem.name;
                }
                else if (ItemType.Split('-')[0] == "struct")
                {
                    strInnerStructParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, _StructName, "struct", out strInnerStructParaDetail);
                }
                else if (ItemType.Split('-')[0] == "array")
                {
                    strInArrayParaDetail = "";
                    strHtml += GetRpcParaHtmlForStruct(paramItem.name, paramItem, _ParaType, strLevel, _StructName, "array", out strInArrayParaDetail);
                }
            }
            strParaDetail += strInnerParaDetail;
            if (strInnerParaDetail.Trim().Length > 0)
                if (strInnerStructParaDetail.Length > 0)
                    strParaDetail += ",";
            strParaDetail += strInnerStructParaDetail;
            if (strInnerStructParaDetail.Length > 0 || strInnerParaDetail.Length > 0)
                if (strInArrayParaDetail.Length > 0)
                    strParaDetail += ",";
            strParaDetail += strInArrayParaDetail;
            strParaDetail += ")";
            _ParaDetails = strParaDetail;
            return strHtml;
        }
        private DataTable ParseWsCommandParam(string _Json)
        {
            DataTable objTagDatatable = new DataTable();
            DataColumn dc1 = new DataColumn("TAG");
            objTagDatatable.Columns.Add(dc1);
            try
            {
                List<IdeWsParam> obj = new List<IdeWsParam>();
                MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(_Json));
                System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
                obj = serializer.ReadObject(ms) as List<IdeWsParam>;
                foreach (IdeWsParam param in obj)
                {
                    objTagDatatable.Rows.Add(param.name.Trim());
                }
                return objTagDatatable;
            }
            catch
            {
                return null;
            }

        }
        private string ReplaceSingleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceSingleCourse(ReplaceBackSlash(_Input));
            }
        }
        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"\\", @"\\");
            }
        }
        private string ReplaceSingleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"'", @"\'");
            }
        }
        private string GetRpcParamType(string _Type)
        {
            var CurrType = "";
            switch (_Type)
            {
                case "0":
                    CurrType = "i4";
                    break;
                case "1":
                    CurrType = "base64";
                    break;
                case "2":
                    CurrType = "boolean";
                    break;
                case "3":
                    CurrType = "date/time";
                    break;
                case "4":
                    CurrType = "double";
                    break;
                case "5":
                    CurrType = "integer";
                    break;
                case "6":
                    CurrType = "string";
                    break;
                case "7":
                    CurrType = "struct";
                    break;
                case "8":
                    CurrType = "array";
                    break;
            }
            return CurrType;
        }

        #endregion
        #region AddBtnClick

        protected void lnkadd_Click(object sender, EventArgs e)
        {
            resetdivs();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "intializewsobject();Enabletextwsobject();", true);
        }
        protected void lnkAddNewDbCommand_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CancelBtnClick();", true);
        }

        #endregion

        #region Boolvalidation

        protected Boolean WSValidateCopyObject()
        {
            string strMessage = "";
            if (txtwscopyobject.Text.Trim().Length == 0)
            {
                strMessage = "* Please enter object Name.";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strMessage + "');", true);

                return true;
            }
            else if (txtwscopyobject.Text.Trim().Length < 3)
            {
                strMessage = "* Object name cannot be less than 3 charecters";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strMessage + "');", true);
                return true;
            }
            else if (txtwscopyobject.Text.Trim().Length > 50)
            {
                strMessage = "* Object name cannot be more than 50 charecters";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strMessage + "');", true);
                return true;
            }
            return false;
        }
        private Boolean WebserviceWsdlCommandValidation(UpdatePanel _Upd)
        {
            string strMessage = "";
            if (ddl_WsConnactor.SelectedValue == "-1")
                strMessage += "Please select web service connector.<br />";
            if (txtWsdl_CmdName.Text.Trim().Length == 0)
                strMessage += "Please enter object name.<br />";
            if (ddlWsConnServices.SelectedValue == "-1")
                strMessage += "Please select service.<br />";
            if (ddl_WsConnectorMethod.SelectedValue == "-1")
                strMessage += "Please select web service method.<br />";
            if (ddlWsdlCommad_DataReturned.SelectedValue == "1")
            {
                if (txtWsdlCommand_DatasetPath.Text.Length == 0)
                    strMessage += "* Please enter dataset path.<br />";
            }
            if (strMessage.Length > 0)
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strMessage + "');", true);
                return false;
            }
            return true;
        }
        private Boolean WebserviceHttpCommandValidation(UpdatePanel _Upd)
        {
            Boolean bolIsValid = true;
            string strMessage = "";

            if (ddl_WsConnactor.SelectedValue == "-1")
            {
                strMessage += " * Please select connector.</br>";
            }
            if (txtWsdl_CmdName.Text.Length == 0)
            {
                strMessage += "* Please enter object name.<br />";
            }
            if (ddlWsCommad_DataReturned.SelectedValue == "1")
            {
                if (txtWsCommand_DatasetPath.Text.Length == 0)
                    strMessage += "* Please enter dataset path.<br />";
            }
            if (strMessage.Length > 0)
            {
                bolIsValid = false;
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strMessage + "');", true);
            }
            return bolIsValid;
        }

        #endregion
        #region copy object
        protected void WSCommandCopy_Save_Click(object sender, EventArgs e)
        {

            if (WSValidateCopyObject())
            {
                return;
            }
            else
            {
                try
                {
                    SaveWsCopyCommand(hdfWsCommand_CommandId.Value, txtwscopyobject.Text);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
                }

            }

        }

        private void SaveWsCopyCommand(string DbCommandId, string COMMAND_NAME)
        {
            GetWsCommand objGetDbCommand = new GetWsCommand(DbCommandId, COMMAND_NAME);
            if (objGetDbCommand.StatusCode == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Web object Name already exist.');", true);
                return;
            }
            else
            {
                AddNewWsCommand objAddNewWsCommand = new AddNewWsCommand(this.SubAdminid, COMMAND_NAME, DbCommandId, this.CompanyId, Convert.ToInt64(DateTime.UtcNow.Ticks));
                if (objAddNewWsCommand.StatusCode == 0)
                {
                    ClearAfterAdd();
                    Reset();
                    txtwscopyobject.Text = "";
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('');$('#aCfmMessage').html('Web service object saved successfully.');SubProcConfirmBoxMessageWo(true,'Saved',250);openwscopyobject(false);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#a2').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
        }

        protected void btnSaveDb_Click(object sender, EventArgs e)
        {
            if (hiddiscardcommadtype.Value == "5")
            {
                SaveDicardcommand();
                if (hiddiscardsuccess.Value != "")
                {
                    hideditdone.Value = "";
                    hiddiscardsuccess.Value = "";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "SubModifiction(false);OnloadWSObjectData();discardedit();", true);
                }


            }
            else if (hiddiscardcommadtype.Value == "2")
            {
                SaveDicardcommand();
                if (hiddiscardsuccess.Value != "")
                {
                    hideditdone.Value = "";
                    hiddiscardcommadtype.Value = "";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "SubModifiction(false);BindDatabaseObjectdata();", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "SubModifiction(false);", true);
            }

        }
        #endregion


        protected void savebtn_Click(object sender, EventArgs e)
        {

            GetWebserviceConnection objGetWebserviceConnection = new GetWebserviceConnection(false, this.SubAdminid, "", hidwsconnectorid.Value, this.CompanyId);
            objGetWebserviceConnection.Process2();
            if (objGetWebserviceConnection != null)
            {
                string CreadentialCheck = "";
                CreadentialCheck = Convert.ToString(objGetWebserviceConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim();
                if (CreadentialCheck != "" && CreadentialCheck != "-1")
                {

                    JObject crdtial = getCradential(CreadentialCheck);
                    if (crdtial != null)
                    {
                        if (Convert.ToString(crdtial["uid"]) != "" && Convert.ToString(crdtial["pwd"]) != "")
                        {
                            ShowTestResult(Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]));
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Invalid credential.');", true);
                    }
                }

                else
                {
                    ShowTestResult("", "");
                }


            }
        }
        protected void btnSaveCredential_Click(object sender, EventArgs e)
        {

            if (ShowTestResult(txtUsername.Text.Trim(), txtPassword.Text))
            {
                saveCredentialInHidden(ltCrd.Value);
            };
        }

        void saveCredentialInHidden(string credentialproperty)
        {
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == credentialproperty)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(crd["uid"])))
                        {
                            crd["uid"] = txtUsername.Text.Trim();
                            crd["pwd"] = txtPassword.Text;
                            break;
                        }
                    }
                hidWsAuthencationMeta.Value = crds.ToString();
            }
        }

        bool ShowTestResult(string _unm, string _pwd)
        {
            bool flag = false;
            string ctype = hdfEditWsType.Value == "1" ? "3" : (hdfEditWsType.Value == "0" ? "2" : "4");
            string outError="";
            string st = Utilities.testObjectRequest(this.CompanyId, hdfWsCommand_CommandId.Value, ctype, (
                (hdflp.Value.Length <= 0) ? new JArray() : JArray.Parse(hdflp.Value))
                , AesEncryption.AESEncrypt(this.CompanyId, _unm), AesEncryption.AESEncrypt(this.CompanyId, _pwd),out outError);
            if (st.Length > 0)
            {
                JObject dataObject = JObject.Parse(st);
                JObject dataObjectresp = JObject.Parse(dataObject["resp"].ToString());
                JObject dataObjectRespStatus = JObject.Parse(dataObjectresp["status"].ToString());

                int statuscode = Convert.ToInt32(dataObjectRespStatus["cd"].ToString());

                if (statuscode == 0)
                {
                    flag = true;
                    JObject dataObjectRespData = JObject.Parse(dataObjectresp["data"].ToString());
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CreateDynamicTable(" + dataObjectRespData["dt"].ToString() + ");", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"opendialogtest(false);SubProcBoxMessageDataConection(true,'Error');$('#aMessage').html('" + dataObjectRespStatus["desc"].ToString() + "(" + statuscode.ToString() + ")" + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"opendialogtest(false);SubProcBoxMessageDataConection(true,'Error');$('#aMessage').html('Http Error Code :" + outError + "');", true);
            }
            return flag;
        }

        JObject getCradential(string _credentialproperty)
        {
            ltCrd.Value = _credentialproperty;
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == _credentialproperty)
                        return crd;
            }
            return null;
        }




    }
}
