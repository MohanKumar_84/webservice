﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Data;
using Microsoft.Data.Edm;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Net;

namespace mFicientCP
{
    public partial class ODataObjects : System.Web.UI.Page
    {
        #region Private Members

        private string strhfs, strhfbid, subAdminId = "", companyId = "", sessionId = "", adminId = "", strPostbackPageMode = "";
        private static ODATA _ODATA = null;
        private static IEdmEntitySet _EdmEntitySet = null;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                strPostbackPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;
                strPostbackPageMode = ((HiddenField)previousPage.Master.FindControl("hidAppNewOrEdit")).Value;

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid))
            {
                if (String.IsNullOrEmpty(strhfs))
                {
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect(@"Default.aspx");
                    return;
                }
                return;
            }
            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            Utilities.SaveInContext(context, strhfs, strhfbid, out subAdminId, out sessionId, out companyId, out adminId);

            //if (!string.IsNullOrEmpty(strhfs))
            //{
            //    string str = Utilities.DecryptString(strhfs);
            //    subAdminId = str.Split(',')[0];
            //    companyId = str.Split(',')[3].ToLower();
            //    sessionId = str.Split(',')[1];
            //    adminId = str.Split(',')[2];
            //}
            if (!IsPostBack == true)
            {
                IdeOdataDropDown();
                bindDataCacheDropdowns();
                bindCreadential();
            }
            if (!IsPostBack || (IsPostBack && !string.IsNullOrEmpty(strPostbackPageMode)))
            {
                bindODataObjects();
            }
        }


        protected void bindCreadential()
        {
            hidWsAuthencationMeta.Value = "";
            GetEnterpriseAdditionalDefinition objAuthMeta = new GetEnterpriseAdditionalDefinition(this.companyId);
            if (objAuthMeta.StatusCode == 0)
            {
                if (objAuthMeta.StatusCode == 0 && objAuthMeta.ResultTable.Rows.Count > 0)
                {
                    string strAuthentication = Convert.ToString(objAuthMeta.ResultTable.Rows[0]["AUTHENTICATION_META"]);
                    if (!string.IsNullOrEmpty(strAuthentication))
                    {
                        JArray lst = JArray.Parse(strAuthentication);
                        if (lst.Count != 0)
                            foreach (JObject item in lst)
                            {
                                if (Convert.ToString(item["uid"]) != "")
                                    item["uid"] = AesEncryption.AESDecrypt(companyId, Convert.ToString(item["uid"]));
                                if (Convert.ToString(item["pwd"]) != "")
                                    item["pwd"] = AesEncryption.AESDecrypt(companyId, Convert.ToString(item["pwd"]));
                            }
                        hidWsAuthencationMeta.Value = lst.ToString();
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Internal Server Error" + "');", true);
            }
        }


        protected void ddlOdataCmd_Conn_Changed(object sender, EventArgs e)
        {
            imgODataConnectorInfo.ToolTip = "";
            string strUrl = "", strVersion = "";
            ddlOdataCmd_EntType.SelectedIndex = 0;

            GetODataSrviceConnector obj = new GetODataSrviceConnector(false, subAdminId, ddlOdataCmd_Conn.SelectedValue, "", companyId);
            if (obj.StatusCode == 0)
            {
                ODATA objOdata = new ODATA(Convert.ToString(obj.ResultTable.Rows[0]["ODATA_CONTENT"]));
                strUrl = Convert.ToString(obj.ResultTable.Rows[0]["ODATA_ENDPOINT"]);
                strVersion = (Convert.ToString(obj.ResultTable.Rows[0]["VERSION"])).Trim();
                imgODataConnectorInfo.ToolTip = strUrl + @" (" + strVersion + @")";
                _ODATA = objOdata;
                ddlOdataCmd_EntityType.Items.Clear();
                ddlOdataCmd_funcImport.Items.Clear();
                ddlOdataCmd_EntityType.Items.Add(new ListItem("Select Entity Type", "-1"));
                ddlOdataCmd_funcImport.Items.Add(new ListItem("Select Function Import", "-1"));
                foreach (IEdmEntityContainer EntCont in objOdata.EntityContainers)
                {
                    foreach (IEdmEntitySet EntSet in objOdata.GetEntitySets(EntCont))
                    {
                        ddlOdataCmd_EntityType.Items.Add(new ListItem(EntSet.Name, EntSet.Name));
                    }

                    foreach (IEdmFunctionImport func in objOdata.GetFunctionImports(EntCont))
                    {
                        ddlOdataCmd_funcImport.Items.Add(new ListItem(func.Name, func.Name));
                    }

                }
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(),
                                              @"initializeConChangedControls();setDefaultDisplay();UniformControlDesign();OdataCmdGlobalVar.ServiceEndpoint='" + ReplaceSingleCourseAndBackSlash(strUrl) + "';OdataCmdGlobalVar.ServiceVersion='" + GetFormatedODataVersion(strVersion) + "';", true);
        }

        protected void ddlOdataCmd_EntityType_Changed(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbKeys = new StringBuilder();

            foreach (IEdmEntityContainer EntCont in _ODATA.EntityContainers)
            {

                foreach (IEdmEntitySet EntSet in _ODATA.GetEntitySets(EntCont))

                    if (EntSet.Name == ddlOdataCmd_EntityType.SelectedValue)
                    {
                        _EdmEntitySet = EntSet;
                        IEdmEntityType objEdmEntityType = (IEdmEntityType)EntSet.ElementType;
                        IEnumerable<IEdmStructuralProperty> obj = _ODATA.GetStructuralProperties(EntSet.ElementType);
                        foreach (IEdmStructuralProperty prop in obj)
                        {
                            sb.Append(sb.ToString().Length > 0 ? "," + prop.Name : prop.Name);
                        }

                        IEnumerable<IEdmStructuralProperty> objEdmEntityTypeKeys = _ODATA.GetEntityTypeKey(objEdmEntityType);
                        foreach (IEdmStructuralProperty prop in objEdmEntityTypeKeys)
                        {
                            sbKeys.Append(sbKeys.ToString().Length > 0 ? "," + prop.Name : prop.Name);
                        }
                    }
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "UniformControlDesign();OdataCmdGlobalVar.StructuralProp='" + ReplaceSingleCourseAndBackSlash(sb.ToString()) + "';OdataCmdGlobalVar.SelectedEntityTypeKeys='" + ReplaceSingleCourseAndBackSlash(sbKeys.ToString()) + "';entityTypeChanged();", true);
        }

        protected void ddlOdataCmd_UsingKey_Changed(object sender, EventArgs e)
        {
            string strScripts = "";
            if (sender != null && e != null)
            {
                if (Convert.ToInt32(ddlOdataCmd_Ent_HttpTyp.SelectedValue) > 1 || (Convert.ToInt32(ddlOdataCmd_Ent_HttpTyp.SelectedValue) == 1 && Convert.ToInt32(ddlOdataCmd_UsingKey.SelectedValue) == 1))
                {
                    StringBuilder sb = new StringBuilder();
                    string property = "";
                    foreach (IEdmEntityContainer EntCont in _ODATA.EntityContainers)
                    {
                        foreach (IEdmEntitySet EntSet in _ODATA.GetEntitySets(EntCont))
                        {
                            if (EntSet.Name == ddlOdataCmd_EntityType.SelectedValue)
                            {
                                _EdmEntitySet = EntSet;
                                IEdmEntityType objEdmEntityType = (IEdmEntityType)EntSet.ElementType;
                                IEnumerable<IEdmStructuralProperty> objEdmEntityTypeKeys = _ODATA.GetEntityTypeKey(objEdmEntityType);
                                foreach (IEdmStructuralProperty prop in objEdmEntityTypeKeys)
                                {
                                    property = "";
                                    EdmTypeKind propKind = EdmTypeKind.Entity;
                                    property = @"{ ""para"" : """ + prop.Name + @""" , ""typ"" : """ + getDataType(_ODATA.GetPropertyType(prop, out propKind)) + @""" }";
                                    sb.Append(sb.ToString().Length > 0 ? "," + property : "[" + property);
                                }

                                sb.Append("]");
                            }
                        }
                    }
                    strScripts = "setInputPara(" + ReplaceSingleCourseAndBackSlash(sb.ToString()) + ");";
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), strScripts + "selectUsingKeyChanged();UniformControlDesign();", true);
            }
        }

        protected void ddlOdataCmd_funcImport_Changed(object sender, EventArgs e)
        {
            ddlOdataCmd_EntityType.SelectedIndex = ddlOdataCmd_EntityType.Items.IndexOf(ddlOdataCmd_EntityType.Items.FindByValue("-1"));
            ddlOdataCmd_EntityType_Changed(null, null);

            string strParam = "", strHttpType = "", strStrutProp = "";
            FunctionType FuncType;
            EdmTypeKind objKind = EdmTypeKind.None;

            FuncType = 0;
            StringBuilder sb = new StringBuilder();
            string property = "";
            foreach (IEdmEntityContainer EntCont in _ODATA.EntityContainers)
            {

                foreach (IEdmFunctionImport func in _ODATA.GetFunctionImports(EntCont))
                {
                    if (func.Name == ddlOdataCmd_funcImport.SelectedValue)
                    {

                        strHttpType = _ODATA.GetHttpMethodType(func, out FuncType);
                        foreach (IEdmFunctionParameter param in func.Parameters)
                        {
                            strParam += (strParam.Length > 0 ? "," : "") + param.Name;

                            property = "";
                            property = @"{ ""para"" : """ + param.Name + @""" , ""typ"" : """ + getDataType(param.Type.FullName()) + @""" }";
                            sb.Append(sb.ToString().Length > 0 ? "," + property : "[" + property);
                        }

                        sb.Append("]");
                        IEnumerable<IEdmStructuralProperty> objStructProp;
                        string strReturn = _ODATA.GetFunctionReturnType(func, out objKind, out objStructProp);
                        if (objStructProp != null)
                        {
                            foreach (IEdmStructuralProperty str in objStructProp)
                            {
                                strStrutProp += (strStrutProp.Length > 0 ? "," : "") + str.Name;
                            }
                        }
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(),
                "UniformControlDesign();OdataCmdGlobalVar.functionInParam='" + ReplaceSingleCourseAndBackSlash(strParam) + "';"
                + "setInputPara(" + ReplaceSingleCourseAndBackSlash(sb.ToString()) + ");OdataCmdGlobalVar.FunctionReturnColl='" + ReplaceSingleCourseAndBackSlash(strStrutProp) + "';"
                + "$('[id$=lblOdataCmd_FuncHType]').html('" + strHttpType + "');"
                + "$('[id$=lblOdataCmd_FuncType]').html('" + (FuncType == 0 ? "" : FuncType.ToString()) + "');functionTypeChanged();", true);
        }

        protected void ddlOdataCmd_Ent_HttpTyp_Changed(object sender, EventArgs e)
        {
            //if (sender != null && e != null)
            //{
            //ddlOdataCmd_EntityType_Changed(null, null);
            if (Convert.ToInt32(ddlOdataCmd_Ent_HttpTyp.SelectedValue) > -1)
            {
                StringBuilder sb = new StringBuilder();
                string property = "";
                foreach (IEdmEntityContainer EntCont in _ODATA.EntityContainers)
                {
                    foreach (IEdmEntitySet EntSet in _ODATA.GetEntitySets(EntCont))
                    {
                        if (EntSet.Name == ddlOdataCmd_EntityType.SelectedValue)
                        {
                            _EdmEntitySet = EntSet;
                            IEdmEntityType objEdmEntityType = (IEdmEntityType)EntSet.ElementType;
                            IEnumerable<IEdmStructuralProperty> obj = _ODATA.GetStructuralProperties(EntSet.ElementType);
                            foreach (IEdmStructuralProperty prop in obj)
                            {
                                property = "";
                                EdmTypeKind propKind = EdmTypeKind.Entity;
                                property = @"{ ""para"" : """ + prop.Name + @""" , ""typ"" : """ + getDataType(_ODATA.GetPropertyType(prop, out propKind)) + @""" }";
                                sb.Append(sb.ToString().Length > 0 ? "," + property : "[" + property);
                            }

                            sb.Append("]");
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "UniformControlDesign();setInputPara(" + ReplaceSingleCourseAndBackSlash(sb.ToString()) + ");httpTypeChanged();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "UniformControlDesign();httpTypeChanged();", true);
            }
            //}
        }

        protected void btnOdataCmd_SaveClick(object sender, EventArgs e)
        {
            try
            {
                save(false);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
            }
        }

        protected void btnSaveCurrentObject_SaveClick(object sender, EventArgs e)
        {
            try
            {
                save(true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
            }
        }

        protected void btnSaveCopiedObject_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(hdfDataObjectId.Value))
                {
                    OdataCommand oDataCmd = new OdataCommand(hdfDataObjectId.Value.Trim(), companyId);
                    oDataCmd.GetCommandDetails();
                    if (oDataCmd != null)
                    {
                        AddOdataCommand objCmd = new AddOdataCommand(companyId, subAdminId, oDataCmd.ConnectorId, txtOdataCopyObjectName.Text.Trim(), Convert.ToInt32(oDataCmd.Resourcetype).ToString(),
                                                 oDataCmd.ResourcePath, oDataCmd.AddtionalResourcepath, oDataCmd.CompleteHttpType, oDataCmd.HttpData,
                                                 oDataCmd.QueryOptions, oDataCmd.InputParajson, oDataCmd.OutputParaJson, Convert.ToInt32(oDataCmd.Functiontype).ToString(), oDataCmd.Responseformat.ToString(),
                                                 oDataCmd.Datajson, oDataCmd.ReturnType, oDataCmd.DataSetPath, oDataCmd.Description, oDataCmd.Cache,
                                                 oDataCmd.Expiryfrequency, oDataCmd.ExpiryCondtion, subAdminId, Convert.ToInt64(DateTime.UtcNow.Ticks), Convert.ToInt64(DateTime.UtcNow.Ticks), oDataCmd.Xreference);
                        if (objCmd.StatusCode == 0)
                        {
                            bindODataObjects();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* OData Object Copied Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Saved', 300);showODataCopyObjectPopUp(false);SubProcODataCmdAddPara(false);initializeDataObjects();UniformControlDesign();", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error copying OData object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error copying OData object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
            }
        }



        protected void btnSaveCredential_Click(object sender, EventArgs e)
        {
            if (ShowTestResult(txtUsername.Text.Trim(), txtPassword.Text))
            {
                saveCredentialInHidden(ltCrd.Value);
            };
        }


        void saveCredentialInHidden(string credentialproperty)
        {
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == credentialproperty)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(crd["uid"])))
                        {
                            crd["uid"] = txtUsername.Text.Trim();
                            crd["pwd"] = txtPassword.Text;
                            break;
                        }
                    }
                hidWsAuthencationMeta.Value = crds.ToString();
            }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            if (companyId.Length == 0) return;
            hdfPreviousDataObjectId.Value = hdfDataObjectId.Value;
            string scripts = EditODataCommand(companyId, subAdminId, hdfDataObjectId.Value, updEditODataObject);
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), scripts + "editODataObject();UniformControlDesign();", true);
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteOdataCommand obj = new DeleteOdataCommand(hdfDataObjectId.Value, subAdminId, companyId);
                obj.Process();
                if (obj.StatusCode == 0)
                {
                    bindODataObjects();
                    updDataObjectsList.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* OData Object Deleted Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Deleted', 300);initializeDataObjects();UniformControlDesign();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error deleting OData object');SubProcBoxMessage(true,'Error', 300);initializeDataObjects();UniformControlDesign();", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true);UniformControlDesign();", true);
            }
        }

        private string EditODataCommand(string _ComapnyId, string _SubAdminId, string _CommandId, UpdatePanel _Upd)
        {
            string strScript = "";
            OdataCommand oDataCmd = new OdataCommand(_CommandId, _ComapnyId);
            oDataCmd.GetCommandDetails();
            if (oDataCmd != null)
            {
                ddlOdataCmd_Conn.SelectedIndex = ddlOdataCmd_Conn.Items.IndexOf(ddlOdataCmd_Conn.Items.FindByValue(Convert.ToString(oDataCmd.ConnectorId.Trim())));
                ddlOdataCmd_Conn_Changed(null, null);

                hdfDataObjectId.Value = _CommandId.Trim();
                ODataRequestType strResourceTyp = oDataCmd.Resourcetype;
                HTTP_TYPE strHttpType = oDataCmd.Httptype;
                string keyType = "";
                if (strResourceTyp == ODataRequestType.EntityType)
                {
                    keyType = oDataCmd.SelectType;
                }

                ddlOdataCmd_UsingKey.SelectedIndex = ddlOdataCmd_UsingKey.Items.IndexOf(ddlOdataCmd_UsingKey.Items.FindByValue(keyType));
                ddlOdataCmd_UsingKey_Changed(null, null);

                if (!string.IsNullOrEmpty(oDataCmd.AddtionalResourcepath.Trim())) txt_AdditionalResourcePath.Text = oDataCmd.AddtionalResourcepath.Trim().Remove(0, 1);

                FunctionType strFunctionType = oDataCmd.Functiontype;
                if (strResourceTyp == ODataRequestType.EntityType)
                {
                    if (strHttpType == HTTP_TYPE.GET)
                    {
                        strScript = "OdataCmd_CreateGetHtml();OdataComdEdit_Get('" + ReplaceSingleCourseAndBackSlash(oDataCmd.Datajson) + "','" +
                                                ReplaceSingleCourseAndBackSlash(oDataCmd.InputParajson) + "','" +
                                                ReplaceSingleCourseAndBackSlash(oDataCmd.OutputParaJson) + "');";
                        ddlOdataCmd_EntityType.SelectedIndex = ddlOdataCmd_EntityType.Items.IndexOf(ddlOdataCmd_EntityType.Items.FindByValue(oDataCmd.ResourcePath.Trim().Split('(')[0]));

                    }
                    else if (strHttpType == HTTP_TYPE.POST)
                    {
                        strScript = "OdataCmd_CreatePostHtml();OdataComdEdit_Post('" + ReplaceSingleCourseAndBackSlash(oDataCmd.Datajson) + "','" +
                                                ReplaceSingleCourseAndBackSlash(oDataCmd.InputParajson) + "','" +
                                                ReplaceSingleCourseAndBackSlash(oDataCmd.OutputParaJson) + "');";
                        ddlOdataCmd_EntityType.SelectedIndex = ddlOdataCmd_EntityType.Items.IndexOf(ddlOdataCmd_EntityType.Items.FindByValue(oDataCmd.ResourcePath.Trim().Split('(')[0]));


                    }
                    else if (strHttpType == HTTP_TYPE.PUT || strHttpType == HTTP_TYPE.PATCH || strHttpType == HTTP_TYPE.MERGE)
                    {
                        strScript = "OdataCmd_CreatePutHtml();OdataComdEdit_Put('" + ReplaceSingleCourseAndBackSlash(oDataCmd.Datajson) + "','" +
                                                   ReplaceSingleCourseAndBackSlash(oDataCmd.InputParajson) + "','" +
                                                   ReplaceSingleCourseAndBackSlash(oDataCmd.OutputParaJson) + "');";
                        ddlOdataCmd_EntityType.SelectedIndex = ddlOdataCmd_EntityType.Items.IndexOf(ddlOdataCmd_EntityType.Items.FindByValue(oDataCmd.ResourcePath.Trim().Split('(')[0]));

                    }
                    else if (strHttpType == HTTP_TYPE.DELETE)
                    {

                        strScript = "OdataCmd_CreateDeleteHtml();OdataComdEdit_Del('" + ReplaceSingleCourseAndBackSlash(oDataCmd.Datajson) + "','" +
                                                      ReplaceSingleCourseAndBackSlash(oDataCmd.InputParajson) + "','" +
                                                      ReplaceSingleCourseAndBackSlash(oDataCmd.OutputParaJson).Trim() + "');";
                        ddlOdataCmd_EntityType.SelectedIndex = ddlOdataCmd_EntityType.Items.IndexOf(ddlOdataCmd_EntityType.Items.FindByValue(oDataCmd.ResourcePath.Trim().Split('(')[0]));

                    }

                    ddlOdataCmd_EntityType_Changed(null, null);

                    strScript += "setInputPara(" + ReplaceSingleCourseAndBackSlash(oDataCmd.InputParajson.Trim()) + ");"; //AddParameterInOdataCmd();SubProcODataCmdAddPara(false);chkSingleValueOnChange('" + chkSingleValue.Checked + "');


                }
                else if (strResourceTyp == ODataRequestType.Function)
                {
                    if (strFunctionType == FunctionType.WEBGET)
                    {
                        ddlOdataCmd_funcImport.SelectedIndex = ddlOdataCmd_funcImport.Items.IndexOf(ddlOdataCmd_funcImport.Items.FindByValue((oDataCmd.ResourcePath.Trim().Split('?')[0]).Trim()));
                        ddlOdataCmd_funcImport_Changed(null, null);

                    }
                    else if (strFunctionType == FunctionType.WEBINVOKE)
                    {
                        ddlOdataCmd_funcImport.SelectedIndex = ddlOdataCmd_funcImport.Items.IndexOf(ddlOdataCmd_funcImport.Items.FindByValue((oDataCmd.ResourcePath.Trim().Split('?')[0]).Trim()));
                        ddlOdataCmd_funcImport_Changed(null, null);
                    }
                    else if (strFunctionType == FunctionType.ACTION)
                    {
                        ddlOdataCmd_funcImport.SelectedIndex = ddlOdataCmd_funcImport.Items.IndexOf(ddlOdataCmd_funcImport.Items.FindByValue(oDataCmd.ResourcePath.Trim()));
                        ddlOdataCmd_funcImport_Changed(null, null);
                    }
                    strScript = "OdataComdEdit_Func('" + ReplaceSingleCourseAndBackSlash(oDataCmd.Datajson.Trim()) + "','" +
                                                          ReplaceSingleCourseAndBackSlash(oDataCmd.InputParajson.Trim()) + "','" +
                                                          ReplaceSingleCourseAndBackSlash(oDataCmd.OutputParaJson.Trim()) + "');"; //$('[id$=txtOdataCmd_Ent_DsPath]').val('" + txtOdataCmd_Ent_DsPath.Text + "');";

                }
            }
            return strScript;
        }

        private void save(bool _saveChanges)
        {
            try
            {
                string ConnectorId = ddlOdataCmd_Conn.SelectedValue;
                string CommandName = txtDataObj_Name.Text;
                string ResourceType = ddlOdataCmd_EntType.SelectedValue;
                string ResourcePath = ddlOdataCmd_EntityType.SelectedValue;
                string HttpType = ddlOdataCmd_Ent_HttpTyp.SelectedValue;
                string HttpData = hdfOdata_HttpData.Value; ;
                string QueryOption = hdfOdata_HttpQuery.Value;
                string InputParamJson = hdfOdata_InputParam.Value;
                string OutputParamJson = hdfOdata_OutputParam.Value;
                string strFunctionType = "0";
                string ResponseFormat = ddlOdataCmd_ResFormat.SelectedValue;
                string DataJson = hdfOdata_DataJson.Value;
                string strReturnType = ddlOdataCmd_Ent_ReturnTyp.SelectedValue;
                string strDsPath = "";
                if (strReturnType == "0") strDsPath = "";
                else strDsPath = txtOdataCmd_Ent_DsPath.Text;

                var strHttpWithKeyType = "";
                string AdditionalSingleValueResourcePath = string.Empty;

                if (ddlOdataCmd_EntType.SelectedValue == "0")
                {
                    strHttpWithKeyType = ddlOdataCmd_Ent_HttpTyp.SelectedValue + "_" + ddlOdataCmd_UsingKey.SelectedValue;
                    strFunctionType = "0";
                    if (HttpType == "1")
                    {
                        if (Convert.ToInt32(ddlOdataCmd_UsingKey.SelectedValue) == 1) ResourcePath += (hdfOdata_PutKeys.Value.Trim().Length > 0 ? "(" + hdfOdata_PutKeys.Value.Trim() + ")" : "");
                        if (txt_AdditionalResourcePath.Text.Trim().Length > 0)
                        {
                            if (!txt_AdditionalResourcePath.Text.Trim().StartsWith("/")) AdditionalSingleValueResourcePath += "/" + txt_AdditionalResourcePath.Text.Trim();
                            else AdditionalSingleValueResourcePath += txt_AdditionalResourcePath.Text.Trim();
                        }
                        if (chkSingleValue.Checked)
                        {
                            strReturnType = "2";
                            ResponseFormat = "2";
                        }
                        else if (strReturnType == "2")
                        {
                            ResponseFormat = "2";
                        }
                    }
                    else if (HttpType == "3" || HttpType == "4" || HttpType == "5")
                    {
                        ResourcePath += (hdfOdata_PutKeys.Value.Trim().Length > 0 ? "(" + hdfOdata_PutKeys.Value.Trim() + ")" : "");
                    }
                    else if (HttpType == "6")
                    {
                        ResourcePath += (hdfOdata_PutKeys.Value.Trim().Length > 0 ? "(" + hdfOdata_PutKeys.Value.Trim() + ")" : "");
                    }

                }

                else if (ddlOdataCmd_EntType.SelectedValue == "1")
                {
                    ResourcePath = ddlOdataCmd_funcImport.SelectedValue;
                    QueryOption = "";
                    strFunctionType = "0";

                    if (hdfOdata_FuncType.Value.Trim() == Convert.ToString(FunctionType.WEBGET))
                    {
                        ResourcePath = ddlOdataCmd_funcImport.SelectedValue + (hdfOdata_HttpData.Value.Trim().Length > 0 ? "?" + hdfOdata_HttpData.Value.Trim() : "");
                        HttpData = "";
                        strFunctionType = "1";
                        HttpType = "1";
                    }
                    else if (hdfOdata_FuncType.Value.Trim() == Convert.ToString(FunctionType.WEBINVOKE))
                    {
                        ResourcePath = ddlOdataCmd_funcImport.SelectedValue + (hdfOdata_HttpData.Value.Trim().Length > 0 ? "?" + hdfOdata_HttpData.Value.Trim() : "");
                        HttpData = "";
                        strFunctionType = "2";

                        if (hdfOdata_FuncHType.Value.Trim().ToUpper() == "GET")
                        {
                            HttpType = "1";
                        }
                        else if (hdfOdata_FuncHType.Value.Trim().ToUpper() == "POST")
                        {
                            HttpType = "2";
                        }
                        else if (hdfOdata_FuncHType.Value.Trim().ToUpper() == "PUT")
                        {
                            HttpType = "3";
                        }
                        else if (hdfOdata_FuncHType.Value.Trim().ToUpper() == "PATCH")
                        {
                            HttpType = "4";
                        }
                        else if (hdfOdata_FuncHType.Value.Trim().ToUpper() == "MERGE")
                        {
                            HttpType = "5";
                        }
                        else if (hdfOdata_FuncHType.Value.Trim().ToUpper() == "DELETE")
                        {
                            HttpType = "6";
                        }
                        else
                        {
                            HttpType = "0";
                        }
                    }
                    else if (hdfOdata_FuncType.Value.Trim() == Convert.ToString(FunctionType.ACTION))
                    {
                        strFunctionType = "3";
                        HttpType = "2";
                    }
                }

                int cacheFrequency = 0;
                if (ddl_DataCache_type.SelectedValue.ToString() == "1")
                    cacheFrequency = Convert.ToInt32(ddl_AbsoluteCachePeriod.SelectedValue.ToString());
                else if (ddl_DataCache_type.SelectedValue.ToString() == "2")
                    cacheFrequency = Convert.ToInt32(ddl_RelativeCachePeriod.SelectedValue.ToString());

                string expDataCacheCondition = getExpectedCacheCondition();

                if (!_saveChanges)
                {
                    if (string.IsNullOrEmpty(hdfDataObjectId.Value))
                    {
                        GetOdataCommand obj = new GetOdataCommand(true, false, subAdminId, "", "", companyId);
                        if (obj.StatusCode == 0 && obj.ResultTable.Rows.Count>0)
                        {
                            string filter = String.Format("ODATA_COMMAND_NAME = '{0}'", txtDataObj_Name.Text.Trim());
                            DataRow[] rows = obj.ResultTable.Select(filter);
                            if (rows.Length > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Object name already exists.');$('#a2').html('');", true);
                                return;
                            }
                        }
                        AddOdataCommand objCmd = new AddOdataCommand(companyId, subAdminId, ConnectorId, CommandName, ResourceType, ResourcePath, AdditionalSingleValueResourcePath, strHttpWithKeyType, HttpData, QueryOption, InputParamJson, OutputParamJson, strFunctionType, ResponseFormat, DataJson, strReturnType, strDsPath, txtDataObj_Desc.Text, Convert.ToInt32(ddl_DataCache_type.SelectedValue.ToString()), cacheFrequency, expDataCacheCondition, subAdminId, Convert.ToInt64(DateTime.UtcNow.Ticks), Convert.ToInt64(DateTime.UtcNow.Ticks), "[]");
                        if (objCmd.StatusCode == 0)
                        {
                            bindODataObjects();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* OData Object Saved Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Saved', 300);SubProcODataCmdAddPara(false);initializeDataObjects();UniformControlDesign();", true);
                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error adding OData object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
                        }

                    }
                    else
                    {
                        UpdateOdataCommand objUpdCmd = new UpdateOdataCommand(companyId, subAdminId, ConnectorId, hdfDataObjectId.Value.Trim(), ResourceType, ResourcePath, AdditionalSingleValueResourcePath, strHttpWithKeyType, HttpData, QueryOption, InputParamJson, OutputParamJson, strFunctionType, ResponseFormat, DataJson, strReturnType, strDsPath, txtDataObj_Desc.Text, Convert.ToInt32(ddl_DataCache_type.SelectedValue.ToString()), cacheFrequency, expDataCacheCondition, Convert.ToInt64(DateTime.UtcNow.Ticks), "[]");
                        if (objUpdCmd.StatusCode == 0)
                        {
                            bindODataObjects();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* OData object Updated successfully." + "');SubProcConfirmBoxMessageDB(true,'Updated', 300);initializeDataObjects();UniformControlDesign();", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error updating OData object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign(); $('[id$=btnDataObj_Cancel]').show();", true);
                        }
                    }
                }
                else
                {
                    UpdateOdataCommand objUpdCmd = new UpdateOdataCommand(companyId, subAdminId, ConnectorId, hdfPreviousDataObjectId.Value.Trim(), ResourceType, ResourcePath, AdditionalSingleValueResourcePath, strHttpWithKeyType, HttpData, QueryOption, InputParamJson, OutputParamJson, strFunctionType, ResponseFormat, DataJson, strReturnType, strDsPath, txtDataObj_Desc.Text, Convert.ToInt32(ddl_DataCache_type.SelectedValue.ToString()), cacheFrequency, expDataCacheCondition, Convert.ToInt64(DateTime.UtcNow.Ticks), "[]");
                    if (objUpdCmd.StatusCode == 0)
                    {
                        bindODataObjects();
                        hdfPreviousDataObjectId.Value = string.Empty;
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* OData object Updated successfully." + "');SubProcConfirmBoxMessageDB(true,'Updated', 300);discardChanges();UniformControlDesign();", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error updating OData object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();ChangeSelection();$('[id$=btnDataObj_Cancel]').show();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string getExpectedCacheCondition()
        {
            string expCondition = "";
            if (ddl_DataCache_type.SelectedValue.ToString() == "1")
            {
                switch (ddl_AbsoluteCachePeriod.SelectedValue.ToString())
                {
                    case "0":
                        expCondition = ddl_AbsoluteMinutes.SelectedValue.ToString();
                        break;
                    case "1":
                        expCondition = ddl_AbsoluteDayHour.SelectedValue.ToString() + ":" + ddl_AbsoluteDayMin.SelectedValue.ToString();
                        break;
                    case "2":
                        expCondition = ddl_AbsoluteWeek.SelectedValue.ToString();
                        break;
                    case "3":
                        expCondition = ddl_AbsoluteDate.SelectedValue.ToString();
                        break;
                    case "4":
                        expCondition = ddl_AbsoluteYear_Day.SelectedValue.ToString() + "/" + ddl_AbsoluteYear_Month.SelectedValue.ToString();
                        break;
                }
            }
            else if (ddl_DataCache_type.SelectedValue.ToString() == "2")
            {
                switch (ddl_RelativeCachePeriod.SelectedValue.ToString())
                {
                    case "0":
                        expCondition = ddl_RelativeMinute.SelectedValue.ToString();
                        break;
                    case "1":
                        expCondition = ddl_RelativeHour.SelectedValue.ToString();
                        break;
                    case "2":
                        expCondition = ddl_RelativeDay.SelectedValue.ToString();
                        break;
                    case "3":
                        expCondition = ddl_RelativeWeek.SelectedValue.ToString();
                        break;
                    case "4":
                        expCondition = ddl_RelativeMonth.SelectedValue.ToString();
                        break;
                }
            }
            return expCondition;
        }

        private string getDataType(string type)
        {
            string value = "";
            switch (type)
            {
                case "Edm.String":
                    value = "0";
                    break;
                case "Edm.Decimal":
                    value = "1";
                    break;
                case "Edm.Int32":
                    value = "2";
                    break;
                case "Edm.Boolean":
                    value = "3";
                    break;
                default:
                    value = "-1";
                    break;
            }
            return value;
        }

        private void IdeOdataDropDown()
        {
            IdeODataHelper objHelperCmd = new IdeODataHelper();
            objHelperCmd.BindOdataConnDp(companyId, subAdminId, ddlOdataCmd_Conn, updAddNewDataObject);
        }

        private void bindODataObjects()
        {
            try
            {
                List<OdataCommand> lstODataCmds = new List<OdataCommand>();
                OdataCommand oDataCmd = null;
                GetOdataCommand objGetodataCommand = new GetOdataCommand(true, false, subAdminId, "", "", companyId);
                UpdateObjectXreference objref = new UpdateObjectXreference();
                DataTable dtApps = objref.GetAppsListForObjectReff(this.companyId);          
                if (objGetodataCommand.StatusCode == 0)
                {
                    DataTable dt = objGetodataCommand.ResultTable;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            oDataCmd = new OdataCommand(Convert.ToString(row["ODATA_COMMAND_ID"]), companyId);
                            oDataCmd.GetCommandDetails();
                            oDataCmd.Usage = Utilities.SerializeJson<List<List<string>>>(Utilities.objectUsage(dtApps.Select("Command_ids like '%OD_" + oDataCmd.CommandId + "%'"), oDataCmd.CommandId, "OD_"));
                    

                            if (!lstODataCmds.Contains(oDataCmd))
                                lstODataCmds.Add(oDataCmd);
                        }
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(updDataObjectsList, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + MficientConstants.ERROR_DATABASE_CONNECTION + "');", true);
                
                hdfCompleteData.Value = JsonConvert.SerializeObject(lstODataCmds);
                hdfSelectedData.Value = getSelectedDataofObjects(lstODataCmds);

                ScriptManager.RegisterStartupScript(updDataObjectsList, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"initializeDataObjects();UniformControlDesign();", true);
            }
            catch 
            {

                ScriptManager.RegisterStartupScript(updDataObjectsList, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"initializeDataObjects();UniformControlDesign();", true);
            }
        }

        private string getSelectedDataofObjects(List<OdataCommand> lstODataObjects)
        {
            string serializedData = string.Empty;
            List<List<string>> lstDataObjects = new List<List<string>>();
            try
            {
                if (lstODataObjects != null && lstODataObjects.Count > 0)
                {
                    List<string> dataObj = new List<string>();
                    foreach (OdataCommand obj in lstODataObjects)
                    {
                        dataObj = new List<string>();
                        dataObj.Add(obj.CommandId);
                        dataObj.Add(obj.CommandName);
                        dataObj.Add(obj.ConnectionName);
                        if (obj.Resourcetype == ODataRequestType.EntityType)
                        {
                            dataObj.Add("EntityType");
                        }
                        else if (obj.Resourcetype == ODataRequestType.Function)
                        {
                            dataObj.Add("Function");
                        }
                        else
                        {
                            dataObj.Add("");
                        }
                        dataObj.Add(obj.Resourcetype.ToString());
                        lstDataObjects.Add(dataObj);
                    }
                }

                serializedData = JsonConvert.SerializeObject(lstDataObjects);
            }
            catch
            {
                serializedData = string.Empty;
            }
            return serializedData;
        }

        private string ReplaceSingleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceSingleCourse(ReplaceBackSlash(_Input));
            }
        }

        protected string GetFormatedODataVersion(object _InputString)
        {
            if (_InputString != null)
            {
                if (Convert.ToString(_InputString).Length > 0)
                {
                    if (Convert.ToString(_InputString).Trim() == "3" || Convert.ToString(_InputString).Trim() == "3.0") return "V3";
                    else if (Convert.ToString(_InputString).Trim() == "2" || Convert.ToString(_InputString).Trim() == "2.0") return "V2";
                    else return "";
                }
                else return "";

            }
            else return "";

        }

        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"\\", @"\\");
            }
        }

        private string ReplaceSingleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"'", @"\'");
            }
        }

        private bool IsStringContainsSpecialChar(string _Name)
        {
            bool bolExists = false;
            Regex Rgx = new Regex("[^a-z0-9A-Z]");
            if (Rgx.IsMatch(_Name))
            {
                bolExists = true;
            }
            else { }
            return bolExists;
        }

        private bool IsInteger(string _Value)
        {
            bool IsInt = false;
            try
            {
                int i = Convert.ToInt16(_Value);
                IsInt = true;
            }
            catch
            {
                IsInt = false;
            }
            return IsInt;
        }

        private void bindDataCacheDropdowns()
        {
            ddl_AbsoluteDayHour.Items.Clear();
            ddl_AbsoluteDayMin.Items.Clear();
            ddl_AbsoluteDate.Items.Clear();
            ddl_AbsoluteYear_Day.Items.Clear();
            ddl_RelativeMinute.Items.Clear();
            ddl_RelativeHour.Items.Clear();
            ddl_RelativeDay.Items.Clear();
            ddl_RelativeWeek.Items.Clear();
            ddl_RelativeMonth.Items.Clear();

            for (int abHr = 0; abHr <= 23; abHr++)
            {
                ddl_AbsoluteDayHour.Items.Add(new ListItem(abHr.ToString(), abHr.ToString()));
            }

            for (int abMin = 0; abMin <= 59; abMin++)
            {
                ddl_AbsoluteDayMin.Items.Add(new ListItem(abMin.ToString(), abMin.ToString()));
            }

            for (int abDate = 1; abDate <= 31; abDate++)
            {
                ddl_AbsoluteDate.Items.Add(new ListItem(abDate.ToString(), abDate.ToString()));
            }

            for (int abYrDay = 1; abYrDay <= 30; abYrDay++)
            {
                ddl_AbsoluteYear_Day.Items.Add(new ListItem(abYrDay.ToString(), abYrDay.ToString()));
            }

            for (int relMin = 0; relMin <= 59; relMin++)
            {
                ddl_RelativeMinute.Items.Add(new ListItem(relMin.ToString(), relMin.ToString()));
            }

            for (int relHr = 0; relHr <= 23; relHr++)
            {
                ddl_RelativeHour.Items.Add(new ListItem(relHr.ToString(), relHr.ToString()));
            }

            for (int relDay = 0; relDay <= 31; relDay++)
            {
                ddl_RelativeDay.Items.Add(new ListItem(relDay.ToString(), relDay.ToString()));
            }

            for (int relWeek = 0; relWeek <= 52; relWeek++)
            {
                ddl_RelativeWeek.Items.Add(new ListItem(relWeek.ToString(), relWeek.ToString()));
            }

            for (int relMnth = 0; relMnth <= 12; relMnth++)
            {
                ddl_RelativeMonth.Items.Add(new ListItem(relMnth.ToString(), relMnth.ToString()));
            }
        }

        protected void savebtn_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"viewODataObject();", true);
            GetOdataCommand objGetodataCommand = new GetOdataCommand(false, false, subAdminId, hdfDataObjectId.Value, "", companyId);
            if (objGetodataCommand != null && objGetodataCommand.StatusCode==0)
            {
                string CreadentialCheck = "";
                CreadentialCheck = Convert.ToString(objGetodataCommand.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]).Trim();
                if (CreadentialCheck != "" && CreadentialCheck != "-1")
                {
                   JObject crdtial = getCradential(CreadentialCheck);
                    if (crdtial != null)
                    {
                        if (Convert.ToString(crdtial["uid"]) != "" && Convert.ToString(crdtial["pwd"]) != "")
                        {
                            ShowTestResult(Convert.ToString(crdtial["uid"]), Convert.ToString(crdtial["pwd"]));
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('* Invalid credential.');", true);
                    }
                }
            }
        }

        JObject getCradential(string _credentialproperty)
        {
            ltCrd.Value = _credentialproperty;
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == _credentialproperty)
                        return crd;
            }
            return null;
        }


        bool ShowTestResult(string _unm, string _pwd)
        {
            bool flag = false;
            string outError = "";
            string st = Utilities.testObjectRequest(this.companyId, hdfDataObjectId.Value, "5", (
                (hdflp.Value.Length <= 0) ? new JArray() : JArray.Parse(hdflp.Value))
                , AesEncryption.AESEncrypt(this.companyId, _unm), AesEncryption.AESEncrypt(this.companyId, _pwd), out outError);
            if (st.Length > 0)
            {
                JObject dataObject = JObject.Parse(st);
                JObject dataObjectresp = JObject.Parse(dataObject["resp"].ToString());
                JObject dataObjectRespStatus = JObject.Parse(dataObjectresp["status"].ToString());
                int statuscode = Convert.ToInt32(dataObjectRespStatus["cd"].ToString());

                if (statuscode == 0)
                {
                    flag = true;
                    JObject dataObjectRespData = JObject.Parse(dataObjectresp["data"].ToString());
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CreateDynamicTable(" + dataObjectRespData["dt"].ToString() + ");", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"opendialogtest(false);SubProcBoxMessageDataConection(true,'Error');$('#aMessage').html('" + dataObjectRespStatus["desc"].ToString() + "(" + statuscode.ToString() + ")" + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"opendialogtest(false);SubProcBoxMessageDataConection(true,'Error');$('#aMessage').html('Http Error Code :" + outError + "');", true);
            }
            return flag;
        }
        
    }
}