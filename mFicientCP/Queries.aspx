﻿<%@ Page Title="mFicient | Support" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="Queries.aspx.cs" Inherits="mFicientCP.Queries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        a.linkSelected:link
        {
            color: #8F8F97;
        }
        .repeaterTable td span
        {
            color: rgb(111,111,111);
        }
        .modalPopUpDetails .fieldsetLegendDiv
        {
            width: 96%;
        }
        .singleSection .fieldsetLegendDiv section .column3
        {
            width: 75% !important;
        }
        #divSrchPnlUser div.selector span
        {
            width:200px;
        }
        #ddlQueryCont div.selector span
        {
            width:200px;     
        }
    </style>
    <script type="text/javascript">
        PostBackFor = {
            MOBILE_USER_QUERIES: 0,
            MY_QUERIES: 1,
            RESOLVED_QUERIES: 2,
            CLOSED_QUERIES: 3
        }
        function postBackOnTabClick(sender, postBackFor) {
            var btnThatCausesPostBack = document.getElementById('<%=btnTabClickPostBack.ClientID %>');
            var hidFieldUsedAfterPostBack = document.getElementById('<%=hidValuesUsedAfterPostBack.ClientID %>');
            hidFieldUsedAfterPostBack.value = postBackFor;
            btnThatCausesPostBack.click();
        }
        function storeSelectedTabIndex(index) {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            hidSelectedTabIndex.value = '';
            hidSelectedTabIndex.value = index;
        }


        function makeTabAfterPostBack() {
            $('#content').find('div.tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        }
        function storeSelectedTabIndex(index) {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            hidSelectedTabIndex.value = '';
            hidSelectedTabIndex.value = index;
        }
        function showSelectedTabOnPostBack() {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');

            var lstMobileUserQueries = $('#lstMobileUserQueries');
            var lstMyQueries = $('#lstMyQueries');
            var lstResolvedQueries = $('#lstResolvedQueries');
            //var lstClosedQueries = $('#lstClosedQueries');

            switch (parseInt(hidSelectedTabIndex.value, 10)) {
                case PostBackFor.MOBILE_USER_QUERIES:
                    lstMobileUserQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstMyQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstResolvedQueries.removeClass(" ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    //lstClosedQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    break;
                case PostBackFor.MY_QUERIES:
                    lstMobileUserQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstMyQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstResolvedQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    //lstClosedQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    break;
                case PostBackFor.RESOLVED_QUERIES:
                    lstMobileUserQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstMyQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstResolvedQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    //lstClosedQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    break;

                case PostBackFor.CLOSED_QUERIES:
                    //lstClosedQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstMyQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstResolvedQueries.removeClass(" ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstMobileUserQueries.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    break;
            }
        }

        function validateCommentText() {
            var txtComment = $('#' + '<%= txtcomment.ClientID %>');
            if (txtComment && $.trim($(txtComment).val()) === "") {
                showMessage('Please enter a comment.', '$.alert.Error', DialogType.Error);
                return false;
            }
            else {
                return true;
            }
        }

        function clearAddQueryControls() {
            try {
                $('#' + ('<%=txtAddQuerySubject.ClientID %>')).val("");
                $('#' + ('<%=txtAddQueryDescription.ClientID %>')).val("");
                $('#divModalContainer').find('.alert').remove();
            }
            catch (Error)
            { }
        }

        function validateAddQueryForm() {
            var txtSubject = $('#' + ('<%=txtAddQuerySubject.ClientID %>'));
            var txtDescription = $('#' + ('<%=txtAddQueryDescription.ClientID %>'));
            var strError = "";
            if (txtSubject && $(txtSubject).val() === "") {
                strError = "Please enter a subject <br/>";
            }
            if (txtDescription && $(txtDescription).val() === "") {
                strError += "Please enter a description<br/>";
            }
            if (strError === "") {
                return true;
            }
            else {
                showAlert(strError, '#divAddQueryError');
                return false;
            }
        }

        function clearFieldsOfAddQuery() {
            var txtSubject = $('#' + ('<%=txtAddQuerySubject.ClientID %>'));
            var txtDescription = $('#' + ('<%=txtAddQueryDescription.ClientID %>'));
            if (txtSubject) $(txtSubject).val();
            if (txtDescription) $(txtDescription).val();
        }
        function getCommentTextBox() {
            return $('#' + '<%=txtcomment.ClientID %>');
        }
        function getCommentErrorDiv() {
            return $('#divCommentErrorAlert');
        }
        function openCommentsPopUp() {
            showModalPopUp('divCommentByUser', 'Add comment', 450, false);
        }
        function validateCommentText() {
            var txtComment = getCommentTextBox();
            if (txtComment) {
                if ($.trim($(txtComment).val()) == "") {
                    $.wl_Alert('Please enter a comment', 'info', getCommentErrorDiv());
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updRepeater" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div class="g12 widget" id="widget_tabs">
                    <div id="div1" class="tab">
                        <ul id="tabList">
                            <li id="lstMobileUserQueries"><a id="lnkMobileUserQueries" href="#divRepeater" onclick="storeSelectedTabIndex(PostBackFor.MOBILE_USER_QUERIES);postBackOnTabClick(this,PostBackFor.MOBILE_USER_QUERIES)">
                                Mobile User Queries</a></li>
                            <li id="lstMyQueries"><a id="lnkMyQueries" href="#divRepeater" onclick="storeSelectedTabIndex(PostBackFor.MY_QUERIES);postBackOnTabClick(this,PostBackFor.MY_QUERIES)">
                                My Queries</a></li>
                            <li id="lstResolvedQueries"><a id="lnkResolvedQueries" href="#divRepeater" onclick="storeSelectedTabIndex(PostBackFor.RESOLVED_QUERIES);postBackOnTabClick(this,PostBackFor.RESOLVED_QUERIES)">
                                Resolved Queries</a></li>
                            <%--<li id="lstClosedQueries"><a id="lnkClosedQueries" href="#divRepeater" onclick="storeSelectedTabIndex(PostBackFor.CLOSED_QUERIES);postBackOnTabClick(this,PostBackFor.CLOSED_QUERIES)">
                                Closed Queries</a></li>--%>
                        </ul>
                        <div id="divRepeater">
                            <section>
                                <div id="divInformation">
                                </div>
                            </section>
                            <section>
                                <asp:Panel ID="pnlResolvedQueriesSearch" runat="server">
                                    <div class="searchRow g12" style="margin-left: 4px;">
                                        <div class="g4" style="padding-top: 10px;">
                                            <div class="fl" style="padding-top: 4px;">
                                                <asp:Label ID="lblForSearchTokenNo" runat="server" Text="Token No :"></asp:Label></div>
                                            <div class="fl" style="margin-left: 10px;">
                                                <asp:TextBox ID="txtSrchPnlTokenNo" runat="server" Width="80%"></asp:TextBox></div>
                                        </div>
                                        <div id="divSrchPnlUser" class="g5" style="padding: 6px 0px 2px 0px;">
                                            <asp:Label ID="lblForDdlUser" runat="server" Text="User :" Style="position: relative;
                                                top: -12px;"></asp:Label>
                                            <asp:DropDownList ID="ddlSrchPnlUser" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Selected="True" Text="All" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="g3">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="fr" OnClick="btnSearch_Click" />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="clear">
                                </div>
                                <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server" Style="width: 99%;
                                    margin: 3px;">
                                    <section>
                                        <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                            <div>
                                                <asp:Label ID="lblHeaderInfo" runat="server"> </asp:Label></div>
                                            <div class="rptTblHeaderLink">
                                                <asp:LinkButton ID="lnkAddNewQuery" runat="server" Text="add" CssClass="repeaterLink fr"
                                                    Visible="false" OnClientClick="clearAddQueryControls();
                                                        showModalPopUpWithOutHeader('divModalContainer','Add Query','350',false);
                                                        changeWidthOfDiv('divPopUpActionCont',POP_UP_ACTION_DIV_2,true);return false;"></asp:LinkButton>
                                            </div>
                                        </asp:Panel>
                                    </section>
                                    <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"
                                        OnItemDataBound="rptUserDetails_ItemDataBound">
                                        <HeaderTemplate>
                                            <table class="repeaterTable">
                                                <thead>
                                                    <tr>
                                                        <th style="display: none;">
                                                            USER ID
                                                        </th>
                                                        <th>
                                                            Name
                                                        </th>
                                                        <th>
                                                            Token Number
                                                        </th>
                                                        <th>
                                                            Query
                                                        </th>
                                                        <th>
                                                            Posted On
                                                        </th>
                                                        <th>
                                                        </th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tbody>
                                                <tr class="repeaterItem">
                                                    <td style="display: none;">
                                                        <asp:Label ID="lblid" runat="server" Text='<%#Eval("USER_ID") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblname" runat="server" Text='<%#Eval("NAME") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblnumber" runat="server" Text='<%#Eval("TOKEN_NO") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblquery" runat="server" Text='<%# Eval("QUERY_SUBJECT") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbldate" runat="server" Text='<%# Eval("POSTED_ON") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                            OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tbody>
                                                <tr class="repeaterItem">
                                                    <td style="display: none;">
                                                        <asp:Label ID="lblid" runat="server" Text='<%#Eval("USER_ID") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblname" runat="server" Text='<%#Eval("NAME") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblnumber" runat="server" Text='<%#Eval("TOKEN_NO") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblquery" runat="server" Text='<%# Eval("QUERY_SUBJECT") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbldate" runat="server" Text='<%# Eval("POSTED_ON") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                            OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <div>
                                    </div>
                                    <div style="clear: both">
                                        <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                        <asp:HiddenField ID="hfTokenNumber" runat="server" />
                                    </div>
                                </asp:Panel>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUserDtlsModal" style="display: none;">
        <div class="modalPopUpDetails">
            <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server" class="singleSection">
                        <fieldset>
                            <div class="fieldsetLegendDiv">
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="lblForUserName" runat="server" Text="Name"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lname" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lblFullName" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="lblForSubject" runat="server" Text="Subject"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label5" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lsubject" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="lblForQuery" runat="server" Text="Query"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label4" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lquery" runat="server"></asp:Label>
                                    </div>
                                </section>
                            </div>
                            <div class="fieldsetLegendDiv">
                                <div>
                                    <asp:Label ID="lblcomments" Text="Comments" runat="server" Style="font-weight: bold;
                                        font-size: 14px;"></asp:Label></div>
                                <div>
                                    <asp:Label ID="lblCommentsRptHeader" Style="font-size: 12px;" runat="server"></asp:Label>
                                </div>
                                <asp:Panel ID="pnlRptComments" runat="server" ScrollBars="Vertical" Height="300"
                                    Style="margin-top: 5px;">
                                    <asp:Repeater ID="rptComments" runat="server">
                                        <ItemTemplate>
                                            <div style="margin-bottom: 5px;">
                                                <div>
                                                    <asp:Label ID="lblcommentby" runat="server" Text=' <%#Eval("USER_NAME") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCommentByName" runat="server" Text=' <%#Eval("FULL_NAME") %>' Style="font-weight: bold;"></asp:Label>
                                                    &nbsp;<asp:Label ID="lblColon" runat="server" Text=":"></asp:Label>&nbsp;
                                                </div>
                                                <div>
                                                    <asp:Label ID="lblcomment" runat="server" Text='<%# Eval("COMMENT") %>'></asp:Label></div>
                                                <hr style="margin: 5px;" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                            </div>
                            <div class="fr" style="margin-right: 3px;">
                                <asp:LinkButton ID="lnkAddComment" runat="server" Text="add comment" Style="font-size: 14px;"
                                    OnClientClick="openCommentsPopUp();return false;"></asp:LinkButton>
                            </div>
                            <div style="clear: both;">
                            </div>
                        </fieldset>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divModalContainer">
        <asp:UpdatePanel ID="updAddQueryContainer" runat="server">
            <ContentTemplate>
                <div id="divAddQueryError">
                </div>
                <div id="divAddQueryContainer" class="manageGroupPopUpCont">
                    <div>
                        <label for="<%=txtAddQuerySubject.ClientID %>">
                            Query Type</label>
                        <div id="ddlQueryCont" style="margin-top: 1px; position: relative; left: -2px;">
                            <asp:DropDownList ID="ddlQueryType" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div>
                        <label for="<%=txtAddQuerySubject.ClientID %>">
                            Subject</label>
                        <div>
                            <asp:TextBox ID="txtAddQuerySubject" runat="server" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <label for="<%=txtAddQueryDescription.ClientID %>">
                            Description</label>
                        <div>
                            <asp:TextBox ID="txtAddQueryDescription" runat="server" MaxLength="16"></asp:TextBox>
                        </div>
                    </div>
                    <div class="modalPopUpAction">
                        <div id="divPopUpActionCont" class="popUpActionCont">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSaveQuery_Click"
                                OnClientClick="return validateAddQueryForm();" /><asp:Button ID="btnCancel" runat="server"
                                    Text="Cancel" CssClass="aspButton" OnClientClick="clearFieldsOfAddQuery();closeModalPopUp('divModalContainer');return false;" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divCommentByUser" style="display: none;">
        <div class="modalPopUpDetails" style="width: 100%; display: block; padding-left: 3px;">
            <asp:UpdatePanel ID="updCommentByUser" runat="server">
                <ContentTemplate>
                    <div id="divCommentErrorAlert">
                    </div>
                    <div class="fieldsetLegendDiv" style="margin-left: 15px; width: auto;">
                        <div class="singleColumn">
                            <asp:Label ID="lblcomment" Text="Comment" CssClass="lightText" runat="server"></asp:Label>
                        </div>
                        <div class="singleColumn" style="margin-top: 5px;">
                            <asp:TextBox ID="txtcomment" TextMode="MultiLine" Rows="3" runat="server" Width="370px"></asp:TextBox>
                        </div>
                        <div class="fl" style="margin-top: 10px;">
                            <asp:CheckBox ID="chkIsResolved" runat="server" Text="Resolved" />
                        </div>
                        <div class="fr" style="margin-top: 10px; margin-right: 1px;">
                            <asp:Button ID="bttncomment" runat="server" Text="Post" OnClientClick="return validateCommentText();"
                                OnClick="bttncomment_Click" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnTabClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnTabClickPostBack" runat="server" OnClick="btnTabClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <div style="clear: both">
    </div>
    <div>
        <asp:HiddenField ID="hidValuesUsedAfterPostBack" runat="server" />
        <asp:HiddenField ID="hidTabSelected" runat="server" />
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            //$("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>
