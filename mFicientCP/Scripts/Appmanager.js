﻿var ApplicaionIconPath = '//enterprise.mficient.com/images/icon';
var ApplicaionIconImage = 'GRAY0207.png';
var ApplicaionIconName = 'GRAY0207';
function AddNewCategoryClick() {
    $('[id$=txtCategoryName]').val('');
    $('[id$=hidMenuCategoryForEdit123]').val('');
    $('[id$=MenuCatDetailHeaderDiv123]').hide();
    $('#lnkMenuCat_Back').hide();
    $('#imgmenuIcon').attr('src', ApplicaionIconPath + '/' + ApplicaionIconImage);
    $('[id$=btnDeleteMenuCategory]').hide();
    $('#MenuIconName').html('application(Default)');
    $('#EditMenuCatDetailsDiv').show();
    $('#MenuCatDetailsDiv').hide();
    SubProcMenuCategory123(true);

}
// info Button  
function ViewInfo(sender) {
    var v = $(sender).parents("tr:first").find('[id*=lblcategorynamedetails]');
    var v3 = $(sender).parents("tr:first").find('[id*=lblversion]');
    var v1 = $(sender).parents("tr:first").find('[id*=lbldeviceinterface]');
    var v2 = $(sender).parents("tr:first").find('[id*=lblpreversion]');
    $('[id$=lblAppInfoCat]').text($(v).text());
    $('[id$=lblappinterface]').text($(v1).text());
    $('[id$=lblpublishversion]').text($(v3).text());
    $('[id$=lbllastversion]').text($(v2).text());

    suboverview(true);
}

function suboverview(_bol) {
    if (_bol) showModalPopUp('SubProcAppInfo', " App Info", 500, false);
    else  $('#SubProcAppInfo').dialog('close');
}

/// Publish Button
function ViewpublishInfo(sender) {
    var parent = $(sender).parents("tr:first");
    $('[id$=hdfPubAppWfId]').val( $(parent).find('[id*=lblWorkflowId]').text());
    formPublishVersionsList($(parent).find('[id*=lblAllVersions]').text(), $(parent).find('[id*=lblversion]').text());
    subpublish(true);
}

function subpublish(_bol) {
    if (_bol)  showModalPopUp('SubProcPublishApp', "Publish Apps", 300, false); 
    else  $('#SubProcPublishApp').dialog('close'); 
}

function SubProcMenuCategory123(_bol) {
    if (_bol) {
        var strMenuHeaderText = $('[id$=hidoverview]').val();
        showModalPopUp('EditMenuCatDetailsDiv123', "Add Category", 355, false);
    }
    else {
        $('#EditMenuCatDetailsDiv123').dialog('close');
    }
}

function EditSubProcMenuCategory(_bol) {
    if (_bol) {
        var strMenuHeaderText = $('[id$=hidoverview]').val();
        showModalPopUp('EditMenuCatDetailsDiv123', "Edit Category", 355, false);
    }
    else {
        $('#EditMenuCatDetailsDiv123').dialog('close');
    }
}

function makeTabAfterPostBack() {
    $('#content').find('div.tab').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        }
    });
    $('#SubProcAppInfo').find('div.tab').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        }
    });
}

function SubProcMenuCategoryToday(_bol) {
var Cat="";

    if (_bol) {
     Cat="Category Details";
        showModalPopUp('SubProcMenuCategory',Cat , 525, false);
    }
    else {
        $('#SubProcMenuCategory').dialog('close');
    }
}

//Validate name Category name
function ValidateNameIdeAppName(_Name) {
    if (_Name.trim().length == 0)
        return 1;
    if (_Name.trim().length > 20)
        return 2;
    if (_Name.trim().length < 3)
        return 3;
    if (CheckIdeAppName(_Name))
        return 4;
    if (Math.floor(_Name.substring(0, 1)) == _Name.substring(0, 1))
        return 5;
    return 0;
}
//Validate Category name
function CheckIdeAppName(_Name) {
    if (!/^[a-z0-9-\s]+$/i.test(_Name))  return true;
    return false;
}

function ValidateMenuCategory() {
    var strMessage = "";
    if ($('[id$=txtCategoryName]').length != 0) {
        var strVal = ValidateNameIdeAppName($('[id$=txtCategoryName]').val());
        if (strVal != 0) {
            switch (strVal) {
                case 1:
                    strMessage += " * Please enter category name.</br>";
                    break;
                case 2:
                    strMessage += " * Name cannot be more than 20 charecters.</br>";
                    break;
                case 3:
                    strMessage += " * Name cannot be less than 3 charecters.</br>";
                    break;
                case 4:
                    strMessage += " * Please enter valid category name.</br>";
                    break;
                case 5:
                    strMessage += " * Please enter valid category name.</br>";
                    break;
            }
        }
    }

    if (strMessage.length != 0) {
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
        return false;
    }
    else return true;
}

function setMenuIconOnSelect(_Name) {
    if (_Name.length != 0) {
        $('#imgmenuIcon').attr('src', ApplicaionIconPath + '/' + _Name);
        $('#imgProcessIcon').attr('src', ApplicaionIconPath + '/' + _Name);
        if (_Name == ApplicaionIconImage) {
            $('#MenuIconName').html('ApplicaionIconName(Default)');
            $('#ProcessIconName').html('ApplicaionIconName(Default)');
        }
        else {
            $('#MenuIconName').html(_Name.split('.')[0]);
            $('#ProcessIconName').html(_Name.split('.')[0]);
        }
    }

}
function setMenuIconOnClear() {
    $('#imgmenuIcon').attr('src', ApplicaionIconPath + '/' + ApplicaionIconImage);
    $('#MenuIconName').html(ApplicaionIconName + '(Default)');
    $('#imgProcessIcon').attr('src', ApplicaionIconPath + '/' + ApplicaionIconImage);
    $('#ProcessIconName').html(ApplicaionIconName + '(Default)');
}

function PreLoadImages(_ImagesArray) {
    $(_ImagesArray).each(function () {
        var img = $('<img/>');
        img[0].src = ApplicaionIconPath + '/' + this;

        img
        .css('width', 32)
        .css('height', 32);
    });
}

function PreLoadImagesFromArray(_ImageArray, _StartIndex, _EndIndex, _Length) {
    for (var i = _StartIndex; i <= _EndIndex + 1; i++) {
        if (i < _Length) {
            var img = $('<img/>');
            img[0].src = ApplicaionIconPath + '/' + _ImageArray[i];

            img
        .css('width', '32px')
        .css('height', '32px');
        }
    }
}

function MenuCatDeleteClick() {
    $('[id$=hdfDelType]').val('4');
    $('#aDelCfmMsg').html('Are you sure you want to delete this menu category ?'); SubProcImageDelConfirnmation(true, 'Delete Menu Category');
}


function SubProcImageDelConfirnmation(_bol, _title) {
    SubProcCommonMethod('SubProcImageDelConfirnmation', _bol, '<div><div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/warning.png"/></div><div style="float:left;font-weight:700;">  ' + _title + '</div></div><div style="clear: both;"></div>', 300);
}


function SubProcCommonMethod(_Id, _Bol, _Title, _Width) {
    if (_Bol)  showModalPopUp(_Id, _Title, _Width, false); 
    else  $('#' + _Id).dialog('close');
}


function SubProcBoxMessage(_bol) {
    if (_bol) {
        showModalPopUp('#SubProcBoxMessage', '<div><div style="float:left;margin-right:5px;max-height:20px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div><div style="float:left;font-weight:700;">Error Message</div></div><div style="clear: both;"></div>', 400);
        $('#btnOkErrorMsg').focus();
    }
    else  $('#SubProcBoxMessage').dialog('close');
}

var TabEnum = {
    DEVICE: 1,
    DATA_COMMAND: 2,
    DATA_COMMAND1: 3

}
function SubProcAppInfo(_bol) {
    SubProcCommonMethod('SubProcAppInfo', _bol, $('[id$=hidoverview]').val(), 450);
}

function SubProcPublishApp(_bol) {
    SubProcCommonMethod('SubProcPublishApp', _bol, '', 200);
}

function storeSelectedTabIndex(index) {
    $( $('[id*=hidTabSelected]')).val(index);
}

function SubProcGroupAddRemove(_bol) {
    SubProcCommonMethod('divgroupsinformation', _bol, 'Group Details', 500);
}

function showSelectedTabOnPostBack() {
    $('#PageCanvasContent').removeClass("hides");
    var hidSelectedTabIndex = $('[id*=hidTabSelected]');
    //this is the first tab and associated div
    var lstDeviceAccount = $('#lsttabCategory');
    var divDeviceSetting = $('#DeviceSetting');
    //this is the second tab and associated div
    var lstDataCommandSetting = $('#lstTabUsersGroups');
    var divDataCommandSetting = $('#DataCommandSettingsDiv');
    //this is third tab and associated divG
    var lstDataCommandSetting1 = $('#lsttabPublished');
    var divDataCommandSetting1 = $('#DataCommandSettingsDiv2');


    switch (parseInt($(hidSelectedTabIndex).val(), 10)) {
        case TabEnum.DEVICE:
            lstDeviceAccount
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

            lstDataCommandSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            lstDataCommandSetting1
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            divDeviceSetting.removeClass("ui-tabs-hide");
            divDataCommandSetting.addClass("ui-tabs-hide");
            divDataCommandSetting1.addClass("ui-tabs-hide");            break;
        case TabEnum.DATA_COMMAND:
            lstDeviceAccount.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            lstDataCommandSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

            lstDataCommandSetting1.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            divDeviceSetting.addClass("ui-tabs-hide");
            divDataCommandSetting.removeClass("ui-tabs-hide");
            divDataCommandSetting1.addClass("ui-tabs-hide");
            break;

        case TabEnum.DATA_COMMAND1:
            lstDeviceAccount.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            lstDataCommandSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            lstDataCommandSetting1.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

            divDeviceSetting.addClass("ui-tabs-hide");
            divDataCommandSetting.addClass("ui-tabs-hide");
            divDataCommandSetting1.removeClass("ui-tabs-hide");
            break;
    }
}

function RptMenuCatRowClick(e, _type) {
    $.each($(e.parentElement)[0].children, function () {
        if (this.title == "id") {
            $('[id$=hdfRptRowIndex]').val(_type + ',' + this.innerHTML.trim());
        }
    });
    row(e);
}

function SetUpdatedDivType(_Type, _Name) {
    $('[id$=hdfRptRowIndex]').val(_Type + ',' + _Name);
    UpdateUpdBtnRowIndex();
}

function UpdateUpdBtnRowIndex() {
    $('[id$=btnDbConnRowIndex]').click();
}

function formPublishVersionsList(csvVersions,currVersion) {
    if (csvVersions == "") {
        $('#lblmsg56').show();
        $('[id$="btnUnpublish"]').hide();
        $('[id$="btnPulishAppSave"]').hide();
        $('#divPublishedVersions').html('');
        return;
    }
    else {
        var aryVersions = csvVersions.split(',');
        if (aryVersions.length > 0) {
            var strHtml = "<table><tbody>";
          
            for (var i = 0; i <= aryVersions.length - 1; i++) {
                var strVersionToDisplay = aryVersions[i];
                if (strVersionToDisplay == currVersion) {
                    strVersionToDisplay = currVersion +" " + "(Current Version)";
                }
                strHtml += "<tr><td><input type=\"radio\" id=\"radVer" + i + aryVersions[i] + "\"" + " name=\"versions\" value=\"" + aryVersions[i] + "\"" + ">" + strVersionToDisplay + "</td></tr>";
            }
            strHtml += "</tbody></table>"; 
        }

        $('#divPublishedVersions').html(strHtml);
        var $radios = $('#SubProcPublishApp').find('input:radio[name=versions]');
        $('[id$="btnUnpublish"]').show();

        if (currVersion != null)
            $radios.filter('[value=\"' + currVersion + '\"]').attr('checked', true);

        if (currVersion == "Not published")
            $('[id$="btnUnpublish"]').hide();

        $('#SubProcPublishApp').find('input:radio').uniform();
        $('#lblmsg56').hide();       
        $('[id$="btnPulishAppSave"]').show();        
    }
}
function setSelectedVersionForSaving() {    
    $( $('[id$="hdfselectedVersion"]')).val($('#SubProcPublishApp').find('input:radio[name=versions]:checked').val());
}

function processSaveNewVersion() {
    var confirmResult = confirm('Are you sure you want to sure publish App?');
    if (confirmResult) {
        setSelectedVersionForSaving();
        return true;
    }
    else {
        return false;
    }
}

// App Defined in the Avilable Apps
function appdefined(e) {
    var existApparr, existAppIDarr, CategoryID;
    CategoryID = $($(e).parents("tr:first").find('[id$=lblMenuCategoryId]')).text();
    existApparr = $($(e).parents("tr:first").find('[id$=LiteralName]')).text().split(",");
    existAppIDarr = $($(e).parents("tr:first").find('[id$=literralwdids]')).text().split(",");
    categoryOldAppList = existApparr;
    var strDisplay = '', divid = '';
    for (var i = 0; i <= existApparr.length - 1; i++) {
        strDisplay = existApparr[i];
        if (strDisplay == undefined || strDisplay == null || strDisplay == 'NULL' || strDisplay == '') {
            break;
        }
        else {
            divid = "div_" + i;
            $('[id$=divexistappchild]').after("<div id=" + divid + "></div>");
            $('#' + divid).append("<span id=\"" + "@WF_NAME_" + i + "\">" + existApparr[i] + "</span>");
            $('#' + divid).append("<span id=\"" + "@WF_ID_" + i + "\"" + "class=\"hides\">" + existAppIDarr[i] + "</span>");
            $('#' + divid).append("<span id=\"" + "@CATEGORY_ID_" + i + "\"" + "class=\"hides\">" + CategoryID + "</span>");
            $('#' + divid).append("<a id=\"" + "lnkUser" + "_" + existApparr[i] + "_" + existAppIDarr[i] + "\"" + "class=\"remove\"" + "onclick=\"moveUserToOtherAvailableUser(this);\"" + ">" + "</a>");
        }
    }
};

function existingApps(e) {
    var avilApparr, avilappwfid, divid1;

    avilApparr = $(e).parents("tr:first").find('[id$=litNotApp]').text().split(","); 
    
    avilappwfid = $(e).parents("tr:first").find('[id$=Literal3]').text().split(",");

    for (var k = 0; k <= avilApparr.length - 1; k++) {
        divid1 = "";
        if (avilApparr[k] != undefined && avilApparr[k] != null && avilApparr[k] != '' && avilApparr[k] != 'NULL') {
            divid1 = "div_a_" + "_" + k;
            $('[id$=divavilableappchild]').after("<div id=" + divid1 + "></div>");
            $('#' + divid1).append("<span id=\"" + "@WF_NAME_a_" + k + "\">" + avilApparr[k] + "</span>");
            $('#' + divid1).append("<span id=\"" + "@WF_ID_a_" + k + "\"" + "class=\"hides\">" + avilappwfid[k] + "</span>");
            $('#' + divid1).append("<span id=\"" + "@CATEGORY_a_ID_" + k + "\"" + "class=\"hides\">" + "</span>");
            $('#' + divid1).append("<a id=\"" + ("lnkUser" + "_a_" + avilApparr[k] + "_" + avilappwfid[k]) + "\"" +
             "class=\"add\"" + "onclick=\"moveUserToExistingUser(this);\"" + ">" + "</a>");
        }
    }
}

function row(sender) {    
    var v1 = $($(sender).parents("tr:first").find('[id*=lblCategoryNm]')).text();
    $('[id$=hidupdatecategoryinfoname]').val(v1);
    $('[id$=txtupdatectegryinfo]').val(v1);

    v1 = $($(sender).parents("tr:first").find('[id*=lblMenuCategoryId]')).text();
    $('[id$=hidupdatecategoryinfoid]').val(v1);
    cleardiv();
    getTheFinalDiaplayIndexOfCatForDBSave();
    appdefined(sender);
    existingApps(sender);
    SubProcMenuCategoryToday(true);
}

function cleardiv() {
    var savediv = '';
    savediv = $('[id$=divExistingUsers]').find('#divexistappchild');
    $('[id$=divExistingUsers]').children().remove();
    $('[id$=divExistingUsers]').append(savediv);
    savediv = '';
    savediv= $('[id$=divOtherAvailableUsers]').find('#divavilableappchild');
    $('[id$=divOtherAvailableUsers]').children().remove();
    $('[id$=divOtherAvailableUsers]').append(savediv);
}

//---------------------------------------------------
function processSaveAppsviaGroup() {
    getExistingAppForSaving();
   // getOtherAvailableAppForSaving();
}

function moveDown(sender) {
    var rowToMove = $(sender).parents('tbody.MoveableBody:first');
    var next = rowToMove.next('tbody.MoveableBody')
    if (next.length == 1) { next.after(rowToMove); }
    _showUpDownArrowInTable();
}
function moveUp(sender) {
    var rowToMove = $(sender).parents('tbody.MoveableBody:first');
    var prev = rowToMove.prev('tbody.MoveableBody')
    if (prev.length == 1) { prev.before(rowToMove); }
    _showUpDownArrowInTable();

}
function getTheFinalDiaplayIndexOfCatForDBSave() {
    var hidFinalDisplayIndex = $('[id$="hidFinalDisplayIndexOfCat"]');
    $(hidFinalDisplayIndex).val("");
    var tbodyMenuCatDtls = $('#tblMenuCatDtls tbody.MoveableBody');
    if (tbodyMenuCatDtls && tbodyMenuCatDtls.length > 0) {
        var strMenuCatValByIndex = $(hidFinalDisplayIndex).val();
        $.each(tbodyMenuCatDtls, function (index, value) {
            var lblMenuCategoryId = $(value).find('[id$="lblMenuCategoryId"]');
            if (strMenuCatValByIndex == "") {
                strMenuCatValByIndex += $(lblMenuCategoryId).text();
            }
            else {
                strMenuCatValByIndex += ";" + $(lblMenuCategoryId).text();
            }
        });
        $(hidFinalDisplayIndex).val(strMenuCatValByIndex);
    }

}
//-------------------------------

function _showUpDownArrowInTable() {
    categoryOldAppList = $($('[id$="hidexistingappingroup"]')).val();  
    var tb1 = $("#tblMenuCatDtls");
    var noOfRows = $("#tblMenuCatDtls").find("tbody tr.repeaterAlternatingItem,tbody tr.repeaterItem").length;
    var count = 0;
    if (noOfRows === 1) {
        $("#tblMenuCatDtls").find("tbody tr.repeaterAlternatingItem,tbody tr.repeaterItem").each(function () {
            $(this).find(".moveUp16x16,.moveDown16x16").hide();
        });
    }
    else if (noOfRows === 2) {
        $("#tblMenuCatDtls").find("tbody tr.repeaterAlternatingItem,tbody tr.repeaterItem").each(function () {
            if (count === 0) {
                $(this).find(".moveDown16x16").show();
                $(this).find(".moveUp16x16").hide();
            }
            else {
                $(this).find(".moveDown16x16").hide();
                $(this).find(".moveUp16x16").show();
            }
            count++;
        });
    }
    else {
        $("#tblMenuCatDtls").find("tbody tr.repeaterAlternatingItem,tbody tr.repeaterItem").each(function () {
            if (count === 0) {
                $(this).find(".moveDown16x16").show();
                $(this).find(".moveUp16x16").hide();
            }
            else if (count === noOfRows - 1) {
                $(this).find(".moveDown16x16").hide();
                $(this).find(".moveUp16x16").show();
            }
            else {
                $(this).find(".moveDown16x16").show();
                $(this).find(".moveUp16x16").show();
            }
            if (count % 2 > 0) {
                $(this).removeClass('repeaterAlternatingItem').removeClass('repeaterItem').addClass('repeaterAlternatingItem');
            }
            else {
                $(this).removeClass('repeaterAlternatingItem').removeClass('repeaterItem').addClass('repeaterItem');
            }
            count++;
        });
    }
}

//---------------------------------------
function processDeleteGroup() {
    var confirmReturn = confirm('Are you sure you want to delete this Apps');
    if (confirmReturn) {
        getExistingUsersForSaving();
        //getOtherAvailableUsersForSaving();
        return true; //allow postback
    }
    else {
        return false; //prevent postback
    }
}
function processSaveGroup() {
    getExistingUsersForSaving();
    //getOtherAvailableUsersForSaving();
    return true;
}
//       Category Edit//
function EditCategory(editBotton) {
    var v = $(editBotton).parent().parent().find("[id*='lblCategoryNm']");
    var v1 = $(editBotton).parent().parent().find("[id*='lblMenuCategoryId']");
    $('[id$=hidMenuCategoryForEditCategoryid]').val($(v).text());
    $('[id$=hidMenuCategoryForEdit123]').val($(v1).text());
    $('[id$=txtCategoryName]').val($(v).text());
    $('[id$=txtcategoryid]').val($(v1).text());
    $('[id$=MenuCatDetailHeaderDiv123]').hide();
    $('#lnkMenuCat_Back').hide();
    $('#imgmenuIcon').attr('src', ApplicaionIconPath + '/' + ApplicaionIconImage);
    $('[id$=btnDeleteMenuCategory]').hide();
    $('#MenuIconName').html('application(Default)');
    $('#EditMenuCatDetailsDiv').show();
    $('#MenuCatDetailsDiv').hide();
    EditSubProcMenuCategory(true);

}
//Confirmation message popup
function SubProcConfirmBoxMessage(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', '<div><div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div><div style="float:left;font-weight:700;">  ' + _title + '</div></div><div style="clear: both;"></div>', _width);
        $('#btnCnfFormSave').focus();
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}
