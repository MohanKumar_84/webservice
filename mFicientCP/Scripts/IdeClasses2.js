﻿
function ControlNew(options) {
    if (!options) options = {};
    this.location = options.loc;
}

function Control(options/*object*/) {
    if (!options) options = {};
    this.location = options.location;
    this.legend = options.legend;
    this.userDefinedName = options.userDefinedName;
    this.id = options.id;
    this.label = options.label;
    this.conditionalDisplay = options.condDisplay;
    this.conditionalControl = options.condCntrl;
    this.imageHeight = options.imageHeight;
    this.imageWidth = options.imageWidth;
    this.txtRowsCount = options.txtRowsCount; //row no of lines
    this.dfltValue = options.dfltValue;
    this.marginLeft = options.marginLeft;
    this.regularExp = options.regularExp;
    this.widthInPercent = options.widthInPercent;
    this.minWidth = options.minWidth;
    this.length = options.length;
    this.maxLength = options.maxLength;
    this.required = options.required;
    this.bindType = options.bindType;
    this.rowClickable = options.rowClickable;
    this.rowId = options.rowId;
    this.title = options.title; //title
    this.titlePosition = options.titlePosition; //title
    this.displayText = options.displayText;
    this.displayVal = options.displayVal;
    this.buttonType = options.buttonType;
    this.nextPage = options.nextPage;
    this.listCntrlType = options.listCntrlType; // Tanika //List Type
    this.headerTxt = options.headerTxt;
    this.footerTxt = options.footerTxt;
    this.datePickType = options.datePickType;
    this.fitToWidth = options.fitToWidth; //for image
    this.useOriginalSize = options.useOriginalSize; // for image
    this.imageSource = this.imageSource;
    this.imageBindType = this.imageBindType;
    this.imageFixedType = this.imageFixedType;
    this.imageDataBindType = this.imageDataBindType;
    this.source = options.source;
    this.validationType = options.validationType;
    //Array of ManualBindOptions
    this.bindOptions = options.bindOptions;
    //Value 0 or 1
    this.showLegend = options.showLegend;
    this.labelThreshold = options.labelThreshold;
    //Stack type 0 or clustered type 1
    this.barGraphType = options.barGraphType;
    this.barGraphPadding = options.barGraphPadding;
    this.barGraphMargin = options.barGraphMargin;
    //Bar graph Direction 0 vertical 1= horizontal
    this.barGraphDir = options.barGraphDir;
    //SHows the Interval of graph.By default it is 1
    this.graphShowTicks = options.graphShowTicks;
    this.xAxisLabel = options.xAxisLabel;
    this.yAxisLabel = options.yAxisLabel;
    //array of DatabindingObj objects
    this.databindObjs = options.databindObjs;
    this.rowIdMappingCol = options.rowIdMappingCol;
    this.imageMappingCol = options.imageMappingCol;
    this.titleMappingCol = options.titleMappingCol;
    this.subTitle = options.subTitle;
    this.infoTxt = options.infoTxt;
    this.additionalInfoTxt = options.additionalInfoTxt;
    this.countBubble = options.countBubble;
    this.dataInsetAttr = options.dataInsetAttr;
    this.dataGroupingAllowed = options.dataGroupingAllowed;
    this.countBubbleShow = options.countBubbleShow;
    this.actionBtnAllowed = options.actionBtnAllowed;
    //Object of ListViewButton
    this.actionButton1 = options.actionButton1;
    //Object of ListViewButton
    this.actionButton2 = options.actionButton2;
    //Object of ListViewButton
    this.actionButton3 = options.actionButton3;
    this.minVal = options.minVal;
    this.maxVal = options.maxVal;
    this.incrementOnScroll = options.incrementOnScroll;
    this.promptText = options.promptText;
    this.promptVal = options.promptVal;
    this.type = options.type;
    this.manualEntry = options.manualEntry;
    this.noOfSeries = options.noOfSeries;
    this.series2MappingColVal = options.series2MappingColVal;
    this.series3MappingColVal = options.series3MappingColVal;
    this.series4MappingColVal = options.series4MappingColVal;
    this.series5MappingCol = options.series5MappingCol;
    this.series1Title = options.series1Title; //Series 1 Label
    this.series2Title = options.series2Title;
    this.series3Title = options.series3Title;
    this.series4Title = options.series4Title;
    this.series5Title = options.series5Title;
    this.series1Factor = options.series1Factor;
    this.series2Factor = options.series2Factor;
    this.series3Factor = options.series3Factor;
    this.series4Factor = options.series4Factor;
    this.series5Factor = options.series5Factor;
    //The decimal places to which value is considered For Series
    this.noOfDecimalPoint = options.noOfDecimalPoint;
    this.valMask = options.valMask;
    this.errorMsg = options.errorMsg;
    this.addButtonText = options.addButtonText;
    this.submitButtonText = options.submitButtonText;
    this.editButtonText = options.editButtonText;
    this.addFormTitle = options.addFormTitle; //tag = atitle
    this.editFormTitle = options.editFormTitle; //tag = etitle
    this.viewFormTitle = options.viewFormTitle; //tag = vtitle
    this.fontStyle = options.fontStyle;
    this.fontSize = options.fontSize;
    this.fontAlign = options.fontAlign;
    this.minItems = options.minItems;
    this.maxItems = options.maxItems;
    this.confirmAddUpdate = options.confirmAddUpdate;
    //Array of EditableListItem
    this.editableListProps = options.editableListProps;
    this.propMapped2Title = options.propMapped2Title; //txt
    this.titlePrefix = options.titlePrefix;
    this.titleSuffix = options.titleSuffix;
    this.titleFactor = options.titleFactor;
    this.additionalTitleText = options.additionalTitleText;
    this.additionalTitlePrefix = options.additionalTitlePrefix;
    this.additionalTitleSuffix = options.additionalTitleSuffix;
    this.additionalTitleFactor = options.additionalTitleFactor;
    this.propMapped2SubTitle = options.propMapped2SubTitle; //dtxt
    this.subTitlePrefix = options.subTitlePrefix;
    this.subTitleSuffix = options.subTitleSuffix;
    this.subTitleFactor = options.subTitleFactor;
    this.additionalSubTtl = options.additionalSubTtl;
    this.additionalSubTtlPrfx = options.additionalSubTtlPrfx;
    this.additionalSubTtlSuffx = options.additionalSubTtlSuffx;
    this.additionalSubTtlFactor = options.additionalSubTtlFactor;
    this.propMapped2Value = options.propMapped2Value;
    this.propMapped2Text = options.propMapped2Text;
    //this.labelValSuffix = options.labelValSuffix;
    //this.labelValPrefix = options.labelValPrefix;
    this.labelText = options.labelText;
    this.multiplicationFactor = options.multiplicationFactor;
    this.text = options.text;
    this.enablePaging = options.enablePaging;
    this.pagingRecordCount = options.pagingRecordCount;
    this.validationRange = options.validationRange;
    //array of TableColumns
    this.tableColumns = options.tableColumns; //tblcols
    this.addEditDelPermissions = options.addEditDelPermissions; //perm as "101" ,
    //List of string 
    this.onChange = options.onChange; //onchng
    this.titleAlign = options.titleAlign; //taln
    this.wrap = options.wrap; //tag = wrp
    this.help = options.help; //tag = hlp
    this.helpTxt = options.helpTxt; //tag = hlpt
    this.titleAlign = options.titleAlign; //no tag found but used in ui
    this.fetchLocation = options.fetchLocation; //tag = floc
    this.refreshLink = options.refreshLink; //tag = rlnk
    this.autoRefreshTime = options.autoRefreshTime; //tag = atref
    this.valueAs = options.valueAs; //tag = valas
    this.dataLblThreshold = options.dataLblThreshold; //tag = lblth
    this.condDisplayCntrlProps = options.condDisplayCntrlProps; //tag = adv
    this.placeholder = options.placeholder; //tag = ptxt
    this.dataTitle = options.dataTitle; //tag = boldtxt
    this.smallText = options.smallText; //tag = smalltxt
    this.countField = options.countField; //tag = cntfl
    this.align = options.align;
    this.emptyListMsg = options.emptyListMsg; //tag = dsptxt;
    this.additionalTitle = options.additionalTitle; //tag = adttxt
    this.isDeleted = options.isDeleted;
    this.isModified = options.isModified;
    this.xRef = options.xRef;

    //Toggle
    this.switchText = this.switchText;
    this.defaultState = this.defaultState;
    this.refreshControls = this.refreshControls;

    //Barcode
    this.barcodeType = this.barcodeType;

    //Location
    this.isHidden = this.isHidden;
    this.allowUsages = this.allowUsages;
}
Control.prototype.propSheetHtmlMapping = PROP_JSON_HTML_MAP.control;
Control.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
Control.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
Control.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
Control.prototype.fnClearAllDatabindObjs = function (databindObj/*DatabindingObj object*/) {
    this.databindObjs = [];
}

Control.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
Control.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
Control.prototype.fnAddListViewButton = function (index/*Pass 1 2 0r 3*/, lstViewBtn/*ListViewButton*/) {
    this.fnDeleteListViewButton(index);
    if (index && lstViewBtn && lstViewBtn instanceof ListViewButton) {
        switch (index) {
            case "1":
                this.button1 = lstViewBtn;
                break;
            case "2":
                this.button2 = lstViewBtn;
                break;
            case "3":
                this.button3 = lstViewBtn;
                break;
        }
    }
}
Control.prototype.fnDeleteListViewButton = function (index/*Pass 1 2 0r 3*/) {
    if (index) {
        switch (index) {
            case "1":
                this.button1 = {};
                break;
            case "2":
                this.button2 = {};
                break;
            case "3":
                this.button3 = {};
                break;
        }
    }
}
Control.prototype.fnEditListViewButton = function (index/*Pass 1 2 0r 3*/, lstViewBtn/*ListViewButton*/) {
    this.fnAddListViewButton(index, lstViewBtn);
}

Control.prototype.fnAddEditableListProps = function (editListProps /*EditableListItem[]*/) {
    if (!this.editableListProps || !$.isArray(this.editableListProps)) {
        this.editableListProps = [];
    }
    if ($.isArray(editListProps)) {
        var self = this;
        $.each(editListProps, function (index, value) {
            if (value && value instanceof EditableListItem) {
                self.editableListProps.push(value);
            }
        });
    }
}
Control.prototype.fnEditEditableListProps = function (editListProp /*EditableListItem*/) {
    //TODO TEST 
    this.fnDeleteEditableListProps();
    this.fnAddEditableListProps(editListProp);
}
Control.prototype.fnClearAllEditableListProps = function () {
    this.editListProps = [];
}

Control.prototype.fnAddTableCol = function (tableCol /*TableColumn []*/) {
    if (!this.tableColumns || !$.isArray(this.tableColumns)) {
        this.tableColumns = [];
    }
    if (tableCol && tableCol instanceof TableColumn) {
        this.tableColumns.push(tableCol);
    }
}
Control.prototype.fnAddTableCols = function (tableCols /*TableColumn []*/) {
    this.fnDeleteAllTableCols();
    if (!this.tableColumns || !$.isArray(this.tableColumns)) {
        this.tableColumns = [];
    }
    $.each(tableCols, function (index, value) {
        if (value && value instanceof TableColumn) {
            this.tableColumns.push(tableCol);
        }
    });
}
Control.prototype.fnDeleteAllTableCols = function () {
    this.tableColumns = [];
}

Control.prototype.fnAddOnChange = function (changeVal /*string*/) {
    if (!this.onChange || !$.isArray(this.onChange)) {
        this.onChange = [];
    }
    if (change) {
        this.onChange.push(changeVal);
    }
}
Control.prototype.fnGetJsonForSavingByCntrlType = function () {

}
//---------------------------------------------------------------------------------------------------------------------------------------------


/*
Command Details 
*/
function DatabindingObj(id/*string*/,
                        cmdParams/*array of DatabindingObjParams*/,
                        index /*string*/,
                        bindObjType /*string*/,
                        name /*string*/,
                        dsName/*string*/,
                        tempUiIndex/*optional if not provided savme as index*/,
                        usrName/*optional string*/,
                        pwd/*optional string*/,
                        options/*object*/) {

    /*options :{
    offlineTblCmdType string DBCommandType Enum
    condLogic: ary Of ConditionalLogic
    }*/
    if (!options) options = {};
    this.index = index;
    this.bindObjType = bindObjType;
    this.name = name;
    this.dsName = dsName;
    this.id = id; //command id
    this.cmdParams = cmdParams;
    this.offlineTblCmdType = options.offlineTblCmdType;
    this.condLogic = options.condLogic;
    this.usrName = usrName ? usrName : "";
    this.pwd = pwd ? pwd : "";
    this.ignoreCache = options.ignoreCache;
    if (tempUiIndex) {
        //used for showing cmds in UI which can be moved (up down changing index) in view
        //but before final saving.
        this.tempUiIndex = tempUiIndex;
    }
    else {
        this.tempUiIndex = index;
    }
}
DatabindingObj.prototype.fillProperties = function (json) {
    if (json) {
        var jsonObj = MF_HELPERS.getJsonObject(json);
        var i = 0;
        if (jsonObj) {
            this.id = jsonObj.cmd;
            this.bindObjType = jsonObj.cmdtyp;
            this.name = jsonObj.cname;
            this.index = jsonObj.idx;
            this.tempUiIndex = jsonObj.idx;
            this.cmdParams = [];
            if (jsonObj.lp && $.isArray(jsonObj.lp) && jsonObj.lp.length > 0) {
                for (i = 0; i <= jsonObj.lp.length - 1; i++) {
                    var objDatabindObjParams = new DatabindingObjParams();
                    objDatabindObjParams.fillProperties(jsonObj.lp[i]);
                    this.fnAddCmdParams(objDatabindObjParams);
                }
            }
            this.dsName = jsonObj.ctrlid;
        }
    }
}
DatabindingObj.prototype.fnResetObjectDetails = function () {
    var cmdParams = this.cmdParams;
    this.cmdParams = [];
    var i = 0;
    if (cmdParams != undefined || cmdParams != null) {
        for (i = 0; i <= cmdParams.length - 1; i++) {
            var cmdParam = cmdParams[i];
            var objDatabindObjParams = new DatabindingObjParams(cmdParam.id, cmdParam.name,
                cmdParam.type, cmdParam.val);
            this.fnAddCmdParams(objDatabindObjParams);
        }
    }
};
DatabindingObj.prototype.fnAddCmdParams = function (databindObjParam) {
    if (!this.cmdParams || !$.isArray(this.cmdParams)) {
        this.cmdParams = [];
    }
    if (databindObjParam && databindObjParam instanceof DatabindingObjParams) {
        this.cmdParams.push(databindObjParam);
    }
};
DatabindingObj.prototype.fnRemoveCmdParams = function () {
    var params = this.cmdParams;
    var _this = this;
    if (params.length > 0) {
        $.each(params, function () {
            _this.cmdParams.pop(this);
        });
    }
};
// DatabindingObj.prototype.fnGetCmdParams = function (value) {
//     return this.cmdParams;
// };
DatabindingObj.prototype.fnSetCmdParams = function (value) {
    this.cmdParams = value;
};
DatabindingObj.prototype.fnDeleteCmdParams = function () {
    this.fnSetCmdParams([]);
};
DatabindingObj.prototype.fnGetCommandName = function () {
    return this.name;
};
DatabindingObj.prototype.fnGetBindTypeObject = function () {
    var iBindType = this.bindObjType;
    return MF_HELPERS.getCommandTypeByValue(iBindType);
};
DatabindingObj.prototype.fnGetBindTypeValue = function () {
    return this.bindObjType;
};
DatabindingObj.prototype.fnSetCacheIgnoreValue = function (value) {
    this.ignoreCache = value;
};
//MOHAN 
DatabindingObj.prototype.fnGetId = function () {
    return this.id;
};
DatabindingObj.prototype.fnGetCmdParams = function () {
    return this.cmdParams;
};
DatabindingObj.prototype.fnGetName = function () {
    return this.name;
};
DatabindingObj.prototype.fnSetName = function (value) {
    this.name = value;
};
DatabindingObj.prototype.fnRenameViewCntrlInCmdParam = function (viewName, oldCntrlName, newCntrlName) {
    if (!viewName || !oldCntrlName || !newCntrlName) return;
    var aryCmdParams = this.fnGetCmdParams();
    if (aryCmdParams && $.isArray(aryCmdParams) && aryCmdParams.length > 0) {
        for (var i = 0; i <= aryCmdParams.length - 1; i++) {
            var objCmdParam = aryCmdParams[i];
            objCmdParam.fnRenameViewCntrlInVal(viewName, oldCntrlName, newCntrlName);
        }
    }
};
DatabindingObj.prototype.fnDeleteViewCntrlInCmdParam = function (viewName, cntrlName) {
    if (!viewName || !cntrlName) return;
    var aryCmdParams = this.fnGetCmdParams();
    if (aryCmdParams && $.isArray(aryCmdParams) && aryCmdParams.length > 0) {
        for (var i = 0; i <= aryCmdParams.length - 1; i++) {
            var objCmdParam = aryCmdParams[i];
            objCmdParam.fnDeleteViewCntrlInVal(viewName, cntrlName);
        }
    }
};
DatabindingObj.prototype.fnGetOfflineTblCmdTypeVal = function () {
    return this.offlineTblCmdType;
};
DatabindingObj.prototype.fnSetOfflineTblCmdTypeVal = function (value /*MF_IDE_CONSTANTS.DBCommandType*/) {
    this.offlineTblCmdType = value;
};
DatabindingObj.prototype.fnGetConditionalLogic = function () {
    return this.condLogic;
};
DatabindingObj.prototype.fnSetConditionalLogic = function (value) {
    this.condLogic = value;
};
DatabindingObj.prototype.fnDeleteConditionalLogic = function () {
    this.fnSetConditionalLogic(null);
};
DatabindingObj.prototype.fnGetDatasetName = function () {
    return this.dsName;
};
DatabindingObj.prototype.fnSetDatasetName = function (value) {
    this.dsName = value;
};
DatabindingObj.prototype.fnGetUserName = function () {
    return this.usrName;
};
DatabindingObj.prototype.fnSetUserName = function (value) {
    this.usrName = value;
};
DatabindingObj.prototype.fnGetPassword = function () {
    return this.pwd;
};
DatabindingObj.prototype.fnSetPassword = function (value) {
    this.pwd = value;
};
DatabindingObj.prototype.fnGetIndex = function () {
    return this.index;
};
DatabindingObj.prototype.fnSetIndex = function (value) {
    this.index = value;
};
DatabindingObj.prototype.fnGetTempUiIndex = function () {
    return this.tempUiIndex;
};
DatabindingObj.prototype.fnSetTempUiIndex = function (value) {
    this.tempUiIndex = value;
};
//MOHAN
/*
Parameters for running the command
*/
function DatabindingObjParams(id, name/*string*/, type/*string*/, val/*string*/) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.val = val;
}
DatabindingObjParams.prototype.fillProperties = function (json) {
    if (json) {
        var jsonObj = MF_HELPERS.getJsonObject(json);
        if (jsonObj) {
            this.name = jsonObj.para;
            this.type = ""; //TODO Check if tag is available
            this.val = jsonObj.val;
        }
    }
}
//MOHAN
DatabindingObjParams.prototype.fnRenameViewCntrlInVal = function (viewName, oldName, newName) {
    if (!viewName || !oldName || !newName) return;
    var strOldValue = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(viewName + '.' + oldName) + ".";
    var strNewValue = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(viewName + '.' + newName) + ".";
    if (this.val && this.val.startsWith(strOldValue)) {
        this.val = this.val.replace(strOldValue, strNewValue);
    }
}
DatabindingObjParams.prototype.fnDeleteViewCntrlInVal = function (viewName, name) {
    if (!viewName || !name) return;
    var strOldValue = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(viewName + '.' + name) + ".";
    if (this.val && this.val.startsWith(strOldValue)) {
        this.val = "";
    }
}
//MOHAN
function ListViewButton(text/*string*/, cmdId/*string*/, cmdType/*string*/, refresh/*bool*/, color/*string*/, cmdParams/*array of DatabindingObjParams*/) {
    this.text = text;
    this.cmdId = cmdId;
    this.cmdType = cmdType;
    this.refresh = refresh;
    this.color = color;
    this.cmdParams = cmdParams;
}

function EditableListItem(name/*string*/, type/*string*/, col/*string*/, unique/*string*/) {
    this.name = name;
    this.type = type;
    this.columns = col;
    this.unique = unique;
}


function ConditionalLogic(column, operator, val) {
    this.column = column;
    this.operator = operator;
    this.val = val;
}

//Tanika
function RowPanel(id/*string*/,
                 type/*string*/,
                 colPanels/*Array of ColumnPanel*/) {
    this.id = id;
    this.type = type;
    this.colmnPanels = colPanels;

    RowPanel.prototype.fnAddColumnPanel = function (colPanel/*ColumnPanel*/) {
        if (!this.colmnPanels || !$.isArray(this.colmnPanels)) {
            this.colmnPanels = [];
        }
        if (colPanel && colPanel instanceof ColumnPanel) {
            this.colmnPanels.push(colPanel);
        }
    }
    RowPanel.prototype.fnGetColumnPanel = function (colPanelId/*ColumnPanelId*/) {
        var objColPanel = null;
        if (this.colmnPanels != null || this.colmnPanels != undefined) {
            for (var col = 0; col <= this.colmnPanels.length - 1; col++) {
                if (this.colmnPanels[col].id == colPanelId)
                    objColPanel = this.colmnPanels[col];
            }
        }
        return objColPanel;
    }
}
//Tanika
function ColumnPanel(index/*string*/,
                    id/*string*/,
                    name/*string*/,
                    display/*bit*/,
                    conditions/*Array of ConditionalLogic*/,
                    controls/*Array of Control*/) {
    this.index = index;
    this.id = id;
    this.name = name;
    this.display = display;
    this.conditions = conditions;
    this.controls = controls;

    ColumnPanel.prototype.fnAddControl = function (control/*Control*/) {
        if (!this.controls || !$.isArray(this.controls)) {
            this.controls = [];
        }
        if (control && control instanceof Control) {
            this.controls.push(control);
        }
    }

    ColumnPanel.prototype.fnGetControl = function (controlId/*ControlId*/) {
        var objControl = null;
        if (this.controls != null || this.controls != undefined) {
            for (var cntrl = 0; cntrl <= this.controls.length - 1; cntrl++) {
                if (this.controls[cntrl].id == controlId)
                    objControl = this.controls[cntrl];
            }
        }
        return objControl;
    }
}

var mfIdeClasses = "mfIdeClasses";

//function AppTransition(options) {
//    if (!options) options = {};
//    this.id = options.id;
//    this.sourceCont = options.sourceCont;
//    this.destinationCont = options.destinationCont;
//    this.sourceFormType = options.sourceFormType;
//    this.sourceType = options.sourceType;
//    this.destinationFormType = options.destinationFormType;
//    this.destinationType = options.destinationType;
//    this.sourceName = options.sourceName;
//    this.sourceId = options.sourceId;
//    this.destinationName = options.destinationName;
//    this.destinationId = options.destinationId;
//}
//AppTransition.prototype.fillProperties = function (json) {
//    if (json) {
//        var jsonObj = MF_HELPERS.getJsonObject(json);
//        this.id = jsonObj.id;
//        this.sourceCont = jsonObj.srccnt;
//        this.destinationCont = jsonObj.dstcnt;
//        this.sourceFormType = jsonObj.srcftyp;
//        this.sourceType = jsonObj.srctyp;
//        this.destinationFormType = jsonObj.dstftyp;
//        this.destinationType = jsonObj.dsttyp;
//        this.sourceName = jsonObj.srcnm;
//        this.sourceId = jsonObj.srcid;
//        this.destinationName = jsonObj.dstnm;
//        this.destinationId = jsonObj.dstid;
//    }
//}
//AppTransition.prototype.getFinalJson = function () {
//    var objTransaction = new Object();
//    objTransaction.id = this.id;
//    objTransaction.srccnt = this.sourceCont;
//    objTransaction.dstcnt = this.destinationCont;
//    objTransaction.srcftyp = this.sourceFormType;
//    objTransaction.srctyp = this.sourceType;
//    objTransaction.dstftyp = this.destinationFormType;
//    objTransaction.dsttyp = this.destinationType;
//    objTransaction.srcnm = this.sourceName;
//    objTransaction.srcid = this.sourceId;
//    objTransaction.dstnm = this.destinationName;
//    objTransaction.dstid = this.destinationId;
//    return JSON.stringify(objTransaction);
//}
//AppTransition.prototype.getFinalObject = function () {
//    var objTransaction = new Object();
//    objTransaction.id = this.id;
//    objTransaction.srccnt = this.sourceCont;
//    objTransaction.dstcnt = this.destinationCont;
//    objTransaction.srcftyp = this.sourceFormType;
//    objTransaction.srctyp = this.sourceType;
//    objTransaction.dstftyp = this.destinationFormType;
//    objTransaction.dsttyp = this.destinationType;
//    objTransaction.srcnm = this.sourceName;
//    objTransaction.srcid = this.sourceId;
//    objTransaction.dstnm = this.destinationName;
//    objTransaction.dstid = this.destinationId;
//    return objTransaction;
//}

