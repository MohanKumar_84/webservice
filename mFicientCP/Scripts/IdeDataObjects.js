﻿var DbCmdParaCount = 1;
var selectdbobjdataset;
var objdataset;
var command;
var permanent;
var discardobject;
var index;
var setclass;
var EditIndex;
var scrollTop;
var dbConectiondetail = [];
var SearchText = "";

//-------Bind Command List data--------------------------------
function LoadDatabaseObjectData() {
    RefreshDbCommand();
    BindDatabaseObjectdata();
    UnformDatabaseObject();
    DataobjectClear();
}

function BindDatabaseObjectdata() {
    $('#divdbdataobject').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tableobj"></table>');
    $('#tableobj').dataTable({
        "language": {
            "lengthMenu": " _MENU_"
        },
        "data": selectdbobjdataset,
        "scrollY": "480px",
        "scrollCollapse": true,
        "paging": false,
        "info": false,
        "lengthMenu": [[20, 25, 30, 45, 55, -1], [20, 25, 30, 45, 55, "All"]],
        "columns": [{ "title": "OBJECT" }, { "title": "CONNECTOR" }, { "title": "TYPE"}]
    });
    $('#tableobj thead').css({ "display": "none" ,"height":"0px"});
    //$('#example_filter label input[type=text]').val(SearchText);
    DatabaseObjectRowClick();
    var table = $('#tableobj').DataTable();
    table.search(SearchText).draw();
    searchText("tableobj");
    $('#tableobj_length select').prop('id', 'pagesize');
    $('#tableobj_length select').prop('class', 'UseUniformCss');
    CallUnformCss('[id$=pagesize]');
    $('#tableobj_filter').css("padding-bottom", "10px");
}

function OperationDatabaseObjectData() {
    BindDatabaseObjectdata();
    UnformDatabaseObject();
}

function DatabaseObjectRowClick() {
    var table = $('#tableobj').DataTable();
    $('#tableobj tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {

        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var rowData = table.row(this).data();
        if (rowData != undefined) {
            var val = $('[id$=hideditdone]').val();
            if (val == '5') {
                SubModifiction(true);
                viewConnectiontest(rowData[0], $(this).index());
            }
            else {
                ViewDBDisplay(rowData[0]);
                EditIndex = $(this).index();
                scrollTop = $('#tableobj').parent().scrollTop();
                setclass = '';
            }
        }
        else {
            alert('No Database Object Selected');
            $('[id$=viewinfo]').hide();
            $('[id$=EditDbCommandDiv]').show();
        }
    });
}
//-----------------------------

//btnDbCmdSwitchToAdv click event
function BtnAdvQbClick() {
    if ($('#hdfMannualOutColQB').val().length == 0) GetAllAutoboxMValue();
    else $("[id$=hdfAutoboxMannulPara]").val($('#hdfMannualOutColQB').val());
    SubProcAddDbCommandDiv(false);
    ClearHiddenFields();
    isCookieCleanUpRequired('false');
    $('#CompletePage').hide();
}

//Add new database command popup.
function SubProcAddDbCommandDiv(_bol, _Width) {
    if (_bol) {
        if (_Width == undefined)
            showModalPopUp('AddDbCommandDiv', 'Database Object', 590);
        else
            showModalPopUp('AddDbCommandDiv', 'Database Object', _Width);
        if ($($("#AddDbCommandDiv")[0].parentElement).length > 0) {
            $($("#AddDbCommandDiv")[0].parentElement).bind('click', function () {
                $("#dialogPara").dialog("close");
            });
        }
        try {
            $('#AddDbCommandDiv').dialog('option', 'close', function () {
                $("#dialogPara").dialog("close");
            });
        }
        catch (err) {
        }
    }
    else {
        $('#AddDbCommandDiv').dialog('close');
    }
}


//Command edit show div on edit
function ShowDbCmdDivEdit() {
    $('#EditDbCommandDiv').show();
    $('#DbCommandDetails').hide();
}

//Command edit hide show
function HideShowEditCmdBtn(e) {
    if (e == true) {
        $('#DbCmdEditDiv').show();
        $('#DbCmdEditSepDiv').show();
    }
    else {
        $('#DbCmdEditDiv').hide();
        $('#DbCmdEditSepDiv').hide();
    }
}

function RefreshDbCommand() {
    var vtest = $('[id$=hiddbobjdatasource]').val();
    if (vtest == "" || vtest == 'undefined' || vtest == '') {
    }
    else {
        selectdbobjdataset = jQuery.parseJSON(vtest);
        objdataset = jQuery.parseJSON($('[id$=hiddbobjdataset]').val());
    }
    $('[id$=hiddbobjdatasource]').val('');
    $('[id$=hiddbobjdataset]').val('')
}

function AfterDelete() {
    BindDatabaseObjectdata();
    UnformDatabaseObject();
    DataobjectClear();
}

// VIEW DISPLAY
function ViewDBDisplay(id) {
    $('[id$=viewinfo]').show();
    $('[id$=EditDbCommandDiv]').hide();
    intializectrl();
    for (var i = 0; i < objdataset.length; i++) {
        if (id == objdataset[i].DB_COMMAND_NAME) {
            discardobject = objdataset[i].DB_COMMAND_NAME;
            $('[id$=lblDbCmd_Name]').text(objdataset[i].DB_COMMAND_NAME);
            $('[id$=lblDbCmd_ConnName]').text(objdataset[i].CONNECTION_NAME);
            $('[id$=hidname]').val(objdataset[i].DB_COMMAND_NAME);
            $('[id$=hidconid]').val(objdataset[i].DB_COMMAND_ID);
            $('[id$=hdfDbCmdPara]').val(objdataset[i].PARAMETER_JSON);
            $('[id$=hidperconnectionname]').val(objdataset[i].CONNECTION_NAME);
            $('[id$=lblDbCmd_CmdType]').text(objdataset[i].DB_COMMAND);
            $('[id$=lblDbCmd_TableName]').text(objdataset[i].TABLE_NAME);
            $('[id$=lblinput]').text(objdataset[i].PARAMETER);
            $('[id$=lblDbCmd_Query]').text(objdataset[i].SQL_QUERY);
            $('[id$=lbloutput]').text(replaceAll(';', '', objdataset[i].COLUMNS));
            $('[id$=hidimageoutput]').val(replaceAll(';', '', objdataset[i].COLUMNS));
            $('[id$=lblDbCmdDesc]').text(objdataset[i].DESCRIPTION);
            $('[id$=hdtablename]').val(objdataset[i].TABLE_NAME);
            $('[id$=hdfDbType]').val(objdataset[i].DB_COMMAND_NAME);
            $('[id$=hdfDbType1]').val(objdataset[i].DB_COMMAND_NAME);
            $('[id$=lbldbtype]').text(objdataset[i].DB_COMMAND_TYPE);
            $('[id$=hiddbtypetrpe]').text(objdataset[i].DB_COMMAND_TYPE);
            $('[id$=lblcreatedby]').text(objdataset[i].CreatedBY);
            $('[id$=lblupdatedby]').text(objdataset[i].FULL_NAME + ' On  ' + objdataset[i].DateTime);
            var strCache = 'None';
            if (objdataset[i].CACHE == 1) {
                strCache = '';
                strCache += 'Absolute [ ';
                switch (objdataset[i].EXPIRY_FREQUENCY) {
                    case 0:
                        strCache += 'Hour At ' + objdataset[i].EXPIRY_CONDITION + ' Min';
                        break;
                    case 1:
                        strCache += 'Day At ' + objdataset[i].EXPIRY_CONDITION.split(':')[0] + ' Hr : ' + objdataset[i].EXPIRY_CONDITION.split(':')[1] + ' Min';
                        break;
                    case 2:
                        strCache += 'Week On ' + objdataset[i].EXPIRY_CONDITION;
                        break;
                    case 3:
                        strCache += 'Month On Date ' + objdataset[i].EXPIRY_CONDITION;
                        break;
                    case 4:
                        strCache += 'Year On ' + objdataset[i].EXPIRY_CONDITION.split(':')[0] + ' Day : ' + objdataset[i].EXPIRY_CONDITION.split(':')[1] + ' Month';
                        break;
                }
                strCache += ']';
            }
            else if (objdataset[i].CACHE == 2) {
                strCache = '';
                strCache += 'Relative [Every ';
                switch (objdataset[i].EXPIRY_FREQUENCY) {
                    case 0:
                        strCache += objdataset[i].EXPIRY_CONDITION + ' Min';
                        break;
                    case 1:
                        strCache += objdataset[i].EXPIRY_CONDITION + ' Hour';
                        break;
                    case 2:
                        strCache += objdataset[i].EXPIRY_CONDITION + ' Day';
                        break;
                    case 3:
                        strCache += objdataset[i].EXPIRY_CONDITION + ' Week';
                        break;
                    case 4:
                        strCache += objdataset[i].EXPIRY_CONDITION + ' Month';
                        break;
                }
                strCache += ']';
            }
            $('[id$=lblDataCache]').text(strCache);
            var usage = jQuery.parseJSON(objdataset[i].Used);
            $('#DbCmdAppDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblApps"></table>');
            if (usage.length > 0) {
                $('#tblApps').dataTable({
                    "data": usage,
                    "columns": [{ "title": "App" }, { "title": "View(s)" }, { "title": "Form(s)"}],
                    "sort": false,
                    "searching": false,
                    "paging": false,
                    "info": false
                });
            }
            else {
                var node = document.createElement("DIV");
                node.style.paddingLeft = "10px";
                var textnode = document.createTextNode("This object is not used any where.");
                node.appendChild(textnode);
                document.getElementById("DbCmdAppDiv").appendChild(node); //.html("This object is not used any where.");
            }
            break;
        }
    }
}
function replaceAll(find, replace, str) {
    while (str.indexOf(find) > -1) {
        str = str.replace(find, replace);
    }
    return str;
}
function intializectrl() {
    $('[id$=txtcopyobject]').val('');
    $('[id$=lblDbCmd_Name]').text('');
    $('[id$=lblDbCmd_ConnName]').text('');
    $('[id$=hidname]').val('');
    $('[id$=hidconid]').val('');
    $('[id$=hidperconnectionname]').val('');
    $('[id$=lblDbCmd_CmdType]').text('');
    $('[id$=lblDbCmd_ReturnType]').text('');
    $('[id$=lblDbCmd_TableName]').text('');
    $('[id$=lblDbCmd_Query]').text('');
    $('[id$=lblDbCmdDesc]').text('');
    $('[id$=hdtablename]').val('');
    $('[id$=hdfDbType]').val('');
    $('[id$=hdfDbType1]').val('');
    $('[id$=lbldbtype]').text('');
    $('[id$=lblcreatedby]').text('');
    $('[id$=lblupdatedby]').text('');
}

//ADD IDE DATA OBJECT

function AddIdedata() {
    $('[id$=viewinfo]').hide();
    $('[id$=EditDbCommandDiv]').show();
}

// Function Control Uniform Design
function UnformDatabaseObject() {
    CallUnformCss('[id$=ddl_DbConnactor]');
    CallUnformCss('[id$=ddlCommandType]');
    CallUnformCss('[id$=ddlDbCmdTableName]');
    CallUnformCss('[id$=dd1DataCatch]');
    CallUnformCss('[id$=dd1AbMonth]');
    CallUnformCss('[id$=Catcmin]');
    CallUnformCss('[id$=AbEvDayDiv]');
    CallUnformCss('[id$=ddlAbMM]');
    CallUnformCss('[id$=dd1Abhh]');
    CallUnformCss('[id$=Select3]');
    CallUnformCss('[id$=ddlAbDay]');
    CallUnformCss('[id$=dd1AbMnDay]');
    CallUnformCss('[id$=dd1AbMnthDay]');
    CallUnformCss('[id$=ddlAbMnth]');
    CallUnformCss('[id$=dd1AbHr]');
    CallUnformCss('[id$=ddlRlt]');
    CallUnformCss('[id$=SelectddlAbHH]');
    CallUnformCss('[id$=drprltmonth]');
    CallUnformCss('[id$=raltweeks]');
    CallUnformCss('[id$=rltdayd]');
    CallUnformCss('[id$=Drprlthour]');
    CallUnformCss('[id$=drprltmin]');
    CallUnformCss('[id$=Drppoptable]');
    CallUnformCss('[id$=drpstoreprocedure]');
    var dd1AbMonth = $('[id$=dd1AbMonth]');
    if (dd1AbMonth) {
        $(dd1AbMonth).siblings('span').width(35);
    }

    var ddlRlt = $('[id$=ddlRlt]');
    if (ddlRlt) {
        $(ddlRlt).siblings('span').width(35);
    }
}
function EditDBobject() {
    $('#DiveditDBobject').show();
    $('#adddivheader').hide();
    $('#btnDbcmdParaAddDiv').show();
    $('#lblInputParametersDiv').show();
    $('#divParameterInfo').show();
    $('#ddlCommandTypeDiv').show();
    $('#DbTblNameDiv').show();
    $('#ddlDbCmdTableName').show();
    $('#Dbsql').show();
    $('#Div73').show();
    $('#txtDbCommand_SqlQueryWH').show();
    $('#viewinfo').hide();
    $('#add').hide();
    $('#Edit').show();
}

function DisplayUpdateControl() {
    $('#btnDbcmdParaAddDiv').show();
    $('#lblInputParametersDiv').show();
    $('#divParameterInfo').show();
    $('#ddlCommandTypeDiv').show();
    $('#DbTblNameDiv').show();
    $('#ddlDbCmdTableName').show();
    $('#Dbsql').show();
    $('#Div73').show();
    $('#txtDbCommand_SqlQueryWH').show();
}
//Database connector delete link event
function DbConnectorDeleteClick() {
    $('[id$=hdfDelType]').val('1');
    var v = $('[id$=lbldbconnectorid]').text();
    $('[id$=hdfDelId]').val(v);
    $('#aDelCfmMsg').html('Are you sure you want to delete this database connector  ?'); SubProcImageDelConfirnmation(true, 'Delete Database Connector');
}
function CanceBtn() {
    $('#EditDbcmdConnDiv').show();
    $('#editCmdDescDiv').show();

}

function BtnCancel() {
    $('#EditDbCommandDiv').hide();
    BindDatabaseObjectdata();
    $('#ideviewinfo').show();
}

function DataobjectClear() {
    $('[id$=lblEditDbCmd_Name]').val('');
}

//Database connector popup
function SubProcPriview(_bol) {
    if (_bol) {
        showModalPopUp('SubProcPriview', 'Database Connector', 505);
    }
    else {
        $('#SubProcPriview').dialog('close');
    }
}

function showCommand() {
    var drpval = $('[id$=ddl_DbConnactor]').val();
    if (drpval == "-1") {
        $('#ddlCommandTypeDiv').hide();
    }
    else {
        $('#ddlCommandTypeDiv').show();
    }
}

//Command edit show div on edit
function ShowDbCmdDivEdit() {
    $('[id$=viewinfo]').hide();
    $('[id$=EditDbCommandDiv]').show();
}


//show delete message popup
function SubProcBoxMessage(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Error', 400);
        $('#btnOkErrorMsg').focus();
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}

function SubProcBoxMessageDataConection(_bol, Header) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', Header, 400);
        $('#btnOkErrorMsg').focus();
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}

//ddl_DbConnactor change event
function DBCmd_Conn_SelectedIndexChange() {
    var selectedFlag = false;
    var ddlcon = $('select[id$=ddl_DbConnactor]');
    for (var i = 0; i < dbConectiondetail.length; i++) {
        if (dbConectiondetail[i].id == ddlcon.val()) {
            $('[id$=hdfDbCmdType]').val(dbConectiondetail[i].typ);
            selectedFlag = true;
            break;
        }
    }
    if ($('[id$=hdfDbCmdType]').val() == '4' || $('[id$=hdfDbCmdType]').val() == '2') $('[id$=divParameterInfo').html('* Use parameter with prefix : in query.');
    else $('[id$=divParameterInfo').html('* Use parameter with prefix @ in query.');

    if (ddlcon.val() != "-1") $('[id$=ddlCommandTypeDiv]').show();
    else $('[id$=ddlCommandTypeDiv]').hide();
    SelectCommandByDefaultdivhide();
    if (selectedFlag) {
        CommandType('');
        createDbCmdParaJson();
    }
    else {
        $('#AddParaDiv').html('');
        $('[id$=hdfDbCmdPara]').val('');
        $('[id$=SqlQueryOutputDiv').hide();
        $('select[id$=ddlCommandType]').parent().children("span").text($('select[id$=ddlCommandType]').children('option:selected').text());
    }
}

//Add new database command popup.
function SubProcAddDbCommandDiv(_bol, _Width) {
    if (_bol) {
        if (_Width == undefined)
            showModalPopUp('AddDbCommandDiv', 'Database Object', 590);
        else
            showModalPopUp('AddDbCommandDiv', 'Database Object', _Width);
        if ($($("#AddDbCommandDiv")[0].parentElement).length > 0) {
            $($("#AddDbCommandDiv")[0].parentElement).bind('click', function () {
                $("#dialogPara").dialog("close");
            });
        }
        try {
            $('#AddDbCommandDiv').dialog('option', 'close', function () {
                $("#dialogPara").dialog("close");
            });
        }
        catch (err) {
        }
    }
    else {
        $('#AddDbCommandDiv').dialog('close');
    }
}

//Database command parameters popup.
function OpenPanelInsertCommands(_bol) {
    if (_bol) {
        showModalPopUp('SqlQueryBuilding', 'Query Builder', 500);
        UnformDatabaseObject();
    }
    else {
        $('#SqlQueryBuilding').dialog('close');
        UnformDatabaseObject();
    }
}

//Database command Delete  Not Allowed.
function DelCommands(_bol) {
    if (_bol) {
        showModalPopUp('ShowDelete', 'Delete Not Allowed', 500);
    }
    else {
        $('#ShowDelete').dialog('close');
    }
    UnformDatabaseObject();
}
// Confirmation message popup
function SubProcConfirmBoxMessageDB(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

//Function Get all StoreProcedure list 
function Getallstoreprocedure() {
    var $hid = $('[id$=hidstoreproc]');

    if ($hid.val()) {
        permanent = jQuery.parseJSON($hid.val());
    }
}

//Function  Selected StoreProcedure 
function SelectedStoreProcedure(_selectvalue) {
    if (_selectvalue != "-1") {
        var json = [];
        var type = '';
        var inputpara = '';
        var t = '';
        jQuery.each(permanent, function (i, val) {
            if (_selectvalue == val.ObjectName) {
                var vinputpara = val.ParameterName;
                var vinputparatype = val.ParameterDataType;
                var val = '';
                var val2 = '';
                if (vinputpara != null) {
                    $('[id$=hdfInsertQueryPara]').val(vinputpara);
                    var toSplit = vinputpara.split(",");
                    var toSplit1 = vinputparatype.split(",");
                    for (var i = 0; i < toSplit.length; i++) {
                        if (inputpara == '') {
                            inputpara = toSplit[i];
                        }
                        else {
                            inputpara += "," + toSplit[i];
                        }
                        val2 = toSplit[i];
                        if (inputpara != '') {
                            for (var j = i; j < toSplit1.length; j++) {
                                var myObject = new Object();
                                myObject.para = toSplit[i];
                                type = '';
                                val = toSplit1[j];

                                if (val == 'decimal') type = 'Decimal';
                                else if (val == 'int') type = 'Integer';
                                else if (val == 'bit') type = 'Boolean';
                                else type = 'String';

                                if (t == '') t = val2;
                                else t += "," + val2;

                                myObject.typ = type;
                                json.push(myObject);
                                if (t != '') {
                                    $('[id$=lblInputParameters]').text(t);

                                }
                                break;
                            }

                        }
                        else {
                        }
                    }
                }
            }
        });
        var myString = JSON.stringify(json);
        if (myString != '') {
            $('[id$=hdfDbCmdPara]').val(myString);
        }
        $('[id$=txtDbCommand_SqlQuery]').val(_selectvalue);
        HideSqlQueryhide();
    }
}

function HideSqlQueryhide() {
    $('#divtxtdbsqquery').hide();
}

function HideSqlQueryShow() {
    $('#divtxtdbsqquery').show();
}

function ClearProc() {
    $('[id$=txtDbCommand_SqlQuery]').val('');
    $('[id$=hdfDbCmdPara]').val('');
    $('[id$=lblInputParameters]').text('No Parameters defined yet');
    HideSqlQueryhide();
}

function ClearInputerparameter() {
    $('[id$=lblInputParameters]').text('No Parameters defined yet');
    $('[id$=hdfDbCmdPara]').val('');
    $('[id$=txtDbCommand_SqlQueryPara]').text('');
}

function opencopyobject(_bol) {
    if (_bol) {
        showModalPopUp('divcopyobject', 'Copy Objects', 400);
    }
    else {
        $('#divcopyobject').dialog('close');

    }
    UnformDatabaseObject();
}

function disabletextobject() {
    $('[id$=txtDbCommand_CommandName]').attr("disabled", "disabled");
    $('[id$=ddl_DbConnactor]').attr("disabled", "disabled");
    $('[id$=ddlCommandType]').attr("disabled", "disabled");
}

function Enabletextobject() {
    $('[id$=txtDbCommand_CommandName]').removeAttr("disabled");
    $('[id$=ddl_DbConnactor]').removeAttr("disabled");
    $('[id$=ddlCommandType]').removeAttr("disabled");
}

function userDataValid() {
    var Name;
    Name = $('[id$=txtcopyobject]').val();
    if (Name == '') {
        alert("Please enter  copy object Name");
        return false;
    }
    else if (Name.length <= 3) {
        alert("Object name not less than three charcter");
        return false;
    }
    else if (Name.length >= 50) {
        alert("Object name not greater than 50 charcter");
        return false;
    }
}

function dataobjectvalidation() {

    var strVal, drpconnector, drpcommandtype, sqlquery, ddlDbCmdTableName, storeprocedure = '';
    var datacatchtype, dd1AbMonth, ddlRlt, dd1AbHr, dd1Abhh, ddlAbMM, ddlAbDay, dd1AbMnthDay, dd1AbMnDay, ddlRlt, drprltmin, Drprlthour, rltdayd, raltweeks = '';
    var strMessage = '';
    var inputparameter = '';
    strVal = $('[id$=txtDbCommand_CommandName]').val();
    drpconnector = $('select[id$=ddl_DbConnactor]').val();
    drpcommandtype = $('select[id$=ddlCommandType]').val();
    sqlquery = $('[id$=txtDbCommand_SqlQuery]').val();
    storeprocedure = $('select[id$=drpstoreprocedure]').val();
    storeprocedure = $.trim(storeprocedure);
    ddlDbCmdTableName = $('select[id$=ddlDbCmdTableName]').val();
    inputparameter = $('#lblInputParameters').text();
    $('[id$=hidlblinputpara]').val(inputparameter);
    datacatchtype = $('select[id$=dd1DataCatch]').val();
    dd1AbMonth = $('select[id$=dd1AbMonth]').val();
    ddlRlt = $('select[id$=ddlRlt]').val();
    dd1AbHr = $('select[id$=dd1AbHr]').val();
    ddlAbMM = $('select[id$=ddlAbMM]').val();
    dd1Abhh = $('select[id$=dd1Abhh]').val();
    ddlAbDay = $('select[id$=ddlAbDay]').val();
    dd1AbMnDay = $('select[id$=dd1AbMnDay]').val();
    dd1AbMnthDay = $('select[id$=dd1AbMnthDay]').val();
    ddlRlt = $('select[id$=ddlRlt]').val();
    drprltmin = $('select[id$=drprltmin]').val();
    Drprlthour = $('select[id$=Drprlthour]').val();
    rltdayd = $('select[id$=rltdayd]').val();
    raltweeks = $('select[id$=raltweeks]').val();
    drprltmonth = $('select[id$=drprltmonth]').val();
    if (storeprocedure != '-1' && drpcommandtype == '5') {
        sqlquery = storeprocedure;
    }
    if (strVal == '') {
        strMessage += " * Please enter data object name.</br>";
    }
    else if (strVal.length < 3) {
        strMessage += " *Object name cannot be less than 3 charecters.</br>";
    }
    else if (strVal.length > 50) {
        strMessage += " * Object name cannot be more than 20 charecters.</br>";
    }
    if (drpconnector == '-1' || drpconnector == '' || drpconnector == '') {
        strMessage += " * Please select  the Connector </br>";
    }
    if (drpconnector != '-1' && drpcommandtype == '-1') {
        strMessage += " * Please select  the command type </br>";
    }
    if (inputparameter == 'No Parameters defined yet' && (drpcommandtype == '2' || drpcommandtype == '3')) {
        strMessage += " * Undefined input parameter </br>";

    }
    if (drpconnector != '-1' && drpcommandtype == '4' && ddlDbCmdTableName == '-1') {
        strMessage += " * Please select  the table name </br>";
    }
    if (drpcommandtype != '-1' && sqlquery == '') {

        strMessage += " * Please enter sql query  </br>";

    }
    if (strMessage.length > 0) {
        showMessage(strMessage, DialogType.Error);
        return false;
    }

    SearchText = $('#tableobj_filter label input').val();
}

function PreventCancel() {
    AddParameterInDbCommand();
    EditDBobject();
    Enabletextobject();
    UnformDatabaseObject();
    var v = $('[id$=hidname]').val();
    ViewDBDisplay(v);
    ChangeRowSelectedCSS(EditIndex);
    //setclass = '';
}


function viewConnectiontest(id, index) {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('5');
    setclass = index;
    for (var i = 0; i < objdataset.length; i++) {
        if (id == objdataset[i].DB_COMMAND_NAME) {
            discardobject = objdataset[i].DB_COMMAND_NAME;
            break;
        }
    }
}

function ChangeSelection() {
    var table = $('#tableobj').DataTable();
    table.$('tr.selected').removeClass('selected');
    if (EditIndex != -1) {        
        ChangeRowSelectedCSS(EditIndex);
        setclass = '';
    }
    else
        ChangeRowSelectedCSS(setclass);
}

function discardedit() {
    ViewDBDisplay(discardobject);
    index = '';
    //  var table = $('#tableobj').DataTable();
    //table.page(2).draw(true);
    ChangeRowSelectedCSS(setclass);

}

function ChangeRowSelectedCSS(selIndex) {
    if (selIndex != undefined) {
        if (selIndex % 2 === 0) {
            $('#tableobj tbody tr').eq(selIndex).addClass('even selected')
        }
        else {
            $('#tableobj tbody tr').eq(selIndex).addClass('odd selected')
        }
    }
    $('#tableobj').parent().scrollTop(scrollTop);
}

function canceldiscard() {
    ViewDBDisplay(discardobject);
    setclass = '';
}

function Adddiscardtype() {
    if ($('[id$=hideditdone]').val() == "5") {
        SubModifiction(true);
        $('[id$=hiddiscardcommadtype]').val('2');
        return false;
    }
    else {
        EditIndex = -1;
        return true;
    }
}

function discardadd() {
    $('[id$=hiddiscardcommadtype]').val('2');
    if ($('[id$=hideditdone]').val() != '' && setclass.toString() != '') {
        canceldiscard();
        $('[id$=hideditdone]').val('');
        return false;
    }
    else {
        return true;
    }
}
//  Testing button Js copy in this section
function dialogtest() {
    //  $('#ShowDnCmdtestParaDiv').html('');
    $('#divdetails').children().remove();
    var strInputParaJson = '';
    if ($('[id$=hdfDbCmdPara]').val().length > 0) {
        var strInputParaJson = '';
        var parajson = jQuery.parseJSON($('[id$=hdfDbCmdPara]').val());
        $('#divdetails').append('<div id="divparameter" class="AddParameter">');
        $.each(jQuery.parseJSON($('[id$=hdfDbCmdPara]').val()), function () {
            strInputParaJson += ', ' + this['para'];
            var strHtml = '<div id="TestAddParaDiv_' + this['para'] + '" >' +
                             '<div class="QPContentText1">' + this['para'] + '</div>' +
                                    '<div class="QPContentType1" style="float:right;padding-right:105px !important;">' +
                                        '<input  id="para_' + this['para'] + '"type="text" style="width:100%" ><br>' +
                                        '</div></div><div style="clear: both;"></div>';
            $('#divparameter').append(strHtml);
        });
    }
    else {
        $('#divdetails').append('<div id="divparameter" class="AddParameter"> No Input Parameter in this object.</div>');
    }
    $('#divdetails').append('<div class="SubProcborderDiv"><div class="SubProcBtnMrgn" style="text-align:center" ><button type="button"  class="InputStyle"  onclick="fncsave();">Execute</button></div>');
    opendialogtest(true);
}

function fncsave() {
    var lp = [];
    if ($('[id$=hdfDbCmdPara]').val().length > 0) {
        $.each(jQuery.parseJSON($('[id$=hdfDbCmdPara]').val()), function () {
            // strInputParaJson += ', ' + this['para'];
            lp.push({ "para": this['para'], "val": $('#para_' + this['para']).val() });
        });
    }
    $('[id$=hdflp]').val(JSON.stringify(lp));
    $('[id$=savebtn]').click();
}

function CreateDynamicTable(result) {
    $('#Div7').children().remove();
    if (result.length > 0) {
        $('#Div7').html('<table id="resulttbl"></table>');
        columnCount = 0;
        rowCount = 0;
        var columndt = [];
        $.each(result, function (headindex, headvalue) {
            columnCount++;
            rowCount = rowCount < headvalue.val.length ? headvalue.val.length : rowCount;
            columndt.push({ 'title': headvalue.cnm });
        });

        var dtRows = [];
        for (var rindex = 0; rindex < rowCount; rindex++) {
            var dtCol = [];
            for (var cindex = 0; cindex < columnCount; cindex++) {
                dtCol.push(result[cindex].val[rindex]);
            }
            dtRows.push(dtCol);
        }
        $('#resulttbl').dataTable({
            "data": dtRows,
            "columns": columndt,
            "sort": false,
            "searching": false,
            "paging": false,
            "info": false
        });
    }
    else {
        $('#Div7').html('<div style="width:100%;text-align:center;">No records found</div>');
    }
    openDynamictable(true, 800); opendialogtest(false);
}

function openDynamictable(_bol, width) {
    if (_bol) {
        var objectheader = $('[id$=hdfDbType]').val();
        objectheader = ' ( ' + objectheader + ' )';

        showModalPopUp('dialogdynamictable', 'Execute Details' + objectheader, width);
    }
    else {
        $('#dialogdynamictable').dialog('close');
    }
}

//Odata connector edit
function opendialogtest(_bol) {
    if (_bol) {
        showModalPopUp('dialogtestPara', 'Execute  Object', 500);
    }
    else {
        $('#dialogtestPara').dialog('close');
    }
}

function SelectCommandByDefaultdivhide() {
    $('[id$=divstoreprocedure').hide();
    $('[id$=divadvacequerybuilder').hide();
    $('[id$=QueryLink').hide();
    $('[id$=divoutputquerylinks').hide();
    $('[id$=divstoreprocedure').hide();
    $('[id$=lnRefButton').hide();
    $('[id$=Div8').hide();
    $('[id$=divdonotexecute').hide();
    $('[id$=divcatchedetails').hide();
    $('[id$=divtxtdbsqquery').show();
    $('[id$=SqlQueryOutputDiv').hide();
    $('[id$=btnDbcmdParaAddDiv').hide();
    $('[id$=divParameterInfo').hide();
    $('[id$=lblInputParametersDiv').hide();
}
function CommandType(isNew) {
    SelectCommandByDefaultdivhide();
    $('[id$=divSelectSqlQueryLabel').show();
    $('[id$=btnDbcmdParaAddDiv').show();
    $('[id$=divParameterInfo').show();
    $('[id$=lblInputParametersDiv').show();
    var selectcommand = $('select[id$=ddlCommandType]').val();
    if (selectcommand == "1") {
        $('[id$=SqlQueryOutputDiv').show();
        $('[id$=divadvacequerybuilder').show();
        $('[id$=divdonotexecute').show();
        $('[id$=lnRefButton').show();
        $('[id$=divoutputquerylinks').show();
        $('[id$=Div8').show();
        $('[id$=divdonotexecute').show();
        $('[id$=divcatchedetails').show();
    }
    else if (selectcommand == "2" || selectcommand == "3") {
        $('[id$=SqlQueryOutputDiv').show();
        $('[id$=QueryLink').show();
        $('[id$=Div8').show();
        $('[id$=divdonotexecute').show();
    }
    else if (selectcommand == "4") {
        $('[id$=SqlQueryOutputDiv').hide();
        $('[id$=divdonotexecute').show();
    }
    else if (selectcommand == "5") {
        $('[id$=divstoreprocedure').show();
        $('[id$=divtxtdbsqquery').hide();
        $('[id$=divSelectSqlQueryLabel').hide();
        Btnstoreprocedure();
    }
    else {
        $('[id$=SqlQueryOutputDiv').hide();
        $('[id$=btnDbcmdParaAddDiv').hide();
        $('[id$=divParameterInfo').hide();
        $('[id$=lblInputParametersDiv').hide();
    }
}


function CommandTypeDetail(selectval) {
    SelectCommandByDefaultdivhide();
    var selectcommand = $(selectval).val();
    if (selectcommand == undefined && $('select[id$=ddlCommandType]').val() != "-1") selectcommand = '5';
    if (selectcommand == "5") {
        $('[id$=divstoreprocedure').show();
        $('[id$=divtxtdbsqquery').hide();
        $('[id$=divSelectSqlQueryLabel').hide();
    }
}

//Load js file from server end
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();

//-----------INPUT PARAMETERS-------------------------------

//Database command parameters popup.
function SubProcDbCmdAddPara(_bol) {
    if (_bol) {
        showModalPopUp('SubProcDbCmdAddPara', 'Object Parameters', 400);
    }
    else {
        $('#SubProcDbCmdAddPara').dialog('close');
    }
}


//this sub is used to add parameter in database command. Tanika
function AddParameterInDbCommand() {
    DbCmdParaCount = 1;
    $('#AddParaDiv').html('');
    var strInputParaJson = '';
    if ($('[id$=hdfDbCmdPara]').val().length > 0) {
        $.each(jQuery.parseJSON($('[id$=hdfDbCmdPara]').val()), function () {

            strInputParaJson += strInputParaJson.length > 0 ? (', ' + this['para']) : this['para'];
            DbCmdParaCount = AddInputParaMeterHTML(DbCmdParaCount, this['para'], this.id);
        });
        if (strInputParaJson.length > 0) $('#lblInputParameters').html(strInputParaJson);
        else $('#lblInputParameters').html("No Parameters defined yet");
        SetIntellisenseOfinputParameter(strInputParaJson.split(','), '[id$=txtDbCommand_SqlQuery]');
        createDbCmdParaJson();
    }
    else
        $('#lblInputParameters').html("No Parameters defined yet");

    $('#txtAddParaName').keydown(function (e) {
        if (e.which == 13) {
            AddParameterOnEvent();
        }
    });

    $('#imgAddQueryPara').bind('click', function (e) {
        AddParameterOnEvent();
    });
}

function AddParameterOnEvent() {
    if ($('#txtAddParaName').val().trim().length == 0) {
        return;
    }
    if (IsStringContainsSpecialChar($('#txtAddParaName').val())) {
        CommonErrorMessages(77);
        return;
    }
    if (IsDbCmdParaNameAlreadyExists()) {
        CommonErrorMessages(34);
        return;
    }
    DbCmdParaCount = AddInputParaMeterHTML(DbCmdParaCount, $('#txtAddParaName').val(), this.id);
    $('#txtAddParaName').val('');
    createDbCmdParaJson();
}

function AddInputParaMeterHTML(DbCmdParaCount, paraName) {
    var strHtml = '<div id="QpAddParaDiv_' + DbCmdParaCount + '"  class="QPContentRow">' +
    '<div class="QPContentText">' + paraName + '</div>' +
    '<div class="QPContentDelete" align="center">' +
        '<img id="imgDelQueryPara_' + DbCmdParaCount + '" alt="" src="//enterprise.mficient.com/images/cross.png" />' +
    '</div>' +
    '</div><div style="clear: both;"></div>';
    $('#AddParaDiv').append(strHtml);
    $('#imgDelQueryPara_' + DbCmdParaCount).bind('click', function (e) {
        $('#QpAddParaDiv_' + DbCmdParaCount).remove();
        createDbCmdParaJson();
    });
    return DbCmdParaCount + 1;
}

//this sub is used to check parameter name already exists or not
function IsDbCmdParaNameAlreadyExists() {
    var Exists = false;
    jQuery.each($('#AddParaDiv')[0].children, function () {
        var innerJson = "";
        jQuery.each(this.children, function () {
            if (this.className == "QPContentText")
                if ($(this).html().trim() == $('#txtAddParaName').val().trim())
                    Exists = true;
        });
    });
    return Exists;
}

//this sub is used to create json of dropdown command
function createDbCmdParaJson() {
    var outerJson = [];
    var strpara = "";
    var strInputParaJson = '';
    var vjson = '';

    jQuery.each($('#AddParaDiv')[0].children, function () {
        jQuery.each(this.children, function () {
            if (this.className == "QPContentText") {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ';
                outerJson.push({ "para": $(this).html() });
                strInputParaJson += $(this).html();

                strpara += strpara.length > 0 ? (',' + $(this).html()) : $(this).html();
            }
        });
    });
    if (strInputParaJson.length > 0) $('#lblInputParameters').html(strInputParaJson);
    else $('#lblInputParameters').html('No Parameters defined yet');

    $('[id$=hdfInsertQueryPara]').val(strpara);
    $('[id$=hdfDbCmdPara]').val(JSON.stringify(outerJson));
    SetIntellisenseOfinputParameter(strpara.split(','), '[id$=txtDbCommand_SqlQuery]');
    SetIntellisenseOfinputParameter(strpara.split(','), '[id$=txtDbCommand_SqlQueryWH]');
}

function SetIntellisenseOfinputParameter(strpara, textboxid) {
    var KeyString = "@";
    if ($('[id$=hdfDbCmdType]').val() == '4' || $('[id$=hdfDbCmdType]').val() == '2') KeyString = ":";
    $(textboxid).atwho({
        at: KeyString,
        data: strpara,
        limit: 200,
        startWithSpace: false,
        callbacks: {
            afterMatchFailed: function (at, el) {
                if (at == '#') {
                    tags.push(el.text().trim().slice(1));
                    this.model.save(tags);
                    this.insert(el.text().trim());
                    return false;
                }
            }
        }
    });
}

//--------OUTPUT PARAMETER-------------------------------------------

//Set output column value
function setValInHdfDataTrans(_DataTranJson, _OutColumn) {
    $('[id$=hfOutCol]').val(_OutColumn);
}

function AutoCompleteTextbox(strJson) {
    $($("[id$=txtDbCommand_SqlQueryPara]").parent()).find('.token-input-list-facebook').each(function () {
        $(this).remove();
    });
    var jsonObj = null;
    try { jsonObj = jQuery.parseJSON(strJson); }
    catch (err) {
    }

    $("[id$=txtDbCommand_SqlQueryPara]").tokenInput(jsonObj,
    {
        itemValidateRegx: /^[0-9a-zA-Z+\_]+$/, allowSameItem: false, theme: "facebook", prePopulate: jsonObj
    });
    $($("[id$=txtDbCommand_SqlQueryPara]").parent()).find('.token-input-list-facebook').each(function () {
        $(this).css('width', '100%');
    });
}

//Get all manually entered values from autocomplete textbox
function GetAllAutoboxMValue() {
    var strOutJson = "";
    $($("[id$=txtDbCommand_SqlQueryPara]").parent()).find('.token-input-tokenRed-facebook p').each(function () {
        if (strOutJson.length > 0) strOutJson += ',';
        strOutJson += '{ "id": "' + this.innerHTML + ';", "name": "' + this.innerHTML + '" }';
    });
    $("[id$=hdfAutoboxMannulPara]").val(strOutJson);
}
//Set auto complete textbox values in hiddenField
function GetAllAutoboxValue1() {
    $("[id$=hdfOutCol]").val(GetAllAutoboxValue("[id$=txtDbCommand_SqlQueryPara]")[2]);
}

//Get values from autocomplete textbox
function GetAllAutoboxValue(_Id) {
    var strJson = "";
    var strOutJson = "";
    var strAllOuTParam = "";
    $($(_Id).parent()).find('.token-input-token-facebook p').each(function () {
        if (strJson.length > 0) strJson += ',';
        strJson += '{ "id": "' + this.innerHTML + '", "name": "' + this.innerHTML + '" }';
        if (strAllOuTParam.length > 0) strAllOuTParam += ',';
        strAllOuTParam += this.innerHTML;
    });

    $($(_Id).parent()).find('.token-input-tokenRed-facebook p').each(function () {
        if (strOutJson.length > 0) strJson += ',';
        strOutJson += '{ "id": "' + this.innerHTML + '", "name": "' + this.innerHTML + '" }';
        if (strAllOuTParam.length > 0) strAllOuTParam += ',';
        strAllOuTParam += this.innerHTML + ';';
    });
    return [strJson, strOutJson, strAllOuTParam];
}
/*
* jQuery Plugin: Tokenizing Autocomplete Text Entry
* Version 1.6.0
*
* Copyright (c) 2009 James Smith (http://loopj.com)
* Licensed jointly under the GPL and MIT licenses,
* choose which one suits your project best!
*
*/
/***************************************************/
(function ($) {
    // Default settings
    var DEFAULT_SETTINGS = {
        // Search settings
        method: "GET",
        contentType: "json",
        queryParam: "q",
        searchDelay: 300,
        minChars: 2,
        propertyToSearch: "name",
        jsonContainer: null,

        // Display settings
        hintText: "Type in a search term.Atleast 2 characters",
        noResultsText: "No results",
        searchingText: "Searching...",
        deleteText: "&times;",
        animateDropdown: true,

        // Tokenization settings
        tokenLimit: null,
        tokenDelimiter: ",",
        preventDuplicates: true,

        // Output settings
        tokenValue: "id",

        // Prepopulation settings
        prePopulate: null,
        processPrePopulate: false,

        // Manipulation settings
        idPrefix: "token-input-",

        // Formatters
        resultsFormatter: function (item) { return "<li>" + item[this.propertyToSearch] + "</li>" },
        tokenFormatter: function (item) { return "<li><p>" + item[this.propertyToSearch] + "</p></li>" },

        // Callbacks
        onResult: null,
        onAdd: null,
        onDelete: null,
        allowSameItem: false,
        itemValidateRegx: null,
        onReady: null
    };

    // Default classes to use when theming
    var DEFAULT_CLASSES = {
        tokenList: "token-input-list",
        tokenRed: "token-input-tokenRed",
        token: "token-input-token",
        tokenDelete: "token-input-delete-token",
        selectedToken: "token-input-selected-token",
        highlightedToken: "token-input-highlighted-token",

        inputToken: "token-input-input-token"
    };

    // Input box position "enum"
    var POSITION = {
        BEFORE: 0,
        AFTER: 1,
        END: 2
    };

    // Keys "enum"
    var KEY = {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        ESCAPE: 27,
        SPACE: 32,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        END: 35,
        HOME: 36,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        NUMPAD_ENTER: 108,
        COMMA: 188,
        SEMI_COLON: 186
    };

    // Additional public (exposed) methods
    var methods = {
        init: function (url_or_data_or_function, options) {
            var settings = $.extend({}, DEFAULT_SETTINGS, options || {});

            return this.each(function () {
                $(this).data("tokenInputObject", new $.TokenList(this, url_or_data_or_function, settings));
            });
        },
        clear: function () {
            this.data("tokenInputObject").clear();
            return this;
        },
        add: function (item) {
            this.data("tokenInputObject").add(item);
            return this;
        },
        remove: function (item) {
            this.data("tokenInputObject").remove(item);
            return this;
        },
        get: function () {
            return this.data("tokenInputObject").getTokens();
        }
    }

    // Expose the .tokenInput function to jQuery as a plugin
    $.fn.tokenInput = function (method) {
        // Method calling and initialization logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else {
            return methods.init.apply(this, arguments);
        }
    };

    // TokenList class for each input
    $.TokenList = function (input, url_or_data, settings) {

        if (typeof (url_or_data) === "object") {
            // Set the local data to search through
            settings.local_data = url_or_data;
        }

        // Build class names
        if (settings.classes) {
            // Use custom class names
            settings.classes = $.extend({}, DEFAULT_CLASSES, settings.classes);
        } else if (settings.theme) {
            // Use theme-suffixed default class names
            settings.classes = {};
            $.each(DEFAULT_CLASSES, function (key, value) {
                settings.classes[key] = value + "-" + settings.theme;
            });
        } else {
            settings.classes = DEFAULT_CLASSES;
        }


        // Save the tokens
        var saved_tokens = [];

        // Keep track of the number of tokens in the list
        var token_count = 0;

        // Basic cache to save on db hits
        //        var cache = new $.TokenList.Cache();

        // Keep track of the timeout, old vals
        var timeout;
        var input_val;

        // Create a new text input an attach keyup events
        var input_box = $("<input type=\"text\"  autocomplete=\"off\">")
        .css({
            outline: "none"
        })
        .attr("id", settings.idPrefix + input.id)
        .focus(function () {
            if (settings.tokenLimit === null || settings.tokenLimit !== token_count) {
                //                show_dropdown_hint();
            }
        })
        .blur(function () {
            //            hide_dropdown();
            $(this).val("");
        })
        //        .bind("keyup keydown blur update", resize_input)
        .keydown(function (event) {
            var previous_token;
            var next_token;

            switch (event.keyCode) {
                case KEY.LEFT:
                case KEY.RIGHT:
                case KEY.UP:
                case KEY.DOWN:
                    break;

                case KEY.BACKSPACE:
                    previous_token = input_token.prev();

                    if (!$(this).val().length) {
                        if (selected_token) {
                            delete_token($(selected_token));
                            hidden_input.change();
                        } else if (previous_token.length) {
                            select_token($(previous_token.get(0)));
                        }

                        return false;
                    } else if ($(this).val().length === 1) {
                    } else {
                    }
                    break;

                //case KEY.TAB:                                            
                //case KEY.ENTER:                                        
                //case KEY.NUMPAD_ENTER:                                          
                case KEY.SEMI_COLON:
                case KEY.COMMA:
                    if (this.value.trim().length == 0) return;
                    if (!(settings.itemValidateRegx).test(this.value.trim())) {
                        $(this).val('');
                        $('#aMessage').html('Output column contains invalid character.');
                        SubProcBoxMessage(true);
                        return;
                    }
                    else {
                        var JsonObject = jQuery.parseJSON('{ "id": "' + this.value + ';", "name": "' + this.value + '" }');
                        add_token(JsonObject);
                        return false;
                    }
                    break;

                case KEY.ESCAPE:
                    //                    hide_dropdown();
                    return true;

                default:
                    if (String.fromCharCode(event.which)) {
                        // set a timeout just long enough to let this function finish.
                        //                        setTimeout(function(){do_search();}, 5);
                    }
                    break;
            }
        });

        // Keep a reference to the original input box
        var hidden_input = $(input)
                           .hide()
                           .val("")
                           .focus(function () {
                               input_box.focus();
                           })
                           .blur(function () {
                               input_box.blur();
                           });

        // Keep a reference to the selected token and dropdown item
        var selected_token = null;
        var selected_token_index = 0;
        var selected_dropdown_item = null;

        // The list to store the token items in
        var token_list = $("<ul />")
        .addClass(settings.classes.tokenList)
        .click(function (event) {
            var li = $(event.target).closest("li");
            if (li && li.get(0) && $.data(li.get(0), "tokeninput")) {
                toggle_select_token(li);
            } else {
                // Deselect selected token
                if (selected_token) {
                    deselect_token($(selected_token), POSITION.END);
                }

                // Focus input box
                input_box.focus();
            }
        })
        .mouseover(function (event) {
            var li = $(event.target).closest("li");
            if (li && selected_token !== this) {
                li.addClass(settings.classes.highlightedToken);
            }
        })
        .mouseout(function (event) {
            var li = $(event.target).closest("li");
            if (li && selected_token !== this) {
                li.removeClass(settings.classes.highlightedToken);
            }
        })
        .insertBefore(hidden_input);

        // The token holding the input box
        var input_token = $("<li />")
        .addClass(settings.classes.inputToken)
        .appendTo(token_list)
        .append(input_box);

        // Pre-populate list if items exist
        hidden_input.val("");
        var li_data = settings.prePopulate || hidden_input.data("pre");
        if (settings.processPrePopulate && $.isFunction(settings.onResult)) {
            li_data = settings.onResult.call(hidden_input, li_data);
        }
        if (li_data && li_data.length) {
            $.each(li_data, function (index, value) {
                insert_token(value);
                checkTokenLimit();
            });
        }

        // Initialization is done
        if ($.isFunction(settings.onReady)) {
            settings.onReady.call();
        }

        //
        // Public functions
        //

        this.clear = function () {
            token_list.children("li").each(function () {
                if ($(this).children("input").length === 0) {
                    delete_token($(this));
                }
            });
        }

        this.add = function (item) {
            add_token(item);
        }

        this.remove = function (item) {
            token_list.children("li").each(function () {
                if ($(this).children("input").length === 0) {
                    var currToken = $(this).data("tokeninput");
                    var match = true;
                    for (var prop in item) {
                        if (item[prop] !== currToken[prop]) {
                            match = false;
                            break;
                        }
                    }
                    if (match) {
                        delete_token($(this));
                    }
                }
            });
        }

        this.getTokens = function () {
            return saved_tokens;
        }

        function checkTokenLimit() {
            if (settings.tokenLimit !== null && token_count >= settings.tokenLimit) {
                input_box.hide();
                return;
            }
        }
        // Inner function to a token to the list
        function insert_token(item) {
            if (!(settings.itemValidateRegx).test(item.id.replace(';', ''))) {
                return;
            }
            var this_token = settings.tokenFormatter(item);
            this_token = $(this_token)
          .addClass(item.id.indexOf(';') != -1 ? settings.classes.tokenRed : settings.classes.token)
          .insertBefore(input_token);

            // The 'delete token' button
            $("<span>" + settings.deleteText + "</span>")
            .addClass(settings.classes.tokenDelete)
            .appendTo(this_token)
            .click(function () {
                delete_token($(this).parent());
                hidden_input.change();
                return false;
            });

            // Store data on the token
            var token_data = { "id": item.id };
            token_data[settings.propertyToSearch] = item[settings.propertyToSearch];
            $.data(this_token.get(0), "tokeninput", item);

            // Save this token for duplicate checking
            saved_tokens = saved_tokens.slice(0, selected_token_index).concat([token_data]).concat(saved_tokens.slice(selected_token_index));
            selected_token_index++;

            // Update the hidden input
            update_hidden_input(saved_tokens, hidden_input);

            token_count += 1;

            // Check the token limit
            if (settings.tokenLimit !== null && token_count >= settings.tokenLimit) {
                input_box.hide();
            }
            return this_token;
        }

        // Add a token to the token list based on user input
        function add_token(item) {
            var callback = settings.onAdd;

            // See if the token already exists and select it if we don't want duplicates
            if (token_count > 0 && settings.preventDuplicates) {
                var found_existing_token = null;
                if (!settings.allowSameItem) {
                    token_list.children().each(function () {
                        var existing_token = $(this);
                        var existing_data = $.data(existing_token.get(0), "tokeninput");
                        if (existing_data && existing_data.id.replace(';', '') === item.id.replace(';', '')) {
                            found_existing_token = existing_token;
                            return false;
                        }
                    });
                }
                if (found_existing_token) {
                    $('#aMessage').html('Output column already exists.');
                    SubProcBoxMessage(true);
                    return;
                }

            }

            // Insert the new tokens
            if (settings.tokenLimit == null || token_count < settings.tokenLimit) {
                insert_token(item);
                checkTokenLimit();
            }

            // Clear input box
            input_box.val("");

            // Don't show the help dropdown, they've got the idea
            // Execute the onAdd callback if defined
            if ($.isFunction(callback)) {
                callback.call(hidden_input, item);
            }
        }

        // Select a token in the token list
        function select_token(token) {
            token.addClass(settings.classes.selectedToken);
            selected_token = token.get(0);

            // Hide input box
            input_box.val("");

            // Hide dropdown if it is visible (eg if we clicked to select token)
            //            hide_dropdown();
        }

        // Deselect a token in the token list
        function deselect_token(token, position) {
            token.removeClass(settings.classes.selectedToken);
            selected_token = null;

            if (position === POSITION.BEFORE) {
                input_token.insertBefore(token);
                selected_token_index--;
            } else if (position === POSITION.AFTER) {
                input_token.insertAfter(token);
                selected_token_index++;
            } else {
                input_token.appendTo(token_list);
                selected_token_index = token_count;
            }

            // Show the input box and give it focus again
            input_box.focus();
        }

        // Toggle selection of a token in the token list
        function toggle_select_token(token) {
            var previous_selected_token = selected_token;

            if (selected_token) {
                deselect_token($(selected_token), POSITION.END);
            }

            if (previous_selected_token === token.get(0)) {
                deselect_token(token, POSITION.END);
            } else {
                select_token(token);
            }
        }

        // Delete a token from the token list
        function delete_token(token) {
            // Remove the id from the saved list
            var token_data = $.data(token.get(0), "tokeninput");
            var callback = settings.onDelete;

            var index = token.prevAll().length;
            if (index > selected_token_index) index--;

            // Delete the token
            token.remove();
            selected_token = null;

            // Show the input box and give it focus again
            input_box.focus();

            // Remove this token from the saved list
            saved_tokens = saved_tokens.slice(0, index).concat(saved_tokens.slice(index + 1));
            if (index < selected_token_index) selected_token_index--;

            // Update the hidden input
            update_hidden_input(saved_tokens, hidden_input);

            token_count -= 1;

            if (settings.tokenLimit !== null) {
                input_box
                .show()
                .val("")
                .focus();
            }

            // Execute the onDelete callback if defined
            if ($.isFunction(callback)) {
                callback.call(hidden_input, token_data);
            }
        }

        // Update the hidden input box value
        function update_hidden_input(saved_tokens, hidden_input) {
            var token_values = $.map(saved_tokens, function (el) {
                return el[settings.tokenValue];
            });
            hidden_input.val(token_values.join(settings.tokenDelimiter));

        }
        // Highlight the query part of the search term
        function highlight_term(value, term) {
            return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<b>$1</b>");
        }

        function find_value_and_highlight_term(template, value, term) {
            return template.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + value + ")(?![^<>]*>)(?![^&;]+;)", "g"), highlight_term(value, term));
        }

    };

} (jQuery));

//---------Image Column -----------------------------------

function ImageColumnPopup(_bol) {
    if (_bol) {

        showModalPopUp('ImageColumnPopup', 'Image Column', 400);
    }
    else {
        $('#ImageColumnPopup').dialog('close');
    }
}

function imageoutputparameter() {
    var commandtype = '';
    var existigcoloum = '';
    var deletecoloum = '';
    commandtype = $('[id$=hidconid]').val();
    existigcoloum = $('[id$=hidimagecoloums]').val();
    if (commandtype != '') {
        $('.ImageQPContentRow').remove();
        DropdownOptionmodified();
        EditImageSource();
    }
    else if (commandtype == '' && existigcoloum != "") {
        $('.ImageQPContentRow').remove();
        EditImageSource();
        displayimageparameter();
    }
    else {
        var parameter = '';
        var strHtml = '';
        var existimageparameter = '';
        $('#Div_ImageColumns').children().remove();
        $('#drpimageparameter').find('option').remove();
        parameter = $('[id$=hidimageoutput]').val();
        if (commandtype == '') parameter = $('#MainCanvas_txtDbCommand_SqlQueryPara').val(); parameter = replaceAll(';', '', parameter);
        if (parameter != '') {
            $('#MainCanvas_txtDbCommand_SqlQueryPara').val(); parameter = replaceAll(';', '', parameter);
            $('#Div_ImageColumns').append('<div id="divimageparameter">');
            parameter = parameter.split(',');
            existimageparameter = $('[id$=hidimagecoloums]').val();
            existimageparameter = existimageparameter.split(',');
            $('#divimagepropertiesnoparameter').hide();
            $('#divimageproperties').show();
            $('#drpimageparameter').empty();
            var mySelect = $('#drpimageparameter');
            mySelect.append('<option value="-1" selected="selected"> Select </option>')
            for (var i = 0; i < parameter.length; i++) {
                var values = $('<option></option>').val(parameter[i]).html(parameter[i]);
                mySelect.append(values);
            }
        }
        else {
            $('#divimagepropertiesnoparameter').show();
            $('#divimageproperties').hide();
            $('#divimagepropertiesnoparameter').html("");
            $('#divimagepropertiesnoparameter').append(' No Image parameter in this object.');
        }
        $('#Div_ImageColumns').append('<div class="SubProcborderDiv"><div class="SubProcBtnMrgn" style="text-align:center" ><button type="button"  class="InputStyle"  onclick="displayimageparameter();">Save</button></div>');
    }
    ImageColumnPopup(true);
    UnformDatabaseObject();
}
// Display image parameter and  before saving json defined
function displayimageparameter() {
    var controlid = '';
    var outputparameter = '';
    var column = [];
    var arraylist = [];
    $('.imagecontent').each(function () {
        if (this.id != '') {
            controlid = this.id.replace('para_', '');
            var coloumname = $('select#drp' + controlid + '  option:selected').val();
            var flag = true;

            $.each(column, function (index, value) {
                if (value[0] == column) {
                    flag = false;
                }
            });
            if (flag) {
                column.push(coloumname);
                arraylist.push([coloumname, $('select#ddlQPOption_' + controlid + '  option:selected').val()]);
                if (outputparameter == '' || outputparameter == undefined) outputparameter = coloumname;
                else outputparameter += ',' + coloumname;
            }
        }
    });
    var addcoloum = $('#drpimageparameter  option:selected').val();
    if (addcoloum != '-1') {
        if ($.inArray(addcoloum, column) < 0) {
            column.push(addcoloum);
            arraylist.push([addcoloum, $('#drpselectimageparameter  option:selected').val()]);

        }
    }

    $('[id$=hidimagecoloums]').val(JSON.stringify(arraylist));
    $('[id$=lblImageColumns]').text(column);
}

function HtmlForImageColumn(val, controlid) {
    var drpdataformat = '<select class="ddlQPOption" id="ddlQPOption_' + val + '"  style="width:118px !important">'
                                            +
                                            '<option value="1">Public URL </option>' +
                                            '<option value="2">Local URL </option>' +
                                           '<option value="3">Local File Path </option>' +
                                            '<option value="4">Base 64 </option></select>';

    var strHtml = '<div  class="ImageQPContentRow"  id="Divallparamenter_' + val + '">' +
                                            '<div class="imagecontent"  id="para_' + $('#drpimageparameter').val() + '">' +
                                            '</div>' +
                                            '<div class="imagecontentType">' +
                                                drpdataformat
                                              + '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img  " alt="" src="//enterprise.mficient.com/images/cross.png" id=' + controlid + '  onclick="RemoveImagebuttonRow(this);"/>' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
    return strHtml;
}
// Image Parameter saved
function ImageAddbuttonClick() {
    var voutput = '';
    var check = $('#drpimageparameter option:selected').val();
    if (check == -1) {
        alert("Please select image parameter");
    }
    else {
        var controlid = 'img_' + $('#drpimageparameter').val();
        controlid = $.trim(controlid);
        var val = $('#drpimageparameter  option:selected').val();
        val = $.trim(val);

        var $divcontrol = $('#Divallparamenter_' + val);
        if ($divcontrol.length > 0) {
            alert("Already added this image parameter");
        }
        else {
            $('#divheader').append(HtmlForImageColumn(val, controlid));
            BindImageDropDown('para_', $('#drpimageparameter').val(), $('#drpselectimageparameter  option:selected').val(), $('#drpselectimageparameter  option:selected').text());
            $('#drpimageparameter option:eq(0)').attr('selected', true);
            $('#drpselectimageparameter option:eq(0)').attr('selected', true);
        }
    }
}
function EditImageSource() {
    var imagecolumns = $('[id$=hidimagecoloums]').val();     // Object image parameter Edit Dynamic parameter and Dynamic Table modified
    if (imagecolumns != '') {
        var imagecolouns = jQuery.parseJSON(imagecolumns);
        for (var i = 0, l = imagecolouns.length; i < l; i++) {
            var controlid = $.trim('img_' + imagecolouns[i][0]);
            var val = imagecolouns[i][0];

            var newparameter = $('#MainCanvas_txtDbCommand_SqlQueryPara').val();

            var flag = true;
            if (newparameter != "") {
                newparameter = replaceAll(';', '', newparameter);
                newparameter = newparameter.split(',');
                if ($.inArray(val, newparameter) < 0) {
                    flag = false;
                }
            }
            var text = imagecolouns[i][1] == '1' ? 'Public URL' : (imagecolouns[i][1] == '2' ? 'Local URL' : (imagecolouns[i][1] == '3' ? 'Local File Path' : 'Base 64'));

            var $divcontrol = $('#Divallparamenter_' + val);
            if ($divcontrol.length > 0) {
                alert("Already added this image parameter");
            }
            else {
                if (flag) {
                    $('#drpimageparameter').val(val);
                    $('#divheader').append(HtmlForImageColumn(val, controlid));
                    BindImageDropDown('para_', imagecolouns[i][0], imagecolouns[i][1], text);
                }
            }
        }
        $('#drpimageparameter option:eq(0)').attr('selected', true);
        $('#drpselectimageparameter option:eq(0)').attr('selected', true);
    }
}

function RemoveImagebuttonRow(imageparameter) {
    $(imageparameter).parent().parent().remove();
}

function BindImageDropDown(parent, selectvalue, formatval, formattext) {
    var parameter = '';
    var $drp = '';
    var values = '';
    var newparameter = "";
    values = $('[id$=hidconid]').val();
    if (values == '') {
        parameter = $('#MainCanvas_txtDbCommand_SqlQueryPara').val();
    }
    else {
        parameter = $('[id$=hidimageoutput]').val();
        newparameter = $('#MainCanvas_txtDbCommand_SqlQueryPara').val();
        if (newparameter != "") parameter = newparameter;
    }
    parameter = replaceAll(';', '', parameter);
    parameter = parameter.split(',');
    values = '';
    drp = 'drp' + selectvalue;
    parent = '#' + parent + selectvalue;
    var $log = $(parent);
    var selectcontrol = $('<select id=' + drp + ' style="width:118px !important">');
    selectcontrol.appendTo($log);
    $drp = $('#' + drp);
    var mySelect = $drp;
    for (var i = 0; i < parameter.length; i++) {
        values = $('<option></option>').val(parameter[i]).html(parameter[i]);
        mySelect.append(values);
    }
    $('#' + drp + '').val(selectvalue);
    $('#' + drp + ' option:selected').text(selectvalue);
    $('#' + 'ddlQPOption_' + selectvalue + '').val(formatval);
    $('#' + 'ddlQPOption_' + selectvalue + '  option:selected').text(formattext);
    drp = '';
}

function ImageDivClear() {
    $('.ImageQPContentRow').remove();   // Dynamically Table Clear in this methods
    $('[id$=hidimageoutput]').val('');
    $('#drpimageparameter').find('option').remove();
    $('#drpimageparameter').append('<option value="-1" selected="selected"> Select </option>')
}

function DropdownOptionmodified() {
    $('#drpimageparameter').find('option').remove();            // In this function update outputparameter  option modified image parameter dropdown
    var parameter = "";
    var editvalues = $('[id$=hidconid]').val();
    parameter = $('#MainCanvas_txtDbCommand_SqlQueryPara').val();
    if (editvalues != "" && parameter == "") parameter = $('[id$=hidimageoutput]').val();
    parameter = replaceAll(';', '', parameter);
    $('[id$=hidimageoutput]').val(parameter);
    parameter = parameter.split(',');
    $('#drpimageparameter').append('<option value="-1" selected="selected"> Select </option>')
    $.each(parameter, function (val, text) {
        $('#drpimageparameter').append($('<option></option>').val(text).html(text));
    });
}

//------------Do Not execute if condition match-------
function DonotExecute() {
    var Editdata = $('[id$=hidnoexecutecondtion]').val();
    var ObjEditdata = [];
    if (Editdata != undefined && Editdata.length > 0)
        ObjEditdata = jQuery.parseJSON(Editdata);

    var parameter = $('#lblInputParameters').text();
    parameter = parameter.trim();
    if (parameter != undefined && parameter != null && parameter.length > 0 && parameter != 'No Parameters defined yet') {
        parameter = parameter.split(',');
        var $table = $('#tabexe');
        $('#tabexe').children().remove();
        $table.addClass('detailsTable');

        $table.append('<thead>').children('thead')
            .append('<tr />').children('tr').append('<th></th><th>Parameter</th><th>Value</th>');

        var $tbody = $table.append('<tbody />').children('tbody');

        if (parameter.length > 0) {
            for (var i = 0; i <= parameter.length - 1; i++) {
                var $tr = $('<tr></tr>');
                var $td = $('<td class="w_50p"></td>');
                var $checkbox = $('<input type=checkbox   id=' + 'chk_' + parameter[i].trim() + '>');
                $checkbox.click(function () { Executetest(this); });
                $td.append($checkbox);
                $tr.append($td);

                var $span = $('<span>' + parameter[i].trim() + '</span>');
                $td = $('<td></td>');
                $td.append($span);
                $tr.append($td);

                var $textbox = $('<input type="text"  +  id = ' + 'txt_' + parameter[i].trim() + ' >');
                $td = $('<td></td>');
                $td.append($textbox);
                $tr.append($td);
                $tbody.append($tr);
                $table.append($tbody);
                $textbox.prop('disabled', true);
                $textbox.blur(function () {
                    SaveNoExecuteCondition(this);
                });

                for (var k = 0; k < ObjEditdata.length; k++) {
                    if (ObjEditdata[k].para == parameter[i].trim()) {
                        var chkitem = $('#chk_' + parameter[i].trim());
                        chkitem.attr("checked", "checked");
                        $('#' + 'txt_' + parameter[i].trim()).val(ObjEditdata[k].val);
                        Executetest(chkitem);
                        break;
                    }
                }
            }
        }
        showModalPopUp('divdonotexecutedetails', 'Donot Execute', 500);
    }
    else {
        alert("Input parameter not defined");
    }
}

function SaveNoExecuteCondition(control, para) {
    var Savedata = $('[id$=hidnoexecutecondtion]').val();
    var ObjSavedata = [];
    var objss = [];
    var noexutevalue = "";
    if (Savedata != undefined && Savedata.length > 0) {
        ObjSavedata = jQuery.parseJSON(Savedata);
        if (control != undefined && control != '') {
            var para = $(control).attr("id");
            para = para.replace("txt_", "");
            para = para.trim();
            var flag = true;
            for (var l = 0; l <= ObjSavedata.length - 1; l++) {
                if (ObjSavedata[l].para == para) {
                    ObjSavedata[l].val = $('#' + 'txt_' + para).val();
                    flag = false;
                    break;
                }
            }
            if (flag) {
                ObjSavedata.push({ "para": para, "val": $('#' + 'txt_' + para).val() });
            }
        }
        else {
            for (var l = 0; l <= ObjSavedata.length - 1; l++) {
                if (ObjSavedata[l].para == para) {
                    ObjSavedata.splice(l, 1);
                    break;
                }
            }
        }
    }
    $('[id$=hidnoexecutecondtion]').val(JSON.stringify(ObjSavedata));
}

function Executetest(control) {
    var check = $(control).is(':checked');
    var controltext = $(control).attr("id");
    controltext = controltext.replace("chk_", "");
    controltext = controltext.trim();
    if (check === true) {
        $('#' + 'txt_' + controltext).removeAttr("disabled");
        SaveNoExecuteCondition($('#' + 'txt_' + controltext), controltext);
    }
    else {
        $('#' + 'txt_' + controltext).val('');
        $('#' + 'txt_' + controltext).attr('disabled', true);
        SaveNoExecuteCondition('', controltext);
    }
}

function DefinedexecutePaameter() {
    $('#divdonotexecutedetails').dialog('close');
    Noexecuteparameter();
    return false;
}

function Noexecuteparameter() {
    var editdetails = '';
    editdetails = $('[id$=hidnoexecutecondtion]').val();
    if (editdetails != '' && editdetails != undefined) {
        var objjson = jQuery.parseJSON(editdetails);
        editdetails = '';
        $.each(objjson, function (idx, obj) {
            if (editdetails == '') editdetails += obj.para + '=' + obj.val;
            else editdetails += ', ' + obj.para + '=' + obj.val;
        });
        $('[id$=Labeldonotexecut]').text(editdetails);
    }
}

//--------------Cache-----------------

function Abmonth(v1) {
    var DCatch = $(v1).val();
    absolute(DCatch);
}

function absolute(DCatch) {
    $('[id$=Catcmin]').hide();
    $('[id$=AbEvDayDiv]').hide();
    $('[id$=AbEvWkDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$=AbEvYrDiv]').hide();
    $('[id$=RltDivmin]').hide();
    $('[id$=RltHour]').hide();
    $('[id$=RltDay]').hide();
    $('[id$=RltWeek]').hide();
    $('[id$=RltMonth]').hide();
    if (DCatch == '0') {
        $('[id$=Catcmin]').show();
    }
    else if (DCatch == '1') {
        $('[id$=AbEvDayDiv]').show();
    }
    else if (DCatch == '2') {
        $('[id$=AbEvWkDiv]').show();
    }
    else if (DCatch == '3') {
        $('[id$=AbEvMnDiv]').show();
    }
    else if (DCatch == '4') {
        $('[id$=AbEvYrDiv]').show();
    }
}

function Relative(v1) {
    var DCatch = $(v1).val();
    relativetcatch(DCatch);
}
function relativetcatch(DCatch) {
    $('[id$=RltDivmin]').hide();
    $('[id$=RltHour]').hide();
    $('[id$=RltDay]').hide();
    $('[id$=RltWeek]').hide();
    $('[id$=RltMonth]').hide();
    $('[id$=Catcmin]').hide();
    $('[id$=AbEvDayDiv]').hide();
    $('[id$=AbEvWkDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$= AbEvYrDiv]').hide();
    if (DCatch == '0') {
        $('[id$=RltDivmin]').show();
    }
    else if (DCatch == '1') {
        $('[id$=RltHour]').show();
    }
    else if (DCatch == '2') {
        $('[id$=RltDay]').show();
    }
    else if (DCatch == '3') {
        $('[id$=RltWeek]').show();
    }
    else if (DCatch == '4') {
        $('[id$=RltMonth]').show();
    }
}

function ClearCatch() {
    $("#hidSelectcatch").val("");
}

function SelectDD(v2) {
    var Dval = $(v2).val();

    if (Dval == "1") {
        var hour = {
            "01": "01",
            "02": "02",
            "03": "03",
            "04": "04"
        };
        var val, text;
        for (text in hour) {
            val = hour[text];
            $('<option/>').val(val).text(text).appendTo($('[id$=SelectddlAbHH]'));
        };
    }
    else {
        var min = {
            "01": "01",
            "02": "02",
            "03": "03",
            "04": "04",
            "05": "05",
            "06": "06",
            "07": "07"
        };
        var val, text;
        for (text in min) {
            val = min[text];
            $('<option/>').val(val).text(text).appendTo($('[id$=SelectddlAbHH]'));
        };

    }

}

function Hide(v1) {
    var DCatch = $(v1).val();
    $('[id$=ddlRlt]').val('0');
    $('[id$=dd1AbMonth]').val('0');
    catchtype(DCatch);
}

function catchtype(val) {
    $('[id$=CatchEvery]').hide();
    $('[id$=RltDiv]').hide();
    $('[id$=RltDivmin]').hide();
    $('[id$=RltHour]').hide();
    $('[id$=RltDay]').hide();
    $('[id$=RltWeek]').hide();
    $('[id$=RltMonth]').hide();
    $('[id$=AbEvDayDiv]').hide();
    $('[id$=AbEvWkDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$=AbEvYrDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$=AbEvYrDiv]').hide();
    if (val == '1') {
        $('[id$=CatchEvery]').show();
        $('[id$=dd1DataCatch]').css({ 'width': 200 });
        $('select[id$=CatchEvery]').val('0');
        absolute("0");
    }
    else if (val == '2') {
        $('[id$=RltDiv]').show();
        $('[id$=dd1DataCatch]').css({ 'width': 200 });
        $('select[id$=RltDiv]').val('0');
        relativetcatch('0');
    }
    else {
        $('select[id$=dd1DataCatch]').val('0');
        $('select[id$=dd1DataCatch]').parent().children("span").text($('select[id$=dd1DataCatch]').children('option:selected').text());
        absolute('0');
        Relative('0');
    }
}
