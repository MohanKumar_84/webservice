﻿// Table DataSet
var selectdataset;
var dataset;
var _selectvalue;
var discardobject;
var index;
var setclass;
var EditIndex;
//Confirmation message popup
function SubProcConfirmBoxMessage(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
        $('#btnCnfFormSave').focus();
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

//Database connector popup
function SubProcPriview(_bol) {
    if (_bol) {
        showModalPopUp('SubProcPriview', 'Database Connector', 505);
    }
    else {
        $('#SubProcPriview').dialog('close');
    }
}


function SubProcImageDelConfirnmation(_bol, _title) {
    SubProcCommonMethod('SubProcImageDelConfirnmation', _bol, "Delete Source", 300);
}

//Uniform DropDown Style Purpose this Function Use

// Confirmation message popup
function SubProcConfirmBoxMessageDB(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}
//show delete message popup
function SubProcBoxConnectedMessage(_bol) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', 'Info', 250);
       // $('#btnOkErrorMsg').focus();
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

//show delete message popup
function SubProcImageNotDelConfirmation(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Delete Not Allowed', 400);

    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}

function DeleteClick() {
    $('#EditDbConnTxtDiv').hide();
    $('#DbConnDetailsDiv').hide();

}

// function in the Catch Condition
// -------------------- NewChangesJS-----------------------------//

// Instiallize the Control
function initializeDbcontrol() {
    imagedbexpand();
    $('#add').show();
    $('#Edit').hide();
    $('[id$=hideditdone]').val('');
    $('[id$=lblconname]').val('');
    //$('[id$=txtConnectionName]').show();
    //  $('[id$=lblconname]').hide();
    $('[id$=divconnectionname]').show();
    $('[id$=divtxtconnectionname]').show();
    $('[id$=divsourcelabel]').hide();
    $('[id$=divsourcelabelname]').hide();

    $('[id$=txtConnectionName]').val('');
    $('[id$=ddlUseMplugin]').val('-1');
    $('select[id$=ddlUseMplugin]').parent().children("span").text($('select[id$=ddlUseMplugin]').children('option:selected').text());
    $('[id$=ddlDbConnectorDbType]').val('-1');
    $('select[id$=ddlDbConnectorDbType]').parent().children("span").text($('select[id$=ddlDbConnectorDbType]').children('option:selected').text());
    $('[id$=dbcredential]').val('-1');
    $('[id$=ddlDbConnectorDbType]').css('width', '143px')
    if ($('select[id$=ddlDbConnectorDbType]').children().last().val() == "5") {
        $('select[id$=ddlDbConnectorDbType]').children().last().remove();
    }
    $('[id$=hidcreuid]').val('');
    $('[id$=hidcrepwd]').val('');
    $('[id$=ddlDbDsns]').val('-1');
    $('[id$=dbcredential]').val('-1');
    $('[id$=txtHostName]').val('');
    $('[id$=txtDatabase]').val('');
    $('[id$=txtDatabaseTimeOut]').val('');
    $('[id$=txtDatabaseAdditionalString]').val('');
    $('[id$=lbldbconnector]').val('');
    $('[id$=lblDbConn_Name]').val('');
    $('[id$=lblDbConn_UseMplugin]').val('');
    $('[id$=lblDbConn_Type]').val('');
    $('[id$=lbltype]').val('');
    $('[id$=lblDbConn_Database]').val('');
    $('[id$=lblDbConn_Host]').val('');
    $('[id$=lblDbConn_DatabaseTimeOut]').val('');
    $('[id$=lblDbConn_DatabaseAdditionalString]').val('');
    $('[id$=lblcreatedby]').val('');
    $('[id$=lblupdatedby]').val('');
    $('[id$=lblupdatedby]').val('');
    $('[id$=lblActivetime]').val('');
    $('[id$=hiddatabaseobject]').val('');
    $('[id$=hdfDbConnId]').val('');
    $('[id$=hidedit]').val('');
    $('[id$=hidusernamet]').val('');
    $('[id$=lblDbConnectionName]').val('');
    $('[id$=lblconname]').val('');
    $('[id$=lbldbCredential]').val('');
    $('[id$=hdfUseMplugin]').val('');
    $('[id$=hiddbpassword]').val('');
    $('[id$=hidupdateconid]').val('');
    $('[id$=lblTestConnRslt]').val('');
    $('[id$=hdfDelId]').val('');
    $('select[id$=dbcredential]').val("-1");
    $('select[id$=dbcredential]').parent().children("span").text($('select[id$=dbcredential]').children('option:selected').text());
    $('[id$=ddlDbConnectorDbType]').removeAttr("disabled");

}


// Datatable the After Creation View Event Work
function viewConnection(id) {
    initializeDbcontrol();
    $('#DbConnDetailsDiv').show();
    $('#Adddetails').hide();
    for (var i = 0; i < dataset.length; i++) {
        if (id == dataset[i].CONNECTION_NAME) {
            discardobject = dataset[i].CONNECTION_NAME;

         
            $('[id$=lblDbConn_UseMplugin]').text(dataset[i].MPLUGIN_AGENT);
            $('[id$=lblDbConn_Database]').text(dataset[i].DATABASE_NAME);
            $('[id$=lblDbConn_Host]').text(dataset[i].HOST_NAME);
            $('[id$=lbldbconnector]').text(dataset[i].CONNECTION_NAME);
            $('[id$=lblDbConn_Name]').text(dataset[i].CONNECTION_NAME);
            $('[id$=hiddbconnectornames]').val(dataset[i].CONNECTION_NAME);

            $('[id$=hidcommadagin]').val(dataset[i].CONNECTION_NAME);
            $('[id$=lblDbConnectionName]').text(dataset[i].CONNECTION_NAME);
            $('[id$=lblDbConn_DatabaseAdditionalString]').text(dataset[i].ADDITIONAL_STRING);
            $('[id$=lblDbConn_Type]').text(dataset[i].DB);
            $('[id$=lblDbConn_Type]').val($(dataset[i].DATABASE_TYPE));
            $('[id$=hdfDelId]').val(dataset[i].DB_CONNECTOR_ID);
            $('[id$=lblDbConn_UseMplugin]').text(dataset[i].MPLUGIN_AGENT);
            var agent = '';
            agent = dataset[i].MPLUGIN_AGENT
            if (agent == 'None') {
                $('[id$=ddlUseMplugin]').val('-1');
            }
            else {
                $('[id$=ddlUseMplugin]').val(agent);
            }
            $('[id$=txtDatabase]').val(dataset[i].DATABASE_NAME);
            $('[id$=txtHostName]').val(dataset[i].HOST_NAME);
            $('[id$=txtDatabaseAdditionalString]').val(dataset[i].ADDITIONAL_STRING);
            $('[id$=txtDatabaseTimeOut]').val(dataset[i].TIME_OUT);
            $('[id$=lbldatabasetype]').text(dataset[i].DB);
            $('[id$=txtConnectionName]').val(dataset[i].CONNECTION_NAME);
            $('[id$=lblconname]').text(dataset[i].CONNECTION_NAME);
            $('[id$=lblcreatedby]').text(dataset[i].CreatedBY);
            $('[id$=lblupdatedby]').text(dataset[i].FULL_NAME + ' On  ' + dataset[i].DateTime);
            $('[id$=lbldatabaseobject]').text(dataset[i].DT_OBJ);
            var str = dataset[i].DT_OBJ;
            if (str === "" || str === "undefined" || str === null) {
                str = "No Database objects Added In this Source";
                var vtest = "<div class=\"ClCmdDMHeader\" style=\"width:100%;background-color:inherit;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + "&nbsp;&nbsp;" + str + "&nbsp;&nbsp;" + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
                $('#WsCmdAppDiv').html(vtest);
            }
            else {
                var str = dataset[i].DT_OBJ;
                var vtest = "<div class=\"ClCmdDMHeader\" style=\"width:100%;background-color:inherit;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + "&nbsp;&nbsp;" + str + "&nbsp;&nbsp;" + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
                $('#WsCmdAppDiv').html(vtest);
            }
            $('[id$=hiddatabaseobject]').val(dataset[i].DT_OBJ);
            $('[id$=hidupdateconid]').val(dataset[i].DB_CONNECTOR_ID);
            $('select[id$=dbcredential]').parent().children("span").text('');
            $('select[id$=dbcredential]').val(dataset[i].CREDENTIAL_PROPERTY);
            $('select[id$=dbcredential]').parent().children("span").text($('select[id$=dbcredential]').children('option:selected').text());

            $('[id$=lbldbCredential]').text($('select[id$=dbcredential]').parent().children("span").text());
            SelectedCredential(dataset[i].CREDENTIAL_PROPERTY);
            if (dataset[i].MPLUGIN_AGENT != "None") { $('[id$=hdfUseMplugin]').val(dataset[i].MPLUGIN_AGENT); }
            $('[id$=ddlUseMplugin]').parent().children("span").text(dataset[i].MPLUGIN_AGENT);
            $('[id$=hiddrpconnectortype]').val(dataset[i].DATABASE_TYPE);
            var vdsn = dataset[i].DATABASE_TYPE;
            if (vdsn === "5") {
                $('select[id$=ddlDbConnectorDbType]').append("<option value=5>DSN(ODBC)</option>");
                $('[id$=ddlDbDsns]').val(dataset[i].DATABASE_NAME);
            }
            $('select[id$=ddlDbConnectorDbType]').val(vdsn);
            $('select[id$=ddlDbConnectorDbType]').parent().children("span").text($('select[id$=ddlDbConnectorDbType]').children('option:selected').text());
            $('[id$=ddlDbConnectorDbType]').attr('disabled', 'disabled');
            $('[id$=ddlDbConnectorDbType]').val(dataset[i].DATABASE_TYPE);
            timeOut(dataset[i].TIME_OUT);
            showConnectionDiv(dataset[i].DATABASE_TYPE);
          
            break;
        }
    }
}

// In label Timout Define
function timeOut(strTimeOut) {
    if (strTimeOut === "" || strTimeOut === "undefined" || strTimeOut === '') {

    }
    else {
        $('[id$=lblDbConn_DatabaseTimeOut]').text(strTimeOut);
    }
}

// Div Show Hide According Connection database type
function showConnectionDiv(strType) {
    if (strType == '1' || strType == '2' || strType == '3' || strType == '4' || strType == '' || strType == 'undefined') {

        $('#NonDsnDetails').show();
        $('#ddlDbNonDsnDiv').show();
        $('#ddlDbDsnDiv').hide();
    }
    else {

        $('#NonDsnDetails').hide();
        $('#ddlDbDsnDiv').show();
        $('#dd1dbDsndivitem').show();
        $('#ddlDbNonDsnDiv').hide();
    }
}

// Table Create In this function

function createTable() {
    $('#divDataSources').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tbldatasources"></table>');
    $('#tbldatasources').dataTable({
        "language": {
            "lengthMenu": " _MENU_"
        },
        "data": selectdataset,
        "lengthMenu": [[20, 25, 30, 45, 55, -1], [20, 25, 30, 45, 55, "All"]],

        "columns": [
            { "title": "NAME" },
            { "title": "DATABASE" },
               { "title": "TYPE" },
              { "title": "MPLUGIN" }
        ]
    });
    var table = $('#tbldatasources').DataTable();
    searchText("tbldatasources");
    $('#tbldatasources tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var rowData = table.row(this).data();  // In this Row Data Fetch

        if (rowData != undefined) {
            var val = $('[id$=hideditdone]').val();
            if (val == '1') {

                SubModifiction(true);
                viewConnectiontest(rowData[0], $(this).index());
              //  EditIndex = $(this).index();
            }
            else {
                viewConnection(rowData[0]);
                EditIndex = $(this).index();
                setclass = '';
            }
        }
        else {
            alert("No Row Selected");
            $('#DbConnDetailsDiv').hide();
            $('#Adddetails').show();
        }
    });
  makeSelectUniformOnPostBack();
    $('#tbldatasources_filter').css("padding-bottom", "10px");

}

// DataBase Load

function bindConnector() {
    var vtest = $('[id$=hiddatasource]').val();
    if (vtest == "" || vtest == '' || vtest == 'undefined') {
    }
    else {
        getdata();
    }
    createTable();
}


// Get Datatable Data
function getdata() {
    selectdataset = jQuery.parseJSON($('[id$=hiddatasource]').val());
    dataset = jQuery.parseJSON($('[id$=hiddataset]').val());
    $('[id$=hiddatasource]').val('');
    $('[id$=hiddataset]').val('');
}


// All Object Details Show
function imgWsCmdAppClick(e) {
    if (e.alt.trim() == 'Expand') {
        $(e).attr('src', '//enterprise.mficient.com/images/collapse.png');
        $(e).attr('alt', 'Collapse');
        $(e).attr('title', 'Collapse');
        $('#divtest').show();
    }
    else {
        $(e).attr('src', '//enterprise.mficient.com/images/expand.png');
        $(e).attr('alt', 'Expand');
        $(e).attr('title', 'Expand');
        $('#divtest').hide();
    }

}

// After Delete View Panel Hide
function afterDeleteHideView() {
    $('#DbConnDetailsDiv').hide();
    $('#Adddetails').show();

}

//Database connector delete link event
function DbConnectorDeleteClick() {
    $('[id$=hdfDelType]').val('1');
    $('#aDelCfmMsg').html('Are you sure you want to delete this database source?'); SubProcImageDelConfirnmation(true, 'Delete Database Connector');
}

//Cancel Button  Show

function cancelButtonShow() {
    $('[id$=btnCancelDbConnBack]').show();
    $('[id$=btnreset]').hide();
}
//   Cancel Button Hide
function cancelButtonhide() {
    $('[id$=btnCancelDbConnBack]').hide();
    $('[id$=btnreset]').show();
}
// Hide Add Div
function aftertestClick() {
    $('#AddNewConnDiv').hide();
}

//Dns
function dsnTestConnection() {

    $('#NonDsnDetails').hide();
    $('#ddlDbDsnDiv').show();
    $('#dd1dbDsndivitem').show();
    $('#ddlDbNonDsnDiv').hide();
}


//Common error message method.
function CommonErrorMessages(_MsgNo) {
    switch (_MsgNo) {
        case 29:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter only numeric values.');
            break;
    }
    SubProcBoxMessage(true);
}
//Database connector edit link event
function showConndivOnEdit() {
    $('#DbConnDetailsDiv').hide();
    $('#Adddetails').show();
    $('#add').hide();
    $('#Edit').show();
    $('[id$=divconnectionname]').hide();
    $('[id$=divtxtconnectionname]').hide();
    $('[id$=divsourcelabel]').show();
    $('[id$=divsourcelabelname]').show();

    $('[id$=btnCancelDbConnBack]').show();
    $('[id$=btnreset]').hide();
    $('[id$=hideditdone]').val('1');
    if ($('[id$=hiddrpconnectortype]').val() == '5') {
        $('[id$=btnDsnGet]').click();
    }
}

//Database connector edit link event
function RemoveshowConndivOnEdit() {
    $('[id$=divconnectionname]').show();
    $('[id$=divtxtconnectionname]').show();
    $('[id$=divsourcelabel]').hide();
    $('[id$=divsourcelabelname]').hide();
    $('[id$=btnCancelDbConnBack]').hide();
    $('[id$=btnreset]').show();
    $('select[id$=ddlDbConnectorDbType]').val('-1');
    $('select[id$=ddlDbConnectorDbType]').parent().children("span").text($('select[id$=ddlDbConnectorDbType]').children('option:selected').text());
    $('[id$=ddlDbConnectorDbType]').prop('disabled', false);
    $('select[id$=ddlUseMplugin]').val('-1');
    $('select[id$=ddlUseMplugin]').parent().children("span").text($('select[id$=ddlUseMplugin]').children('option:selected').text());
    initializeDbcontrol();
    showConnectionDiv('1');
}

//txtDatabaseTimeOut keypress events
function DbConnTimeoutKeypress(event) {
    if (event.keyCode == 46 || event.keyCode == 8) {
    }
    else {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
            CommonErrorMessages(29);
        }
    }
}
//Cancel Edit Button
function cancelUpdate() {
    $('#DbConnDetailsDiv').show();
    $('#Adddetails').hide();
    $('[id$=hideditdone]').val('');

}


function addDSN() {
    $('select[id$=ddlDbConnectorDbType]').append("<option value=5>DSN(ODBC)</option>");
}
function imagedbexpand() {
    var Ctr = $('#img14');
    Ctr.attr('src', '//enterprise.mficient.com/images/expand.png');
    Ctr.attr('alt', 'Expand');
    Ctr.attr('title', 'Expand');
    $('#divtest').hide();

}

//Function  Selected StoreProcedure 
function SelectedCredential(_selectvalue) {
    var $hid = $('[id$=hidallcredential]');
    var permanent = '';
    var inputpara = '';
    var vinputpara = '';
    if (_selectvalue != "-1") {
        if ($hid.val()) {
            permanent = jQuery.parseJSON($hid.val());
            for (var i = 0; i < permanent.length; i++) {
                if (_selectvalue == permanent[i].CreadentialId) {
                    $('[id$=hidcreuid]').val('');
                    $('[id$=hidcrepwd]').val('');
                   $('[id$=txtdbusercreatial]').val('');
                    var username = permanent[i].UserId;
                    var password = permanent[i].Password;
                    var typeval = permanent[i].Typeval;
                    $('[id$=hidtypevalcredential]').val(permanent[i].Typeval);
                    $('[id$=hidcreuid]').val(username);
                    $('[id$=hidcrepwd]').val(password);
                    $('[id$=txtdbusercreatial]').removeAttr("disabled");
                    if (username !='') {
                        $('[id$=txtdbusercreatial]').val(username);
                        $('[id$=txtdbusercreatial]').attr("disabled", "disabled"); 
                    }
                    break;
                }
            }
        }
    }
}
function Setname() {
    var dbcname = $('[id$=hiddbconnectornames]').val();
    $('[id$=lblconname]').text(dbcname);
}


function DbValidation() {
    var ConnectionName, HostName, DatabaseName, dbcredential, Dbtype, Dsn, mplugin;
    ConnectionName = $('[id$=txtConnectionName]').val();
    HostName = $('[id$=txtHostName]').val();
    DatabaseName = $('[id$=txtDatabase]').val();
    dbcredential = $('select[id$=dbcredential]').val();
    Dbtype = $('select[id$=ddlDbConnectorDbType]').val();
    mplugin = $('select[id$=ddlUseMplugin]').val();
    Dsn = $('select[id$=ddlDbDsns]').val();
    var strMessage = "";
    if (ConnectionName == '' && HostName == '' && DatabaseName == '') {
        strMessage += "Please check following points</br> </br> ";
    }
    else if (ConnectionName == '') {

        strMessage += "* Please enter  sources  name.</br>";
    }
    else if (ConnectionName.length > 20) {
        strMessage += "* Source  name cannot be more than 20 characters.</br>";
    }
    else if (ConnectionName.length < 3) {
        strMessage += "* Source  name cannot be less than 3 characters.</br>";
    }
    else if (ConnectionName.length >= 50) {
        strMessage += "* Source  name not greater than 50 charcter.</br>";

    }
    if (Dbtype == '' || Dbtype == '-1' || Dbtype == 'undefined') {
        strMessage += " * Please select   database type.</br>";

    }
    if (Dbtype == '5') {

        if (Dsn == '-1' || Dsn == 'undefined' || Dsn == '') {
            strMessage += "*Please select Dsn name.</br>";
        }
        if (mplugin == "-1" || mplugin == 'undefined' || mplugin == '') {
            strMessage += "* Please select mplugin.</br>";
        }

    }
    else {
        if (HostName == '') {
            strMessage += "* Please enter  hostname.</br>";
        }
        if (HostName.length > 60 ) {
            strMessage += "* Host name cannot be more than 60 characters.</br>";
        }
        if (DatabaseName == '') {
            strMessage += "* Please enter  database name.</br>";
        }
        if (DatabaseName.length > 50) {
            strMessage += "* Database name cannot be more than 50 characters.</br>";

        }
        if (dbcredential == '' || dbcredential == '-1' || dbcredential == 'undefined' && Dbtype != '5') {
            strMessage += "* Please select credential.</br>";
        }
    }
    if (strMessage.length > 0) {
        showMessage(strMessage, DialogType.Error);
        return false;
    }

}


//Database connector edit
function DataCredentialBy(_bol) {
    if (_bol) {

        showModalPopUp('divdb', 'Enter Credential', 300);
     //   $('[id$=txtdbusercreatial]').val('');
        $('[id$=txtdbpasswordcreatial]').val('');
    }
    else {
        $('#divdb').dialog('close');
    }
}




// function Defined Credtial
function SetdbCretaial() {
    Clearcredential();
    var user = $('[id$=txtdbusercreatial]').val();
    var password = $('[id$=txtdbpasswordcreatial]').val();
  $('[id$=hidcreuid]').val(user);
  $('[id$=hidcrepwd]').val(password);
}
function Credentialvalidation() {
    var user = $('[id$=txtdbusercreatial]').val();
    var password = $('[id$=txtdbpasswordcreatial]').val();
    if (user == '') {
        alert('please enter username');
    }
    else if (password == '') {
        alert('please enter password');
    }
    if (user != '' && password != '') {
        DataCredentialBy(false);
         SetdbCretaial();
        return true;
    }
    else {
        return false;
    }
    
}



function texteditConnectionbtnclick() {
    $('[id$=hidtestconnection]').val('1');
}

function textviewConnectionbtnclick() {

    $('[id$=hidtestconnection]').val('0');
}


function Clearcredential() {
    $('[id$=hidcreuid]').val('');
    $('[id$=hidcrepwd]').val('');
}

function CredentialOnchange() {
    var vcre = $('select[id$=dbcredential]').val();
    var conid = $('[id$= hidupdateconid]').val();
    var connectorname=$('[id$= hiddbconnectornames]').val();

    if (conid != '' || conid != 'undefined' && vcre != '-1') {
  
        SelectedCredential(vcre);
    }
    else if (conid != "" || conid != 'undefined' && vcre == '-1') {
        viewConnection(connectorname);

        $('[id$=hidcreuid]').val('');
        $('[id$=hidcrepwd]').val('');
    }
    else if (vcre != '-1') {

        SelectedCredential(vcre);
    }
}

function ddConectordesable() {
    $('[id$=ddlDbConnectorDbType]').attr('disabled', 'disabled');
}


function discardedit() {
    if (setclass.toString() != '') {
        viewConnection(discardobject);
        ChangeRowSelectedCSS(setclass);
    }
    else {
        openaddform();
        setclass = '';
        index = '';
    }
}


function viewConnectiontest(id,index) {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('1');
    setclass = '';
    setclass = index;
    for (var i = 0; i < dataset.length; i++) {
        if (id == dataset[i].CONNECTION_NAME) {
            discardobject = '';
            discardobject = dataset[i].CONNECTION_NAME;
            $('[id$=hidcommadagin]').val(dataset[i].CONNECTION_NAME);
            break;
        }
    }
}


function Dbaddbtndiscard() {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('2');
}


function openaddform() {
    initializeDbcontrol();
    createTable();
    setclass = '';
    index = '';
    $('[id$=btnreset]').show();
    $('[id$=btnCancelDbConnBack]').hide();
}

function Discarddataobjectcancel() {
    var postback = $('[id$=hdfPostBackPageMode]').val();
    var v = $('[id$=hidcommadagin]').val();
    if (postback == 'dbsource') {
        //viewConnection(v);
        showConndivOnEdit();
        ChangeRowSelectedCSS(EditIndex);
    }
}

function ChangeRowSelectedCSS(val) {
    selIndex = val;
    if (selIndex != undefined) {
        $('#tbldatasources tbody tr.selected').removeClass('selected');
        if (selIndex % 2 === 0) {
            $('#tbldatasources tbody tr').eq(selIndex).addClass('even selected')
        }
        else {
            $('#tbldatasources tbody tr').eq(selIndex).addClass('odd selected')
        }
    }
}







if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();