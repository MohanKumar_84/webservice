﻿var selectwsdataset;
var wsdataset;
var permanent;
var discardobject;
var index;
var setclass;
var EditIndex;

function InitializeWsConnectors() {
    var wsConnList = $('[id$=hidwsdataset]').val();
    if (wsConnList !== "" && wsConnList !== 'undefined' && wsConnList !== null) {
        selectwsdataset = jQuery.parseJSON($('[id$=hidwsdataset]').val());
        wsdataset = jQuery.parseJSON($('[id$=hidwscdataset]').val());
    }
    BindWsConnectors();
    $('#wsEiditedtxtDiv').hide();
}

//initialize control value (Add new connector)
function WsAddNewConnector() {
    InitializeControls();
    $('[id$=WsConnDetailsDivview]').hide();
    $('[id$=WsConnAddDetailsDiv]').show();
    $('[id$=editwsdivheader]').hide();
    $('[id$=adddivheader]').show();
    $('[id$=divWsConName]').show();
    $('[id$=wsEiditedtxtDiv]').hide();
    InitializeWsConnectors();
    $('[id$=btnwsreset]').show();
    $('[id$=btnHtmWsConnectorBack]').hide();
}
//Connector edit link click event
function ShowWsConnDivOnEdit() {
    $('[id$=btnHtmWsConnectorBack]').show();
    $('[id$=btnwsreset]').hide();
    $('[id$=WsConnAddDetailsDiv]').show();
    $('[id$=editwsdivheader]').show();
    $('#WsConnDetailsDiv').hide();
    $('[id$=adddivheader]').hide();
    $('#wsEiditedtxtDiv').show();
    $('#WsConnDetailsDivview').hide();
    $(".DbCmdRowows").show();
    $('#divWsConName').hide();
    $('#lblwsconname').show();
    
    $('[id$=hideditdone]').val('1');
}
//Show http authentication div
function ShowWsConnHttpDiv(_Show) {
    if (_Show) {
        $('#httpAuthenticationDiv').show();
        chkhttpOnChangeChk(this);
    }
    else {
        $('#httpAuthenticationDiv').hide();
    }
}

//chkhttp chnage event
function chkhttpOnChange(e) {
    ShowWsConnHttpDiv($('[id$=chkhttp]')[0].checked);
}

//Message box in wsdlfile upload
function SubProcBoxUldWsdlCfm(_bol, _width, _Type) {
    if (_bol) {
        // var strTitle = 'Error';
        if (_Type == 1)
            strTitle = "Upload Wsdl File";
        $('#UploadWsdlCfnDiv').show(); $('#btnUploadWsdlCfn').show(); $('#GetWsdlMannual').hide();
        showModalPopUp('SubProcBoxUldWsdlCfm', strTitle, _width);
    }
    else {
        $('#SubProcBoxUldWsdlCfm').dialog('close');
    }
}

function chkhttpOnChangeChk(e) {
    ShowFixedConnHttpDivsh(true);
}

//Show http authentication div
function ShowFixedConnHttpDivsh(_Show) {
    if (_Show) {
        $('#divCredential').show();
        $('#divCtlCredential').show();
        $('#Divwscredential').show();
        $('#divwscredentialtype').show();
    }
    else {
        $('#Divwscredential').hide();
        $('#divwscredentialtype').hide();
        $('#divCredential').hide();
        $('#divCtlCredential').hide();
    }
}

//Show/hide control on connector edit
function WsConnectorEdit(_Bol) {
    if (_Bol) {
        $('#wsEiditedLabelDiv').hide();
        $('#wsEiditedtxtDiv').show();
        $('#txtWsConnectionName').hide();
        $('#btnHtmWsConnectorBack').hide();
        $('[id$=txtWsConnectionName]').removeAttr('disabled');
        $('[id$=ddlWebSrcConnectorType]').removeAttr('disabled');
    }
    else {
        $('#wsEiditedtxtDiv').hide();
        $('#txtWsConnectionName').show();
        $('#btnHtmWsConnectorBack').show();
        $('[id$=txtWsConnectionName]').attr('disabled', 'disabled');
        $('[id$=ddlWebSrcConnectorType]').attr('disabled', 'disabled');
        $('#btnHtmWsConnectorBack').show();

    }
}

//btnHtmWsConnectorBack click event
function WsConnectorBackClick() {
    $('[id$=WsConnEditDetailsDiv]').hide();
    $('#WsConnDetailsDiv').show();
}

//Command delete link click event
function WsConnectorDeleteClick() {
    $('[id$=hdfDelType]').val('2');
    $('#aDelCfmMsg').html('Are you sure you want to delete this webservice Source ?'); SubProcImageDelConfirnmation(true, 'Delete Webservice Source');
}

//Add wsdl manual/ click event
function ShowGetWsdlMannualDiv() {
    $('#UploadWsdlCfnDiv').hide(); $('#btnUploadWsdlCfn').hide(); $('#GetWsdlMannual').show();
}

//ddlWebSrcConnectorType change event
function WebSrcConnTypeOnChange() {
    if ($('[id$=ddlWebSrcConnectorType]').length > 0) {
        if ($('[id$=ddlWebSrcConnectorType]')[1] != undefined) {
            ShowWsMIMEDiv($('[id$=ddlWebSrcConnectorType]')[1].value);
        }
    }

}

//Show hide MimeTypeDiv
function ShowWsMIMEDiv(_Value) {
    if (_Value == "http") $('#MimeTypeDiv').show();
    else $('#MimeTypeDiv').hide();
}



function WDataDisplay(id) {
    $('#WsConnDetailsDivview').show();
    $('#WsConnDetailsDiv').show();
    $('[id$=WsConnAddDetailsDiv]').hide();

    for (var i = 0; i < wsdataset.length; i++) {
        if (id == wsdataset[i].CONNECTION_NAME) {
            discardobject = wsdataset[i].CONNECTION_NAME;
            $('[id$=lblWsConn_Type]').text(wsdataset[i].WEBSERVICE_TYPEALIES);
            var agent = '';
            agent = wsdataset[i].MPLUGIN_AGENT;
            if (agent == 'None') {
                $('[id$=ddlWsUseMplugin]').val('-1');
            }
            else {
                $('[id$=ddlWsUseMplugin]').val(agent);
            }
            $('[id$=lblWsConn_UseMplugin]').text(wsdataset[i].MPLUGIN_AGENT);
            $('[id$=lblWsConn_Url]').text(wsdataset[i].WEBSERVICE_URL);
            $('[id$=lblwscretedby]').text(wsdataset[i].CreatedBY);
            $('[id$=lblwsupdatedby]').text(wsdataset[i].FULL_NAME + ' On  ' + wsdataset[i].DateTime);
            $('[id$=lblWsConn_RType]').text(wsdataset[i].RESPONSE_TYPE);
            var v1 = $.trim(wsdataset[i].WEBSERVICE_TYPE);
            var v2;
            if (v1 == 'http') {
                v2 = "REST";
                $('#MimeTypeDiv').show();
            }
            else if (v1 == 'wsdl') {
                v2 = "WSDL";
                $('#MimeTypeDiv').hide();
            }
            else {
                v2 = "XML-RPC";
                $('#MimeTypeDiv').hide();
            }
            $('[id$=ddlWebSrcConnectorType]').val(v1);
            $('[id$=ddlWebSrcConnectorType]').parent().children("span").text(v2);
            $('[id$=ddlWebSrcConnectorType]').prop("disabled", true);
            $('[id$=ddlWsUseMplugin]').parent().children("span").text(wsdataset[i].MPLUGIN_AGENT);
            $('[id$=txtWsConnectorUrl]').val(wsdataset[i].WEBSERVICE_URL);
            $('[id$=txtWsConnectionName]').val(wsdataset[i].CONNECTION_NAME);
            $('[id$=hidcommadagin]').val(wsdataset[i].CONNECTION_NAME);

            $('[id$=ddlWebSrcConnectorRsp]').val(wsdataset[i].RESPONSE_TYPE);
            $('[id$=ddlWebSrcConnectorRsp]').parent().children("span").text(wsdataset[i].RESPONSE_TYPE);
            $('[id$=drphttpauthenticationtype]').parent().children("span").val(wsdataset[i].RESPONSE_TYPE);
            $('[id$=hidwebdataobj]').val(wsdataset[i].WS_OBJ);
            var str = wsdataset[i].WS_OBJ;
            if (str === "" || str === "undefined" || str === null) {
                str = "No Web objects yet not added in this source";
                var vtest = "<div class=\"ClCmdDMHeader\" style=\"width:100%;background-color:inherit;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + "&nbsp;&nbsp;" + str + "&nbsp;&nbsp;" + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
            }
            else {
                str = wsdataset[i].WS_OBJ;
                var vtest = "<div class=\"ClCmdDMHeader\" style=\"width:100%;background-color:inherit;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + "&nbsp;&nbsp;" + str + "&nbsp;&nbsp;" + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
            }
            $('#WsCmdAppdtl').html(vtest);
            $('[id$=drphttpauthenticationtype]').val(wsdataset[i].AUTHENTICATION_TYPE);
            $('select[id$=drphttpauthenticationtype]').parent().children("span").text($('select[id$=drphttpauthenticationtype]').children('option:selected').text());
            if (wsdataset[i].CREDENTIAL_PROPERTY != "-1") {
                $('[id$=chkhttp]')[0].checked = true;
                chkhttpOnChange(this);
                var vcrendention = $.trim(wsdataset[i].CREDENTIAL_PROPERTY);
                $('[id$=drpCredentiail]').val(vcrendention);
                $('[id$=hidwscredebttial]').val(vcrendention);
                $('select[id$=drpCredentiail]').parent().children("span").text($('select[id$=drpCredentiail]').children('option:selected').text());
                if (vcrendention != "-1") {
                    SelectedWSCredential(vcrendention);
                }
            }
            else {
            }
            $('[id$=lblwsconnector]').text(wsdataset[i].CONNECTION_NAME);
            $('[id$=lblwsconname]').text(wsdataset[i].CONNECTION_NAME);
            $('[id$=lblwsname]').text(wsdataset[i].CONNECTION_NAME);
            $('[id$=hidconnames]').val(wsdataset[i].CONNECTION_NAME);
            $('[id$=lblWsConn_Name]').text(wsdataset[i].WS_CONNECTOR_ID);
            $('[id$=hdfWsConnId]').val(wsdataset[i].WS_CONNECTOR_ID);
            break;
        }
    }
}

function BindWsConnectors() {
    $('#divwssource').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="wstable"></table>');
    $('#wstable').dataTable({
        "language": {

            "lengthMenu": " _MENU_"

        },
        "data": selectwsdataset,
        "lengthMenu": [[20, 25, 30, 45, 55, -1], [20, 25, 30, 45, 55, "All"]],

        "columns": [
            { "title": "NAME" },
            { "title": "TYPE" },
            { "title": "URL" },
            { "title": "MPLUGIN" }

        ]
    });
    searchText("wstable");
    var table = $('#wstable').DataTable();
    $('#wstable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {

        }
        else {

            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var rowData = table.row(this).data();
        if (rowData != undefined) {
            var val = '';
            val = $('[id$=hideditdone]').val();
            if (val == '1') {
                SubModifiction(true);
                viewConnectiontest(rowData[0], $(this).index());
                setclass = $(this).index();

            }
            else {
                InitializeControls();
                WDataDisplay(rowData[0]);
                EditIndex = $(this).index();
              //  setclass = '';
            }
        }

        else {
            alert('No Row Data Selected');
            $('#WsConnDetailsDivview').hide();
            $('[id$=WsConnAddDetailsDiv]').show();
            $('#WsConnDetailsDiv').hide();
        }
    });
    makeSelectUniformOnPostBack();
    $('#wstable_filter').css("padding-bottom", "10px");
}

function viewConnectiontest(id,index) {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('3');
   // setclass = '';
    setclass = index;
    for (var i = 0; i < wsdataset.length; i++) {
        if (id == wsdataset[i].CONNECTION_NAME) {
            discardobject = '';
            discardobject = wsdataset[i].CONNECTION_NAME;
            $('[id$=hidcommadagin]').val(wsdataset[i].CONNECTION_NAME);
            break;
        }
    }
}

function discardedit() {
    if (setclass.toString() != '') {
        WDataDisplay(discardobject);
        ChangeRowSelectedCSS(setclass);
        EditIndex = '';
    }
    else {
        WsAddNewConnector();
        setclass = '';
        EditIndex = '';
    }
}

//            Confirmation message popup
function SubProcConfirmBoxMessage(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

function CancelUpdate() {
    $('[id$=hideditdone]').val('');
    WDataDisplay(discardobject);

}

function test() {
    $('[id$=WsConnAddDetailsDiv]').show();
    $('#WsConnDetailsDivview').hide();
}

function DeletetestClick() {
    $('[id$=WsConnAddDetailsDiv]').hide();
    $('[id$=WsConnDetailsDivview]').show();
    $('#WsConnDetailsDiv').show();
   // $('[id$=btnwsreset]').show();

}



function ChangeWsdlFileId(_Id) {
    $('[id$=hdfWsdlUploadId]').val(_Id);
}

function imgWsCmdAppClickStart(e) {
    if (e.alt.trim() == 'Expand') {
        $(e).attr('src', '//enterprise.mficient.com/images/collapse.png');
        $(e).attr('alt', 'Collapse');
        $(e).attr('title', 'Collapse');

        $('#divwstest').show();

    }
    else {
        $(e).attr('src', '//enterprise.mficient.com/images/expand.png');
        $(e).attr('alt', 'Expand');
        $(e).attr('title', 'Expand');

        $('#divwstest').hide();
    }

}
//show delete message popup
function SubProcImageNotDelConfirmation(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Delete Not Allowed', 400);
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}
//Odata connector edit
function WSDataCredentialBy(_bol) {
    if (_bol) {
        showModalPopUp('divWsPredefined', 'Enter Credetial', 300);
        $('[id$=txtwsspredefined]').val('');
        $('[id$=txtwspredefinedpassword]').val('');
    }
    else {
        $('#divWsPredefined').dialog('close');
    }
}

// Ws Function Reset Control

function InitializeControls() {
    // Edit and Add Connector Reset
    $('[id$=hideditdone]').val('');
    
    $('[id$=hdfWsConnId]').val('');
    $('[id$=lblwsconname]').val('');
    $('[id$=txtWsConnectionName]').val('');
    $('[id$=ddlWebSrcConnectorType]').val('-1');
    $('[id$=ddlWebSrcConnectorType]').change();
    $('[id$=ddlWebSrcConnectorType]').removeAttr('disabled');
    $('[id$=txtwsspredefined]').val('');
    $('[id$=txtwspredefinedpassword]').val('');
    $('[id$=txtWsConnectorUrl]').val('');
    $('[id$=hdfWsSavedUrl]').val('');
    $('[id$=ddlWsUseMplugin]').val('-1');
    $('[id$=ddlWsUseMplugin]').change();
    $('[id$=hdfWsUseMplugin]').val('-1');
    $('[id$=drphttpauthenticationtype]').val('0');
    $('select[id$=drphttpauthenticationtype]').parent().children("span").text($('select[id$=drphttpauthenticationtype]').children('option:selected').text());
    $('select[id$=ddlWsUseMplugin]').parent().children("span").text($('select[id$=ddlWsUseMplugin]').children('option:selected').text());
    $('select[id$=ddlWebSrcConnectorType]').parent().children("span").text($('select[id$=ddlWebSrcConnectorType]').children('option:selected').text());
    $('[id$=ddlWebSrcConnectorRsp]').val('XML');
    $('[id$=chkhttp]')[0].checked = false;
    $('[id$=drphttpauthenticationtype]').val('-1');
    $('[id$=drpCredentiail]').val('-1');
    $('select[id$=drpCredentiail]').val("-1");
    $('select[id$=drpCredentiail]').parent().children("span").text($('select[id$=drpCredentiail]').children('option:selected').text());
    $('[id$=txtwspredefinedpassword]').val('');
    $('[id$=txtwsspredefined]').val('');
    $('#httpAuthenticationDiv').hide();
    $('#divheadeAu').hide();
    $('[id$=lblWsConn_Name]').html('');
    $('[id$=lblWsConn_Type]').html('');
    $('[id$=lblWsConn_UseMplugin]').html('');
    $('[id$=lblWsConn_Url]').html('');
    $('[id$=lblWsConn_RType]').html('');
    $('[id$=lblwscretedby]').html('');
    $('[id$=lblwsupdatedby]').html('');
    $('[id$=lblWsConn_UserName]').html('');
    $('[id$=lblWsConn_Password]').html('');
    $('[id$=hidwebdataobj]').val('');
    $('[id$=hidwscredebttial]').val('');
    $('[id$=btnHtmWsConnectorBack]').hide();
    $('[id$=btnwsreset]').show();
    imageexpand();

}

// WebService Source
function imageexpand() {
    var Ctr = $('#img1');
    Ctr.attr('src', '//enterprise.mficient.com/images/expand.png');
    Ctr.attr('alt', 'Expand');
    Ctr.attr('title', 'Expand');
    $('#divwstest').hide();
}

//Function  Selected StoreProcedure 
function SelectedWSCredential(_selectvalue) {
    var $hid = $('[id$=hidwscreatedtial]');
    var permanent = '';

    if (_selectvalue != "-1") {

        if ($hid.val()) {
            permanent = jQuery.parseJSON($hid.val());

            for (var i = 0; i < permanent.length; i++) {

                if (_selectvalue == permanent[i].CreadentialId) {

                    $('[id$=hidwsusername]').val('');
                    $('[id$=hidwsusernamepassword]').val('');
                    $('[id$=hidwstypeofcredential]').val('');
                    var username = permanent[i].UserId;
                    var password = permanent[i].Password;
                    var typeval = permanent[i].Typeval;

                 
                    $('[id$=hidwsusername]').val(username);
                    $('[id$=hidwsusernamepassword]').val(password);
                    $('[id$=hidwstypeofcredential]').val(typeval);
                    break;
                }
            }
        }
    }
}

function resetclictadd() {
    $('[id$=WsConnDetailsDivview').hide();
    $('[id$=WsConnAddDetailsDiv]').show();
    $('[id$=adddivheader]').show();
    $('[id$=editwsdivheader]').hide();
    $('[id$=divWsConName]').show();
    $('[id$=wsEiditedtxtDiv]').hide();
}

function WsValidation() {
    var strVal, webdervicetype, url, wscredential, chk;
    var strMessage = '';
    strVal = $('[id$=txtWsConnectionName]').val();
    webdervicetype = $('select[id$=ddlWebSrcConnectorType]').val();
    url = $('[id$=txtWsConnectorUrl]').val();

    chk = $('[id$=chkhttp]')[0].checked;

    wscredential = $('select[id$=drpCredentiail]').val();
    if (strVal == '') {
        strMessage += " * Please enter webservice source name.</br>";
    }
    else if (strVal < 3) {
        strMessage += " * Source name cannot be less than 3 charecters.</br>";
    }
    else if (strVal.length > 20) {
        strMessage += " * Source name cannot be more than 20 charecters.</br>";
    }
    if (webdervicetype == '-1') {
        strMessage += " * Please select webservice type.</br>";
    }
    if (url == '') {
        strMessage += " * Please enter webservice url.</br>";
    }
    if (chk == true) {
        if (wscredential == '' || wscredential == '-1') {
            strMessage += "* Please select credential.</br>";
        }
    }
    if (strMessage.length > 0) {
        showMessage(strMessage, DialogType.Error);
        return false;
    }
}


function wscredential() {
    var vcre = $('select[id$=drpCredentiail]').val();
    var conid = $('[id$=hidconnames]').val();
    if (conid != ''  && vcre != '-1' ) {
        ShowWsConnDivOnEdit();
        SelectedWSCredential(vcre);
    }
    else if (conid != ''  && vcre == '-1' ) {
        ShowWsConnDivOnEdit();
        $('[id$=hidwsusernamepassword]').val('');
        $('[id$=hidwstypeofcredential]').val('');
    }
    else if (vcre != '-1') {
        SelectedWSCredential(vcre);
    }
}



function wsdataCredentialvalidation() {
    var user = $('[id$=txtwsspredefined]').val();
    var password = $('[id$=txtwspredefinedpassword]').val();
    if (user == '') {
        alert('Please enter username');
    }
//    else if (password == '') {
//        alert('Please enter password');
//    }
    if (user != '' && password != '') {
        WSDataCredentialBy(false);
        return true;
    }
  
    else {
        return false;
    }
}


function Wsaddbtndiscard() {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('4');
}


function Discarddataobjectcancel() {
    var postback = $('[id$=hdfPostBackPageMode]').val();
    var v = $('[id$=hidcommadagin]').val();
     if (postback == 'wssource') {
         ChangeRowSelectedCSS(EditIndex);
    }
}


function ChangeRowSelectedCSS(val) {
    selIndex = val;
    if (selIndex != undefined) {
        $('#wstable tbody tr.selected').removeClass('selected');
        if (selIndex % 2 === 0) {
            $('#wstable tbody tr').eq(selIndex).addClass('even selected')
        }
        else {
            $('#wstable tbody tr').eq(selIndex).addClass('odd selected')
        }
    }
}


//function IsDialog() {
//var boolcheck = $('#SubProobjectChange').dialog("isOpen");
//if (boolcheck == true)
//    SubModifiction(false);
//boolcheck = '';
//}

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();


 