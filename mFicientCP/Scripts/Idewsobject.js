﻿// Table Object DataSet
var selectwsobjdataset;
var objwsdataset;
var wsdlOutParamHash = {};
var wsdlMethodValue = "";
var wsdlCmpxHash = {};
var wsdlSimpleHash = {};
var wsdlMainHash = {};
var WsParaHash = {};
var discardobject;
var index;
var setclass;
var selIndex;
var vwebservictype;
var parameter;
var SearchText = "";

function BindDatabaseObjectdata() {
    $('#divwsdataobject').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tablewsobj"></table>');
    $('#tablewsobj').dataTable({
        "language": {
            "lengthMenu": "  _MENU_"
        },
        "data": selectwsobjdataset,
        "lengthMenu": [[20, 25, 30, 45, 55, -1], [20, 25, 30, 45, 55, "All"]],
        "columns": [
         { "title": "OBJECT" },
        { "title": "CONNECTOR" }
        ]
    });
    searchText("tablewsobj");
    var table = $('#tablewsobj').DataTable();
    table.search(SearchText).draw();
    $('#tablewsobj_length select').prop('id', 'pagesize');
    $('#tablewsobj_length select').prop('class', 'UseUniformCss');
    CallUnformCss('[id$=pagesize]');
    $('#tablewsobj_filter').css("padding-bottom", "10px");
    $('#tablewsobj tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var rowData = table.row(this).data();
        if (rowData != undefined) {
            var val = $('[id$=hideditdone]').val();
            if (val == '1') {
                SubModifiction(true);
                viewConnectiontest(rowData[0], $(this).index());
            }
            else {
                ViewWSDisplay(rowData[0]);
                selIndex = $(this).index();
                setclass = '';
            }
        }
        else {
            $('#WsCmdDetailsDiv').hide();
            $('#EditDbCommandDiv').show();
            alert('No Web Objectd Selected');
        }
    });
}

function RefreshDbCommand() {
    var vtest = $('[id$=hidwsdataset]').val();
    if (vtest == "" || vtest == 'undefined' || vtest == '') {
    }
    else {
        selectwsobjdataset = jQuery.parseJSON($('[id$=hidwsselected]').val());
        objwsdataset = jQuery.parseJSON($('[id$=hidwsdataset]').val());
    }
}

function OnloadWSObjectData() {
    RefreshDbCommand();
    BindDatabaseObjectdata();
    CallUniformCssForWsCmd();
}

//  VIEW  WS DISPLAY
function ViewWSDisplay(id) {
    for (var i = 0; i < objwsdataset.length; i++) {
        if (id == objwsdataset[i].WS_COMMAND_NAME) {
            $('#EditDbCommandDiv').hide();
            $('#WsCmdDetailsDiv').show();
            discardobject = objwsdataset[i].WS_COMMAND_NAME;
            $('[id$=lblWsCmd_Name]').text(objwsdataset[i].WS_COMMAND_NAME);
            $('[id$=lblcreatedby]').text(objwsdataset[i].CreatedBY);
            $('[id$=lblupdatedby]').text(objwsdataset[i].FULL_NAME + ' On  ' + objwsdataset[i].DateTime);
            $('[id$=hidwebdatasource]').val(objwsdataset[i].CONNECTION_NAME);
            $('[id$=hidwscondtls]').val(objwsdataset[i].WS_COMMAND_NAME);
            $('[id$=hidwsconname]').val(objwsdataset[i].WS_COMMAND_NAME);
            $('[id$=hidparameter]').val(objwsdataset[i].PARAMETER);
            $('[id$=hidservicetype]').val(objwsdataset[i].WEBSERVICE_TYPE);
            $('[id$=hidwsconnectorid]').val(objwsdataset[i].WS_CONNECTOR_ID);
            vwebservictype = $.trim(objwsdataset[i].WEBSERVICE_TYPE);
            $('[id$=lblWsCmd_Inputpara]').text(objwsdataset[i].RPCIN);
            if (vwebservictype == 'rpc') $('[id$=hidparameter]').val(objwsdataset[i].RPCIN);
            $('[id$=lblWsCmd_Outputpara]').text(objwsdataset[i].RPCOUT);
            $('[id$=lblWsCmd_ServiceType]').text(vwebservictype);
            $('[id$=lblWsCmd_WeSrc]').text(objwsdataset[i].SERVICE);
            $('[id$=lblWsCmd_PortType]').text(objwsdataset[i].PORT_TYPE);
            $('[id$=lblWsCmd_Method]').text(objwsdataset[i].METHOD);
            $('[id$=hdfWsCommand_CommandId]').val(objwsdataset[i].WS_COMMAND_ID);
            $('[id$=lblWsCmdDesc]').text(objwsdataset[i].DESCRIPTION);
            $('[id$=lblWsCmd_DataPara]').text(objwsdataset[i].HTTP_REQUEST);
            var blkstr = [];
            if (vwebservictype == 'http') {
                ViewHttp();
                $('[id$=lblWsCmd_InPara]').text(objwsdataset[i].URL);
                $('[id$=lblWsCmd_DataSetPath]').text(objwsdataset[i].DATASET_PATH);
                var V = objwsdataset[i].OPERATION_ACTION;
                $('[id$=lblWsCmd_InHost]').text(objwsdataset[i].WEBSERVICE_URL);

                var vtag = objwsdataset[i].TAG;
                if (vtag === "" || vtag === "undefined" || vtag === null) {
                    $('[id$=lblWsCmd_OutPara]').text('');
                }
                else {
                    var obj = jQuery.parseJSON(vtag);
                    jQuery.each(obj, function (i, val) {
                        blkstr.push([val.name]);
                    });
                    $('[id$=lblWsCmd_OutPara]').text(blkstr.join(", "));
                }
            }
            else if (vwebservictype == 'rpc') {
                ViewRpc();
                var vtag = objwsdataset[i].TAG;
                var obj = jQuery.parseJSON(vtag);
                if (obj != null) {
                    jQuery.each(obj.param, function (i, val) {
                        blkstr.push(val.name);
                    });
                }
            }
            else ViewWSDL();
            var usage = jQuery.parseJSON(objwsdataset[i].Used);
            $('#DbCmdAppDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblApps"></table>');
            if (usage.length > 0) {
                $('#tblApps').dataTable({
                    "data": usage,
                    "columns": [{ "title": "App" }, { "title": "View(s)" }, { "title": "Form(s)"}],
                    "sort": false,
                    "searching": false,
                    "paging": false,
                    "info": false
                });
            }
            else {
                var node = document.createElement("DIV");
                node.style.paddingLeft = "10px";
                var textnode = document.createTextNode("This object is not used any where.");
                node.appendChild(textnode);
                document.getElementById("DbCmdAppDiv").appendChild(node); //.html("This object is not used any where.");
            }
            break;
        }
    }
}

function intializewsobject() {
    $('[id$=lblWsCmd_Name]').text('');
    $('[id$=lblcreatedby]').text('');
    $('[id$=lblupdatedby]').text('');
    $('[id$=hidwebdatasource]').val('');
    $('[id$=hidwscondtls]').val('');
    $('[id$=hidwsconname]').val('');
    $('[id$=hdfWsCmdPara]').val('');
    $('[id$=lblWsCmd_Inputpara]').text('');
    $('[id$=lblWsCmd_Outputpara]').text('');
    $('[id$=lblWsCmd_ServiceType]').text('');
    $('[id$=lblWsCmd_WeSrc]').text('');
    $('[id$=lblWsCmd_PortType]').text('');
    $('[id$=lblWsCmd_Method]').text('');
    $('[id$=hdfWsCommand_CommandId]').val('');
    $('[id$=lblWsCmdDesc]').text('');
    $('[id$=lblWsCmd_DataPara]').text('');
    $('[id$=lblWsCmd_InPara]').text('');
    $('[id$=lblWsCmd_DataSetPath]').text('');
    $('[id$=lblWsCmd_InHost]').text('');
    $('[id$=lblWsCmd_OutPara]').text('');
    $('[id$=txtwscopyobject]').val('');
}

//Function Http
function ViewHttp() {
    $('#wsservcicenm').hide();
    $('#mport').hide();
    $('#hmethod').hide();
    $('#viewRpcparameter').hide();
    $('#viewRpcparameterout').hide();
    $('#viewRpcparameterout2').hide();
    $('#wshttpcmddtlddiv').show();
    $('#wshttpcmddtlddiv2').show();
    $('#wshttpcmddtlddiv3').show();
    $('#wshttpcmddt1div4').show();
    $('#wsservcicenm').hide();
    $('#hmethod').hide();
    $('#WsdlOutParamDiv').hide();
    RemoveStyle();
}

//function RPC
function ViewRpc() {
    $('#viewRpcparameter').show();
    $('#viewRpcparameterout').show();
    $('#wshttpcmddtlddiv').hide();
    $('#wshttpcmddtlddiv2').hide();
    $('#wshttpcmddtlddiv3').hide();
    $('#wshttpcmddt1div4').hide();
    $('#viewRpcparameterout2').show();
    $('#wsservcicenm').hide();
    $('#hmethod').hide();
    $('#WsdlOutParamDiv').hide();
    AddStyle();
}

function ViewWSDL() {
    $('#viewRpcparameter').hide();
    $('#viewRpcparameterout').hide();
    $('#wshttpcmddtlddiv').hide();
    $('#wshttpcmddtlddiv2').hide();
    $('#wshttpcmddtlddiv3').hide();
    $('#wshttpcmddt1div4').hide();
    $('#wsservcicenm').show();
    $('#hmethod').show();
    $('#mport').hide();
    $('#WsHttpCmdDetailsDiv').hide();
    $('#WsdlOutParamDiv').show();
    AddStyle();
}
// Function
function AddWsObject() {
    $('#EditDbCommandDiv').show();
    $('#WsCmdDetailsDiv').hide();
    $('#EditDbCommandDivtest').show();
}

function imgWsCmdAppClick(e) {
    if (e.alt.trim() == 'Expand') {
        $(e).attr('src', '//enterprise.mficient.com/images/collapse.png');
        $(e).attr('alt', 'Collapse');
        $(e).attr('title', 'Collapse');
        $('#WsCmdAppDiv').show();
    }
    else {
        $(e).attr('src', '//enterprise.mficient.com/images/expand.png');
        $(e).attr('alt', 'Expand');
        $(e).attr('title', 'Expand');
        $('#WsCmdAppDiv').hide();
    }
    $('[id$=hdfIsMplugIn]').val('1');
}

function HideWsCmdAddNameDiv(_Type) {
    if (_Type == '0') {
        $('#HttptxtCmdDiv').hide();
        $('#RpcTxtCmdDiv').hide();
        $('#WsdllblCmdDiv').show();
        $('[id$=ddl_WsConnactor]').attr('disabled', 'disabled');
    }
    else if (_Type == '1') {
        $('#WsdltxtCmdDiv').show();
        $('#HttptxtCmdDiv').show();
        $('#RpcTxtCmdDiv').show();
        $('#WsdllblCmdDiv').hide();
        $('[id$=ddl_WsConnactor]').removeAttr('disabled');
        if ($($('[id$=ddl_WsConnactor]')[0].parentElement).length > 0) {
            $($('[id$=ddl_WsConnactor]')[0].parentElement).removeClass('disabled');
            $('[id$=ddl_WsConnactor]').removeClass('disabled');
        }
    }
}

//Web service command edit link click event
function WsCommandEditClick() {
    $('[id$=EditWsCmdDetailsDiv]').show();
    $('#btnWsCmdSaveDiv').show();
    $('#WsCmdDetailsDiv').hide();
    if ($('[id$=hdfEditWsType]').val() == "0") {
        AddWsCommandDiv(false); AddWsCommandDiv(true, 520);
    }
    if ($('[id$=hdfEditWsType]').val() == "1") {
        AddWsCommandDiv(false); AddWsCommandDiv(true, 520);
    }
    if ($('[id$=hdfEditWsType]').val() == "2") {
        AddWsCommandDiv(false); AddWsCommandDiv(true, 575);
    }
}

//Show webservice/odata command intelligence box
function SubWsIntls(_bol, _Id) {
    if (_bol) {
        $("#dialogInnerDiv").html('');
        $("#SubWsIntls").dialog({
            closeOnEscape: false,
            resizable: false,
            autoOpen: true,
            minWidth: 150,
            width: $(_Id).width(),
            draggable: false,
            zIndex: 100000
        });
        $($('#SubWsIntls')[0].previousElementSibling).hide();
        $('#' + $('#SubWsIntls').parent().find('div.ui-dialog-titlebar')[0].children[0].id).remove();
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').find('a.ui-state-focus').removeClass('ui-state-focus');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-widget-header');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-corner-all');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-helper-clearfix');
        $('#SubWsIntls').parent().find('div.ui-dialog-content').removeClass('ui-widget-content');
        $('#SubWsIntls').parent().find('div.ui-dialog-content').removeClass('ui-dialog-content');
        $($("#SubWsIntls")[0].parentElement).css('z-index', '103');
        var TopPos = $($(_Id)[0].parentElement).offset().top + 25;
        var LeftPos = $($(_Id)[0].parentElement).offset().left;
        try { TopPos = TopPos - ($(window).scrollTop() != undefined ? $(window).scrollTop() : 0); }
        catch (err) { TopPos = $($(_Id)[0].parentElement).offset().top + 25; }
        try { LeftPos = LeftPos - ($(window).scrollLeft() != undefined ? $(window).scrollLeft() : 0); }
        catch (err) { LeftPos = $($(_Id)[0].parentElement).offset().left; }
        $("#SubWsIntls").dialog({ position: [LeftPos, TopPos] });
        $($("#SubWsIntls")[0].parentElement).css('top', TopPos);
        $($("#SubWsIntls")[0].parentElement).css('left', LeftPos);
        $("#SubWsIntls").focus();
        $($('#SubWsIntls')[0].previousElementSibling).hide();
    }
    else {
        if ($("#SubWsIntls").is(':visible')) {
            $("#SubWsIntls").dialog("close");
        }
    }
}

//Change Dropdown style
function CallUniformCssForWsCmd() {
    CallUnformCss('[id$=ddl_WsConnactor]');
    CallUnformCss('[id$=ddl_WsConnectorMethod]');
    CallUnformCss("[id$=ddlWsConnServices]");
    CallUnformCss('[id$=ddlWsCommad_DataReturned]');
    CallUnformCss('[id$=ddlWsdlCommad_DataReturned]');
    CallUnformCss('[id$=ddlWsConnServices]');
    CallUnformCss('[id$=ddlRpc_ParaType]');
    CallUnformCss('[id$=dd1DataCatch]');
    CallUnformCss('[id$=dd1AbMonth]');
    CallUnformCss('[id$=Catcmin]');
    CallUnformCss('[id$=AbEvDayDiv]');
    CallUnformCss('[id$=ddlAbMM]');
    CallUnformCss('[id$=dd1Abhh]');
    CallUnformCss('[id$=Select3]');
    CallUnformCss('[id$=ddlAbDay]');
    CallUnformCss('[id$=dd1AbMnDay]');
    CallUnformCss('[id$=dd1AbMnthDay]');
    CallUnformCss('[id$=ddlAbMnth]');
    CallUnformCss('[id$=dd1AbHr]');
    CallUnformCss('[id$=ddlRlt]');
    CallUnformCss('[id$=SelectddlAbHH]');
    CallUnformCss('[id$=drprltmonth]');
    CallUnformCss('[id$=raltweeks]');
    CallUnformCss('[id$=rltdayd]');
    CallUnformCss('[id$=Drprlthour]');
    CallUnformCss('[id$=drprltmin]');
    CallUnformCss('[id$=ddlContantType]');
}
function ClearCatch() {
    $("#hidSelectcatch").val("");
}

function EditWebserviceObject() {
    $('#EditDbCommandDiv').show();
    $('#viewinfo').hide();
    $('#DiveditDBobject').show();
    $('#adddivheader').hide();
    $('[id$=txtWsdl_CmdName]').attr('disabled', 'disabled');
    var Commandid = $('[id$=hdfWsCommand_CommandId]').val();
    UpdateUpdBtnRowIndex();
}

//Hide ws command div relate to ws type.
function HideWsCmdEditDiv(_Type) {
    if (_Type == '0') {
        $('#WsdlCmdDiv').show();
        $('#SimpleHttpCmdPropDiv').hide();
        $('#RpcCmdPropDiv').hide();
    }
    else if (_Type == '1') {
        $('#WsdlCmdDiv').hide();
        $('#SimpleHttpCmdPropDiv').show();
        $('#RpcCmdPropDiv').hide();
        //      hide DatasetPath
        $('#DivWsCmd_DsPathCtrl').hide();
        $('#DivWsCmd_DsPathHead').hide();

        $('#DivWsdlCmd_DsPathCtrl').hide();
        $('#DivWsdlCmd_DsPathHead').hide();
    }
    else if (_Type == '2') {
        $('#WsdlCmdDiv').hide();
        $('#Div2').show();
        $('#SimpleHttpCmdPropDiv').hide();
        $('#RpcCmdPropDiv').show();
        $('#RpcMethodDiv').show();
        $('#rpcout').show();

    }
    else if (_Type == '3') {
        $('#WsdlCmdDiv').hide();
        $('#SimpleHttpCmdPropDiv').hide();
        $('#RpcCmdPropDiv').hide();
    }
}

//Check is DivWsdlCmd_DsPathCtrl is visible 
function wsPuralPathVisible() {
    AppActiveCtrl['IsDsPath'] = 0;
    AppActiveCtrl['DsPathValue'] = '';
    AppActiveCtrl['DsPathItem'] = '';
    if ($('#DivWsdlCmd_DsPathCtrl').is(':visible')) {

        if (!wsIsDsPathValid($('[id$=txtWsdlCommand_DatasetPath]').val().trim())) {
            return false;
        }
    }
    return true;
}

//Hide divs on connector change
function HideDivWsConnChnage(_Type) {
    if (_Type == '0') {
        $('#ddlWsConnServicesDiv').hide();
        $('#ddlWsConnMethodDiv').hide();
    }
    else if (_Type == '1') {
        $('#ddlWsConnServicesDiv').show();
        $('#ddlWsConnMethodDiv').hide();

    }
    else if (_Type == '2') {
        $('#ddlWsConnServicesDiv').show();
    }
    else if (_Type == '3') {
        $('#ddlWsConnServicesDiv').show();
        $('#ddlWsConnMethodDiv').show();
    }
}

//ddl_WsConnectorMethod change event
function WsdlMethodChange(e) {
    WsParaHash['0'] = ['[]'];
    $('[id$=hdfWsCmdPara]').val('');
    WsdlOutParamHideShow();
    wsdlMethodValue = "";
    wsdlMethodValue = $(e).val();
    WsdlEditCmdParam();
    $('[id$=ddlWsdlCommad_DataReturned]').val('0');
    $('[id$=ddlWsdlCommad_DataReturned]').change();
}

//Hide/show command out parameter div
function WsdlOutParamHideShow() {
    if ($('[id$=ddl_WsConnectorMethod]').val() == '-1') {
        $('#WsdlOutParamDiv').hide();
    }
    else {
        $('#WsdlOutParamDiv').show();
    }
}

//Collect wsdl command parameter in collection for intelligence
function WsdlEditCmdParam() {
    wsdlCmpxHash = {};
    wsdlMainHash = {};
    wsdlSimpleHash = {};
    wsdlMainHash[0] = ''; ;
    AppActiveCtrl['IsContainsArray'] = '0';
    AppActiveCtrl['DataSetPath'] = '';
    var strHtml = '';
    var strVCount = 0;
    var FirstCount = 0;
    if (wsdlOutParamHash[wsdlMethodValue] != null) {
        if (wsdlOutParamHash[wsdlMethodValue][0].trim().length > 0) {
            var JsonTest = jQuery.parseJSON(wsdlOutParamHash[wsdlMethodValue][0]);
            if (JsonTest.C_Name != undefined) AppActiveCtrl['DataSetPath'] = JsonTest.C_Name;
            $.each(jQuery.parseJSON(wsdlOutParamHash[wsdlMethodValue][0]), function () {
                if (this.length > 0) {
                    var sName = this[0]['S_Name'];
                    var cName = this[0]['C_Name'];
                    var cType = this[0]['C_Types'];
                    var sType = this[0]['S_Types'];

                    if (cType != undefined) {
                        if (cType.length > 0) {
                            GetWsdlCmplxType(cType, cName, false, FirstCount);
                        }
                    }
                    if (sName != undefined) {
                        if (sName.length > 0) {
                            wsdlSimpleHash[strVCount] = sName;
                            strVCount = strVCount + 1;
                        }
                    }
                    if (sType != undefined) {
                        if (sType.length > 0) {
                            $.each(sType, function () {
                                wsdlCmpxHash[cName] = (wsdlCmpxHash[cName] != undefined ? (wsdlCmpxHash[cName] + '.' + this['S_Name']) : this['S_Name']);
                            });
                        }
                    }
                }
            });
        }
    }
    $('[id$=hdfWsdlOutParamPfix]').val(AppActiveCtrl['DataSetPath']);
}

function addNewWsPara() {
    var IsValid = true;
    $('#WsParaContent').find("*").each(function () {
        if (this.id.split('_')[0] == 'WsParatext') {
            if (this.value.trim().length == 0) {
                $('#aMessage').html('Please enter parameter name.');
                SubProcBoxMessage(false);
                IsValid = false;
            }
        }
    });
    if (!IsValid) return;
    WsParaCount = WsParaCount + 1;
    $('#WsParaContent').append('<div id="WsParaContentText_' + WsParaCount + '" class="DpContentRow"><div class="DpContentText"> <input id="WsParatext_' + WsParaCount + '" type="text" class="txtDpOption" /></div><div class="DpContentValue"> <input id="WsParaPath_' + WsParaCount + '" type="text"  class="txtDpOption"/></div><div id="WsParaDel_' + WsParaCount + '" class="DpOptionContentDelete"><img id="imgWsParaDelete_' + WsParaCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" title="Delete Property"/></div> </div><div style="clear: both;"></div>');
    $('#imgWsParaDelete_' + WsParaCount).bind('click', function () {
        $('#' + this.parentNode.parentNode.id).remove();
        wsAppendAddBtn();
    });
    $('#WsParaPath_' + WsParaCount).bind('click', function (event) {
        AppActiveCtrl['Id'] = this.id;
    });
    $('#WsParaPath_' + WsParaCount).bind('keyup', function (event) {
        if (AppActiveCtrl['CmdType'] != '1') wsdlPathChangeEvent(this, event, false);
    });
    $('#WsParaPath_' + WsParaCount).bind('keydown', function (event) {
        if (AppActiveCtrl['CmdType'] != '1') wsdlPathChangeEvent(this, event, true);
    });
    wsAppendAddBtn();
}

//Append ws command add button in last div
function wsAppendAddBtn() {
    var strLastDiv = "";
    var strHtml = '<img id="imgWsParamAdd" alt="add" src="//enterprise.mficient.com/images/add.png" style="margin-left:5px;" class="CursorPointer" title="Add Property"/>';
    $('#imgWsParamAdd').remove();
    $('#WsParaContent').find('.DpOptionContentDelete').each(function () {
        strLastDiv = this;
    });
    if (strLastDiv.length != 0) {
        $(strLastDiv).append(strHtml);
        $('#imgWsParamAdd').bind('click', function () {
            addNewWsPara();
        });
    }
}

function SubProcWsCmdOutputPara(_bol) {

    if (_bol) showModalPopUp('SubProcWsCmdOutputPara', 'Output Properties', 520);

    else $('#SubProcWsCmdOutputPara').dialog('close');

    if (_bol) {
        if ($($("#SubProcWsCmdOutputPara")[0].parentElement).length > 0) {
            $($("#SubProcWsCmdOutputPara")[0].parentElement).bind('click', function () {
                SubWsIntls(false);

            });
        }
        try {
            $('#SubProcWsCmdOutputPara').dialog('option', 'close', function () {
                SubWsIntls(false);
            });
            $('#SubProcWsCmdOutputPara').bind('dialogdragstart', function () {
                SubWsIntls(false);
            });
        }
        catch (err) {
        }
    }
}

//Used to show values in intelligence according to control value
function wsdlPathChangeEvent(e, event, IsKeyDown) {
    if (IsKeyDown) {
        if (event.which == '13') {
            if ($("#SubWsIntls").is(':visible')) {
                event.preventDefault();
                $("#SubWsIntls").find('.INTS-SELECTED').each(function () {
                    AddSelectedInslsVal(this.id);
                    var test = "";
                });
            }
        }
        if (event.which == '40' || event.which == '38') {
        }
        else {
            SubWsIntls(false, e);
        }
        $('#' + AppActiveCtrl['Id']).focus();
    }
    else {
        if (AppActiveCtrl['Id'] != e.id) AppActiveCtrl['LastKeyDown'] = 0;
        AppActiveCtrl['Id'] = e.id;
        AppActiveCtrl['Value'] = $(e).val();
        if ((event.which == '40' || event.which == '38') && (AppActiveCtrl['LastKeyDown'] == event.which)) {
            if ($('#SubWsInnrIntls').html().trim().length > 0) SubWsIntls(true, e);
        }
        else {
            if ($('#SubProcWsCmdOutputPara').is(':visible') && AppActiveCtrl['IsDsPath'] == 1) {
                wsdlPropPathIntls(e.id, $(e).val());
            }
            else {
                wsdlParamIntlsn(e.id, $(e).val());
            }

            if ($('#SubWsInnrIntls').html().trim().length > 0) SubWsIntls(true, e);
        }
        $(e).focus();
        if (AppActiveCtrl['LastDivCount'] == undefined || AppActiveCtrl['LastDivCount'].length == 0) AppActiveCtrl['LastDivCount'] == 1;
        if (event.which == '40') {
            if ($("#SubWsIntls").is(':visible')) {
                var test = "";
                if (AppActiveCtrl['LastKeyDown'] != '40') {
                    if ($(AppActiveCtrl['SelectedIntls']).length > 0) {
                        changeIntlsColor('#WfViewCtrlDiv_' + (parseInt(AppActiveCtrl['SelectedIntls'].split('_')[1]) + 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = 2;
                    }
                }
                else {
                    changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] + 1));
                    AppActiveCtrl['LastKeyDown'] = event.which;
                    if (AppActiveCtrl['LastDivCount'] >= (AppActiveCtrl['MaxDivCount'] - 1)) {
                        changeIntlsColor('#WfViewCtrlDiv_1');
                        AppActiveCtrl['LastDivCount'] = 1;
                    }
                    else AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] + 1);
                }
            }
        }
        else if (event.which == '38') {
            if ($("#SubWsIntls").is(':visible')) {
                var test = "";
                if (AppActiveCtrl['LastKeyDown'] != '38') {
                    if ((AppActiveCtrl['LastDivCount'] - 1) < 1) {
                        changeIntlsColor('#WfViewCtrlDiv_1');
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = 1;
                    }
                    else {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] - 1);
                    }
                }
                else {
                    if ((AppActiveCtrl['LastDivCount'] - 1) < 1) {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['MaxDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['MaxDivCount'] - 1);
                    }
                    else {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] - 1);
                    }
                }
            }
        }
        else {
            AppActiveCtrl['LastKeyDown'] = event.which;
            AppActiveCtrl['LastDivCount'] = 1;
        }
    }
}

//This sub is used to add output parameter
function AddRpcOutPara() {
    rpcParaType = 'Out';
    RpcControlInt();
    $('#hdfRpcParamLevel').val('0');
    $('#hdfIsInnerParam').val('0');
    var IsParamExist = false;
    $.each($('#rpcOutParaContDiv1')[0].children, function (e) {
        if (this.className == "rpcContText") {
            IsParamExist = true;
        }
    });
    if (!IsParamExist) {
        $('[id$=txtRpc_ParaName]').val('root');
        $('[id$=txtRpc_ParaName]').attr('disabled', 'disabled');
    }
    SubProcAddRpcPara(true, 'Add Xml Rpc Output parameters');
}

//Add rpc command parameter popup
function SubProcAddRpcPara(_bol, _title) {
    if (_bol) {
        showModalPopUp('SubProcAddRpcPara', _title, 500);
    }
    else {
        $('#SubProcAddRpcPara').dialog('close');
    }
}

//Get wsdl complax type parameters from output param json
function GetWsdlCmplxType(_cType, _cPName, _IsArray, _Count) {
    var sName = _cType[0]['S_Name'];
    var sType = _cType[0]['S_Types'];
    var cName = _cType[0]['C_Name'];
    var cType = _cType[0]['C_Types'];
    var isArray = _cType[0]['Is_Array'];
    if (isArray == '1' && _Count == 0) AppActiveCtrl['IsContainsArray'] = '1';
    if (_Count != 0) {
        wsdlMainHash[0] = ((wsdlMainHash[0]).length > 0 ? (wsdlMainHash[0]) + '.' + _cPName + (_IsArray == '1' ? '[]' : '') : (_cPName + (_IsArray == '1' ? '[]' : '')));
    }
    else {
        AppActiveCtrl['DataSetPath'] = (AppActiveCtrl['DataSetPath'].length > 0 ? AppActiveCtrl['DataSetPath'] + '.' + _cPName : AppActiveCtrl['DataSetPath']);
    }
    _Count = _Count + 1;
    if (cType != undefined && cType.length != 0) {
        if (cType.length > 0) {
            wsdlCmpxHash[_cPName] = (wsdlCmpxHash[_cPName] != undefined ? (wsdlCmpxHash[_cPName] + '.' + cName + (isArray == '1' ? '[]' : '')) : cName + (isArray == '1' ? '[]' : ''))
            GetWsdlCmplxType(cType, cName, isArray, _Count);
        }
    }
    else if (sName != undefined) {
        if (sName.length > 0) {
            wsdlMainHash[0] = ((wsdlMainHash[0]).length > 0 ? (wsdlMainHash[0]) + '.' + sName : sName);
            wsdlCmpxHash[_cPName] = (wsdlCmpxHash[_cPName] != undefined ? (wsdlCmpxHash[_cPName] + '.' + sName) : sName);
        }
    }
    else if (cType.length == 0 && sType != undefined && isArray == '1') {
        if (cName.length > 0) {
            wsdlMainHash[0] = ((wsdlMainHash[0]).length > 0 ? (wsdlMainHash[0]) + '.' + cName + '[]' : cName + '[]');
            wsdlCmpxHash[_cPName] = (wsdlCmpxHash[_cPName] != undefined ? (wsdlCmpxHash[_cPName] + '.' + cName + '[]') : cName + '[]');
            GetSimpleTypeArrayItem(sType);
        }
    }
    if (sType != undefined) {
        if (sType.length > 0) {
            //When method return complex type
            if (_cPName != cName && wsdlCmpxHash[_cPName] == undefined) {
                wsdlMainHash[0] = ((wsdlMainHash[0]).length > 0 ? (wsdlMainHash[0]) + '.' + cName : cName);
                wsdlCmpxHash[_cPName] = cName;
            }
            $.each(sType, function () {
                //This Simple Type is related to inner complex type
                wsdlCmpxHash[cName] = (wsdlCmpxHash[cName] != undefined ? (wsdlCmpxHash[cName] + '.' + this['S_Name']) : this['S_Name']);
            });
            return;
        }
    }
}

//This sub is used to add input parameter
function AddRpcInPara() {
    rpcParaType = 'In';
    RpcControlInt();
    $('#hdfRpcParamLevel').val('0');
    $('#hdfIsInnerParam').val('0');
    SubProcAddRpcPara(true, 'Add Xml Rpc Input parameters');
}

//This sub is used to add output parameter
function AddRpcOutPara() {
    rpcParaType = 'Out';
    RpcControlInt();
    $('#hdfRpcParamLevel').val('0');
    $('#hdfIsInnerParam').val('0');
    var IsParamExist = false;
    $.each($('#rpcOutParaContDiv1')[0].children, function (e) {
        if (this.className == "rpcContText") {
            IsParamExist = true;
        }
    });
    if (!IsParamExist) {
        $('[id$=txtRpc_ParaName]').val('root');
        $('[id$=txtRpc_ParaName]').attr('disabled', 'disabled');
    }
    SubProcAddRpcPara(true, 'Add Xml Rpc Output parameters');
}

//This sub is to intialize control in add parameter screen.
function RpcControlInt() {
    $('#ddlRpc_ParaType').val('-1');
    $('[id$=txtRpc_ParaName]').val('');
    $('#ArrayDataTypeDiv').hide();
    $('#AddInArrayDataTypeDiv').hide();
    $('#AddInStructDataTypeDiv').show();
    $('[id$=txtRpc_ParaName]').removeAttr('disabled');
    if ($('#uniform-ddlRpc_ParaType ').length != 0) if ($('#uniform-ddlRpc_ParaType ')[0].children[0].tagName == 'SPAN') $($('#uniform-ddlRpc_ParaType ')[0].children[0]).html('Select Parameter Type');
}

//Add new webservice command popup
function AddWsCommandDiv(_bol, _width) {
    if (_bol) {
        txtDspKeyEvent();
        showModalPopUp('AddWsCommandDiv', "Web service Object", _width);
    }
    else {
        $('#AddWsCommandDiv').dialog('close');
    }
}

function rpcParaCollapse(e) {
    if (e.alt.trim() == 'Expand') {
        $('#' + e.id).attr('src', '//enterprise.mficient.com/images/collapse.png');
        $('#' + e.id).attr('title', 'Collapse');
        $('#' + e.id).attr('alt', 'Collapse');
        if (e.id == "imgRpcInPara") {
            $('#DivRpcInPara').show();
        }
        else {
            $('#DivRpcOutPara').show();
        }
    }
    else {
        $('#' + e.id).attr('src', '//enterprise.mficient.com/images/expand.png');
        $('#' + e.id).attr('title', 'Expand');
        $('#' + e.id).attr('alt', 'Expand');
        if (e.id == "imgRpcInPara") {
            $('#DivRpcInPara').hide();
        }
        else {
            $('#DivRpcOutPara').hide();
        }
    }
    AddWsCommandDiv(false); AddWsCommandDiv(true, 575);
}

//txtWsdlCommand_DatasetPath events
function txtDspKeyEvent() {
    var _Id = '[id$=txtWsdlCommand_DatasetPath]';
    $(_Id).unbind('click');
    $(_Id).unbind('keyup');
    $(_Id).unbind('keydown');
    $(_Id).bind('click', function (event) {
        AppActiveCtrl['Id'] = this.id;
    });
    $(_Id).bind('keyup', function (event) {
        wsdlPathChangeEvent(this, event, false);
    });
    $(_Id).bind('keydown', function (event) {
        wsdlPathChangeEvent(this, event, true);
    });
}

//Delete confirmation of rpc parameter
function DeleteRpcParamYes() {
    if ($('#hdfParamDelIds').val().trim().length == 0) {
        return;
    }
    var Type = $('#hdfParamDelIds').val().split('_')[1];
    var Name = $('#hdfParamDelIds').val().split('_')[2];
    $('#' + Type + 'Para_' + Name).remove();
    $('#aLevel' + Type + 'Para_' + Name).remove();
    $('#aPre' + Type + 'Para_' + Name).remove();
    $('#Sep' + Type + 'Para_' + Name).remove();
    $('#SepType' + Type + 'PType_' + Name).remove();
    $('#' + Type + 'PType_' + Name).remove();
    SubProcDeleteRpcPram(false);
}

//This sub is used to add parameter.
function BtnRpcAddParaClick() {
    AddWsCommandDiv(false);
    AddWsCommandDiv(true, 575);
    SubProcAddRpcPara(false);
    SubProcAddRpcPara(true, 'Add Xml Rpc Input parameters');
    AddRpcPara(parseInt($('#hdfRpcParamLevel').val(), 10));
}
//This sub is used to check in parameter name is exists or not.
function IsOutputParamExist(_Name) {
    var isExist = false;
    $.each($('#rpcOutParaContDiv1')[0].children, function () {
        if (this.className == 'rpcContText') {
            if ($('#aLevelOutPara_' + this.innerHTML.trim())[0].innerHTML.trim() == 0) {
                isExist = true;
            }
            if (this.innerHTML.trim() == _Name) {
                isExist = true;
            }
        }
    });
    return isExist;
}

//Add rpc command parameter
function AddRpcPara(_Level) {
    if (IsStringContainsSpecialChar($('[id$=txtRpc_ParaName]').val()) || $('[id$=txtRpc_ParaName]').val().trim() == "") {
        $('#aMessage').html('Please enter valid parameter name.');
        //   SubProcBoxMessage(true);
        return;
    }
    if (rpcParaType == 'Out') {
        if ($('#hdfIsInnerParam').val() == '0') {
            if (IsOutputParamExist($('[id$=txtRpc_ParaName]').val())) {
                $('#aMessage').html('Output parameter name already exists.');
                //       SubProcBoxMessage(true);
                return;
            }
        }
    }
    if (IsParaNameExistInRpc($('[id$=txtRpc_ParaName]').val())) {
        $('#aMessage').html('Parameter name already exists.');
        SubProcBoxMessage(true);
        return;
    }
    if ($('#ddlRpc_ParaType').val() == '-1') {
        $('#aMessage').html('Please select valid parameter type.');
        SubProcBoxMessage(true);
        return;
    }
    if ($('[id$=txtRpc_ParaName]').val().length == 0) {
        $('#aMessage').html('Please enter parameter name.');
        SubProcBoxMessage(true);
        return;
    }

    var strLeftHtml = "";
    var strRigthHtml = "";
    var strMarginLeft = "";
    var strHtml = "";
    var addImgHtml = "";
    var intLevel = _Level;
    strMarginLeft = 'style="margin-left:' + (intLevel * 20) + 'px;"';
    _ParaType = rpcParaType;
    var parentLeft = '#rpc' + _ParaType + 'ParaContDiv1';
    var parentRight = '#rpc' + rpcParaType + 'ParaContDiv2';
    var ParentParam = '';
    if (intLevel != 0) {
        ParentParam = $('#hdfRpcParamParent').val().split(',')[2]
    }
    if ($('#ddlRpc_ParaType').val() == "struct" || $('#ddlRpc_ParaType').val() == "array") {
        addImgHtml = '<div class="rpcContAdd"><img id="ImgAdd_' + rpcParaType + '_' + $('[id$=txtRpc_ParaName]').val() + '" src="//enterprise.mficient.com/images/add.png" alt="Add"  onclick="AddRpcParamClick(this);" /></div>';
    }
    
    strLeftHtml = '<div id="' + _ParaType + 'Para_' + $('[id$=txtRpc_ParaName]').val() + '" class="rpcContText" ' + strMarginLeft + '>' +
                                                           $('[id$=txtRpc_ParaName]').val() +
                                                      '</div>' +
                                                      '<a id="aLevel' + _ParaType + 'Para_' + $('[id$=txtRpc_ParaName]').val() + '" class="hide"  type="level">' + intLevel + '</a>' +
                                                      '<a id="aPre' + _ParaType + 'Para_' + $('[id$=txtRpc_ParaName]').val() + '" class="hide">' + ParentParam + '</a>' +
                                                      '<div id="Sep' + _ParaType + 'Para_' + $('[id$=txtRpc_ParaName]').val() + '" class="clear"></div>';

    strRigthHtml = '<div id="' + rpcParaType + 'PType_' + $('[id$=txtRpc_ParaName]').val() + '"  class="rpcContType">' +
                                                                '<div class="FLeft">' + $('#ddlRpc_ParaType').val() + '</div>' +
                                                                '<div class="rpcContDelete"><img id="Img_' + rpcParaType + '_' + $('[id$=txtRpc_ParaName]').val() + '" src="//enterprise.mficient.com/images/cross.png" alt="Delete" onclick="DeleteRpcParamClick(this);" /></div>' + addImgHtml +
                                                            '</div>' +
                                                            '<div id="SepType' + rpcParaType + 'PType_' + $('[id$=txtRpc_ParaName]').val() + '" class="clear"></div>';

    if (intLevel == 0) {
        $(parentLeft).append(strLeftHtml);
        $(parentRight).append(strRigthHtml);
    }
    else {
        parentLeft = '#' + rpcParaType + 'Para_' + $('#hdfRpcParamParent').val().split(',')[0];
        parentRight = '#' + rpcParaType + 'PType_' + $('#hdfRpcParamParent').val().split(',')[0];
        if ($('#hdfRpcParamParent').val().split(',')[1] == '0') {
            $(parentLeft).after(strLeftHtml);
            $(parentRight).after(strRigthHtml);
        }
        else {
            $(parentLeft).before(strLeftHtml);
            $(parentRight).before(strRigthHtml);
        }
    }

    $('#rpc' + _ParaType + 'ParaContDiv1').show();
    $('#rpc' + _ParaType + 'ParaContDiv2').show();
    SubProcAddRpcPara(false);
}

//This sub is used to check in parameter name is exists or not.
function IsParaNameExistInRpc(_Name) {
    var isExist = false;
    $.each($('#rpc' + rpcParaType + 'ParaContDiv1')[0].children, function () {
        if (this.className == "rpcContText") {
            if (this.innerHTML.trim() == _Name) isExist = true;
        }
    });
    return isExist;
}

//Delete rpc command parameter html
function DeleteRpcParamClick(e) {
    if (e.id.length > 0) {
        $('#hdfParamDelIds').val(e.id);
        rpcParaType = e.id.split('_')[1];
        var Type = e.id.split('_')[1];
        var Name = e.id.split('_')[2];
        var Level = parseInt($('#aLevel' + Type + 'Para_' + Name)[0].innerText.trim());
        var strJson = "";

        if ($('#' + Type + 'PType_' + Name)[0].innerText.trim() == 'struct') {
            strJson = GetRpcParaAtLevelAfterStruct((Level + 1), Name, rpcParaType);
        }
        else if ($('#' + Type + 'PType_' + Name)[0].innerText.trim().split('-')[0] == 'array') {
            strJson = GetRpcParaAtLevelAftrArray((Level + 1), Name, rpcParaType, false);
        }
        if (strJson.length == 0) {
            SubProcDeleteRpcPram(true);
        }
        else {
            $('#aMessage').html('Please delete inner parameters.');
            SubProcBoxMessage(true);
        }
    }
}

//This sub is to used to get inner parameter of array type parameter.
function GetRpcParaAtLevelAftrArray(_Level, _ParentPara, _Type, _IsValid) {
    var strJson = '';
    var strType = "";
    var innerJson = "";
    $.each($('#rpc' + _Type + 'ParaContDiv1')[0].children, function (e) {
        if (this.type == "level") {
            if ($('#aLevel' + _Type + 'Para_' + this.id.split('_')[1].trim())[0].innerHTML.trim() == _Level) {
                if (_ParentPara != $('#aPre' + _Type + 'Para_' + this.id.split('_')[1].trim())[0].innerHTML.trim()) return;
                strType = $('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim().split('-')[0];
                if (strType == 'struct') {
                    innerJson = "";
                    innerJson = GetRpcParaAtLevelAftrArray((_Level + 1), this.id.split('_')[1].trim(), _Type, _IsValid);
                    if (strJson.length > 0) strJson += ',';
                    strJson += '{"name":"' + this.id.split('_')[1].trim() + '",';
                    strJson += '"typ":"' + GetRpcParamType(strType) + '",';
                    strJson += '"inparam":[' + innerJson + ']}';
                    if (_IsValid) {
                        if (innerJson.length == 0) {
                            $('#aMessage').html('Please enter child nodes in struct parameter.');
                            SubProcBoxMessage(true);
                        }
                    }
                }
                else if (strType == 'array') {
                    innerJson = "";
                    innerJson = GetRpcParaAtLevelAftrArray((_Level + 1), this.id.split('_')[1].trim(), _Type, _IsValid);
                    if (strJson.length > 0) strJson += ',';
                    strJson += '{"name":"' + this.id.split('_')[1].trim() + '",';
                    strJson += '"typ":"' + GetRpcParamType(strType) + '",';
                    strJson += '"inparam":[' + innerJson + ']}';
                    if (_IsValid) {
                        if (innerJson.length == 0) {
                            $('#aMessage').html('Please enter child nodes in array parameter.');
                            SubProcBoxMessage(true);
                        }
                    }
                }
                else {
                    if (strJson.length > 0) strJson += ',';
                    strJson += '{"name":"' + this.id.split('_')[1].trim() + '",';
                    strJson += '"typ":"' + GetRpcParamType(strType) + '"}';
                }
            }
        }
    });
    return strJson;
}

//Validate webservice command
function WsCommandValidation() {
    var strMessage = "";
    if ($('[id$=ddl_WsConnactor]').length != 0) {
        if ($('[id$=ddl_WsConnactor]')[1].value == "-1")
            strMessage += " * Please select connector.</br>";
    }
    if ($('[id$=hdfEditWsType]').val() == "1") {
        if ($('[id$=txtWsdl_CmdName]').length != 0) {
            var strVal = ValidateNameInIde($('[id$=txtWsdl_CmdName]').val());
            if (strVal != 0) {
                if (strVal == 1) {
                    strMessage += " * Please enter object name.</br>";
                }
                else if (strVal == 2) {
                    strMessage += " * Object name cannot be more than 20 charecters.</br>";
                }
                else if (strVal == 3) {
                    strMessage += " * Object name cannot be less than 3 charecters.</br>";
                }
                else if (strVal == 4) {
                    strMessage += " * Please enter valid object name.</br>";
                }
                else if (strVal == 5) {
                    strMessage += " * Please enter valid object name.</br>";
                }
            }
        }
        try {
            if ($('[id$=ddlWsCommad_DataReturned]')[1].value == "1") {
                if ($('[id$=txtWsCommand_DatasetPath]').val().trim().length == 0)
                    strMessage += " * Please enter dataset path.<br />";
            }
        }
        catch (err) {
        }

    }
    else if ($('[id$=hdfEditWsType]').val() == "0") {
        if ($('[id$=txtWsdl_CmdName]').length != 0) {
            var strVal = ValidateNameInIde($('[id$=txtWsdl_CmdName]').val());
            if (strVal != 0) {
                if (strVal == 1) {
                    strMessage += " * Please enter object name.</br>";
                }
                else if (strVal == 2) {
                    strMessage += " * Object name cannot be more than 20 charecters.</br>";
                }
                else if (strVal == 3) {
                    strMessage += " * Object name cannot be less than 3 charecters.</br>";
                }
                else if (strVal == 4) {
                    strMessage += " * Please enter valid object name.</br>";
                }
                else if (strVal == 5) {
                    strMessage += " * Please enter valid object name.</br>";
                }
            }
        }
        if ($('[id$=ddlWsConnServices]').length != 0) {
            if ($('[id$=ddlWsConnServices]')[1].value == "-1")
                strMessage += " * Please select service.</br>";
        }

        if ($('[id$=ddl_WsConnectorMethod]').length != 0) {
            if ($('[id$=ddl_WsConnectorMethod]')[1].value == "-1")
                strMessage += " * Please select web service method.</br>";
        }
    }
    else if ($('[id$=hdfEditWsType]').val() == "2") {
        if ($('[id$=txtWsdl_CmdName]').length != 0) {
            var strVal = ValidateNameInIde($('[id$=txtWsdl_CmdName]').val());
            if (strVal != 0) {
                if (strVal == 1) {
                    strMessage += " * Please enter object name.</br>";
                }
                else if (strVal == 2) {
                    strMessage += " * Object name cannot be more than 20 charecters.</br>";
                }
                else if (strVal == 3) {
                    strMessage += " * Object name cannot be less than 3 charecters.</br>";
                }
                else if (strVal == 4) {
                    strMessage += " * Please enter valid object name.</br>";
                }
                else if (strVal == 5) {
                    strMessage += " * Please enter valid object name.</br>";
                }
            }
        }
        if ($('[id$=txtRpc_MethodName]').val().trim().length == 0) {
            strMessage += " * Please enter valid method name.</br>";
        }
        CreateRpcParameterjson('In', true);
        CreateRpcParameterjson('Out', true);
    }
    SearchText = $('#tablewsobj_filter label input').val();
    if (strMessage.length != 0) {
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
        return false;
    }
    else  return true;
}



//Create json fro rpc parameters
function CreateRpcParameterjson(_Type, _IsValid) {
    var strJson = "";
    var Level;
    var innerJson = "";
    $.each($('#rpc' + _Type + 'ParaContDiv1')[0].children, function (e) {
        if (this.className == "rpcContText") {
            Level = parseInt($('#aLevel' + _Type + 'Para_' + this.innerHTML.trim())[0].innerHTML.trim());
            if ($('#aLevel' + _Type + 'Para_' + this.innerHTML.trim())[0].innerHTML.trim() != "0") return;
            if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim() != 'struct' && $('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim().split('-')[0] != 'array') {
                if (strJson.length > 0) strJson += ',';
                strJson += '{"name":"' + this.innerHTML.trim() + '",';
                strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '"}';
            }
            else if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim() == 'struct') {
                innerJson = "";
                if (strJson.length > 0) strJson += ',';
                innerJson = GetRpcParaAtLevelAftrArray((Level + 1), this.innerHTML.trim(), _Type, _IsValid);
                strJson += '{"name":"' + this.innerHTML.trim() + '",';
                strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '",';
                strJson += '"inparam":[' + innerJson + ']}';
                if (_IsValid) {
                    if (innerJson.length == 0) {
                        $('#aMessage').html('Please enter child nodes in struct parameter.');
                        SubProcBoxMessage(true);
                    }
                }
            }
            else if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim().split('-')[0] == 'array') {
                innerJson = "";
                if (strJson.length > 0) strJson += ',';
                strJson += '{"name":"' + this.innerHTML.trim() + '",';
                if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim() == 'array') {
                    innerJson = GetRpcParaAtLevelAftrArray((Level + 1), this.innerHTML.trim(), _Type, _IsValid);
                    strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '",';
                    strJson += '"inparam":[' + innerJson + ']}';
                    if (_IsValid) {
                        if (innerJson.length == 0) {
                            $('#aMessage').html('Please enter child nodes in array parameter.');
                            SubProcBoxMessage(true);
                        }
                    }
                }
                else   strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '"}';
            }
        }
    });
    strJson = '{"param":[' + strJson + ']}';
    if (_Type == 'In')    $('[id$=hdfRpcParam]').val(strJson); 
    else  $('[id$=hdfOutRpcParam]').val(strJson); 
    return strJson;
}

//Return type of rpc paramters
function GetRpcParamType(_Type) {
    var CurrType = "";
    CurrType = GetRpcParamBaseType(_Type);
    return CurrType;
}
//Return type of rpc paramters
function GetRpcParamBaseType(_Type) {
    var CurrType = "";
    switch (_Type) {
        case 'i4':
            CurrType = "0";
        case 'base64':
            CurrType = "1";
            break;
        case 'boolean':
            CurrType = "2";
            break;
        case 'date/time':
            CurrType = "3";
            break;
        case 'double':
            CurrType = "4";
            break;
        case 'integer':
            CurrType = "5";
            break;
        case 'string':
            CurrType = "6";
            break;
        case 'struct':
            CurrType = "7";
            break;
        case 'array':
            CurrType = "8";
            break;
    }
    return CurrType;
}

//Create json fro rpc parameters
function CreateRpcParameterjson(_Type, _IsValid) {
    var strJson = "";
    var Level;
    var innerJson = "";
    $.each($('#rpc' + _Type + 'ParaContDiv1')[0].children, function (e) {
        if (this.className == "rpcContText") {
            Level = parseInt($('#aLevel' + _Type + 'Para_' + this.innerHTML.trim())[0].innerHTML.trim());
            if ($('#aLevel' + _Type + 'Para_' + this.innerHTML.trim())[0].innerHTML.trim() != "0") return;
            if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim() != 'struct' && $('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim().split('-')[0] != 'array') {
                if (strJson.length > 0) strJson += ',';
                strJson += '{"name":"' + this.innerHTML.trim() + '",';
                strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '"}';
            }
            else if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim() == 'struct') {
                innerJson = "";
                if (strJson.length > 0) strJson += ',';
                innerJson = GetRpcParaAtLevelAftrArray((Level + 1), this.innerHTML.trim(), _Type, _IsValid);
                strJson += '{"name":"' + this.innerHTML.trim() + '",';
                strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '",';
                strJson += '"inparam":[' + innerJson + ']}';
                if (_IsValid) {
                    if (innerJson.length == 0) {
                        $('#aMessage').html('Please enter child nodes in struct parameter.');
                        SubProcBoxMessage(true);
                    }
                }
            }
            else if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim().split('-')[0] == 'array') {
                innerJson = "";
                if (strJson.length > 0) strJson += ',';
                strJson += '{"name":"' + this.innerHTML.trim() + '",';
                if ($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim() == 'array') {
                    innerJson = GetRpcParaAtLevelAftrArray((Level + 1), this.innerHTML.trim(), _Type, _IsValid);
                    strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '",';
                    strJson += '"inparam":[' + innerJson + ']}';
                    if (_IsValid) {
                        if (innerJson.length == 0) {
                            $('#aMessage').html('Please enter child nodes in array parameter.');
                            SubProcBoxMessage(true);
                        }
                    }
                }
                else {
                    strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.innerHTML.trim())[0].innerText.trim()) + '"}';
                }
            }
        }
    });
    strJson = '{"param":[' + strJson + ']}';
    if (_Type == 'In') {
        $('[id$=hdfRpcParam]').val(strJson);
    }
    else {
        $('[id$=hdfOutRpcParam]').val(strJson);
    }
    return strJson;
}

//Used to add values in itelligence popup
function wsdlParamIntlsn(_Id, _Val) {
    $('#SubWsInnrIntls').html('');
    $("#dialogInnerDiv").html('');
    var strVCount = 1;
    AppActiveCtrl['Length'] = 0;
    if (AppActiveCtrl['Value'].length == 1 || AppActiveCtrl['Value'].indexOf('.') == -1) {
        $.each(wsdlSimpleHash, function () {
            if (this.length != 0) {
                $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + this + '</div>');
                WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                strVCount = strVCount + 1;
            }
        });
    }
    wsdlComplexParamIntls(strVCount);
}

//Used to add  complex parameter in  intelligence popup
function wsdlComplexParamIntls(strVCount) {
    $.each(wsdlMainHash, function () {
        if (AppActiveCtrl['Value'].length == 1 || AppActiveCtrl['Value'].indexOf('.') == -1) {
            if (this.split('.')[0].length != 0 && this.split('.')[0].indexOf('[]') == -1) {
                AppActiveCtrl['Length'] = 0;
                $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + this.split('.')[0] + '</div>');
                WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                strVCount = strVCount + 1;
            }
            else if (this.split('.')[0].indexOf('[]') != -1) {
                AppActiveCtrl['Length'] = 0;
                $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + this.split('.')[1] + '[]</div>');
                WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                strVCount = strVCount + 1;
            }
        }
        else if (AppActiveCtrl['Value'].indexOf('.') != -1) {
            var arr = AppActiveCtrl['Value'].split('.');
            var ln = AppActiveCtrl['Value'].split('.').length - 2;
            AppActiveCtrl['Length'] = (ln + 1);
            if (wsdlCmpxHash[arr[ln]] != undefined) {
                if (wsdlCmpxHash[arr[ln]].indexOf('[]') != -1) {
                    if (wsdlCmpxHash[wsdlCmpxHash[arr[ln]].replace('[]', '')] != undefined) {
                        if (wsdlCmpxHash[wsdlCmpxHash[arr[ln]].replace('[]', '')].length > 0) {
                            $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + (wsdlCmpxHash[wsdlCmpxHash[arr[ln]].replace('[]', '')] + '[]') + '</div>');
                            WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                            strVCount = strVCount + 1;
                        }
                    }
                }
                else {
                    $.each(wsdlCmpxHash[arr[ln].replace('[]', '')].split('.'), function () {
                        if (this.length > 0) {
                            if (this.length != 0) {
                                $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + this + '</div>');
                                WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                                strVCount = strVCount + 1;
                            }
                        }
                    });
                }
            }
            else {
                if (wsdlCmpxHash[arr[ln].replace('[]', '')] != undefined) {
                    $.each(wsdlCmpxHash[arr[ln].replace('[]', '')].split('.'), function () {
                        if (this.length > 0) {
                            $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + this + '</div>');
                            WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                            strVCount = strVCount + 1;
                        }
                    });
                }
            }
        }
    });
    changeIntlsColor('#WfViewCtrlDiv_1');
    AppActiveCtrl['MaxDivCount'] = strVCount;
}

function wsdlPropPathIntls() {
    $('#SubWsInnrIntls').html('');
    var strVCount = 1;
    AppActiveCtrl['Length'] = 0;
    if (AppActiveCtrl['Value'].length == 1 || AppActiveCtrl['Value'].indexOf('.') == -1) {
        var strDsItem = '';
        $.each(wsdlCmpxHash, function () {
            if (this.length > 0) {
                if (this.trim() == AppActiveCtrl['DsPathItem']) {
                    strDsItem = AppActiveCtrl['DsPathItem'];
                }
            }
        });
        if (strDsItem.length == 0) return;
        $('#SubWsInnrIntls').html('<div id="WfViewCtrlDiv_1" class="DbCmdAddPara CursorPointer">' + strDsItem + '</div>');
        WsdlParamMouseEvent('WfViewCtrlDiv_1');
        changeIntlsColor('#WfViewCtrlDiv_1');
        AppActiveCtrl['MaxDivCount'] = 2;
    }
    else {
        if (AppActiveCtrl['Value'].split('.')[0].trim() != AppActiveCtrl['DsPathItem']) return;
        wsdlComplexParamIntls(strVCount);
    }
}

function Cleartext() {
    $('[id$=txtWsdl_CmdName]').val('');
    $('[id$=txtWsdlCmdDesc]').val('');
    $('[id$=txtWsdlCommand_DatasetPath').val('');
    $('[id$=txtWsCommand_Url').val('');
    $('[id$=txtWsCommand_Para').val('');
    $('[id$=txtRpc_MethodName').val('');
    $('[id$=ddl_WsConnactor').val('-1');
    $('[id$=ddlWsConnServices').val('-1');
    $('[id$=ddl_WsConnectorMethod').val('-1');
    $('[id$= ddlWsdlCommad_DataReturned').val('-1');
}

//btnSaveWsCmdParam Click event
function SaveWsPara() {
    var strJson = "";
    var IsValid = true;
    $('#WsParaContent').find("*").each(function () {
        if (this.id.split('_')[0] == 'WsParatext') {
            if (this.value.trim().length == 0) {
                $('#aMessage').html('Please enter parameter name.');
                SubProcBoxMessage(true);
                IsValid = false;
            }
            else {
                if (AppActiveCtrl['CmdType'] != '1' && ($('#WsParaPath_' + this.id.split('_')[1]).val().trim().indexOf('[]') != -1)) {
                    $('#aMessage').html('Output property path is invalid.<br />Property path containing array is not allowed.');
                    SubProcBoxMessage(true);
                    IsValid = false;
                }
            }
            if (strJson.length > 0) strJson += ",";
            strJson += '{"name":' + JSON.stringify(this.value.trim()) + ',"path":' + JSON.stringify(WsdlEditParamPath(false, $('#WsParaPath_' + this.id.split('_')[1]).val().trim())) + '}';
        }
    });
    if (!IsValid) return;
    strJson = '[' + strJson + ']';
    WsParaHash['0'] = [strJson];
    $('[id$=hdfWsCmdPara]').val(strJson);
    SubProcWsCmdOutputPara(false);
}

function AddNewWebObject() {
    $('[id$=viewinfo]').hide();
    $('[id$=Div1]').show();
}

function RowClick() {

    var table = $('#tablewsobj').DataTable();
    $('#tablewsobj tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('#AddNewConnDiv').show();
            $('#WsCmdDetailsDiv').hide();
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            var rowData = table.row(this).data();
            ViewWSDisplay(rowData[0]);
            $('#WsCmdDetailsDiv').show();
            $('#EditDbCommandDiv').hide();
        }
    });
}
//WsCommand dataset path related div hide/show
function SetWsDatasetPathVisible(bol) {
    if (!bol) {
        $('#DivWsCmd_DsPathCtrl').hide();
        $('#DivWsCmd_DsPathHead').hide();

        $('#DivWsdlCmd_DsPathCtrl').hide();
        $('#DivWsdlCmd_DsPathHead').hide();
        $('#WsdlOutParamDiv').hide();
    }
    else {
        $('#DivWsCmd_DsPathCtrl').show();
        $('#DivWsCmd_DsPathHead').show();

        $('#DivWsdlCmd_DsPathCtrl').show();
        $('#DivWsdlCmd_DsPathHead').show();
        $('#WsdlOutParamDiv').show();
    }
}
//Webservice command back click event
function WsCommandBackClick() {
    $('#btnWsCmdSaveDiv').hide();
    $('[id$=EditWsCmdDetailsDiv]').hide();
    $('#WsCmdDetailsDiv').show();
    //   AddWsCommandDiv(false); AddWsCommandDiv(true, 520);
    CallUniformCssForWsCmd();
}
//Set wsdl command datasetpath value
function IntWsdlDsPath() {
    var ctrlId = '[id$=txtWsdlCommand_DatasetPath]';
    var strPfix = AppActiveCtrl['DataSetPath'] + '.';
    if (AppActiveCtrl['DataSetPath'].length != 0 && $(ctrlId).val().trim().indexOf(strPfix) == 0) {
        $(ctrlId).val($(ctrlId).val().trim().replace(strPfix, ''));
    }
}
function ShowHideHeader() {
    $('#adddivheader').hide();
    $('#DiveditDBobject').show();
}
function CancelBtnClick() {
    $('#AddDiv').hide();
    $('#WsCmdDetailsDiv').show();
    BindDatabaseObjectdata();
    //  CallUniformCssForWsCmd();
    ClearValue();
}
function ClearValue() {
    $('[id$=lblUrl]').text('');
    $('[id$=txtWsdl_CmdName]').val('');
    $('[id$=txtWsdlCmdDesc]').val('');
    $('[id$=txtWsCommand_Url]').val('');
    $('[id$=ddl_WsConnactor]').val('-1');
}
function WsDisplayMethod() {
    $('#WsdlCmdDetailsDiv').show();
}
//Delete confirmation popup
function SubProcDeleteRpcPram(_bol) {
    SubProcCommonMethod('SubProcDeleteRpcPram', _bol, "Delete Parameter", 300);
}
//Add parameter click event 
function AddRpcParamClick(e) {
    if (e.id.length > 0) {
        var Type = e.id.split('_')[1];
        var Name = e.id.split('_')[2];
        rpcParaType = Type;
        RpcControlInt();
        $('#hdfRpcParamParent').val(Name);
        $('#hdfRpcParamLevel').val((parseInt(($('#aLevel' + Type + 'Para_' + Name)[0].innerHTML).trim(), 10) + 1));
        SubProcAddRpcPara(true, 'Add Xml Rpc Input parameters');
        var DivId = RpcParamAppendBeforeDiv(Name, parseInt(($('#aLevel' + Type + 'Para_' + Name)[0].innerHTML).trim(), 10));
        if (DivId.length == 0) $('#hdfRpcParamParent').val(Name + ',0' + ',' + Name);
        else $('#hdfRpcParamParent').val(DivId + ',1' + ',' + Name);
        $('#hdfIsInnerParam').val('1');
        if ($('#' + Type + 'PType_' + Name)[0].children[0].innerHTML == 'array') {
            var ChildJson = GetRpcParaAtLevelAftrArray(parseInt(($('#aLevel' + Type + 'Para_' + Name)[0].innerHTML).trim(), 10) + 1, Name, rpcParaType, false);
            if (ChildJson.length > 0) {
                SubProcAddRpcPara(false);
                $('#aMessage').html('Array already contains type.');
                SubProcBoxMessage(true);
            }
        }
    }
}
//Add rpc command parameter html
function RpcParamAppendBeforeDiv(_Para, _Level) {
    var strJson = "";
    var Level;
    var start = 0;
    var DivId = '';
    $.each($('#rpc' + rpcParaType + 'ParaContDiv1')[0].children, function (e) {
        if (this.className == "rpcContText") {
            if (DivId.length > 0) {
                return '';
            }
            if (parseInt($('#aLevel' + rpcParaType + 'Para_' + this.innerHTML.trim())[0].innerText.trim()) != _Level) {

            }
            else {
                //if ($('#aLevel' + rpcParaType + 'Para_' + this.innerHTML.trim())[0].innerHTML.trim() != _Level.toString()) return;
                if (start == 1) {
                    return DivId = this.innerHTML.trim();
                }
                if (this.innerHTML.trim() == _Para) start = 1;
            }
        }
    });
    return DivId;
}
///*********Code related to wsdl/http command output parameter*************//
var WsParaCount = 0;
//Edit ws command parameter (btnWsdlCmdOutput,btnWsHttpCmdOutput event)
function EditWsCmdPara(_IsWsdl) {
    //  if (!wsPuralPathVisible()) return;
    $('#WsParaContent').html(''); WsParaCount = 0;
    AppActiveCtrl['Id'] = '';
    AppActiveCtrl['Value'] = '';
    AppActiveCtrl['LastKeyDown'] = 0;
    $('[id$=txtWsdlCommand_DatasetPath]').blur();
    var strHtml = "";
    if (WsParaHash['0'] != null) {
        $.each(jQuery.parseJSON(WsParaHash['0'][0]), function () {
            WsParaCount = WsParaCount + 1;
            $('#WsParaContent').append('<div id="WsParaContentText_' + WsParaCount + '" class="DpContentRow">' +
                    '<div class="DpContentText"> <input id="WsParatext_' + WsParaCount + '" type="text" class="txtDpOption" /></div>' +
                    '<div class="DpContentValue"> <input id="WsParaPath_' + WsParaCount + '" type="text"  class="txtDpOption"/></div>' +
                    '<div id="WsParaDel_' + WsParaCount + '" class="DpOptionContentDelete">' +
                        '<img id="imgWsParaDelete_' + WsParaCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" title="Delete Property"/>' +
                    '</div>' +
                  '</div><div style="clear: both;"></div>');
            $('#WsParatext_' + WsParaCount).val(this['name']);
            $('#WsParaPath_' + WsParaCount).val(WsdlEditParamPath(true, this['path']));
            $('#imgWsParaDelete_' + WsParaCount).bind('click', function () {
                $('#' + this.parentNode.parentNode.id).remove();
                wsAppendAddBtn();
            });
            if (_IsWsdl) {
                $('#WsParaPath_' + WsParaCount).bind('click', function (event) {
                    AppActiveCtrl['Id'] = this.id;
                });
                $('#WsParaPath_' + WsParaCount).bind('keyup', function (event) {
                    if (AppActiveCtrl['CmdType'] != '1') wsdlPathChangeEvent(this, event, false);
                });
                $('#WsParaPath_' + WsParaCount).bind('keydown', function (event) {
                    if (AppActiveCtrl['CmdType'] != '1') wsdlPathChangeEvent(this, event, true);
                });
            }
        });
    }
    if (WsParaCount == 0) addNewWsPara();
    wsAppendAddBtn();
    SubProcWsCmdOutputPara(true);
}

//Used to show values in intelligence according to control value
function wsdlPathChangeEvent(e, event, IsKeyDown) {
    if (IsKeyDown) {
        if (event.which == '13') {
            if ($("#SubWsIntls").is(':visible')) {
                event.preventDefault();
                $("#SubWsIntls").find('.INTS-SELECTED').each(function () {
                    AddSelectedInslsVal(this.id);
                    var test = "";
                });
            }
        }
        if (event.which == '40' || event.which == '38') {
        }
        else {
            if ($("#SubWsIntls").is(':visible')) {
                SubWsIntls(false, e);
            }
        }
        $('#' + AppActiveCtrl['Id']).focus();
    }
    else {
        if (AppActiveCtrl['Id'] != e.id) AppActiveCtrl['LastKeyDown'] = 0;
        AppActiveCtrl['Id'] = e.id;
        AppActiveCtrl['Value'] = $(e).val();
        if ((event.which == '40' || event.which == '38') && (AppActiveCtrl['LastKeyDown'] == event.which)) {
            if ($('#SubWsInnrIntls').html().trim().length > 0) SubWsIntls(true, e);
        }
        else {
            if ($('#SubProcWsCmdOutputPara').is(':visible') && AppActiveCtrl['IsDsPath'] == 1) {
                wsdlPropPathIntls(e.id, $(e).val());
            }
            else {
                wsdlParamIntlsn(e.id, $(e).val());
            }

            if ($('#SubWsInnrIntls').html().trim().length > 0) SubWsIntls(true, e);
        }
        $(e).focus();
        if (AppActiveCtrl['LastDivCount'] == undefined || AppActiveCtrl['LastDivCount'].length == 0) AppActiveCtrl['LastDivCount'] == 1;
        if (event.which == '40') {
            if ($("#SubWsIntls").is(':visible')) {
                var test = "";
                if (AppActiveCtrl['LastKeyDown'] != '40') {
                    if ($(AppActiveCtrl['SelectedIntls']).length > 0) {
                        changeIntlsColor('#WfViewCtrlDiv_' + (parseInt(AppActiveCtrl['SelectedIntls'].split('_')[1]) + 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = 2;
                    }
                }
                else {
                    changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] + 1));
                    AppActiveCtrl['LastKeyDown'] = event.which;
                    if (AppActiveCtrl['LastDivCount'] >= (AppActiveCtrl['MaxDivCount'] - 1)) {
                        changeIntlsColor('#WfViewCtrlDiv_1');
                        AppActiveCtrl['LastDivCount'] = 1;
                    }
                    else AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] + 1);
                }
            }
        }
        else if (event.which == '38') {
            if ($("#SubWsIntls").is(':visible')) {
                var test = "";
                if (AppActiveCtrl['LastKeyDown'] != '38') {
                    if ((AppActiveCtrl['LastDivCount'] - 1) < 1) {
                        changeIntlsColor('#WfViewCtrlDiv_1');
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = 1;
                    }
                    else {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] - 1);
                    }
                }
                else {
                    if ((AppActiveCtrl['LastDivCount'] - 1) < 1) {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['MaxDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['MaxDivCount'] - 1);
                    }
                    else {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] - 1);
                    }
                }
            }
        }
        else {
            AppActiveCtrl['LastKeyDown'] = event.which;
            AppActiveCtrl['LastDivCount'] = 1;
        }
    }
}

//set parameter path value
function WsdlEditParamPath(_IsEdit, _Val) {
    if (_Val == undefined) return '';
    _Val = _Val.trim();
    if (AppActiveCtrl['CmdType'] != '0') return _Val;
    if (_IsEdit) {
        var strPfix = AppActiveCtrl['DataSetPath'] + '.';
        if (AppActiveCtrl['DataSetPath'].length != 0 && _Val.indexOf(strPfix) == 0) {
            return _Val.replace(strPfix, '');
        }
        else return _Val;
    }
    else {
        if (AppActiveCtrl['DataSetPath'].length != 0 && _Val.length != 0 && (!$('#DivWsdlCmd_DsPathCtrl').is(':visible'))) {
            return AppActiveCtrl['DataSetPath'] + '.' + _Val;
        }
        else return _Val;
    }
}

function Hide(v1) {
    var DCatch = $(v1).val();
    $('[id$=hidSelectcatch]').val(DCatch);
}

function EditCancelupdate() {
    $('#WsCmdDetailsDiv').show();
    $('#AddDiv').hide();
}
//Database command Delete  Not Allowed.
function DelWsCommands(_bol) {
    if (_bol) {
        showModalPopUp('ShowDelete', 'Delete Not Allowed', 500);
        CallUniformCssForWsCmd();
    }
    else {
        $('#ShowDelete').dialog('close');
        CallUniformCssForWsCmd();
    }
}

function mprortHide() {
    $('#mport').hide();
}

function WsdlOutHide() {
    $('#WsdlOutParamDiv').show();
}
function RPCInpuctutOutput() {
    $('#viewRpcparameterout').show();
    $('#viewRpcparameterout2').show();
}
//WsHttpCmdDetailsDiv
function AddStyle() {
    $('[id$=WsHttpCmdDetailsDiv').addClass("hide");
}
function RemoveStyle() {
    $('[id$=WsHttpCmdDetailsDiv').removeClass("hide");
}
function AfterSuccessfullUpdate() {
    $('#WsdlCmdDiv').hide();
    $('#SimpleHttpCmdPropDiv').hide();
    $('#RpcCmdPropDiv').hide();
}

//This sub is to used to get inner parameter of struct type parameter.
function GetRpcParaAtLevelAfterStruct(_Level, _ParentPara, _Type) {
    var strJson = '';
    var innerJson = "";
    $.each($('#rpc' + _Type + 'ParaContDiv1')[0].children, function (e) {
        if (this.type == "level") {
            if ($('#aLevel' + _Type + 'Para_' + this.id.split('_')[1].trim())[0].innerHTML.trim() == _Level) {
                if (_ParentPara != $('#aPre' + _Type + 'Para_' + this.id.split('_')[1].trim())[0].innerHTML.trim()) return;
                if ($('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim() != 'struct') {
                    if (strJson.length > 0) strJson += ',';
                    strJson += '{"name":"' + this.id.split('_')[1].trim() + '",';
                    if ($('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim().split('-')[0] != 'array') {
                        strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim()) + '"}';
                    }
                    else {
                        innerJson = "";
                        innerJson = GetRpcParaAtLevelAftrArray((_Level + 1), this.id.split('_')[1].trim(), _Type);
                        strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim()) + '",';
                        if ($('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim().split('-')[1] == 'array') {
                            strJson += '"inparam":[' + innerJson + ']}';
                        }
                        else {
                            strJson += '"inparam":""}';
                        }
                    }
                }
                else {
                    innerJson = "";
                    if (strJson.length > 0) strJson += ',';
                    innerJson = GetRpcParaAtLevelAfterStruct((_Level + 1), this.id.split('_')[1].trim(), _Type);
                    strJson += '{"name":"' + this.id.split('_')[1].trim() + '",';
                    strJson += '"typ":"' + GetRpcParamType($('#' + _Type + 'PType_' + this.id.split('_')[1].trim())[0].innerText.trim()) + '",';
                    strJson += '"inparam":[' + innerJson + ']}';
                }
            }
        }
    });
    return strJson;
}
//RPC Case Input Output Paramenter Set

function InCaseOfRPC() {
    if ($('[id$=hdfEditWsType]').val() != "1" && $('[id$=hdfEditWsType]').val() != "0") {
        CreateRpcParameterjson('In', true);
        CreateRpcParameterjson('Out', true);
    }
}

//function RPC Table Show

function RPCShow() {
    $('#rpc' + _ParaType + 'ParaContDiv1').show();
    $('#rpc' + _ParaType + 'ParaContDiv2').show();
}

//WsCommand dataset path change event
function SetWsDatasetPathChange(e) {
    if (e.value == "0") {
        $('#DivWsCmd_DsPathCtrl').hide();
        $('#DivWsCmd_DsPathHead').hide();

        $('#DivWsdlCmd_DsPathCtrl').hide();
        $('#DivWsdlCmd_DsPathHead').hide();
    }
    else {
        $('#DivWsCmd_DsPathCtrl').show();
        $('#DivWsCmd_DsPathHead').show();
        $('#DivWsdlCmd_DsPathCtrl').show();
        $('#DivWsdlCmd_DsPathHead').show();
    }
}

//WsCommand dataset path related div hide
function SetWsDatasetPathVisible() {
    $('#DivWsCmd_DsPathCtrl').show();
    $('#DivWsCmd_DsPathHead').show();
}


//WsCommand dataset path related div hide
function SetWsDatasetPathNotVisible() {
    $('#DivWsCmd_DsPathCtrl').hide();
    $('#DivWsCmd_DsPathHead').hide();
}

// Confirmation message popup
function SubProcConfirmBoxMessageWo(_bol, _title, _width) {
    if (_bol) showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    else $('#SubProcConfirmBoxMessage').dialog('close');
}

function openwscopyobject(_bol) {
    if (_bol)   showModalPopUp('divwscopyobject', 'Copy Objects', 400);
    else $('#divwscopyobject').dialog('close');
    CallUniformCssForWsCmd();
}

function disabletextwsobject() {
    $('[id$=txtWsdl_CmdName]').attr("disabled", "disabled");
    $('[id$=ddl_WsConnactor]').attr("disabled", "disabled");
}

function Enabletextwsobject() {
    $('[id$=txtWsdl_CmdName]').removeAttr("disabled");
    $('[id$=ddl_WsConnactor]').removeAttr("disabled");
}

function userwsValid() {
    var Name;
    Name = $('[id$=txtwscopyobject]').val();
    if (Name == '') {
        alert("Please enter  copy object Name");
        return false;
    }
    else if (Name.length <= 3) {
        alert("Object name not less than three charcter");
        return false;
    }
    else if (Name.length >= 50) {
        alert("Object name not greater than 50 charcter");
        return false;
    }
}

function wsobjectvalidation() {
    var strVal, drpconnector, drpcommandtype = '';
    var datacatchtype, dd1AbMonth, ddlRlt, dd1AbHr, dd1Abhh, ddlAbMM, ddlAbDay, dd1AbMnthDay, dd1AbMnDay, ddlRlt, drprltmin, Drprlthour, rltdayd, raltweeks = '';
    var strMessage = '';
    strVal = $('[id$=txtWsdl_CmdName]').val();
    drpconnector = $('select[id$=ddl_WsConnactor]').val();
    if (strVal == '')  strMessage += " * Please enter web service object name.</br>"; 
    else if (strVal.length < 3)  strMessage += " *Object name cannot be less than 3 charecters.</br>"; 
    else if (strVal.length > 50)   strMessage += " * Object name cannot be more than 50 charecters.</br>"; 
    if (drpconnector == '-1' || drpconnector == '' || drpconnector == '')  strMessage += " * Please select  the Connector </br>"; 
    if (strMessage.length > 0) {
        showMessage(strMessage, DialogType.Error);
        return false;
    }
}

function Abmonth(v1) {
    var DCatch = $(v1).val();
    absolute(DCatch);
}

function absolute(DCatch) {
    $('[id$=Catcmin]').hide();
    $('[id$=AbEvDayDiv]').hide();
    $('[id$=AbEvWkDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$=AbEvYrDiv]').hide();
    $('[id$=RltDivmin]').hide();
    $('[id$=RltHour]').hide();
    $('[id$=RltDay]').hide();
    $('[id$=RltWeek]').hide();
    $('[id$=RltMonth]').hide();
    if (DCatch == '0')   $('[id$=Catcmin]').show(); 
    else if (DCatch == '1') $('[id$=AbEvDayDiv]').show(); 
    else if (DCatch == '2') $('[id$=AbEvWkDiv]').show(); 
    else if (DCatch == '3') $('[id$=AbEvMnDiv]').show(); 
    else if (DCatch == '4') $('[id$=AbEvYrDiv]').show(); 
}

function Relative(v1) {
    var DCatch = $(v1).val();
    relativetcatch(DCatch);
}
function relativetcatch(DCatch) {
    $('[id$=RltDivmin]').hide();
    $('[id$=RltHour]').hide();
    $('[id$=RltDay]').hide();
    $('[id$=RltWeek]').hide();
    $('[id$=RltMonth]').hide();
    $('[id$=Catcmin]').hide();
    $('[id$=AbEvDayDiv]').hide();
    $('[id$=AbEvWkDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$= AbEvYrDiv]').hide();
    if (DCatch == '0') $('[id$=RltDivmin]').show(); 
    else if (DCatch == '1')  $('[id$=RltHour]').show(); 
    else if (DCatch == '2') $('[id$=RltDay]').show(); 
    else if (DCatch == '3')    $('[id$=RltWeek]').show(); 
    else if (DCatch == '4')  $('[id$=RltMonth]').show();
}

function Hide(v1) {
    var DCatch = $(v1).val();
    $('[id$=ddlRlt]').val('-1');
    $('[id$=dd1AbMonth]').val('-1');
    catchtype(DCatch);
}

function catchtype(val) {
    $('[id$=CatchEvery]').hide();
    $('[id$=RltDiv]').hide();
    $('[id$=RltDivmin]').hide();
    $('[id$=RltHour]').hide();
    $('[id$=RltDay]').hide();
    $('[id$=RltWeek]').hide();
    $('[id$=RltMonth]').hide();
    $('[id$=AbEvDayDiv]').hide();
    $('[id$=AbEvWkDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$=AbEvYrDiv]').hide();
    $('[id$=AbEvMnDiv]').hide();
    $('[id$=AbEvYrDiv]').hide();
    if (val == '1') {
        $('[id$=CatchEvery]').show();
        $('[id$=dd1DataCatch]').css({ 'width': 200 });
        $('select[id$=CatchEvery]').val('0');
        absolute("0");
    }
    else if (val == '2') {
        $('[id$=RltDiv]').show();
        $('[id$=dd1DataCatch]').css({ 'width': 200 });
        $('select[id$=RltDiv]').val('0');
        relativetcatch('0');
    }
    else {
        $('select[id$=dd1DataCatch]').val('0');
        $('select[id$=dd1DataCatch]').parent().children("span").text($('select[id$=dd1DataCatch]').children('option:selected').text());
        absolute('0');
        Relative('0');
    }
}

function Odataaddbtndiscard() {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('6');
}

function viewConnectiontest(id, index) {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('5');
    setclass = index;
    for (var i = 0; i < objwsdataset.length; i++) {
        if (id == objwsdataset[i].WS_COMMAND_NAME) {
            discardobject = objwsdataset[i].WS_COMMAND_NAME;
            break;
        }
    }
}

function discardedit() {
    if (setclass != undefined && setclass.toString() != '') {
        ViewWSDisplay(discardobject);
        if (setclass % 2 === 0) $('#tablewsobj tbody tr').eq(setclass).addClass('even selected')
        else $('#tablewsobj tbody tr').eq(setclass).addClass('odd selected')
    }
     setclass = '';
}


function addbtnclick() {
    var val = $('[id$=hideditdone]').val();
    $('[id$=hiddiscardcommadtype]').val('2');
    if (val != '') {
        SubModifiction(true);
        return false;
    }
    else  return true;
}

function ChangeRowSelectedCSS(selIndex) {
    if (selIndex != undefined) {
        $('#tablewsobj tbody tr.selected').removeClass('selected');
        if (selIndex % 2 === 0) $('#tablewsobj tbody tr').eq(selIndex).addClass('even selected')
        else  $('#tablewsobj tbody tr').eq(selIndex).addClass('odd selected')
    }
}

function CancelDiscard() {
    ChangeRowSelectedCSS(selIndex);
}

function dialogtest() {
    $('#divdetails').children().remove();
    var servicetype = $('[id$=hidservicetype]').val();
    parameter = $('[id$=hidparameter]').val();
    var aryFinalTypesForUI = [];
    if (parameter != '' && ($.trim(servicetype) == 'http' || $.trim(servicetype) == 'rpc')) {
        $('#divdetails').append('<div id="divparameter" class="AddParameter">');
        aryFinalTypesForUI = parameter.split(',');
    }
    else if (parameter != '' && $.trim(servicetype) == 'wsdl') {
        var objFinalInputParam = $.parseJSON(parameter);
        var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objFinalInputParam.S_Types, "");
        var aryComplexTypesForUI = _getWsdlComplexTypeForUI(objFinalInputParam.C_Types, "");
        for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) 
            aryFinalTypesForUI.push(arySimpleTypesForUI[j]);

        for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++)
            aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
    }

    if (aryFinalTypesForUI.length) {
        $('#divdetails').append('<div id="divparameter" class="AddParameter">');
        var i;
        for (i = 0; i < aryFinalTypesForUI.length; i++) {
            var strHtml = '<div id="TestAddParaDiv_' + aryFinalTypesForUI[i] + '" >' +
                             '<div class="QPContentText1">' + aryFinalTypesForUI[i] + '</div>' +
                                    '<div class="QPContentType1" style="float:right;padding-right:105px !important;">' +
                                        '<input  id="para_' + aryFinalTypesForUI[i] + '"type="text" style="width:100%" ><br>' +
                                        '</div></div><div style="clear: both;"></div>';

            $('#divparameter').append(strHtml);
        }
    }
    else 
        $('#divdetails').append('<div id="divparameter" class="AddParameter"> No Input Parameter in this object.</div>');
    $('#divdetails').append('<div class="SubProcborderDiv"><div class="SubProcBtnMrgn" style="text-align:center;" ><button  onclick="fncsave();" type="button"  class="InputStyle">Execute</button></div></div>');
    $('[id$=hdflp]').val(JSON.stringify(aryFinalTypesForUI));
    opendialogtest(true);
}


//Odata connector edit
function opendialogtest(_bol) {
    if (_bol)  showModalPopUp('dialogtestPara', 'Execute  Object', 500);
    else $('#dialogtestPara').dialog('close');
}

function _getWsdlInputParamsForUI() {
    var objFinalInputParam = $.parseJSON(parameter);
    var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objFinalInputParam.S_Types, "");
    var aryComplexTypesForUI = _getWsdlComplexTypeForUI(objFinalInputParam.C_Types, "");
    var aryFinalTypesForUI = [];
    for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
        aryFinalTypesForUI.push(arySimpleTypesForUI[j]);
    }
    for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++) {
        aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
    }
    return aryFinalTypesForUI;
}

function _getWsdlSimpleTypeForUI(arrayOfSimpleType, containerObjectName) {
    var aryFinalSimpleType = [];
    if (arrayOfSimpleType && $.isArray(arrayOfSimpleType) && arrayOfSimpleType.length > 0) {
        for (var i = 0; i <= arrayOfSimpleType.length - 1; i++) {
            var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arrayOfSimpleType[i].S_Name :
                                                arrayOfSimpleType[i].S_Name;
            aryFinalSimpleType.push(strSimpleTypeForUI);
        }
    }
    return aryFinalSimpleType;
}

function _getWsdlComplexTypeForUI(complexTypes/*array*/, containerObjectName) {
    var aryFinalComplexType = [];
    if (complexTypes && $.isArray(complexTypes) && complexTypes.length > 0) {
        for (var i = 0; i <= complexTypes.length - 1; i++) {
            var objComplexType = complexTypes[i];
            var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objComplexType.S_Types, objComplexType.C_Name);
            var aryInnerComplexType = _getWsdlComplexTypeForUI(objComplexType.C_Types, objComplexType.C_Name);
            for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arySimpleTypesForUI[j] :
                                                arySimpleTypesForUI[j];
                aryFinalComplexType.push(strSimpleTypeForUI);
            }
            for (var k = 0; k <= aryInnerComplexType.length - 1; k++) {
                var strComplexTypeForUI = containerObjectName ? containerObjectName + "." + aryInnerComplexType[k] :
                                                aryInnerComplexType[k];
                aryFinalComplexType.push(strComplexTypeForUI);
            }
        }
    }
    return aryFinalComplexType;
}

function fncsave() {
    var aryFinalComplexType = JSON.parse($('[id$=hdflp]').val());
    var lp = [];
    for (i = 0; i < aryFinalComplexType.length; i++) {
        lp.push({ "para": aryFinalComplexType[i], "val": $('#para_' + aryFinalComplexType[i]).val() });
    }

    $('[id$=hdflp]').val(JSON.stringify(lp));
    $('[id$=savebtn]').click();
}

function Credentialvalidation() {
    var user = $('[id$=txtUsername]').val();
    var password = $('[id$=txtPassword]').val();
    if (user == '') {
        alert('Please enter username');
    }
    else if (password == '') {
        alert('Please enter password');
    }
    if (user != '' && password != '') {
        DataCredentialBy(false);
        return true;
    }
    else {
        return false;
    }
}

function SubProcBoxMessageDataConection(_bol, Header) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', Header, 400);
        $('#btnOkErrorMsg').focus();
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}

//Load js file from server end
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
