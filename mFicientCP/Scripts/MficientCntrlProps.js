(function ($) {
    $.fn.mficientCntrlProps = function (opts) {
        var CONSTANTS = {
            classes: {
                "innerPropertiesDiv": "InnerPropertieDiv",
                "propertyName": "propertyName",
                "propertyValue": "propertyValue",
                "txtProp": "txtProp",
                "propertyHeader": "propertyHeader",
                "propertyHeaderImage": "propertyHeaderImage",
                "propertyHeaderText": "propertyHeaderText",
                "propertyCollapse": "PropertyCollapse",
                "ddlProp": "ddlProp",
                "propDisable": "propDisable",
                "btnProp": "btnProp",
                "txtBarPropSmall": "txtBarPropSmall",
                "txtPropSmall": "txtPropSmall",
                "imgIconClass": "MenuIconHead",
                "groupWrapper": "propSheetCntrlGrpWrapper",
                "propSheetFocus": "propSheetFocus",
                "cntrlTextArea": "TextArea",
                "cntrlTextAreaLabel": "TextAreaLabel",
                "helpHeaderText": "HelpHeadDiv",
                "helpDescription": "HelpContentDiv",
                "groupHeader": "propSheetGroupHeader",
                "groupHeaderImage": "propSheetGroupHeaderText",
                "groupHeaderText": "propSheetGroupHeaderImage"
            }
        };

        var defaults = {
            propertySheetJson: {},
            editUIJson: "", //json string stored in database
            editObject: null//eg:App object, view Object,form object--> if available pass this.If provided then editJson (even if provided) will not be used.
        };

        var options = $.extend(defaults, opts);
        //Control type which is formed (Controls which is dragged.)
        var $self = $(this);
        if (options.propertySheetJson) {
            //var objPropSheet = $.parseJSON(options.propertySheetJson);
            var objPropSheet = options.propertySheetJson;
            if (objPropSheet) {
                //passing MF_IDE_CONSTANTS.CONTROLS to "type" property--> which has the property for prefix.
                var _mfControlPrefix = objPropSheet.type.propPluginPrefix; //MF_IDE_CONSTANTS.CONTROLS
                $self.children().remove(); //remove any previous html.
                $self.html(_formHtml(objPropSheet, _mfControlPrefix));
                if (options.editObject) {
                    $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet, options.editObject);
                }
                else {
                    $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet, _formObjectForData(objPropSheet.type, options.editUIJson));
                }
                $self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper, {
                    getControlPropertyId: _getPropControlPrefix,
                    getControlWrapperId: _getWrapperDivPrefix,
                    getBrowseBtnControlId: _getBrowseBtnPrefix,
                    getGroupWrapperDivPrefix: _getGroupWrapperDivPrefix,
                    getControlDtls: _getControlDtls,
                    getAllWrapperDivs: _getAllWrapperControls
                });
                if (options.editObject || options.editUIJson) {
                    _initControls(objPropSheet, _mfControlPrefix, $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet),
                            $self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper));
                }
                else {
                    _initControls(objPropSheet, _mfControlPrefix, options.editObject,
                            $self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper));
                }
                //_initControls(objPropSheet, _mfControlPrefix, $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet),
                //$self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper));
                _bindEvents(objPropSheet, _mfControlPrefix);
                _bindEventsForVisualChangesOnCntrlFocus(objPropSheet);
            }
        }
        return $self;
        //The Object For which the property html is generated.
        //The object is stored as data with the div.
        function _formObjectForData(type, json/*string*/) {
            var objForData = null;
            switch (type) {
                case MF_IDE_CONSTANTS.CONTROLS.SECTION:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.LABEL:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.LIST:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.SEPARATOR:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.BARCODE:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.TABLE:
                    break;
                case MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.APPS:
                    objForData = new Apps();
                    if (json) {
                        //value avaiable for edit.
                        objForData.fillProperties(json);
                    }
                    break;
                case MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.FORM:
                    if (json) {
                        //value avaiable for edit.
                        form.fillProperties(json);
                    }
                    break;
            }
            return objForData;
        }
        function _formHtml(objPropSheet, mfControlPrefix) {
            var strHtml = "";
            if (objPropSheet) {
                if (objPropSheet.groups &&
                $.isArray(objPropSheet.groups) &&
                objPropSheet.groups.length > 0) {

                    var objPropGroups = objPropSheet.groups;
                    var strGroupName = "";
                    var objGroup = {};
                    for (var i = 0; i <= objPropGroups.length - 1; i++) {
                        strGroupName = objPropGroups[i].name;
                        objGroup = objPropGroups[i];
                        //Group Wrapper
                        strHtml += _getDivStartTagWithMultipleClasses(
                                _getGroupWrapperDivPrefix(mfControlPrefix, objGroup.prefixText),
                                [CONSTANTS.classes.groupWrapper]);
                        //Get Properties HTML
                        strHtml += _getHtmlOfPropTextAndControlFromPropObj(objGroup, mfControlPrefix, strGroupName);
                        //End Properties
                        strHtml += _getDivEndTag();
                        //End Group Wrapper
                    }
                }
            }
            return strHtml;
        }
        function _initControls(objPropSheet, mfControlPrefix, editObject/*object*/, pluginPublicHelper) {
            if (objPropSheet) {
                if (objPropSheet.groups &&
                $.isArray(objPropSheet.groups) &&
                objPropSheet.groups.length > 0) {

                    var objPropGroups = objPropSheet.groups;
                    var strGroupName = "",
                        strGroupPrefixText = "";
                    var objGroup = {};
                    for (var i = 0; i <= objPropGroups.length - 1; i++) {
                        strGroupPrefixText = objPropGroups[i].prefixText;
                        objGroup = objPropGroups[i];
                        _initControlsByGroup(objGroup, mfControlPrefix, strGroupPrefixText, editObject, pluginPublicHelper);
                    }
                }
            }
        }
        function _bindEvents(objPropSheet, mfControlPrefix) {
            if (objPropSheet) {
                if (objPropSheet.groups &&
                $.isArray(objPropSheet.groups) &&
                objPropSheet.groups.length > 0) {

                    var objPropGroups = objPropSheet.groups;
                    var strGroupName = "",
                        strGroupPrefixText = "";
                    var objGroup = {};
                    for (var i = 0; i <= objPropGroups.length - 1; i++) {
                        strGroupPrefixText = objPropGroups[i].prefixText;
                        objGroup = objPropGroups[i];
                        _bindEventsForControlByGroup(objGroup, mfControlPrefix, strGroupPrefixText);
                    }
                }
            }
        }
        function _bindEventsForVisualChangesOnCntrlFocus(objPropSheet) {
            if (objPropSheet) {
                var $wrapperDivs = $self.find("." + CONSTANTS.classes.innerPropertiesDiv);
                $wrapperDivs.on('click', function (evnt) {
                    var $otherPropSheetsWrapperDivs = $([]),
                        aryOtherPropSheetVisible = objPropSheet.otherPropSheetVisible,
                        i = 0,
                        objPropSheetCntrl = null,
                        divData = null,
                        strHelpTextDivId = "",
                        $helpTextDiv = $([]);
                    if (aryOtherPropSheetVisible &&
                        $.isArray(aryOtherPropSheetVisible) &&
                        aryOtherPropSheetVisible.length > 0) {

                        for (i = 0; i <= aryOtherPropSheetVisible.length - 1; i++) {
                            objPropSheetCntrl = aryOtherPropSheetVisible[i].propSheetCntrl;

                            if (objPropSheetCntrl && $.isFunction(objPropSheetCntrl.AllWrapperDivs)) {

                                $otherPropSheetsWrapperDivs = objPropSheetCntrl.AllWrapperDivs();
                                $otherPropSheetsWrapperDivs.removeClass(CONSTANTS.classes.propSheetFocus);
                            }
                        }
                    }
                    $wrapperDivs.removeClass(CONSTANTS.classes.propSheetFocus);
                    $(this).addClass(CONSTANTS.classes.propSheetFocus);
                    divData = $(this).data(MF_IDE_CONSTANTS.jqueryCommonDataKey);
                    if (divData) {
                        strHelpTextDivId = divData.helpTextDivId;
                        $helpTextDiv = $('#' + strHelpTextDivId);
                        if (divData.helpText && divData.helpText.header && divData.helpText.description) {
                            $helpTextDiv.find('.' + CONSTANTS.classes.helpHeaderText).html(divData.helpText.header);
                            $helpTextDiv.find('.' + CONSTANTS.classes.helpDescription).html(divData.helpText.description);
                        }
                        else {
                            $helpTextDiv.find('.' + CONSTANTS.classes.helpHeaderText).html('');
                            $helpTextDiv.find('.' + CONSTANTS.classes.helpDescription).html('');
                        }
                    }
                    evnt.stopPropagation();
                });
            }
        }
        //Init CONTROLS
        function _initControlsByGroup(group/*Object of group*/,
            mfControlPrefix, groupPrefixText,
            editObject/*object*/, pluginPublicHelper/*object*/) {
            var blnIsEdit = false, mapFunctionToCall = null;
            var objControlsToPassForEditInit = null;
            if (editObject) {
                blnIsEdit = true;
                objControlsToPassForEditInit = {};
            }
            for (var i = 0; i <= group.properties.length - 1; i++) {
                var property = group.properties[i];
                var propPrefixText = property.prefixText;
                var $control = $self.find("#" + _getPropControlPrefix(mfControlPrefix, groupPrefixText, propPrefixText));
                var $divWrapperParent = $self.find("#" + _getWrapperDivPrefix(mfControlPrefix, groupPrefixText, propPrefixText));
                //try to use this.this is a better method.
                var objCntrlJqueryObjects = _getControlDtls(mfControlPrefix, groupPrefixText, propPrefixText);
                $divWrapperParent.data(MF_IDE_CONSTANTS.jqueryCommonDataKey, {
                    helpTextDivId: group.helpTextDivId,
                    helpText: property.helpText
                });
                var $browseBtn = $();
                if (property.init &&
                $.isPlainObject(property.init)) {

                    var objInit = property.init;

                    switch (property.control) {
                        //This is textbox which is disabled             
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.label:
                            break;
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox:
                            $control.attr('maxlength', objInit.maxlength);
                            $control.prop("disabled", objInit.disabled);
                            break;
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea:
                            break;
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown:
                            $control.prop("disabled", objInit.disabled);
                            switch (objInit.bindOptionsType) {
                                case MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual:
                                    $control.children().remove();
                                    var strOptions = "";
                                    if (property.options && property.options.length > 0) {
                                        for (var j = 0; j <= property.options.length - 1; j++) {
                                            strOptions += "<option value= \"" + property.options[j].value + "\">";
                                            strOptions += property.options[j].text + "</option>";
                                        }
                                    }
                                    $control.html(strOptions);
                                    break;
                                case MF_PROPS_CONSTANTS.DDL_BIND_TYPE:
                                    break;
                                case MF_PROPS_CONSTANTS.DDL_BIND_TYPE.webservice:
                                    break;
                                case MF_PROPS_CONSTANTS.DDL_BIND_TYPE.odata:
                                    break;
                            }
                            break;
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.browse:
                            $browseBtn = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, propPrefixText));
                            $control.val(objInit.defltValue);
                            $browseBtn.prop("disabled", objInit.disabled);
                            break;
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon:
                            $browseBtn = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, propPrefixText));
                            var $icon = $browseBtn.closest('.' + CONSTANTS.classes.propertyValue).find('img[class="' + CONSTANTS.classes.imgIconClass + '\"]');
                            $browseBtn.prop("disabled", objInit.disabled);
                            $icon.attr("src", objInit.iconSrc);
                            $control = $icon;
                            break;
                    }
                    if (objInit.hidden) {
                        $divWrapperParent.hide();
                    }
                    else {
                        $divWrapperParent.show();
                    }
                    if (blnIsEdit) {
                        //adding value to control html when in edit process
                        mapFunctionToCall = editObject.propSheetHtmlMapping.groups[groupPrefixText] &&
                            editObject.propSheetHtmlMapping.groups[groupPrefixText][propPrefixText];
                        if (mapFunctionToCall && $.isFunction(mapFunctionToCall)) {
                            mapFunctionToCall($control, editObject, {
                                cntrlHtmlObjects: objCntrlJqueryObjects
                            });
                        }
                    }
                    else {
                        if (objInit.defltValue) {
                            $control.val(objInit.defltValue);
                        }
                    }
                    if (objInit.finalEditInit && objInit.finalEditInit.passForFinalEdit === true) {
                        objControlsToPassForEditInit[objInit.finalEditInit.propName] = {
                            wrapDiv: $divWrapperParent,
                            control: $control,
                            browseBtn: $browseBtn
                        };
                    }
                }
            }
            if (blnIsEdit) {
                //final initialisation on edit.there may be instances when you want to show hisde some div based
                //on the value set on certain control.or do some other initialisation which was not possible
                //when the control was given value above.Initialisation after all the values in controls are set.
                var otherInitOnEdit = editObject.propSheetHtmlMapping.groups[groupPrefixText] &&
                            editObject.propSheetHtmlMapping.groups[groupPrefixText]["OtherInitOnEdit"];
                if (otherInitOnEdit && $.isFunction(otherInitOnEdit)) {
                    //otherInitOnEdit(editObject, pluginPublicHelper);
                    otherInitOnEdit(editObject, objControlsToPassForEditInit);
                }
            }
        }
        //BIND EVENTS
        function _bindEventsForControlByGroup(group/*Object of group*/,
            mfControlPrefix, groupPrefixText) {
            for (var i = 0; i <= group.properties.length - 1; i++) {
                var property = group.properties[i];
                var $control = $self.find("#" + _getPropControlPrefix(mfControlPrefix, groupPrefixText, property.prefixText));
                var objCntrlJqueryObjects = _getControlDtls(mfControlPrefix, groupPrefixText, property.prefixText);
                switch (property.control) {
                    //This is textbox which is disabled             
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.label:
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox:
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea:
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown:
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.browse:
                        $control = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, property.prefixText));
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon:
                        $control = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, property.prefixText));
                        break;
                }
                if (property.events &&
                $.isArray(property.events) &&
                property.events.length > 0) {
                    for (var j = 0; j <= property.events.length - 1; j++) {
                        var evnt = property.events[j];
                        var objEvntData = {};
                        if (evnt.arguments) {
                            objEvntData = processGetArgumentObjectForFunc(evnt.arguments, mfControlPrefix);
                        }
                        objEvntData.propObject = $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet);
                        objEvntData.selfUiCntrls = objCntrlJqueryObjects;
                        //evnt.arguments = [];
                        //evnt.arguments.push(objEvntArgs);
                        // if ($.isFunction(evnt.func)) {
                        // var func = function (evnt) {
                        // return function () {
                        // evnt.func.apply(evnt.context ? evnt.context : $self, evnt.arguments);
                        // };
                        // } (evnt)
                        // $control.on(evnt.name, func);
                        // }
                        if ($.isFunction(evnt.func)) {
                            $control.on(evnt.name, objEvntData, evnt.func);
                            // $control.on('click', function () {
                            // alert('testing alert')
                            // });
                        }

                    }
                }
                switch (property.control) {
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea:
                        var objJqueryObjects = objCntrlJqueryObjects; //for closure.

                        objJqueryObjects.textAreaLabel.on('click', function (evnt) {
                            objJqueryObjects.control.show();
                            objJqueryObjects.textAreaLabel.hide();
                            objJqueryObjects.control.parent().show("slow");
                            objJqueryObjects.control.parents('.' + CONSTANTS.classes.propertyValue).addClass('w_100');
                            objJqueryObjects.wrapperDiv
                            .find('.' + CONSTANTS.classes.propertyName)
                            .addClass('w_100')
                            .append('<span class="fr SpanDoneForTxtAreaCntrl cursorClickable mrgr5" style="color:blue;font-size:10px;text-decoration:underline;">done</span>');

                            objJqueryObjects.wrapperDiv.addClass('propSheetFocus');
                            evnt.stopPropagation();
                        });
                        objJqueryObjects.wrapperDiv.on('click', function (evnt) {
                            if ($(evnt.target).attr('id') !== objJqueryObjects.control.attr('id') &&
                                    objJqueryObjects.control.is(':visible')) {


                                objJqueryObjects.control.parent().hide("slow", function () {
                                    var strCntrlValue = "";
                                    strCntrlValue = objJqueryObjects.control.val();
                                    if (strCntrlValue && strCntrlValue.length > 0) {
                                        // if (strCntrlValue.length > 25) {
                                        //     objJqueryObjects.textAreaLabel.val(strCntrlValue.substring(0, 25) + '...');
                                        // }
                                        // else {
                                        //     objJqueryObjects.textAreaLabel.val(strCntrlValue);
                                        // }
                                        MF_HELPERS.setTextOfTextAreaLabelOfPropSheet(
                                            strCntrlValue,
                                            objJqueryObjects.textAreaLabel);
                                    }
                                    else {
                                        objJqueryObjects.textAreaLabel.val('');
                                    }
                                    objJqueryObjects.textAreaLabel.show();
                                    objJqueryObjects.control.parents('.' + CONSTANTS.classes.propertyValue).removeClass('w_100');
                                    objJqueryObjects.wrapperDiv
                                    .find('.' + CONSTANTS.classes.propertyName)
                                    .removeClass('w_100')
                                    .find('span.SpanDoneForTxtAreaCntrl')
                                    .remove();
                                });
                            }
                        });
                        break;

                }
            }
        }

        //END BIND EVENTS

        //HTML FORMATION HELPERS
        function _getHtmlOfPropTextAndControlFromPropObj(group/*Object of group*/,
            mfControlPrefix, groupName) {
            var strHtml = _getGroupNameWrapperHtml(groupName, group.hidden); //correct
            strHtml += _getDivStartTag("", CONSTANTS.classes.PropertyCollapse);
            for (var i = 0; i <= group.properties.length - 1; i++) {
                strHtml += _getCollapsiblePropWrapperHtml(group.properties[i], mfControlPrefix, group.prefixText);

            }
            strHtml += _getDivEndTag();
            strHtml += _getDivEndTag();
            return strHtml;
        }
        function _getGroupNameWrapperHtml(groupName, isHidden) {
            var strHtml = "";
            if (groupName && !isHidden) {
                strHtml += _getDivStartTag("", CONSTANTS.classes.propertyHeader);
                strHtml += "<span class =\"" + CONSTANTS.classes.propertyCollapse + " " + CONSTANTS.classes.propertyHeaderImage + "\"></span>";

                strHtml += _getDivStartTag("", CONSTANTS.classes.propertyHeaderText);
                strHtml += groupName;
                strHtml += _getDivEndTag();

                strHtml += _getDivEndTag();
            }
            return strHtml;
        }
        function _getCollapsiblePropWrapperHtml(property, mfControlPrefix, groupNamePrefix) {
            var strHtml = "";
            if (property) {
                strHtml += _getCompleteInnerPropWrapperDiv(property, mfControlPrefix, groupNamePrefix);
            }
            return strHtml;
        }
        function _getCompleteInnerPropWrapperDiv(property, mfControlPrefix, groupNamePrefix) {
            var strHtml = "";

            //get Inner WrapperDiv 
            strHtml += _getDivStartTag(
                _getWrapperDivPrefix(mfControlPrefix, groupNamePrefix, property.prefixText),
                CONSTANTS.classes.innerPropertiesDiv
                );
            //get propertyName div
            strHtml += _getPropertyNameCompleteHtml(property.text);
            //end propertyName div
            strHtml += _getControlHtml(property, mfControlPrefix, groupNamePrefix);
            strHtml += _getDivEndTag();
            //end get Inner WrapperDiv div
            return strHtml;
        }
        function _getPropertyNameCompleteHtml(propName) {
            return _getDivStartTag("", CONSTANTS.classes.propertyName) + '<span  style="margin-left: 20px; width: 80%;">' + propName + '</span>' + _getDivEndTag();
        }
        function _getControlHtml(property, mfControlPrefix, groupNamePrefix) {
            var strHtml = _getDivStartTag("", CONSTANTS.classes.propertyValue);
            if (property) {
                switch (property.control) {
                    //This is textbox which is disabled             
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.label:
                        strHtml += "<div><input id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText);
                        strHtml += "\" ";
                        strHtml += "type=\"text\" value=\"\" class=\"" + CONSTANTS.classes.txtProp + "\" maxlength=\"50\" readonly=\"readonly\"/></div>";
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox:
                        strHtml += "<div><input id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\"";
                        strHtml += " type=\"text\" value=\"\" class=\"" + CONSTANTS.classes.txtProp + "\" maxlength=\"" + property.init.maxlength + "\" onkeydown=\"return enterKeyFilter(event);\"/></div>";
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea:
                        var id = _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText);
                        strHtml += "<div><input id=\"" + _getTextAreaLabelPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\"";
                        strHtml += " type=\"text\" value=\"\" class=\"" + CONSTANTS.classes.txtProp + " " + CONSTANTS.classes.cntrlTextAreaLabel + "\"";
                        strHtml += " maxlength=\"50\" readonly=\"readonly\"/></div>";
                        strHtml += "<div class=\"fl hide w_98\">";
                        strHtml += "<textarea id=\"" + id + "\"";
                        strHtml += " value=\"\" class=\"w_100 " + CONSTANTS.classes.cntrlTextArea + "\" maxlength=\"" + property.init.maxlength + "\"";
                        strHtml += " rows=\"3\" cols=\"30\" onkeydown=\"return enterKeyLinefeed('" + id + "', event);\"></textarea>";
                        strHtml += "</div>";
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown:
                        strHtml += "<div><select id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\"";
                        strHtml += " class=\"" + CONSTANTS.classes.ddlProp + "\" onkeydown=\"return enterKeyFilter(event);\"></select></div>";
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.browse:
                        strHtml += "";
                        strHtml = _getDivStartTag("", CONSTANTS.classes.propertyValue + " " + CONSTANTS.classes.propDisable);
                        strHtml += "<div class=\"fl w_80\"><input id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText);
                        strHtml += "\"";
                        strHtml += "type=\"text\" maxlength=\"50\"  ";
                        strHtml += "class=\"" + CONSTANTS.classes.txtPropSmall + " w_100" + "\" disabled=\"disabled\" readonly=\"readonly\" /></div>  ";
                        strHtml += "<div class=\"fr w_20\"><input  id=\"" + _getBrowseBtnPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\" type=\"button\" value=\"...\" ";
                        strHtml += "class=\"w_100 " + CONSTANTS.classes.btnProp + "\" /></div>";
                        break;
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon:
                        strHtml += "<div class=\"fl \"><img alt=\"Icon\" id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\"" + "class=\"" + CONSTANTS.classes.imgIconClass + "\" src=\"\" style=\"margin: 2px;\"></div> ";
                        strHtml += "<div><input  id=\"" + _getBrowseBtnPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\" type=\"button\" value=\"...\" ";
                        strHtml += "class=\"" + CONSTANTS.classes.btnProp + "\" /></div>";
                        break;
                }
            }
            strHtml += _getDivEndTag();
            return strHtml;
        }

        //Gives a div start tag with class = InnerPropertiesDiv
        function _getInnerPropertyDivStartTag(id) {
            return _getDivStartTag(id, CONSTANTS.classes.innerPropertiesDiv);
        }
        /*
        Returns div start tag.
        If no id is provided then div with class name is returned.
        */
        function _getDivStartTag(id/*string*/, cssClass/*string*/) {
            if (id && cssClass) {
                return "<div id=\"" + id + "\" class=\"" + cssClass + "\">";
            }
            else if (id && !cssClass) {
                return "<div id=\"" + id + "\">";
            }
            else if (!id && cssClass) {
                return "<div class=\"" + cssClass + "\">";
            }
            else if (!id && !cssClass) {
                return "<div>";
            }
        }
        function _getDivStartTagWithMultipleClasses(id/*string*/, cssClasses/*array of strings*/) {
            var strClasses = "",
                i = 0;
            if ($.isArray(cssClasses) && cssClasses.length > 0) {
                for (i = 0; i <= cssClasses.length - 1; i++) {
                    strClasses += cssClasses[i] + " ";
                }
            }
            if (id && strClasses) {
                return "<div id=\"" + id + "\" class=\"" + strClasses + "\">";
            }
            else if (id && !strClasses) {
                return "<div id=\"" + id + "\">";
            }
            else if (!id && strClasses) {
                return "<div class=\"" + strClasses + "\">";
            }
            else if (!id && !strClasses) {
                return "<div>";
            }
        }
        function _getDivEndTag() {
            return "</div>";
        }
        //END HTML FORMATION HELPERS
        //Event Arguments HELPERS
        function processGetArgumentObjectForFunc(eventArgsInfo/*arguments tag in prop sheet*/, mfControlPrefix) {
            var objArgsObject = null;
            if (eventArgsInfo && $.isPlainObject(eventArgsInfo)) {
                objArgsObject = new Object();
                $.each(eventArgsInfo, function (key, value) {
                    switch (key) {
                        case "cntrls":
                            objArgsObject[key] = _getControlsToPassAsFuncArgs(value, mfControlPrefix);
                            break;
                    }
                    //alert( key + ": " + value );
                });
            }
            return objArgsObject;
        }
        function _getControlsToPassAsFuncArgs(controlDtls, mfControlPrefix) {
            var objCntrolDtls = null;
            if (controlDtls) {
                objCntrolDtls = new Object();
                $.each(controlDtls, function (index, value) {
                    var objControlHtmlInfo = new Object();
                    var objControlHtmlDetails = null;
                    objControlHtmlInfo.controlId = _getPropControlPrefix(mfControlPrefix, value.grpPrefText, value.cntrlPrefText);
                    objControlHtmlInfo.wrapperDiv = _getWrapperDivPrefix(mfControlPrefix, value.grpPrefText, value.cntrlPrefText);
                    objControlHtmlInfo.browseBtn = _getBrowseBtnPrefix(mfControlPrefix, value.grpPrefText, value.cntrlPrefText);
                    objControlHtmlDetails = _getControlDtls(mfControlPrefix, value.grpPrefText, value.cntrlPrefText);
                    objControlHtmlInfo.browseBtnCntrl = objControlHtmlDetails.browseBtn;
                    objControlHtmlInfo.controlCntrl = objControlHtmlDetails.control;
                    objControlHtmlInfo.wrapperDivCntrl = objControlHtmlDetails.wrapperDiv;
                    objControlHtmlInfo.textAreaLabelCntrl = objControlHtmlDetails.textAreaLabel;
                    objCntrolDtls[value.rtrnPropNm] = objControlHtmlInfo;
                });
            }
            return objCntrolDtls;
        }
        //END EVENT ARGUMENTS HELPERS
        //PREFIX HELPERS
        //return the prefix added to every Property control.
        function _getPropControlPrefix(mfControlPrefix, groupPrefix, propName) {
            return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_Control";
        }
        function _getWrapperDivPrefix(mfControlPrefix, groupPrefix, propName) {
            return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_WrapDiv";
        }
        function _getBrowseBtnPrefix(mfControlPrefix, groupPrefix, propName) {
            return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_Browse";
        }
        function _getGroupWrapperDivPrefix(mfControlPrefix, groupPrefix) {
            return mfControlPrefix + "_" + groupPrefix;
        }
        function _getTextAreaLabelPrefix(mfControlPrefix, groupPrefix, propName) {
            return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_TALabel";
        }
        //Helper function for providing control Info to outside function
        function _getControlDtls(mfControlPrefix, groupPrefix, propName) {
            var objControl = new PropertySheetControl();
            objControl.wrapperDiv = $('#' + _getWrapperDivPrefix(mfControlPrefix, groupPrefix, propName));
            objControl.control = $('#' + _getPropControlPrefix(mfControlPrefix, groupPrefix, propName));
            objControl.browseBtn = $('#' + _getBrowseBtnPrefix(mfControlPrefix, groupPrefix, propName));
            objControl.textAreaLabel = $('#' + _getTextAreaLabelPrefix(mfControlPrefix, groupPrefix, propName));
            return objControl;
        }
        function _getAllWrapperControls() {
            return $self.find("." + CONSTANTS.classes.innerPropertiesDiv);
        }
        //PREFIX HELPERS
    };
})(jQuery);
(function ($) {
    $.fn.mficientRefreshCntrlsOnChng = function (optns) {
        var CONSTANTS = {
            Controls: [],
            SelectedControls: [],
            arrCheckbox: []
        };

        var options = $.extend(optns);
        var $self = this;
        $self.html(_getRefreshControlsHtml());
        $.each(CONSTANTS.arrCheckbox, function () {
            $('#' + this).unbind('click');
            $('#' + this).bind('click', function () {
                if ($(this).is(":checked")) {
                    options.SelectedControls.push(this.id.split('_')[0]);
                }
                else {
                    options.SelectedControls.pop(this.id.split('_')[0]);
                }
            });
        });

        this.data("SelectedControls", options.SelectedControls);

        function _getRefreshControlsHtml() {
            var strHtml = "";
            var DpOptionCount = 1;
            $('#OnChangeElmDiv').html('');
            $.each(options.Controls, function () {
                strHtml += "<tr>"
                            + "<td id=\"CtrlOnChangeText_" + DpOptionCount + "\"> "
                            + this
                            + "</td>"
                            + "<td style=\"width: 30px; padding-left: 25px;\">"
                            + "<input id=\"" + this + "_ChktrlOnChange_" + DpOptionCount + "\" type=\"checkbox\" " + ($.inArray(this.trim(), options.SelectedControls) > -1 ? " checked=\"checked\" " : "") + " />"
                            + "</td>"
                            + "</tr>";

                CONSTANTS.arrCheckbox.push(this + "_ChktrlOnChange_" + DpOptionCount);
                DpOptionCount = DpOptionCount + 1;
            });
            return strHtml;
        }
    };
})(jQuery);