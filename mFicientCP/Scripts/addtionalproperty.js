﻿var amazonlist = {};
var AmazonBindingData = [];
var vtag;
var totalRows = 0;
var addButtonHtml = '<a style="float:left;"><img  src=\css/images/pencil16x16.png\ title="Edit" onclick="EditAmazonProperty(this);"/></a>&nbsp;';
var RemoveAmazonButtonHtml = '<a  style="float:left; padding-left:30px;" data-tooltip=Delete onclick="DeleteAmazonItem(this);"><img  src=\"images/remove3_grey.png\" title="Remove"/></a>&nbsp;';
var AuthenticationAddButtonHtml = '<a  onclick="AuthenticationSave(this);" data-tooltip=Add><img  src=\"images/add3_grey.png\" title="Save credential"/></a>&nbsp;';
var AuthenticationRemoveButtonHtml = '<a   data-tooltip=Delete onclick="AuthenticationDelete(this);"><img  src=\"images/remove3_grey.png\" title="Remove credential"/></a>&nbsp;';
var AuthenticationTypeHtml = '<select id="selAuthenticationTypeHtml" class="UseUniformCss" style="width:150px !important;" onchange="AuthenticationTypeChange();">' +
                                    '<option value="0">Fixed Credential For All</option>' +
                                    '<option value="1">Fixed Credential Per User</option>' +
                                    '<option value="2">Credentials From Mobile App</option>' +
                                    '<option value="3">Password From Mobile App</option></select>';
var AuthenticationNameHtml = '<input type="text" id="AuthenticationName" style="width:100px;" class="Intext" maxlength="50" value="">';
var AuthenticationtagHtml = '<input type="text" id="Authenticationtag" style="width:100px;" class="Intext" maxlength="50" value="">';
var AuthenticationLifeHtml = '<select id="selAuthenticationLifeHtml" class="UseUniformCss">' +
                                    '<option value="1">mFicient Session</option>' +
                                    '<option value="2">App Session</option></select>';
var SettingCrButtonHtml = '<a style="float:left;" @@id@@ data-tooltip=CredentialSetting onclick="SettingOfCredential(\'@@unm@@\',\'@@password@@\',\'@@crtag@@\');"><img  src=\"images/settings_grey.png\" title="Set credential"/></a>&nbsp;';
var AuthenticationNameEdit = "<a data-tooltip=Updatename onclick=AuthenticationSaveName(this);><img  src=\css/images/pencil16x16.png\ title=Set credential/  class=btnstyle></a>";
var DeleteConfirmationMessage = 'Are you sure you want to delete this Additional Property.';
AuthenticationListData = [];
function LoadAmazonPage() {
    var straaData = "";
    straaData = $('[id$=hidAmazonData]').val();
    var amazontype = "";
    if (straaData.length > 0) {
        amazonlist = jQuery.parseJSON(straaData);
    }
    else {
        amazonlist["acr"] = [];
        amazonlist["acp"] = [];
    }
    if (straaData != "") {
        if (typeof amazonlist.acr != 'undefined') {
            jQuery.each(amazonlist.acr, function (i, val) {
                if (val.type == 1) amazontype = "Normal User";
                else amazontype = "Cognito Pool";
                AmazonBindingData.push([val.name, val.id, amazontype, addButtonHtml + RemoveAmazonButtonHtml]);
            });
        }
        if (typeof amazonlist.acp != 'undefined') {
            jQuery.each(amazonlist.acp, function (i, val) {
                if (val.type == 1) amazontype = "Normal User";
                else amazontype = "Cognito Pool";
                AmazonBindingData.push([val.name, val.id, amazontype, addButtonHtml + RemoveAmazonButtonHtml]);
            });
        }
    }
    BindAmazonDatabasedata(AmazonBindingData);
}
function BindAmazonDatabasedata(AmazonBindingData) {
    $('#divAmazon').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblAmazon"></table>');
    $('#tblAmazon').dataTable({
        "data": AmazonBindingData,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "bSort": false,
        "columns": [{ "title": "Name" }, { "title": "Key" }, { "title": "Type" }, { "title": "<button style=float:right;width:70px; onclick='return AddAmazonRow()'  title=Add New> Add </button>"}]
    });

    var tableAuth = $('#tblAmazon').DataTable();
    $('#tblAmazon tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            tableAuth.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
    $('#tblAmazon tbody td').css('text-align', 'left');
}
function AddAmazonRow() {
    showModalPopUp('divamazonaddrow', " Amazon AWS ", '350', false);
    clearAmazoncntrl();
    return false;
}
function AmzonSecretKey(oselect) {
    var selectval = $(oselect).val();
    if (selectval == "1") amazonnormalusershow();
    else amazoncognatousershow();
}
function amazonnormalusershow() {
    $('[id$=divamazonvalue]').hide();
    $('[id$=divnormaluser]').show();
}
function amazoncognatousershow() {
    $('[id$=divamazonvalue]').show();
    $('[id$=divnormaluser]').hide();
}
function SaveAmzonSecreatkey() {
    var flag = false;
    flag = validationamazonProperty();
    if (flag) {
        var bool = true;
        $.each(AmazonBindingData, function (key, vas) {
            if (vas[0] == $('[id$=textamazonname]').val()) {
                bool = false;
            }
        });
        if ($('[id$=hideditAmazondata]').val() == "") {
            if (bool)
                $.each(AmazonBindingData, function (key, vas) {
                    if (vas[1] == $('[id$=textamazonkey]').val()) {
                        bool = false;
                    }
                });
            var amazontype = $('select[id$=selectamazontype]').val();
            items = {}
            items["name"] = $('[id$=textamazonname]').val();
            items["id"] = $('[id$=textamazonkey]').val();
            items["sr"] = $('select[id$=DropDownserviceregion]').val();
            items["type"] = amazontype;
            if (amazontype == 1) {
                items["ak"] = $('[id$=txtaccesskey]').val();
                items["sak"] = $('[id$=txtsecrectaccesskey]').val();

                if (bool) {
                    amazonlist.acr.push(items);
                    AmazonBindingData.push([items["name"], items["id"], ((amazontype == 1) ? "Normal User" : "Cognito Pool"),
                            addButtonHtml + RemoveAmazonButtonHtml]);
                }
            }
            else {
                items["cr"] = $('select[id$=DropDownCognitoRegion]').val();
                items["cpid"] = $('[id$=DropDownCognitopoolId]').val();
                if (bool) {
                    amazonlist.acp.push(items);
                    AmazonBindingData.push([items["name"], items["id"], ((amazontype == 1) ? "Normal User" : "Cognito Pool"),
                            addButtonHtml + RemoveAmazonButtonHtml]);
                }
            }
        }
        else {
            var editvalue = $('[id$=hideditAmazondata]').val().split(',');
            if (editvalue[1] == 1) {
                $.each(amazonlist.acr, function (key, value) {
                    if (value.name == editvalue[0]) {

                        if (bool == false) {
                        }
                        else {
                            value.name = $('[id$=textamazonname]').val();
                            value.sr = $('select[id$=DropDownserviceregion]').val();
                            value.ak = $('[id$=txtaccesskey]').val();
                            value.sak = $('[id$=txtsecrectaccesskey]').val();
                        }
                    }
                });
            }
            else {
                $.each(amazonlist.acp, function (keys, value) {
                    if (value.name == editvalue[0]) {
                        if (bool == false) {
                        }
                        else {
                            value.name = $('[id$=textamazonname]').val();
                            value.sr = $('select[id$=DropDownserviceregion]').val();
                            value.cr = $('select[id$=DropDownCognitoRegion]').val();
                            value.cpid = $('[id$=DropDownCognitopoolId').val();
                        }
                    }

                });
            }
            if (bool === true) {
                jQuery.each(AmazonBindingData, function (index, value) {
                    if (value[0] == editvalue[0]) {
                        value[0] = $('[id$=textamazonname]').val();
                    }
                });
            }
        }
        $('[id$=hidAmazonData]').val(JSON.stringify(amazonlist));
        if ($('[id$=hideditAmazondata]').val() != "") {

         if(editvalue[0] === $('[id$=textamazonname]').val() )     bool = true;
        }
        if (bool == false) {
            alert('Name or  key is already exist');
        }
        else {
            BindAmazonDatabasedata(AmazonBindingData);
            SubAmazonclose();
            UpdateAmazon();
        }
    }
}
function SubAmazonclose() {
    $('#divamazonaddrow').dialog('close');

}
function clearAmazoncntrl() {
    $('[id$=textamazonname]').val('');
    $('[id$=textamazonkey]').val('');
    $('[id$=txtaccesskey]').val('');
    $('[id$=txtsecrectaccesskey]').val('');
    $('select[id$=DropDownCognitoRegion]').val('0');
    $('select[id$=DropDownCognitoRegion]').parent().children("span").text($('select[id$=DropDownCognitoRegion]').children('option:selected').text());
    $('select[id$=DropDownserviceregion]').val('0');
    $('select[id$=DropDownserviceregion]').parent().children("span").text($('select[id$=DropDownserviceregion]').children('option:selected').text());
    $('select[id$=selectamazontype]').val('1');
    $('select[id$=selectamazontype]').parent().children("span").text($('select[id$=selectamazontype]').children('option:selected').text());
    $('[id$=DropDownCognitopoolId]').val('');
    $('[id$=hideditAmazondata]').val("");
    $('[id$=textamazonkey]').removeAttr("disabled");
    $('[id$=selectamazontype]').removeAttr("disabled");
    $('[id$=divamazonvalue]').hide();
    $('[id$=divnormaluser]').show();
    $('[id$=extedittextbox]').show();
    $('[id$=editkeylablel]').hide();


}
function ValidateSecreatkey() {
    var amazontype = $('select[id$=selectamazontype]').val();
    var amzoncompletedetails = '';
    amzoncompletedetails = $('[id$=textamazonname]').val() + ',' + $('[id$=textamazonkey]').val() + ',' + $('[id$=txtaccesskey]').val() + ',' + $('[id$=txtsecrectaccesskey]').val() + ',' + $('select[id$=DropDownserviceregion]').val() + ',' + $('select[id$=DropDownCognitoRegion]').val() + ',' + $('select[id$=DropDownCognitopoolId]').val() + ',' + amazontype;
    if (amzoncompletedetails != '')
        amzoncompletedetails = amzoncompletedetails.split(",");

}
function DeleteAmazonItem(value) {
    var DeleteConfirmation = 'Are you sure you want to delete this Amazon Property.';
    var table = $('#tblAmazon').DataTable();
    var row = table.row($(value).parents('tr'));
    var data = row.data();
    var confirmMsg = confirm(DeleteConfirmation);
    if (confirmMsg) {
        row.remove().draw();
        var deletevalues = "";
        if (data[2] == 'Normal User') deletevalues = 1;
        else deletevalues = 2;
        RemoveAmazonItem(deletevalues, data[0]);
    }
    else {
        BindAmazonDatabasedata(AmazonBindingData);
    }
}
function RemoveAmazonItem(deletevalues, name) {
    var outputjson = "";
    if (deletevalues == 1) {
        amazonlist.acr = $.grep(amazonlist.acr, function (element, index) {
            return element.name != name;
        });
    }
    else {
        amazonlist.acp = $.grep(amazonlist.acp, function (element, index) {
            return element.name != name;
        });
    }
    AmazonBindingData = $.grep(AmazonBindingData, function (element, index) {
        return element[0] != name;
    });
    outputjson = JSON.stringify(amazonlist);
    $('[id$=hidAmazonData]').val(outputjson);
    UpdateAmazon();
}
function EditAmazonProperty(val) {
    clearAmazoncntrl();
    $('[id$=divamazonvalue]').hide();
    $('[id$=divnormaluser]').hide();
    $('[id$=extedittextbox]').hide();
    $('[id$=editkeylablel]').show();
    showModalPopUp('divamazonaddrow', " Edit Amazon AWS ", '350', false);
    var table = $('#tblAmazon').DataTable();
    var row = table.row($(val).parents('tr'));
    var data = row.data();
    var selectvalues = "";
    if (data[2] == 'Normal User') selectvalues = 1;
    else selectvalues = 2;
    if (selectvalues == 1) {
        $('[id$=divamazonvalue]').hide();
        $('[id$=divnormaluser]').show();
        $.each(amazonlist.acr, function (key, value) {
            if (value.id == data[1]) {
                $('[id$=hideditAmazondata]').val(data[0] + ',' + selectvalues);
                $('[id$=textamazonname]').val(value.name);
                $('[id$=textamazonkey]').val(value.id);
                $('[id$=lblamazonkey]').text(value.id);
                
                $('[id$=txtaccesskey]').val(value.ak);
                $('[id$=txtsecrectaccesskey]').val(value.sak);
                $('select[id$=DropDownserviceregion]').val(value.sr);
                $('select[id$=DropDownserviceregion]').parent().children("span").text($('select[id$=DropDownserviceregion]').children('option:selected').text());
                $('select[id$=selectamazontype]').val(value.type);
                $('select[id$=selectamazontype]').parent().children("span").text($('select[id$=selectamazontype]').children('option:selected').text());
                $('select[id$=DropDownCognitoRegion]').val('0');
                $('select[id$=DropDownCognitoRegion]').parent().children("span").text($('select[id$=DropDownCognitoRegion]').children('option:selected').text());
                $('[id$=DropDownCognitopoolId]').val('');
            }
        }
                );
    }
    else {
        $('[id$=divamazonvalue]').show();
        $('[id$=divnormaluser]').hide();
        $.each(amazonlist.acp, function (keys, value) {
            if (value.id == data[1]) {
                $('[id$=hideditAmazondata]').val(data[0] + ',' + selectvalues);
                $('[id$=textamazonname]').val(value.name);
                $('[id$=textamazonkey]').val(value.id);
                $('[id$=lblamazonkey]').text(value.id);
                $('[id$=txtaccesskey]').val('');
                $('[id$=txtsecrectaccesskey]').val('');
                $('[id$=DropDownCognitopoolId]').val(value.cpid);
                $('select[id$=DropDownserviceregion]').val(value.sr);
                $('select[id$=DropDownserviceregion]').parent().children("span").text($('select[id$=DropDownserviceregion]').children('option:selected').text());
                $('select[id$=selectamazontype]').val(value.type);
                $('select[id$=selectamazontype]').parent().children("span").text($('select[id$=selectamazontype]').children('option:selected').text());
                $('select[id$=DropDownCognitoRegion]').val(value.cr);
                $('select[id$=DropDownCognitoRegion]').parent().children("span").text($('select[id$=DropDownCognitoRegion]').children('option:selected').text());
            }
        });
    }
    $('[id$=textamazonkey]').attr("disabled", "disabled");
    $('[id$=selectamazontype]').attr("disabled", "disabled");
}
function validationamazonProperty() {
    var amazontype = $('select[id$=selectamazontype]').val();
    var name = $('[id$=textamazonname]').val();
    var amazonkey = $('[id$=textamazonkey]').val();
    var poolid = $('[id$=DropDownCognitopoolId]').val();
    var strMessage = "";
    if (amazontype == 1) {
        if (name == '') {
            strMessage += "Please check following points</br> </br> * Please enter name </br>";
        }
        if (amazonkey == '') {
            strMessage += "* Please enter Amazon key </br>";
        }
    }
    else {

        if (name == '') {
            strMessage += "Please check following points</br> </br> * Please enter Name </br> ";
        }
        if (amazonkey == '') {
            strMessage += "* Please enter Amazon key </br>";
        }
        if (poolid == '') {
            strMessage += "* Please enter Pool Id </br>";
        }
    }
    if (strMessage.length > 0) {
        $('#aCfmMessage').html(strMessage);
        $('#aCFmessage').html('');

        showModalPopUp("SubProcConfirmBoxMessage", "Error", "350", false);
        return false;
    }
    else {
        return true;
    }
}
function LoadAuthenticatioPage() {
    var AuthenticationBindingData = [];
    var straaData = $('[id$=hidAuthenticationData]').val();
    $('[id$=hidAuthenticationData]').val("");
    if (straaData.length > 0) {
        AuthenticationListData = jQuery.parseJSON(straaData);
    }
    jQuery.each(AuthenticationListData, function (i, val) {//"name":"","tag"="",type=""
        var valtype = (val.typeval == "0" ? SettingCrButtonHtml.replace("@@unm@@", val.uid).replace("@@password@@", val.pwd) : "").replace("@@crtag@@", val.tag).replace("@@id@@", "") + AuthenticationRemoveButtonHtml;
        AuthenticationBindingData.push([val.name + AuthenticationNameEdit, val.tag, val.type, val.life, valtype, ]);
    });
    AuthenticationBindingData.push([AuthenticationNameHtml, AuthenticationtagHtml, AuthenticationTypeHtml, AuthenticationLifeHtml, SettingCrButtonHtml.replace("@@unm@@", '').replace("@@password@@", '').replace("@@crtag@@", "").replace('@@id@@', 'id="New_Cr_setting"') + AuthenticationAddButtonHtml]);
    BindAuthenticationDatabasedata(AuthenticationBindingData);
}
function BindAuthenticationDatabasedata(AuthenticationBindingData) {
    $('#divAuthentication').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblAuthentication"></table>');
    $('#tblAuthentication').dataTable({
        "data": AuthenticationBindingData,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "bSort": false,
        "columns": [{ "title": "Name" }, { "title": "Key" }, { "title": "Credential Type" }, { "title": "Persistence Life" }, { "title": ""}]
    });

    var tableAuth = $('#tblAuthentication').DataTable();
    $('#tblAuthentication tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            tableAuth.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
    $("#selAuthenticationLifeHtml").hide();
}
//Delete Additional Property
function AuthenticationDelete(sender) {
    var table = $('#tblAuthentication').DataTable();
    var row = table.row($(sender).parents('tr'));
    var data = row.data();
    var confirmMsg = confirm(DeleteConfirmationMessage);
    if (confirmMsg) {
        // isCookieCleanUpRequired('false');
        row.remove().draw();
        AuthenticationListData.splice(row.index(), 1);
        totalRows--;
        UpdateAuthenticationDataToDatabase(data[1], "3");
        return true;
    }
    else return false;
}
function UpdateAuthenticationDataToDatabase(tag, opration) {
    $('[id$=hidAuthenticationData]').val(JSON.stringify(AuthenticationListData));

    // $('[id$=hidData]').val(JSON.stringify(AddPropertyListData));
    $('[id$=hidUpdateTag]').val(tag + "/" + opration + "/2");
    UpdateUpdBtnRowIndex();
}
//Save Additional Property
function AuthenticationSave(sender) {
    var table = $('#tblAuthentication').DataTable();
    var row = table.row($(sender).parents('tr'));
    var Authentication = {};

    var seltype = document.getElementById("selAuthenticationTypeHtml");
    var optionType = seltype.options[seltype.selectedIndex];

    var sellife = document.getElementById("selAuthenticationLifeHtml");
    var optionLife = sellife.options[sellife.selectedIndex];

    Authentication.name = $('#AuthenticationName').val();
    Authentication.tag = $('#Authenticationtag').val();
    //----max credential check-----
    if (totalRows >= 6) {
        alert("Max Credential Already Added"); return;
    }
    //----max credential name -----
    var errorMessage = CheckAuthenticationName(Authentication.name.toLowerCase());
    //----max credential tag-----
    if (errorMessage.length <= 0) errorMessage = CheckAuthenticationTag(Authentication.tag.toLowerCase());
    if (errorMessage.length > 0) { alert(errorMessage); return; }
    //----max credential username  & password-----
    if (optionType.value == "0") {
        if ($.trim($('#cr_tag').val()).length == 0 && $.trim($('#txt_FixedUserId').val()).length > 0 && $('#txt_FixedPwd').val().length > 0) {
        }
        else { alert("Enter Credential Values"); return; }
    }

    EditTag = Authentication.tag;
    Authentication.type = optionType.text;
    Authentication.typeval = optionType.value;
    if (Authentication.typeval == "1") {
        Authentication.life = "None";
        Authentication.lifeval = "0";
    }
    else {
        Authentication.life = optionLife.text;
        Authentication.lifeval = optionLife.value;
    }
    Authentication.uid = $.trim($('#txt_FixedUserId').val());

    Authentication.pwd = $('#txt_FixedPwd').val();

    AuthenticationListData.push(Authentication);

    totalRows++; row.remove().draw();
    //var valtype = (optionType.value == "0" ? SettingCrButtonHtml.replace("@@unm@@", Authentication.uid).replace("@@password@@", Authentication.pwd) : "").replace("@@id@@", "") + AuthenticationRemoveButtonHtml;
    var valtype = (optionType.value == "0" ? SettingCrButtonHtml.replace("@@unm@@", Authentication.uid).replace("@@password@@", Authentication.pwd) : "").replace("@@crtag@@", Authentication.tag).replace("@@id@@", "") + AuthenticationRemoveButtonHtml;
    table.row.add([Authentication.name + AuthenticationNameEdit, Authentication.tag, Authentication.type, Authentication.life, valtype]).draw();
    table.row.add([AuthenticationNameHtml, AuthenticationtagHtml, AuthenticationTypeHtml, AuthenticationLifeHtml, SettingCrButtonHtml.replace("@@unm@@", '').replace("@@password@@", '').replace("@@crtag@@", "").replace('@@id@@', 'id="New_Cr_setting"') + AuthenticationAddButtonHtml]).draw();

    $("#selAuthenticationLifeHtml").hide();
    UpdateAuthenticationDataToDatabase(EditTag, "1");
    Cleartextbox();
}
function CheckAuthenticationName(PropName, rowData) {
    var table = $('#tblAuthentication').DataTable();
    var errorMessage = '';

    if (PropName.length <= 0) {
        errorMessage = 'Enter Credential Name';
    }
    else {
        if (PropName.toLowerCase() == "mficient") {
            errorMessage = 'Credential Name Already Exists';
        }
        else {
            table.rows().indexes().each(function (idx) {
                var rowData = table.row(idx).data();
                if (rowData[0].toLowerCase().indexOf(PropName + '<') === 0)//PropName == rowData[0].toLowerCase())
                    errorMessage = 'Credential Name Already Exists';
            });
        }
        if (errorMessage.length <= 0)
            errorMessage = (/^[A-Za-z][a-zA-Z0-9 ]*$/.test(PropName.toLowerCase())) ? '' : 'Invalid Credential Name';
    }
    return errorMessage;
}
function CheckAuthenticationTag(PropTag) {
    var table = $('#tblAuthentication').DataTable();
    var errorMessage = ''; 
    if (PropTag.length <= 0) {
        errorMessage = 'Enter Credential Tag';
    }
    else {
        table.rows().indexes().each(function (idx) {
            var rowData = table.row(idx).data();
            if (PropTag == rowData[1].toLowerCase())
                errorMessage = 'Credential Tag Already Exists';
        });
        if (errorMessage.length <= 0)
            errorMessage = (/^[A-Za-z][a-zA-Z0-9_]*$/.test(PropTag.toLowerCase())) ? '' : 'Invalid Additional Property Tag';
    }
    return errorMessage;
}
function AuthenticationTypeChange() {
    $("#selAuthenticationLifeHtml").show();
    if ($('#selAuthenticationTypeHtml').val() == "0" || $('#selAuthenticationTypeHtml').val() == "1") { $("#selAuthenticationLifeHtml").hide(); }
    if ($('#selAuthenticationTypeHtml').val() == "0") {
        $("#New_Cr_setting").show();
        Cleartextbox();
    }
    else $("#New_Cr_setting").hide();
}
function SettingOfCredential(username, password, tag) {

    showModalPopUp('divModelCredential', " Setting ", '350', false);
    $('#txt_FixedUserId').val(username);
    $('#txt_FixedPwd').val(password);
    $('#cr_tag').val(tag);
    $('#divNameCr').hide();
}
function BindforAll() {
    var username = $('[id$=hidusername]').val();
    var pass = $('[id$=hidpassword]').val();
    $('#txt_FixedUserId').val(username);
    $('#txt_FixedPwd').val(pass);

}
function Cleartextbox() {
    $('#txt_FixedUserId').val('');
    $('#txt_FixedPwd').val('');
}
function UpdateFixedForAllUserCeredential() {
    $('#divModelCredential').dialog('close');
    jQuery.each(AuthenticationListData, function (i, val) {//"name":"","tag"="",type=""
        if (AuthenticationListData[i].tag == $('#cr_tag').val()) {
            AuthenticationListData[i].uid = '';
            AuthenticationListData[i].pwd = '';
            AuthenticationListData[i].uid = $('#txt_FixedUserId').val();
            AuthenticationListData[i].pwd = $('#txt_FixedPwd').val();

            UpdateAuthenticationDataToDatabase(AuthenticationListData[i].tag, "2");
            Cleartextbox();
            return true;
        }
    });
    return false;
}
function updatecredentialname() {
    showModalPopUp('divupdatename', " Name Setting  ", '300', false);
}
function AuthenticationSaveName(sender) {
    updatecredentialname();
    vtag = sender;
    $('#txtcredentialname').val('');
    $('#txtcredentialname').val($(sender).parents('tr').find('td::nth-child(1)').text());
}
function UpdateAuthenticationname() {
    $('#divupdatename').dialog('close');
    var table = $('#tblAuthentication').DataTable();
    var row = table.row($(vtag).parents('tr'));
    var datarow = row.data();
    $.each(AuthenticationListData, function (i, val) {
        if (AuthenticationListData[i].tag == datarow[1]) {
            var newName = $('#txtcredentialname').val();
            AuthenticationListData[i].name = newName;
            $('#txtcredentialname').val('');
            datarow[0] = newName + AuthenticationNameEdit;
            vtag = '';
            UpdateAuthenticationDataToDatabase(AuthenticationListData[i].tag, "2");
            row.data(datarow).draw();
            return true;
        }
    });
    return false;
}
//UnformDropDownDesign
function Uniform() {
  
    CallUnformCss($('#tblAuthentication:last tr:last td:eq(2) select'));
    CallUnformCss($('#tblAuthentication:last tr:last td:eq(3) select'));
}
function SubProcConfirmBoxMessage(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}
function SubProcImageDelConfirnmation(_bol, _title) {
    SubProcCommonMethod('SubProcImageDelConfirnmation', _bol, " Delete Additional Parameter", 450);
}
function SubProcCommonMethod(_Id, _Bol, _Title, _Width) {
    if (_Bol) {
        showModalPopUp(_Id, _Title, _Width, false);
    }
    else {
        $('#' + _Id).dialog('close');
    }
}