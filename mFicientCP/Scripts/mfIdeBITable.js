﻿
mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.biTable = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, tableObj) {
                $(control).val(tableObj.type.UIName)
            },
            Name: function (control, tableObj) {
                $(control).val(tableObj.userDefinedName);
            },
            Description: function (control, tableObj) {
                $(control).val(tableObj.description);
            },
            eventHandlers: {
                Type: function (evnt) {
                },
                Name: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        showMessage("Name already exists for another control.Please provide some other name", DialogType.Error);
                        $self.val(propObject.userDefinedName);
                        return;
                    }
                    propObject.userDefinedName = $self.val();
                },
                Description: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.description = $self.val();
                }
            }
        },
        Data: {
            Databinding: function (control, tableObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                var objDatabindingObj = tableObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    $(control).val(objDatabindingObj.fnGetBindTypeValue());
                }
            },
            DataObject: function (control, tableObj) {
                var objDatabindingObj = tableObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                        var objPropSheetCntrl = PROP_JSON_HTML_MAP.biTable.propSheetCntrl.Data.getDataObject();
                        if (objPropSheetCntrl) {
                            objPropSheetCntrl.control.val(objDatabindingObj.fnGetCommandName());
                            objPropSheetCntrl.wrapperDiv.show();
                        }

                    }
                    else {
                        $(control).val("Select object");
                        objPropSheetCntrl.wrapperDiv.hide();
                    }
                }
            },
            AllowSorting: function (control, tableObj) {
                if (tableObj.allowSorting) {
                    $(control).val(tableObj.allowSorting);
                }
            },
            AllowGrouping: function (control, tableObj) {
                if (tableObj.allowGrouping) {
                    $(control).val(tableObj.allowGrouping);
                }
            },
            AllowFilter: function (control, tableObj) {
                if (tableObj.allowFilter) {
                    $(control).val(tableObj.allowFilter);
                }
            },
            AllowPlot: function (control, tableObj) {
                if (tableObj.allowPlot) {
                    $(control).val(tableObj.allowPlot);
                }
            },
            ShowRowNo: function (control, tableObj) {
                if (tableObj.showRowNo) {
                    $(control).val(tableObj.showRowNo);
                }
            },
            DisplayRowCount: function (control, tableObj) {
                if (tableObj.displayRowCount) {
                    $(control).val(tableObj.displayRowCount);
                }
            },
            eventHandlers: {
                Databinding: function (evnt) {
                    var evntData = evnt.data;
                    //var confirm = confirm('Are you sure you want to change Databindin')
                    var $dataObjectWrapper = $('#' + evntData.cntrls.DataObject.wrapperDiv);
                    var $dataObjectCntrl = $('#' + evntData.cntrls.DataObject.controlId);
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    if ($self.val() === "0") {
                        $dataObjectWrapper.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        propObject.fnRemoveDatabindObjs();
                        propObject.fnDeleteColumns();
                    }
                    else {
                        $dataObjectWrapper.show();
                        propObject.fnRemoveDatabindObjs();
                        propObject.fnDeleteColumns();
                        var dataObject = new DatabindingObj("", [], "0", $self.val(), "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                    }
                    $dataObjectCntrl.val("Select Object");
                },
                DataObject: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var intlJson = [],
                        isEdit = false;
                    try {
                        if (propObject && propObject.databindObjs
                        && $.isArray(propObject.databindObjs)) {
                            $.each(propObject.databindObjs, function (index, value) {
                                var objDatabindObj = jQuery.extend(true, {}, value);
                                switch (objDatabindObj.bindObjType) {
                                    case "1":
                                        //bindType = MF_HELPERS.getCommandTypeByValue(value.bindObjType);
                                        mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 1100);
                                        break;
                                    case "2":
                                        mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 1100);
                                        break;
                                    case "5":
                                        mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 1100);
                                        break;
                                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:

                                        //                                        intlJson = MF_HELPERS
                                        //                                                    .intlsenseHelper
                                        //                                                    .getControlDatabindingIntellisenseJson(
                                        //                                                        objCurrentForm, propObject.id, true
                                        //                                                    );
                                        if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                            isEdit = true;
                                        }

                                        mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.TABLE,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                        showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 1100, false);
                                        break;
                                }
                            });
                        }
                    }
                    catch (error) {
                        if (error instanceof MfError) {
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                        } else {
                            console.log(error);
                        }
                    }
                },
                AllowSorting: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.allowSorting = $self.val();
                },
                AllowGrouping: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.allowGrouping = $self.val();
                },
                AllowFilter: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.allowFilter = $self.val();
                },
                AllowPlot: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.allowPlot = $self.val();
                },
                ShowRowNo: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.showRowNo = $self.val();
                },
                DisplayRowCount: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.displayRowCount = $self.val();
                }
            }
        },
        Behaviour: {
            Display: function (control, editableListObj) {
                //$(control).val(appObj.type.UIName)
            },
            Conditions: function (control, editableListObj) {
                //$(control).val(editableListObj.userDefinedName);
            },
            eventHandlers: {
                Display: function (evnt) {
                },
                Conditions: function (evnt) {
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getDataDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TABLE.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        return {
            Data: {
                getDataObject: _getDataDataObjectCntrl
            }
        };
    })()
};
mFicientIde.PropertySheetJson.BITable = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE,
    "groups": [
        {
            "name": "Control Properties",
            "prefixText": "ControlProperties",
            "hidden": true,
            "properties": [{
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [

                ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Table",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.biTable.groups.ControlProperties.eventHandlers.Name,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.biTable.groups.ControlProperties.eventHandlers.Description,
                    "context": "",
                    "arguments": {}
                }

                ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
            ]
        },
        {
            "name": "Data",
            "prefixText": "Data",
            "hidden": false,
            "properties": [
              {
                  "text": "Databinding",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Databinding",
                  "defltValue": "0",
                  "validations": [

                 ],
                  "customValidations": [

                 ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.Databinding,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DataObject",
                                 rtrnPropNm: "DataObject"
                             }
                          ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                    {
                        "text": "None",
                        "value": "0"
                    },
                    {
                        "text": "Database Object",
                        "value": "1"
                    },
                    {
                        "text": "Web Service Object",
                        "value": "2"
                    },
                    {
                        "text": "OData Object",
                        "value": "5"
                    },
                    {
                        "text": "Offline Database",
                        "value": "6"
                    }
                 ]
              },
              {
                  "text": "Data Object",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "DataObject",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                    {
                        "func": "functionName",
                        "context": "context"
                    }
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.DataObject,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Object",
                      "hidden": true
                  }
              },
              {
                  "text": "Enable Sorting",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AllowSorting",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.AllowSorting,
                        "context": "",
                        "arguments": {
                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Yes",
                        "value": "1"
                    },
                    {
                        "text": "No",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Enable Grouping",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AllowGrouping",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.AllowGrouping,
                        "context": "",
                        "arguments": {
                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Yes",
                        "value": "1"
                    },
                    {
                        "text": "No",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Enable Filter",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AllowFilter",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.AllowFilter,
                        "context": "",
                        "arguments": {
                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Yes",
                        "value": "1"
                    },
                    {
                        "text": "No",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Enable Plot",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AllowPlot",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.AllowPlot,
                        "context": "",
                        "arguments": {
                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Yes",
                        "value": "1"
                    },
                    {
                        "text": "No",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Show Row Number",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "ShowRowNo",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.ShowRowNo,
                        "context": "",
                        "arguments": {

                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Yes",
                        "value": "1"
                    },
                    {
                        "text": "No",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Display Row Count",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "DisplayRowCount",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Data.eventHandlers.DisplayRowCount,
                        "context": "",
                        "arguments": {

                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "As Header",
                        "value": "1"
                    },
                    {
                        "text": "As Footer",
                        "value": "2"
                    }
                    ,
                    {
                        "text": "As Header / Footer",
                        "value": "3"
                    }
                 ]
              }

           ]
        },
        {
            "name": "Behaviour",
            "prefixText": "Behaviour",
            "hidden": true,
            "properties": [
              {
                  "text": "Display",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Display",
                  "defltValue": "1",
                  "validations": [
                ],
                  "customValidations": [

                 ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Behaviour.eventHandlers.Display,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "Conditions",
                                 rtrnPropNm: "Conditions"
                             }
                          ]
                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": true,
                      "hidden": true,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Always",
                        "value": "1"
                    },
                    {
                        "text": "Conditional",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Conditions",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "Conditions",
                  "disabled": false,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                    {
                        "func": "functionName",
                        "context": "context"
                    }
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.biTable.groups.Behaviour.eventHandlers.ConditionalDisplay,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Conditions",
                      "hidden": true
                  }
              }
           ]
        }
    ]
};
function Table(options) {
    if (!options) options = {};
    this.id = options.id;
    this.userDefinedName = options.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Table;
    this.description = options.description;
    this.type = options.type;
    this.columns = options.columns; //array of TableColumn
    this.databindObjs = options.databindObjs; //array of Databinding Objs
    this.conditionalDisplay = options.conditionalDisplay;
    this.condDisplayCntrlProps = options.condDisplayCntrlProps;
    this.allowSorting = options.allowSorting;
    this.allowGrouping = options.allowGrouping;
    this.allowFilter = options.allowFilter;
    this.allowPlot = options.allowPlot;
    this.showRowNo = options.showRowNo;
    this.isDeleted = options.isDeleted;
    this.oldName = options.oldName;
    this.isNew = options.isNew;
    this.displayRowCount = options.displayRowCount;
}
Table.prototype = new ControlNew();
Table.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.biTable;
Table.prototype.fnRemoveDatabindObjs = function () {
    this.databindObjs = [];
};
Table.prototype.fnSetDefaults = function () {
    this.columns = [];
    this.allowSorting = "1";
    this.allowGrouping = "1";
    this.allowFilter = "1";
    this.allowPlot = "1";
    this.showRowNo = "1";
    this.displayRowCount = "0";
};
Table.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
//shalini Reset Function
Table.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
    var columns = this.columns;
    this.columns = [];
    i = 0;
    if (columns && $.isArray(columns) && columns.length > 0) {
        for (i = 0; i <= columns.length - 1; i++) {
            var objcolumn = new TableColumn(columns[i]);
            this.fnAddcolumns(objcolumn);
        }
    }
}
Table.prototype.fnAddcolumns = function (column) {
    if (!this.columns || !$.isArray(this.columns)) {
        this.columns = [];
    }
    if (column && column instanceof TableColumn) {
        this.columns.push(column);
    }
};
Table.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        return databindObj.name !== name;
    });
    this.databindObjs = aryNewDatabindObjs;
};
Table.prototype.fnSetDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
Table.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Table.prototype.fnGetColumns = function () {
    return this.columns;
};
// Table.prototype.fnGetBindTypeOfDatabindObj = function () {
// var objDatabindingObject = this.fnGetDatabindingObj();
// var objBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
// if (objDatabindingObject) {
// objBindType = objDatabindingObject.fnGetBindTypeObject()
// }
// return objBindType;
// };
// Table.prototype.fnGetBindTypeValueOfDatabindObj = function () {
// var objDatabindingObject = this.fnGetDatabindingObj();
// var iBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
// if (objDatabindingObject) {
// iBindType = objDatabindingObject.fnGetBindTypeValue()
// }
// return iBindType;
// };
Table.prototype.fnSetColumns = function (columns) {
    this.columns = columns;
};
Table.prototype.fnDeleteColumns = function () {
    this.columns = [];
};
function TableColumn(options/*object*/) {
    if (!options) options = {};
    this.column = options.column;
    this.index = options.index;
    this.header = options.header;
    this.datatype = options.datatype;
    this.displayFormat = options.displayFormat;
    this.dataFormat = options.dataFormat;
    this.prefix = options.prefix;
    this.suffix = options.suffix;
    this.factor = options.factor;
    this.displayRowCount = options.displayRowCount;
}
mFicientIde.BiTableControlHelper = (function () {
    var jqueryKeyForActionBtnSettings = "actionBtnSettings";
    function _fillDatabindingSettingsInHtml(cntrl) {
        if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;

        var objDatabinding = cntrl.fnGetDatabindingObj();

        var objDataObjectCntrl = PROP_JSON_HTML_MAP.biTable.propSheetCntrl.Data.getDataObject();
        if (objDatabinding) {
            var databindObjName = objDatabinding.fnGetCommandName();
            objDataObjectCntrl.control.val(
                databindObjName ? databindObjName : "Select Object");
        }
        else {
            objDataObjectCntrl.control.val("Select Object");
        }

    }
    return {
        fillDatabindingSettingsInHtml: function (cntrl) {
            _fillDatabindingSettingsInHtml(cntrl);
        }
    };
})();

var mfIdeTableControl = "mfIdeTableControl";
