﻿
mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.editableList = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function(control, editableListObj) {
                $(control).val(editableListObj.type.UIName)
            },
            Name: function(control, editableListObj) {
                $(control).val(editableListObj.userDefinedName);
            },
            Description: function(control, editableListObj) {
                $(control).val(editableListObj.description);
            },
            eventHandlers: {
                Type: function(evnt) {
                    var event = evnt;
                },
                Name: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        evnt.target.value = propObject.userDefinedName;
                        return;
                    }
                    //propObject.userDefinedName = $self.val();
                    propObject.fnRename($self.val());
                },
                Description: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.description = $self.val();
                }
            }
        },
        ListBehaviour: {
           Properties: function(control, editableListObj) {
              var aryProperties = editableListObj.fnGetProperties();
              if(aryProperties && $.isArray(aryProperties)){
                  $(control).val(aryProperties.length);
              }
              else{
                $(control).val("0");
            }
           },
           MinItems: function(control, editableListObj) {
               $(control).val(editableListObj.minItems);
           },
           MaxItems: function(control, editableListObj) {
               $(control).val(editableListObj.maxItems);
           },
           EmptyListMessage: function(control, editableListObj,options) {
               var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel,
                   strEmptyListMsg = editableListObj.emptyListMsg;
               $(control).val(strEmptyListMsg); 
               MF_HELPERS.setTextOfTextAreaLabelOfPropSheet(
                   strEmptyListMsg,
                   $textAreaLabel);
               //$(control).val(editableListObj.emptyListMsg);
           },
           AddConfirmation: function(control, editableListObj) {
               $(control).val(editableListObj.addConfirmation);
           },
           eventHandlers: {
               Properties: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    try{
                        editableListHelper.processShowListPropTable(propObject.properties);
                    }
                    catch(error){
                        if (error instanceof MfError){
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                        }else{
                            console.log(error);
                        }
                    } 
               },
               MinItems: function(evnt) {
                   var $self = $(this);
                   var evntData = evnt.data;
                   var propObject = evntData.propObject;
                   propObject.minItems = $self.val();
               },
               MaxItems: function(evnt) {
                   var $self = $(this);
                   var evntData = evnt.data;
                   var propObject = evntData.propObject;
                   propObject.maxItems = $self.val();
               },
               EmptyListMessage: function(evnt) {
                   var $self = $(this);
                   var evntData = evnt.data;
                   var propObject = evntData.propObject;
                   propObject.emptyListMsg = $self.val();
               },
               AddConfirmation: function(evnt) {
                   var $self = $(this);
                   var evntData = evnt.data;
                   var propObject = evntData.propObject;
                   propObject.addConfirmation = $self.val();
               }
           }
        },
        Appearance: {
            eventHandlers: {
             }
        },
        ChildForms: {
            eventHandlers: {
             }
        },
        ItemDisplay: {
            Title: function(control, editableListObj) {
                $(control).val(editableListObj.title);
            },
            AdditionalTitle: function(control, editableListObj) {
                $(control).val(editableListObj.additionalTitle);
            },
            Subtitle: function(control, editableListObj) {
                $(control).val(editableListObj.subTitle);
            },
            AdditionalSubtitle: function(control, editableListObj) {
                $(control).val(editableListObj.additionalSubTitle);
            },
            eventHandlers: {
                Title: function(evnt) {
                    try{
                          var $self = $(this);
                          var evntData = evnt.data;
                          var propObject = evntData.propObject;
                          var editListProps =  propObject.fnGetProperties();
                          if(!editListProps || ($.isArray(editListProps) && editListProps.length === 0)){
                              throw new MfError(["Please add atleast one property for editable list"]);
                          }
                          editableListHelper.processShowEditListTitleSetting();
                    }
                    catch(error){
                        if (error instanceof MfError){
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                        }else{
                            console.log(error);
                        }
                    } 
        
                },
                AdditionalTitle: function(evnt) {
                    try{
                          var $self = $(this);
                          var evntData = evnt.data;
                          var propObject = evntData.propObject;
                          var editListProps =  propObject.fnGetProperties();
                          if(!editListProps || ($.isArray(editListProps) && editListProps.length === 0)){
                              throw new MfError(["Please add atleast one property for editable list"]);
                          }
                          editableListHelper.processShowEditListAdditionalTitleSetting();
                    }
                    catch(error){
                        if (error instanceof MfError){
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                        }else{
                            console.log(error);
                        }
                    } 
                 },
                Subtitle: function(evnt) {
                    try{
                          var $self = $(this);
                          var evntData = evnt.data;
                          var propObject = evntData.propObject;
                          var editListProps =  propObject.fnGetProperties();
                          if(!editListProps || ($.isArray(editListProps) && editListProps.length === 0)){
                              throw new MfError(["Please add atleast one property for editable list"]);
                          }
                          editableListHelper.processShowEditListSubtitleSetting();
                    }
                    catch(error){
                        if (error instanceof MfError){
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                        }else{
                            console.log(error);
                        }
                    }
                    //editableListHelper.processShowEditListSubtitleSetting();
                },
                AdditionalSubtitle: function(evnt) {
                    try{
                          var $self = $(this);
                          var evntData = evnt.data;
                          var propObject = evntData.propObject;
                          var editListProps =  propObject.fnGetProperties();
                          if(!editListProps || ($.isArray(editListProps) && editListProps.length === 0)){
                              throw new MfError(["Please add atleast one property for editable list"]);
                          }
                          editableListHelper.processShowEditListAdditionalSubtitleSetting();
                    }
                    catch(error){
                        if (error instanceof MfError){
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                        }else{
                            console.log(error);
                        }
                    }
                }
            }
        },
        Data: {
            Databinding: function(control, editableListObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if(objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline){
                   strOptions +='<option value="'+MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id+'">None</option>';
                   strOptions += '<option value="'+MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id+'">Offline Database</option>';
                   control.html(strOptions);
                }
                else{
                }
                var objDatabindingObj = editableListObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    $(control).val(objDatabindingObj.fnGetBindTypeValue());
                }
            },
            DataObject: function(control, editableListObj) {
               var objDatabindingObj = editableListObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                        var objPropSheetCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.Data.getDataObject();
                        if (objPropSheetCntrl) {
                            objPropSheetCntrl.control.val(objDatabindingObj.fnGetCommandName());
                            objPropSheetCntrl.wrapperDiv.show();
                        }

                    }
                    else {
                        $(control).val("Select object");
                        objPropSheetCntrl.wrapperDiv.hide();
                    }
                }
            },
            eventHandlers: {
                Databinding: function(evnt) {
                    var evntData = evnt.data;
                    var $DataObjectWrapper = $('#' + evntData.cntrls.DataObject.wrapperDiv);
                    var $dataObjectCntrl = $('#' + evntData.cntrls.DataObject.controlId);
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    if ($self.val() === "0") {
                        $DataObjectWrapper.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        propObject.fnRemoveDatabindObjs();
                    }
                    else{
                        $DataObjectWrapper.show();
                        propObject.fnRemoveDatabindObjs();
                        var dataObject = new DatabindingObj("", [], "0", $self.val(), "", "", "", "", "", {ignoreCache:false});
                        propObject.fnAddDatabindObj(dataObject);
                    }
                    // if ($self === "1") {
                    //     $DataObject.show();
                    //     bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                    // }
                    // else if ($self.val() === "2") {
                    //     $DataObject.show();
                    //     bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                    // }
                    // else if ($self.val() === "5") {
                    //     $DataObject.show();
                    //     bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                    // }
                   // var objPropSheetCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.Data.getDataObject();
                    //objPropSheetCntrl.control.val("Select Object");
                    $dataObjectCntrl.val("Select Object");
                },
                DataObject: function(evnt) {
                    var propObject = evnt.data.propObject;
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var intlJson=[],
                        isEdit = false;
                    try{
                    var aryProperties = propObject.properties;
                    if(!aryProperties || ($.isArray(aryProperties) && aryProperties.length === 0)){
                        throw new MfError(['Please add atleast one property to the editable list.']);    
                    }
                    if(propObject && propObject.databindObjs 
                        && $.isArray(propObject.databindObjs)){
                       $.each(propObject.databindObjs, function (index,value) {
                            var objDatabindObj = jQuery.extend(true, {}, value);
                            switch (value.bindObjType) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject,objDatabindObj);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 500, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 500, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 500, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    
                                    intlJson =  MF_HELPERS
                                                .intlsenseHelper
                                                .getControlDatabindingIntellisenseJson(
                                                    objCurrentForm,propObject.id,true
                                                );
                                    if( objDatabindObj != null 
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ){
                                            isEdit = true;
                                    }
                                    
                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType:MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST,
                                        databindObject:objDatabindObj,
                                        objectType:MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit:isEdit,
                                        intlsJson : intlJson,
                                        control :propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object',420, false);
                                break;    
                            }
                        });
                    }
                    }
                    catch(error){
                        if (error instanceof MfError){
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                        }else{
                            console.log(error);
                        }
                    }
                }
            }
        },
        Behaviour: {
            Display: function(control, editableListObj) {
                //$(control).val(appObj.type.UIName)
            },
            Conditions: function(control, editableListObj) {
                //$(control).val(editableListObj.userDefinedName);
            },
            eventHandlers: {
                Display: function(evnt) {
                },
                Conditions: function(evnt) {
                }
            }
        },
        AddItem: {
            AllowItemAddition: function(control, editableListObj) {
                $(control).val(editableListObj.allowItemAddition);
            },
            AddButtonText: function(control, editableListObj,options) {
                var allowItemAddition = editableListObj.fnGetAllowItemAdditionValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemAddition === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                $(control).val(editableListObj.addBtnText);
            },
            NextButtonTextAdd: function(control, editableListObj,options) {
                var allowItemAddition = editableListObj.fnGetAllowItemAdditionValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemAddition === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                $(control).val(editableListObj.addNextBtnText);
            },
            AddFormTitle: function(control, editableListObj,options) {
                var allowItemAddition = editableListObj.fnGetAllowItemAdditionValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemAddition === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                $(control).val(editableListObj.addFormTitle);
            },
            AddForm: function(control, editableListObj,options) {
                var allowItemAddition = editableListObj.fnGetAllowItemAdditionValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemAddition === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                //$(control).val(appObj.type.UIName)
            },
            eventHandlers: {
                AllowItemAddition: function(evnt) {
                     var $self = $(this);
                     var evntData = evnt.data;
                     var propObject = evntData.propObject,
                        addButtonTextCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.AddItem.getAddButtonText(),
                        nextButtonTextAdd  =PROP_JSON_HTML_MAP.editableList.propSheetCntrl.AddItem.getNextButtonTextAdd(),
                        addFormTitle = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.AddItem.getAddFormTitle(),
                        addForm = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.AddItem.getAddForm();
                     propObject.fnSetAllowItemAdditionValue($self.val());
                     if($self.val() === "1"){
                        addButtonTextCntrl.wrapperDiv.show();
                        nextButtonTextAdd.wrapperDiv.show();
                        addFormTitle.wrapperDiv.show();
                        addForm.wrapperDiv.show();
                     }
                     else{
                         addButtonTextCntrl.wrapperDiv.hide();
                         nextButtonTextAdd.wrapperDiv.hide();
                         addFormTitle.wrapperDiv.hide();
                         addForm.wrapperDiv.hide();
                         propObject.fnDeleteAddNextButtonText();
                         propObject.fnDeleteAddFormTitle();
                         propObject.fnDeleteAddButtonText();
                         propObject.fnDeleteChildFormByType(MF_IDE_CONSTANTS.CHILD_FORM_TYPE.ADD);
                         addFormTitle.control.val("Edit Item");
                         addForm.control.val("Click to Edit");
                         nextButtonTextAdd.control.val("");
                         addButtonTextCntrl.control.val("");
                     }
                },
                AddButtonText: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.addBtnText = $self.val();
                },
                NextButtonTextAdd: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.addNextBtnText = $self.val();
                },
                AddFormTitle: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.addFormTitle = $self.val();
                },
                AddForm: function(evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    if(form.name == undefined || form.name == "" || form.name == null){
                        showMessage('Form name cannot be less than 3 characters.', DialogType.Error);
                        return;
                    }
                    if(propObject.properties == undefined || propObject.properties.length <= 0){
                        showMessage('Please add at least one property in Editable List.', DialogType.Error);
                        return;
                    }
                    childForm = new Form();
                    var createNewForm = true;
                    if(propObject.childForms != undefined && propObject.childForms.length > 0){
                        $.each(propObject.childForms, function(){
                            if(this.childFormType.idPrefix == mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.ADD.idPrefix){
                                childForm = this;
                                createNewForm = false;
                                editChildForm(propObject, childForm);
                            }
                        });
                    }
                    if(createNewForm)  {
                            childForm = new Form();
                            childForm.childFormType = mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.ADD;
                            addChildForm(propObject);
                    }
                }
            }
        },
        EditItem: {
            AllowItemEditing: function(control, editableListObj) {
                $(control).val(editableListObj.allowItemEditing);
            },
            NextButtonTextEdit: function(control, editableListObj,options) {
                var allowItemEditing = editableListObj.fnGetAllowItemEditingValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemEditing === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                $(control).val(editableListObj.editNextBtnText);
            },
            EditFormTitle: function(control, editableListObj,options) {
                var allowItemEditing = editableListObj.fnGetAllowItemEditingValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemEditing === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                $(control).val(editableListObj.editFormTitle);
            },
            EditForm: function(control, editableListObj,options) {
                var allowItemEditing = editableListObj.fnGetAllowItemEditingValue();
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if(allowItemEditing === "1"){
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else{
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                //$(control).val(appObj.userDefinedName);
            },
            eventHandlers: {
               AllowItemEditing: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject,
                       nextButtonTextEdit  =PROP_JSON_HTML_MAP.editableList.propSheetCntrl.EditItem.getNextButtonTextEdit(),
                       editFormTitle = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.EditItem.getEditFormTitle(),
                       editForm = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.EditItem.getEditForm();
                    propObject.fnSetAllowItemEditingValue($self.val());
                    if($self.val() === "1"){
                       nextButtonTextEdit.wrapperDiv.show();
                       editFormTitle.wrapperDiv.show();
                       editForm.wrapperDiv.show();
                    }
                    else{
                        nextButtonTextEdit.wrapperDiv.hide();
                        editFormTitle.wrapperDiv.hide();
                        editForm.wrapperDiv.hide();
                        propObject.fnDeleteEditNextButtonText();
                        propObject.fnDeleteEditFormTitle();
                        propObject.fnDeleteChildFormByType(MF_IDE_CONSTANTS.CHILD_FORM_TYPE.EDIT);
                        editFormTitle.control.val("Edit Item");
                        editForm.control.val("Click to Edit");
                        nextButtonTextEdit.control.val("");
                    }
                    //propObject.allowItemEditing = $self.val();
               },
               NextButtonTextEdit: function(evnt) {
                   var $self = $(this);
                   var evntData = evnt.data;
                   var propObject = evntData.propObject;
                   propObject.editNextBtnText = $self.val();
               },
               EditFormTitle: function(evnt) {
                   var $self = $(this);
                  var evntData = evnt.data;
                  var propObject = evntData.propObject;
                  propObject.editFormTitle = $self.val();
               },
               EditForm: function(evnt) {
                   var evntData = evnt.data;
                   var propObject = evntData.propObject;
                   if(form.name == undefined || form.name == "" || form.name == null){
                       showMessage('Form name cannot be less than 3 characters.', DialogType.Error);
                       return;
                   }
                   if(propObject.properties == undefined || propObject.properties.length <= 0){
                       showMessage('Please add at least one property in Editable List.', DialogType.Error);
                       return;
                   }
                   childForm = new Form();
                   var createNewForm = true;
                   if(propObject.childForms != undefined && propObject.childForms.length > 0){
                       $.each(propObject.childForms, function(){
                           if(this.childFormType.idPrefix == mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.EDIT.idPrefix){
                               childForm = this;
                               createNewForm = false;
                               editChildForm(propObject, childForm);
                           }
                       });
                   }
                   if(createNewForm) {
                           childForm.childFormType = mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.EDIT;
                           addChildForm(propObject);
                   }
               }
            }
        },
        ViewItem: {
            ViewFormTitle: function(control, editableListObj) {
                $(control).val(editableListObj.viewFormTitle);
            },
            ViewForm: function(control, editableListObj) {
                //$(control).val(editableListObj.description);
            },
            eventHandlers: {
                ViewFormTitle: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.viewFormTitle = $self.val();
                    
                },
                ViewForm: function(evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    if(form.name == undefined || form.name == "" || form.name == null){
                        showMessage('Form name cannot be less than 3 characters.', DialogType.Error);
                        return;
                    }
                    if(propObject.properties == undefined || propObject.properties.length <= 0){
                        showMessage('Please add at least one property in Editable List.', DialogType.Error);
                        return;
                    }
                    childForm = new Form();
                    var createNewForm = true;
                    if(propObject.childForms != undefined && propObject.childForms.length > 0){
                        $.each(propObject.childForms, function(){
                            if(this.childFormType.idPrefix == mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.VIEW.idPrefix){
                                childForm = this;
                                createNewForm = false;
                                editChildForm(propObject, childForm);
                            }
                        });
                    }
                    if(createNewForm) {
                            childForm.childFormType = mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.VIEW;
                            addChildForm(propObject);
                    }
                }
            }
        },
        DeleteItem: {
            AllowItemDelete: function(control, editableListObj) {
                $(control).val(editableListObj.allowItemDelete);
            },
            eventHandlers: {
                AllowItemDelete: function(evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.allowItemDelete = $self.val();
                }
            }
        }
    },
    propSheetCntrl:(function(){
        function _getDivControlHelperData(){
            return $('#'+MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getListBehaviorPropertiesCntrl(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "ListBehaviour","Properties");
            }
            return objControl;
        }
        function _getItemDispTitleCntrl(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "ItemDisplay","Title");
            }
            return objControl;
        }
        function _getItemDispAdditionalTitleCntrl(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "ItemDisplay","AdditionalTitle");
            }
            return objControl;
        }
        function _getItemDispSubtileCntrl(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "ItemDisplay","Subtitle");
            }
            return objControl;
        }
        function _getItemDispAdditionalSubtitleCntrl(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "ItemDisplay","AdditionalSubtitle");
            }
            return objControl;
        }
        function _getDataDataObjectCntrl(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "Data","DataObject");
            }
            return objControl;
        }
        function _getAddItemAddButtonText(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "AddItem","AddButtonText");
            }
            return objControl;
        }
        function _getAddItemNextButtonTextAdd(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "AddItem","NextButtonTextAdd");
            }
            return objControl;
        }
        function _getAddItemAddFormTitle(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "AddItem","AddFormTitle");
            }
            return objControl;
        }
        function _getAddItemAddForm(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "AddItem","AddForm");
            }
            return objControl;
        }
        function _getEditItemNextButtonTextEdit(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "EditItem","NextButtonTextEdit");
            }
            return objControl;
        }
        function _getEditItemEditFormTitle(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "EditItem","EditFormTitle");
            }
            return objControl;
        }
        function _getEditItemEditForm(){
            var divData = _getDivControlHelperData();
            var objControl = null;
            if(divData){
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.propPluginPrefix,
                    "EditItem","EditForm");
            }
            return objControl;
        }
        return {
            ListBehaviour :{
                getProperties :_getListBehaviorPropertiesCntrl
            },
            ItemDisplay :{
                getTitle :_getItemDispTitleCntrl,
                getAdditionalTitle:_getItemDispAdditionalTitleCntrl,
                getSubtitle:_getItemDispSubtileCntrl,
                getAdditionalSubtitle:_getItemDispAdditionalSubtitleCntrl
            },
            Data:{
                getDataObject:_getDataDataObjectCntrl
            },
            AddItem:{
                getAddButtonText:_getAddItemAddButtonText,
                getNextButtonTextAdd :_getAddItemNextButtonTextAdd,
                getAddFormTitle : _getAddItemAddFormTitle,
                getAddForm : _getAddItemAddForm
            },
            EditItem:{
               getNextButtonTextEdit :_getEditItemNextButtonTextEdit,
               getEditFormTitle : _getEditItemEditFormTitle,
               getEditForm : _getEditItemEditForm
            }
        };
    })()
};
mFicientIde.PropertySheetJson.EditableList = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST,
    "groups": [
        {
            "name": "Control Properties",
            "prefixText": "ControlProperties",
            "hidden": true,
            "properties": [{
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [
    
                ],
                "customValidations": [
    
                ],
                "events": [
    
                ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Editable List",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [
    
                ],
                "customValidations": [
    
                ],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.ControlProperties.eventHandlers.Name,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [
    
                ],
                "customValidations": [
    
                ],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.ControlProperties.eventHandlers.Description,
                    "context": "",
                    "arguments": {}
                }
    
                ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
            ]
        },
        {
            "name": "List Behaviour",
            "prefixText": "ListBehaviour",
            "hidden": false,
            "properties": [
                {
                "text": "Properties",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Properties",
                "defltValue": "0",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "click",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.ListBehaviour.eventHandlers.Properties,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "disabled": false,
                    "hidden": false,
                    "defltValue": "0"
                }
            },
            {
                "text": "Min Items",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinItems",
                "noOfLines": "0",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.ListBehaviour.eventHandlers.MinItems,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }},
            {
                "text": "Max Items",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxItems",
                "noOfLines": "0",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.ListBehaviour.eventHandlers.MaxItems,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }},
             {
                 "text": "Empty List Message",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                 "prefixText": "EmptyListMessage",
                 "noOfLines": "0",
                 "validations": [],
                 "customValidations": [],
                 "events": [{
                     "name": "change",
                     "func": PROP_JSON_HTML_MAP.editableList.groups.ListBehaviour.eventHandlers.EmptyListMessage,
                     "context": "",
                     "arguments": {}
                 }],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "maxlength": 50,
                     "disabled": false,
                     "defltValue": "",
                     "hidden": false
                 }
             },
             {
                 "text": "Add/Edit/Delete Confirm",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "AddConfirmation",
                 "defltValue": "value",
                 "validations": [],
                 "customValidations": [],
                 "events": [{
                     "name": "change",
                     "func": PROP_JSON_HTML_MAP.editableList.groups.ListBehaviour.eventHandlers.AddConfirmation,
                     "context": "",
                     "arguments": {}
                 }],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false,
                     "defltValue":"0"
                 },
                 "options": [{
                     "text": "Yes",
                     "value": "1"
                 }, {
                     "text": "No",
                     "value": "0"
                 }]
             }
                
                
            ]
        },
        {
            "name": "Appearance",
            "prefixText": "Appearance",
            "hidden": true,
            "properties": [
              ]
        },
        {
            "name": "ChildForms",
            "prefixText": "ChildForms",
            "hidden": true,
            "properties": [
               ]
        },
        {
            "name": "Item Display",
            "prefixText": "ItemDisplay",
            "hidden": false,
            "properties": [
                {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Title",
                "defltValue": "0",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "click",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.ItemDisplay.eventHandlers.Title,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "disabled": false,
                    "hidden": false
                }
                },
                {
                    "text": "Subtitle",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                    "prefixText": "AdditionalTitle",
                    "defltValue": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.ItemDisplay.eventHandlers.AdditionalTitle,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "selectValueBy": "",
                    "init": {
                        "disabled": false,
                        "hidden": false
                    }
                }, {
                    "text": "Info",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                    "prefixText": "Subtitle",
                    "defltValue": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.ItemDisplay.eventHandlers.Subtitle,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "selectValueBy": "",
                    "init": {
                        "disabled": false,
                        "hidden": false
                    }
                }, {
                    "text": "Additional Info",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                    "prefixText": "AdditionalSubtitle",
                    "defltValue": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.ItemDisplay.eventHandlers.AdditionalSubtitle,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "selectValueBy": "",
                    "init": {
                        "disabled": false,
                        "hidden": false
                    }
                }
            ]
        },
        {
            "name": "Data",
            "prefixText": "Data",
            "hidden": false,
            "properties": [
              {
                  "text": "Databinding",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Databinding",
                  "defltValue": "0",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.Data.eventHandlers.Databinding,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DataObject",
                                 rtrnPropNm: "DataObject"
                             }
                          ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                    {
                        "text": "None",
                        "value": "0"
                    },
                    {
                        "text": "Database Object",
                        "value": "1"
                    },
                    {
                        "text": "Web Service Object",
                        "value": "2"
                    },
                    {
                        "text": "OData Object",
                        "value": "5"
                    },
                    {
                         "text": "Offline Database",
                         "value": "6"
                     }
                 ]
              },
              {
                  "text": "Data Object",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "DataObject",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [
                    ],
                  "customValidations": [
                  ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.Data.eventHandlers.DataObject,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Object",
                      "hidden": true
                  }
              }
           ]
        },
        {
            "name": "Behaviour",
            "prefixText": "Behaviour",
            "hidden": true,
            "properties": [
              {
                  "text": "Display",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Display",
                  "defltValue": "1",
                  "validations": [
                ],
                  "customValidations": [
    
                 ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.Behaviour.eventHandlers.Display,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "Conditions",
                                 rtrnPropNm: "Conditions"
                             }
                          ]
                        }
                    }
    
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": true,
                      "hidden": true,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Always",
                        "value": "1"
                    },
                    {
                        "text": "Conditional",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Conditions",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "Conditions",
                  "disabled": false,
                  "noOfLines": "0",
                  "validations": [
    
                 ],
                  "customValidations": [
                    {
                        "func": "functionName",
                        "context": "context"
                    }
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.Behaviour.eventHandlers.ConditionalDisplay,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Conditions",
                      "hidden": true
                  }
              }
           ]
        },
        {
            "name": "Add Item",
            "prefixText": "AddItem",
            "hidden": false,
            "properties": [
                {
                "text": "Allow",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "AllowItemAddition",
                "defltValue": "value",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.editableList.groups.AddItem.eventHandlers.AllowItemAddition,
                    "context": "",
                    "arguments": {
                        "cntrls":[
                          {
                              grpPrefText:"AllowItemAddition",
                              cntrlPrefText:"AddButtonText",
                              rtrnPropNm : "AddButtonText"
                          },
                          {
                              grpPrefText:"AllowItemAddition",
                              cntrlPrefText:"NextButtonTextAdd",
                              rtrnPropNm : "NextButtonTextAdd"
                          },
                          {
                              grpPrefText:"AllowItemAddition",
                              cntrlPrefText:"AddFormTitle",
                              rtrnPropNm : "AddFormTitle"
                          },
                          {
                              grpPrefText:"AllowItemAddition",
                              cntrlPrefText:"AddForm",
                              rtrnPropNm : "AddForm"
                          }
                         ]}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [{
                    "text": "Yes",
                    "value": "1"
                }, {
                    "text": "No",
                    "value": "0"
                }]
                    
                },
                {
                    "text": "Add Button Text",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                    "prefixText": "AddButtonText",
                    "noOfLines": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.AddItem.eventHandlers.AddButtonText,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "init": {
                        "maxlength": 50,
                        "disabled": false,
                        "defltValue": "",
                        "hidden": false
                    }
                },
                {
                    "text": "Next Button Text",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                    "prefixText": "NextButtonTextAdd",
                    "noOfLines": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.AddItem.eventHandlers.NextButtonTextAdd,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "init": {
                        "maxlength": 50,
                        "disabled": false,
                        "defltValue": "",
                        "hidden": false
                    }
                },
                {
                    "text": "Title",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                    "prefixText": "AddFormTitle",
                    "noOfLines": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.AddItem.eventHandlers.AddFormTitle,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "init": {
                        "maxlength": 50,
                        "disabled": false,
                        "defltValue": "Add Item",
                        "hidden": false
                    }
                },
                {
                    "text": "Form",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                    "prefixText": "AddForm",
                    "defltValue": "0",
                    "validations": [],
                    "customValidations": [],
                    "events": [{
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.editableList.groups.AddItem.eventHandlers.AddForm,
                        "context": "",
                        "arguments": {}
                    }],
                    "CntrlProp": "",
                    "HelpText": "",
                    "selectValueBy": "",
                    "init": {
                        "disabled": false,
                        "hidden": false,
                        "defltValue":"Click to Edit"
                    }
                }
           ]
        },
        {
            "name": "Edit Item",
            "prefixText": "EditItem",
            "hidden": false,
            "properties": [
              {
                  "text": "Allow",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AllowItemEditing",
                  "defltValue": "1",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.editableList.groups.EditItem.eventHandlers.AllowItemEditing,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "finalEditInit": {
                          "passForFinalEdit": false,
                          "propName": ""
                      }
                  },
                  "options": [{
                      "text": "No",
                      "value": "0"
                  }, {
                      "text": "Yes",
                      "value": "1"
                  }]
              },
              {
                  "text": "Next Button Text",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "NextButtonTextEdit",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.editableList.groups.EditItem.eventHandlers.NextButtonTextEdit,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Title",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "EditFormTitle",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.editableList.groups.EditItem.eventHandlers.EditFormTitle,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": "Edit Item",
                      "hidden": false
                  }
              },
              {
                  "text": "Form",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "EditForm",
                  "defltValue": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.editableList.groups.EditItem.eventHandlers.EditForm,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "Click to Edit"
                  }
              }
           ]
        },
        {
            "name": "View Item",
            "prefixText": "ViewItem",
            "hidden": false,
            "properties": [
              {
                  "text": "Title",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "ViewFormTitle",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.editableList.groups.ViewItem.eventHandlers.ViewFormTitle,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": "View Item",
                      "hidden": false
                  }
              },
              {
                  "text": "Form",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "ViewForm",
                  "defltValue": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.editableList.groups.ViewItem.eventHandlers.ViewForm,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "disabled": false,
                      "hidden": false,
                      "defltValue":"Click to Edit"
                  }
              }
           ]
        },
        {
            "name": "Delete Item",
            "prefixText": "DeleteItem",
            "hidden": false,
            "properties": [
              {
              "text": "Allow",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "AllowItemDelete",
              "defltValue": "1",
              "validations": [],
              "customValidations": [],
              "events": [{
                  "name": "change",
                  "func": PROP_JSON_HTML_MAP.editableList.groups.DeleteItem.eventHandlers.AllowItemDelete,
                  "context": "",
                  "arguments": {}
              }],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false,
                  "finalEditInit": {
                      "passForFinalEdit": false,
                      "propName": ""
                  }
              },
              "options": [{
                  "text": "No",
                  "value": "0"
              }, {
                  "text": "Yes",
                  "value": "1"
              }]
                  
              }
           ]
        }
    ]
};
//CLASSES
function EditableList(options) {
    if (!options) options = {};
    this.id = options.id;
    this.userDefinedName = options.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.EditableList;
    this.description = options.description;
    this.type = options.type;
    
    this.addBtnText = options.addBtnText;
    this.addNextBtnText = options.addNextBtnText; 
    this.editNextBtnText = options.editNextBtnText;
    this.addFormTitle = options.addFormTitle;//Default Add Item
    this.editFormTitle = options.editFormTitle;//Default Edit Item
    this.viewFormTitle = options.viewFormTitle;//Default View Item
    this.emptyListMsg = options.emptyListMsg;
    
    this.title = options.title;
    this.titlePrefix = options.titlePrefix;
    this.titleSuffix = options.titleSuffix;
    this.titleMultFactor = options.titleMultFactor;
    
    this.additionalTitle = options.additionalTitle;
    this.additionalTitlePrefix = options.additionalTitlePrefix;
    this.additionalTitleSuffix = options.additionalTitleSuffix;
    this.additionalTitleMultFactor = options.additionalTitleMultFactor;
    
    this.subTitle = options.subTitle;
    this.subTitlePrefix = options.subTitlePrefix;
    this.subTitlePrefixSuffix = options.subTitleSuffix;
    this.subTitleMultFactor = options.subTitleMultFactor;
    
    this.additionalSubTitle = options.additionalSubTitle;
    this.additionalSubTitlePrefix = options.subTitlePrefix;
    this.additionalSubTitleSuffix = options.additionalSubTitle;
    this.additionalSubTitleMultFactr = options.additionalSubTitleMultFactr;
    
    this.allowItemAddition = options.allowItemAddition;//Default 0
    this.allowItemEditing = options.allowItemEditing;//Default 0
    this.allowItemDelete = options.allowItemDelete;//Default 0
    this.minItems = options.minItems;//Default 1
    this.maxItems = options.maxItems;//Default 0
    this.addConfirmation = options.addConfirmation;//Default 0. Confirmation  fro child form
    
    this.databindObjs = options.databindObjs;//array of Databinding Objs
    this.conditionalDisplay = options.conditionalDisplay;
    this.condDisplayCntrlProps = options.condDisplayCntrlProps;
    
    this.properties = options.properties;//array of EditableListItem

    this.childForms = options.childForms;

    this.isDeleted = options.isDeleted;
    this.oldName = options.oldName;
    this.isNew = options.isNew;
}
EditableList.prototype = new ControlNew();
EditableList.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.editableList;
EditableList.prototype.fnSetProperties = function(listProperties/*EditableListItem Array*/){
  this.properties = listProperties;  
};
EditableList.prototype.fnGetProperties = function(){
  return this.properties;  
};
EditableList.prototype.fnAddProperty=function(prop/*EditableListItem*/){
    if (!this.properties || !$.isArray(this.properties)) {
        this.properties = [];
    }
    if(prop && prop instanceof EditableListItem){
        this.properties.push(prop);
    }
    
};
EditableList.prototype.fnAddChildForm =function(childForm/*ChildForm*/){
    var valid = true;
    if(childForm.childFormType.idPrefix != mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.VIEW.idPrefix){
        if(childForm.bindedProps != undefined && childForm.bindedProps.length == this.properties.length)
            valid = true;
        else
            valid = false;
    }
    if(valid){
        if (!this.childForms || !$.isArray(this.childForms)) {
            this.childForms = [];
        }
        var _this = this;

        if(childForm && childForm instanceof Form){
            if(this.childForms != undefined && this.childForms.length > 0){
                $.each(this.childForms, function(){
                    if(this.childFormType == childForm.childFormType){
                        _this.fnDeleteChildFormObj(this.id);
                    }
                });
            }
        }

        if(childForm && childForm instanceof Form){
            this.childForms.push(childForm);
        }
        return true;
    }
    else{
        showMessage('All EditableList Properties are not linked to a control.', DialogType.Error);
        return false;
    }
};
EditableList.prototype.fnDeleteChildFormObj = function (childFormId) {
    var aryNewChildForms = $.grep(this.childForms, function (childForm) {
       return childForm.id !== childFormId;
    });
    this.childForms = aryNewChildForms;
};
EditableList.prototype.fnGetChildForms = function(){
  return this.childForms;  
};
EditableList.prototype.fnSetChildForms = function(childForms){
   this.childForms = childForms;  
};
EditableList.prototype.fnDeleteChildFormByType = function (type/*CONSTANTS.CHILD_FORM_TYPE*/) {
    var aryChildForms = this.fnGetChildForms();
    if(aryChildForms && $.isArray(aryChildForms) && aryChildForms.length>0){
        aryChildForms = $.grep(aryChildForms,function(value,index){
                return value.childFormType !== type;
        });
        this.fnSetChildForms(aryChildForms);
    }
    else{
        this.fnSetChildForms([]);
    }
};
//shalini Reset Function
EditableList.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }

    var properties = this.properties;
    this.properties = [];
    i = 0;
    if (properties && $.isArray(properties) && properties.length > 0) {
        for (i = 0; i <= properties.length - 1; i++) {
            var objProperty = new EditableListItem(properties[i]);
            this.fnAddProperty(objProperty);
        }
    }

    var childForms = this.childForms;
    this.childForms = [];
    i = 0;
    if (childForms && $.isArray(childForms) && childForms.length > 0) {
        for (i = 0; i <= childForms.length - 1; i++) {
            var objChildForm = new Form(childForms[i]);
            objChildForm.fnResetObjectDetails();
            this.fnAddChildForm(objChildForm);
        }
    }
}
//-------
EditableList.prototype.fnGetAllPropNames= function(){
    var aryPropNames = [];
    if(this.properties && $.isArray(this.properties) && this.properties.length>0){
        for(var i=0;i<= this.properties.length-1;i++){
            var prop = this.properties[i];
            aryPropNames.push(prop.name);
        }
    }
    return aryPropNames;
};
EditableList.prototype.fnProcessPropNameRemoved=function(){
    //Think this is the best possible method. 
    //Consider the situatuion if someone deletes prop name and adds new
    //prop with same name.
    //so checking after save is the best option. 
    var properties = this.properties;
    if(properties && $.isArray(properties) && properties.length>0){
        var aryAllPropNames = this.fnGetAllPropNames();
        if($.inArray(this.title,aryAllPropNames) === -1){
            this.title ="";
        }
        if($.inArray(this.additionalTitle,aryAllPropNames) === -1){
            this.additionalTitle ="";
        }
        if($.inArray(this.subTitle,aryAllPropNames) === -1){
            this.subTitle ="";
        }
        if($.inArray(this.additionalSubTitle,aryAllPropNames) === -1){
            this.additionalSubTitle ="";
        }
        //REMOVE FROM DATABINDING VALUES
        //THE PROPERTY IS ALREADY REMOVED. THERE IS NO DEPENDANCY WITH DATABINDING. 
        //THE MAPPED COL IS IN PROPERTY IT WILL BE REMOVED AUTOMATICAALY
    }
    else{
        this.title = "";
        this.additionalTitle = "";
        this.subTitle = "";
        this.additionalSubTitle ="";
        //TODO REMOVE DATABINDING VALUES
    }
};
EditableList.prototype.fnRemoveDatabindObjs = function(){
    this.databindObjs = [];
};
EditableList.prototype.fnAddDatabindObj = function(databindObj){
    if(!this.databindObjs || !$.isArray(this.databindObjs)){
        this.databindObjs = [];
    }
    if(databindObj && databindObj instanceof DatabindingObj){
        this.databindObjs.push(databindObj);
    }
};
EditableList.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
       return databindObj.name !== name;
    });
    this.databindObjs = aryNewDatabindObjs;
};
EditableList.prototype.fnSetDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
EditableList.prototype.fnGetPropertyByName = function (name) {
    var aryListItems =[];
    if(this.properties && $.isArray(this.properties)){
        aryListItems= $.grep(this.properties, function (property) {
          return property.name === name;
        });
    }
    return aryListItems[0];//prop name is unique only one value would be foound. 
};
EditableList.prototype.fnDeleteMappedColOfAllProps = function (name) {
    if(this.properties && $.isArray(this.properties) && this.properties.length>0){
       for(var i=0;i<= this.properties.length-1;i++){
           var property = this.properties[i];
           property.fnClearMappedDataCol();
       }
    }
};
EditableList.prototype.fnGetDatabindingObj = function(){
    return this.databindObjs && this.databindObjs[0];//only one element in array
};
EditableList.prototype.fnGetAllowItemAdditionValue = function(){
    return this.allowItemAddition;
};
EditableList.prototype.fnSetAllowItemAdditionValue = function(value){
     this.allowItemAddition = value;
};
EditableList.prototype.fnGetAllowItemEditingValue = function(){
    return this.allowItemEditing;
};
EditableList.prototype.fnSetAllowItemEditingValue = function(value){
     this.allowItemEditing = value;
};

EditableList.prototype.fnGetEditNextButtonText = function(){
  return this.editNextBtnText;   
};
EditableList.prototype.fnSetEditNextButtonText = function(value){
   this.editNextBtnText = value;  
};
EditableList.prototype.fnDeleteEditNextButtonText = function(){
   this.fnSetEditNextButtonText("")  ;
};
EditableList.prototype.fnGetEditFormTitle = function(){
  return this.editFormTitle;   
};
EditableList.prototype.fnSetEditFormTitle = function(value){
   this.editFormTitle = value;  
};
EditableList.prototype.fnDeleteEditFormTitle = function(){
   this.fnSetEditFormTitle("Edit Item");
};


EditableList.prototype.fnGetAddNextButtonText = function(){
  return this.addNextBtnText;   
};
EditableList.prototype.fnSetAddNextButtonText = function(value){
   this.addNextBtnText = value;  
};
EditableList.prototype.fnDeleteAddNextButtonText = function(){
   this.fnSetAddNextButtonText("")  ;
};
EditableList.prototype.fnGetAddButtonText = function(){
  return this.addBtnText;   
};
EditableList.prototype.fnSetAddButtonText = function(value){
   this.addBtnText = value;  
};
EditableList.prototype.fnDeleteAddButtonText = function(){
   this.fnSetAddButtonText("")  ;
};
EditableList.prototype.fnGetAddFormTitle = function(){
  return this.addFormTitle;   
};
EditableList.prototype.fnSetAddFormTitle = function(value){
   this.addFormTitle = value;  
};
EditableList.prototype.fnDeleteAddFormTitle = function(){
   this.fnSetAddFormTitle("Edit Item");
};
EditableList.prototype.fnRename = function (name) {
    if (!this.fnIsNewlyAdded() &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
EditableList.prototype.fnIsNewlyAdded = function () {
    return this.isNew;
};


function EditableListItem(options) {
    if(!options)options = {};
    this.name = options.name;
    this.type = options.type;
    this.mappedDataCol = options.mappedDataCol;
    this.unique = options.unique;
}
EditableListItem.prototype.fnClearMappedDataCol = function (){
    this.mappedDataCol = "";
};
EditableListItem.prototype.fnGetName = function (){
    return this.name;
};
//CLASSES
var editableListHelper =(function(){
    var EditListItemDispType ={
        title :"title",
        additionalTitle :"additionalTitle",
        subTitle :"subTitle",
        additionalSubtitle:"additionalSubtitle"
    };
    var dataKeyItemDispSetting = "dataKeyItemDispSetting";
    var _editableListPropsHelper = (function(){
        function _getPropsSettingTable(){
            return $('#tblEditListProps');
        }
        
        function _getColumnsOfHtmlTblRow(isAdditionRow){
            var strHtml = "";
              strHtml += '<td class="w_90p" ><input  type="checkbox" class="PrimaryKey"></td>';
              strHtml += '<td class="w_180p" ><input  type="text" maxlength="20" class="ItemName w_180p" onkeydown=\"return enterKeyFilter(event);\"></td>';
              strHtml += '<td class="w_180p">'+_getItemTypeDropDown()+'</td>';
              if(!isAdditionRow){
                strHtml += "<td><span class=\"crossImg16x16\"></span></td>";
              }
              else{
                 strHtml += "<td><span class=\"addImg16x16\"></span></td>"; 
              }
              strHtml +='</tr>';
           return strHtml;
        }
        function _getDtlsTableRowWithCross() {
            var strHtml = '<tr>';
            strHtml += _getColumnsOfHtmlTblRow(false);
            strHtml += '</tr>';
            return strHtml;
        }
        function _getDtlsTableRowOnlyAdd() {
            var strHtml = '<tr class="AdditionRow">';
            strHtml += _getColumnsOfHtmlTblRow(true);
            strHtml += '</tr>';
            return strHtml;
        }
        
        function _getItemTypeDropDown(){
            var strDropDown = '<select class="ItemType w_180p" onkeydown=\"return enterKeyFilter(event);\">';
            strDropDown += '<option value="0">String</option>';
            strDropDown += '<option value="1">Integer</option>';
            strDropDown += '<option value="2">Decimal</option>';
            strDropDown += '</select>';
            return strDropDown;
        }
        function _formHtml(listProperties){
            var $tblEditListProps = _getPropsSettingTable();
            $tblEditListProps.find('tbody tr').remove();
            var strRowHtmlToAdd = "";
            if(listProperties && 
                $.isArray(listProperties) && 
                listProperties.length>0){
                
                for(var i=0;i<=listProperties.length-1;i++){
                    strRowHtmlToAdd += _getDtlsTableRowWithCross();
                }
                if(strRowHtmlToAdd){
                    $tblEditListProps.find('tbody').html(strRowHtmlToAdd);
                }
            }
        }
        function _addAdditionRowToTable() {
            var $mappingTable = _getPropsSettingTable();
            var $tbody = $mappingTable.find('tbody');
            var $tr = $tbody.find('tr');
    
            if ($tr.length > 0) {
                $tbody.find('tr:last').after(_getDtlsTableRowOnlyAdd());
            }
            else {
                $tbody.append(_getDtlsTableRowOnlyAdd());
            }
        }
        
        function _fillDataInHtml(listProperties){
           if(listProperties && 
                $.isArray(listProperties) && 
                listProperties.length>0) {
                
                var $propSettingTbl = _getPropsSettingTable();
                var $tableRows = $propSettingTbl.find('tbody tr').not('.AdditionRow');
                $.each(listProperties,function(index,listProp){
                    var objListProp = listProp;
                    var $row = $($tableRows[index]);
                    // if(objListProp.unique === "1"){
                    //     $row.find('.PrimaryKey').prop('checked',true);    
                    //}
                    //else{
                    // }
                    // $row.find('.ItemName').val(objListProp.name);
                    //$row.find('.ItemType').val(objListProp.type);
                    //     $row.data(MF_IDE_CONSTANTS.jqueryCommonDataKey,objListProp);
                    _fillDataInHtmlByRow($row,objListProp);
                });
           }
        }
        function _fillDataInHtmlByRow(tr/*jqueryObject*/,listProperty){
              var objListProp = listProperty;
              var $row = tr;
              if(objListProp.unique === "1"){
                  $row.find('.PrimaryKey').prop('checked',true);    
              }
              else{
              }
              $row.find('.ItemName').val(objListProp.name);
              $row.find('.ItemType').val(objListProp.type);
              $row.data(MF_IDE_CONSTANTS.jqueryCommonDataKey,objListProp);
        }
        function  _getPropsFromHtml(){
            var aryItems = [];
            var $propSettingTbl = _getPropsSettingTable();
            var $tr = $propSettingTbl.find('tbody tr');
            var objEditListItem = null;
            $tr.each(function(index){
                var $self = $(this);
                var $txtName = $self.find('.ItemName');
                var $ddlType = $self.find('.ItemType');
                var $chkPrimaryKey = $self.find('.PrimaryKey');
                var rowData = $self.data(MF_IDE_CONSTANTS.jqueryCommonDataKey);
                var strAssociatedMappedData = "";
                if(rowData){
                    strAssociatedMappedData = rowData.mappedDataCol;
                }    
                if($txtName.val() !== ""){
                    objEditListItem = new EditableListItem({
                                          name :$txtName.val(),
                                          type:$ddlType.val(),
                                          unique :$chkPrimaryKey.is(':checked')?"1":"0",
                                          mappedDataCol:strAssociatedMappedData
                                      });
                     aryItems.push(objEditListItem);
                  }
                    
            });
            return aryItems;
        }
        function _validatePropertiesValue(){
            var aryProperties = _getPropsFromHtml();
            var propNames = [];
            var isValid = true;
            var aryErrors =[];
            if(aryProperties && $.isArray(aryProperties) && aryProperties.length>0){
                for(var i=0;i<=aryProperties.length-1;i++){
                    var prop = aryProperties[i];//should get prop
                    if(prop.name){//should get prop.name
                        if(prop.name && prop.name.toLowerCase() === 'count'){
                           isValid = false;
                           aryErrors.push('Please enter valid property name.Property name cannot be "count".');
                           break; 
                        }
                        else{
                            if(MF_HELPERS.ideValidation.valContainsSpecialCharacter(prop.name)){
                               aryErrors.push('Please enter valid property name.Name cannot have any special character.');
                               break; 
                            }
                            else if(prop.name.trim().length > 20){
                               aryErrors.push('Property name cannot be more than 20 characters.');
                               break; 
                            }
                            else if(prop.name.trim().length <3){
                               aryErrors.push('Property name cannot be less than 3 characters.');
                               break; 
                            }
                            else if($.inArray(prop.name,propNames) !== -1){
                               aryErrors.push('Property name should be unique.');
                               break;  
                            }
                            else{
                                propNames.push(prop.name);
                            }
                        }
                    }
                }
            }
            return aryErrors;
        }
        function _setEventsOfSettingTableLastRow(){
            var $propSettingTable = _getPropsSettingTable();
            var $tr = $propSettingTable.find('tbody tr').not('.AdditionRow');
            $tr.last().find('.crossImg16x16').on('click',function(evnt){
                 var blnConfirm = confirm('Are you sure you want to remove this property.');
                     if(blnConfirm){
                     $(this).closest('tr').remove();
                  }
            });
        }
        function _setEventsOfSettingTable(){
            var $propSettingTable = _getPropsSettingTable();
            var $tr = $propSettingTable.find('tbody tr');
            $tr.find('.crossImg16x16').on('click',function(evnt){
                 var blnConfirm = confirm('Are you sure you want to remove this property.');
                    if(blnConfirm){
                    $(this).closest('tr').remove();
                 }
              });
            $tr.find('.addImg16x16').on('click',_processAddRowToTable);  
        }
        
        function _addDetailsRowToTable(detailsTable/*jquery object*/) {
            var $detailsTable = null;
            if (detailsTable) {
                $detailsTable = detailsTable;
            }
            else {
                $detailsTable = _getPropsSettingTable();
            }
            var $tbody = $detailsTable.find('tbody');
            var $tr = $tbody.find('tr').not('.AdditionRow');
    
            if ($tr.length > 0) {
                $tr.last().after(_getDtlsTableRowWithCross());
            }
            else {
                $tbody.find('tr.AdditionRow').before(_getDtlsTableRowWithCross());
            }
        }
        function _processAddRowToTable(evnt) {
            try {
                var $row = $(this).closest('tr');
                //var errors = _validateColumnAddition($row);
                //if (errors.length > 0)
                    //throw new MfError(errors);
    
                var $detailsTable = _getPropsSettingTable(),
                $txtName = $row.find('.ItemName'),
                $ddlType = $row.find('.ItemType'),
                $chkPrimaryKey = $row.find('.PrimaryKey'),
                objEditListItem = new EditableListItem({
                    name :$txtName.val(),
                    type:$ddlType.val(),
                    unique :$chkPrimaryKey.is(':checked')?"1":"0",
                    mappedDataCol:""
                });
                _addDetailsRowToTable($detailsTable);
    
    
                var $tbody = $detailsTable.find('tbody');
                var $tr = $tbody.find('tr').not('.AdditionRow');
    
                _fillDataInHtmlByRow($tr.last(), objEditListItem);
                _setEventsOfSettingTableLastRow();
                _resetAdditionRow();
            }
            catch (error) {
                if (error instanceof MfError) {
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                } else {
                    console.log(error);
                }
            }
    
        }
        function _resetAdditionRow() {
            var $detailsTable = _getPropsSettingTable();
            var $additionRow = $detailsTable.find('tr.AdditionRow');
            $additionRow.find('.ItemName').val("");
            $additionRow.find('.ItemType').val("0");
            $additionRow.find('.PrimaryKey').prop('checked',false);
        }
        return {
            createHtmlForProps :function(listProperties) {
              _formHtml(listProperties);
              _fillDataInHtml(listProperties);
              _addAdditionRowToTable();
              _setEventsOfSettingTable();
           },
            addRowToPropTable:function(){
              var $propTable = _getPropsSettingTable();
              var $tbody = $propTable.find('tbody'); 
              var $tr = $tbody.find('tbody tr');
              if($tr.length>0){
                 $tr.last().append(_getRowOfSettingTable()); 
              }
              else{
                  $tbody.append(_getRowOfSettingTable());
              }
           _setEventsOfSettingTableLastRow();
        },
            savePropertiesInEditListObject:function(){
                var objEditableList = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                var aryError = _validatePropertiesValue();
                if(aryError.length>0){
                    throw new MfError(aryError);
                }
                var aryProps =[];
                aryProps = _getPropsFromHtml();
                objEditableList.fnSetProperties(aryProps);
                objEditableList.fnProcessPropNameRemoved();
                var objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ListBehaviour.getProperties();
                objPropCntrl.control.val(aryProps.length);
                _editListItemDisplaySettingsHelper.changeHtml.processSetSettingsValueInHtml();
            }
        };
    })();
    var _editListItemDisplaySettingsHelper =(function(){
        function _getItemDisplaySettingDiv(){
            return $('#divEditListItemDispSetting');
        }
        function _setDataOfContainerDiv(settingType){
            var $settingDiv = _getItemDisplaySettingDiv();
            if(!settingType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            $settingDiv.data(dataKeyItemDispSetting,{
                settingType:settingType
             });
        }
        function _removeDataOfContainerDiv(){
            var $settingDiv = _getItemDisplaySettingDiv();
            $settingDiv.removeData(dataKeyItemDispSetting);
        }
        function _getDataOfContainerDiv(){
            var $settingDiv = _getItemDisplaySettingDiv();
            return $settingDiv.data('dataKeyItemDispSetting');
        }
        function _bindTitleTextDroptDown(properties){
            var options ='<option value="-1">Select Property</option>';
            if(properties && $.isArray(properties) && properties.length>0){
                for(var i=0;i<=properties.length-1;i++){
                    options += '<option value="'+properties[i].name+'">'+properties[i].name+'</option>';
                }
            }
            $('#ddlEtlTitleText').html('');
            $('#ddlEtlTitleText').html(options);
        }
        function _showSettingsByType(editList/*object editable list*/){
            if(!editList)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            _bindTitleTextDroptDown(editList.properties);
            var divData = _getDataOfContainerDiv();
            if(!divData)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var $txtPrefix = $('#txtEtlTitlePre');
            var $txtEtlTitleSuff = $('#txtEtlTitleSuff');
            var $txtEtlTitlefact = $('#txtEtlTitlefact');
            var $ddlEtlTitleText = $('#ddlEtlTitleText');
            switch(divData.settingType){
                case EditListItemDispType.title:
                    $ddlEtlTitleText.val(editList.title);
                    $txtPrefix.val(editList.titlePrefix);
                    $txtEtlTitleSuff.val(editList.titleSuffix);
                    $txtEtlTitlefact.val(editList.titleMultFactor);
                    break;
               case EditListItemDispType.additionalTitle:
                   $ddlEtlTitleText.val(editList.additionalTitle);
                   $txtPrefix.val(editList.additionalTitlePrefix);
                   $txtEtlTitleSuff.val(editList.additionalTitleSuffix);
                   $txtEtlTitlefact.val(editList.additionalTitleMultFactor);
               break;
               case EditListItemDispType.subTitle:
                   $ddlEtlTitleText.val(editList.subTitle);
                   $txtPrefix.val(editList.subTitlePrefix);
                   $txtEtlTitleSuff.val(editList.subTitleSuffix);
                   $txtEtlTitlefact.val(editList.subTitleMultFactor);
               break;
               case EditListItemDispType.additionalSubtitle:
                   $ddlEtlTitleText.val(editList.additionalSubTitle);
                   $txtPrefix.val(editList.additionalSubTitlePrefix);
                   $txtEtlTitleSuff.val(editList.additionalSubTitleSuffix);
                   $txtEtlTitlefact.val(editList.additionalSubTitleMultFactor);
               break;
            }
        }
        function _validateItemDispSaving(){
            var $txtPrefix = $('#txtEtlTitlePre');
            var $txtEtlTitleSuff = $('#txtEtlTitleSuff');
            var $txtEtlTitlefact = $('#txtEtlTitlefact');
            var $ddlEtlTitleText = $('#ddlEtlTitleText');
            var aryErrors = [];
            if($ddlEtlTitleText.val() === "-1"){
                aryErrors.push("Please select a property.");
            }
            if($txtPrefix.val().trim().length>20){
                aryErrors.push("Prefix value should be less than 20 characters.");
            }
            if($txtEtlTitleSuff.val().trim().length>20){
                aryErrors.push("Suffix value should be less than 20 characters.");
            }
            if($txtEtlTitlefact.val() &&
                !MF_HELPERS.ideValidation.validateMultiplicationFact($txtEtlTitlefact.val())){
                aryErrors.push("Multiplication factor should be number and not equal to zero.") ;       
            }
            return aryErrors;
           
        }
        function _saveItemDisplaySettingByType(){
            var objEditableList =  MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
            if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var divData = _getDataOfContainerDiv();
            if(!divData)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var aryErrors = _validateItemDispSaving();
            if(aryErrors.length>0){
                throw new MfError(aryErrors);
            }
            var $txtPrefix = $('#txtEtlTitlePre');
            var $txtEtlTitleSuff = $('#txtEtlTitleSuff');
            var $txtEtlTitlefact = $('#txtEtlTitlefact');
            var $ddlEtlTitleText = $('#ddlEtlTitleText');
            switch(divData.settingType){
                case EditListItemDispType.title:
                    objEditableList.title=$ddlEtlTitleText.val();
                    objEditableList.titlePrefix=$txtPrefix.val();
                    objEditableList.titleSuffix=$txtEtlTitleSuff.val();
                    objEditableList.titleMultFactor=$txtEtlTitlefact.val();
                    break;
               case EditListItemDispType.additionalTitle:
                   objEditableList.additionalTitle=$ddlEtlTitleText.val();
                   objEditableList.additionalTitlePrefix=$txtPrefix.val();
                   objEditableList.additionalTitleSuffix=$txtEtlTitleSuff.val();
                   objEditableList.additionalTitleMultFactor=$txtEtlTitlefact.val();
               break;
               case EditListItemDispType.subTitle:
                   objEditableList.subTitle=$ddlEtlTitleText.val();
                   objEditableList.subTitlePrefix=$txtPrefix.val();
                   objEditableList.subTitleSuffix=$txtEtlTitleSuff.val();
                   objEditableList.subTitleMultFactor=$txtEtlTitlefact.val();
               break;
               case EditListItemDispType.additionalSubtitle:
                   objEditableList.additionalSubTitle=$ddlEtlTitleText.val();
                   objEditableList.additionalSubTitlePrefix=$txtPrefix.val();
                   objEditableList.additionalSubTitleSuffix=$txtEtlTitleSuff.val();
                   objEditableList.additionalSubTitleMultFactor=$txtEtlTitlefact.val();
               break;
            }
        }
        function _setSettingsValueInHtmlByType(){
            var objEditableList =  MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
            if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var divData = _getDataOfContainerDiv();
            if(!divData)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var objPropCntrl = null;
            switch(divData.settingType){
                case EditListItemDispType.title:
                    objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getTitle();
                    objPropCntrl.control.val(objEditableList.title);
                    break;
               case EditListItemDispType.additionalTitle:
                   objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getAdditionalTitle();
                   objPropCntrl.control.val(objEditableList.additionalTitle);
               break;
               case EditListItemDispType.subTitle:
                   objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getSubtitle();
                   objPropCntrl.control.val(objEditableList.subTitle);
               break;
               case EditListItemDispType.additionalSubtitle:
                   objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getAdditionalSubtitle();
                   objPropCntrl.control.val(objEditableList.additionalSubTitle);
               break;
            }
        }
        function _setSettingsValueInHtml(){
            var objEditableList =  MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
            if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var objPropCntrl = null;
            objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getTitle();
            objPropCntrl.control.val(objEditableList.title);
            
            objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getAdditionalTitle();
            objPropCntrl.control.val(objEditableList.additionalTitle);
            
            objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getSubtitle();
            objPropCntrl.control.val(objEditableList.subTitle);
            
            objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.ItemDisplay.getAdditionalSubtitle();
            objPropCntrl.control.val(objEditableList.additionalSubTitle);
            
        }
        return {
            processTitleSetting:function(editList/*object optional*/){
                var objEditableList = null;
                if(!editList){
                    objEditableList = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                }
                else{
                    objEditableList = editList;
                }
                if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
               _setDataOfContainerDiv(EditListItemDispType.title);
               _showSettingsByType(objEditableList);
            },
            processAdditionalTitleSetting:function(editList/*object optional*/){
                  var objEditableList = null;
                  if(!editList){
                      objEditableList = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                  }
                  else{
                      objEditableList = editList;
                  }
                  if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                 _setDataOfContainerDiv(EditListItemDispType.additionalTitle);
                 _showSettingsByType(objEditableList);
             },
            processSubtitleSetting:function(editList/*object optional*/){
                   var objEditableList = null;
                   if(!editList){
                       objEditableList = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                   }
                   else{
                       objEditableList = editList;
                   }
                   if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                  _setDataOfContainerDiv(EditListItemDispType.subTitle);
                  _showSettingsByType(objEditableList);
              },
            processAdditionalSubtitleSetting:function(editList/*object optional*/){
                 var objEditableList = null;
                 if(!editList){
                     objEditableList = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                 }
                 else{
                     objEditableList = editList;
                 }
                 if(!objEditableList)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                _setDataOfContainerDiv(EditListItemDispType.additionalSubtitle);
                _showSettingsByType(objEditableList);
            },
            processSaveItemDisplaySettingByType:function(){
                _saveItemDisplaySettingByType();
                _setSettingsValueInHtmlByType();
                _removeDataOfContainerDiv();
                closeModalPopUp("divEditListItemDispSetting");
            },
            cancelItemDisplaySetting:function(){
                _removeDataOfContainerDiv();
                closeModalPopUp("divEditListItemDispSetting");
            },
            changeHtml:{
                //Changes values of All Settings Html if value is changed in properties
                processSetSettingsValueInHtml :function(){
                    _setSettingsValueInHtml();
                }
            }
        };
    })();
    return{
        processShowListPropTable :function(listProperties){
            _editableListPropsHelper.createHtmlForProps(listProperties);
            showModalPopUp("divEditableListProps","Items",600, false);
        },
        addRowToPropTable:function(){
             _editableListPropsHelper.addRowToPropTable();
        },
        processSavePropertiesInEditListObject:function(){
            _editableListPropsHelper.savePropertiesInEditListObject();
            closeModalPopUp('divEditableListProps');
        },
        processShowEditListTitleSetting:function(editList/*object optional if available then pass. */){
            _editListItemDisplaySettingsHelper.processTitleSetting(editList);
            showModalPopUp("divEditListItemDispSetting","Title",400,false);
        },
        processShowEditListAdditionalTitleSetting:function(editList/*object optional if available then pass. */){
              _editListItemDisplaySettingsHelper.processAdditionalTitleSetting(editList);
              showModalPopUp("divEditListItemDispSetting","Additional Title",400,false);
          },
        processShowEditListSubtitleSetting:function(editList/*object optional if available then pass. */){
           _editListItemDisplaySettingsHelper.processSubtitleSetting(editList);
           showModalPopUp("divEditListItemDispSetting","Subtitle",400,false);
        },
        processShowEditListAdditionalSubtitleSetting:function(editList/*object optional if available then pass. */){
           _editListItemDisplaySettingsHelper.processAdditionalSubtitleSetting(editList);
           showModalPopUp("divEditListItemDispSetting","Additional Subtitle",400,false);
        },
        cancelItemDisplaySetting:function(){
            _editListItemDisplaySettingsHelper.cancelItemDisplaySetting();
        },
        processSaveItemSettingByType:function(){
            _editListItemDisplaySettingsHelper.processSaveItemDisplaySettingByType();
        }
    };
})();

$(document).ready(function() {
    $('#btnAddEditListOption').on('click',function(evnt){
        editableListHelper.addRowToPropTable();
    });
    $('#btnEditListPropsSave').on('click',function(evnt){
        try{
            editableListHelper.processSavePropertiesInEditListObject();
          }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnEditListItemDispSettingSave').on('click',function(evnt){
        try{
            editableListHelper.processSaveItemSettingByType();
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnEditListItemDispSettingClose').on('click',function(evnt){
        try{
            editableListHelper.cancelItemDisplaySetting();
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnEditListPropCancel').on('click',function(evnt){
        try{
           closeModalPopUp("divEditableListProps");
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
});

/*------------------------------------Tanika---------------------------------------*/
 
function addChildForm(cntrl){
    app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
    $('#FormDesignContainerDiv').html('');
    $('#TbFormDesignContainerDiv').html('');
    childForm.isDeleted = false;
    childForm.isChildForm = true;
    childForm.isNew = true;
    childForm.appId = app.id;
    initializeChildForm();
    if(form.isTablet) childForm.isTablet = true;
    else childForm.isTablet = false;
    switch(childForm.childFormType.idPrefix){
        case "ADD":
            childForm.id = $('[id$=hdfCurrentFormId]').val() + "_addForm";
            childForm.name = "Add";
            childForm.formDescription = "Add";
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('draggableSeparatorDiv');
            CtrlAttAddAndEnabled('SpacerDiv');
            //CtrlAttAddAndEnabled('draggableImageDiv');
            CtrlAttAddAndEnabled('draggableRepeaterDiv');
            CtrlAttAddAndEnabled('draggableEditableListDiv');
            CtrlAttAddAndEnabled('draggableLocationDiv');
            CtrlAttAddAndEnabled('PieChartDiv');
            CtrlAttAddAndEnabled('BarGraphDiv');
            CtrlAttAddAndEnabled('LineChartDiv');
            CtrlAttAddAndEnabled('AngularGuageDiv');
            CtrlAttAddAndEnabled('CylinderGuageDiv');
            CtrlAttAddAndEnabled('DrilldownPieChartDiv');
            CtrlAttAddAndEnabled('TableDiv');
            CtrlAttAddAndEnabled('SignatureDiv');
            CtrlAttAddAndEnabled('PictureDiv');
            break;
        case "EDIT":
            childForm.id = $('[id$=hdfCurrentFormId]').val() + "_editForm";
            childForm.name = "Edit";
            childForm.formDescription = "Edit";
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('draggableSeparatorDiv');
            CtrlAttAddAndEnabled('SpacerDiv');
            //CtrlAttAddAndEnabled('draggableImageDiv');
            CtrlAttAddAndEnabled('draggableRepeaterDiv');
            CtrlAttAddAndEnabled('draggableEditableListDiv');
            CtrlAttAddAndEnabled('draggableLocationDiv');
            CtrlAttAddAndEnabled('PieChartDiv');
            CtrlAttAddAndEnabled('BarGraphDiv');
            CtrlAttAddAndEnabled('LineChartDiv');
            CtrlAttAddAndEnabled('AngularGuageDiv');
            CtrlAttAddAndEnabled('CylinderGuageDiv');
            CtrlAttAddAndEnabled('DrilldownPieChartDiv');
            CtrlAttAddAndEnabled('TableDiv');
            CtrlAttAddAndEnabled('SignatureDiv');
            CtrlAttAddAndEnabled('PictureDiv');
            break;
        case "VIEW":
            childForm.id = $('[id$=hdfCurrentFormId]').val() + "_viewForm";
            childForm.name = "View";
            childForm.formDescription = "View";
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('draggableSeparatorDiv');
            CtrlAttAddAndEnabled('SpacerDiv');
            CtrlAttAddAndEnabled('draggableTextboxDiv');
            CtrlAttAddAndEnabled('draggableTextareaDiv');
            CtrlAttAddAndEnabled('draggableDropdownDiv');
            CtrlAttAddAndEnabled('draggableRadioButtonDiv');
            CtrlAttAddAndEnabled('draggableCheckboxDiv');
            CtrlAttAddAndEnabled('draggableRepeaterDiv');
            CtrlAttAddAndEnabled('draggableEditableListDiv');
            CtrlAttAddAndEnabled('draggableDatetimePickerDiv');
            CtrlAttAddAndEnabled('draggableToggleDiv');
            CtrlAttAddAndEnabled('draggableSliderDiv');
            CtrlAttAddAndEnabled('draggableBarcodeDiv');
            CtrlAttAddAndEnabled('draggableLocationDiv');
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('PieChartDiv');
            CtrlAttAddAndEnabled('BarGraphDiv');
            CtrlAttAddAndEnabled('DrilldownPieChartDiv');
            CtrlAttAddAndEnabled('LineChartDiv');
            CtrlAttAddAndEnabled('TableDiv');
            CtrlAttAddAndEnabled('AngularGuageDiv');
            CtrlAttAddAndEnabled('CylinderGuageDiv');
            //CtrlAttAddAndEnabled('draggableImageDiv');
            CtrlAttAddAndEnabled('SignatureDiv');
            CtrlAttAddAndEnabled('PictureDiv');
            break;
        }

    //cntrl.fnAddChildForm(childForm);
    addNewChildForm();
}

function editChildForm(cntrl, objChildForm){
    objChildForm.isNew = false;
    switch(objChildForm.childFormType.idPrefix){
        case "ADD":
            childForm.id = $('[id$=hdfCurrentFormId]').val() + "_addForm";
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('draggableSeparatorDiv');
            CtrlAttAddAndEnabled('SpacerDiv');
            CtrlAttAddAndEnabled('draggableRepeaterDiv');
            CtrlAttAddAndEnabled('draggableEditableListDiv');
            CtrlAttAddAndEnabled('draggableLocationDiv');
            CtrlAttAddAndEnabled('PieChartDiv');
            CtrlAttAddAndEnabled('BarGraphDiv');
            CtrlAttAddAndEnabled('LineChartDiv');
            CtrlAttAddAndEnabled('AngularGuageDiv');
            CtrlAttAddAndEnabled('CylinderGuageDiv');
            CtrlAttAddAndEnabled('TableDiv');
            CtrlAttAddAndEnabled('SignatureDiv');
            CtrlAttAddAndEnabled('PictureDiv');
            break;
        case "EDIT":
            childForm.id = $('[id$=hdfCurrentFormId]').val() + "_editForm";
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('draggableSeparatorDiv');
            CtrlAttAddAndEnabled('SpacerDiv');
            CtrlAttAddAndEnabled('draggableRepeaterDiv');
            CtrlAttAddAndEnabled('draggableEditableListDiv');
            CtrlAttAddAndEnabled('draggableLocationDiv');
            CtrlAttAddAndEnabled('PieChartDiv');
            CtrlAttAddAndEnabled('BarGraphDiv');
            CtrlAttAddAndEnabled('LineChartDiv');
            CtrlAttAddAndEnabled('AngularGuageDiv');
            CtrlAttAddAndEnabled('CylinderGuageDiv');
            CtrlAttAddAndEnabled('TableDiv');
            CtrlAttAddAndEnabled('SignatureDiv');
            CtrlAttAddAndEnabled('PictureDiv');
            break;
        case "VIEW":
            childForm.id = $('[id$=hdfCurrentFormId]').val() + "_viewForm";
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('draggableSeparatorDiv');
            CtrlAttAddAndEnabled('SpacerDiv');
            CtrlAttAddAndEnabled('draggableTextboxDiv');
            CtrlAttAddAndEnabled('draggableTextareaDiv');
            CtrlAttAddAndEnabled('draggableDropdownDiv');
            CtrlAttAddAndEnabled('draggableRadioButtonDiv');
            CtrlAttAddAndEnabled('draggableCheckboxDiv');
            CtrlAttAddAndEnabled('draggableRepeaterDiv');
            CtrlAttAddAndEnabled('draggableEditableListDiv');
            CtrlAttAddAndEnabled('draggableDatetimePickerDiv');
            CtrlAttAddAndEnabled('draggableToggleDiv');
            CtrlAttAddAndEnabled('draggableSliderDiv');
            CtrlAttAddAndEnabled('draggableBarcodeDiv');
            CtrlAttAddAndEnabled('draggableLocationDiv');
            CtrlAttAddAndEnabled('draggableGroupboxDiv');
            CtrlAttAddAndEnabled('PieChartDiv');
            CtrlAttAddAndEnabled('BarGraphDiv');
            CtrlAttAddAndEnabled('LineChartDiv');
            CtrlAttAddAndEnabled('TableDiv');
            CtrlAttAddAndEnabled('SignatureDiv');
            CtrlAttAddAndEnabled('PictureDiv');
            CtrlAttAddAndEnabled('AngularGuageDiv');
            CtrlAttAddAndEnabled('CylinderGuageDiv');
            break;
        }

        childFormEdit();
}

var mfIdeEditList = "mfIdeEditList";