﻿Number.isInteger = Number.isInteger || function(value) {
    return typeof value === "number" && 
           isFinite(value) && 
           Math.floor(value) === value;
};
function isStringValueInt(n){
    return Number(n) === parseInt(n) && Number(n) % 1 === 0;
}

// function isStringValueFloat(n){
//     return n === Number(n) && n % 1 !== 0;
// }
var OFFLINE_CONSTANTS = {
    "NotApplicable": "NA",
    "OfflineRptRowClickNumber": 53,
    "regxName": /^[a-z0-9]+$/i,
    "NotApplicableValInDb": "0",
    "ColMapTxtBoxFixedParams": [
        { label: "FNAME (Fixed)", value: "_@FNAME" }, { label: "LNAME (Fixed)", value: "_@LNAME " }, { label: "FULLNAME (Fixed)", value: "_@FULLNAME" },
        { label: "UID (Fixed)", value: "_@UID" }, { label: "EMAIL (Fixed)", value: "_@EMAIL" }, { label: "EMPLOYEEID (Fixed)", value: "_@EMPLOYEEID " },
        { label: "DESIGNATION (Fixed)", value: "_@DESIGNATION" }, { label: "MOBILE (Fixed)", value: "_@MOBILE" }, { label: "DOB (Fixed)", value: "_@DOB" },
        { label: "LOCATION (Fixed)", value: "_@LOCATION" }, { label: "REGION (Fixed)", value: "_@REGION" }, { label: "DIVISION (Fixed)", value: "_@DIVISION" },
        { label: "DEPARTMENT (Fixed)", value: "_@DEPARTMENT" }, { label: "DEVICEID (Fixed)", value: "_@DEVICEID" }, { label: "DEVICETYPE (Fixed)", value: "_@DEVICETYPE" },
        { label: "LATITUDE (Fixed)", value: "_@LATITUDE" }, { label: "LONGITUDE (Fixed)", value: "_@LONGITUDE" }, { label: "SYSTEMDATE (Fixed)", value: "_@SYSTEMDATE" },
        { label: "SYSTEMDATETIME (Fixed)", value: "_@SYSTEMDATETIME" }, { label: "SYSTEMTIME (Fixed)", value: "_@SYSTEMTIME" }, { label: "SYSTEMTICKSDATETIME (Fixed)", value: "_@SYSTEMTICKSDATETIME" },
        { label: "SYSTEMUNIXDATETIME (Fixed)", value: "_@SYSTEMUNIXDATETIME " }
    ],
    "InputParamType": {
        "Fixed": 1,
        "COnstant": 2,
        "OFDBColumn": "3"
    },
    jqueryCommonDatakey:"dataKey",
    intlsenseFixedParams:[{prop:"@FIRSTNAME",val:"@FIRSTNAME",category:"Fixed Values"},{prop:"@LASTNAME",val:"@LASTNAME",category:"Fixed Values"},{prop:"@FULLNAME",val:"@FULLNAME",category:"Fixed Values"},{prop:"@UID",val:"@UID",category:"Fixed Values"},{prop:"@EMAIL",val:"@EMAIL",category:"Fixed Values"},{prop:"@EMPLOYEEID",val:"@EMPLOYEEID",category:"Fixed Values"},{prop:"@DESIGNATION",val:"@DESIGNATION",category:"Fixed Values"},{prop:"@MOBILE",val:"@MOBILE",category:"Fixed Values"},{prop:"@DOB",val:"@DOB",category:"Fixed Values"},{prop:"@LOCATION",val:"@LOCATION",category:"Fixed Values"},{prop:"@REGION",val:"@REGION",category:"Fixed Values"},{prop:"@DIVISION",val:"@DIVISION",category:"Fixed Values"},{prop:"@DEPARTMENT",val:"@DEPARTMENT",category:"Fixed Values"},{prop:"@DEVICEID",val:"@DEVICEID",category:"Fixed Values"},{prop:"@DEVICEMODEL",val:"@DEVICEMODEL",category:"Fixed Values"},{prop:"@LATITUDE",val:"@LATITUDE",category:"Fixed Values"},{prop:"@LONGITUDE",val:"@LONGITUDE",category:"Fixed Values"},{prop:"@DATETIME",val:"@DATETIME",category:"Fixed Values"},{prop:"@DATE",val:"@DATE",category:"Fixed Values"},{prop:"@TIME",val:"@TIME",category:"Fixed Values"},{prop:"@DATETIMETICKS",val:"@DATETIMETICKS",category:"Fixed Values"},{prop:"@DATETICKS",val:"@DATETICKS",category:"Fixed Values"},{prop:"@DATEYYYYMMDD",val:"@DATEYYYYMMDD",category:"Fixed Values"},{prop:"@UNIQUEID",val:"@UNIQUEID",category:"Fixed Values"},{prop:"@WIFI",val:"@WIFI",category:"Fixed Values"}],
    DataUploadWithinDefault : "7"
};
var MF_EXCEPTION ={
    
    objectNotFound : "Object not found",
    argumentsNull : "Arguments Null Exception",
    InvalidArgumentType :"Argument type not same as that expected"
};
var Offline_Table_DataType = {
    "String": "0",
    "Number": "1",
    "Decimal": "2",
    "File" :"3"
};
var Offline_Table_DataType_STRING = {
    "String": "String",
    "Number": "Number",
    "Decimal": "Decimal",
    "File" :"File"
};
var WEB_SERVICE_TYPE = {
    http: 1,
    wsdlSoap: 2,
    rpcXml: 3
};
var WEB_SERVICE_TYPE_TEXT = {
    http:"http",
    wsdlSoap:"wsdl",
    rpcXml: "rpc"
};
var OFFLINE_TABLE_FORM_TAB = {
    TabTableContent: "0",
    TabColumnsContent: "1",
    TabAppsContent: "2",
    TabSyncContent: "3"
};
var OFFLINE_PAGE_DISPLAY_MODE = {
    RPtDetails: "0",
    FormMode: "1",
    FormModeEditView: "2"
};
var OFFLINE_FORM_DISPLAY_MODE = {
    Add: "0",
    Edit: "1",
    EditView: "2"
};
var OFFLINE_TABLE_TYPE ={
    UploadData : "0",
    DownloadData:"1",
    DownloadAndUploadData:"2"
};
var OFFLINE_DT_SYNC_ON_TYPE ={
    OnLogin: "1",
    OnAppStartUp:"2",
    Manual:"4",
    OnAppClose:"3"
};
var IDE_COMMAND_OBJECT_TYPES= {
    None: { id: "0", UiName: "None" },
    db: { id: "1", UiName: "Database" },
    wsdl: { id: "2", UiName: "Webservice ( Soap )" },
    http: { id: "3", UiName: "Webservice ( Http )" },
    xmlRpc: { id: "4", UiName: "Webservice ( Xml RPC )" },
    odata: { id: "5", UiName: "Webservice ( ODATA )" }
};
var DB_COMMAND_TYPE = {
    Select: "1",
    Insert: "2",
    Update: "3",
    Delete: "4"
};
var OFFLINE_TBL_UPLOAD_SEQUENCE={
    LIFO : "0",
    FIFO :"1"
};
(function ($) {
    $.uniform = {
        options: {
            selectClass: 'selector',
            radioClass: 'radio',
            checkboxClass: 'checker',
            fileClass: 'uploader',
            filenameClass: 'filename',
            fileBtnClass: 'action',
            fileDefaultText: 'No file selected',
            fileBtnText: 'Choose File',
            checkedClass: 'checked',
            focusClass: 'focus',
            disabledClass: 'disabled',
            buttonClass: 'button',
            activeClass: 'active',
            hoverClass: 'hover',
            useID: true,
            idPrefix: 'uniform',
            resetSelector: false,
            autoHide: true
        },
        elements: []
    };

    if ($.browser.msie && $.browser.version < 7) {
        $.support.selectOpacity = false;
    } else {
        $.support.selectOpacity = true;
    }

    $.fn.uniform = function (options) {

        options = $.extend($.uniform.options, options);

        var el = this;
        //code for specifying a reset button
        if (options.resetSelector != false) {
            $(options.resetSelector).mouseup(function () {
                function resetThis() {
                    $.uniform.update(el);
                }
                setTimeout(resetThis, 10);
            });
        }

        function doInput(elem) {
            $el = $(elem);
            $el.addClass($el.attr("type"));
            storeElement(elem);
        }

        function doTextarea(elem) {
            $(elem).addClass("uniform");
            storeElement(elem);
        }

        function doSelect(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.selectClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            var selected = elem.find(":selected:first");
            if (selected.length == 0) {
                selected = elem.find("option:first");
            }
            spanTag.html(selected.html());

            elem.css('opacity', 0);
            elem.wrap(divTag);
            elem.before(spanTag);

            //redefine variables
            divTag = elem.parent("div");
            spanTag = elem.siblings("span");
            //5/9/2012
            //elem.width(divTag.outerWidth(false));
            var elemWidth = 0; //9/11/2012
            if (divTag.outerWidth(false) == 10) //width of the div tag if elem initial width is zero
            {
                elemWidth = divTag.outerWidth(false) + spanTag.outerWidth(false) + 25 + 2 + 10;
                //where 25 and 2 is the padding provided to the span tag in jquery.uniform.css,10 is the padding provided to wrapperDiv in css
                //there is an additional 10px area due to the uniform image//adding that as well to work consistently
            } else {
                elemWidth = spanTag.outerWidth(false) + 25 + 2 + 10; //where 25,2 is the padding provided to the span tag in jquery.uniform.css
            }
            elem.width(elemWidth);
            elem.bind({
                "change.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                    divTag.removeClass(options.activeClass);
                },
                "focus.uniform": function () {
                    divTag.addClass(options.focusClass);
                },
                "blur.uniform": function () {
                    divTag.removeClass(options.focusClass);
                    divTag.removeClass(options.activeClass);
                },
                "mousedown.uniform touchbegin.uniform": function () {
                    divTag.addClass(options.activeClass);
                },
                "mouseup.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "click.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "mouseenter.uniform": function () {
                    divTag.addClass(options.hoverClass);
                },
                "mouseleave.uniform": function () {
                    divTag.removeClass(options.hoverClass);
                    divTag.removeClass(options.activeClass);
                },
                "keyup.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                }
            });

            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }
            $.uniform.noSelect(spanTag);

            storeElement(elem);
            //added on 19/12/2012 to set th width of the drop down according to the width of the selected text
            $(elem).change(function() {
                  setDropDownListWidthOnChange($(elem));
                });
            $(divTag).hover(
            function(){
                var elemSiblingSpan = $(elem).siblings("span");
                if($(elem).outerWidth() < $(elemSiblingSpan).outerWidth()){
                setDropDownListWidthOnChange($(elem)) ;
                }
            });
               
        }

        function doCheckbox(elem) {
            var $el = $(elem);
            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.checkboxClass);

            //assign the id of the element
            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
        .css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "click.uniform touchend.uniform": function () {
                if (!$(this).is(":checked")) {
                    //box was just unchecked, uncheck span
                    spanTag.removeClass(options.checkedClass);
                } else {
                    //box was just checked, check span.
                    spanTag.addClass(options.checkedClass);
                }
                
                // if (!$(this).attr("checked")) {
                //     //box was just unchecked, uncheck span
                //     spanTag.removeClass(options.checkedClass);
                // } else {
                //     //box was just checked, check span.
                //     spanTag.addClass(options.checkedClass);
                // }
            },
            "mousedown.uniform touchbegin.uniform": function () {
                divTag.addClass(options.activeClass);
            },
            "mouseup.uniform touchend.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            //handle defaults
            if ($(elem).is(":checked")) {
                //box is checked by default, check our box
                spanTag.addClass(options.checkedClass);
            }

            //handle disabled state
            if ($(elem).is(":disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);
        }

        function doRadio(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.radioClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
        .css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "click.uniform touchend.uniform": function () {
                if (!$(elem).attr("checked")) {
                    //box was just unchecked, uncheck span
                    spanTag.removeClass(options.checkedClass);
                } else {
                    //box was just checked, check span
                    var classes = options.radioClass.split(" ")[0];
                    $("." + classes + " span." + options.checkedClass + ":has([name='" + $(elem).attr('name') + "'])").removeClass(options.checkedClass);
                    spanTag.addClass(options.checkedClass);
                }
            },
            "mousedown.uniform touchend.uniform": function () {
                if (!$(elem).is(":disabled")) {
                    divTag.addClass(options.activeClass);
                }
            },
            "mouseup.uniform touchbegin.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform touchend.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            //handle defaults
            if ($(elem).attr("checked")) {
                //box is checked by default, check span
                spanTag.addClass(options.checkedClass);
            }
            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);

        }

        function doFile(elem) {
            //sanitize input
            var $el = $(elem);
            if( $el && $($el.parent()[0]).attr('class') !== "uploader"){//8/5/2013 Mohan
            
                var divTag = $('<div />'),
        filenameTag = $('<span>' + options.fileDefaultText + '</span>'),
        btnTag = $('<span>' + options.fileBtnText + '</span>');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.fileClass);
            filenameTag.addClass(options.filenameClass);
            btnTag.addClass(options.fileBtnClass);

            if (options.useID && $el.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));
            }

            //wrap with the proper elements
            $el.wrap(divTag);
            $el.after(btnTag);
            $el.after(filenameTag);

            //redefine variables
            divTag = $el.closest("div");
            filenameTag = $el.siblings("." + options.filenameClass);
            btnTag = $el.siblings("." + options.fileBtnClass);

            //set the size
            if (!$el.attr("size")) {
                var divWidth = divTag.width();
                //$el.css("width", divWidth);
                $el.attr("size", divWidth / 10);
            }

            //actions
            var setFilename = function () {
                var filename = $el.val();
                if (filename === '') {
                    filename = options.fileDefaultText;
                } else {
                    filename = filename.split(/[\/\\]+/);
                    filename = filename[(filename.length - 1)];
                }
                filenameTag.text(filename);
            };

            // Account for input saved across refreshes
            setFilename();

            $el.css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "mousedown.uniform": function () {
                if (!$(elem).is(":disabled")) {
                    divTag.addClass(options.activeClass);
                }
            },
            "mouseup.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            // IE7 doesn't fire onChange until blur or second fire.
            if ($.browser.msie) {
                // IE considers browser chrome blocking I/O, so it
                // suspends tiemouts until after the file has been selected.
                $el.bind('click.uniform.ie7', function () {
                    setTimeout(setFilename, 0);
                });
            } else {
                // All other browsers behave properly
                $el.bind('change.uniform', setFilename);
            }

            //handle defaults
            if ($el.attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            $.uniform.noSelect(filenameTag);
            $.uniform.noSelect(btnTag);

            storeElement(elem);
            }
            
            

        }

        $.uniform.restore = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }

            $(elem).each(function () {
                if ($(this).is(":checkbox")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is("select")) {
                    //remove sibling span
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is(":radio")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is(":file")) {
                    //remove sibling spans
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is("button, :submit, :reset, a, input[type='button']")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                }

                //unbind events
                $(this).unbind(".uniform");

                //reset inline style
                $(this).css("opacity", "1");

                //remove item from list of uniformed elements
                var index = $.inArray($(elem), $.uniform.elements);
                $.uniform.elements.splice(index, 1);
            });
        };

        function storeElement(elem) {
            //store this element in our global array
            elem = $(elem).get();
            if (elem.length > 1) {
                $.each(elem, function (i, val) {
                    $.uniform.elements.push(val);
                });
            } else {
                $.uniform.elements.push(elem);
            }
        }

        //noSelect v1.0
        $.uniform.noSelect = function (elem) {
            function f() {
                return false;
            };
            $(elem).each(function () {
                this.onselectstart = this.ondragstart = f; // Webkit & IE
                $(this)
          .mousedown(f) // Webkit & Opera
        .css({
            MozUserSelect: 'none'
        }); // Firefox
            });
        };

        $.uniform.update = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }
            //sanitize input
            elem = $(elem);

            elem.each(function () {
                //do to each item in the selector
                //function to reset all classes
                var $e = $(this);

                if ($e.is("select")) {
                    //element is a select
                    var spanTag = $e.siblings("span");
                    var divTag = $e.parent("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    //reset current selected text
                    spanTag.html($e.find(":selected").html());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":checkbox")) {
                    //element is a checkbox
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }
                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":radio")) {
                    //element is a radio
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":file")) {
                    var divTag = $e.parent("div");
                    var filenameTag = $e.siblings(options.filenameClass);
                    btnTag = $e.siblings(options.fileBtnClass);

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    filenameTag.text($e.val());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":submit") || $e.is(":reset") || $e.is("button") || $e.is("a") || elem.is("input[type=button]")) {
                    var divTag = $e.closest("div");
                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                }

            });
        };

        return this.each(function () {
            if ($.support.selectOpacity) {
                var elem = $(this);

                if (elem.is("select")) {
                    //element is a select
                    if (elem.attr("multiple") != true) {
                        //element is not a multi-select
                        if (elem.attr("size") == undefined || elem.attr("size") <= 1) {
                            doSelect(elem);
                        }
                    }
                } else if (elem.is(":checkbox")) {
                    //element is a checkbox
                    doCheckbox(elem);
                } else if (elem.is(":radio")) {
                    //element is a radio
                    doRadio(elem);
                } else if (elem.is(":file")) {
                    //element is a file upload
                    doFile(elem);
                } else if (elem.is(":text, :password, input[type='email']")) {
                    doInput(elem);
                } else if (elem.is("textarea")) {
                    doTextarea(elem);
                } else if (elem.is("a") || elem.is(":submit") || elem.is(":reset") || elem.is("button") || elem.is("input[type=button]")) {
                    //doButton(elem);
                }

            }
        });
    };
})(jQuery);

function setDropDownListWidthOnChange(dropdown) {
    var elem = dropdown;
    var divTag = elem.parent("div");
    var spanTag = elem.siblings("span");
    var selected = elem.find(":selected:first");
    if (selected.length == 0) {
        selected = elem.find("option:first");
    }
    spanTag.html(selected.html());
    var elemWidth = 0;
    if (divTag.outerWidth(false) == 10) {
        elemWidth = divTag.outerWidth(false) + spanTag.outerWidth(false) + 25 + 2 + 10;
    } else {
        elemWidth = spanTag.outerWidth(false) + 25 + 2 + 10;
    }
    elem.width(elemWidth);
}
function makeControlsUniform(){
    //$('input,select:not(".notUniform")').uniform();
    $("select,input, input[type=file]")
    .not('select[multiple],.notUniform,.dataTables_length select').uniform();
}

function OfflineTableCont(options){
    if(!options)options = {};
    this.tbl = options.tbl;
}
OfflineTableCont.prototype.fnSetTable = function(table/*OfflineDatatable*/){
    this.tbl = table;
};
OfflineTableCont.prototype.fnGetTable = function(){
    return this.tbl;
};
OfflineTableCont.prototype.fnResetObjectDetails = function(){
   var objTbl = this.fnGetTable();
   if(objTbl){
       objTbl = new OfflineDatatable(objTbl);
       objTbl.fnResetObjectDetails();
       this.fnSetTable(objTbl);
   }
};

function SyncDetailsCont(options){
    if(!options)options = {};
    this.sync = options.sync;
}
SyncDetailsCont.prototype.fnSetSync=function(syncDtls/*SyncDtls*/){
    this.sync = syncDtls;
};
SyncDetailsCont.prototype.fnGetSync=function(){
    return this.sync;
};
SyncDetailsCont.prototype.fnResetObjectDetails = function(){
    var objSyncDetails = this.fnGetSync();
    if(objSyncDetails){
        objSyncDetails = new SyncDtls(objSyncDetails);
        objSyncDetails.fnResetObjectDetails();
        this.fnSetSync(objSyncDetails);
    }
};


function OfflineDatatable(options){
    if(!options)options = {};
    this.nm = options.nm;//table name
    this.id = options.id;//table id
    this.typ = options.typ;//table type OFFLINE_TABLE_TYPE
    this.sync = options.sync;//OFFLINE_DT_SYNC_ON_TYPE ary ["0","1"]etc
    this.seq = options.seq;//OFFLINE_TBL_UPLOAD_SEQUENCE 
    this.nor = options.nor;//no of records
    this.prmt = options.prmt;//prompt user confirmation
    //this.down = options.down;//array of CmdParamDtls;
    //this.up = options.up;//array of CmdParamDtls;
    this.col = options.col;//OfflineDatatableCol array
}
OfflineDatatable.prototype.fnResetObjectDetails = function(){
    var columns = this.col,
        aryColumns=[],
        syncTypes = this.sync;
    if(columns && $.isArray(columns) && columns.length >0) {
        for (var i = 0; i <= columns.length - 1; i++) {
            var objcolumn = new OfflineDatatableCol(columns[i]);
            aryColumns.push(objcolumn);
        }
    }
    this.fnSetColumns(aryColumns);
    
    if($.isArray(syncTypes)){
        this.fnSetSyncType(this.fnGetDefaultSyncOnTypes());
    }
};
OfflineDatatable.prototype.fnSetDefaultsValues = function(){
    this.nm = "";
    this.id = "";
    this.typ = "0";
    this.sync = [];
    this.seq = "0";
    this.nor = "0";
    this.prmt = "0";
    this.col = [];
};
OfflineDatatable.prototype.fnSetColumns = function (cols/*array OfflineDatatableCol*/){
    this.col = cols;
};
OfflineDatatable.prototype.fnGetColumns= function(){
    return this.col;
};

OfflineDatatable.prototype.fnGetName = function (){
    return this.nm;
};
OfflineDatatable.prototype.fnSetName= function(value){
    this.nm = value;
};

OfflineDatatable.prototype.fnGetType = function (){
    return this.typ;
};
OfflineDatatable.prototype.fnSetType= function(value /*OFFLINE_TABLE_TYPE*/){
    this.typ = value;
};
OfflineDatatable.prototype.fnGetDefaultSyncOnTypes = function (){
    return "1111";
};

OfflineDatatable.prototype.fnGetSyncType = function (){
    return this.sync;
};
OfflineDatatable.prototype.fnSetSyncType= function(value /*OFFLINE_DT_SYNC_ON_TYPE*/){
    this.sync = value;
};

OfflineDatatable.prototype.fnGetSequence = function (){
    return this.seq;
};
OfflineDatatable.prototype.fnSetSequence= function(value /*OFFLINE_TBL_UPLOAD_SEQUENCE*/){
    this.seq = value;
};
OfflineDatatable.prototype.fnGetTransferBlockSize = function (){
    return this.nor;
};
OfflineDatatable.prototype.fnSetTransferBlockSize= function(value){
    this.nor = value;
};
OfflineDatatable.prototype.fnGetUserConfirmationPromt = function (){
    return this.prmt;
};
OfflineDatatable.prototype.fnSetUserConfirmationPrompt= function(value){
    this.prmt = value;
};
OfflineDatatable.prototype.fnSetId=function(value){
    this.id = value;
};
OfflineDatatable.prototype.fnGetId=function() {
    return this.id;
};

function OfflineDatatableCol(options){
    if(!options) options = {};
    this.cnm = options.cnm;//column name
    this.ctyp = options.ctyp;//column data type Offline_Table_DataType
    this.dpnt = options.dpnt;//no of decimal places
    //this.min = options.min;//min value
    //this.max = options.max;//max value
    this.dval  = options.dval;//default value
    //this.pk = options.pk;//primary key
    /*Earlier for ctyp the Offline_Table_DataType_STRING value was saved
        after Offline_Table_DataType is being saved.
        So for the earlier already defined datataled to work doing this code
    */
    switch(this.ctyp){
        case Offline_Table_DataType_STRING.String:
            this.ctyp = Offline_Table_DataType.String;
            break;
        case Offline_Table_DataType_STRING.Number:
            this.ctyp = Offline_Table_DataType.Number;
            break;
        case Offline_Table_DataType_STRING.Decimal:
            this.ctyp = Offline_Table_DataType.Decimal;
            break;
    }
}
OfflineDatatableCol.prototype.fnGetName=function(){
    return this.cnm;
};
OfflineDatatableCol.prototype.fnGetDataType = function(){
    return this.ctyp;
};
OfflineDatatableCol.prototype.fnGetDecimalPlaces = function(){
    return this.dpnt;
};
OfflineDatatableCol.prototype.fnGetMinValue = function(){
    //return this.min;
};
OfflineDatatableCol.prototype.fnGetMaxValue = function(){
    //return this.max;
};
OfflineDatatableCol.prototype.fnGetDefaultValue = function(){
    return this.dval;
};
OfflineDatatableCol.prototype.fnGetPrimaryKeyValue = function(){
    //return this.pk;
};

function SyncDtls(params){
    if(!params)params={};
    this.cmdType = params.cmdType;
    this.conVal = params.conVal;//connection value
    this.download = params.download;//SyncDtlsDownload
    this.upload = params.upload;//SyncDtlsUpload
    this.isInitialDownload = params.isInitialDownload;
}

SyncDtls.prototype.fnSetCmdType = function(cmdType/*IDE_COMMAND_OBJECT_TYPES*/){
    switch(cmdType){
        case IDE_COMMAND_OBJECT_TYPES.db:
                this.cmdType = IDE_COMMAND_OBJECT_TYPES.db.id;
            break;
        case IDE_COMMAND_OBJECT_TYPES.wsdl:
                this.cmdType = IDE_COMMAND_OBJECT_TYPES.wsdl.id;
            break;
        case IDE_COMMAND_OBJECT_TYPES.http:
                this.cmdType = IDE_COMMAND_OBJECT_TYPES.http.id;
            break;
        case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                this.cmdType = IDE_COMMAND_OBJECT_TYPES.xmlRpc.id;
            break;
        case IDE_COMMAND_OBJECT_TYPES.odata:
                this.cmdType = IDE_COMMAND_OBJECT_TYPES.odata.id;
            break;
    }
};
SyncDtls.prototype.fnGetCmdType = function(){
    var cmTypeVal = this.cmdType,
        cmdType = null;
    
    switch(cmTypeVal){
        case IDE_COMMAND_OBJECT_TYPES.db.id:
                cmdType = IDE_COMMAND_OBJECT_TYPES.db;
            break;
        case IDE_COMMAND_OBJECT_TYPES.wsdl.id:
                cmdType = IDE_COMMAND_OBJECT_TYPES.wsdl;
            break;
        case IDE_COMMAND_OBJECT_TYPES.http.id:
                cmdType = IDE_COMMAND_OBJECT_TYPES.http;
            break;
        case IDE_COMMAND_OBJECT_TYPES.xmlRpc.id:
                cmdType = IDE_COMMAND_OBJECT_TYPES.xmlRpc;
            break;
        case IDE_COMMAND_OBJECT_TYPES.odata.id:
                cmdType = IDE_COMMAND_OBJECT_TYPES.odata;
            break;
    }
    return cmdType;
};

SyncDtls.prototype.fnSetConnection = function(value){
    this.conVal = value;
};
SyncDtls.prototype.fnGetConnection = function(){
    return this.conVal;
};

SyncDtls.prototype.fnSetDownload = function(value /*SyncDtlsDownload*/){
    this.download = value;
};
SyncDtls.prototype.fnGetDownload = function(){
    return this.download;
};
SyncDtls.prototype.fnDeleteDownload = function(){
    this.download = null;
};
SyncDtls.prototype.fnSetUpload = function(value /*SyncDtlsUpload*/){
    this.upload = value;
};
SyncDtls.prototype.fnGetUpload = function(){
    return this.upload;
};
SyncDtls.prototype.fnDeleteUpload = function(){
    this.upload = null;
};
SyncDtls.prototype.fnSetIsInitialDownload = function(value /*1 or 0*/){
    this.isInitialDownload = value;
};
SyncDtls.prototype.fnGetIsInitialDownload = function(){
    return this.isInitialDownload;
};

SyncDtls.prototype.fnResetObjectDetails = function(){
    var objToReset = this.fnGetDownload();
    if(objToReset){
        objToReset = new SyncDtlsDownload(objToReset);
        objToReset.fnResetObjectDetails();
        this.fnSetDownload(objToReset);
    }
    objToReset = this.fnGetUpload();
    if(objToReset){
        objToReset = new SyncDtlsUpload(objToReset);
        objToReset.fnResetObjectDetails();
        this.fnSetUpload(objToReset);
    }
};

function SyncDtlsDownload(params){
    if(!params)params={};
    this.command = params.command;
    this.inputParam = params.inputParam;
    this.outputParam = params.outputParam;
    this.expiry = params.expiry;
    this.disallowAfterExpiry = params.disallowAfterExpiry;
    this.cleanRecord = params.cleanRecord;
}

SyncDtlsDownload.prototype.fnSetCommand = function(value){
    this.command = value;
};
SyncDtlsDownload.prototype.fnGetCommand = function(){
    return this.command;
};
SyncDtlsDownload.prototype.fnDeleteCommand = function(){
    this.command = "";
};
SyncDtlsDownload.prototype.fnSetInputParam = function(value/*arry of CmdParamDtls*/){
    this.inputParam = value;
};
SyncDtlsDownload.prototype.fnGetInputParam = function(){
    return this.inputParam;
};
SyncDtlsDownload.prototype.fnDeleteInputParam = function(){
    this.inputParam = [];
};

SyncDtlsDownload.prototype.fnSetOutputParam = function(value/*arry of CmdParamDtls*/){
    this.outputParam = value;
};
SyncDtlsDownload.prototype.fnGetOutputParam = function(){
    return this.outputParam;
};
SyncDtlsDownload.prototype.fnDeleteOutputParam = function(){
    this.outputParam =[];
};

SyncDtlsDownload.prototype.fnSetDisallowAfterExpiry = function(value /*0 or 1*/){
    this.disallowAfterExpiry = value;
};
SyncDtlsDownload.prototype.fnGetDisallowAfterExpiry = function(){
    return this.disallowAfterExpiry;
};

SyncDtlsDownload.prototype.fnSetCleanRecord = function(value /*0 or 1*/){
    this.cleanRecord = value;
};
SyncDtlsDownload.prototype.fnGetCleanRecord = function(){
    return this.cleanRecord;
};

SyncDtlsDownload.prototype.fnSetExpiry = function(value /*CachingDetails*/){
    this.expiry = value;
};
SyncDtlsDownload.prototype.fnGetExpiry = function(){
    return this.expiry;
};
SyncDtlsDownload.prototype.fnDeleteExpiry = function(){
    this.expiry = null;
};

SyncDtlsDownload.prototype.fnResetObjectDetails = function(){
    var savedParams = [],
        aryTempParams=[],
        objParamMap=null,
        i=0,
        objExpiry = null;
        
    savedParams = this.fnGetInputParam();    
    if($.isArray(savedParams) && savedParams.length >0) {
        for ( i = 0; i <= savedParams.length - 1; i++) {
            objParamMap = new CmdParamDtls(savedParams[i]);
            aryTempParams.push(objParamMap);
        }
        this.fnSetInputParam(aryTempParams);
    }
    
    aryTempParams =[];
    savedParams =[];
    savedParams = this.fnGetOutputParam();    
    if(savedParams && $.isArray(savedParams) && savedParams.length >0) {
        for ( i = 0; i <= savedParams.length - 1; i++) {
            objParamMap = new CmdParamDtls(savedParams[i]);
            aryTempParams.push(objParamMap);
        }
        this.fnSetOutputParam(aryTempParams);
    }
    
    objExpiry = this.fnGetExpiry();
    if(objExpiry){
        objExpiry = new CachingDetails(objExpiry);
        this.fnSetExpiry(objExpiry);
    }
};



function SyncDtlsUpload(params){
    if(!params)params={};
    this.deleteRecordAfterUpload = params.deleteRecordAfterUpload;
    this.modifiedSameAsNewRecord = params.modifiedSameAsNewRecord;
    this.newRecord = params.newRecord;//CommandDetails
    this.modifiedRecord = params.modifiedRecord;//CommandDetails
    this.uploadCriteria = params.uploadCriteria;
    this.uploadWithin = params.uploadWithin;
}
SyncDtlsUpload.prototype.fnSetUploadCriteria = function(value){
    this.uploadCriteria = value;
};
SyncDtlsUpload.prototype.fnGetUploadCriteria = function(){
    return this.uploadCriteria;
};
SyncDtlsUpload.prototype.fnSetUploadWithin = function(value){
    this.uploadWithin = value;
};
SyncDtlsUpload.prototype.fnGetUploadWithin = function(){
    return this.uploadWithin;
};
SyncDtlsUpload.prototype.fnSetDeleteRecordAfterUpload = function(value/*0 or 1*/){
    this.deleteRecordAfterUpload = value;
};
SyncDtlsUpload.prototype.fnGetDeleteRecordAfterUpload = function(){
    return this.deleteRecordAfterUpload;
};
SyncDtlsUpload.prototype.fnSetModifiedSameAsNewRecord= function(value/*0 or 1*/){
    this.modifiedSameAsNewRecord = value;
};
SyncDtlsUpload.prototype.fnGetModifiedSameAsNewRecord = function(){
    return this.modifiedSameAsNewRecord;
};
SyncDtlsUpload.prototype.fnDeleteModifiedSameAsNewRecord = function(){
    this.modifiedSameAsNewRecord = null;
};

SyncDtlsUpload.prototype.fnSetNewRecord= function(value/*CommandDetails*/){
    this.newRecord = value;
};
SyncDtlsUpload.prototype.fnGetNewRecord = function(){
    return this.newRecord;
};
SyncDtlsUpload.prototype.fnDeleteNewRecord = function(){
    this.newRecord = null;
};
SyncDtlsUpload.prototype.fnSetModifiedRecord= function(value/*CommandDetails*/){
    this.modifiedRecord = value;
};
SyncDtlsUpload.prototype.fnGetModifiedRecord = function(){
    return this.modifiedRecord;
};
SyncDtlsUpload.prototype.fnDeleteModifiedRecord = function(){
    this.modifiedRecord = null;
};
SyncDtlsUpload.prototype.fnResetObjectDetails = function(){
    var objToReset = this.fnGetNewRecord();
    if(objToReset){
        objToReset = new CommandDetails(objToReset);
        objToReset.fnResetObjectDetails();
        this.fnSetNewRecord(objToReset);
    }
    objToReset = this.fnGetModifiedRecord();
    if(objToReset){
        objToReset = new CommandDetails(objToReset);
        objToReset.fnResetObjectDetails();
        this.fnSetModifiedRecord(objToReset);
    }
    
    if(mfUtil.isNullOrUndefined(this.fnGetUploadCriteria())){
        this.fnSetUploadCriteria("");
    }
    if(mfUtil.isNullOrUndefined(this.fnGetUploadWithin())){
        this.fnSetUploadWithin("1");
    }
};

function CommandDetails(params){
    if(!params)params= {};
    this.command = params.command;
    this.params = params.params;//array of CmdParamDtls
}
CommandDetails.prototype.fnSetCommand= function(value/*string*/){
    this.command = value;
};
CommandDetails.prototype.fnGetCommand = function(){
    return this.command;
};
CommandDetails.prototype.fnGetParams = function(){
    return this.params;
};
CommandDetails.prototype.fnSetParams = function(value/*array of CmdParamDtls*/){
    this.params = value;
};
CommandDetails.prototype.fnDeleteParams = function(){
    this.params = [];
};
CommandDetails.prototype.fnResetObjectDetails = function(){
    var savedParams = [],
        aryTempParams=[],
        objParamMap=null,
        i=0;
        
    savedParams = this.fnGetParams();    
    if($.isArray(savedParams) && savedParams.length >0) {
        for ( i = 0; i <= savedParams.length - 1; i++) {
            objParamMap = new CmdParamDtls(savedParams[i]);
            aryTempParams.push(objParamMap);
        }
        this.fnSetParams(aryTempParams);
    }
};


function CmdParamDtls(options){
    if(!options)options = {};
    this.para = options.para;
    this.val = options.val;
}
CmdParamDtls.prototype.fnGetVal=function(){
    return this.val;
};
CmdParamDtls.prototype.fnGetPara = function(){
    return this.para;
};
CmdParamDtls.prototype.fnSetVal=function(value){
     this.val = value;
};
CmdParamDtls.prototype.fnSetPara = function(value){
     this.para = value;
};

function CachingDetails(options){
    if(!options)options = {};
    this.type = options.type;
    this.frequency = options.frequency;
    this.condition = options.condition;
}
CachingDetails.prototype.fnGetType=function(){
    return this.type;
};
CachingDetails.prototype.fnSetType=function(value){
     this.type = value;
};

CachingDetails.prototype.fnGetFrequency=function(){
    return this.frequency;
};
CachingDetails.prototype.fnSetFrequency=function(value){
     this.frequency = value;
};

CachingDetails.prototype.fnGetCondition=function(){
    return this.condition;
};
CachingDetails.prototype.fnSetCondition=function(value){
     this.condition = value;
};

var mfOflnTblHelpers =(function(){
    
    function _getWebserviceTypeByText(text){
        var wsType ="";
        if(text){
            switch(text){
                case WEB_SERVICE_TYPE_TEXT.http:
                    wsType = WEB_SERVICE_TYPE.http;
                break;    
                case WEB_SERVICE_TYPE_TEXT.wsdlSoap:
                    wsType = WEB_SERVICE_TYPE.wsdlSoap;
                break;
                case WEB_SERVICE_TYPE_TEXT.rpcXml:
                    wsType = WEB_SERVICE_TYPE.rpcXml;
                break;
            }
        }
        return wsType;
    }
    var _dbCommandHelpers = (function () {
        function _parseUICollectionHidField() {
            var aryDbObjects = [];
            var hidFieldValue = $(':hidden[id$="hidAllDbCommands"]').val();
            if (hidFieldValue) {
                aryDbObjects = $.parseJSON(hidFieldValue);
            }
            return aryDbObjects;
        }
        function _getObjectsOfIdAndNameForDdlBind() {
            var aryObjsOfIdAndName = [];
            var aryDbObjects = _parseUICollectionHidField();
            if (aryDbObjects && aryDbObjects.length > 0) {
                for (var i = 0; i <= aryDbObjects.length - 1; i++) {
                    var dbObject = aryDbObjects[i];
                    aryObjsOfIdAndName.push({
                        val: dbObject.commandId,
                        text: dbObject.commandName
                    });
                }
            }
            return aryObjsOfIdAndName;
        }
        function _getCommandObject(cmdId) {
            var cmdObj = null;
            var aryCmdObjects = _parseUICollectionHidField();
            if (aryCmdObjects && aryCmdObjects.length > 0) {
                for (var i = 0; i <= aryCmdObjects.length - 1; i++) {
                    cmdObj = aryCmdObjects[i];
                    if (cmdObj.commandId === cmdId) {
                        break;
                    }
                    else {
                        cmdObj = null;
                    }
                }
            }
            return cmdObj;
        }
        function _getInputParamsForUI(cmdId) {
            var aryInputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.paramJson) {
                var objFinalInputParam = $.parseJSON(cmdObj.paramJson);
                if (objFinalInputParam) {
                    $.each(objFinalInputParam, function (index, value) {
                        var objParamDtl = value;
                        if (objParamDtl) {
                            aryInputParams.push(objParamDtl.para);
                        }
                    });
                }
            }
            return aryInputParams;
        }
        function _getOutputParamsForUI(cmdId) {
            var aryOutputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.columns) {
                aryOutputParams = cmdObj.columns.split(',');
            }
            return aryOutputParams;
        }
        function _getAllDbCmdOfSameConn(connectionId) {
            var aryObjsOfSameConnectionId = [];
            var aryAllDbObjects = _parseUICollectionHidField();
            aryObjsOfSameConnectionId = $.grep(aryAllDbObjects, function (value, index) {
                return value.connectorId === connectionId;
            });
            return aryObjsOfSameConnectionId;
        }
        function _getAllDbCmdOfSameConnForDdlBind(connectionId) {
            var aryObjsOfSameConnectionId = _getAllDbCmdOfSameConn(connectionId);
            var aryForDdlBind = [];
            $.each(aryObjsOfSameConnectionId, function (index, value) {
                aryForDdlBind.push({
                    val: value.commandId,
                    text: value.commandName
                });
            });
            return aryForDdlBind;
        }
        function _getConnectionIdOfCmd(cmdId) {
            var objCmd = _getCommandObject(cmdId);
            if (objCmd) {
                return objCmd.connectorId;
            }
            else {
                return "";
            }
        }
        function _getDbObjectsByCmdType(cmdType) {
            var aryAllDbCmds = _parseUICollectionHidField();
            var aryFilteredObjects = [];
            if (aryAllDbCmds && $.isArray(aryAllDbCmds) && aryAllDbCmds.length > 0) {
                switch (cmdType) {
                    case DB_COMMAND_TYPE.Select:
                        aryFilteredObjects = $.grep(aryAllDbCmds, function (value, index) {
                            return value.commandType === DB_COMMAND_TYPE.SELECT;
                        });
                        break;
                    case DB_COMMAND_TYPE.Insert:
                        aryFilteredObjects = $.grep(aryAllDbCmds, function (value, index) {
                            return value.commandType === DB_COMMAND_TYPE.Insert;
                        });    
                        break;
                    case DB_COMMAND_TYPE.Update:
                        break;
                    case DB_COMMAND_TYPE.Delete:
                        break;
                }
            }
            return aryFilteredObjects;
        }
        function _getDbObjectsByCmdTypeByConn(cmdType,connectionId) {
            var aryDbCmds = _getAllDbCmdOfSameConn(connectionId);
            var aryFilteredObjects = [];
            if (aryDbCmds && $.isArray(aryDbCmds) && aryDbCmds.length > 0) {
                switch (cmdType) {
                    case DB_COMMAND_TYPE.Select:
                        aryFilteredObjects = $.grep(aryDbCmds, function (value, index) {
                            return value.commandType === DB_COMMAND_TYPE.Select;
                        });
                        break;
                    case DB_COMMAND_TYPE.Insert:
                        aryFilteredObjects = $.grep(aryDbCmds, function (value, index) {
                            return value.commandType === DB_COMMAND_TYPE.Insert;
                        });    
                        break;
                    case DB_COMMAND_TYPE.Update:
                        aryFilteredObjects = $.grep(aryDbCmds, function (value, index) {
                            return value.commandType === DB_COMMAND_TYPE.Update;
                        });    
                        break;
                    case DB_COMMAND_TYPE.Delete:
                        break;
                }
            }
            return aryFilteredObjects;
        }
        function _getDbObjectCmdTypeText(cmdType){
            var commandType = "";
            switch (cmdType) {
                case DB_COMMAND_TYPE.SELECT:
                    commandType = "SELECT";
                    break;
                case DB_COMMAND_TYPE.INSERT:
                    commandType = "INSERT";
                    break;
                case DB_COMMAND_TYPE.UPDATE:
                    ommandType = "UPDATE";
                    break;
                case DB_COMMAND_TYPE.DELETE:
                    commandType = "DELETE";
                    break;
            }
            return commandType;
        }
        function _getCommandsAsOptionsForDdl(
            cmds,
            addSelectOption,
            selectOptionValue){
            
            var aryCmds = cmds,
               strOptions = "",i=0,objDbConn = null;
            if(addSelectOption != null && 
                  addSelectOption === true){
                
                strOptions += '<option value='+selectOptionValue+'>Select</option>';
            }
            if(aryCmds && $.isArray(aryCmds) && aryCmds.length>0){
                for(i=0;i<=aryCmds.length-1;i++){
                    objDbConn = aryCmds[i];
                    if(objDbConn){
                        strOptions += 
                        '<option value='+objDbConn.commandId+'>'+
                          objDbConn.commandName+'</option>';
                    }
                }
            }
            return strOptions;
        }
        return {
            getObjectsForDdlBind: function () {
                return _getObjectsOfIdAndNameForDdlBind();
            },
            getInputParamsForCmd: function (cmdId) {
                if (!cmdId) throw exceptionMessage.argumentsNull;
                return _getInputParamsForUI(cmdId);
            },
            getObjectsByCmdId: function (cmdIds/*array of command ids*/) {
                var aryObjects = [];
                if (cmdIds && $.isArray(cmdIds) && cmdIds.length > 0) {
                    for (var i = 0; i <= cmdIds.length - 1; i++) {
                        aryObjects.push(_getCommandObject(cmdIds[i]));
                    }
                }
                return aryObjects;
            },
            getDbCmdsForDdlBindInBatchProcMode: function (connectionId) {
                if (connectionId) {
                    return _getAllDbCmdOfSameConnForDdlBind(connectionId);
                }
                else {
                    return this.getObjectsForDdlBind();
                }
            },
            getConnectionIdOfCmd: function (cmdId) {
                return _getConnectionIdOfCmd(cmdId);
            },
            getAllDbObjects: function () {
                return _parseUICollectionHidField();
            },
            getDbObjectsByCmdType: function (cmdType) {
                return _getDbObjectsByCmdType(cmdType);
            },
            getObjectByCmdId: function (cmdId) {
                if(!cmdId)return null;
                return _getCommandObject(cmdId);
            },
            getDbObjectCmdTypeText: function (cmdType) {
               return _getDbObjectCmdTypeText(cmdType);
            },
            getOutputParamsForCmd: function (cmdId) {
                return _getOutputParamsForUI(cmdId);
            },
            getSelectCommandsAsOptionsForDdl:function(connId,
                    addSelectOption,
                    selectOptionValue){
                
                var aryCmds = _getDbObjectsByCmdTypeByConn(
                        DB_COMMAND_TYPE.Select,
                        connId
                    );
                
                return _getCommandsAsOptionsForDdl(aryCmds,
                        addSelectOption,
                        selectOptionValue);
            },
            getInsertCommandsAsOptionsForDdl:function(connId,
                    addSelectOption,
                    selectOptionValue){
                
                var aryCmds = _getDbObjectsByCmdTypeByConn(
                        DB_COMMAND_TYPE.Insert,
                        connId
                    );
                
                return _getCommandsAsOptionsForDdl(aryCmds,
                        addSelectOption,
                        selectOptionValue);
            },
            getUpdateCommandsAsOptionsForDdl:function(connId,
                    addSelectOption,
                    selectOptionValue){
                
                var aryCmds = _getDbObjectsByCmdTypeByConn(
                        DB_COMMAND_TYPE.Update,
                        connId
                    );
                
                return _getCommandsAsOptionsForDdl(aryCmds,
                        addSelectOption,
                        selectOptionValue);
            }
        };

    })();
    var _wsCommandHelpers = (function () {
        function _parseUICollectionHidField(wsType/*WEB_SERVICE_TYPE*/) {
            var aryAllWsObjects = _getAllWsObjects();
            var aryObjectsByType = [];
            switch (wsType) {
                case WEB_SERVICE_TYPE.wsdlSoap:
                    aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                        return value.webserviceType.trim() === WEB_SERVICE_TYPE_TEXT.wsdlSoap;
                    });
                    break;
                case WEB_SERVICE_TYPE.xmlRpc:
                    aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                        return value.webserviceType.trim() === WEB_SERVICE_TYPE_TEXT.xmlRpc;
                    });
                    break;
                case WEB_SERVICE_TYPE.http:
                    aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                        return value.webserviceType.trim() === WEB_SERVICE_TYPE_TEXT.http;
                    });
                    break;
            }
            return aryObjectsByType;
        }
        function _getAllWsObjects() {
            var aryWsObjects = [];
            var hidFieldValue = $(':hidden[id$="hidAllWsCommands"]').val();
            if (hidFieldValue) {
                aryWsObjects = $.parseJSON(hidFieldValue);
            }
            return aryWsObjects;
        }
        function _getCmdObjectById(cmdId, wsType/*Constants.webserviceType*/) {
            var aryWsObjects = _parseUICollectionHidField(wsType);
            var wsObject = null;
            wsObject = $.grep(aryWsObjects, function (value, index) {
                return value.commandId === cmdId;
            });
            return wsObject[0];
        }
        function _getWsdlSimpleTypeForUI(arrayOfSimpleType, containerObjectName) {
            var aryFinalSimpleType = [];
            if (arrayOfSimpleType && $.isArray(arrayOfSimpleType) && arrayOfSimpleType.length > 0) {
                for (var i = 0; i <= arrayOfSimpleType.length - 1; i++) {
                    var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arrayOfSimpleType[i].S_Name :
                                                arrayOfSimpleType[i].S_Name;
                    aryFinalSimpleType.push(strSimpleTypeForUI);
                }
            }
            return aryFinalSimpleType;
        }
        function _getWsdlComplexTypeForUI(complexTypes/*array*/, containerObjectName) {
            var aryFinalComplexType = [];
            if (complexTypes && $.isArray(complexTypes) && complexTypes.length > 0) {
                for (var i = 0; i <= complexTypes.length - 1; i++) {
                    var objComplexType = complexTypes[i];
                    var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objComplexType.S_Types, objComplexType.C_Name);
                    var aryInnerComplexType = _getWsdlComplexTypeForUI(objComplexType.C_Types, objComplexType.C_Name);
                    for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                        var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arySimpleTypesForUI[j] :
                                                arySimpleTypesForUI[j];
                        aryFinalComplexType.push(strSimpleTypeForUI);
                    }
                    for (var k = 0; k <= aryInnerComplexType.length - 1; k++) {
                        var strComplexTypeForUI = containerObjectName ? containerObjectName + "." + aryInnerComplexType[k] :
                                                aryInnerComplexType[k];
                        aryFinalComplexType.push(strComplexTypeForUI);
                    }
                }
            }
            return aryFinalComplexType;
        }
        var _parseXmlRpcParams = (function () {
            return {
                getAllPossibleXmlRpcParams: function (paramsArray) {
                    var aryFinalCombinationsOfText = [];
                    var aryOfFinalProperties = [];
                    var aryParamsFromJson = paramsArray;
                    var strVlue = "";
                    var aryOfPossibleCombinationFromObject = [],
                        i=0,k=0;
                    if (aryParamsFromJson.length > 0) {
                        for (i = 0; i <= aryParamsFromJson.length - 1; i++) {
                            var strPropFinalText = "";
                            aryOfPossibleCombinationFromObject = [];
                            aryOfPossibleCombinationFromObject = _getPossibleTextValsFromObject(aryParamsFromJson[i], strPropFinalText);
                            if (aryOfPossibleCombinationFromObject) {
                                if (typeof aryOfPossibleCombinationFromObject === "string") {
                                    aryOfFinalProperties.push(aryOfPossibleCombinationFromObject);
                                }
                                else if ($.isArray(aryOfPossibleCombinationFromObject)) {
                                    for (k = 0; k <= aryOfPossibleCombinationFromObject.length - 1; k++) {
                                        aryOfFinalProperties.push(aryOfPossibleCombinationFromObject[k]);
                                    }
                                }
                            }
                        }
                    }
                    i = 0;
                    for (i = 0; i <= aryOfFinalProperties.length - 1; i++) {
                        var aryOfFinalCombination = aryOfFinalProperties[i];
                        if (typeof aryOfFinalCombination === "string") {
                            aryFinalCombinationsOfText.push(aryOfFinalCombination);
                        }
                        else if ($.isArray(aryOfFinalCombination)) {
                            for (k = 0; k <= aryOfFinalCombination.length - 1; k++) {
                                aryFinalCombinationsOfText.push(aryOfFinalCombination[k]);
                            }
                        }
                    }
                    return aryFinalCombinationsOfText;
                }
            };
            function _getPossibleTextValsFromObject(propObject, previousStringEvluted /*string*/) {
                var aryFinalCombintionsOfText = [];
                var aryOfCombinationsInnerProp = [];
                if ($.isPlainObject(propObject)) {
                    previousStringEvluted = addTextFromPropObject(propObject, previousStringEvluted);
                    if (propObject.hasOwnProperty("inparam")) {
                        aryOfCombinationsInnerProp = _loopThroughInnerProperty(propObject.inparam, previousStringEvluted);
                    }
                }
                if (aryOfCombinationsInnerProp.length > 0) {
                    for (var i = 0; i <= aryOfCombinationsInnerProp.length - 1; i++) {
                        aryFinalCombintionsOfText.push(aryOfCombinationsInnerProp[i]);
                    }
                    return aryFinalCombintionsOfText;
                }
                else {
                    aryFinalCombintionsOfText.push(previousStringEvluted);
                    return aryFinalCombintionsOfText;
                }

            }
            function addTextFromPropObject(propObject, previousPropText) {
                if (previousPropText === "") {
                    return propObject.name;
                }
                else {
                    return previousPropText + "." + propObject.name;
                }

            }
            function _loopThroughInnerProperty(innerPropArryObject, previousPropTextEvaluated) {
                var aryOfCombinationsInnerProp = [];

                if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
                    for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
                        var strStrtString = previousPropTextEvaluated;
                        if ($.isPlainObject(innerPropArryObject[i])) {
                            var aryValsFromObject = _getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString);
                            if(aryValsFromObject && $.isArray(aryValsFromObject) && aryValsFromObject.length>0){
                                for(var k=0;k<=aryValsFromObject.length-1;k++){
                                    aryOfCombinationsInnerProp.push(aryValsFromObject[k]);
                                }
                            }
                            else{
                                if(typeof aryValsFromObject === "string"){
                                    aryOfCombinationsInnerProp.push(aryValsFromObject);
                                }
                            }
                            
                        }

                    }
                }
                return aryOfCombinationsInnerProp;
            }
        })();
        function _getObjectsOfIdAndNameForDdlBind(wsType) {
            var aryObjsOfIdAndName = [];
            var aryWsObjects = _parseUICollectionHidField(wsType);
            if (aryWsObjects && aryWsObjects.length > 0) {
                for (var i = 0; i <= aryWsObjects.length - 1; i++) {
                    var wsObject = aryWsObjects[i];
                    aryObjsOfIdAndName.push({
                        val: wsObject.commandId,
                        text: wsObject.commandName
                    });
                }
            }
            return aryObjsOfIdAndName;
        }
        function _getXmlRpcInputParamsForUI(cmdId, wsType/*Constants.webserviceType*/) {
            var aryInputParams = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.parameter) {
                var objFinalInputParam = $.parseJSON(cmdObj.parameter);
                aryFinalTypesForUI = _parseXmlRpcParams.getAllPossibleXmlRpcParams(objFinalInputParam.param);
                return aryFinalTypesForUI;
            }
        }
        
        function _getXmlRpcOutputParamsForUI(tags/*json of output stored in database.*/) {
            if (tags) {
                var objFinalOutputParam = $.parseJSON(tags);
                aryFinalTypesForUI = _parseXmlRpcParams.getAllPossibleXmlRpcParams(objFinalOutputParam.param);
                return aryFinalTypesForUI;
            }
        }
        function _getHttpInputParamsForUI(cmdId, wsType) {
            var aryInputParams = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.parameter) {
                aryInputParams = cmdObj.parameter.split(',');
            }
            return aryInputParams;
        }
        
        function _getWsdlInputParamsForUI(cmdId, wsType/*WEB_SERVICE_TYPE*/) {
            var aryInputParams = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.parameter) {
                var objFinalInputParam = $.parseJSON(cmdObj.parameter);
                var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objFinalInputParam.S_Types, "");
                var aryComplexTypesForUI = _getWsdlComplexTypeForUI(objFinalInputParam.C_Types, "");
                var aryFinalTypesForUI = [];
                for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                    aryFinalTypesForUI.push(arySimpleTypesForUI[j]);
                }
                for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++) {
                    aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
                }
                return aryFinalTypesForUI;
            }
        }
        
        function _getWsdlOutputParamsForUI(tags/**json of output stored in database.**/) {
            if (tags) {
                var objFinalOutputParam = $.parseJSON(tags);
                var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objFinalOutputParam.S_Types, "");
                var aryComplexTypesForUI = _getWsdlComplexTypeForUI(objFinalOutputParam.C_Types, "");
                var aryFinalTypesForUI = [];
                for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                    aryFinalTypesForUI.push(arySimpleTypesForUI[j]);
                }
                for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++) {
                    aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
                }
                return aryFinalTypesForUI;
            }
        }
        
        function _getOutputParamsForCmd(cmdId, wsType/*WEB_SERVICE_TYPE*/) {
            var aryTags = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.tag) {
                switch(wsType){
                    case WEB_SERVICE_TYPE.xmlRpc:
                            aryTags = _getXmlRpcOutputParamsForUI(cmdObj.tag);
                    break;
                    case WEB_SERVICE_TYPE.http:
                            var objTags = $.parseJSON(cmdObj.tag);
                            $.each(objTags, function () {
                                aryTags.push(this.name);
                            });
                    break;
                    case WEB_SERVICE_TYPE.wsdlSoap:
                            aryTags = _getWsdlOutputParamsForUI(cmdObj.tag);
                    break;
                }
            }
            return aryTags;
        }
        // function _getAllDbCmdOfSameConn(connectionId) {
        //     var aryObjsOfSameConnectionId = [];
        //     var aryAllObjects = _parseUICollectionHidField();
        //     aryObjsOfSameConnectionId = $.grep(aryAllObjects, function (value, index) {
        //         return value.connectorId === connectionId;
        //     });
        //     return aryObjsOfSameConnectionId;
        // }
        function _getAllCmdsOfSameConnByWsType(connectionId,wsType/*IDE_COMMAND_OBJECT_TYPES*/) {
            var aryObjsOfSameConnectionId = [],
                webServiceType = "";
            switch(wsType){
                case IDE_COMMAND_OBJECT_TYPES.wsdl:
                    webServiceType = WEB_SERVICE_TYPE.wsdlSoap;
                break;
                case IDE_COMMAND_OBJECT_TYPES.http:
                    webServiceType = WEB_SERVICE_TYPE.http;
                break;
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                    webServiceType = WEB_SERVICE_TYPE.rpcXml;
                break;
            }
            var aryAllObjects = _parseUICollectionHidField(webServiceType);
            aryObjsOfSameConnectionId = $.grep(aryAllObjects, function (value, index) {
                return value.connectorId === connectionId;
            });
            return aryObjsOfSameConnectionId;
        }
        function _getCmdsAsOptionsOfDdlByConnIdAndWsType(connectionId,
                    wsType/*IDE_COMMAND_OBJECT_TYPES*/,
                    addSelectOption,
                    selectOptionValue){
            
            var aryCmds = _getAllCmdsOfSameConnByWsType(connectionId,wsType),
               strOptions = "",i=0,objCmd = null;
            
            if(addSelectOption != null && 
                  addSelectOption === true){
                
                strOptions += '<option value='+selectOptionValue+'>Select</option>';
            }
            if(aryCmds && $.isArray(aryCmds) && aryCmds.length>0){
                for(i=0;i<=aryCmds.length-1;i++){
                    objCmd = aryCmds[i];
                    if(objCmd){
                        strOptions += 
                        '<option value='+objCmd.commandId+'>'+
                          objCmd.commandName+'</option>';
                    }
                }
            }
            return strOptions;
        }
        return {
            getObjectsForDdlBind: function (wstype) {
                return _getObjectsOfIdAndNameForDdlBind(wstype);
            },
            getInputParamsForCmd: function (cmdId, wsType/*WEB_SERVICE_TYPE*/) {
                var aryInputParams = [];
                switch (wsType) {
                    case WEB_SERVICE_TYPE.wsdlSoap:
                        aryInputParams = _getWsdlInputParamsForUI(cmdId, wsType);
                        break;
                    case WEB_SERVICE_TYPE.xmlRpc:
                        aryInputParams = _getXmlRpcInputParamsForUI(cmdId, wsType);
                        break;
                    case WEB_SERVICE_TYPE.http:
                        aryInputParams = _getHttpInputParamsForUI(cmdId, wsType);
                        break;
                }
                return aryInputParams;
            },
            //Tanika
            getOutputParamsForCmd: function (cmdId, wsType/*WEB_SERVICE_TYPE*/) {
                return _getOutputParamsForCmd(cmdId, wsType);
            },
            getWsObjectById:function(cmdId){
                if(!cmdId)return null;
                var aryAllWsObjects = _getAllWsObjects();
                var wsObject = null;
                wsObject = $.grep(aryAllWsObjects, function (value, index) {
                    return value.commandId === cmdId;
                });
                return wsObject[0];
            },
            getCmdsAsOptionsOfDdlByConnAndWsType:function(
                    connId,
                    wsType/*IDE_COMMAND_OBJECT_TYPES*/,
                    addSelectOption,
                    selectOptionValue){
                
                return _getCmdsAsOptionsOfDdlByConnIdAndWsType(connId,
                        wsType,
                        addSelectOption,
                        selectOptionValue);
            }
        };

    })();
    var _odataCommandHelpers = (function () {
        function _parseUICollectionHidField() {
            var aryOdataObjects = [];
            var hidFieldValue = $(':hidden[id$="hidAllOdataCommands"]').val();
            if (hidFieldValue) {
                aryOdataObjects = $.parseJSON(hidFieldValue);
            }
            return aryOdataObjects;
        }
        function _getObjectsOfIdAndNameForDdlBind() {
            var aryObjsOfIdAndName = [];
            var aryOdataObjects = _parseUICollectionHidField();
            if (aryOdataObjects && aryOdataObjects.length > 0) {
                for (var i = 0; i <= aryOdataObjects.length - 1; i++) {
                    var odataObject = aryOdataObjects[i];
                    aryObjsOfIdAndName.push({
                        val: odataObject.commandId,
                        text: odataObject.commandName
                    });
                }
            }
            return aryObjsOfIdAndName;
        }
        function _getCommandObject(cmdId) {
            var cmdObj = null;
            var aryCmdObjects = _parseUICollectionHidField();
            if (aryCmdObjects && aryCmdObjects.length > 0) {
                for (var i = 0; i <= aryCmdObjects.length - 1; i++) {
                    cmdObj = aryCmdObjects[i];
                    if (cmdObj.commandId === cmdId) {
                        break;
                    }
                    else {
                        cmdObj = null;
                    }
                }
            }
            return cmdObj;
        }
        
        function _getInputParamsForUI(cmdId) {
            var aryInputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.inputParamJson) {
                var objFinalInputParam = $.parseJSON(cmdObj.inputParamJson);
                if (objFinalInputParam) {
                    $.each(objFinalInputParam, function (index, value) {
                        var objParamDtl = value;
                        if (objParamDtl) {
                            aryInputParams.push(objParamDtl.para);
                        }
                    });
                }
            }
            return aryInputParams;
        }
        function _getOutputParamsForUI(cmdId) {
            var aryOuptputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.outputParamJson) {
                var objFinalOutputParam = $.parseJSON(cmdObj.outputParamJson);
                if (objFinalOutputParam) {
                    $.each(objFinalOutputParam, function (index, value) {
                        var objParamDtl = value;
                        if (objParamDtl) {
                            aryOuptputParams.push(objParamDtl.name);
                        }
                    });
                }
            }
            return aryOuptputParams;
        }
        
        function _getAllCmdsOfSameConn(connectionId) {
            var aryObjsOfSameConnectionId = [];
            var aryAllObjects = _parseUICollectionHidField();
            aryObjsOfSameConnectionId = $.grep(aryAllObjects, function (value, index) {
                return value.connectorId === connectionId;
            });
            return aryObjsOfSameConnectionId;
        }
        function _getCmdsAsOptionsOfDdlByConn(connId,
                    addSelectOption,
                    selectOptionValue){
            
            var aryCmds = _getAllCmdsOfSameConn(connId),
               strOptions = "",i=0,objCmd = null;
            if(addSelectOption != null && 
                  addSelectOption === true){
                
                strOptions += '<option value='+selectOptionValue+'>Select</option>';
            }
            if(aryCmds && $.isArray(aryCmds) && aryCmds.length>0){
                for(i=0;i<=aryCmds.length-1;i++){
                    objCmd = aryCmds[i];
                    if(objCmd){
                        strOptions += 
                        '<option value='+objCmd.commandId+'>'+
                          objCmd.commandName+'</option>';
                    }
                }
            }
            return strOptions;
        }
        return {
            getObjectsForDdlBind: function () {
                return _getObjectsOfIdAndNameForDdlBind();
            },
            getAllOdataObjects: function () {
                return _parseUICollectionHidField();
            },
            getInputParamsForCmd: function (cmdId) {
                return _getInputParamsForUI(cmdId);
            },
            getOdataObjectById:function(cmdId){
                if(!cmdId)return null;
                var aryAllOdataObjects = _parseUICollectionHidField();
                var oDataObject = null;
                oDataObject = $.grep(aryAllOdataObjects, function (value, index) {
                    return value.commandId === cmdId;
                });
                return oDataObject[0];
            },
            getOutputParamsForCmd: function (cmdId) {
                return _getOutputParamsForUI(cmdId);
            },
            getCmdsAsOptionsOfDdlByConn:function(connId,
                addSelectOption,
                selectOptionValue){
                
                return _getCmdsAsOptionsOfDdlByConn(connId,
                        addSelectOption,
                        selectOptionValue);
            }
        };

    })();
    var _dbConnectionHelpers=(function(){
         function _parseUICollectionHidField() {
             var aryDbConnsObjects = [];
             var hidFieldValue = $(':hidden[id$="hidAllDbConnections"]').val();
             if (hidFieldValue) {
                 aryDbConnsObjects = $.parseJSON(hidFieldValue);
             }
             return aryDbConnsObjects;
          }
          function _getConns(){
              return _parseUICollectionHidField();
          }
          function _getConnsAsOptionsOfDdl(addSelectOption,selectOptionValue){
              var aryDbConns = _getConns(),
                 strOptions = "",i=0,objDbConn = null;
              if(addSelectOption != null && 
                    addSelectOption === true){
                  
                  strOptions += '<option value='+selectOptionValue+'>Select</option>';
              }
              if(aryDbConns && $.isArray(aryDbConns) && aryDbConns.length>0){
                  for(i=0;i<=aryDbConns.length-1;i++){
                      objDbConn = aryDbConns[i];
                      if(objDbConn){
                          strOptions += 
                          '<option value='+objDbConn.ConnectorId+'>'+
                            objDbConn.ConnectionName+'</option>';
                      }
                  }
              }
              return strOptions;
          }
          function _getConnById(connId){
              var aryConns = _getConns(),
                objConn = null;
            if(aryConns && $.isArray(aryConns) && aryConns.length>0){
                aryConns = $.grep(aryConns,function(value,index){
                        return value.ConnectorId === connId;
                });
            }
            return aryConns[0];
          }
        return {
            getConnsAsOptionsOfDdl:function(addSelectOption,selectOptionValue){
                return _getConnsAsOptionsOfDdl(
                        addSelectOption,
                        selectOptionValue);
            },
            getConnById:function(connId){
               return _getConnById(connId);
            }
        };
    })();
    var _wsConnectionHelpers=(function(){
        
        function _parseUICollectionHidField() {
             var aryConnsObjects = [];
             var hidFieldValue = $(':hidden[id$="hidAllWsConnections"]').val();
             if (hidFieldValue) {
                 aryConnsObjects = $.parseJSON(hidFieldValue);
             }
             return aryConnsObjects;
          }
          function _getConns(){
              return _parseUICollectionHidField();
          }
          function _getWsdlConns(){
              var aryConnsObjects =_getConns();
              if(aryConnsObjects && $.isArray(aryConnsObjects) && 
                aryConnsObjects.length>0){
                
                aryConnsObjects  = $.grep(aryConnsObjects,function(value,index){
                     if(value && value.WebserviceType){
                        return value.WebserviceType.trim() === WEB_SERVICE_TYPE_TEXT.wsdlSoap;
                     }
                });
              }
              return aryConnsObjects;
          }
          function _getHttpConns(){
              var aryConnsObjects =_getConns();
              if(aryConnsObjects && $.isArray(aryConnsObjects) && 
                aryConnsObjects.length>0){
                
                aryConnsObjects  = $.grep(aryConnsObjects,function(value,index){
                    if(value && value.WebserviceType){ 
                     return value.WebserviceType.trim() === WEB_SERVICE_TYPE_TEXT.http;
                    }
                });
              }
              return aryConnsObjects; 
          }
          function _getXmlRpcConns(){
              var aryConnsObjects =_getConns();
              if(aryConnsObjects && $.isArray(aryConnsObjects) && 
                aryConnsObjects.length>0){
                
                aryConnsObjects  = $.grep(aryConnsObjects,function(value,index){
                     if(value && value.WebserviceType){
                     return value.WebserviceType.trim() === WEB_SERVICE_TYPE_TEXT.rpcXml;
                     }
                });
              }
              return aryConnsObjects; 
          }
          function _getConnsAsOptionsOfDdl(
                wsType,
                addSelectOption,
                selectOptionValue){
              
              var aryConns = [],
                 strOptions = "",i=0,objConn = null;
              
              switch(wsType){
                case WEB_SERVICE_TYPE.http:
                        aryConns = _getHttpConns();
                    break;
                case WEB_SERVICE_TYPE.wsdlSoap:
                        aryConns = _getWsdlConns();
                    break;
                case WEB_SERVICE_TYPE.rpcXml:
                        aryConns = _getXmlRpcConns();
                    break;
              }
              
              if(addSelectOption != null && 
                    addSelectOption === true){
                  
                  strOptions += '<option value='+selectOptionValue+'>Select</option>';
              }
              if(aryConns && $.isArray(aryConns) && aryConns.length>0){
                  for(i=0;i<=aryConns.length-1;i++){
                      objConn = aryConns[i];
                      if(objConn){
                          strOptions += 
                          '<option value='+objConn.ConnectorId+'>'+
                            objConn.ConnectionName+'</option>';
                      }
                  }
              }
              return strOptions;
          }
          function _getConnById(connId){
              var aryConns = _getConns(),
                objConn = null;
            if(aryConns && $.isArray(aryConns) && aryConns.length>0){
                aryConns = $.grep(aryConns,function(value,index){
                        return value.ConnectorId === connId;
                });
            }
            return aryConns[0];
          }
          
        return {
            getConnsAsOptionsOfDdl:function(wsType/*WEB_SERVICE_TYPE*/,
                addSelectOption,
                selectOptionValue){
                
                return _getConnsAsOptionsOfDdl(
                        wsType,
                        addSelectOption,
                        selectOptionValue);
            },
            getConnById:function(connId){
               return _getConnById(connId);
            }
        };
        
    })();
    var _odataConnectionHelpers=(function(){
        function _parseUICollectionHidField() {
             var aryConnsObjects = [];
             var hidFieldValue = $(':hidden[id$="hidAllOdataConnections"]').val();
             if (hidFieldValue) {
                 aryConnsObjects = $.parseJSON(hidFieldValue);
             }
             return aryConnsObjects;
          }
          function _getConns(){
              return _parseUICollectionHidField();
          }
          function _getConnsAsOptionsOfDdl(addSelectOption,selectOptionValue){
              var aryConns = _getConns(),
                 strOptions = "",i=0,objConn = null;
              if(addSelectOption != null && 
                    addSelectOption === true){
                  
                  strOptions += '<option value='+selectOptionValue+'>Select</option>';
              }
              if(aryConns && $.isArray(aryConns) && aryConns.length>0){
                  for(i=0;i<=aryConns.length-1;i++){
                      objConn = aryConns[i];
                      if(objConn){
                          strOptions += 
                          '<option value='+objConn.ConnectorId+'>'+
                            objConn.ConnectionName+'</option>';
                      }
                  }
              }
              return strOptions;
          }
          function _getConnById(connId){
              var aryConns = _getConns(),
                objConn = null;
            if(aryConns && $.isArray(aryConns) && aryConns.length>0){
                aryConns = $.grep(aryConns,function(value,index){
                        return value.ConnectorId === connId;
                });
            }
            return aryConns[0];
          }
        return {
            getConnsAsOptionsOfDdl:function(addSelectOption,selectOptionValue){
                return _getConnsAsOptionsOfDdl(
                        addSelectOption,
                        selectOptionValue);
            },
            getConnById:function(connId){
               return _getConnById(connId);
            }
        };
        
    })();
    return {
        getOflnTblTypeTextByValue:function(value){
            var strTableType = "";
            switch(value){
                case OFFLINE_TABLE_TYPE.UploadData:
                    strTableType = "Upload Data";
                break;
                case OFFLINE_TABLE_TYPE.DownloadData:
                    strTableType = "Download";
                break;
                case OFFLINE_TABLE_TYPE.DownloadAndUploadData:
                    strTableType = "Upload And Download";
                break;
            }
            return strTableType;
        },
        getOflnTblSyncOnTypeTextByValue:function(value){
            var strSyncOnType = "";
            switch(value){
                case OFFLINE_DT_SYNC_ON_TYPE.OnLogin:
                    strSyncOnType = "Login";
                break;    
                case OFFLINE_DT_SYNC_ON_TYPE.OnAppStartUp:
                        strSyncOnType = "App start up";
                break;
                case OFFLINE_DT_SYNC_ON_TYPE.Manual:
                    strSyncOnType = "Manual";
                break;
            }
            return strSyncOnType;
        },
        getOflnTblUploadSeqTextByValue:function(value){
            var strSeqText = "";
            switch(value){
                case OFFLINE_TBL_UPLOAD_SEQUENCE.LIFO:
                    strSeqText = "Last In First Out";
                break;
                case OFFLINE_TBL_UPLOAD_SEQUENCE.FIFO:
                    strSeqText = "First In First Out";
                break;
            }
            return strSeqText;
        },
        getWebserviceTypeByText:function(text){
            return _getWebserviceTypeByText();
        },
        mfErrorHelper: (function () {
            return {
                getHtmlListOfErrors: function (errors/*array*/) {
                    var strHtmlList = "";
                    if (errors && $.isArray(errors) && errors.length > 0) {
                        strHtmlList = '<ul class="errors">';
                        $.each(errors, function (index, value) {
                            strHtmlList += '<li>' + value + '</li>';
                        });
                        strHtmlList += '</ul>';
                    }
                    return strHtmlList;
                }

            };
        })(),
        getInputParamsForCmdObject: function (cmdId,
            commandType/*IDE_COMMAND_OBJECT_TYPES*/) {
            var aryInputParams = [];
            switch (commandType) {
                case IDE_COMMAND_OBJECT_TYPES.db:
                    aryInputParams = _dbCommandHelpers.getInputParamsForCmd(cmdId);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl:
                    aryInputParams = _wsCommandHelpers.getInputParamsForCmd(cmdId,
                        WEB_SERVICE_TYPE.wsdlSoap);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.http:
                    aryInputParams = _wsCommandHelpers.getInputParamsForCmd(cmdId,
                        WEB_SERVICE_TYPE.http);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                    aryInputParams = _wsCommandHelpers.getInputParamsForCmd(cmdId,
                        WEB_SERVICE_TYPE.xmlRpc);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata:
                    aryInputParams = _odataCommandHelpers.getInputParamsForCmd(cmdId);
                    break;
            }
            return aryInputParams;
        },
        getOutputParamsForCmdObject: function (cmdId, 
            commandType/*IDE_COMMAND_OBJECT_TYPES*/) {
            var aryParams = [];
            switch (commandType) {
                case IDE_COMMAND_OBJECT_TYPES.db:
                    aryParams = _dbCommandHelpers.getOutputParamsForCmd(cmdId);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl:
                    aryParams = _wsCommandHelpers.getOutputParamsForCmd(cmdId,
                        WEB_SERVICE_TYPE.wsdlSoap);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.http:
                    aryParams = _wsCommandHelpers.getOutputParamsForCmd(cmdId,
                        WEB_SERVICE_TYPE.http);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                    aryParams = _wsCommandHelpers.getOutputParamsForCmd(cmdId,
                        WEB_SERVICE_TYPE.xmlRpc);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata:
                    aryParams = _odataCommandHelpers.getOutputParamsForCmd(cmdId);
                    break;
            }
            return aryParams;
        },
        getDbSelectCmdsForConnection:function(connId){
            _dbCommandHelpers.getDbObjectsByCmdTypeAndConn(
                DB_COMMAND_TYPE.Select,connId);
        },
        getDbInsertCmdsForConnection:function(connId){
            _dbCommandHelpers.getDbObjectsByCmdTypeAndConn(
                DB_COMMAND_TYPE.Insert,connId);
        },
        getConnsAsOptionsOfDdl:function(cmdObjectType,addSelectOption,selectOptionValue){
            var strOptions = "";
            switch(cmdObjectType){
                case IDE_COMMAND_OBJECT_TYPES.db:
                     strOptions= _dbConnectionHelpers.getConnsAsOptionsOfDdl(
                            addSelectOption,selectOptionValue);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl:
                        strOptions= _wsConnectionHelpers.getConnsAsOptionsOfDdl(
                            WEB_SERVICE_TYPE.wsdlSoap,
                            addSelectOption,
                            selectOptionValue);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.http:
                        strOptions= _wsConnectionHelpers.getConnsAsOptionsOfDdl(
                            WEB_SERVICE_TYPE.http,
                            addSelectOption,
                            selectOptionValue);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                        strOptions= _wsConnectionHelpers.getConnsAsOptionsOfDdl(
                            WEB_SERVICE_TYPE.rpcXml,
                            addSelectOption,
                            selectOptionValue);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata:
                        strOptions= _odataConnectionHelpers.getConnsAsOptionsOfDdl(
                            addSelectOption,selectOptionValue);
                    break;    
            }
            return strOptions;
        },
        getCommandTypeByValue: function (val) {
            var type = "";
            switch (val) {
                case "0":
                    type =IDE_COMMAND_OBJECT_TYPES.None;
                break;
                case "1":
                    type = IDE_COMMAND_OBJECT_TYPES.db;
                    break;
                case "2":
                    type =IDE_COMMAND_OBJECT_TYPES.wsdl;
                    break;
                case "3":
                    type = IDE_COMMAND_OBJECT_TYPES.http;
                    break;
                case "4":
                    type = IDE_COMMAND_OBJECT_TYPES.xmlRpc;
                    break;
                case "5":
                    type = IDE_COMMAND_OBJECT_TYPES.odata;
                    break;
            }
            return type;
        },
        getCommandObjByCmdId: function (commandType/*IDE_COMMAND_OBJECT_TYPES*/, cmdId) {
            var cmdObj = [];
            switch (commandType) {
                case IDE_COMMAND_OBJECT_TYPES.db:
                    cmdObj = _dbCommandHelpers.getObjectByCmdId(cmdId);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl:
                case IDE_COMMAND_OBJECT_TYPES.http:
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                    cmdObj = _wsCommandHelpers.getWsObjectById(cmdId);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata:
                    cmdObj = _odataCommandHelpers.getOdataObjectById(cmdId);
                    break;
            }
            return cmdObj;
        },
        getSelectCmdsByConnsAsOptionsForDdl:function(connId,
                addSelectOption,
                selectOptionValue){
            return _dbCommandHelpers.getSelectCommandsAsOptionsForDdl(connId,
                                      addSelectOption,
                                      selectOptionValue);
        },
        getInsertCmdsByConnAsOptionsForDdl:function(connId,
                addSelectOption,
                selectOptionValue){
           return _dbCommandHelpers.getInsertCommandsAsOptionsForDdl(connId,
                   addSelectOption,
                   selectOptionValue);
            
        },
        getUpdateCmdsByConnAsOptionsForDdl:function(connId,
                addSelectOption,
                selectOptionValue){
            return _dbCommandHelpers.getUpdateCommandsAsOptionsForDdl(connId,
                    addSelectOption,
                    selectOptionValue);
            
        },
        getWsCmdsByConnAsOptionsForDdl:function(connId,
                wsType,
                addSelectOption,
                selectOptionValue){
            return _wsCommandHelpers.getCmdsAsOptionsOfDdlByConnAndWsType(
                    connId,
                    wsType,
                    addSelectOption,
                    selectOptionValue);
            
        },
        getOdataCmdsByConnAsOptionsForDdl:function(connId,
                addSelectOption,
                selectOptionValue){
            return _odataCommandHelpers.getCmdsAsOptionsOfDdlByConn(
                    connId,
                    addSelectOption,
                    selectOptionValue);
            
        },
        getConnectionObjByConnId:function(commandType/*IDE_COMMAND_OBJECT_TYPES*/,connId){
            var connObj = null;
            switch (commandType) {
                case IDE_COMMAND_OBJECT_TYPES.db:
                    connObj = _dbConnectionHelpers.getConnById(connId);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl:
                case IDE_COMMAND_OBJECT_TYPES.http:
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                    connObj = _wsConnectionHelpers.getConnById(connId);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata:
                    connObj = _odataConnectionHelpers.getConnById(connId);
                    break;
            }
            return connObj;
            
        },
        getWeekdayNameByVal : function(value){
            var strWeekday = "";
            switch(value){
                case "1" :
                    strWeekday = "Monday";
                    break;
                case "2" :
                    strWeekday = "Tuesday";
                    break;
                case "3" :
                    strWeekday = "Wednesday";
                    break;
                case "4" :
                    strWeekday = "Thursday";
                    break;
                case "5" :
                    strWeekday = "Friday";
                    break;
                case "6" :
                    strWeekday = "Saturday";
                    break;
                case "7" :
                    strWeekday = "Sunday";
                    break;
            }
            return strWeekday;
        },
        getMonthNameByVal : function(value){
            var strMonth = "";
            switch(value){
                case "1" :
                    strMonth = "January";
                    break;
                case "2" :
                    strMonth = "February";
                    break;
                case "3" :
                    strMonth = "March";
                    break;
                case "4" :
                    strMonth = "April";
                    break;
                case "5" :
                    strMonth = "May";
                    break;
                case "6" :
                    strMonth = "June";
                    break;
                case "7" :
                    strMonth = "July";
                    break;
                case "8" :
                    strMonth = "August";
                break;
                case "9" :
                    strMonth = "September";
                break;
                case "10" :
                    strMonth = "October";
                break;
                case "11" :
                    strMonth = "November";
                break;
                case "12" :
                    strMonth = "December";
                break;
            }
            return strMonth;
        },
        getSyncOnDtlsForDatatable:function(syncDtls){
            var strSyncDtls = "",
                strTempSyncDtl = "";
            if($.isArray(syncDtls) && syncDtls.length>0){
                for(var i=0;i<=syncDtls.length-1;i++){
                    strTempSyncDtl = "";
                    switch(syncDtls[i]){
                        case  "1" :
                            strTempSyncDtl  ="On Login"; 
                            break;
                        case  "2" :
                            strTempSyncDtl  ="On App Startup";
                            break;
                        case  "3" :
                            strTempSyncDtl  ="On App Close";
                            break;
                        case  "4" :
                            strTempSyncDtl  ="Manual";
                            break;
                    }
                    
                    if(strSyncDtls){
                      strSyncDtls += ", "+strTempSyncDtl  ;  
                    }
                    else{
                        strSyncDtls += strTempSyncDtl;
                    }
                }
            }
            return strSyncDtls;
        }
    };
})();


var MFODB = (function ($){ 
    var isResetReqOnEditClose = true;
    
    function _getMainContainerDiv(){
        return $('#divMainContainer');
    }
    //HELPER FUNCTIONS
    var _util = {
        deserializeJson :function(json){
            return $.parseJSON(json);
        },
        serializeObjectToJson:function(obj){
            return JSON.stringify(obj);
        },
        setUniformedDropDownValue:function(ddlObject, value){
            if (ddlObject && value) {
                //$(ddlObject).val(value);
                $.each($(ddlObject).children('option'), function (index, option) {
                    if ($(option).val() === value) {
                        $(option).attr('selected', 'seleted');
                    } else {
                        $(option).removeAttr('selected', 'selected');
                    }
                });
                $(ddlObject).val(value);
                var $spanElement = $(ddlObject).siblings('span');
                if ($spanElement) {
                    $($spanElement).text($(ddlObject).find(":selected").text());
                }
            }
        
        },
        checkUncheckUniformedCheckbox :function(chkBox/*jquery object*/,checkCheckbox){
            var chkBoxParent =$([]);
            if(chkBox){
                if(checkCheckbox == null)checkCheckbox = false;
                chkBoxParent = chkBox.parent();
                if(checkCheckbox === true){
                    chkBox.prop("checked",true);
                    if (chkBoxParent.is("span")) {
                        chkBoxParent.attr("class", "checked");
                    }
                }
                else{
                    chkBox.prop("checked",false);
                    if (chkBoxParent.is("span")) {
                        chkBoxParent.removeAttr("class");
                    }
                }
            }
        }
    };
    var _hdf = {
        hidOldNewTblDtlForSaveAndOpenNew: function(){
            return $('[id$="hidOldNewTblDtlForSaveAndOpenNew"]');
        },
        hidTblDataToOpenAfterSaveAndOpenNew: function(){
            return $('[id$="hidTblDataToOpenAfterSaveAndOpenNew"]');
        }
    };
    function _getMappingValueToSaveInObjectByType(val) {
        var strReturnVal = "";
        if (typeof val === 'string') {
            val = val.trim();
            if (val === "") {
                strReturnVal = val;
            }
            else if (val.startsWith("\"") && val.endsWith("\"")) {
                strReturnVal = val.substring(1, val.length - 1);
            }
            else if (val.startsWith('@')) {
                strReturnVal = '_'+val; 
            }
            else {
                strReturnVal = '_$' + val;
            }
        }
        else {
        }
        return strReturnVal;
    }
    function _getMappingValueForHtmlByType(val){
        var strReturn = "";
        if (val) {
            if (val.startsWith('_$')) {
                strReturn = val.substring(2);
            }
            else if (val.startsWith('_@')) {
                strReturn = val.substring(1);
            }
            else {
                strReturn = "\"" + val + "\"";
            }
        }
        return strReturn;
    }
    function _getTheParamValueFromMappedObject(paramObject, paramName) {
        var strValue = "";
        if (paramObject) {
            $.each(paramObject, function (index, value) {
                if (value.fnGetPara() === paramName) {
                    strValue =_getMappingValueForHtmlByType(value.fnGetVal());
                    return false;
                }
            });
        } else {
            strValue = "";
        }
        return strValue;
    }
    function _getOfflineFormSaveErrorHtmlByTab(errors /*Array*/) {
        var strHtmlList = "";
        if (errors && $.isArray(errors) && errors.length > 0) {
            strHtmlList = '<ul class="errors">';
            $.each(errors, function (index, value) {
                strHtmlList += '<li>' + value + '</li>';
            });
            strHtmlList += '</ul>';
        }
        return strHtmlList;
        // var strError = "";
        // if ($.isArray(errors) && errors.length > 0) {
        //     strError += '<div class="popupErrorWithHtml">';
        //     strError += '<h6>' + heading + '</h6>';
        //     strError += '<ul>';
        //     $.each(errors, function (index, value) {
        //         strError += "<li>" + value + "</li>";
        //     });
        //     strError += '</ul>';
        //     strError += '</div>';
        // }
        // return strError;
    }
    function _commandHasInputParameters(commandId,commandType){
        if (mfOflnTblHelpers.getInputParamsForCmdObject(commandId,commandType).length > 0) {
            return true;
        } else {
            return false;
        }
    }
    function _commandHasOutputParameters(commandId,commandType){
        if (mfOflnTblHelpers.getOutputParamsForCmdObject(commandId,commandType).length > 0) {
            return true;
        } else {
            return false;
        }
    }
    function _setFinalOfflineCmpltJsonForSavingInHid() {
        
        var objTable = _globalData.getTable();
        
        if(objTable){
        }
        else{
            objTable = {};
        }
        
        var o = new OfflineTableCont();
        o.fnSetTable(objTable);
        
        $(':hidden[id$="hidFinalTableJsonForSaving"]').val(_util.serializeObjectToJson(o));
    }
    function _setFinalSyncJsonForSavingInHid() {
        var objSyncDetails = _globalData.getSyncDtls();
        var o = new SyncDetailsCont();
        o.fnSetSync(objSyncDetails);
        $(':hidden[id$="hidFinalSyncJsonForSavning"]').val(_util.serializeObjectToJson(o));
    }
    
    function _showHideOfflineTblNameTextbox(show /*bool*/) {
        var txtOfflineTblName = $('input[type="text"][id$="txtOfflineTblName"]', _contextDivs.getOfflineTabCont());
        if (show) {
            $(txtOfflineTblName).show();
        } else {
            $(txtOfflineTblName).hide();
        }
    }
    function _showHideOfflineTblNameLabel(show /*bool*/) {
        var lblOfflineTblName = $('span[id$="lblOfflineTblName"]', _contextDivs.getOfflineTabCont());
        if (show) {
            $(lblOfflineTblName).show();
        } else {
            $(lblOfflineTblName).hide();
        }
    }
    //END SHOW HIDE OFFLINE CONTROLS
    
    //context divs
    var _contextDivs = {
        getOfflineTable :function(){
            return $('#divOfflineDatatables');
        },
        getOfflineTabCont :function(){
            $('#PopupDiv #divOfflineTabCont');
        },
        getOfflineEditViewTabCont :function(){
            $('#PopupDiv #divEditOfflineTabCont');
        },
        getOfflineHidFields :function(){
            $('#divOfflineDatatables #offlineHidFields');
        },
        getEditOfflineTabCont :function(){
            $('#PopupDiv #divEditOfflineTabCont');
        },
        getPopupDiv :function(){
            $('#PopupDiv');
        }
    };
    
    var _globalData = {
        
        getDefaultData :function(){
            return {
                pageMode : OFFLINE_PAGE_DISPLAY_MODE.FormMode,
                formMode : OFFLINE_FORM_DISPLAY_MODE.Add,
                tabSelected :OFFLINE_TABLE_FORM_TAB.TabTableContent,
                table : new OfflineDatatable(),
                syncDetails :new SyncDtls(),
                isEdit :false,
                dbSavedTable:new OfflineDatatable(),
                dbSavedSyncDetails:new SyncDtls()
            };
        },
        setData : function(data){
            _getMainContainerDiv().data(OFFLINE_CONSTANTS.jqueryCommonDatakey,data);
        },
        getData : function(){
            return _getMainContainerDiv().data(OFFLINE_CONSTANTS.jqueryCommonDatakey);
        },
        getSyncDtls : function(){
            var objGlobalData = this.getData(),
                objSyncDtls = null;
            if(objGlobalData){
                objSyncDtls = objGlobalData.syncDetails;
            }
            return objSyncDtls;
        },
        setSyncDtls : function(syncDtls /*SyncDtls */){
            var objGlobalData = this.getData();
            if(syncDtls && syncDtls instanceof SyncDtls){
                if(objGlobalData){
                    objGlobalData.syncDetails = syncDtls;
                }
                else{
                    objGlobalData = this.getDefaultData();
                    this.setData(objGlobalData);
                    objGlobalData.syncDetails = syncDtls;
                }
            }
        },
        deleteSyncDtls : function(){
           var objGlobalData = this.getData(); 
           if(objGlobalData){
               objGlobalData.syncDetails = null;
           }
        },
        getCommandType : function(){
            var objSyncDtls = this.getSyncDtls(),
                cmdType = "";
            
            if(objSyncDtls){
                cmdType = objSyncDtls.fnGetCmdType();
            }    
            return cmdType;
        },
        setCommandType : function(data){
            var objSyncDtls = this.getSyncDtls();
            switch(data){
                case IDE_COMMAND_OBJECT_TYPES.db.id:
                        data = IDE_COMMAND_OBJECT_TYPES.db;
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl.id:
                        data = IDE_COMMAND_OBJECT_TYPES.wsdl;
                    break;
                case IDE_COMMAND_OBJECT_TYPES.http.id:
                        data = IDE_COMMAND_OBJECT_TYPES.http;
                    break;
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc.id:
                        data = IDE_COMMAND_OBJECT_TYPES.xmlRpc;
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata.id:
                        data = IDE_COMMAND_OBJECT_TYPES.odata;
                    break;
            }
            if(objSyncDtls){
                objSyncDtls.fnSetCmdType(data);
            }
            else{
                objSyncDtls = new SyncDtls();
                this.setSyncDtls(objSyncDtls);
                objSyncDtls.fnSetCmdType(data);
            }
        },
        deleteCommandType : function(){
            var objSyncDtls = this.getSyncDtls();
            if(objSyncDtls){
                objSyncDtls.fnSetCmdType("");
            }    
        },
        
        getConnection : function(){
            var objSyncDtls = this.getSyncDtls(),
                strConnection = "";
            
            if(objSyncDtls){
                strConnection = objSyncDtls.fnGetConnection();
            }    
            return strConnection;
        },
        setConnection : function(data){
            var objSyncDtls = this.getSyncDtls();
            
            if(objSyncDtls){
                objSyncDtls.fnSetConnection(data);
            }
            else{
                objSyncDtls = new SyncDtls();
                this.setSyncDtls(objSyncDtls);
                objSyncDtls.fnSetConnection(data);
            }
        },
        deleteConnection : function(){
            var objSyncDtls = this.getSyncDtls();
            if(objSyncDtls){
                objSyncDtls.fnSetConnection("");
            }    
        },
        
        getUploadData : function(){
            var objSyncDtls = this.getSyncDtls(),
                objUploadDtls = null;
            
            if(objSyncDtls){
                objUploadDtls = objSyncDtls.fnGetUpload();
            }    
            return objUploadDtls;
        },
        setUploadData : function(data){
            var objSyncDtls = this.getSyncDtls();
            if(objSyncDtls){
                objSyncDtls.fnSetUpload(data);
            }
            else{
                objSyncDtls = new SyncDtls();
                this.setSyncDtls(objSyncDtls);
                objSyncDtls.fnSetUpload(data);
            }
        },
        deleteUploadData : function(){
            var objSyncDtls = this.getSyncDtls();
            if(objSyncDtls){
                objSyncDtls.fnDeleteUpload();
            }    
        },
        
        getUploadNewRec :function(){
            var objUploadDtls = this.getUploadData(),
                objNewRec = null;
            if(objUploadDtls){
                objNewRec = objUploadDtls.fnGetNewRecord();
            }
            return objNewRec;
        },
        setUploadNewRec : function(data){
            var objUploadData = this.getUploadData();
            if(objUploadData){
                objUploadData.fnSetNewRecord(data);
            }
            else{
                objUploadData = new SyncDtlsUpload();
                this.setUploadData(objUploadData);
                objUploadData.fnSetNewRecord(data);
            }
        },
        deleteUploadNewRec : function(){
            var objUploadData = this.getUploadData();
            if(objUploadData){
                objUploadData.fnDeleteNewRecord();
            }
        },
        getUploadNewRecCmd : function(){
            var objNewRec = this.getUploadNewRec();
            if(objNewRec){
                return objNewRec.fnGetCommand();
            }
            else{
                return "";
            }    
        },
        getUploadNewRecInputParams : function(){
            var objNewRec = this.getUploadNewRec();
            if(objNewRec){
                return objNewRec.fnGetParams();
            }
            else{
                return [];
            }
        },
        setUploadNewRecCmd : function(commandId/*string*/){
            var objNewRec = this.getUploadNewRec();
            if(objNewRec){
                objNewRec.fnSetCommand(commandId);
            }
            else{
                objNewRec = new CommandDetails();
                this.setUploadNewRec(objNewRec);
                objNewRec.fnSetCommand(commandId);
            }
        },
        setUploadNewRecInputParams : function(params /*array of mapped params*/){
            var objNewRec = this.getUploadNewRec();
            if(objNewRec){
                objNewRec.fnSetParams(params);
            }
            else{
               objNewRec = new CommandDetails();
               this.setUploadNewRec(objNewRec);
               objNewRec.fnSetParams(params);
            }
        },
        deleteUploadNewRecInputParams : function(){
            var objNewRec = this.getUploadNewRec();
            if(objNewRec){
                objNewRec.fnSetParams([]);
            }
        },
        getUploadModifiedRec :function(){
            var objUploadDtls = this.getUploadData(),
                objModifiedRec = null;
            if(objUploadDtls){
                objModifiedRec = objUploadDtls.fnGetModifiedRecord();
            }
            return objModifiedRec;
        },
        setUploadModifiedRec : function(data){
            var objUploadData = this.getUploadData();
            if(objUploadData){
                objUploadData.fnSetModifiedRecord(data);
            }
            else{
                objUploadData = new SyncDtlsUpload();
                this.setUploadData(objUploadData);
                objUploadData.fnSetModifiedRecord(data);
            }
        },
        deleteUploadModifiedRec : function(){
            var objUploadData = this.getUploadData();
            if(objUploadData){
                objUploadData.fnDeleteModifiedRecord();
            }
        },
        getUploadModifiedRecCmd : function(){
            var objModifiedRec = this.getUploadModifiedRec();
            if(objModifiedRec){
                return objModifiedRec.fnGetCommand();
            }
            else{
                return "";
            }
        },
        getUploadModifiedRecInputParams : function(){
            var objModifiedRec = this.getUploadModifiedRec();
            if(objModifiedRec){
                return objModifiedRec.fnGetParams();
            }
            else{
                return [];
            }
        },
        setUploadModifiedRecCmd : function(commandId/*string*/){
            var objModifiedRec = this.getUploadModifiedRec();
            if(objModifiedRec){
                objModifiedRec.fnSetCommand(commandId);
            }
            else{
                objModifiedRec = new CommandDetails();
                this.setUploadModifiedRec(objModifiedRec);
                objModifiedRec.fnSetCommand(commandId);
            }
        },
        setUploadModifiedRecInputParams : function(params /*array of mapped params*/){
            var objModifiedRec = this.getUploadModifiedRec();
            if(objModifiedRec){
                objModifiedRec.fnSetParams(params);
            }
            else{
               objModifiedRec = new CommandDetails();
               this.setUploadModifiedRec(objModifiedRec);
               objModifiedRec.fnSetParams(params);
            }
        },
        deleteUploadModifiedRecInputParams : function(){
            var objModifiedRec = this.getUploadModifiedRec();
            if(objModifiedRec){
                objModifiedRec.fnSetParams([]);
            }
        },
        getIsInitialDownload : function(){
            var objSyncDtls = this.getSyncDtls(),
                isInitialDownload = 0;
            
            if(objSyncDtls){
                isInitialDownload = objSyncDtls.fnGetIsInitialDownload();
            }    
            return isInitialDownload;
        },
        setIsInitialDownload : function(value){
            var objSyncDtls = this.getSyncDtls();
            if(objSyncDtls){
                objSyncDtls.fnSetIsInitialDownload(value);
            }
            else{
                objSyncDtls = new SyncDtls();
                this.setSyncDtls(objSyncDtls);
                objSyncDtls.fnSetIsInitialDownload(value);
            }
        },
        
        getDeleteRecordAfterUpload : function(){
            var objUpload = this.getUploadData(),
                deleteRecordAfterUpload = 0;
            
            if(objUpload){
                deleteRecordAfterUpload = objUpload.fnGetDeleteRecordAfterUpload();
            }    
            return deleteRecordAfterUpload;
        },
        setDeleteRecordAfterUpload : function(value){
            var objUpload = this.getUploadData();
            if(objUpload){
                objUpload.fnSetDeleteRecordAfterUpload(value);
            }
            else{
                objUpload = new SyncDtlsUpload();
                this.setUploadData(objUpload);
                objUpload.fnSetDeleteRecordAfterUpload(value);
            }
        },
        getModifiedRecSameAsNewRec : function(){
            var objUpload = this.getUploadData(),
                modifiedRecSameAsNewRec = 0;
            
            if(objUpload){
                modifiedRecSameAsNewRec = objUpload.fnGetModifiedSameAsNewRecord();
            }    
            return modifiedRecSameAsNewRec;
        },
        setUploadCriteria : function(value){
            var objUpload = this.getUploadData();
            if(objUpload){
                objUpload.fnSetUploadCriteria(value);
            }
            else{
                objUpload = new SyncDtlsUpload();
                this.setUploadData(objUpload);
                objUpload.fnSetUploadCriteria(value);
            }
        },
        setUploadWithin : function(value){
            var objUpload = this.getUploadData();
            if(objUpload){
                objUpload.fnSetUploadWithin(value);
            }
            else{
                objUpload = new SyncDtlsUpload();
                this.setUploadData(objUpload);
                objUpload.fnSetUploadWithin(value);
            }
        },
        getUploadCriteria : function(value){
            var objUpload = this.getUploadData();
            if(objUpload){
                return objUpload.fnGetUploadCriteria();
            }
            else{
                return "";
            }
        },
        getUploadWithin : function(value){
            var objUpload = this.getUploadData();
            if(objUpload){
                return objUpload.fnGetUploadWithin();
            }
            else{
                return "1";
            }
        },
        setModifiedRecSameAsNewRec : function(value){
            var objUpload = this.getUploadData();
            if(objUpload){
                objUpload.fnSetModifiedSameAsNewRecord(value);
            }
            else{
                objUpload = new SyncDtlsUpload();
                this.setUploadData(objUpload);
                objUpload.fnSetModifiedSameAsNewRecord(value);
            }
        },
        getDownloadData : function(){
            var objSyncDtls = this.getSyncDtls(),
                objDownloadDtls = null;
            
            if(objSyncDtls){
                objDownloadDtls = objSyncDtls.fnGetDownload();
            }    
            return objDownloadDtls;
        },
        setDownloadData : function(data){
            var objSyncDtls = this.getSyncDtls();
            if(objSyncDtls){
                objSyncDtls.fnSetDownload(data);
            }
            else{
                objSyncDtls = new SyncDtls();
                this.setSyncDtls(objSyncDtls);
                objSyncDtls.fnSetDownload(data);
            }
        },
        deleteDownloadData : function(){
           var objSyncDtls = this.getSyncDtls();
           if(objSyncDtls){
               objSyncDtls.fnDeleteDownload();
           }  
        },
        setDownloadCommand : function(commandId){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnSetCommand(commandId);
            }
            else{
                objDownloadData = new SyncDtlsDownload();
                this.setDownloadData(objDownloadData);
                objDownloadData.fnSetCommand(commandId);
            }
        },
        getDownloadCommand : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                return objDownloadData.fnGetCommand();
            }
            else{
                return "";
            }
        },
        deleteDownloadCommand : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnDeleteCommand();
            }
        },
        setDownloadInputParam : function(params){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnSetInputParam(params);
            }
            else{
                objDownloadData = new SyncDtlsDownload();
                this.setDownloadData(objDownloadData);
                objDownloadData.fnSetInputParam(params);
            }
        },
        getDownloadInputParam : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                return objDownloadData.fnGetInputParam();
            }
            else{
                return [];
            }
        },
        deleteDownloadInputParam : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnDeleteInputParam();
            }
        },
        setDownloadOutputParam : function(params){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnSetOutputParam(params);
            }
            else{
                objDownloadData = new SyncDtlsDownload();
                this.setDownloadData(objDownloadData);
                objDownloadData.fnSetOutputParam(params);
            }
        },
        getDownloadOutputParam : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                return objDownloadData.fnGetOutputParam();
            }
            else{
                return [];
            }
        },
        deleteDownloadOutputParam : function(){
             var objDownloadData = this.getDownloadData();
             if(objDownloadData){
                 objDownloadData.fnDeleteOutputParam();
             }
         },
        setDownloadExpiry : function(value/*cachingDetails*/){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnSetExpiry(value);
            }
            else{
                objDownloadData = new SyncDtlsDownload();
                this.setDownloadData(objDownloadData);
                objDownloadData.fnSetExpiry(value);
            }
        },
        getDownloadExpiry : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                return objDownloadData.fnGetExpiry();
            }
            else{
                return null;
            }
        },
        setDisallowDataUseAfterExpiry : function(value/*cachingDetails*/){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnSetDisallowAfterExpiry(value);
            }
            else{
                objDownloadData = new SyncDtlsDownload();
                this.setDownloadData(objDownloadData);
                objDownloadData.fnSetDisallowAfterExpiry(value);
            }
        },
        getDisallowDataUseAfterExpiry : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                return objDownloadData.fnGetDisallowAfterExpiry();
            }
            else{
                return null;
            }
        },
        setCleanRecordBeforeDownload : function(value/*cachingDetails*/){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnSetCleanRecord(value);
            }
            else{
                objDownloadData = new SyncDtlsDownload();
                this.setDownloadData(objDownloadData);
                objDownloadData.fnSetCleanRecord(value);
            }
        },
        getCleanRecordBeforeDownload : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                return objDownloadData.fnGetCleanRecord();
            }
            else{
                return null;
            }
        },
        deleteDownloadExpiry : function(){
            var objDownloadData = this.getDownloadData();
            if(objDownloadData){
                objDownloadData.fnDeleteExpiry();
            }
        },
        setFormMode  : function (  formMode/*OFFLINE_PAGE_DISPLAY_MODE*/){
            var objGlobalData =  this.getData();
            if(objGlobalData){
              objGlobalData.formMode = formMode;
            }
            else{
                objGlobalData = this.getDefaultData();
                this.setData(objGlobalData);
                objGlobalData.formMode = formMode;
            }
        },
        getFormMode : function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
               return objGlobalData.formMode;
            }
        },
        getTabSelected : function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
               return objGlobalData.tabSelected;
            }
            else{
                throw "Global Data  " + MF_EXCEPTION.objectNotFound ;
            }
        },
        setTabSelected : function(tabSelected/*OFFLINE_TABLE_FORM_TAB*/){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                objGlobalData.tabSelected = tabSelected;
            }
            else{
                throw "Global Data  " + MF_EXCEPTION.objectNotFound ;
            }
        },
        getTable:function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                return objGlobalData.table;
            }
            else{
                return null;
            }
        },
        setTable : function(tbl/*OfflineDatatable*/){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                objGlobalData.table = tbl;
            }
            else{
               objGlobalData = this.getDefaultData();
               this.setData(objGlobalData);
               objGlobalData.table = tbl;
            }
        },
        getTableName : function(){
            var objTable = this.getTable(),
                tblName = "";
            
            if(objTable){
                tblName = objTable.fnGetName();
            }    
            return tblName;
        },
        getTableId : function(){
            var objTable = this.getTable(),
                tblId = "";
            
            if(objTable){
                tblId = objTable.fnGetId();
            }    
            return tblId;
        },
        getTableType : function(){
           var objTable = this.getTable(),
               tblType = "";
           
           if(objTable){
               tblType = objTable.fnGetType();
           }    
           return tblType;
        },
        getTableSyncOn : function(){
           var objTable = this.getTable(),
               syncOn = objTable.fnGetDefaultSyncOnTypes();
           
           if(objTable){
               syncOn = objTable.fnGetSyncType();
               if($.isArray(syncOn)){
                   syncOn = objTable.fnGetDefaultSyncOnTypes();
               }
           }    
           return syncOn;
        },
        getTableUploadSequence : function(){
           var objTable = this.getTable(),
               uploadSeq = "";
           
           if(objTable){
               uploadSeq = objTable.fnGetSequence();
           }    
           return uploadSeq;
        },
        getTransferBlockSize : function(){
           var objTable = this.getTable(),
               transferBlockSize = "";
           
           if(objTable){
               transferBlockSize = objTable.fnGetTransferBlockSize();
           }    
           return transferBlockSize;
        },
        getConfirmationPrompt : function(){
           var objTable = this.getTable(),
               confirmationPrompt = "";
           
           if(objTable){
               confirmationPrompt = objTable.fnGetUserConfirmationPromt();
           }    
           return confirmationPrompt;
        },
        getColumns : function(){
           var objTable = this.getTable(),
               columns = [];
           
           if(objTable){
               columns = objTable.fnGetColumns();
           }    
           return columns;
        },
        deleteTable : function(){
            var objGlobalData =  _getPageGlobalData();
            if(objGlobalData){
                objGlobalData.table = null;
            }
        },
        deleteSyncDetails : function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                objGlobalData.syncDetails = null;
            }
        },
        setIsEdit : function(value){
            var objGlobalData = this.getData();
            if(objGlobalData){
                objGlobalData.isEdit = value;
            }
            else{
                throw "Global Data " + MF_EXCEPTION.objectNotFound;
            }
        },
        getIsEdit : function(){
            var objGlobalData = this.getData();
            if(objGlobalData){
               return objGlobalData.isEdit;
            }
            else{
                throw "Global Data " + MF_EXCEPTION.objectNotFound;
            }
        },
        setDbSavedTable : function(tbl){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                objGlobalData.dbSavedTable = tbl;
            }
            else{
                throw "Global Data " + MF_EXCEPTION.objectNotFound;
            }
        },
        getDbSavedTable: function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                return objGlobalData.dbSavedTable;
            }
            else{
                throw "Global Data " + MF_EXCEPTION.objectNotFound;
            }
        },
        deleteDbSavedTable:function(){
            var objGlobalData = this.getData();
            if(objGlobalData){
                objGlobalData.dbSavedTable = null;
            }
        },
        setDbSavedSyncDetails : function(syncDtls){
            if(syncDtls && syncDtls instanceof SyncDtls){
                var objGlobalData =  this.getData();
                if(objGlobalData){
                    objGlobalData.dbSavedSyncDetails = syncDtls;
                }
                else{
                   throw "Global Data " + MF_EXCEPTION.objectNotFound;
                }
            }
        },
        getDbSavedSyncDetails: function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                return  objGlobalData.dbSavedSyncDetails;
            }
            else{
                throw "Global Data " + MF_EXCEPTION.objectNotFound;
            }
        },
        deleteDbSavedSyncDetails:function(){
            var objGlobalData =  this.getData();
            if(objGlobalData){
                objGlobalData.dbSavedSyncDetails = null;
            }
        }
    };
    
    var _tableTab = (function(){
        var _getjQueryObj={
            txtTblName : function(){
                return $('[id$="txtOfflineTblName"]');
            },
            ddlOfflineTblType : function(){
                return $('select[id$="ddlOfflineTblType"]');
            },
            chkUserConfirmation : function(){
                return $(':checkbox[id$="chkOfflineUserConfirmation"]');
            },
            ddlUploadSeq : function(){
                return $('select[id$="ddlUploadSeq"]');
            },
            txtBlockSize : function(){
                return $('input[id$="txtBlockSize"]');
            },
            lblTblName : function(){
                return $('[id$="lblOfflineTblName"]');
            },
            chkSyncOnLogin : function(){
                return $('#chkSyncLogin');
            },
            chkSyncOnAppStartup : function(){
                return $('#chkSyncAppStartup');
            },
            chkSyncOnAppClose : function(){
                return $('#chkSyncAppClose');
            },
            chkSyncManual : function(){
                return $('#chkSyncManual');
            },
            divUploadingSeqAndSizeInfoCont : function(){
                return $('#divUploadingSeqAndSizeInfoCont');
            },
            chkOfflineUserConfirmation : function(){
                return $('#chkOfflineUserConfirmation');
            },
            allCntrls : function(){
                return {
                    txtTblName : this.txtTblName(),
                    ddlOfflineTblType: this.ddlOfflineTblType(),
                    chkUserConfirmation : this.chkUserConfirmation(),
                    ddlUploadSeq: this.ddlUploadSeq(),
                    txtBlockSize: this.txtBlockSize(),
                    lblTblName: this.lblTblName(),
                    chkSyncOnLogin: this.chkSyncOnLogin(),
                    chkSyncOnAppStartup: this.chkSyncOnAppStartup(),
                    chkSyncOnAppClose: this.chkSyncOnAppClose(),
                    chkSyncManual: this.chkSyncManual()
                };
            } 
        };
        var _showHide={
            divUploadingSeqAndSizeInfoCont: function(show){
                if(show === true){
                  _getjQueryObj
                  .divUploadingSeqAndSizeInfoCont().show();
                }
                else{
                    _getjQueryObj
                    .divUploadingSeqAndSizeInfoCont().hide();
                }
                //TEMPORARY WORK
                $('#divBlockSize').hide();
            }   
        };
        var _setValue = {
            ddlOfflineTblType:function(value){
                var $control = _getjQueryObj.ddlOfflineTblType();
                $control.val(value);
                updateUniformedControl($control);
            },
            chkUserConfirmation: function(check){
                var $control = _getjQueryObj.chkUserConfirmation();
                
                if(check === true){
                    $control.prop("checked",true);
                }
                else{
                    $control.prop("checked",false);
                }
                updateUniformedControl($control);
            },
            ddlUploadSeq:function(value){
                var $control = _getjQueryObj.ddlUploadSeq();
                $control.val(value);
                updateUniformedControl($control);
            }
        };
        function _getSyncOnValueSelectedForSaving(){
            var strSyncOnValues = "",
                $chkSyncOnLogin =_getjQueryObj.chkSyncOnLogin(),
                $chkSyncOnAppStartup = _getjQueryObj.chkSyncOnAppStartup(),
                $chkSyncOnAppClose = _getjQueryObj.chkSyncOnAppClose(),
                $chkSyncManual = _getjQueryObj.chkSyncManual();
            if($chkSyncOnLogin.is(":checked")){
                strSyncOnValues += "1";
            }
            else{
                strSyncOnValues += "0";
            }
            if($chkSyncOnAppStartup.is(":checked")){
                strSyncOnValues += "1";
            }
            else{
                strSyncOnValues += "0";
            }
            if($chkSyncOnAppClose.is(":checked")){
                strSyncOnValues += "1";
            }
            else{
                strSyncOnValues += "0";
            }
            if($chkSyncManual.is(":checked")){
                strSyncOnValues += "1";
            }
            else{
                strSyncOnValues += "0";
            }
            return strSyncOnValues;
        }
        function _isAnySyncOnTypeSelected(){
            var $chkSyncOnLogin =_getjQueryObj.chkSyncOnLogin(),
                $chkSyncOnAppStartup = _getjQueryObj.chkSyncOnAppStartup(),
                $chkSyncOnAppClose = _getjQueryObj.chkSyncOnAppClose(),
                $chkSyncManual = _getjQueryObj.chkSyncManual();
            var isSelected = false;
            if($chkSyncOnLogin.is(":checked")){
                isSelected = true;
            }
            else if($chkSyncOnAppStartup.is(":checked")){
                isSelected = true;
            }
            else if($chkSyncOnAppClose.is(":checked")){
                isSelected = true;
            }
            else if($chkSyncManual.is(":checked")){
                isSelected = true;
            }
            return isSelected;
        }
        function _setSyncOnDataInHtml(value){
            var $chkSyncOnLogin =_getjQueryObj.chkSyncOnLogin(),
                $chkSyncOnAppStartup = _getjQueryObj.chkSyncOnAppStartup(),
                $chkSyncOnAppClose = _getjQueryObj.chkSyncOnAppClose(),
                $chkSyncManual = _getjQueryObj.chkSyncManual(),
                syncDetails = [],
                aryChkBoxesInOrder = [],
                i=0;
            
            syncDetails = value.split("");
            aryChkBoxesInOrder.push($chkSyncOnLogin);
            aryChkBoxesInOrder.push($chkSyncOnAppStartup);
            aryChkBoxesInOrder.push($chkSyncOnAppClose);
            aryChkBoxesInOrder.push($chkSyncManual);
            for(i=0;i<=3;i++){
                if(syncDetails[i] === "1"){
                   _util.checkUncheckUniformedCheckbox(
                        aryChkBoxesInOrder[i],
                        true
                    );
                    
                }
                else{
                   _util.checkUncheckUniformedCheckbox(
                        aryChkBoxesInOrder[i],
                        false
                    );
                }
            }
        }
        function _uncheckAllSyncOnTypeCheckboxes(){
            var $chkSyncOnLogin =_getjQueryObj.chkSyncOnLogin(),
                $chkSyncOnAppStartup = _getjQueryObj.chkSyncOnAppStartup(),
                $chkSyncOnAppClose = _getjQueryObj.chkSyncOnAppClose(),
                $chkSyncManual = _getjQueryObj.chkSyncManual();
            _util.checkUncheckUniformedCheckbox(
                $chkSyncOnLogin,
                false);
            _util.checkUncheckUniformedCheckbox(
                $chkSyncOnAppStartup,
                false);  
            _util.checkUncheckUniformedCheckbox(
            $chkSyncManual,
            false); 
            _util.checkUncheckUniformedCheckbox(
            $chkSyncOnAppClose,
            false);
        }
        function _setOfflineTableInGlobalData(){
            var objTableTabCntrls =  _getjQueryObj.allCntrls(),
                objTable = _globalData.getTable(),
                strOflnTblType="",
                blnIsEdit = _globalData.getIsEdit();
            if(objTable){
            }
            else{
               objTable = new OfflineDatatable();
               objTable.fnSetDefaultsValues();
               _globalData.setTable(objTable);
            }
            objTable.fnSetName(objTableTabCntrls.txtTblName.val());
            if(blnIsEdit === false){
                objTable.fnSetId("");
            }
            else{
            }
            strOflnTblType = objTableTabCntrls.ddlOfflineTblType.val();
            objTable.fnSetType(strOflnTblType);
            
            //objTable.fnSetSyncType(objTableTabCntrls.ddlOfflineSyncOn.val());
            objTable.fnSetSyncType(
                _getSyncOnValueSelectedForSaving()
            );
            
            
            objTable.fnSetUserConfirmationPrompt(
                objTableTabCntrls.chkUserConfirmation.is(':checked') ? "1" : "0"
            );
            
            
            if(strOflnTblType === OFFLINE_TABLE_TYPE.UploadData ){
                objTable.fnSetSequence(objTableTabCntrls.ddlUploadSeq.val());
                objTable.fnSetTransferBlockSize(objTableTabCntrls.txtBlockSize.val());
            }
            else{
                objTable.fnSetSequence("");
                objTable.fnSetTransferBlockSize(0);
            }
        }
        return{
            getjQueryObj :{
                txtTblName : function(){
                    return _getjQueryObj.txtTblName();
                },
                ddlOfflineTblType : function(){
                    return _getjQueryObj.ddlOfflineTblType();
                },
                chkUserConfirmation : function(){
                    return _getjQueryObj.chkUserConfirmation();
                },
                ddlUploadSeq : function(){
                    return _getjQueryObj.ddlUploadSeq();
                },
                txtBlockSize : function(){
                    return _getjQueryObj.txtBlockSize();
                },
                lblTblName : function(){
                    return _getjQueryObj.lblTblName();
                },
                chkSyncOnLogin : function(){
                    return _getjQueryObj.chkSyncOnLogin();
                },
                chkSyncOnAppStartup : function(){
                    return _getjQueryObj.txtTblName();
                },
                chkSyncOnAppClose : function(){
                    return _getjQueryObj.txtTblName();
                },
                chkSyncManual : function(){
                    return _getjQueryObj.txtTblName();
                },
                divUploadingSeqAndSizeInfoCont : function(){
                   return _getjQueryObj.divUploadingSeqAndSizeInfoCont();
                },
                allCntrls : function(){
                    return {
                        txtTblName : this.txtTblName(),
                        ddlOfflineTblType: this.ddlOfflineTblType(),
                        chkUserConfirmation : this.chkUserConfirmation(),
                        ddlUploadSeq: this.ddlUploadSeq(),
                        txtBlockSize: this.txtBlockSize(),
                        lblTblName: this.lblTblName(),
                        chkSyncOnLogin: this.chkSyncOnLogin(),
                        chkSyncOnAppStartup: this.chkSyncOnAppStartup(),
                        chkSyncOnAppClose: this.chkSyncOnAppClose(),
                        chkSyncManual: this.chkSyncManual()
                    };
                } 
            },
            showHide :{
                divUploadingSeqAndSizeInfoCont :function(show){
                   _showHide.divUploadingSeqAndSizeInfoCont(show) ;
                } 
            },
            getSyncOnValueSelectedForSaving:function(){
                return _getSyncOnValueSelectedForSaving();
            },
            isAnySyncOnTypeSelected:function(){
                _isAnySyncOnTypeSelected();
            },
            setSyncOnDataInHtml:function(value){
                _setSyncOnDataInHtml(value);
            },
            uncheckAllSyncOnTypeCheckboxes:function(){
                _uncheckAllSyncOnTypeCheckboxes();
            },
            setValue : {
                ddlOfflineTblType:function(value){
                   _setValue.ddlOfflineTblType(value);
                },
                chkUserConfirmation: function(check){
                    _setValue.chkUserConfirmation(check);
                },
                ddlUploadSeq:function(value){
                    _setValue.ddlUploadSeq(value);
                }
            },
            setOfflineTableInGlobalData: function(){
                _setOfflineTableInGlobalData();
            }
        };
    })();
    
    var _columnTab = (function(){
        
        function _initialiseColumnSettingDynamicRow(){
            var offlineDTClmnDefinition = $("#OfflineDTClmnDefinition", _contextDivs.getOfflineTabCont());
            if (offlineDTClmnDefinition) {
                var trDynamicRow = $(offlineDTClmnDefinition).find(".dynamicRow");
                if (trDynamicRow) {
                    var name = $(trDynamicRow).find("input[name='ColumnNm']"),
                        dataType = $(trDynamicRow).find("select[name='DataType']"),
                        defaultVal = $(trDynamicRow).find("input[name='DefaultVal']"),
                        //minVal = $(trDynamicRow).find("input[name='MinValue']"),
                        //maxVal = $(trDynamicRow).find("input[name='MaxValue']"),
                        decPlaces = $(trDynamicRow).find("select[name='DecimalPlaces']"),
                        spanNotApplicable = $(trDynamicRow).find("span[id='spanNotApplicable']");
                        //$primary=$(trDynamicRow).find(".Primary");
                    $(name).val("");
                    $(dataType).val("0");
                    $(defaultVal).val("");
                    //$(minVal).val("0");
                    //$(maxVal).val("100");
                    $(decPlaces).hide();
                    $(spanNotApplicable).show();
                    //$primary.prop("checked",false);
                }
            }
        }
        function _setEventsOfColumnTableCross(){
            
            var offlineDTClmnDefinition = $("#OfflineDTClmnDefinition",
                                _contextDivs.getOfflineTabCont());
            var $spanColumnRemove = $(offlineDTClmnDefinition).find("tbody tr .ColumnRemove");                  
            $spanColumnRemove.off('click');
            $spanColumnRemove.on('click',function(evnt){
               var confirmDelete = confirm('Are you sure you want to remove the column');
               if (confirmDelete) {
                   var $self = $(this);
                   if (!_isColumnToDeleteAlreadyMapped($self)) {
                       var tr = $self.closest('tr');
                       tr.fadeOut(400, function () {
                           tr.remove();
                           _setColumnsInGlobalTableObject();
                       });
                        return false;
                   } else {
                       showMessage(
                           "The column is already mapped to a input / output parameter.You must first remove the mapping before deleting column.",
                           MessageOption.Error, DialogType.Error);
                   }
               }
            });
        }
        function _addHTMLToClmDefinitionTbl(options/*object*/) {
            //{index, clnName, dataType, defVal, minVal, maxVal, decPlaces primary,dataTypeVal}
            var offlineDTClmnDefinition = $("#OfflineDTClmnDefinition", _contextDivs.getOfflineTabCont());
            var offlineEditDTClmnDefinition = $("#OfflineDTClmnDefinitionEdit", _contextDivs.getEditOfflineTabCont());
            var htmlToAdd = "<tr>";
            var htmlToAddWithoutCross = "",
                index = options.index;
            htmlToAdd += "<td><span id=\"spanName" + index + "\">" + options.clnName + "</span></td>";
            htmlToAdd += "<td><span id=\"spanDataType" + index + "\">" + options.dataType + "</span><span class=\"hide\" id=\"spanDataTypeVal" + index + "\">" + options.dataTypeVal + "</span></td>";
            htmlToAdd += "<td><span id=\"spanDefault" + index + "\">" + options.defVal + "</span></td>";
            //htmlToAdd += "<td><span id=\"spanMinVal" + index + "\">" + options.minVal + "</span></td>";
            //htmlToAdd += "<td><span id=\"spanMaxVal" + index + "\">" + options.maxVal + "</span></td>";
            htmlToAdd += "<td><span id=\"spanDecPlaces" + index + "\">" + options.decPlaces + "</span></td>";
            htmlToAddWithoutCross = htmlToAdd + "</tr>";
            htmlToAdd += "<td><span class=\"ColumnRemove crossImg16x16\"></span></td>";
            htmlToAdd += "</tr>"; //.eq(index - 1)
            $(offlineDTClmnDefinition).find("tbody tr.dynamicRow").before(htmlToAdd);
            $(offlineEditDTClmnDefinition).find("tbody tr.dynamicEditRow").before(htmlToAddWithoutCross);
            _setEventsOfColumnTableCross();
        }
        
        function _getAllColumnsAddedToOfflineTbl() {
            var aryColumns = [],
                objTable = _globalData.getTable();
            if(objTable){
                aryColumns = objTable.fnGetColumns();
            }
            return aryColumns;
        }
        function _isColumnToDeleteAlreadyMapped(crossImgObject/*jquery object*/) {
            if (!crossImgObject) return;
            var $crossImgObject = crossImgObject;
            //var strColName = $($(crossImgObject).parent().parent().find('span[id^="spanName"]')).text();
            var strColName = $crossImgObject.
                            closest('tr').
                            find('span[id^="spanName"]').text();
            var blnIsColumnMappedToParam = false,
                aryUpInParams = [],
                aryDownInParams=[],
                aryDownUpParams=[],
                aryMappedParams = [],
                i=0;
            
            for(i=0;i<=3;i++){
               aryMappedParams = [];
               switch(i){
                    case 0 :
                        aryMappedParams = _globalData.getDownloadOutputParam();
                       break;
                    case 1 :
                        aryMappedParams = _globalData.getDownloadInputParam();
                        break;
                    case 2 :
                        aryMappedParams = _globalData.getUploadModifiedRecInputParams();
                        break;
                    case 3 :
                        aryMappedParams = _globalData.getUploadNewRecInputParams();
                        break;
               }
               if( $.isArray(aryMappedParams) &&
                   aryMappedParams.length>0){
                   
                   $.each(aryMappedParams, function (index, value) {
                       if (value) {
                           if (_getMappingValueForHtmlByType(value.fnGetVal()) === strColName) {
                               blnIsColumnMappedToParam = true;
                               return false;
                           }
   
                       }
                   });        
               }
               if(blnIsColumnMappedToParam){
                   break;
               }
               
            }
            return blnIsColumnMappedToParam;

        }
        
        function _doesColumnNameExists(valueToCheck) {
            if (valueToCheck) {
                var blnExists = false;
                var objOfflineTable = _globalData.getTable();
                if(objOfflineTable){
                    var aryColumns = objOfflineTable.fnGetColumns();
                    if(aryColumns && $.isArray(aryColumns)
                        && aryColumns.length>0){
                        
                        aryColumns = $.grep(aryColumns,function(value,index){
                                return value.fnGetName().toLowerCase() === valueToCheck.toLowerCase();
                        });
                   
                        if(aryColumns.length>0){
                            blnExists = true;
                        }    
                                
                    }
               }

                return blnExists;
            }
        }
        function _getCountOfColumnsAddedToOfflineTbl() {
            //return $("#OfflineDTClmnDefinition tbody tr:not(\".dynamicRow\")").length;
            var aryColumns = _getAllColumnsAddedToOfflineTbl();
            var iCount =0;
            if(aryColumns && $.isArray(aryColumns) && aryColumns.length>0){
                iCount = aryColumns.length;
            }
            return iCount;
        }
        function _getColumnsFromHtml() {
            var aryAllColumnList = [];
            $("#OfflineDTClmnDefinition tbody tr:not(\".dynamicRow\")").each(function () {
                var spanColumnName = $(this).find("span[id^='spanName']");
                var spanDataType = $(this).find("span[id^='spanDataType']");
                var spanDataTypeVal = $(this).find("span[id^='spanDataTypeVal']");
                var spanDefault = $(this).find("span[id^='spanDefault']");
                //var spanMinVal = $(this).find("span[id^='spanMinVal']");
                //var spanMaxVal = $(this).find("span[id^='spanMaxVal']");
                var spanDecPlaces = $(this).find("span[id^='spanDecPlaces']");
                //var chkPrimary = $(this).find(".ChkPrimary");
                var iDecimalPlaces = 0;
                switch ($(spanDataTypeVal).text()) {
                    case Offline_Table_DataType.String:
                        iDecimalPlaces = 0;
                        break;
                    case Offline_Table_DataType.Number:
                        iDecimalPlaces = 0;
                        break;
                    case Offline_Table_DataType.Decimal:
                        iDecimalPlaces = $(spanDecPlaces).text();
                        break;
                }
                var clmnObj = new OfflineDatatableCol({
                        cnm : $(spanColumnName).text(),
                        ctyp:$(spanDataTypeVal).text(),
                        dpnt:iDecimalPlaces.toString(),
                        //min:$(spanMinVal).text(),
                        //max:$(spanMaxVal).text(),
                        dval:$(spanDefault).text(),
                        //pk:chkPrimary.is(":checked")?"1":"0"
                    });
                //var strClmJson = _util.serializeObjectToJson(clmnObj);
                //aryAllColumnList.push(strClmJson);
                aryAllColumnList.push(clmnObj);
            });
            return aryAllColumnList;
        }
        function _spanAddNewColumnClick(sender){
            if (sender) {
                var parsedDefaultVl = "";
                var offlineDTClmnDefinition = $("#OfflineDTClmnDefinition", _contextDivs.getOfflineTabCont());
                var parentTdOfSener = $(sender).parent();
                var parentTrOfTd = $(parentTdOfSener).parent();
                var name = $(parentTrOfTd).find("input[name='ColumnNm']"),
                    dataType = $(parentTrOfTd).find("select[name='DataType']"),
                    defaultVal = $(parentTrOfTd).find("input[name='DefaultVal']"),
                    //minVal = $(parentTrOfTd).find("input[name='MinValue']"),
                    //maxVal = $(parentTrOfTd).find("input[name='MaxValue']"),
                    decPlaces = $(parentTrOfTd).find("select[name='DecimalPlaces']"),
                    spanNotApplicable = $(parentTrOfTd).find("span[id='spanNotApplicable']");
                    //$primary=$(parentTrOfTd).find(".Primary");

                //Validating
                var strError = "",
                    parsedMinVal = "",
                    parsedMaxVal = "",
                    strTrimmedName ="",
                    strDefaultValue = "";
                
                
                strTrimmedName = $.trim($(name).val());
                if ( strTrimmedName === "") {
                    strError += "<li>Please enter Column name</li>";
                } else {
                    if (!OFFLINE_CONSTANTS.regxName.test(strTrimmedName)) {
                        strError += "<li>Please enter a valid Column name.Only numbers and letters are allowed.</li>";
                    }
                    else if(strTrimmedName.toLowerCase() === "rowid" || strTrimmedName.toLowerCase() === "oid"){
                        strError += "<li>RowId and OId as column name is not allowed.</li>";
                    }
                    else if(strTrimmedName === "RowCount") {
                        strError += "<li>RowCount as column name is not allowed.</li>";
                    }
                    else if (strTrimmedName.length > 50) {
                        strError += "<li>Please enter a valid Column name.Length should be less than 50 characters.</li>";
                    } else if (_doesColumnNameExists(strTrimmedName)) {
                        strError += "<li>Column name already exists.</li>";
                    }
                }
                strDefaultValue = $(dataType).val();
                if ( strDefaultValue === Offline_Table_DataType.Decimal ||
                     strDefaultValue === Offline_Table_DataType.Number) {
                    
                    if ($.trim($(defaultVal).val()) === "") {
                        strError += "<li>Please enter default value</li>";
                    } else {
                        
                        if ($(dataType).val() === Offline_Table_DataType.Decimal) {
                            parsedDefaultVl = parseFloat($(defaultVal).val());
                            if (isNaN(parsedDefaultVl)) {
                                strError += "<li>Please enter a valid default value</li>";
                            }
                        } else if ($(dataType).val() === Offline_Table_DataType.Number) {
                            parsedDefaultVl = parseInt($(defaultVal).val());
                            if (isNaN(parsedDefaultVl)) {
                                strError += "<li>Please enter a valid default value</li>";
                            }
                        }
                    }
                }
                if (strError === "") {
                    var decPlacesValue = "",
                        defaultValue = "";
                    switch ($(dataType).val()) {
                        case Offline_Table_DataType.Decimal:
                            decPlacesValue = $(decPlaces).val();
                            defaultValue = parseFloat($(defaultVal).val()).toFixed(decPlacesValue);
                            break;
                        case Offline_Table_DataType.String:
                            decPlacesValue = OFFLINE_CONSTANTS.NotApplicable;
                            defaultValue = $(defaultVal).val();
                            break;
                        case Offline_Table_DataType.Number:
                            decPlacesValue = OFFLINE_CONSTANTS.NotApplicable;
                            defaultValue = parseInt($(defaultVal).val());
                            break;
                        case Offline_Table_DataType.File:   
                            decPlacesValue = OFFLINE_CONSTANTS.NotApplicable;
                            defaultValue = $(defaultVal).val();
                            break;    
                    }

                    var index = $(offlineDTClmnDefinition).
                                find("tbody tr:not(tr.dynamicRow)").
                                length; /* To get the total rows in the table */
                    //{index, clnName, dataType, defVal, minVal, maxVal, decPlaces}
                    _addHTMLToClmDefinitionTbl({
                        index : index,
                        clnName:$.trim($(name).val()),
                        dataType:$(dataType).children(':selected').text(),
                        dataTypeVal:$(dataType).val(),
                        defVal:$.trim(defaultValue),
                        //minVal:$.trim($(minVal).val()),
                        //maxVal:$.trim($(maxVal).val()),
                        decPlaces:$.trim(decPlacesValue),
                        //primary:$primary.is(":checked")?true:false
                    });
                    _setColumnsInGlobalTableObject();
                    _initialiseColumnSettingDynamicRow();
                    
                } else {
                    showMessage("<ul>" + strError + "</ul>", MessageOption.Error, DialogType.Error);
                }
            }
        }
        function _setColumnsInGlobalTableObject(){
            var aryColumns = _getColumnsFromHtml(),
                objOfflineTable =_globalData.getTable() ;
            if(objOfflineTable){
                objOfflineTable.fnSetColumns(aryColumns);
            }
        }
        function _isTheValueAOfdbCol(text){
            var columns = _getAllColumnsAddedToOfflineTbl();
            if($.isArray(columns) && columns.length>0){
                columns = $.grep(columns,function(value,index){
                       return value.fnGetName() === text;
                });
                if($.isArray(columns) && columns.length>0){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        return{
            formColumnsHtml : function(columns/*array of columns*/){
                $('#OfflineDTClmnDefinition tbody tr:not("tr.dynamicRow")', _contextDivs.getOfflineTabCont()).remove();
               
                
                _initialiseColumnSettingDynamicRow();
                var aryClmDetls = columns;
                
                if (aryClmDetls && $.isArray(aryClmDetls)) {
                    $.each(aryClmDetls, function (index, value) {
                        var objClmDet = value;
                        var strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable,
                            strColumnTypeInString = "";
                        switch (objClmDet.fnGetDataType()) {
                            case Offline_Table_DataType.String:
                                strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable;
                                strColumnTypeInString = Offline_Table_DataType_STRING.String;
                                break;
                            case Offline_Table_DataType.Number:
                                strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable;
                                strColumnTypeInString = Offline_Table_DataType_STRING.Number;
                                break;
                            case Offline_Table_DataType.Decimal:
                                strDecimalPlaces = objClmDet.dpnt;
                                strColumnTypeInString = Offline_Table_DataType_STRING.Decimal;
                                break;
                        }
                        _addHTMLToClmDefinitionTbl({
                            index : index,
                            clnName:objClmDet.fnGetName(),
                            dataType:strColumnTypeInString,
                            dataTypeVal:objClmDet.fnGetDataType(),
                            defVal:objClmDet.fnGetDefaultValue(),
                            minVal:objClmDet.fnGetMinValue(),
                            maxVal:objClmDet.fnGetMaxValue(),
                            decPlaces:objClmDet.fnGetDecimalPlaces(),
                            primary:objClmDet.fnGetPrimaryKeyValue()==="1"?true:false
                        });
                    });
                }
            },
            getCountOfColumnsAddedToOfflineTbl:function(){
                return _getCountOfColumnsAddedToOfflineTbl();
            },
            getAllColumns : function(){
                return _getAllColumnsAddedToOfflineTbl();
            },
            setColumnsInGlobalTableObject: function () {
                _setColumnsInGlobalTableObject();
            },
            spanAddNewColumnClick:function(sender){
                _spanAddNewColumnClick(sender);
            },
            isTheValueAOfdbCol : function(text){
                return _isTheValueAOfdbCol(text);
            }
        };
    })();
    
    var _syncTab = (function(){
        var _getjQueryObj = {
            initialDownloadCont :function(){
                return $('#divInitialDownloadCont');
            },
            chkInitialDownload:function(){
                return $('#chkInitialDownload');
            },
            deleteAfterUploadCont:function(){
                return $('#divInitialDownloadCont');
            },
            chkDeleteAfterUpload:function(){
                return $('#chkUploadDeleteRec');
            },
            modifiedRecUploadCmdCont:function(){
                return $('#divModifiedRecUploadCommandCont'); 
            },
            ddlModifiedRecUploadCmd:function(){
                return $('#ddlModifiedRecUploadCommand'); 
            },
            modifiedRecUploadCmdSettingsCont:function(){
                return $('#divModifiedRecUploadCmdSettingsCont');
            },
            ddlSyncOnlnOfflnCommand:function(){
               return $('select[id$="ddlSyncOnlnOfflnCommand"]');
            },
            ddlSyncOfflnOnlnCommand:function(){
               return $('select[id$="ddlSyncOfflnOnlnCommand"]');
            },
            chkModifiedRecSameAsNewRecord:function(){
               return $('#chkModifiedRecSameAsNewRecord');
            },
            divOfflineSyncDwnLdFieldsCont:function(){
                return $('#divOfflineSyncDwnLdFieldsCont');
            },
            tblOflnModifiedRecInsertCmdInpParam:function(){
                return $('#tblOflnModifiedRecInsertCmdInpParam');
            },
            ddlSyncCommandType : function(){
                return $('#ddlSyncCommandType');
            },
            ddlSyncOnlineConn : function(){
                return $('#ddlSyncOnlineConn');
            },
            divOfflineSyncUpLdFieldsCont : function(){
                return $('#divOfflineSyncUpLdFieldsCont');
            },
            tblOfflineDownloadCmdInParam:function(){
                return $('#tblOfflineDownloadCmdInParam');
            },
            tblOfflineDownloadCmdOutput : function(){
                return $('#tblOfflineDownloadCmdOutput');
            },
            tblOfflineUploadCmdInParam:function(){
                return $('#tblOfflineUploadCmdInpParam');
            },
            tblEditOflnModifiedRecInsertCmdInpParam : function(){
                return $('#tblEditOflnModifiedRecInsertCmdInpParam');  
            },
            tblEditOfflineSelectCmdInParam : function(){
                return $('#tblEditOfflineSelectCmdInParam'); 
            },
            tblEditOfflineInsertCmdInpParam:function(){
                return $('#tblEditOfflineInsertCmdInpParam'); 
            },
            tblEditOfflineSelectCmdOutput:function(){
                return $('#tblEditOfflineSelectCmdOutput');
            },
            divUploadModifiedRecCont:function(){
                return $('#divUploadModifiedRecCont');
            },
            divDisallowDataAfterExpiryCont : function(){
                return $('#divDisallowDataAfterExpiryCont');
            },
            chkDisallowDataAfterExpiry : function(){
                return $('#chkDisallowDataAfterExpiry');
            },
            divCleanRecBeforeDownloadCont : function(){
                return $('#divCleanRecBeforeDownloadCont');
            },
            chkCleanRecBeforeDownload : function(){
                return $('#chkCleanRecBeforeDownload');
            },
            txtUploadRecordCriteria : function(){
                return $('input[id$="txtUploadRecordCriteria"]');
            },
            txtShouldUploadWithin : function(){
                return $('input[id$="txtShouldUploadWithin"]');
            },
            allCntrls : function(){
                return {
                    ddlSyncCommandType : this.ddlSyncCommandType(),
                    ddlSyncOnlineConn: this.ddlSyncOnlineConn(),
                    ddlSyncDownCommand:this.ddlSyncOnlnOfflnCommand(),
                    ddlSyncUpCommand :this.ddlSyncOfflnOnlnCommand(),
                    chkInitialDownload : this.chkInitialDownload(),
                    chkDeleteAfterUpload : this.chkDeleteAfterUpload(),
                    ddlModifiedRecUploadCmd : this.ddlModifiedRecUploadCmd(),
                    chkModifiedRecSameAsNewRecord : this.chkModifiedRecSameAsNewRecord(),
                    divUploadModifiedRecCont: this.divUploadModifiedRecCont(),
                    modifiedRecUploadCmdSettingsCont:this.modifiedRecUploadCmdSettingsCont(),
                    divDisallowDataAfterExpiryCont: this.divDisallowDataAfterExpiryCont(),
                    chkDisallowDataAfterExpiry : this.chkDisallowDataAfterExpiry(),
                    divCleanRecBeforeDownloadCont : this.divCleanRecBeforeDownloadCont(),
                    chkCleanRecBeforeDownload : this.chkCleanRecBeforeDownload(),
                    txtUploadRecordCriteria : this.txtUploadRecordCriteria(),
                    txtShouldUploadWithin : this.txtShouldUploadWithin()
                }
            }
        };
        var _setInnerHtml = {
            ddlModifiedRecUploadCmd:function(html/*string*/){
                _getjQueryObj.ddlModifiedRecUploadCmd().html(html);
            },
            ddlSyncOnlnOfflnCommand:function(html/*string*/){
                _getjQueryObj.ddlSyncOnlnOfflnCommand().html(html);
            },
            ddlSyncOfflnOnlnCommand:function(html/*string*/){
                _getjQueryObj.ddlSyncOfflnOnlnCommand().html(html);
            }
        };
        var _setInitialState = {
            ddlModifiedRecUploadCmd:function(){
                _setInnerHtml.ddlModifiedRecUploadCmd('<option value="-1">Select</option>');
            },
            ddlSyncOnlnOfflnCommand:function(){
                _setInnerHtml.ddlSyncOnlnOfflnCommand('<option value="-1">Select</option>');
            },
            ddlSyncOfflnOnlnCommand:function(){
                _setInnerHtml.ddlSyncOfflnOnlnCommand('<option value="-1">Select</option>');
            },
            chkInitialDownload:function(){
                _setValue.chkInitialDownload(false);
            },
            chkModifiedRecSameAsNewRecord:function(){
               _setValue.chkModifiedRecSameAsNewRecord(true);
            }
        };
        var _setValue = {
            ddlModifiedRecUploadCmd:function(value){
                var $control = _getjQueryObj.ddlModifiedRecUploadCmd();
                $control.val(value);
                updateUniformedControl($control);
            },
            ddlSyncOnlnOfflnCommand:function(value){
                var $control = _getjQueryObj.ddlSyncOnlnOfflnCommand();
                $control.val(value);
                updateUniformedControl($control);
            },
            ddlSyncOfflnOnlnCommand:function(value){
                var $control = _getjQueryObj.ddlSyncOnlnOfflnCommand();
                $control.val(value);
                updateUniformedControl($control);
            },
            chkInitialDownload :function(check/*boolean*/){
                var $control = _getjQueryObj.chkInitialDownload();
                
                if(check === true){
                    $control.prop("checked",true);
                }
                else{
                    $control.prop("checked",false);
                }
                updateUniformedControl($control);
            },
            chkModifiedRecSameAsNewRecord :function(check/*boolean*/){
                var $control = _getjQueryObj.chkModifiedRecSameAsNewRecord();
                
                if(check === true){
                    $control.prop("checked",true);
                }
                else{
                    $control.prop("checked",false);
                }
                updateUniformedControl($control);
            },
            ddlSyncCommandType:function(value){
                var $control = _getjQueryObj.ddlSyncCommandType();
                $control.val(value);
                updateUniformedControl($control);
            },
            ddlSyncOnlineConn:function(value){
                var $control = _getjQueryObj.ddlSyncOnlineConn();
                $control.val(value);
                updateUniformedControl($control);
            }
        };
        var _showHide = {
            initialDownloadCont :function(show/*boolean*/){
                if(show === true){
                    _getjQueryObj.initialDownloadCont().show();
                }
                else{
                    _getjQueryObj.initialDownloadCont().hide();
                }
            },
            chkInitialDownload :function(show/*boolean*/){
                if(show=== true){
                    _getjQueryObj.chkInitialDownload().show();
                }
                else{
                    _getjQueryObj.chkInitialDownload().hide();
                }
            },
            deleteAfterUploadCont :function(show/*boolean*/){
                if(show === true){
                    _getjQueryObj.deleteAfterUploadCont().show();
                }
                else{
                    _getjQueryObj.deleteAfterUploadCont().hide();
                }
            },
            chkDeleteAfterUpload :function(show/*boolean*/){
                if(show=== true){
                    _getjQueryObj.chkDeleteAfterUpload().show();
                }
                else{
                    _getjQueryObj.chkDeleteAfterUpload().hide();
                }
            },
            modifiedRecUploadCmdCont:function(show/*boolean*/){
                if(show=== true){
                    _getjQueryObj.modifiedRecUploadCmdCont().show();
                }
                else{
                    _getjQueryObj.modifiedRecUploadCmdCont().hide();
                }
             },
            modifiedRecUploadCmdSettingsCont:function(show/*boolean*/){
                  if(show=== true){
                      _getjQueryObj.modifiedRecUploadCmdSettingsCont().show();
                  }
                  else{
                      _getjQueryObj.modifiedRecUploadCmdSettingsCont().hide();
                  }
             },
            divOfflineSyncUpLdFieldsCont : function(show/*boolean*/){
                if(show=== true){
                    _getjQueryObj.divOfflineSyncUpLdFieldsCont().show();
                }
                else{
                    _getjQueryObj.divOfflineSyncUpLdFieldsCont().hide();
                }
            },
            divOfflineSyncDwnLdFieldsCont : function(show/*boolean*/){
                if(show=== true){
                    _getjQueryObj.divOfflineSyncDwnLdFieldsCont().show();
                }
                else{
                    _getjQueryObj.divOfflineSyncDwnLdFieldsCont().hide();
                }
            }
        };
        var _paramMapping = (function(){
            
            var MAPPING_PROCESS = {
                modifiedRec : "modifiedRec",
                download : "download",
                upload :"upload"
            }
            //autocomplete
            function _getTagsForAutoCompleteTextbox(mappingProcess /*MAPPING_PROCESS*/) {
                var availableTags=[];
                var aryFinalAvailableTags = [];
                if(mappingProcess === MAPPING_PROCESS.upload || mappingProcess === MAPPING_PROCESS.modifiedRec){
                    availableTags = _columnTab.getAllColumns();
                    if ($.isArray(availableTags) && availableTags.length>0) {
                        for (var i = 0; i <= availableTags.length - 1; i++) {
                            aryFinalAvailableTags.push({ prop: availableTags[i].fnGetName(), val: availableTags[i].fnGetName(),category:"Columns" });
                        }
                        //{prop:"@FNAME",val:"@FNAME",category:"Fixed Values"}
                    }
                }
                else{
                    $.merge(aryFinalAvailableTags, OFFLINE_CONSTANTS.intlsenseFixedParams);
                }
                return aryFinalAvailableTags;
            }
            /*
                * table --> jquery object table where html needs to be generated
            */
            function _makeParamMappingTextboxAutoComplete(table,mappingProcess/*MAPPING_PROCESS*/) {
                var aryFinalAvailableTags = _getTagsForAutoCompleteTextbox(mappingProcess);
                table.find("tbody tr").each(function () {
                    var inputParmVal = $(this).find("input[id^='inputPrm']");
                    $(inputParmVal).textIntellisense({
                        IntlsJson:JSON.stringify(aryFinalAvailableTags)
                    });
                });
            }
            
            var _inputParamHelper = (function(){
                function _getHtmlForClmMapping(index, value) {
                    var spanId = "spanPrmNm" + index;
                    var inputId = "inputPrm" + index;
                    var strHTMLToInsert = "<tr><td><span id=\"" + spanId + "\">" + value + "</span></td><td><input type=\"text\" id=\"" + inputId + "\" onkeydown=\"return enterKeyFilter(event);\"></input></td></tr>";
                    return strHTMLToInsert;
                }
            
                function _getHtmlForClmMappedForEditView(index, value) {
                    var spanId = "spanPrmNm" + index;
                    var inputId = "spanPrmVal" + index;
                    var strHTMLToInsert = "<tr><td><span id=\"" + spanId + "\">" + value + "</span></td><td><span  id=\"" + inputId + "\"></span></td></tr>";
                    return strHTMLToInsert;
                }
                function _setParamValInTextBoxForEdit(inputParams/*array of output parmas*/,table) {
                   if($.isArray(inputParams) && inputParams.length>0){  
                        table.find("tbody tr").each(function () {
                            var spanParamName = $(this).find("span[id^='spanPrmNm']");
                            var inputParmVal = $(this).find("input[id^='inputPrm']");
                            $(inputParmVal).val(
                                _getTheParamValueFromMappedObject(
                                    inputParams, $(spanParamName).text()
                            ));
                        });
                   }
                }
                function _setParamValInSpanForEditView(inputParams/*array of output parmas*/,table) {
                    if($.isArray(inputParams) && inputParams.length>0){ 
                        table.find("tbody tr").each(function () {
                            var spanParamName = $(this).find("span[id^='spanPrmNm']");
                            var inputParmVal = $(this).find("span[id^='spanPrmVal']");
                            
                            $(inputParmVal).text(
                                _getTheParamValueFromMappedObject(
                                    inputParams, $(spanParamName).text()
                            ));
                        });
                    }
                }
                /*
                    * inputParamArray --> array of cmd objects input params
                    * isEdit --> bool 
                    * table --> jquery object table where html needs to be generated
                */
                function _formHtmlForInputParamMapping(inputParamArray,forEditView,table) {
                    var objTBody = null;
                    objTBody = table.find("tbody");
                    objTBody.children().remove();
                    if (inputParamArray && inputParamArray.length > 0) {
                        $.each(inputParamArray, function (index, value) {
                            if (forEditView) {
                                $(objTBody).append(_getHtmlForClmMappedForEditView(index, value));
                            } else {
                                $(objTBody).append(_getHtmlForClmMapping(index, value));
                            }
                        });
                    }
                }
                
                function _isSameOfdbColValueMappedToDiffInputParam (procFor){
                    var $table = $([]);
                    switch(procFor){
                        case MAPPING_PROCESS.modifiedRec:
                                $table = _getjQueryObj.tblOflnModifiedRecInsertCmdInpParam();
                            break;
                        case MAPPING_PROCESS.download:
                                $table = _getjQueryObj.tblOfflineDownloadCmdInParam();
                            break;
                        case MAPPING_PROCESS.upload:
                            $table = _getjQueryObj.tblOfflineUploadCmdInParam();
                        break;    
                    }
                    var availableTags = _columnTab.getAllColumns();
                    var aryOfVlueSelected = [];
                    var isSameValueMappedToDiffInput = false;
                    if ($.isArray(availableTags)){ 
                        
                        $table
                        .find("tbody tr").each(function () {
                            
                            var spanParamName = $(this).find("span[id^='spanPrmNm']");
                            var txtParamValue = $(this).find("input[id^='inputPrm']");
                            var objParamVal = {};
                            objParamVal.para = $(spanParamName).text();
                            objParamVal.val = $(txtParamValue).val();
                            if (objParamVal.val !== "") {
                                if(_columnTab.isTheValueAOfdbCol(objParamVal.val)){
                                    if ($.inArray(objParamVal.val, aryOfVlueSelected) === -1) {
                                         aryOfVlueSelected.push(objParamVal.val);
                                         return true;
                
                                    } else {
                                        isSameValueMappedToDiffInput = true;
                                        return false;
                                    }
                                }    
                            }
                            
                        });
            
                    }
                    return isSameValueMappedToDiffInput;
                }
                
                function _getMappedObjectsFromHtml(procFor){
                    var aryInputParams = [];
                    var $table = $([]);
                    switch(procFor){
                        case MAPPING_PROCESS.modifiedRec:
                                $table = _getjQueryObj.tblOflnModifiedRecInsertCmdInpParam();
                            break;
                        case MAPPING_PROCESS.download:
                                $table = _getjQueryObj.tblOfflineDownloadCmdInParam();
                            break;
                        case MAPPING_PROCESS.upload:
                                $table = _getjQueryObj.tblOfflineUploadCmdInParam();
                        break;    
                    }
                    $table
                    .find("tbody tr").each(function () {
                        
                        var spanParamName = $(this).find("span[id^='spanPrmNm']");
                        var txtParamValue = $(this).find("input[id^='inputPrm']");
                        var objParamVal = new CmdParamDtls();
                        var strFinalValueMapped = _getMappingValueToSaveInObjectByType($(txtParamValue).val());
                        objParamVal.fnSetPara($(spanParamName).text());
                        objParamVal.fnSetVal(strFinalValueMapped);
                        
                        aryInputParams.push(objParamVal);
                        
                    });
                    
                    return aryInputParams;
                }
                return {
                    processShowInputParamPopup : function(procFor/*MAPPING_PROCESS*/,forEditView){
                        
                        var commandId= "",
                            commandType = "",
                            $tblForFormingHtml= $([]),
                            strPopupDiv = "",
                            objSyncTabControls = _getjQueryObj.allCntrls(),
                            arySavedInputParams =[],
                            blnCheckColumnsExistBeforeMapping = false;
                        
                        if (forEditView == null) {
                               forEditView = false;
                        }
                        commandType = _globalData.getCommandType();
                        switch(procFor){
                            case MAPPING_PROCESS.modifiedRec:
                                    //$ddlCommand = _getjQueryObj.ddlModifiedRecUploadCmd();
                                    commandId = _globalData.getUploadModifiedRecCmd();
                                    if(forEditView=== true){
                                        $tblForFormingHtml = _getjQueryObj.tblEditOflnModifiedRecInsertCmdInpParam();
                                        strPopupDiv = 'divEditOflnModifiedRecInsertCmdInpParamCont';
                                        arySavedInputParams = _globalData.getUploadModifiedRecInputParams();
                                    }
                                    else{
                                        $tblForFormingHtml = _getjQueryObj.tblOflnModifiedRecInsertCmdInpParam();
                                        strPopupDiv = 'divOflnModifiedRecInsertCmdInpParamCont';
                                        arySavedInputParams = _globalData.getUploadModifiedRecInputParams(); 
                                    }
                                break;
                            case MAPPING_PROCESS.download:
                                    //$ddlCommand = _getjQueryObj.ddlSyncOnlnOfflnCommand();
                                    commandId = _globalData.getDownloadCommand();
                                    if(forEditView=== true){
                                        $tblForFormingHtml = _getjQueryObj.tblEditOfflineSelectCmdInParam();
                                        strPopupDiv = 'divEditOfflineSelectCmdInParamCont';
                                        arySavedInputParams = _globalData.getDownloadInputParam();
                                    }
                                    else{
                                        $tblForFormingHtml = _getjQueryObj.tblOfflineDownloadCmdInParam();
                                        strPopupDiv = 'divOfflineDownloadCmdInParamCont';
                                        arySavedInputParams = _globalData.getDownloadInputParam();
                                    }
                                break;
                            case MAPPING_PROCESS.upload:
                                //$ddlCommand = _getjQueryObj.ddlSyncOfflnOnlnCommand();
                                commandId = _globalData.getUploadNewRecCmd();
                                if(forEditView=== true){
                                    $tblForFormingHtml = _getjQueryObj.tblEditOfflineInsertCmdInpParam();
                                    strPopupDiv = 'divEditOfflineInsertCmdInpParamCont';
                                    arySavedInputParams = _globalData.getUploadNewRecInputParams();
                                }
                                else{
                                    $tblForFormingHtml = _getjQueryObj.tblOfflineUploadCmdInParam();
                                    strPopupDiv = 'divOfflineUploadCmdInpParamCont';
                                    arySavedInputParams = _globalData.getUploadNewRecInputParams();
                                }
                            break;    
                        }
                        
                       var aryInputParam = [];
                       
                        if (!forEditView) {
                           
                            if (commandType == undefined || commandType === "") {
                                throw new MfError(["Please select a object type"]);
                            }
                            // if ($ddlCommand.val() == "-1") {
                            //     throw new MfError(["Please select a command"]);
                            // }
                            if (commandId == undefined || commandId === "") {
                                throw new MfError(["Please select a object"]);
                            }
                        }
                       
                        aryInputParam = mfOflnTblHelpers
                                        .getInputParamsForCmdObject(
                                            commandId,
                                            commandType
                                        );
                        if (aryInputParam.length > 0) {
                            if(procFor === MAPPING_PROCESS.download){

                                blnCheckColumnsExistBeforeMapping = false;
                            }
                            else{
                                blnCheckColumnsExistBeforeMapping = true;
                            }
                            if(blnCheckColumnsExistBeforeMapping){
                                if(_columnTab.getCountOfColumnsAddedToOfflineTbl() > 0){
                                    
                                }
                                else{
                                    showMessage("Please add columns to offline tables.", MessageOption.Info, DialogType.Info);
                                    return;
                                }
                            }
                            if (!forEditView) {
                                _formHtmlForInputParamMapping(aryInputParam
                                        ,false
                                        ,$tblForFormingHtml);
                                  
                                _setParamValInTextBoxForEdit(arySavedInputParams,$tblForFormingHtml);
                                _makeParamMappingTextboxAutoComplete($tblForFormingHtml,procFor);
                                showModalPopUp(strPopupDiv,
                                    "Input Parameters", 600, false);
                             } else {
                                _formHtmlForInputParamMapping(aryInputParam
                                     ,true
                                     ,$tblForFormingHtml);
                                _setParamValInSpanForEditView(arySavedInputParams,$tblForFormingHtml);
                                showModalPopUp(strPopupDiv,
                                    "Input Parameters", 600, false);
                            }
                        } else {
                           showMessage("No input parameter available for command", MessageOption.Info, DialogType.Info);
                        }
                    },
                    getMappedObjectsFromHtml : function(procFor/*MAPPING_PROCESS*/){
                        return _getMappedObjectsFromHtml(procFor);
                    },
                    isSameOfdbColValueMappedToDiffInputParam:function(procFor/*MAPPING_PROCESS*/){
                        return _isSameOfdbColValueMappedToDiffInputParam(procFor);
                    }
                };
            })();
            var _outputParamHelper = (function(){
                function _getHtmlForClmMappedForEditView(index, value) {
                    var spanId = "spanPrmNm" + index;
                    var inputId = "spanPrmVal" + index;
                    var strHTMLToInsert = "<tr><td><span id=\"" + spanId + "\">" + value + "</span></td><td><span  id=\"" + inputId + "\"></span></td></tr>";
                    return strHTMLToInsert;
                }
                function _getHTMLOutputClmMapping(index, value) {
                    var spanId = "spanPrmNm" + index;
                    var inputId = "selectClm" + index;
                    var strHTMLToInsert = "<tr><td><span id=\"" + spanId + "\">" + value + "</span></td>";
                    strHTMLToInsert += "<td><select name=\"" + inputId + "\"  class=\"notUniform\" onkeydown=\"return enterKeyFilter(event);\">";
                    strHTMLToInsert += "</select></td></tr>";
                    return strHTMLToInsert;
                }
                function _addOptionsToDropDowns(availableTags,table/*jquery obj*/) {
                   
                    if ($.isArray(availableTags)) {
                        table.find("tbody tr").each(function () {
                            var selectParmVal = $(this).find("select[name^='selectClm']"),
                                strColVal = "";
                            $(selectParmVal).append($('<option>', {
                                value: "-1",
                                text: "None"
                            }));
                            $.each(availableTags, function (index, val) {
                                strColVal = val.fnGetName();
                                $(selectParmVal).append($('<option>', {
                                    value: strColVal,
                                    text: strColVal
                                }));
                            });
                        });
                    }
                }
                function _setParamValInDropDownForEdit(outputParams/*array of output parmas*/,table) {
                    if($.isArray(outputParams) && outputParams.length>0){
                        table.find("tbody tr").each(function () {
                            var spanParamName = $(this).find("span[id^='spanPrmNm']");
                            var selectParmVal = $(this).find("select[name^='selectClm']");
                            
                            var paramVal = _getTheParamValueFromMappedObject(
                                              outputParams, $(spanParamName).text()
                                             );
                            
                            if (paramVal === "") {
                                $(selectParmVal).val("-1");
                            } else {
                                $(selectParmVal).val(paramVal);
                            }
                        });
                    }
                }
                function _setParamValInSpanForEditView(outputParams/*array of output parmas*/,table) {
                    if($.isArray(outputParams) && outputParams.length>0){
                        table.find("tbody tr").each(function () {
                            var spanParamName = $(this).find("span[id^='spanPrmNm']");
                            var spanParmVal = $(this).find("span[id^='spanPrmVal']");
                            var paramVal = _getTheParamValueFromMappedObject(
                                                outputParams, $(spanParamName).text()
                                           );
                            $(spanParmVal).text(paramVal);
                        });
                    }
                }
                /*
                    * outputParamArray --> array of cmd objects input params
                    * isEdit --> bool 
                    * table --> jquery object table where html needs to be generated
                */
                function _formHtmlForInputParamMapping(outputParamArray,forEditView,table) {
                    var objTBody = null;
                    objTBody = table.find("tbody");
                    objTBody.children().remove();
                    if (outputParamArray && outputParamArray.length > 0) {
                        $.each(outputParamArray, function (index, value) {
                            if (forEditView) {
                                $(objTBody).append(_getHtmlForClmMappedForEditView(index, value));
                            } else {
                                $(objTBody).append(_getHTMLOutputClmMapping(index, value));
                            }
                        });
                    }
                }
                function _isSameOfdbColValueMappedToDiffOutputParam(procFor) {
                    var $table = $([]);
                    switch(procFor){
                        case MAPPING_PROCESS.modifiedRec:
                            break;
                        case MAPPING_PROCESS.download:
                                $table = _getjQueryObj.tblOfflineDownloadCmdOutput();;
                            break;
                    }
                    var availableTags = _columnTab.getAllColumns();
                    var aryOfVlueSelected = [];
                    var isSameValueMappedToDiffInput = false;
                    if (availableTags) {
                        $table.find("tbody tr").each(function () {
                            var spanParamName = $(this).find("span[id^='spanPrmNm']");
                            var selectParamVal = $(this).find("select[name^='selectClm']");
                            var objParamVal = new Object();
                            objParamVal.para = $(spanParamName).text();
                            objParamVal.val = $(selectParamVal).val();
                            if (objParamVal.val !== "" && objParamVal.val !== "-1") {
                                if(_columnTab.isTheValueAOfdbCol(objParamVal.val)){
                                    if ($.inArray(objParamVal.val, aryOfVlueSelected) === -1) {
                                        aryOfVlueSelected.push(objParamVal.val);
                                        return true;
            
                                    } else {
                                        isSameValueMappedToDiffInput = true;
                                        return true;
                                    }
                                }
                            }    
                        });
            
                    }
                    return isSameValueMappedToDiffInput;
                }
                
                function _getMappedObjectsFromHtml(procFor) {
                    var $table = $([]);
                    switch(procFor){
                        case MAPPING_PROCESS.modifiedRec:
                            break;
                        case MAPPING_PROCESS.download:
                                $table = _getjQueryObj.tblOfflineDownloadCmdOutput();;
                            break;
                    }
                    var aryDownloadOutput = [];
                    var objOutputJson = {};
                    $table.find("tbody tr").each(function () {
                        var spanParamName = $(this).find("span[id^='spanPrmNm']");
                        var selectPrmColVal = $(this).find("select[name^='selectClm']");
                        var strParamValue = "";
                        var objParamVal = new CmdParamDtls();
                        objParamVal.fnSetPara($(spanParamName).text());
                        if($(selectPrmColVal).val() === "-1"){
                            strParamValue = "";
                        }
                        else{
                            strParamValue = _getMappingValueToSaveInObjectByType(
                                $(selectPrmColVal).val());
                        }
                      
                        objParamVal.fnSetVal(strParamValue);
                        
                        aryDownloadOutput.push(objParamVal);
                    });
                    
                    return aryDownloadOutput;
                }
                return {
                   processShowOutputParamPopup : function(procFor/*MAPPING_PROCESS*/,forEditView) {
                       if (forEditView == null) {
                           forEditView = false;
                       }
                      
                       var commandId= "",
                           commandType = "",
                           $tblForFormingHtml= $([]),
                           strPopupDiv = "",
                           objSyncTabControls = _getjQueryObj.allCntrls(),
                           arySavedOutputParams =[];
                       commandType = _globalData.getCommandType();
                       switch(procFor){
                           case MAPPING_PROCESS.modifiedRec:
                               break;
                           case MAPPING_PROCESS.download:
                                   //$ddlCommand = _getjQueryObj.ddlSyncOnlnOfflnCommand();
                                   commandId = _globalData.getDownloadCommand();
                                   if(forEditView === true){
                                        $tblForFormingHtml = _getjQueryObj.tblEditOfflineSelectCmdOutput();
                                        strPopupDiv = 'divEditOfflineSelectCmdOutput';
                                   }
                                   else{
                                       $tblForFormingHtml = _getjQueryObj.tblOfflineDownloadCmdOutput();
                                       strPopupDiv = 'divOfflineDownloadCmdOutput';
                                   }
                                   arySavedOutputParams = _globalData.getDownloadOutputParam();
                               break;
                       }
                       var aryOutputParams = [];
                       if(!forEditView){
                           if (commandType == undefined || commandType === "") {
                            //   showMessage("Please select a command type", MessageOption.Error, DialogType.Error);
                            //   return;
                            throw new MfError(["Please select a object type"]);
                           }
                           if (commandId == undefined || commandId === "") {
                            //   showMessage("Please select a command", MessageOption.Error, DialogType.Error);
                            //   return;
                                throw new MfError(["Please select a object"]);
                           }
                       }
                       
                       aryOutputParams = mfOflnTblHelpers
                                        .getOutputParamsForCmdObject(
                                            commandId,
                                            commandType
                                        );
                       
                       if (aryOutputParams.length > 0) {
                           if (_columnTab.getCountOfColumnsAddedToOfflineTbl() > 0) {
                               if (!forEditView) {
                                   _formHtmlForInputParamMapping(aryOutputParams, false,$tblForFormingHtml);
                                   _addOptionsToDropDowns(_columnTab.getAllColumns(),$tblForFormingHtml);
                                   _setParamValInDropDownForEdit(arySavedOutputParams,$tblForFormingHtml);
                                   showModalPopUp(strPopupDiv,"Output Parameters", 500, false);
                               } else {
                                   _formHtmlForInputParamMapping(aryOutputParams, true,$tblForFormingHtml);
                                   _setParamValInSpanForEditView(arySavedOutputParams,$tblForFormingHtml);
                                   showModalPopUp(strPopupDiv,"Output Parameters", 500, false);
                               }
                           } else {
                               showMessage("Please add columns to offline tables.", MessageOption.Info, DialogType.Info);
                           }
                        } else {
                           showMessage("No output parameter available for command", MessageOption.Info, DialogType.Info);
                       }
                    },
                   getMappedObjectsFromHtml : function(procFor/*MAPPING_PROCESS*/){
                       return _getMappedObjectsFromHtml(procFor);
                   },
                   isSameOfdbColValueMappedToDiffOutputParam:function(procFor/*MAPPING_PROCESS*/){
                       return _isSameOfdbColValueMappedToDiffOutputParam(procFor);
                   } 
                };
            })();
            
            return {
                modifiedRec :{
                    showInputParamPopup :function(forEditView /*bool*/){
                       if (forEditView == null) {
                           forEditView = false;
                       
                       }
                        _inputParamHelper.processShowInputParamPopup(MAPPING_PROCESS.modifiedRec,forEditView);
                    },
                    saveMappingDtls : function(){
                       if (_inputParamHelper.isSameOfdbColValueMappedToDiffInputParam(MAPPING_PROCESS.modifiedRec)) {
                           throw new MfError(["You have mapped more than one parameter with same column."]);
                       }
                       var aryMappedParams = _inputParamHelper.getMappedObjectsFromHtml(MAPPING_PROCESS.modifiedRec);
                      
                       if ($.isArray(aryMappedParams) &&
                           aryMappedParams.length>0) {
                           
                            _globalData.setUploadModifiedRecInputParams(aryMappedParams);
                             
                       } else {
                           _globalData.setUploadModifiedRecInputParams([]);
                       }
                    },
                    closeInputMappingPopup:function(){
                       closeModalPopUp('divOflnModifiedRecInsertCmdInpParamCont');
                    }
                },
                download :{
                    showInputParamPopup :function(forEditView /*bool*/){
                       if (forEditView == null) {
                           forEditView = false;
                       }
                     _inputParamHelper.processShowInputParamPopup(MAPPING_PROCESS.download,forEditView);  
                    },
                    showOutputParamPopup : function(forEditView/*bool*/){
                        if (forEditView == null) {
                              forEditView = false;
                          }
                        _outputParamHelper.processShowOutputParamPopup(MAPPING_PROCESS.download,forEditView);  
                    },
                    saveInputMappingDtls : function(){
                        if (_inputParamHelper.isSameOfdbColValueMappedToDiffInputParam(MAPPING_PROCESS.download)) {
                            throw new MfError(["You have mapped more than one parameter with same column."]);
                        }
                        var aryMappedParams = _inputParamHelper.getMappedObjectsFromHtml(MAPPING_PROCESS.download);
                       
                        if ($.isArray(aryMappedParams) &&
                            aryMappedParams.length>0) {
                            
                             _globalData.setDownloadInputParam(aryMappedParams);
                              
                        } else {
                            
                            _globalData.setDownloadInputParam([]);
                        }
                    },
                    saveOutputMappingDtls : function(){
                        if (_outputParamHelper.isSameOfdbColValueMappedToDiffOutputParam(MAPPING_PROCESS.download)) {
                            throw new MfError(["You have mapped more than one parameter with same column."]);
                        }
                        var aryMappedParams = _outputParamHelper.getMappedObjectsFromHtml(MAPPING_PROCESS.download);
                       
                        if ($.isArray(aryMappedParams) &&
                            aryMappedParams.length>0) {
                            
                             _globalData.setDownloadOutputParam(aryMappedParams);
                              
                        } else {
                            
                            _globalData.setDownloadOutputParam([]);
                        }
                    },
                    closeOutputMappingPopup:function(){
                       closeModalPopUp('divOfflineDownloadCmdOutput');
                    },
                    closeInputMappingPopup:function(){
                       closeModalPopUp('divOfflineDownloadCmdInParamCont');
                    }
                },
                upload:{
                    showInputParamPopup :function(forEditView /*bool*/){
                       if (forEditView == null) {
                           forEditView = false;
                       }
                     _inputParamHelper.processShowInputParamPopup(MAPPING_PROCESS.upload,forEditView);  
                    },
                    saveInputMappingDtls : function(){
                        if (_inputParamHelper.isSameOfdbColValueMappedToDiffInputParam(MAPPING_PROCESS.upload)) {
                            throw new MfError(["You have mapped more than one parameter with same column."]);
                        }
                        var aryMappedParams = _inputParamHelper.getMappedObjectsFromHtml(MAPPING_PROCESS.upload);
                       
                        if ($.isArray(aryMappedParams) &&
                            aryMappedParams.length>0) {
                            
                             _globalData.setUploadNewRecInputParams(aryMappedParams);
                              
                        } else {
                            
                            _globalData.setUploadNewRecInputParams([]);
                        }
                    },
                    closeInputMappingPopup:function(){
                       closeModalPopUp('divOfflineUploadCmdInpParamCont');
                    }
                }
            };
        })();
        
        
        function _doProcessByChkModifiedRecSameAsNewRec(cntrl/*jquery object*/){
            if(cntrl.prop("checked")=== true){
                _showHide.modifiedRecUploadCmdSettingsCont(false);
                _setValue.ddlModifiedRecUploadCmd("-1");
                updateUniformedControl(_getjQueryObj.ddlModifiedRecUploadCmd());
                _globalData.setModifiedRecSameAsNewRec("1");
                _globalData.deleteUploadModifiedRec();
            }
            else{
               _showHide.modifiedRecUploadCmdSettingsCont(true);
               _globalData.setModifiedRecSameAsNewRec("0");
            }
        }
        function _doProcessByChkInitialDownload(cntrl/*jquery object*/){
            if(cntrl.prop("checked")=== true){
                _showHide.divOfflineSyncDwnLdFieldsCont(true);
                _globalData.setIsInitialDownload("1");
            }
            else{
               _showHide.divOfflineSyncDwnLdFieldsCont(false);
               _globalData.setIsInitialDownload("0");
               _setValue.ddlSyncOnlnOfflnCommand("-1");
               _globalData.deleteDownloadData();
            }
        }
        function _doProcessForChkUploadDeleteRec(cntrl){
            if(cntrl.prop("checked")=== true){
                //the text of check box is not upload delete rec but do not delete rec
                //hensce 0 for check and 1 for uncheck
                _globalData.setDeleteRecordAfterUpload("0");
                _getjQueryObj.divUploadModifiedRecCont().show();
            }
            else{
               _globalData.setDeleteRecordAfterUpload("1");
               _getjQueryObj.divUploadModifiedRecCont().hide();
            }
        }
        function _setSyncDetailsInGlobalDataFromHtml(){
            var objSyncDetails =_globalData.getSyncDtls(),
                strTableType = _globalData.getTableType(),
                $chkInitialDownload = _getjQueryObj.chkInitialDownload(),
                $chkDeleteAfterUpload = _getjQueryObj.chkDeleteAfterUpload(),
                $chkModifiedRecSameAsNewRecord = _getjQueryObj.chkModifiedRecSameAsNewRecord(),
                $chkCleanRecBeforeDownload = _getjQueryObj.chkCleanRecBeforeDownload(),
                $chkDisallowDataAfterExpiry = _getjQueryObj.chkDisallowDataAfterExpiry(),
                $txtUploadRecordCriteria = _getjQueryObj.txtUploadRecordCriteria(),
                $txtShouldUploadWithin = _getjQueryObj.txtShouldUploadWithin(),
                strUploadWithin = "";
                
                
            if(!objSyncDetails){
                _globalData.setSyncDtls(new SyncDtls());
            }
            else{
                if(strTableType === OFFLINE_TABLE_TYPE.UploadData){
                    if($chkInitialDownload.prop("checked")=== true){
                        _globalData.setIsInitialDownload("1");
                        _globalData.setDownloadExpiry(getCacheDetailsForSaving());
                        _globalData.setDisallowDataUseAfterExpiry(
                            $chkDisallowDataAfterExpiry.prop("checked")===true?"1":"0");
                        _globalData.setCleanRecordBeforeDownload(
                                $chkCleanRecBeforeDownload.prop("checked")=== true ?"1":"0");
                    
                    }else{
                        _globalData.setIsInitialDownload("0");
                        _globalData.deleteDownloadData();
                    }
                    if($chkDeleteAfterUpload.prop("checked")=== true){
                        _globalData.setDeleteRecordAfterUpload("0");
                    }
                    else{
                        _globalData.setDeleteRecordAfterUpload("1");
                        if($chkModifiedRecSameAsNewRecord.prop("checked")=== true){
                            _globalData.setModifiedRecSameAsNewRec("1");
                        }
                        else{
                            _globalData.setModifiedRecSameAsNewRec("0");
                        }
                    }
                    _globalData.setUploadCriteria($.trim($txtUploadRecordCriteria.val()));
                    strUploadWithin = $txtShouldUploadWithin.val();
                    if(mfUtil.isNullOrUndefined(strUploadWithin) || mfUtil.isEmptyString(strUploadWithin)){
                        strUploadWithin = OFFLINE_CONSTANTS.DataUploadWithinDefault;
                    }
                    else{
                    
                        
                    }
                    _globalData.setUploadWithin(strUploadWithin);
                }
                else{
                    _globalData.setDownloadExpiry(getCacheDetailsForSaving());
                    _globalData.setDisallowDataUseAfterExpiry(
                        $chkDisallowDataAfterExpiry.prop("checked")===true?"1":"0");
                    _globalData.setCleanRecordBeforeDownload(
                            $chkCleanRecBeforeDownload.prop("checked")=== true ?"1":"0");
                }
            }
        }
        
        // function _doProcessShowModifiedRecUploadInputParam(isEdit){
            
        // }
        return{
            showHide :{
               initialDownloadCont :function(show/*boolean*/){
                   _showHide.initialDownloadCont(show);
               },
               chkInitialDownload :function(show/*boolean*/){
                   _showHide.chkInitialDownload(show);
               },
               deleteAfterUploadCont :function(show/*boolean*/){
                   _showHide.deleteAfterUploadCont(show);
               },
               chkDeleteAfterUpload :function(show/*boolean*/){
                   _showHide.chkDeleteAfterUpload(show);
               },
               modifiedRecUploadCmdCont:function(show/*boolean*/){
                   _showHide.modifiedRecUploadCmdCont(show);
                },
               modifiedRecUploadCmdSettingsCont:function(show/*boolean*/){
                     _showHide.modifiedRecUploadCmdSettingsCont(show);
                },
               divOfflineSyncUpLdFieldsCont: function(show/*boolean*/){
                   _showHide.divOfflineSyncUpLdFieldsCont(show);
               },
               divOfflineSyncDwnLdFieldsCont : function(show/*boolean*/){
                   _showHide.divOfflineSyncDwnLdFieldsCont(show);
               }
            },
            getjQueryObj :{
                initialDownloadCont:function(){
                    return _getjQueryObj.initialDownloadCont();
                },
                chkInitialDownload:function(){
                    return _getjQueryObj.chkInitialDownload();
                },
                deleteAfterUploadCont :function(){
                    return _getjQueryObj.deleteAfterUploadCont();
                },
                chkDeleteAfterUpload :function(show/*boolean*/){
                   return _getjQueryObj.chkDeleteAfterUpload();
                },
                modifiedRecUploadCmdCont:function(){
                   return _getjQueryObj.modifiedRecUploadCmdCont();
                },
                ddlModifiedRecUploadCmd:function(){
                    return _getjQueryObj.ddlModifiedRecUploadCmd();
                },
                modifiedRecUploadCmdSettingsCont:function(){
                    return _getjQueryObj.modifiedRecUploadCmdSettingsCont();
                },
                ddlSyncOnlnOfflnCommand:function(){
                   return _getjQueryObj.ddlSyncOnlnOfflnCommand();
               },
                ddlSyncOfflnOnlnCommand:function(){
                   return _getjQueryObj.ddlSyncOfflnOnlnCommand();
               },
                chkModifiedRecSameAsNewRecord:function(){
                   return _getjQueryObj.chkModifiedRecSameAsNewRecord();
                },
                divUploadModifiedRecCont:function(){
                   return _getjQueryObj.divUploadModifiedRecCont();
                },
                allCntrls : function(){
                    return _getjQueryObj.allCntrls();
                }
            },
            setInitialState:{
                ddlModifiedRecUploadCmd:function(){
                    _setInitialState.ddlModifiedRecUploadCmd();
                },
                ddlSyncOnlnOfflnCommand:function(){
                    _setInitialState.ddlSyncOnlnOfflnCommand();
                },
                ddlSyncOfflnOnlnCommand:function(){
                    _setInitialState.ddlSyncOfflnOnlnCommand();
                },
                chkInitialDownload:function(){
                    _setInitialState.chkInitialDownload();
                },
                chkModifiedRecSameAsNewRecord:function(){
                    _setInitialState.chkModifiedRecSameAsNewRecord();
                }
            },
            setInnerHtml :{
                ddlModifiedRecUploadCmd:function(html/*string*/){
                    _setInnerHtml.ddlModifiedRecUploadCmd(html);
                },
                ddlSyncOnlnOfflnCommand:function(html/*string*/){
                    _setInnerHtml.ddlSyncOnlnOfflnCommand(html);
                },
                ddlSyncOfflnOnlnCommand:function(html/*string*/){
                    _setInnerHtml.ddlSyncOfflnOnlnCommand(html);
                }
            },
            setValue :{
                ddlModifiedRecUploadCmd:function(value/*string*/){
                    _setValue.ddlModifiedRecUploadCmd(value);
                },
                ddlSyncOnlnOfflnCommand:function(value/*string*/){
                    _setValue.ddlSyncOnlnOfflnCommand(value);
                },
                ddlSyncOfflnOnlnCommand:function(value/*string*/){
                    _setValue.ddlSyncOfflnOnlnCommand(value);
                },
                chkInitialDownload :function(check/*boolean*/){
                    _setValue.chkInitialDownload(check);
                },
                chkModifiedRecSameAsNewRecord :function(check/*boolean*/){
                    _setValue.chkModifiedRecSameAsNewRecord(check);
                },
                ddlSyncCommandType:function(value){
                    _setValue.ddlSyncCommandType(value);
                },
                ddlSyncOnlineConn:function(value){
                    _setValue.ddlSyncOnlineConn(value);
                }
            },
            doProcessByChkModifiedRecSameAsNewRec:function(cntrl/*jquery object*/){
                _doProcessByChkModifiedRecSameAsNewRec(cntrl);
            },
            doProcessByChkInitialDownload:function(cntrl/*jquery object*/){
                _doProcessByChkInitialDownload(cntrl);
            },
            doProcessForChkUploadDeleteRec:function(cntrl){
                _doProcessForChkUploadDeleteRec(cntrl);
            },
            setSyncDetailsInGlobalDataFromHtml : function(){
                _setSyncDetailsInGlobalDataFromHtml();
            },
            paramMapping:{
                modifiedRec :{
                    showInputParamPopup :function(forEditView/*bool*/){
                       _paramMapping.modifiedRec.showInputParamPopup(forEditView);
                    },
                    isSameValueMappedToDiffInputParam : function(){
                        return _paramMapping.modifiedRec.isSameValueMappedToDiffInputParam();
                    },
                    getMappedObjectsFromHtml :function(){
                        return _paramMapping.modifiedRec.getMappedObjectsFromHtml();
                    },
                    saveMappingDtls : function(){
                        _paramMapping.modifiedRec.saveMappingDtls();
                    },
                    closeInputMappingPopup : function(){
                        _paramMapping.modifiedRec.closeInputMappingPopup();
                    }
                },
                download : {
                    showInputParamPopup :function(forEditView/*bool*/){
                       _paramMapping.download.showInputParamPopup(forEditView);
                    },
                    showOutputParamPopup : function(forEditView/*bool*/){
                       _paramMapping.download.showOutputParamPopup(forEditView);  
                    },
                    saveInputMappingDtls : function(){
                        _paramMapping.download.saveInputMappingDtls();
                    },
                    saveOutputMappingDtls : function(){
                        _paramMapping.download.saveOutputMappingDtls();
                    },
                    closeOutputMappingPopup:function(){
                       _paramMapping.download.closeOutputMappingPopup();
                    },
                    closeInputMappingPopup:function(){
                       _paramMapping.download.closeInputMappingPopup();
                    }
                },
                upload:{
                    showInputParamPopup :function(forEditView/*bool*/){
                       _paramMapping.upload.showInputParamPopup(forEditView);
                    },
                    saveInputMappingDtls : function(){
                        _paramMapping.upload.saveInputMappingDtls();
                    },
                    closeInputMappingPopup:function(){
                       _paramMapping.upload.closeInputMappingPopup();
                    }
                }
            }
        };
    })();
    
    var _editTableTab = (function(){
        var _getJqueryObj = {
            lblTblName : function (){return $('[id$="lblEditOfflineTblName"]');},
            lblOfflineTblType:function (){return $('[id$="lblEditOfflineTblType"]');},
            chkUserConfirmation :function (){return $(':checkbox[id$="chkEditOfflineUserConfirmation"]');},
            lblUploadSeq:function (){return $('[id$="lblEditUploadSeq"]');},
            lblBlockSize:function (){return $('[id$="lblEditBlockSize"]');},
            chkSyncOnLogin:function (){return $('#chkEditSyncLogin');},
            chkSyncOnAppStartup:function (){return $('#chkEditSyncAppStartup');},
            chkSyncOnAppClose:function (){return $('#chkEditSyncAppClose');},
            chkSyncManual:function (){return $('#chkEditSyncManual');},
            divEditUploadingSeqAndSizeInfoCont:function(){return $('#divEditUploadingSeqAndSizeInfoCont');},
            allControls:function(){
                return {
                    lblTblName :  this.lblTblName(),
                    lblOfflineTblType : this.lblOfflineTblType(),
                    chkUserConfirmation : this.chkUserConfirmation(),
                    lblUploadSeq : this.lblUploadSeq(),
                    lblBlockSize : this.lblBlockSize(),
                    chkSyncOnLogin : this.chkSyncOnLogin(),
                    chkSyncOnAppStartup : this.chkSyncOnAppStartup(),
                    chkSyncOnAppClose : this.chkSyncOnAppClose(),
                    chkSyncManual : this.chkSyncManual(),
                    divEditUploadingSeqAndSizeInfoCont:this.divEditUploadingSeqAndSizeInfoCont()
                }
            }
        };
        function _setSyncOnDataInHtml(value){
            var allTableTabControls = _getJqueryObj.allControls(),
                syncDetails = [],
                aryChkBoxesInOrder = [],
                i=0;
            
            syncDetails = value.split("");
            aryChkBoxesInOrder.push(allTableTabControls.chkSyncOnLogin);
            aryChkBoxesInOrder.push(allTableTabControls.chkSyncOnAppStartup);
            aryChkBoxesInOrder.push(allTableTabControls.chkSyncOnAppClose);
            aryChkBoxesInOrder.push(allTableTabControls.chkSyncManual);
            for(i=0;i<=3;i++){
                if(syncDetails[i] === "1"){
                   _util.checkUncheckUniformedCheckbox(
                        aryChkBoxesInOrder[i],
                        true
                    );
                    
                }
                else{
                   _util.checkUncheckUniformedCheckbox(
                        aryChkBoxesInOrder[i],
                        false
                    );
                }
            }
            
        }
        return{
            getJqueryObj:{
                allControls : function(){
                    return _getJqueryObj.allControls();
                }
            },
            setSyncOnDataInHtml:function(value){
                _setSyncOnDataInHtml(value);
            }
        };
    })();
    
    var _editColumnTab = (function(){
        function _addHTMLToClmDefinitionTbl(options/*object*/) {
            var offlineEditDTClmnDefinition = $("#OfflineDTClmnDefinitionEdit", _contextDivs.getEditOfflineTabCont());
            var htmlToAdd = "<tr>";
            var htmlToAddWithoutCross = "",
                index = options.index;
                
            htmlToAdd += "<td><span id=\"spanName" + index + "\">" + options.clnName + "</span></td>";
            htmlToAdd += "<td><span id=\"spanDataType" + index + "\">" + options.dataType + "</span></td>";
            htmlToAdd += "<td><span id=\"spanDefault" + index + "\">" + options.defVal + "</span></td>";
            htmlToAdd += "<td><span id=\"spanDecPlaces" + index + "\">" + options.decPlaces + "</span></td>";
            htmlToAddWithoutCross = htmlToAdd + "</tr>";
            
            $(offlineEditDTClmnDefinition).find("tbody tr.dynamicEditRow").before(htmlToAddWithoutCross);
        }
        return{
            formColumnsHtml : function(columns/*array of columns*/){
                $("#OfflineDTClmnDefinitionEdit tbody tr:not(\"tr.dynamicEditRow\")", _contextDivs.getEditOfflineTabCont()).remove();
                var aryClmDetls = columns;
                
                if (aryClmDetls && $.isArray(aryClmDetls)) {
                    $.each(aryClmDetls, function (index, value) {
                        var objClmDet = value;
                        var strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable,
                            strColumnTypeInString = "";
                        switch (objClmDet.fnGetDataType()) {
                            case Offline_Table_DataType.String:
                                strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable;
                                strColumnTypeInString = Offline_Table_DataType_STRING.String;
                                break;
                            case Offline_Table_DataType.Number:
                                strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable;
                                strColumnTypeInString = Offline_Table_DataType_STRING.Number;
                                break;
                            case Offline_Table_DataType.Decimal:
                                strDecimalPlaces = objClmDet.dpnt;
                                strColumnTypeInString = Offline_Table_DataType_STRING.Decimal;
                                break;
                            case Offline_Table_DataType.File:
                                strDecimalPlaces = OFFLINE_CONSTANTS.NotApplicable;
                                strColumnTypeInString = Offline_Table_DataType_STRING.File;
                            break;    
                        }
                        _addHTMLToClmDefinitionTbl({
                            index : index,
                            clnName:objClmDet.fnGetName(),
                            dataType:strColumnTypeInString,
                            defVal:objClmDet.fnGetDefaultValue(),
                            minVal:objClmDet.fnGetMinValue(),
                            maxVal:objClmDet.fnGetMaxValue(),
                            decPlaces:objClmDet.fnGetDecimalPlaces(),
                            primary:objClmDet.fnGetPrimaryKeyValue()==="1"?true:false
                        });
                    });
                }
            }
        };
    })();
    
    var _editSyncTab = (function(){
        var _getJqueryObj = {
            lblSyncCommandType : function (){return $('[id$="lblEditSyncCommandType"]');},
            lblSyncOnlineConn:function (){return $('[id$="lblEditSyncOnlineDBConn"]');},
            lblSyncDownCommand :function (){return $('[id$="lblEditSyncOnlnOfflnCommand"]');},
            lblSyncUpCommand:function (){return $('[id$="lblEditOfflnOnlnCommand"]');},
            chkEditInitialDownload: function (){return $('#chkEditInitialDownload');},
            divEditOfflineSyncDwnLdFieldsCont:function (){return $('#divEditOfflineSyncDwnLdFieldsCont');},
            chkEditUploadDeleteRec:function (){return $('#chkEditUploadDeleteRec');},
            divEditOfflineSyncUpLdFieldsCont:function (){return $('#divEditOfflineSyncUpLdFieldsCont');},
            divEditViewModifiedRecCont : function(){return $('#divEditViewModifiedRecCont');},
            chkEditModifiedRecSameAsUpload: function(){return $('#chkEditModifiedRecSameAsUpload');},
            lblEditModifiedRecUploadCommand:function(){return $('[id$="lblEditModifiedRecUploadCommand"]');},
            divEditModifiedRecUploadCmdSettingsCont:function(){return $('#divEditModifiedRecUploadCmdSettingsCont');},
            divEditInitialDownloadCont :function(){return $('#divEditInitialDownloadCont');},
            chkEditDisallowDataAfterExpiry:function(){return $('#chkEditDisallowDataAfterExpiry');},
            chkEditCleanRecBeforeDownload:function(){return $('#chkEditCleanRecBeforeDownload');},
            lblEditUploadRecordCriteria : function(){return $('[id$="lblEditUploadRecordCriteria"]');},
            lblEditUploadShouldBeWithin : function(){return $('[id$="lblEditUploadShouldBeWithin"]');},
            allControls:function(){
                return {
                    lblSyncCommandType :  this.lblSyncCommandType(),
                    lblSyncOnlineConn : this.lblSyncOnlineConn(),
                    lblSyncDownCommand : this.lblSyncDownCommand(),
                    lblSyncUpCommand : this.lblSyncUpCommand(),
                    chkEditInitialDownload:this.chkEditInitialDownload(),
                    divEditOfflineSyncDwnLdFieldsCont : this.divEditOfflineSyncDwnLdFieldsCont(),
                    chkEditUploadDeleteRec : this.chkEditUploadDeleteRec(),
                    divEditOfflineSyncUpLdFieldsCont: this.divEditOfflineSyncUpLdFieldsCont(),
                    divEditViewModifiedRecCont: this.divEditViewModifiedRecCont(),
                    chkEditModifiedRecSameAsUpload:this.chkEditModifiedRecSameAsUpload(),
                    lblEditModifiedRecUploadCommand : this.lblEditModifiedRecUploadCommand(),
                    divEditModifiedRecUploadCmdSettingsCont:this.divEditModifiedRecUploadCmdSettingsCont(),
                    divEditInitialDownloadCont : this.divEditInitialDownloadCont(),
                    chkEditDisallowDataAfterExpiry:this.chkEditDisallowDataAfterExpiry(),
                    chkEditCleanRecBeforeDownload:this.chkEditCleanRecBeforeDownload(),
                    lblEditUploadRecordCriteria : this.lblEditUploadRecordCriteria(),
                    lblEditUploadShouldBeWithin : this.lblEditUploadShouldBeWithin()
                }
            }
        };
        
        return{
            getJqueryObj:{
                allControls : function(){
                    return _getJqueryObj.allControls();
                }
            }
        };
    })();
    
    var _process = (function(){
        function _initialiseAllTabControls(){
            
            _columnTab.formColumnsHtml([]);
            
            var objTableTabControls = _tableTab.getjQueryObj.allCntrls(),
                objSyncTabControls = _syncTab.getjQueryObj.allCntrls();
            
                objTableTabControls.txtTblName.val("");
                _tableTab.setValue.ddlOfflineTblType(OFFLINE_TABLE_TYPE.UploadData.toString());
                _showHideCntrlsByTblTypeSelected();
                _tableTab.uncheckAllSyncOnTypeCheckboxes();
                _tableTab.setValue.chkUserConfirmation(false);
                _tableTab.setValue.ddlUploadSeq(OFFLINE_TBL_UPLOAD_SEQUENCE.LIFO);
                objTableTabControls.txtBlockSize.val("5");
                
                _syncTab.setValue.ddlSyncCommandType("-1");
                objSyncTabControls.ddlSyncOnlineConn.html('<option value="-1">Select</option>');
               _syncTab.setValue.ddlSyncOnlineConn("-1");
                objSyncTabControls.ddlSyncDownCommand.html('<option value="-1">Select</option>');
                _syncTab.setValue.ddlSyncOnlnOfflnCommand("-1");
                objSyncTabControls.ddlSyncUpCommand.html('<option value="-1">Select</option>');
                _syncTab.setValue.ddlSyncOfflnOnlnCommand("-1");
                
                _syncTab.showHide.initialDownloadCont(true);
                _syncTab.setInitialState.chkInitialDownload();
                _syncTab.showHide.divOfflineSyncUpLdFieldsCont(true);
                _syncTab.showHide.divOfflineSyncDwnLdFieldsCont(false);
                _syncTab.setInitialState.ddlSyncOnlnOfflnCommand();
                _syncTab.setInitialState.ddlModifiedRecUploadCmd();
                _syncTab.setValue.chkModifiedRecSameAsNewRecord(true);
               
                _syncTab.getjQueryObj.chkDeleteAfterUpload().prop('checked',false);
                updateUniformedControl(_syncTab.chkDeleteAfterUpload);
                _syncTab.getjQueryObj.divUploadModifiedRecCont().hide();
                _syncTab.doProcessByChkModifiedRecSameAsNewRec(
                        _syncTab.getjQueryObj.chkModifiedRecSameAsNewRecord());
            
                updateUniformedControl(_syncTab.getjQueryObj.chkModifiedRecSameAsNewRecord());
                objSyncTabControls.txtUploadRecordCriteria.val("");
                objSyncTabControls.txtShouldUploadWithin.val("1");
                //updateUniformedControl(objSyncTabControls.txtShouldUploadWithin);
                initializeDataCache();
        }
        function _showHideCntrlsByTblTypeSelected(){
            var ddlOfflineTblType = _tableTab.getjQueryObj.ddlOfflineTblType(),
                isInitialDownload = _globalData.getIsInitialDownload();
                
            if ($(ddlOfflineTblType).val() === "0") {
                
                _syncTab.showHide.initialDownloadCont(true);
                
                if(isInitialDownload === "0"){
                    _syncTab.showHide.divOfflineSyncDwnLdFieldsCont(false);
                }
                else{
                    _syncTab.showHide.divOfflineSyncDwnLdFieldsCont(true);
                }
                _tableTab.showHide.divUploadingSeqAndSizeInfoCont(true);
                _syncTab.showHide.divOfflineSyncUpLdFieldsCont(true);
                
            } else if ($(ddlOfflineTblType).val() === "1") {
                _syncTab.showHide.divOfflineSyncUpLdFieldsCont(false);
                _tableTab.showHide.divUploadingSeqAndSizeInfoCont(false);
                _syncTab.showHide.divOfflineSyncDwnLdFieldsCont(true);
                _syncTab.showHide.initialDownloadCont(false);
            
                
            } else if ($(ddlOfflineTblType).val() === "2") {
            }
        }
        function _showSelectedTabInUIForAddMode(){
            var $datatableTab = $('#divOfllineDatatableTab',
                _contextDivs.getOfflineTabCont()),
                
            selectedTab = _globalData.getTabSelected();
            
            if(selectedTab != null){
                $datatableTab.tabs({active:parseInt(selectedTab)});
            }
            else{
                $datatableTab.tabs({active:0});
            }
        }
        function _showSelectedTabInUIForViewMode(){
            var selectedTab = _globalData.getTabSelected(),
            $editDatatableTab = $('#divEditOfllineDatatableTab',
                _contextDivs.getOfflineEditViewTabCont());
            
            if(selectedTab != null){
                $editDatatableTab.tabs({active:parseInt(selectedTab)});
            }
            else{
                $editDatatableTab.tabs({active:0});
            }
        }
        function _showHideAddModeFormContDiv(show /*bool*/){
            var divOfflineTabCont = $('#divOfflineTabCont', _contextDivs.getPopupDiv());
            if (show) {
                $(divOfflineTabCont).show();
            } else {
                $(divOfflineTabCont).hide();
            }
        }
        function _showHideEditModeFormContDiv(show /*bool*/){
            var divOfflineEditTabCont = $('#divEditOfflineTabCont', _contextDivs.getPopupDiv());
            if (show) {
                $(divOfflineEditTabCont).show();
            } else {
                $(divOfflineEditTabCont).hide();
            }
        }
        function _validateForSaving() {
            var aryErrors =[],
                objSyncDetails = _globalData.getSyncDtls(),
                objOfflineTable = _globalData.getTable(),
                iParsedText = "",
                iNameLength = 50,
                aryTempErrors = [],
                aryMappedParams = [],
                isEdit = _globalData.getIsEdit(),
                blnAnySyncTypeSelected = false,
                objUpload = null,
                objCommandDtls = null,
                objDownload = null,
                strError="",
                isInitialDownload="0",
                syncTypes = [],
                strTemp="";
            
            if(isEdit === false){
                if (objOfflineTable.fnGetName() === ""){
                    aryTempErrors.push("Please enter a table name.");
                }
                else{
                    if (!OFFLINE_CONSTANTS.regxName.test(objOfflineTable.fnGetName())){
                        aryTempErrors.push("Please enter a valid table name.Only letters and numbers are allowed.");
                    }
                    else if (objOfflineTable.fnGetName().length > 50){
                        aryTempErrors.push("Please enter a valid table name.Length should be less than 50 characters.");
                    }
                }
            }
            syncTypes = objOfflineTable.fnGetSyncType().split("");
            if($.inArray("1",syncTypes)!== -1){
                blnAnySyncTypeSelected = true;
            }
            if(blnAnySyncTypeSelected === false){
                aryTempErrors.push("Please select a Sync on type.");
            }
            if (objOfflineTable.fnGetType() === "0"){
                if (objOfflineTable.fnGetTransferBlockSize() === ""){
                    aryTempErrors.push("Please enter a block size of record to upload.");
                }
                else{
                    strTemp = objOfflineTable.fnGetTransferBlockSize();
                    if(!$.isNumeric(strTemp)){
                        aryTempErrors.push("Please enter a valid block size.");
                    }
                    else if(!isStringValueInt(strTemp)){
                        aryTempErrors.push("Please enter a valid block size.");
                    }
                    else if(parseInt(strTemp) < 0){
                        aryTempErrors.push("Please enter a valid block size. Block size cannot be less than zero.");
                    }
                    else if(parseInt(strTemp) >25){
                        aryTempErrors.push("Please enter a valid block size. Block size cannot be more than 25.")
                    }
                }
            }
            if($.isArray(aryTempErrors) && aryTempErrors.length>0){
                strError ="";
                strError += "<b>Table</b>";
                strError += _getOfflineFormSaveErrorHtmlByTab(aryTempErrors);
                aryErrors.push(strError);
            }
            aryTempErrors = [];
            //SecondTab
            aryCols = objOfflineTable.fnGetColumns();
            if (!aryCols || !$.isArray(aryCols) || aryCols.length === 0){
                aryTempErrors.push("Please add columns to the data table.");
            }
            if($.isArray(aryTempErrors) && aryTempErrors.length>0){
                strError ="";
                strError += "<b>Columns</b>";
                strError += _getOfflineFormSaveErrorHtmlByTab(aryTempErrors);
                aryErrors.push(strError);
            }
            aryTempErrors = [];
            //Fourth tab
            if (!objSyncDetails.fnGetCmdType() || objSyncDetails.fnGetCmdType() === ""){
                aryTempErrors.push("Please select a object type.");
            }
            else if (!objSyncDetails.fnGetConnection()  || objSyncDetails.fnGetConnection() === ""){
                aryTempErrors.push("Please select a connection.");
            }
            else{
                if (objOfflineTable.fnGetType() === "0"){
                    isInitialDownload = _globalData.getIsInitialDownload();
                    if(isInitialDownload === "1"){
                        objDownload = objSyncDetails.fnGetDownload();
                        if(!objDownload){
                            aryTempErrors.push("Please select a object for download.");
                        }
                        else {
                            if(!objDownload.fnGetCommand()){
                                aryTempErrors.push("Please select a object for download.");
                            }
                            else{ 
                                if (_commandHasInputParameters(objDownload.fnGetCommand(),objSyncDetails.fnGetCmdType())){
                                    aryMappedParams = objDownload.fnGetInputParam();
                                    if (!$.isArray(aryMappedParams) || aryMappedParams.length === 0){
                                            aryTempErrors.push("Please map the download input parameters to columns.");
                                    }
                                }
                                if (_commandHasOutputParameters(objDownload.fnGetCommand(),objSyncDetails.fnGetCmdType())){
                                    aryMappedParams = objDownload.fnGetOutputParam();
                                    if (!$.isArray(aryMappedParams) || aryMappedParams.length === 0){
                                            aryTempErrors.push("Please map the download output parameters to columns.");
                                    }
                                }
                            }
                        }
                    }
                    objUpload = objSyncDetails.fnGetUpload();
                    if(!objUpload){
                        aryTempErrors.push("Please select a object for upload new record.");
                    }
                    else{
                        objCommandDtls = objUpload.fnGetNewRecord();
                        if (!objCommandDtls){
                           aryTempErrors.push("Please select a object for upload new record."); 
                        }
                        else{
                            if  (    _commandHasInputParameters(
                                        objCommandDtls.fnGetCommand(),
                                        objSyncDetails.fnGetCmdType()
                                    )
                                ){
                                    aryMappedParams = objCommandDtls.fnGetParams();
                                    if (!$.isArray(aryMappedParams) || aryMappedParams.length === 0){
                                        aryTempErrors.push("Please map the new record input parameters to columns.");
                                    }
                               
                            }
                        }
                        
                        if(objUpload.fnGetModifiedSameAsNewRecord() === "0"){
                            objCommandDtls = objUpload.fnGetModifiedRecord();
                            if (!objCommandDtls){
                                aryTempErrors.push("Please select a object for upload modified record."); 
                            }
                            else{
                                if (    _commandHasInputParameters(
                                            objCommandDtls.fnGetCommand(),
                                            objSyncDetails.fnGetCmdType()
                                        )
                                    ){
                                        aryMappedParams = objCommandDtls.fnGetParams();
                                        if (!$.isArray(aryMappedParams) || aryMappedParams.length === 0){
                                            aryTempErrors.push("Please map the modified record input parameters to columns.");
                                        }
                                   
                                }
                            }
                        }
                        strTemp = objUpload.fnGetUploadWithin();
                        if(!$.isNumeric(strTemp)){
                            aryTempErrors.push("Please enter valid Upload within value.");
                        }
                        else if(!isStringValueInt(strTemp)){
                            aryTempErrors.push("Please enter valid Upload within value.");
                        }
                        else if(parseInt(strTemp) < 0){
                            aryTempErrors.push("Please enter valid Upload within value.");
                        }
                    }
                }
                else if (objOfflineTable.fnGetType() === "1" ){
                    
                    objDownload = objSyncDetails.fnGetDownload();
                    if(!objDownload){
                        aryTempErrors.push("Please select a object for download.");
                    }
                    else {
                        if(!objDownload.fnGetCommand()){
                            aryTempErrors.push("Please select a object for download.");
                        }
                        else{ 
                            if (_commandHasInputParameters(objDownload.fnGetCommand(),objSyncDetails.fnGetCmdType())){
                                aryMappedParams = objDownload.fnGetInputParam();
                                if (!$.isArray(aryMappedParams) || aryMappedParams.length === 0){
                                        aryTempErrors.push("Please map the download input parameters to columns.");
                                }
                            }
                            if (_commandHasOutputParameters(objDownload.fnGetCommand(),objSyncDetails.fnGetCmdType())){
                                aryMappedParams = objDownload.fnGetOutputParam();
                                if (!$.isArray(aryMappedParams) || aryMappedParams.length === 0){
                                        aryTempErrors.push("Please map the download output parameters to columns.");
                                }
                            }
                        }
                    }
                }
                
            }
            if($.isArray(aryTempErrors) && aryTempErrors.length>0){
                strError ="";
                strError += "<b>Sync</b>";
                strError += _getOfflineFormSaveErrorHtmlByTab(aryTempErrors);
                aryErrors.push(strError);
            }
            aryTempErrors = [];
            return aryErrors;
        }
        function _setOfflineTableEditViewInUI(){
            
            var objTableTabControls = _editTableTab.getJqueryObj.allControls(),
                objSyncTabControls = _editSyncTab.getJqueryObj.allControls(),
                syncCmdType = IDE_COMMAND_OBJECT_TYPES.db,
                objDownloadCmd = null,
                objNewRecCmd = null,
                objModifiedRecCmd= null,
                objConnection = null,
                aryColumns = [],
                uploadCriteria = "";
           
                //TABLE TAB
               objTableTabControls.lblTblName.text(_globalData.getTableName());
               objTableTabControls.lblOfflineTblType.text(
                   mfOflnTblHelpers.getOflnTblTypeTextByValue(_globalData.getTableType())
               );
               
               _editTableTab.setSyncOnDataInHtml(_globalData.getTableSyncOn());
               if(_globalData.getConfirmationPrompt() === "1"){
                   _util.checkUncheckUniformedCheckbox(objTableTabControls.chkUserConfirmation,true);
               }
               else{
                  _util.checkUncheckUniformedCheckbox(objTableTabControls.chkUserConfirmation,false);
               }
               
               objTableTabControls.lblUploadSeq.text(
                   mfOflnTblHelpers.getOflnTblUploadSeqTextByValue(_globalData.getTableUploadSequence())
               );
               objTableTabControls.lblBlockSize.text(_globalData.getTransferBlockSize());
               
               //COLUMN TAB
               aryColumns = _globalData.getColumns();
               _editColumnTab.formColumnsHtml(aryColumns);
            
            
                //SYNC TAB
                syncCmdType = _globalData.getCommandType();
                
                
                
                objConnection = mfOflnTblHelpers.getConnectionObjByConnId(
                                    syncCmdType,
                                    _globalData.getConnection()
                                );
                
                
                objSyncTabControls.lblSyncCommandType.text(syncCmdType.UiName);
                if(objConnection){
                    objSyncTabControls.lblSyncOnlineConn.text(objConnection.ConnectionName);
                }
                else{
                    objSyncTabControls.lblSyncOnlineConn.text("");
                }
                if(_globalData.getTableType() === OFFLINE_TABLE_TYPE.UploadData){
                    objSyncTabControls.divEditOfflineSyncUpLdFieldsCont.show();
                    objTableTabControls.divEditUploadingSeqAndSizeInfoCont.show();
                    //TEMPORARY
                    $('#divBlockSizeEdit').hide();
                    if(_globalData.getIsInitialDownload() === "1"){
                        objSyncTabControls.divEditInitialDownloadCont.show();
                        objSyncTabControls.chkEditInitialDownload.prop('checked',true);
                        objSyncTabControls.divEditOfflineSyncDwnLdFieldsCont.show();
                        objDownloadCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                            syncCmdType,
                            _globalData.getDownloadCommand()
                        );
                        if(objDownloadCmd){
                            objSyncTabControls.lblSyncDownCommand.text(objDownloadCmd.commandName);
                        }
                        else{
                            objSyncTabControls.lblSyncDownCommand.text("");
                        }
                        $('span[id$="lblEditSyncDownloadCache"]').text(getCacheDetailsForEditView(_globalData.getDownloadExpiry()));
                        if(_globalData.getDisallowDataUseAfterExpiry() === "1"){
                            objSyncTabControls.chkEditDisallowDataAfterExpiry.prop('checked',true);
                        }
                        else{
                            objSyncTabControls.chkEditDisallowDataAfterExpiry.prop('checked',false);
                        }
                        if(_globalData.getCleanRecordBeforeDownload() === "1"){
                            objSyncTabControls.chkEditCleanRecBeforeDownload.prop('checked',true);
                        }
                        else{
                            objSyncTabControls.chkEditCleanRecBeforeDownload.prop('checked',false);
                        }
                    }
                    else{
                        
                        objSyncTabControls.chkEditInitialDownload.prop('checked',false);
                        objSyncTabControls.chkEditDisallowDataAfterExpiry.prop('checked',false);
                        objSyncTabControls.chkEditCleanRecBeforeDownload.prop('checked',false);
                        objSyncTabControls.divEditInitialDownloadCont.hide();
                        objSyncTabControls.divEditOfflineSyncDwnLdFieldsCont.hide();
                    }
                    updateUniformedControl(objSyncTabControls.chkEditInitialDownload);
                    if(_globalData.getDeleteRecordAfterUpload() === "0"){
                        objSyncTabControls.chkEditUploadDeleteRec.prop('checked',true);
                        objSyncTabControls.divEditViewModifiedRecCont.show();
                        if(_globalData.getModifiedRecSameAsNewRec() === "0"){
                            objSyncTabControls.chkEditModifiedRecSameAsUpload.prop('checked',false);
                            objSyncTabControls.divEditModifiedRecUploadCmdSettingsCont.show();
                        }
                        else{
                           objSyncTabControls.chkEditModifiedRecSameAsUpload.prop('checked',true); 
                           objSyncTabControls.divEditModifiedRecUploadCmdSettingsCont.hide();
                        }
                        updateUniformedControl(objSyncTabControls.chkEditModifiedRecSameAsUpload);
                        objModifiedRecCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                            syncCmdType,
                            _globalData.getUploadModifiedRecCmd()
                        );
                        if(objModifiedRecCmd){
                            objSyncTabControls.lblEditModifiedRecUploadCommand.text(objModifiedRecCmd.commandName);
                        }
                    }
                    else if(_globalData.getDeleteRecordAfterUpload() === "1"){
                        objSyncTabControls.chkEditUploadDeleteRec.prop('checked',false);
                        objSyncTabControls.divEditViewModifiedRecCont.hide();
                    }
                    updateUniformedControl(objSyncTabControls.chkEditUploadDeleteRec);
                    objNewRecCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                        syncCmdType,
                        _globalData.getUploadNewRecCmd()
                    );
                    if(objNewRecCmd){
                        objSyncTabControls.lblSyncUpCommand.text(objNewRecCmd.commandName);
                    }
                    
                    uploadCriteria = _globalData.getUploadCriteria();
                    if(!mfUtil.isNullOrUndefined(uploadCriteria) &&  !mfUtil.isEmptyString(uploadCriteria)){
                    }
                    else{
                        uploadCriteria = "None";
                    }
                    objSyncTabControls.lblEditUploadRecordCriteria.text(uploadCriteria);
                    objSyncTabControls.lblEditUploadShouldBeWithin.text(_globalData.getUploadWithin() + (_globalData.getUploadWithin()==="1"?" day" : " days"));
                }
                else{
                    objSyncTabControls.chkEditInitialDownload.prop('checked',false);
                    objDownloadCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                        syncCmdType,
                        _globalData.getDownloadCommand()
                    );
                    if(objDownloadCmd){
                        objSyncTabControls.lblSyncDownCommand.text(objDownloadCmd.commandName);
                    }
                    else{
                        objSyncTabControls.lblSyncDownCommand.text("");
                    }
                    if(_globalData.getDisallowDataUseAfterExpiry() === "1"){
                        objSyncTabControls.chkEditDisallowDataAfterExpiry.prop('checked',true);
                    }
                    else{
                        objSyncTabControls.chkEditDisallowDataAfterExpiry.prop('checked',false);
                    }
                    if(_globalData.getCleanRecordBeforeDownload() === "1"){
                        objSyncTabControls.chkEditCleanRecBeforeDownload.prop('checked',true);
                    }
                    else{
                        objSyncTabControls.chkEditCleanRecBeforeDownload.prop('checked',false);
                    }
                    $('span[id$="lblEditSyncDownloadCache"]').text(getCacheDetailsForEditView(_globalData.getDownloadExpiry()));
                    objTableTabControls.divEditUploadingSeqAndSizeInfoCont.hide();
                    objSyncTabControls.divEditOfflineSyncUpLdFieldsCont.hide();
                    objSyncTabControls.divEditOfflineSyncDwnLdFieldsCont.show();
                    objSyncTabControls.divEditInitialDownloadCont.hide();
                }
                updateUniformedControl(objSyncTabControls.chkEditDisallowDataAfterExpiry);
                updateUniformedControl(objSyncTabControls.chkEditCleanRecBeforeDownload);
        }
        function _setOfflineTableEditFormInUI(){
            var objTableTabControls = _tableTab.getjQueryObj.allCntrls(),
                objSyncTabControls = _syncTab.getjQueryObj.allCntrls(),
                syncCmdType = IDE_COMMAND_OBJECT_TYPES.db,
                objDownloadCmd = null,
                objUploadCmd = null,
                objConnection = null,
                aryColumns = [],
                strTableType = "",
                strConnection="";
                
            
                objTableTabControls.txtTblName.val(_globalData.getTableName());
                objTableTabControls.lblTblName.text(_globalData.getTableName());
                
                strTableType = _globalData.getTableType();
                _util.setUniformedDropDownValue(
                    objTableTabControls.ddlOfflineTblType,
                    strTableType);
                
                _tableTab.setSyncOnDataInHtml(_globalData.getTableSyncOn());
                
                if(_globalData.getConfirmationPrompt() === "1"){
                    _util.checkUncheckUniformedCheckbox(objTableTabControls.chkUserConfirmation,true);
                }
                else{
                    _util.checkUncheckUniformedCheckbox(objTableTabControls.chkUserConfirmation,false);
                }
                if(strTableType === OFFLINE_TABLE_TYPE.UploadData){
                    
                    _util.setUniformedDropDownValue(
                        objTableTabControls.ddlUploadSeq,
                        _globalData.getTableUploadSequence());
                    
                    objTableTabControls.txtBlockSize.val(_globalData.getTransferBlockSize());    
                }
                
                aryColumns = _globalData.getColumns();
                _columnTab.formColumnsHtml(aryColumns);
            
            
                _showHideCntrlsByTblTypeSelected();
                syncCmdType = _globalData.getCommandType();
                
                _util.setUniformedDropDownValue(objSyncTabControls.ddlSyncCommandType,
                    syncCmdType.id);
                
                var strDdlOptions = "";
                objConnection = mfOflnTblHelpers.getConnectionObjByConnId(
                    syncCmdType,
                    _globalData.getConnection()
                );
                
                    
                strDdlOptions = mfOflnTblHelpers.getConnsAsOptionsOfDdl(syncCmdType,
                    true,"-1");
                
                objSyncTabControls.ddlSyncOnlineConn.html('');
                
                objSyncTabControls.ddlSyncOnlineConn.html(strDdlOptions);
                
                if(objConnection){
                    strConnection = objConnection.ConnectorId;
                    _util.setUniformedDropDownValue(objSyncTabControls.ddlSyncOnlineConn,strConnection);
                    switch(syncCmdType){
                        case IDE_COMMAND_OBJECT_TYPES.db:
                            strSelectCmds = 
                                mfOflnTblHelpers.
                                getSelectCmdsByConnsAsOptionsForDdl(
                                        strConnection,true,"-1"
                                    );
                            strInsertCmds  =
                                mfOflnTblHelpers.
                                getInsertCmdsByConnAsOptionsForDdl(
                                    strConnection,true,"-1"
                                );
                            strUpdateCmds = 
                                mfOflnTblHelpers.
                                getUpdateCmdsByConnAsOptionsForDdl(
                                    strConnection,false,""
                                );
                            strDownCmds = strSelectCmds; 
                            strUpCmds = strInsertCmds+strUpdateCmds;
                            break;
                        case    IDE_COMMAND_OBJECT_TYPES.wsdl:
                        case    IDE_COMMAND_OBJECT_TYPES.http:
                        case    IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                                strUpCmds = 
                                    mfOflnTblHelpers.
                                    getWsCmdsByConnAsOptionsForDdl(
                                        strConnection,syncCmdType,true,"-1"
                                    );
                                strDownCmds = strUpCmds;    
                            break;
                        case IDE_COMMAND_OBJECT_TYPES.odata:
                            strUpCmds= 
                                mfOflnTblHelpers.
                                getOdataCmdsByConnAsOptionsForDdl(
                                    strConnection,true,"-1"
                                );
                            strDownCmds = strUpCmds; 
                            break;
                    }
                    objSyncTabControls.ddlSyncDownCommand.html('');
                    objSyncTabControls.ddlSyncUpCommand.html('');
                    objSyncTabControls.ddlModifiedRecUploadCmd.html('');
                    objSyncTabControls.ddlSyncDownCommand.html(strDownCmds);
                    objSyncTabControls.ddlSyncUpCommand.html(strUpCmds);
                    objSyncTabControls.ddlModifiedRecUploadCmd.html(strUpCmds);
                }    
                
                if(_globalData.getTableType() === OFFLINE_TABLE_TYPE.UploadData){
                    if(_globalData.getIsInitialDownload() === "1"){
                        objSyncTabControls.chkInitialDownload.prop('checked',true);
                        objDownloadCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                            syncCmdType,
                            _globalData.getDownloadCommand()
                        );
                        if(objDownloadCmd){
                            objSyncTabControls.ddlSyncDownCommand.val(objDownloadCmd.commandId);
                        }
                        else{
                            objSyncTabControls.ddlSyncDownCommand.val("-1");
                        }
                        setEditCacheValInUI(_globalData.getDownloadExpiry());
                        if(_globalData.getDisallowDataUseAfterExpiry() === "1"){
                            objSyncTabControls.chkDisallowDataAfterExpiry.prop('checked',true);
                        }
                        else{
                            objSyncTabControls.chkDisallowDataAfterExpiry.prop('checked',false);
                        }
                        if(_globalData.getCleanRecordBeforeDownload() === "1"){
                            objSyncTabControls.chkCleanRecBeforeDownload.prop('checked',true);
                        }
                        else{
                            objSyncTabControls.chkCleanRecBeforeDownload.prop('checked',false);
                        }
                    }
                    else{
                        objSyncTabControls.chkInitialDownload.prop('checked',false);
                        objSyncTabControls.chkDisallowDataAfterExpiry.prop('checked',false);
                        objSyncTabControls.chkCleanRecBeforeDownload.prop('checked',false);
                    }
                    updateUniformedControl(objSyncTabControls.ddlSyncDownCommand);
                    updateUniformedControl(objSyncTabControls.chkInitialDownload);
                    if(_globalData.getDeleteRecordAfterUpload() === "0"){
                        objSyncTabControls.chkDeleteAfterUpload.prop('checked',true);
                        objSyncTabControls.divUploadModifiedRecCont.show();
                        if(_globalData.getModifiedRecSameAsNewRec() === "1"){
                            objSyncTabControls.chkModifiedRecSameAsNewRecord.prop('checked',true);
                            objSyncTabControls.modifiedRecUploadCmdSettingsCont.hide();
                        }
                        else{
                           objSyncTabControls.chkModifiedRecSameAsNewRecord.prop('checked',false); 
                           objSyncTabControls.modifiedRecUploadCmdSettingsCont.show();
                        }
                        updateUniformedControl(objSyncTabControls.chkModifiedRecSameAsNewRecord);
                        objModifiedRecCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                            syncCmdType,
                            _globalData.getUploadModifiedRecCmd()
                        );
                        if(objModifiedRecCmd){
                            objSyncTabControls.ddlModifiedRecUploadCmd.val(objModifiedRecCmd.commandId);
                        }
                    }
                    else if(_globalData.getDeleteRecordAfterUpload() === "1"){
                        objSyncTabControls.chkDeleteAfterUpload.prop('checked',false);
                        objSyncTabControls.divUploadModifiedRecCont.hide();
                    }
                    updateUniformedControl(objSyncTabControls.chkDeleteAfterUpload);
                    updateUniformedControl(objSyncTabControls.ddlModifiedRecUploadCmd);
                    updateUniformedControl(objSyncTabControls.chkModifiedRecSameAsNewRecord);
                    objNewRecCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                        syncCmdType,
                        _globalData.getUploadNewRecCmd()
                    );
                    if(objNewRecCmd){
                        objSyncTabControls.ddlSyncUpCommand.val(objNewRecCmd.commandId);
                    }
                    updateUniformedControl(objSyncTabControls.ddlSyncUpCommand);
                    objSyncTabControls.txtUploadRecordCriteria.val(_globalData.getUploadCriteria());
                    objSyncTabControls.txtShouldUploadWithin.val(_globalData.getUploadWithin());
                    //updateUniformedControl(objSyncTabControls.ddlShouldUploadWithin);
                }
                else{
                    objSyncTabControls.chkInitialDownload.prop('checked',false);
                    updateUniformedControl(objSyncTabControls.chkInitialDownload);
                    objDownloadCmd = mfOflnTblHelpers.getCommandObjByCmdId(
                        syncCmdType,
                        _globalData.getDownloadCommand()
                    );
                    if(objDownloadCmd){
                        objSyncTabControls.ddlSyncDownCommand.val(objDownloadCmd.commandId);
                        setEditCacheValInUI(_globalData.getDownloadExpiry());
                    }
                    else{
                        objSyncTabControls.ddlSyncDownCommand.val("-1");
                    }
                    if(_globalData.getDisallowDataUseAfterExpiry() === "1"){
                        objSyncTabControls.chkDisallowDataAfterExpiry.prop('checked',true);
                    }
                    else{
                        objSyncTabControls.chkDisallowDataAfterExpiry.prop('checked',false);
                    }
                    if(_globalData.getCleanRecordBeforeDownload() === "1"){
                        objSyncTabControls.chkCleanRecBeforeDownload.prop('checked',true);
                    }
                    else{
                        objSyncTabControls.chkCleanRecBeforeDownload.prop('checked',false);
                    }
                    updateUniformedControl(objSyncTabControls.ddlSyncDownCommand);
                }
                updateUniformedControl(objSyncTabControls.chkDisallowDataAfterExpiry);
                updateUniformedControl(objSyncTabControls.chkCleanRecBeforeDownload);
                
            }
       
        function _setTableGlobalDataForEdit(rowOfflnTblData){
            var strTblContJson ="",
            objTblContJson=null,
            objTbl=null,
            strCloneTbl = "",
            cloneTbl=null;
            if(rowOfflnTblData){
                strTblContJson = rowOfflnTblData.TableJson;
                if(strTblContJson){
                    objTblContJson = new OfflineTableCont(_util.deserializeJson(strTblContJson));
                    objTblContJson.fnResetObjectDetails();
                    objTbl = objTblContJson.fnGetTable();
                    strCloneTbl = _util.serializeObjectToJson(objTbl);
                    cloneTbl =  new OfflineDatatable(_util.deserializeJson(strCloneTbl));
                    cloneTbl.fnResetObjectDetails();
                }
            }
            _globalData.setTable(objTbl);
            _globalData.setDbSavedTable(cloneTbl);
        }
        function _setSyncGlobalDataForEdit(rowOfflnTblData){
            var strSyncContJson = "",
            objSyncContJson= null,
            objSync = null,
            strCloneSyncDtl="",
            objCloneSyncDtl =null;
            
            if(rowOfflnTblData){
                strSyncContJson = rowOfflnTblData.SyncJson;
                if(strSyncContJson){
                    objSyncContJson = new SyncDetailsCont(_util.deserializeJson(strSyncContJson));
                    objSyncContJson.fnResetObjectDetails();
                    objSync = objSyncContJson.fnGetSync();
                    
                    strCloneSyncDtl =_util.serializeObjectToJson(objSync);
                    objCloneSyncDtl = new SyncDtls(_util.deserializeJson(strCloneSyncDtl));
                    objCloneSyncDtl.fnResetObjectDetails();
                }
            }
            _globalData.setSyncDtls(objSync);
            _globalData.setDbSavedSyncDetails(objCloneSyncDtl);
        }
       
        return {
            initialiseAllTabControls: function(){
                _initialiseAllTabControls();
            },
            addNewTableInUI : function(){
               var tableId = _globalData.getTableId();
               if(tableId){
                 removeHighlightOfAllRows(tableId);  
               }
                _initialiseAllTabControls();
                _globalData.setData(_globalData.getDefaultData());
                _actionBtns.showHideForAddTbl();
                initializeDataCache();
                _globalData.setFormMode(OFFLINE_FORM_DISPLAY_MODE.Add);
                _showSelectedTabInUIForAddMode();
                _showSelectedTabInUIForViewMode();
                _showHideAddModeFormContDiv(true);
                _showHideEditModeFormContDiv(false);
            },
            showHideCntrlsByTblTypeSelected: function () {
                _showHideCntrlsByTblTypeSelected();
            },
            showSelectedTabInUIForAddMode : function(){
                _showSelectedTabInUIForAddMode();
            },
            showHideAddModeFormContDiv: function(show/*bool*/){
                _showHideAddModeFormContDiv(show);
            },
            showHideEditModeFormContDiv: function(show/*bool*/){
                _showHideEditModeFormContDiv(show);
            },
            validateForSaving : function(){
               return _validateForSaving();
            },
            setOfflineTableEditViewInUI: function(){
                _setOfflineTableEditViewInUI();
            },
            setTableGlobalDataForEdit:function(rowOfflnTblData/*data stored in row*/){
                _setTableGlobalDataForEdit(rowOfflnTblData);
            },
            setSyncGlobalDataForEdit:function(rowOfflnTblData){
                _setSyncGlobalDataForEdit(rowOfflnTblData);
            },
            setOfflineTableEditFormInUI:function(){
                _setOfflineTableEditFormInUI();
            },
            editOfflineTable : function(rowOfflnTblData){
                //var objTable=null,objSyncDetails = null;
                 _globalData.setData(_globalData.getDefaultData());
                 _initialiseAllTabControls();
                 _setTableGlobalDataForEdit(rowOfflnTblData);
                _setSyncGlobalDataForEdit(rowOfflnTblData);
                 _globalData.setIsEdit(true);
                 _setOfflineTableEditViewInUI();
                 //this.showHideContentByTblTypeSelected();
                 _showHideEditModeFormContDiv(true);
                 _showHideAddModeFormContDiv(false);
                 _globalData.setFormMode(OFFLINE_FORM_DISPLAY_MODE.EditView);
            },
            doAfterSaveAndOpenNewTblSuccess : function(){
                var $hidOldNewTblDtlForSaveAndOpenNew = _hdf.hidOldNewTblDtlForSaveAndOpenNew(),
                    $hidTblDataToOpenAfterSaveAndOpenNew = _hdf.hidTblDataToOpenAfterSaveAndOpenNew(),
                    objDataForOpeningNew = $.parseJSON($hidOldNewTblDtlForSaveAndOpenNew.val());
                
                highlightDatatableRowByOflnTblId(objDataForOpeningNew.newTableId);
                
                this.editOfflineTable($.parseJSON($hidTblDataToOpenAfterSaveAndOpenNew.val()));
                
                $hidOldNewTblDtlForSaveAndOpenNew.val('');
                
                $hidTblDataToOpenAfterSaveAndOpenNew.val('');
            },
            doAfterSaveAndOpenNewTblOnUpdateFailure : function(){
                var $hidOldNewTblDtlForSaveAndOpenNew = _hdf.hidOldNewTblDtlForSaveAndOpenNew(),
                    $hidTblDataToOpenAfterSaveAndOpenNew = _hdf.hidTblDataToOpenAfterSaveAndOpenNew(),
                    objDataForOpeningNew = $.parseJSON($hidOldNewTblDtlForSaveAndOpenNew.val());
                
                highlightDatatableRowByOflnTblId(objDataForOpeningNew.oldTableId);
                
                $hidOldNewTblDtlForSaveAndOpenNew.val('');
                
                $hidTblDataToOpenAfterSaveAndOpenNew.val('');
            },
            doAfterSaveAndOpenNewTblOnNewTblFetchFailure : function(){
                var $hidOldNewTblDtlForSaveAndOpenNew = _hdf.hidOldNewTblDtlForSaveAndOpenNew(),
                    $hidTblDataToOpenAfterSaveAndOpenNew = _hdf.hidTblDataToOpenAfterSaveAndOpenNew();
                
                this.addNewTableInUI();
                
                $hidOldNewTblDtlForSaveAndOpenNew.val('');
                
                $hidTblDataToOpenAfterSaveAndOpenNew.val('');
            }
        };
    })();
    
    var _actionBtns = (function(){
        var _getjQueryObj = {
            btnSave : function(){
                return $('#btnSaveOfflineTblSettings1');
            },
            btnCancel : function(){
                return $('#btnCancelOfflineTblSettings');
            },
            btnEditCancel : function(){
                return $('#btnEditCancelOfflineTblSettings');
            },
            btnDelete : function(){
                return $('#btnDeleteOfflineTblSettings');
            }
        };
        var _showHide = {
            btnSave : function(show){
                if(show === true){
                    _getjQueryObj.btnSave().show();
                }
                else{
                    _getjQueryObj.btnSave().hide();
                }
            },
            btnCancel : function(show){
                if(show === true){
                    _getjQueryObj.btnCancel().show();
                }
                else{
                    _getjQueryObj.btnCancel().hide();
                }
            },
            btnEditCancel : function(show){
               if(show === true){
                   _getjQueryObj.btnEditCancel().show();
               }
               else{
                   _getjQueryObj.btnEditCancel().hide();
               }
            },
            btnDelete : function(show){
                if(show === true){
                    _getjQueryObj.btnDelete().show();
                }
                else{
                    _getjQueryObj.btnDelete().hide();
                }
            }
        }
        return {
            getjQueryObj :{
                btnSave : function(){
                  return _getjQueryObj.btnSave();
                },
                btnCancel : function(){
                  return _getjQueryObj.btnCancel();
                },
                btnEditCancel : function(){
                  return _getjQueryObj.btnEditCancel();
                },
                btnDelete : function(){
                  return _getjQueryObj.btnDelete();
                }
            },
            showHide :{
                btnSave : function(show){
                    _showHide.btnSave(show);
                },
                btnCancel : function(show){
                    _showHide.btnCancel(show);
                },
                btnEditCancel : function(show){
                   _showHide.btnEditCancel(show);
                },
                btnDelete : function(show){
                    _showHide.btnDelete(show);
                }
            },
            showHideForEditTbl : function(){
                _showHide.btnSave(true);
                _showHide.btnCancel(false);
                _showHide.btnEditCancel(true);
                _showHide.btnDelete(true);
            },
            showHideForAddTbl : function(){
                _showHide.btnSave(true);
                _showHide.btnCancel(true);
                _showHide.btnEditCancel(false);
                _showHide.btnDelete(false);
            }
        };
    })();
    
    var _editDiscardConfirmationProcs = (function(){
        var _get$Obj = {
            divOflnTblEditDiscardCont : function(){
                return $('#divOflnTblEditDiscardCont');
            }
        };
        var _globalData = {
            setData : function(data){
                _get$Obj.divOflnTblEditDiscardCont().data(OFFLINE_CONSTANTS.jqueryCommonDatakey,data);
            },
            getData : function(){
               return  _get$Obj.divOflnTblEditDiscardCont().data(OFFLINE_CONSTANTS.jqueryCommonDatakey);
            },
            getOldTableId : function(){
                return this.getData().oldTableId;
            },
            getNewTableId : function(){
                return this.getData().newTableId;
            },
            getNewTableData : function(){
                return this.getData().newTableData;
            },
            removeData : function(){
                _get$Obj.divOflnTblEditDiscardCont().removeData(OFFLINE_CONSTANTS.jqueryCommonDatakey);
            }
        };
        function _processSaveAndOpenNewTbl(){
            _tableTab.setOfflineTableInGlobalData();
            _syncTab.setSyncDetailsInGlobalDataFromHtml();
            var aryErrors = _process.validateForSaving();
            
            if ($.isArray(aryErrors) && aryErrors.length>0){
                highlightDatatableRowByOflnTblId(_globalData.getOldTableId());
                closeModalPopUp('divOflnTblEditDiscardCont');
                throw new MfError(aryErrors);
            }
            else{
                _setFinalOfflineCmpltJsonForSavingInHid();
                _setFinalSyncJsonForSavingInHid();
                _hdf.hidOldNewTblDtlForSaveAndOpenNew().val(JSON.stringify(_globalData.getData()));
                _globalData.removeData();
                closeModalPopUp('divOflnTblEditDiscardCont');
                processPostbackByHtmlCntrl.postBack(
                    postBackByHtmlProcess.SaveAndOpenNewTbl,
                    [],"");
            }
        }
        function _processSaveAndOpenSameTbl(){
            _tableTab.setOfflineTableInGlobalData();
            _tableTab.setOfflineTableInGlobalData();
            _syncTab.setSyncDetailsInGlobalDataFromHtml();
            var aryErrors = _process.validateForSaving();
            
            if ($.isArray(aryErrors) && aryErrors.length>0){
                highlightDatatableRowByOflnTblId(_globalData.getOldTableId());
                closeModalPopUp('divOflnTblEditDiscardCont');
                throw new MfError(aryErrors);
            }
            else{
                _setFinalOfflineCmpltJsonForSavingInHid();
                _setFinalSyncJsonForSavingInHid();
                _hdf.hidOldNewTblDtlForSaveAndOpenNew().val(JSON.stringify(_globalData.getData()));
                _globalData.removeData();
                closeModalPopUp('divOflnTblEditDiscardCont');
                processPostbackByHtmlCntrl.postBack(
                    postBackByHtmlProcess.SaveAndOpenSameTbl,
                    [],"");
            }
        }
        return {
            discard : function(discard /*boolean*/){
                if(discard === true){
                    _process.editOfflineTable(_globalData.getNewTableData());
                    _globalData.removeData();
                    closeModalPopUp('divOflnTblEditDiscardCont');
                }
                else{
                    if(_globalData.getOldTableId() === _globalData.getNewTableId()){
                        _processSaveAndOpenSameTbl();
                    }
                    else{
                        _processSaveAndOpenNewTbl();
                    }
                }
            },
            cancel : function(){
                highlightDatatableRowByOflnTblId(_globalData.getOldTableId());
                _globalData.removeData();
                closeModalPopUp('divOflnTblEditDiscardCont');
            },
            globalData : {
                setData : function(data){
                    _globalData.setData(data);
                },
                removeData : function(){
                    _globalData.removeData();
                }
            }
        };
    })();
    
    //END EDIT OFFLINE TABLE HEPERS
    
    return OFDB = {
        showHidePageByPageModeInHid: function () {
        },
        showHideControlsOfFormsByProcessType: function (formDisplayMode/*OFFLINE_FORM_DISPLAY_MODE*/) {
            switch (formDisplayMode) {
                case OFFLINE_FORM_DISPLAY_MODE.Add:
                    _showHideOfflineTblNameLabel(false);
                    _showHideOfflineTblNameTextbox(true);
                    _showOfflineActionButtonForNewTbl();
                    break;
                case OFFLINE_FORM_DISPLAY_MODE.Edit:
                    _showHideOfflineTblNameLabel(true);
                    _showHideOfflineTblNameTextbox(false);
                    this.showOfflineActionButtonForEdit();
                    break;
            }
        },
        showHideOfflineDBTblFormTab:function(blnSHow){
            _showHideOfflineDBTblFormTab(blnSHow);
        },
        showHideOfflineDBTblFormViewTab:function(blnShow){
            _showHideOfflineDBTblFormViewTab(blnShow);
        },
        setDecimalPlacesInputAndDefaultVal: function (sender) {
            var offlineDTClmnDefinition = $("#OfflineDTClmnDefinition", _contextDivs.getOfflineTabCont());
            var parentTdOfSener = $(sender).parent();
            var parentTrOfTd = $(parentTdOfSener).parent();
            var decPlaces = $(parentTrOfTd).find("select[name='DecimalPlaces']"),
                spanNotApplicable = $(parentTrOfTd).find("span[id='spanNotApplicable']"),
                defaultVal = $(parentTrOfTd).find("input[name='DefaultVal']");
            var maxVal = $(parentTrOfTd).find("input[name='MaxValue']");
            var minVal = $(parentTrOfTd).find("input[name='MinValue']");
            if ($(sender).val() === Offline_Table_DataType.Decimal) {
                $(decPlaces).show();
                $(decPlaces).val("2");
                $(spanNotApplicable).hide();
                $(defaultVal).val("0.00");
                $(maxVal).val("0");
                // $(minVal).val("–2147483648");
            } else {
                if ($(sender).val() === Offline_Table_DataType.Number) {
                    $(defaultVal).val("0");
                    $(maxVal).val("0");
                    //$(minVal).val("–2147483648");
                } else if ($(sender).val() === Offline_Table_DataType.String) {
                    $(defaultVal).val("");
                    $(maxVal).val("100");
                }
                $(decPlaces).hide();
                $(spanNotApplicable).show();
            }
        },
                
        offlineTblFormClose: function (divIdToClose /*string*/) {

            this.setOfflineFormTabInHid(OFFLINE_TABLE_FORM_TAB.TabTableContent);
            this.setOfflineViewPageMode(OFFLINE_PAGE_DISPLAY_MODE.RPtDetails);
            this.clearOfflineTblFormControls();
            this.showHidePageByPageModeInHid();
            this.showSelectedTabForOfflineTblForm();
            closeModalPopUp(divIdToClose);
        },
        
        makeTabAfterPostBack: function () {
            $('.divOfflinedatables').find('div.OfflineDT_Tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        },
        
        makeOfflineInputUniform: function () {
            $('.divOfflinedatables').find("select,input, input[type=file]").not('select[multiple],.notUniform').offlineuniform();
        },
                
        doProcessOfflineTblEditCancel: function () {
            MFODB.setIsResetReqOnEditClose(true);
            closeModalPopUp('divOfflineTabCont');
            showModalPopUp("divEditOfflineTabCont", "Offline Table Details", 900, false);
            return false;
        },
        doProcessAfterSaveOrDelete: function () {
            this.setIsResetReqOnEditClose(false);
            this.setOfflineViewPageMode(OFFLINE_PAGE_DISPLAY_MODE.RPtDetails);
            this.setInitialStateOfFormControls();
            this.setOfflineFormTabInHid(OFFLINE_TABLE_FORM_TAB.TabTableContent);
            closeModalPopUp('divOfflineTabCont');
            closeModalPopUp('divEditOfflineTabCont');
            //setting the default
            this.setIsResetReqOnEditClose(true);
        },
        
        setIsResetReqOnEditClose: function (isResetReq /*bool*/) {
            isResetReqOnEditClose = isResetReq;
        },
                
        setSyncTabCommandAndConnDtlJsonForSaving: function () {
            var hidOfflineSyncCmdAndConJsonForSaving = $('input[type="hidden"][id$="hidOfflineSyncCmdAndConJsonForSaving"]', _contextDivs.getOfflineHidFields());
            var ddlSyncOnlineConn = $('select[id$="ddlSyncOnlineConn"]');
            var ddlSyncOnlnOfflnCommand = $('select[id$="ddlSyncOnlnOfflnCommand"]');
            var ddlSyncOfflnOnlnCommand = $('select[id$="ddlSyncOfflnOnlnCommand"]');
            var ddlOfflineTblType = $('select[id$="ddlOfflineTblType"]', _contextDivs.getOfflineTabCont());
            var objSyncCmdAndConnectionDtl = new Object();
            objSyncCmdAndConnectionDtl.conn = $(ddlSyncOnlineConn).val();
            if ($(ddlOfflineTblType).val() === "0") {
                objSyncCmdAndConnectionDtl.dwnCmd = "-1";
                objSyncCmdAndConnectionDtl.upCmd = $(ddlSyncOfflnOnlnCommand).val();
            } else if ($(ddlOfflineTblType).val() === "1") {
                objSyncCmdAndConnectionDtl.dwnCmd = $(ddlSyncOnlnOfflnCommand).val();
                objSyncCmdAndConnectionDtl.upCmd = "-1";
            } else if ($(ddlOfflineTblType).val() === "2") {
                objSyncCmdAndConnectionDtl.dwnCmd = $(ddlSyncOnlnOfflnCommand).val();
                objSyncCmdAndConnectionDtl.upCmd = $(ddlSyncOfflnOnlnCommand).val();
            }
            $(hidOfflineSyncCmdAndConJsonForSaving).val(_util.serializeObjectToJson(objSyncCmdAndConnectionDtl));
        },
        
        processResetFormOnPostBackError: function () {
            resetEditFormOnErrorInBackend();
            return false;
        },
        processResetFormOnPostBackErrInEditView: function () {
            resetClientCmptTblHIdForEidtCancel();
            return false;
        },
        processOfflineEditAndEditViewDelete: function () {
            var blnConfirm = confirm('Are you sure you want to delete this offline table.');
            if (blnConfirm) {
                this.setSyncTabCommandAndConnDtlJsonForSaving();
                _setFinalOfflineCmpltJsonForSavingInHid();
                _setFinalSyncJsonForSavingInHid();
                return true;
            }
            else {
                return false;
            }
        },
        
        setDefaultPageGlobalData:function(){
            _setDefaultPageGlobalData();
        },
        getPageGlobalData :function(){
            return _getPageGlobalData();
        },
        
        processEditOfflineTable:function(rowOfflnTblData/*data stored in row of repaeter.*/){
            _process.editOfflineTable(rowOfflnTblData);
        },
        processEditViewEdit: function () {
            _process.setOfflineTableEditFormInUI();
            _actionBtns.showHideForEditTbl();
            _process.showHideEditModeFormContDiv(false);
            _process.showHideAddModeFormContDiv(true);
            _globalData.setFormMode(OFFLINE_FORM_DISPLAY_MODE.Edit);
            return false;
        },
        processEditViewCancel: function () {
            _process.addNewTableInUI();
            _globalData.setFormMode(OFFLINE_FORM_DISPLAY_MODE.Add);
        },
        processEditFormCancel: function () {
            var objTable = _globalData.getDbSavedTable();
            var objSyncDetails = _globalData.getDbSavedSyncDetails();
            
            var strClonedObj = _util.serializeObjectToJson(objTable);
            objTable = new OfflineDatatable(_util.deserializeJson(strClonedObj));
            objTable.fnResetObjectDetails();
            _globalData.setTable(objTable);
            
            strClonedObj = _util.serializeObjectToJson(objSyncDetails);
            objSyncDetails = new SyncDtls(_util.deserializeJson(strClonedObj));
            objSyncDetails.fnResetObjectDetails();
            _globalData.setSyncDtls(objSyncDetails);
            
            _process.setOfflineTableEditViewInUI();
            _process.showHideEditModeFormContDiv(true);
            _process.showHideAddModeFormContDiv(false);
            
            _globalData.setFormMode(OFFLINE_FORM_DISPLAY_MODE.EditView);
        },
        processDeleteOfflineTable:function(){
            var blnConfirm = confirm('Are you sure you want to delete the offline table');
            if(blnConfirm === true){
                var objTable = _globalData.getTable();
                if(objTable){
                    processPostbackByHtmlCntrl.postBack(
                        postBackByHtmlProcess.DeleteOfflineDt,
                        [objTable.fnGetId()],"");
                }
            }
            
        },
        processAddNewTableInUI:function(){
            _process.addNewTableInUI();
        },
        doAfterSaveAndOpenNewTblSuccess:function(){
            _process.doAfterSaveAndOpenNewTblSuccess();
        },
        doAfterSaveAndOpenNewTblOnUpdateFailure: function(){
            _process.doAfterSaveAndOpenNewTblOnUpdateFailure();
        },
        doAfterSaveAndOpenNewTblOnNewTblFetchFailure:function(){
            _process.doAfterSaveAndOpenNewTblOnNewTblFetchFailure();
        },
        pageControlEvents :{
            ddlSyncCommandTypeChange:function(sender/*jquery object*/,evnt){
                var $ddlSyncOnlineConn = $('#ddlSyncOnlineConn'),
                    cmdType = sender.val(),
                    ideCmdObjectType = IDE_COMMAND_OBJECT_TYPES.db,
                    strDdlOptions = "";
                if(cmdType === "-1"){
                    strDdlOptions = '<option value="-1">select</option>';
                    cmdType = "";
                }
                else {
                    switch(cmdType)    {
                        case IDE_COMMAND_OBJECT_TYPES.db.id:
                                ideCmdObjectType = IDE_COMMAND_OBJECT_TYPES.db;
                            break;
                        case IDE_COMMAND_OBJECT_TYPES.wsdl.id:
                                ideCmdObjectType = IDE_COMMAND_OBJECT_TYPES.wsdl;
                        break;
                        case IDE_COMMAND_OBJECT_TYPES.http.id:
                                ideCmdObjectType = IDE_COMMAND_OBJECT_TYPES.http;
                        break;
                        case IDE_COMMAND_OBJECT_TYPES.xmlRpc.id:
                            ideCmdObjectType = IDE_COMMAND_OBJECT_TYPES.xmlRpc;
                        break;
                        case IDE_COMMAND_OBJECT_TYPES.odata.id:
                            ideCmdObjectType = IDE_COMMAND_OBJECT_TYPES.odata;
                        break;
                    }
                    strDdlOptions = mfOflnTblHelpers.getConnsAsOptionsOfDdl(
                        ideCmdObjectType,
                        true,"-1");
                    
                }
                $ddlSyncOnlineConn.html('');
                $ddlSyncOnlineConn.html(strDdlOptions);
                _util.setUniformedDropDownValue($ddlSyncOnlineConn,"-1");
                _syncTab.setInitialState.ddlSyncOnlnOfflnCommand();
                _syncTab.setInitialState.ddlSyncOfflnOnlnCommand();
                _syncTab.setInitialState.ddlModifiedRecUploadCmd();
                updateUniformedControl(_syncTab.getjQueryObj.ddlSyncOnlnOfflnCommand());
                updateUniformedControl(_syncTab.getjQueryObj.ddlSyncOfflnOnlnCommand());
                updateUniformedControl(_syncTab.getjQueryObj.ddlModifiedRecUploadCmd());
                _globalData.setCommandType(cmdType);
                _globalData.deleteDownloadData();
                _globalData.deleteUploadData();
            },
            ddlSyncOnlineConnChange:function(sender,evnt){
                var tableTabControls = _tableTab.getjQueryObj.allCntrls(),
                    syncTabControls = _syncTab.getjQueryObj.allCntrls(),
                    strSelectCmds = "",
                    strInsertCmds ="",
                    strUpdateCmds = "",
                    strConnection = "",
                    strCommandType = syncTabControls.ddlSyncCommandType.val(),
                    cmdType = null,
                    strDownCmds = "",
                    strUpCmds="";
                    
                strConnection =  sender.val();
                if(strConnection && strConnection != "-1"){
                    cmdType = mfOflnTblHelpers.getCommandTypeByValue(strCommandType);
                    switch(cmdType){
                        case IDE_COMMAND_OBJECT_TYPES.db:
                            strSelectCmds = 
                                mfOflnTblHelpers.
                                getSelectCmdsByConnsAsOptionsForDdl(
                                        strConnection,true,"-1"
                                    );
                            strInsertCmds  =
                                mfOflnTblHelpers.
                                getInsertCmdsByConnAsOptionsForDdl(
                                    strConnection,true,"-1"
                                );
                            if(strInsertCmds){
                                strUpdateCmds = 
                                    mfOflnTblHelpers.
                                    getUpdateCmdsByConnAsOptionsForDdl(
                                        strConnection,false,""
                                    );
                            }
                            else{
                                strUpdateCmds = 
                                    mfOflnTblHelpers.
                                    getUpdateCmdsByConnAsOptionsForDdl(
                                        strConnection,true,"-1"
                                    );
                            }
                            strDownCmds = strSelectCmds; 
                            strUpCmds = strInsertCmds+strUpdateCmds;
                            break;
                        case    IDE_COMMAND_OBJECT_TYPES.wsdl:
                        case    IDE_COMMAND_OBJECT_TYPES.http:
                        case    IDE_COMMAND_OBJECT_TYPES.xmlRpc:
                                strUpCmds = 
                                    mfOflnTblHelpers.
                                    getWsCmdsByConnAsOptionsForDdl(
                                        strConnection,cmdType,true,"-1"
                                    );
                                strDownCmds = strUpCmds;    
                            break;
                        case IDE_COMMAND_OBJECT_TYPES.odata:
                            strUpCmds= 
                                mfOflnTblHelpers.
                                getOdataCmdsByConnAsOptionsForDdl(
                                    strConnection,true,"-1"
                                );
                            strDownCmds = strUpCmds; 
                            break;
                    }
                    syncTabControls.ddlSyncDownCommand.html('');
                    syncTabControls.ddlSyncUpCommand.html('');
                    syncTabControls.ddlSyncDownCommand.html(strDownCmds);
                    syncTabControls.ddlSyncUpCommand.html(strUpCmds);
                    _syncTab.setInnerHtml.ddlModifiedRecUploadCmd(strUpCmds);
                    
                    updateUniformedControl(_syncTab.getjQueryObj.ddlSyncOnlnOfflnCommand());
                    updateUniformedControl(_syncTab.getjQueryObj.ddlSyncOfflnOnlnCommand());
                    updateUniformedControl(_syncTab.getjQueryObj.ddlModifiedRecUploadCmd());
                }
                else{
                    strConnection = "";
                    syncTabControls.ddlSyncDownCommand.html('<option value="-1">Select</option>');
                    syncTabControls.ddlSyncUpCommand.html('<option value="-1">Select</option>');
                    _syncTab.setInnerHtml.ddlModifiedRecUploadCmd('<option value="-1">Select</option>');
                    
                    updateUniformedControl(_syncTab.getjQueryObj.ddlSyncOnlnOfflnCommand());
                    updateUniformedControl(_syncTab.getjQueryObj.ddlSyncOfflnOnlnCommand());
                    updateUniformedControl(_syncTab.getjQueryObj.ddlModifiedRecUploadCmd());
                }
                _globalData.setConnection(strConnection);
                _globalData.deleteDownloadData();
                _globalData.deleteUploadData();
                
            },
            lnkTabTableClick:function(sender,evnt){
                _globalData.setTabSelected(OFFLINE_TABLE_FORM_TAB.TabTableContent);
            },
            lnkTabColumnsClick:function(sender,evnt){
                
            },
            lnkTabSyncClick:function(sender,evnt){
                
            },
            spanAddColumnToTableClick:function(sender,evnt){
                _columnTab.spanAddNewColumnClick(sender);
            },
            spanRemoveColumnFromTableClick:function(sender,evnt){
                if (sender) {
                    var confirmDelete = confirm('Are you sure you want to remove the column');
                    if (confirmDelete) {
                        var $self = $(sender);
                        if (!_isColumnToDeleteAlreadyMapped($self)) {
                            var tr = $self.closest('tr');
                            tr.fadeOut(400, function () {
                                tr.remove();
                                MFODB.saveColumnsAddedToOfflineTableObject();
                            });
                             return false;
                        } else {
                            showMessage(
                                "The column is already mapped to a input / output parameter.You have to first remove the mapping before deleting column.",
                                MessageOption.Error, DialogType.Error);
                        }
                    }
                }
            },
            btnUploadInputParamSave:function(sender,evnt){
                _syncTab.paramMapping.upload.saveInputMappingDtls();
                _syncTab.paramMapping.upload.closeInputMappingPopup();
            },
            btnDownloadInputParamSave:function(sender,evnt){
                _syncTab.paramMapping.download.saveInputMappingDtls();
                _syncTab.paramMapping.download.closeInputMappingPopup();
            },
            btnDownloadOutputParamSave:function(sender,evnt){
                _syncTab.paramMapping.download.saveOutputMappingDtls();
                _syncTab.paramMapping.download.closeOutputMappingPopup();
            },
            ddlSyncOnlnOfflnCommandChange:function(sender,evnt){
                if(sender.val() !== "-1"){
                    _globalData.setDownloadCommand(sender.val());
                    _globalData.deleteDownloadInputParam();
                    _globalData.deleteDownloadOutputParam();
                }
                else{
                    _globalData.deleteDownloadData();
                }
            },
            ddlSyncOfflnOnlnCommandChange:function(sender,evnt){
                if(sender.val() !== "-1"){
                    _globalData.setUploadNewRecCmd(sender.val());
                    _globalData.deleteUploadNewRecInputParams();
                }
                else{
                    _globalData.deleteUploadNewRec();
                }
            },
            ddlOfflineTblTypeChange:function(sender,evnt){
                var $self = $(sender),
                    strVal = $self.val(),
                    objDownloadData = null;    
                
                if(strVal === "0"){
                    objDownloadData = _globalData.getDownloadData();
                    if(objDownloadData){
                        _globalData.setIsInitialDownload("1");
                        _syncTab.setValue.chkInitialDownload(true);
                    }
                    else{
                        _globalData.setIsInitialDownload("0");
                        _syncTab.setValue.ddlSyncOnlnOfflnCommand("-1");
                        _syncTab.setValue.chkInitialDownload(false);
                    }
                }
                else if(strVal === "1"){
                    _globalData.setIsInitialDownload("0");
                    _globalData.deleteUploadData();
                }
                else if(strVal === "2"){
                    
                }
                _process.showHideCntrlsByTblTypeSelected();
                
            },
            lnkAddNewOfflineTableClick:function(sender,evnt){
                _process.addNewTableInUI();
            },
            lnkEditViewEditClick :function(sender,evnt){
                MFODB.processEditViewEdit();
                return false;
            },
            lnkEditViewDeleteClick:function(sender,evnt){
                
            },
            lnkEditViewCancelClick:function(sender,evnt){
                MFODB.processEditViewCancel(); 
            },
            btnSaveOfflineTblSettingsClick:function(sender,evnt){
                _tableTab.setOfflineTableInGlobalData();
                _syncTab.setSyncDetailsInGlobalDataFromHtml();
                var aryErrors = _process.validateForSaving();
                var eProcess = postBackByHtmlProcess.SaveNewOfflineDt,
                    blnIsEdit = false;
                if ($.isArray(aryErrors) && aryErrors.length>0){
                    //return false;
                    throw new MfError(aryErrors);
                }
                else{
                    _setFinalOfflineCmpltJsonForSavingInHid();
                    _setFinalSyncJsonForSavingInHid();
                    blnIsEdit = _globalData.getIsEdit();
                    
                    if(blnIsEdit === false){
                        processPostbackByHtmlCntrl.postBack(
                            postBackByHtmlProcess.SaveNewOfflineDt,
                            [],"");
                    }
                    else{
                          processPostbackByHtmlCntrl.postBack(
                            postBackByHtmlProcess.UpdateOfflineDt,
                            [],"");
                    }
                }
            },
            btnEditCancelOfflineTblSettingsClick:function(sender,evnt){
                MFODB.processEditFormCancel();
            },
            btnDeleteOfflineTblSettings:function(sender,evnt){
                MFODB.processDeleteOfflineTable();   
            },
            lnkOfflineEditViewDelete:function(sender,evnt){
                MFODB.processDeleteOfflineTable();
            },
            chkModifiedRecSameAsNewRecord:function(sender,evnt){
                _syncTab.doProcessByChkModifiedRecSameAsNewRec(sender);
            },
            chkUploadDeleteRec:function(sender,evnt){
                _syncTab.doProcessForChkUploadDeleteRec(sender);
            },
            chkInitialDownload:function(sender,evnt){
                _syncTab.doProcessByChkInitialDownload(sender);
            },
            spanEditModifiedRecUploadInputParam:function(sender,evnt){
                _syncTab.paramMapping.modifiedRec.showInputParamPopup(true);
            },
            spanModifiedRecUploadInputParam:function(sender,evnt){
                _syncTab.paramMapping.modifiedRec.showInputParamPopup(false);
            },
            btnUpldModifiedRecInParamSave: function(sender,evnt){
                _syncTab.paramMapping.modifiedRec.saveMappingDtls();
                _syncTab.paramMapping.modifiedRec.closeInputMappingPopup();
            },
            btnUpldModifiedRecInParamCancel:function(sender,evnt){
                _syncTab.paramMapping.modifiedRec.closeInputMappingPopup();
            },
            spanEditOnlnOfflnInput : function(sender,evnt){
                _syncTab.paramMapping.download.showInputParamPopup(false);
            },
            spanEditViewDownloadInput : function(sender,evnt){
                _syncTab.paramMapping.download.showInputParamPopup(true);
            },
            spanEditOnlnOfflnOutput:function(sender,evnt){
                _syncTab.paramMapping.download.showOutputParamPopup(false);
            },
            //spanEditViewOnlnOfflnOutput:function(sender,evnt){
               
            // },
            spanEditViewDownloadOutput : function(sender,evnt){
                _syncTab.paramMapping.download.showOutputParamPopup(true);
            },
            spanOfflnOnlnInput : function(sender,evnt){
                _syncTab.paramMapping.upload.showInputParamPopup(false);
            },
            spanEditUploadInput : function(sender,evnt){
                _syncTab.paramMapping.upload.showInputParamPopup(true);
            },
            
            ddlModifiedRecUploadCommand:function(sender,evnt){
                if(sender.val() !== "-1"){
                    _globalData.setUploadModifiedRecCmd(sender.val());
                    _globalData.deleteUploadModifiedRecInputParams();
                }
                else{
                    _globalData.deleteModifiedNewRec();
                }
            },
            btnCancelOfflineTblSettings:function(sender,evnt){
                _process.addNewTableInUI();
            },
            chkOfflineUserConfirmation:function(sender,evnt){
                
            }
        },
        globalData :{
            getFormDisplayPageMode : function(){
                return _globalData.getFormMode();
            },
            getTableId : function(){
                return _globalData.getTableId();
            }
        },
        editDiscardConfirmationProcs : {
            discard : function(discard /*boolean*/){
               _editDiscardConfirmationProcs.discard(discard);
            },
            cancel : function(){
                _editDiscardConfirmationProcs.cancel();
            },
            globalData : {
                setData : function(data){
                    _editDiscardConfirmationProcs.globalData.setData(data);
                },
                removeData : function(){
                    _editDiscardConfirmationProcs.globalData.removeData();
                }
            }
        }
    }
} (jQuery));


$(document).ready(function() {
    
    $('#ddlSyncCommandType').on('change',function(evnt){
        try{
            MFODB.pageControlEvents.ddlSyncCommandTypeChange($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#ddlSyncOnlineConn').on('change',function(evnt){
       try{
           MFODB.pageControlEvents.ddlSyncOnlineConnChange($(this),evnt);
       }
       catch(error){
           if (error instanceof MfError){
               showMessage(
                   mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                   DialogType.Error);
           }else{
               console.log(error);
           }
       } 
   });
    $('#lnkTabTable').on('click',function(evnt){
       try{
           MFODB.pageControlEvents.lnkTabTableClick($(this),evnt);
       }
       catch(error){
           if (error instanceof MfError){
               showMessage(
                   mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                   DialogType.Error);
           }else{
               console.log(error);
           }
       } 
   });
    $('#lnkTabColumns').on('click',function(evnt){
       try{
           MFODB.pageControlEvents.lnkTabColumnsClick($(this),evnt);
       }
       catch(error){
           if (error instanceof MfError){
               showMessage(
                   mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                   DialogType.Error);
           }else{
               console.log(error);
           }
       } 
   });
    $('#lnkTabSync').on('click',function(evnt){
       try{
           MFODB.pageControlEvents.lnkTabSyncClick($(this),evnt);
       }
       catch(error){
           if (error instanceof MfError){
               showMessage(
                   mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                   DialogType.Error);
           }else{
               console.log(error);
           }
       } 
   });
    $('#spanAddColumnToTable').on('click',function(evnt){
       try{
           MFODB.pageControlEvents.spanAddColumnToTableClick($(this),evnt);
       }
       catch(error){
           if (error instanceof MfError){
               showMessage(
                   mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                   DialogType.Error);
           }else{
               console.log(error);
           }
       } 
   });
    $('#btnUpldInParamSave').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnUploadInputParamSave($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnDwnldInParamSave').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnDownloadInputParamSave($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnDwnldOutParamSave').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnDownloadOutputParamSave($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('select[id$="ddlSyncOnlnOfflnCommand"]').on('change',function(evnt){
        try{
            MFODB.pageControlEvents.ddlSyncOnlnOfflnCommandChange($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });  
    $('select[id$="ddlSyncOfflnOnlnCommand"]').on('change',function(evnt){
        try{
            MFODB.pageControlEvents.ddlSyncOfflnOnlnCommandChange($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('select[id$="ddlOfflineTblType"]').on('change',function(evnt){
        try{
            MFODB.pageControlEvents.ddlOfflineTblTypeChange($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#lnkAddNewOfflineTable').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.lnkAddNewOfflineTableClick($(this),evnt);
            return false;
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('[id$="lnkEditViewEdit"]').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.lnkEditViewEditClick($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('[id$="lnkOfflineEditViewDelete"]').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.lnkEditViewDeleteClick($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('[id$="lnkEditViewBack"]').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.lnkEditViewCancelClick($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnSaveOfflineTblSettings1').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnSaveOfflineTblSettingsClick($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnEditCancelOfflineTblSettings').on('click',function(evnt){
         try{
             MFODB.pageControlEvents.btnEditCancelOfflineTblSettingsClick($(this),evnt);
         }
         catch(error){
             if (error instanceof MfError){
                 showMessage(
                     mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                     DialogType.Error);
             }else{
                 console.log(error);
             }
         } 
     });
    $('#btnDeleteOfflineTblSettings').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnDeleteOfflineTblSettings($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    }); 
    $('#lnkOfflineEditViewDelete').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.lnkOfflineEditViewDelete($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    }); 
    $('#chkModifiedRecSameAsNewRecord').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.chkModifiedRecSameAsNewRecord($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#chkUploadDeleteRec').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.chkUploadDeleteRec($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#chkInitialDownload').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.chkInitialDownload($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanEditModifiedRecUploadInputParam').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanEditModifiedRecUploadInputParam($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanModifiedRecUploadInputParam').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanModifiedRecUploadInputParam($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnUpldModifiedRecInParamSave').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnUpldModifiedRecInParamSave($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnUpldModifiedRecInParamCancel').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnUpldModifiedRecInParamCancel($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanEditOnlnOfflnInput').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanEditOnlnOfflnInput($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanEditViewDownloadInput').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanEditViewDownloadInput($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanEditOnlnOfflnOutput').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanEditOnlnOfflnOutput($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanEditViewDownloadOutput').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanEditViewDownloadOutput($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanOfflnOnlnInput').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanOfflnOnlnInput($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('select[id ="ddlModifiedRecUploadCommand"]').on('change',function(evnt){
        try{
            MFODB.pageControlEvents.ddlModifiedRecUploadCommand($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnCancelOfflineTblSettings').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.btnCancelOfflineTblSettings($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#spanEditUploadInput').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.spanEditUploadInput($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        }
    });
    $('#chkOfflineUserConfirmation').on('click',function(evnt){
        try{
            MFODB.pageControlEvents.chkOfflineUserConfirmation($(this),evnt);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        }
    });
    $('#btnOflnTblEditDiscardYes').on('click',function(evnt){
        try{
            MFODB.editDiscardConfirmationProcs.discard(true);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnOflnTblEditDiscardClose').on('click',function(evnt){
        try{
            MFODB.editDiscardConfirmationProcs.cancel();
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
    $('#btnOflnTblEditDiscardNo').on('click',function(evnt){
        try{
            MFODB.editDiscardConfirmationProcs.discard(false);
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(
                    mfOflnTblHelpers.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                    DialogType.Error);
            }else{
                console.log(error);
            }
        } 
    });
});


function updateUniformedControl(cntrl/*jquery object*/){
    $.uniform.update(cntrl);
}


function ddl_DataCache_type_Changed(e) {
    //initializeDataCache();
    if($(e).val() == '0'){
        initializeDataCache();
    }
    else if ($(e).val() == '1') {
        $('#divAbsoluteTime').show();
        $('#divAbsoluteMinutes').show();
        $('#divRelativeTime').hide();
        $('#divRelativeMinute').hide();
        $('#divRelativeHour').hide();
        $('#divRelativeDay').hide();
        $('#divRelativeWeek').hide();
        $('#divRelativeMonth').hide();
    }
    else if ($(e).val() == '2') {
        $('#divAbsoluteTime').hide();
        $('#divAbsoluteMinutes').hide();
        $('#divAbsoluteDay').hide();
        $('#divAbsoluteWeek').hide();
        $('#divAbsoluteDate').hide();
        $('#divAbsoluteYear').hide();
        $('#divRelativeTime').show();
        $('#divRelativeMinute').show();
    }
}
function initializeDataCache() {
    
    $('select[id$="ddl_DataCache_type"]').val('0');
    updateUniformedControl($('select[id$="ddl_DataCache_type"]'));
    $('[id$=ddl_AbsoluteCachePeriod]').val('0');
    $('select[id$=ddl_AbsoluteCachePeriod]').parent().children("span").text($('select[id$=ddl_AbsoluteCachePeriod]').children('option:selected').text());

    $('[id$=ddl_AbsoluteMinutes]').val('0');
    $('select[id$=ddl_AbsoluteMinutes]').parent().children("span").text($('select[id$=ddl_AbsoluteMinutes]').children('option:selected').text());

    $('[id$=ddl_AbsoluteDayHour]').val('0');
    $('select[id$=ddl_AbsoluteDayHour]').parent().children("span").text($('select[id$=ddl_AbsoluteDayHour]').children('option:selected').text());

    $('[id$=ddl_AbsoluteDayMin]').val('0');
    $('select[id$=ddl_AbsoluteDayMin]').parent().children("span").text($('select[id$=ddl_AbsoluteDayMin]').children('option:selected').text());

    $('[id$=ddl_AbsoluteWeek]').val('1');
    $('select[id$=ddl_AbsoluteWeek]').parent().children("span").text($('select[id$=ddl_AbsoluteWeek]').children('option:selected').text());

    $('[id$=ddl_AbsoluteDate]').val('1');
    $('select[id$=ddl_AbsoluteDate]').parent().children("span").text($('select[id$=ddl_AbsoluteDate]').children('option:selected').text());

    $('[id$=ddl_AbsoluteYear_Day]').val('1');
    $('select[id$=ddl_AbsoluteYear_Day]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Day]').children('option:selected').text());

    $('[id$=ddl_AbsoluteYear_Month]').val('1');
    $('select[id$=ddl_AbsoluteYear_Month]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Month]').children('option:selected').text());

    $('[id$=ddl_RelativeCachePeriod]').val('0');
    $('select[id$=ddl_RelativeCachePeriod]').parent().children("span").text($('select[id$=ddl_RelativeCachePeriod]').children('option:selected').text());

    $('[id$=ddl_RelativeMinute]').val('0');
    $('select[id$=ddl_RelativeMinute]').parent().children("span").text($('select[id$=ddl_RelativeMinute]').children('option:selected').text());

    $('[id$=ddl_RelativeHour]').val('0');
    $('select[id$=ddl_RelativeHour]').parent().children("span").text($('select[id$=ddl_RelativeHour]').children('option:selected').text());

    $('[id$=ddl_RelativeDay]').val('0');
    $('select[id$=ddl_RelativeDay]').parent().children("span").text($('select[id$=ddl_RelativeDay]').children('option:selected').text());

    $('[id$=ddl_RelativeWeek]').val('0');
    $('select[id$=ddl_RelativeWeek]').parent().children("span").text($('select[id$=ddl_RelativeWeek]').children('option:selected').text());

    $('[id$=ddl_RelativeMonth]').val('0');
    $('select[id$=ddl_RelativeMonth]').parent().children("span").text($('select[id$=ddl_RelativeMonth]').children('option:selected').text());

    $('#divAbsoluteTime').hide();
    $('#divRelativeTime').hide();
    $('#divAbsoluteMinutes').hide();
    $('#divAbsoluteDay').hide();
    $('#divAbsoluteWeek').hide();
    $('#divAbsoluteDate').hide();
    $('#divAbsoluteYear').hide();
    $('#divRelativeTime').hide();
    $('#divRelativeMinute').hide();
    $('#divRelativeHour').hide();
    $('#divRelativeDay').hide();
    $('#divRelativeWeek').hide();
    $('#divRelativeMonth').hide();
}
function ddl_AbsoluteCachePeriod_Changed(e) {
    $('#divAbsoluteMinutes').hide();
    $('#divAbsoluteDay').hide();
    $('#divAbsoluteWeek').hide();
    $('#divAbsoluteDate').hide();
    $('#divAbsoluteYear').hide();
    switch ($(e).val()) {
        case '0':
            $('#divAbsoluteMinutes').show();
            break;
        case '1':
            $('#divAbsoluteDay').show();
            break;
        case '2':
            $('#divAbsoluteWeek').show();
            break;
        case '3':
            $('#divAbsoluteDate').show();
            break;
        case '4':
            $('#divAbsoluteYear').show();
            break;
    }
}
function ddl_RelativeCachePeriod_Changed(e) {
    $('#divRelativeMinute').hide();
    $('#divRelativeHour').hide();
    $('#divRelativeDay').hide();
    $('#divRelativeWeek').hide();
    $('#divRelativeMonth').hide();
    switch ($(e).val()) {
        case '0':
            $('#divRelativeMinute').show();
            break;
        case '1':
            $('#divRelativeHour').show();
            break;
        case '2':
            $('#divRelativeDay').show();
            break;
        case '3':
            $('#divRelativeWeek').show();
            break;
        case '4':
            $('#divRelativeMonth').show();
            break;
    }
}
function getCacheDetailsForSaving(){
    var cacheFrequency = 0,
        strCacheCondition = "",
        objCacheDtls = null;
    if ($('select[id$="ddl_DataCache_type"]').val() == "1"){
        cacheFrequency = $('select[id$="ddl_AbsoluteCachePeriod"]').val();
    }
    else if ($('select[id$="ddl_DataCache_type"]').val() == "2"){
        cacheFrequency = $('select[id$="ddl_RelativeCachePeriod"]').val();
    }
        
    strCacheCondition =  getCacheCondition(); 
    objCacheDtls = new CachingDetails();
    objCacheDtls.fnSetType($('select[id$="ddl_DataCache_type"]').val());
    objCacheDtls.fnSetFrequency(cacheFrequency);
    objCacheDtls.fnSetCondition(strCacheCondition);
    return objCacheDtls;
}
function getCacheCondition(){
    var expCondition = "",
        $ddl_DataCache_type = $('select[id$="ddl_DataCache_type"]'),
        $ddl_AbsoluteCachePeriod = $('select[id$="ddl_AbsoluteCachePeriod"]'),
        $ddl_RelativeCachePeriod = $('select[id$="ddl_RelativeCachePeriod"]'),
        strCacheType = $ddl_DataCache_type.val(),
        strAbsoluteCachePeriod= $ddl_AbsoluteCachePeriod.val(),
        strRelativeCachePeriod= $ddl_RelativeCachePeriod.val();
    if (strCacheType == "1"){
        switch (strAbsoluteCachePeriod){
            case "0":
                expCondition = $('select[id$="ddl_AbsoluteMinutes"]').val();
                break;
            case "1":
                expCondition = $('select[id$="ddl_AbsoluteDayHour"]').val() + ":" + $('select[id$="ddl_AbsoluteDayMin"]').val();
                break;
            case "2":
                expCondition = $('select[id$="ddl_AbsoluteWeek"]').val();
                break;
            case "3":
                expCondition = $('select[id$="ddl_AbsoluteDate"]').val();
                break;
            case "4":
                expCondition = $('select[id$="ddl_AbsoluteYear_Day"]').val() + "/" + $('select[id$="ddl_AbsoluteYear_Month"]').val();
                break;
        }
    }
    else if (strCacheType == "2"){
        switch (strRelativeCachePeriod){
            case "0":
                expCondition = $('select[id$="ddl_RelativeMinute"]').val();
                break;
            case "1":
                expCondition = $('select[id$="ddl_RelativeHour"]').val();
                break;
            case "2":
                expCondition = $('select[id$="ddl_RelativeDay"]').val();
                break;
            case "3":
                expCondition = $('select[id$="ddl_RelativeWeek"]').val();
                break;
            case "4":
                expCondition = $('select[id$="ddl_RelativeMonth"]').val();
                break;
        }
    }
    return expCondition;
}
function setEditCacheValInUI(cachingDetails) {
    if(cachingDetails){
        initializeDataCache();
        cacheType = cachingDetails.fnGetType();
        expFrequency = cachingDetails.fnGetFrequency();
        expCondition = cachingDetails.fnGetCondition();
        $('[id$=ddl_DataCache_type]').val(cacheType);
        $('select[id$=ddl_DataCache_type]').parent().children("span").text($('select[id$=ddl_DataCache_type]').children('option:selected').text());
        if (cacheType == "1") {
            $('[id$=ddl_AbsoluteCachePeriod]').val(expFrequency);
            $('select[id$=ddl_AbsoluteCachePeriod]').parent().children("span").text($('select[id$=ddl_AbsoluteCachePeriod]').children('option:selected').text());
            $('#divAbsoluteTime').show();
            switch (expFrequency) {
                case '0':
                    $('[id$=ddl_AbsoluteMinutes]').val(expCondition);
                    $('select[id$=ddl_AbsoluteMinutes]').parent().children("span").text($('select[id$=ddl_AbsoluteMinutes]').children('option:selected').text());
                    $('#divAbsoluteMinutes').show();
                    break;
                case '1':
                    var arry = expCondition.split(':');
                    $('[id$=ddl_AbsoluteDayHour]').val(arry[0]);
                    $('select[id$=ddl_AbsoluteDayHour]').parent().children("span").text($('select[id$=ddl_AbsoluteDayHour]').children('option:selected').text());
                    $('[id$=ddl_AbsoluteDayMin]').val(arry[1]);
                    $('select[id$=ddl_AbsoluteDayMin]').parent().children("span").text($('select[id$=ddl_AbsoluteDayMin]').children('option:selected').text());
                    $('#divAbsoluteDay').show();
                    break;
                case '2':
                    $('[id$=ddl_AbsoluteWeek]').val(expCondition);
                    $('select[id$=ddl_AbsoluteWeek]').parent().children("span").text($('select[id$=ddl_AbsoluteWeek]').children('option:selected').text());
                    $('#divAbsoluteWeek').show();
                    break;
                case '3':
                    $('[id$=ddl_AbsoluteDate]').val(expCondition);
                    $('select[id$=ddl_AbsoluteDate]').parent().children("span").text($('select[id$=ddl_AbsoluteDate]').children('option:selected').text());
                    $('#divAbsoluteDate').show();
                    break;
                case '4':
                    var arry = expCondition.split('/');
                    $('[id$=ddl_AbsoluteYear_Day]').val(arry[0]);
                    $('select[id$=ddl_AbsoluteYear_Day]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Day]').children('option:selected').text());
                    $('[id$=ddl_AbsoluteYear_Month]').val(arry[1]);
                    $('select[id$=ddl_AbsoluteYear_Month]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Month]').children('option:selected').text());
                    $('#divAbsoluteYear').show();
                    break;
            }
        }
        else if (cacheType == "2") {
            $('[id$=ddl_RelativeCachePeriod]').val(expFrequency);
            $('select[id$=ddl_RelativeCachePeriod]').parent().children("span").text($('select[id$=ddl_RelativeCachePeriod]').children('option:selected').text());
            $('#divRelativeTime').show();
            switch (expFrequency) {
                case '0':
                    $('[id$=ddl_RelativeMinute]').val(expCondition);
                    $('select[id$=ddl_RelativeMinute]').parent().children("span").text($('select[id$=ddl_RelativeMinute]').children('option:selected').text());
                    $('#divRelativeMinute').show();
                    break;
                case '1':
                    $('[id$=ddl_RelativeHour]').val(expCondition);
                    $('select[id$=ddl_RelativeHour]').parent().children("span").text($('select[id$=ddl_RelativeHour]').children('option:selected').text());
                    $('#divRelativeHour').show();
                    break;
                case '2':
                    $('[id$=ddl_RelativeDay]').val(expCondition);
                    $('select[id$=ddl_RelativeDay]').parent().children("span").text($('select[id$=ddl_RelativeDay]').children('option:selected').text());
                    $('#divRelativeDay').show();
                    break;
                case '3':
                    $('[id$=ddl_RelativeWeek]').val(expCondition);
                    $('select[id$=ddl_RelativeWeek]').parent().children("span").text($('select[id$=ddl_RelativeWeek]').children('option:selected').text());
                    $('#divRelativeWeek').show();
                    break;
                case '4':
                    $('[id$=ddl_RelativeMonth]').val(expCondition);
                    $('select[id$=ddl_RelativeMonth]').parent().children("span").text($('select[id$=ddl_RelativeMonth]').children('option:selected').text());
                    $('#divRelativeMonth').show();
                    break;
            }
        }
    }
}
function getSuffixForDateForUI(day){
    var strSuffix = "";
    switch(day){
        case "1":
        case "21" :
        case "31":  
            strSuffix = "st";
            break;
        case "2":
        case "22":
            strSuffix = "nd";
            break;
        case "3":
        case "23":
            strSuffix = "rd";
            break;
        default:
            strSuffix = "th";
            break;
    }   
    return strSuffix;
}
function getCacheDetailsForEditView(cachingDetails){
    var cacheType = "",
        expFrequency = "",
        expCondition = "",
        strCacheDetails = "None";
    if(cachingDetails){
        cacheType = cachingDetails.fnGetType();
        expFrequency = cachingDetails.fnGetFrequency();
        expCondition = cachingDetails.fnGetCondition();
        strCacheDetails = "";
        if (cacheType == "1") {
            strCacheDetails +=  "Absolute cache : ";
            switch (expFrequency) {
                case '0':
                    strCacheDetails += "Every " + expCondition + " hour";
                    break;
                case '1':
                    var arry = expCondition.split(':');
                    strCacheDetails += "Every " + arry[0] + " Hour and "+ arry[1] +" minutes" ;
                    break;
                case '2':
                    strCacheDetails += "Every week on " +  mfOflnTblHelpers.getWeekdayNameByVal(expCondition);
                    break;
                case '3':
                    strCacheDetails += "Every month on " + expCondition+getSuffixForDateForUI(expCondition);
                    break;
                case '4':
                    var arry = expCondition.split('/');
                    strCacheDetails += "Every Year on " + arry[0] + getSuffixForDateForUI(arry[0])+" "+ mfOflnTblHelpers.getMonthNameByVal(arry[1]) ;
                    break;
            }
        }
        else if (cacheType == "2") {
            strCacheDetails +=  "Relative cache : ";
            switch (expFrequency) {
                case '0':
                    strCacheDetails += "After " +expCondition + " minutes";
                    break;
                case '1':
                    strCacheDetails += "After " +expCondition + " hours";
                    break;
                case '2':
                    strCacheDetails += "After " +expCondition + " days";
                    break;
                case '3':
                    strCacheDetails += "After " +expCondition + " weeks";
                    break;
                case '4':
                    strCacheDetails += "After " +expCondition + " months";
                    break;
            }
        }
        else{
            strCacheDetails = "None";
        }
    }
    return strCacheDetails;
}
