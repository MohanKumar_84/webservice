﻿mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Section = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.userDefinedName = evnt.target.value;
                },
                TemplateChange: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.userDefinedName = evnt.target.value;
                }
            }
        }
    }
};

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Section = {
    "type": MF_IDE_CONSTANTS.CONTROLS.SECTION,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Template",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Template",
                "defltValue": "value",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.ControlProperties.eventHandlers.TemplateChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            }
         ]
      }
   ]
};

function RowPanel(id/*string*/,
                 type/*string*/,
                 colPanels/*Array of ColumnPanel*/) {
    this.id = id;
    this.type = type;
    this.colmnPanels = colPanels;

    RowPanel.prototype.fnAddColumnPanel = function (colPanel/*ColumnPanel*/) {
        if (!this.colmnPanels || !$.isArray(this.colmnPanels)) {
            this.colmnPanels = [];
        }
        if (colPanel && colPanel instanceof ColumnPanel) {
            this.colmnPanels.push(colPanel);
        }
    }
    RowPanel.prototype.fnGetColumnPanel = function (colPanelId/*ColumnPanelId*/) {
        var objColPanel = null;
        if (this.colmnPanels != null || this.colmnPanels != undefined) {
            for (var col = 0; col <= this.colmnPanels.length - 1; col++) {
                if (this.colmnPanels[col].id == colPanelId)
                    objColPanel = this.colmnPanels[col];
            }
        }
        return objColPanel;
    }
}


    //Tanika
function ColumnPanel(index/*string*/,
                    id/*string*/,
                    name/*string*/,
                    display/*bit*/,
                    conditions/*Array of ConditionalLogic*/,
                    controls/*Array of Control*/) {
    this.index = index;
    this.id = id;
    this.name = name;
    this.display = display;
    this.conditions = conditions;
    this.controls = controls;

    ColumnPanel.prototype.fnAddControl = function (control/*Control*/) {
        if (!this.controls || !$.isArray(this.controls)) {
            this.controls = [];
        }
        if (control && control instanceof Control) {
            this.controls.push(control);
        }
    }

    ColumnPanel.prototype.fnGetControl = function (controlId/*ControlId*/) {
        var objControl = null;
        if (this.controls != null || this.controls != undefined) {
            for (var cntrl = 0; cntrl <= this.controls.length - 1; cntrl++) {
                if (this.controls[cntrl].id == controlId)
                    objControl = this.controls[cntrl];
            }
        }
        return objControl;
    }
}