﻿
/*---------------------------------------------------TextBox---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                    //propObject.userDefinedName = evnt.target.value;
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                setTextboxLabel(appObj);
            },
            Inline: function (control, appObj) {
                $(control).val(appObj.inline);
                getInlineTextboxHtml(appObj);
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                setTextboxStyle(appObj);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $LabelValueProperty.css('color', '#' + appObj.fontColor + ' !important');
                }
                else
                    $LabelValueProperty.css('color', '#000000' + ' !important');

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        var $LabelProp = $('#' + appObj.id + '_Label');
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $LabelProp.css('color', fontCol + ' !important');
                        }
                        else {
                            $LabelProp.css('color', '#000000' + ' !important');
                        }
                    }
                });

                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).on("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Placeholder: function (control, appObj) {
                $(control).val(appObj.placeholder);
                setTextboxPlaceHolder(appObj);
            },
            NoofLines: function (control, appObj) {
                $(control).val(appObj.txtRowsCount)
            },
            MaxLength: function (control, appObj) {
                $(control).val(appObj.maxLength)
            },
            MinWidth: function (control, appObj) {
                $(control).val(appObj.minWidth)
            },
            Width: function (control, appObj) {
                $(control).val(appObj.widthInPercent)
            },
            Mask: function (control, appObj) {
                $(control).val(appObj.maskValue)
            },
            Size: function (control, cntrlObj) {
                if (!cntrlObj.dataMini)
                    cntrlObj.dataMini = "0";
                setTextboxSize(cntrlObj);
                $(control).val(cntrlObj.fnGetDataMini());
            },
            Icon: function (control, cntrlObj, options) {
                var aryOptionsForDdl = [{ text: "None", val: "0"}];
                aryOptionsForDdl = $.merge(aryOptionsForDdl,
                                    MF_HELPERS.jqueryMobileIconsHelper.getAsTextValPairList());
                MF_HELPERS.bindDropdowns([$(control)], aryOptionsForDdl);
                var strIcon = cntrlObj.fnGetIcon();
                if (!mfUtil.isNullOrUndefined(strIcon)) {
                    if (mfUtil.isEmptyString(strIcon)) {
                        $(control).val("0");
                    }
                    else {
                        $(control).val(strIcon);
                    }
                }
                else {
                    $(control).val("0");
                }

                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.inline && cntrlObj.inline === "1") {
                    currentCntrl.wrapperDiv.show();
                    setTextboxIcon(cntrlObj);
                }
                else {
                    currentCntrl.wrapperDiv.hide();
                }
            },
            eventHandlers: {
                TxtLabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var $LabelProperty = $('#' + propObject.id);
                    propObject.labelText = evnt.target.value;
                    setTextboxLabel(propObject);
                },
                TxtFontStyle: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    setTextboxStyle(propObject);
                },
                TxtValidateKeyPressed: function (evnt) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(evnt);
                },
                TxtPlaceholderValChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.placeholder = evnt.target.value;
                    setTextboxPlaceHolder(propObject);
                },
                TxtNoOfLinesChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.txtRowsCount = evnt.target.value;
                },
                TxtMaxLengthChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.maxLength = evnt.target.value;
                },
                TxtMinWidthChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.minWidth = evnt.target.value;
                },
                TxtWidthChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.widthInPercent = evnt.target.value;
                },
                TxtMaskChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.maskValue = evnt.target.value;
                },
                TxtInlineChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.inline = evnt.target.value;
                    var cntrls = evnt.data.cntrls;
                    getInlineTextboxHtml(propObject);

                    var $IconWrapper = $('#' + cntrls.Icon.wrapperDiv);
                    var $IconControl = $('#' + cntrls.Icon.controlId);
                    if (propObject.inline === "1") {
                        propObject.fnSetIcon("0");
                        $IconWrapper.show();
                    }
                    else {
                        propObject.icon = "";
                        $IconControl.val("");
                        $IconWrapper.hide();
                    }
                },
                Size: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fnSetDataMini(evnt.target.value);
                    setTextboxSize(propObject);
                },
                Icon: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var cntrls = evntData.cntrls;
                    propObject.fnSetIcon($self.val());

                    setTextboxIcon(propObject);
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                //$(control).val(appObj.bindType.id);
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            BindToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Data.getBindToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else {
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            DefaultValue: function (control, appObj) {
                $(control).val(appObj.dfltValue);
                setTextboxPlaceHolder(appObj);
            },
            eventHandlers: {
                BindToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Data.getBindToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty, propObject))
                        propObject.bindToProperty = evnt.target.value;
                    else {
                        if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Data.getDataObjectCntrl();
                    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                    intlJson = [],
                    isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.TEXTBOX,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                },
                DefaultValChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.dfltValue = evnt.target.value;
                    setTextboxPlaceHolder(propObject);
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            ValidationType: function (control, appObj) {
                $(control).val(appObj.validationType);
            },
            MinValue: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getMinValueCntrl();
                if (appObj.validationType == '1' || appObj.validationType == '2') {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.minVal);
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            MaxValue: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getMaxValueCntrl();
                if (appObj.validationType == '1' || appObj.validationType == '2') {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.maxVal);
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            ValidateMinLength: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getMinLengthCntrl();
                if (appObj.validationType == '3' || appObj.validationType == '4') {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.minLeng);
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            ValidateMaxLength: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getMaxLengthCntrl();
                if (appObj.validationType == '3' || appObj.validationType == '4') {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.maxLeng);
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            Value: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getValidationValueCntrl();
                if (appObj.validationType == "6") {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.multipleOf);
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            ValidationErrorMsg: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getValidationErrorMsgCntrl();
                if (appObj.validationType > -1) {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.errorMsg);
                    $(control).show();
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            Regx: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Validation.getRegxCntrl();
                if (appObj.validationType == "5") {
                    $(currentCntrl.wrapperDiv).show();
                    $(control).val(appObj.regularExp);
                }
                else {
                    $(control).val('');
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                },
                TxtValidation: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.validationType = evnt.target.value;
                    var $ValidationErrorMsgWrapper = $('#' + cntrls.ValidationErrorMsg.wrapperDiv);
                    var $ValidationValueWrapper = $('#' + cntrls.Value.wrapperDiv);
                    var $RegxWrapper = $('#' + cntrls.Regx.wrapperDiv);
                    var $MinValueWrapper = $('#' + cntrls.MinValue.wrapperDiv);
                    var $MaxValueWrapper = $('#' + cntrls.MaxValue.wrapperDiv);
                    var $MinLengthWrapper = $('#' + cntrls.ValidateMinLength.wrapperDiv);
                    var $MaxLengthWrapper = $('#' + cntrls.ValidateMaxLength.wrapperDiv);

                    var $ValidationErrorMsgControl = $('#' + cntrls.ValidationErrorMsg.controlId);
                    var $ValidationValueControl = $('#' + cntrls.Value.controlId);
                    var $RegxControl = $('#' + cntrls.Regx.controlId);
                    var $MinValueControl = $('#' + cntrls.MinValue.controlId);
                    var $MaxValueControl = $('#' + cntrls.MaxValue.controlId);
                    var $MinLengthControl = $('#' + cntrls.ValidateMinLength.controlId);
                    var $MaxLengthControl = $('#' + cntrls.ValidateMaxLength.controlId);

                    $($ValidationErrorMsgControl).val('');
                    propObject.errorMsg = '';
                    $($ValidationValueControl).val('');
                    propObject.multipleOf = '';
                    $($RegxControl).val('');
                    propObject.regularExp = '';
                    $($MinValueControl).val('');
                    propObject.minVal = '';
                    $($MaxValueControl).val('');
                    propObject.maxVal = '';

                    if (evnt.target.value > -1) {
                        $ValidationErrorMsgWrapper.show();
                        $ValidationErrorMsgControl.show();
                        if (evnt.target.value == "6") {
                            $ValidationValueWrapper.show();
                            $ValidationValueControl.show();
                            $RegxWrapper.hide();
                            $RegxControl.hide();
                            $MinValueWrapper.hide();
                            $MinValueControl.hide();
                            $MaxValueWrapper.hide();
                            $MaxValueControl.hide();

                            $MinLengthWrapper.hide();
                            $MinLengthControl.hide();
                            $MaxLengthWrapper.hide();
                            $MaxLengthControl.hide();
                        }
                        else if (evnt.target.value == "5") {
                            $RegxWrapper.show();
                            $RegxControl.show();
                            $ValidationValueWrapper.hide();
                            $ValidationValueControl.hide();
                            $MinValueWrapper.hide();
                            $MinValueControl.hide();
                            $MaxValueWrapper.hide();
                            $MaxValueControl.hide();

                            $MinLengthWrapper.hide();
                            $MinLengthControl.hide();
                            $MaxLengthWrapper.hide();
                            $MaxLengthControl.hide();
                        }
                        else if (evnt.target.value == "1" || evnt.target.value == "2") {
                            $ValidationValueWrapper.hide();
                            $ValidationValueControl.hide();
                            $RegxWrapper.hide();
                            $RegxControl.hide();
                            $MinValueWrapper.show();
                            $MinValueControl.show();
                            $MaxValueWrapper.show();
                            $MaxValueControl.show();

                            $MinLengthWrapper.hide();
                            $MinLengthControl.hide();
                            $MaxLengthWrapper.hide();
                            $MaxLengthControl.hide();
                        }
                        else if (evnt.target.value == "3" || evnt.target.value == "4") {
                            $ValidationValueWrapper.hide();
                            $ValidationValueControl.hide();
                            $RegxWrapper.hide();
                            $RegxControl.hide();
                            $MinValueWrapper.hide();
                            $MinValueControl.hide();
                            $MaxValueWrapper.hide();
                            $MaxValueControl.hide();

                            $MinLengthWrapper.show();
                            $MinLengthControl.show();
                            $MaxLengthWrapper.show();
                            $MaxLengthControl.show();
                        }
                        else {
                            $ValidationValueWrapper.hide();
                            $ValidationValueControl.hide();
                            $RegxWrapper.hide();
                            $RegxControl.hide();
                            $MinValueWrapper.hide();
                            $MinValueControl.hide();
                            $MaxValueWrapper.hide();
                            $MaxValueControl.hide();

                            $MinLengthWrapper.hide();
                            $MinLengthControl.hide();
                            $MaxLengthWrapper.hide();
                            $MaxLengthControl.hide();
                        }
                    }
                    else {
                        $ValidationErrorMsgWrapper.hide();
                        $ValidationErrorMsgControl.hide();
                        $ValidationValueWrapper.hide();
                        $ValidationValueControl.hide();
                        $RegxWrapper.hide();
                        $RegxControl.hide();
                        $MinValueWrapper.hide();
                        $MinValueControl.hide();
                        $MaxValueWrapper.hide();
                        $MaxValueControl.hide();


                        $MinLengthWrapper.hide();
                        $MinLengthControl.hide();
                        $MaxLengthWrapper.hide();
                        $MaxLengthControl.hide();
                    }

                },
                MinValue: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;
                    if (propObject.maxVal != undefined || propObject.maxVal != null) {
                        if (parseFloat(evnt.target.value) > parseFloat(propObject.maxVal)) {
                            if (propObject.minVal != undefined) evnt.target.value = propObject.minVal;
                            else evnt.target.value = "";
                            showMessage('Minimum value cannot be greater than maximum value.', DialogType.Error);
                            return;
                        }
                    }
                    propObject.minVal = evnt.target.value;
                },
                MaxValue: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;
                    if (propObject.minVal != undefined || propObject.minVal != null) {
                        if (parseFloat(evnt.target.value) < parseFloat(propObject.minVal)) {
                            if (propObject.maxVal != undefined) evnt.target.value = propObject.maxVal;
                            else evnt.target.value = "";
                            showMessage('Maximum value cannot be less than minimum value.', DialogType.Error);
                            return;
                        }
                    }
                    propObject.maxVal = evnt.target.value;
                },
                ValidateMinLength: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;
                    if (propObject.maxLeng != undefined || propObject.maxLeng != null) {
                        if (parseInt(evnt.target.value, 10) > parseInt(propObject.maxLeng, 10)) {
                            if (propObject.minLeng != undefined) evnt.target.value = propObject.minLeng;
                            else evnt.target.value = "";
                            showMessage('Minimum length cannot be greater than maximum length.', DialogType.Error);
                            return;
                        }
                    }
                    propObject.minLeng = evnt.target.value;
                },
                ValidateMaxLength: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;
                    if (propObject.minLeng != undefined || propObject.minLeng != null) {
                        if (parseInt(evnt.target.value, 10) < parseInt(propObject.minLeng, 10)) {
                            if (propObject.maxLeng != undefined) evnt.target.value = propObject.maxLeng;
                            else evnt.target.value = "";
                            showMessage('Maximum length cannot be less than minimum length.', DialogType.Error);
                            return;
                        }
                    }
                    propObject.maxLeng = evnt.target.value;
                },
                TxtValidationValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.multipleOf = evnt.target.value;
                },
                TxtValidationErrorMsg: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (propObject.validationType != "-1" && evnt.target.value.trim().length <= 0) {
                        showMessage('Please enter validation error message.', DialogType.Error);
                    }
                    else if (propObject.validationType != "-1" && evnt.target.value.trim().length > 100) {
                        showMessage('Validation error message should not be contains more than 100 characters.', DialogType.Error);
                    }
                    else {
                        propObject.errorMsg = evnt.target.value;
                    }

                },
                TxtValidationRegx: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.regularExp = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);

                    var scriptOnBlur = '', scriptOnChange = '', scriptOnFocus = '', script = '';

                    _jsAceEditorOnChang.setTextInEditor("");
                    var options = '<option value="onBlur">onBlur</option>'
                             + '<option value="onChange">onChange</option>'
                                + '<option value="onFocus">onFocus</option>';

                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onChange");
                    $.uniform.update('#drpCntrlEvents');
                    var prevSelectedVal = $('#drpCntrlEvents')[0].value;

                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
                    if (propObject.onBlur != null && propObject.onBlur != undefined) scriptOnBlur = propObject.onBlur;
                    if (propObject.onFocus != null && propObject.onFocus != undefined) scriptOnFocus = propObject.onFocus;
                    switch ($('#drpCntrlEvents')[0].value) {
                        case "onBlur":
                            if (scriptOnBlur != null && scriptOnBlur != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnBlur));
                            }
                            break;
                        case "onChange":
                            if (scriptOnChange != null && scriptOnChange != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                            }
                            break;
                        case "onFocus":
                            if (scriptOnFocus != null && scriptOnFocus != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnFocus));
                            }
                            break;
                    }
                    $('#drpCntrlEvents').unbind('change');
                    $('#drpCntrlEvents').bind('change', function () {
                        $.uniform.update('#drpCntrlEvents');
                        script = _jsAceEditorOnChang.getText();
                        switch (prevSelectedVal) {
                            case "onBlur":
                                if (script != null && script != undefined) {
                                    scriptOnBlur = Base64.encode(script);
                                }
                                break;
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onFocus":
                                if (script != null && script != undefined) {
                                    scriptOnFocus = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditorOnChang.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "onBlur":
                                if (scriptOnBlur != null && scriptOnBlur != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnBlur));
                                }
                                break;
                            case "onChange":
                                if (scriptOnChange != null && scriptOnChange != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                                }
                                break;
                            case "onFocus":
                                if (scriptOnFocus != null && scriptOnFocus != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnFocus));
                                }
                                break;
                        }
                    });

                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        bindAdvanceScripts(propObject, scriptOnBlur, scriptOnChange, scriptOnFocus);
                        return false;
                    });
//                    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//                    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//                        bindAdvanceScripts(propObject, scriptOnBlur, scriptOnChange, scriptOnFocus);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        scriptOnBlur = '', scriptOnChange = '', scriptOnFocus = ''
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay);
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getBindToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Data", "BindToProperty");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getMinValueCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "MinValue");
            }
            return objControl;
        }
        function _getMaxValueCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "MaxValue");
            }
            return objControl;
        }
        function _getMinLengthCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "ValidateMinLength");
            }
            return objControl;
        }
        function _getMaxLengthCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "ValidateMaxLength");
            }
            return objControl;
        }
        function _getValidationValueCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "Value");
            }
            return objControl;
        }
        function _getRegxCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "Regx");
            }
            return objControl;
        }
        function _getValidationErrorMsgCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Validation", "ValidationErrorMsg");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }

        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getBindToPropertyCntrl: _getBindToPropertyCntrl,
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Validation: {
                getMinValueCntrl: _getMinValueCntrl,
                getMaxValueCntrl: _getMaxValueCntrl,
                getMinLengthCntrl: _getMinLengthCntrl,
                getMaxLengthCntrl: _getMaxLengthCntrl,
                getValidationValueCntrl: _getValidationValueCntrl,
                getRegxCntrl: _getRegxCntrl,
                getValidationErrorMsgCntrl: _getValidationErrorMsgCntrl

            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Textbox = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Textbox",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtLabelTextChange,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtLabelTextChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtFontStyle,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Placeholder",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Placeholder",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtPlaceholderValChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtPlaceholderValChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "No. of Lines",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "NoofLines",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtNoOfLinesChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtNoOfLinesChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtValidateKeyPressed,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Max Length",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxLength",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtMaxLengthChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtMaxLengthChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtValidateKeyPressed,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "50",
                    "hidden": false
                }
            },
            {
                "text": "Min Width (px)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinWidth",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtMinWidthChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtMinWidthChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtValidateKeyPressed,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "280",
                    "hidden": false
                }
            },
            {
                "text": "Width",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Width",
                "defltValue": "3",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtWidthChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "25%",
                      "value": "0"
                  },
                  {
                      "text": "50%",
                      "value": "1"
                  },
                  {
                      "text": "75%",
                      "value": "2"
                  },
                  {
                      "text": "100%",
                      "value": "3"
                  }
               ]
            },
            {
                "text": "Mask",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Mask",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtMaskChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
             {
                 "text": "Size",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Size",
                 "defltValue": "0",
                 "validations": [
                     ],
                 "customValidations": [

                        ],
                 "events": [
                           {
                               "name": "change",
                               "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.Size,
                               "context": "",
                               "arguments": {
                                   "cntrls": [
                                 ]
                               }
                           }
                        ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                     {
                         "text": "Normal",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
                     },
                     {
                         "text": "Mini",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
                     }
                   ]
             },
             {
                 "text": "Inline",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Inline",
                 "defltValue": "0",
                 "validations": [

               ],
                 "customValidations": [

               ],
                 "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.TxtInlineChanged,
                      "context": "",
                      "arguments": {
                          "cntrls": [{
                              grpPrefText: "Appearance",
                              cntrlPrefText: "Icon",
                              rtrnPropNm: "Icon"
                          }]
                      }
                  }
               ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
             },
            {
                "text": "Icon",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Icon",
                "defltValue": "",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.Icon,
                    "context": "",
                    "arguments": {
                    }
                }],
                "CntrlProp": "",
                "helpText": {},
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": []
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindToProperty",
                  "defltValue": "0",
                  "validations": [

                   ],
                  "customValidations": [

                   ],
                  "events": [
                      {
                          "name": "change",
                          "func": PROP_JSON_HTML_MAP.Textbox.groups.Data.eventHandlers.BindToProperty,
                          "context": "",
                          "arguments": {
                              "cntrls": [
                            ]
                          }
                      }
                   ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                   ]
              },
              {
                  "text": "Databinding",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Databinding",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
              },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                "text": "Default Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "DefaultValue",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Data.eventHandlers.DefaultValChange,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Data.eventHandlers.DefaultValChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Validation",
          "prefixText": "Validation",
          "hidden": false,
          "properties": [
              {
                  "text": "Required",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Required",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
              },
            {
                "text": "Validation Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ValidationType",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.TxtValidation,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "MinValue",
                               rtrnPropNm: "MinValue"
                           },
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "MaxValue",
                               rtrnPropNm: "MaxValue"
                           },
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "ValidateMinLength",
                               rtrnPropNm: "ValidateMinLength"
                           },
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "ValidateMaxLength",
                               rtrnPropNm: "ValidateMaxLength"
                           },
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "Value",
                               rtrnPropNm: "Value"
                           },
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "ValidationErrorMsg",
                               rtrnPropNm: "ValidationErrorMsg"
                           },
                           {
                               grpPrefText: "Validation",
                               cntrlPrefText: "Regx",
                               rtrnPropNm: "Regx"
                           }
                        ]
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "None",
                      "value": "-1"
                  },
                  {
                      "text": "Email",
                      "value": "0"
                  },
                  {
                      "text": "Integer",
                      "value": "1"
                  },
                  {
                      "text": "Decimal",
                      "value": "2"
                  },
                  {
                      "text": "Alphanumeric",
                      "value": "3"
                  },
                  {
                      "text": "Alphabets",
                      "value": "4"
                  },
                  {
                      "text": "CustomRegex",
                      "value": "5"
                  },
                  {
                      "text": "Multiple of",
                      "value": "6"
                  }
               ]
            },
            {
                "text": "Min Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinValue",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.MinValue,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.MinValue,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Max Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxValue",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.MaxValue,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.MaxValue,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Min Length",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "ValidateMinLength",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.ValidateMinLength,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Max Length",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "ValidateMaxLength",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.ValidateMaxLength,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Value",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.TxtValidationValue,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                },
                "options": [
               ]
            },
            {
                "text": "Regx",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Regx",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.TxtValidationRegx,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                },
                "options": [
               ]
            },
            {
                "text": "Validation Error Msg.",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "ValidationErrorMsg",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Validation.eventHandlers.TxtValidationErrorMsg,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "maxlength": 150,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                },
                "options": [
               ]
            }
          ]
      },
      {
          "name": "Advance",
          "prefixText": "Advance",
          "hidden": false,
          "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Textbox.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Textbox(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Textbox;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.placeholder = opts.placeholder;
    this.txtRowsCount = opts.txtRowsCount;
    this.maxLength = opts.maxLength;
    this.minWidth = opts.minWidth;
    this.widthInPercent = opts.widthInPercent;
    this.maskValue = opts.maskValue;
    this.dfltValue = opts.dfltValue;
    this.displayText = opts.displayText;
    this.required = opts.required;
    this.validationType = opts.validationType;
    this.minVal = opts.minVal;
    this.maxVal = opts.maxVal;
    this.minLeng = opts.minLeng;
    this.maxLeng = opts.maxLeng;
    this.multipleOf = opts.multipleOf;
    this.errorMsg = opts.errorMsg;
    this.regularExp = opts.regularExp;
    this.bindToProperty = opts.bindToProperty;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.dataMini = opts.dataMini;
    this.onChange = opts.onChange;
    this.onBlur = opts.onBlur;
    this.onFocus = opts.onFocus;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.inline = opts.inline;
    this.icon = opts.icon;
}

Textbox.prototype = new ControlNew();
Textbox.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox;
Textbox.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
Textbox.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Label Text";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    this.widthInPercent = "0";
    this.maxLength = "50";
    this.minWidth = "280";
    this.widthInPercent = "3";
    this.maskValue = "0";
    this.required = "1";
    this.validationType = "-1";
    this.conditionalDisplay = "1";
    this.fontStyle = "0";
    this.inline = "0";
    this.isDeleted = false;
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
};
Textbox.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Textbox.prototype.fnGetIcon = function () {
    return this.icon;
};
Textbox.prototype.fnSetIcon = function (value) {
    this.icon = value;
};
Textbox.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
Textbox.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}

Textbox.prototype.fnResetObjectDetails = function () {
    if (mfUtil.isNullOrUndefined(this.fnGetDataMini())) {
        this.fnSetDataMini(MF_IDE_CONSTANTS.CNTRL_SIZES.normal);
    }
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
Textbox.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}

Textbox.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
Textbox.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
Textbox.prototype.fnGetDisplayText = function () {
    return this.displayText;
};
Textbox.prototype.fnSetDisplayText = function (value) {
    this.displayText = value;
};
Textbox.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Textbox.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
Textbox.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};

function getInlineTextboxHtml(propObject) {
    var strHtml = '';
    if (propObject) {
        var idCount = propObject.id.split('_')[1];
        if (propObject.inline === "1") {
            strHtml = '<div id="textboxLabelOuterDiv_' + idCount + '" class="node Ctrl_Width96 clearfix" style="border-bottom:1px solid black;min-height:25px;" >'
                            + '<div id="TXT_' + idCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node">'
                                + '<img id="TXT_' + idCount + '_Img" src=""/>'
                            + '</div>'
                           + '<div class="clear hide"></div>'
                            + '<div id="TXT_' + idCount + '_Label" class="node" style="float:left;padding-top:8px;padding-right:6px;min-width:10px;max-width:80px; overflow:hidden; white-space: nowrap; -ms-text-overflow: ellipsis;">Label Text'
                            + '</div>'
                            + '<div id="textboxDiv_' + idCount + '" style="float:left;padding-top:7px;"class="node">'
                                + '<input id="TXT_' + idCount + '" type="text" style="border:0;white-space: nowrap; overflow:hidden; -ms-text-overflow: ellipsis;" onkeydown=\"return enterKeyFilter(event);\" readonly/>'
                            + '</div>'
                        + '</div>';
        }
        else {
            strHtml = '<div id="textboxLabelOuterDiv_' + idCount + '" class="node">'
                            + '<div id="TXT_' + idCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node hide">'
                                + '<img id="TXT_' + idCount + '_Img" src=""/>'
                            + '</div>'
                            + '<div id="TXT_' + idCount + '_Label" class="node" style="white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;" >Label Text'
                            + '</div>'
                           + '<div class="clear"></div>'
                            + '<div id="textboxDiv_' + idCount + '" class="node Ctrl_Width96 IdeUi-input-text IdeUi-shadow-inset IdeUi-corner-all IdeUi-btn-shadow IdeUi-body-c IdeUi-input-has-clear">'
                                + '<input id="TXT_' + idCount + '" type="text" style="white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;" class="IdeUi-input-text IdeUi-body-c IdeUi-input" onkeydown=\"return enterKeyFilter(event);\" readonly/>'
                            + '</div>'
                        + '</div>'
        }

        $('#outerDiv' + idCount).html('');
        $('#outerDiv' + idCount).append(strHtml);

        if (propObject.labelText != undefined && propObject.labelText != null && propObject.labelText.length > 0)
            $('#' + propObject.id + '_Label').html(propObject.labelText);
        else
            $('#' + propObject.id + '_Label').html('');

        $('#' + propObject.id + '_Label').css('color', propObject.fontColor + ' !important');
        setTextboxSize(propObject);
        setTextboxPlaceHolder(propObject);
        setTextboxIcon(propObject);
        setTextboxStyle(propObject);
        setTextboxLabel(propObject);
    }
}

function setTextboxStyle(propObject) {
    var lblStyle = getTextFontStyle(propObject);
    $('#' + propObject.id + '_Label').removeClass();
    $('#' + propObject.id + '_Label').addClass(lblStyle + ' node');
}

function setTextboxLabel(propObject) {
    if (propObject.labelText != undefined && propObject.labelText != null && propObject.labelText.length > 0) {
        $('#' + propObject.id + '_Label').html(propObject.labelText);
        if (propObject.inline && propObject.inline === "0") $('#' + propObject.id + '_Label').css('margin-bottom', '');
    }
    else {
        $('#' + propObject.id + '_Label').html("");
        if (propObject.inline && propObject.inline === "0") $('#' + propObject.id + '_Label').css('margin-bottom', '-4px');
    }
}

function setTextboxIcon(propObject) {
    if (propObject.inline && propObject.inline === "1") {
        var iconName = propObject.fnGetIcon();
        var iconPath = MF_HELPERS.jqueryMobileIconsHelper.getCompleteIconPath(iconName);
        var iconNameSuffix = "-black.png";
        if (iconName != undefined && iconName != "0" && parseInt(iconPath.length) > 0 && iconPath != undefined) {
            $('#' + propObject.id + '_ImgDiv').removeClass('hide');
            $('#' + propObject.id + '_Img').attr("src", iconPath + iconNameSuffix);
        }
        else
            $('#' + propObject.id + '_ImgDiv').addClass('hide');
    }
    else
        $('#' + propObject.id + '_ImgDiv').addClass('hide');
}

function setTextboxSize(propObject) {
    var size = propObject.fnGetDataMini();
    var $TextboxControl = $('#' + propObject.id);
    if (size === "0") {
        if (propObject.inline != "1") $TextboxControl.css("height", "27px");
        $TextboxControl.css("font-size", "12.5px");
        $('#' + propObject.id + '_Label').css("font-size", "12.5px");
    }
    else if (size === "1") {
        if (propObject.inline != "1") $TextboxControl.css("height", "20px");
        $TextboxControl.css("font-size", "11px");
        $('#' + propObject.id + '_Label').css("font-size", "11px");
    }
}

function setTextboxPlaceHolder(propObject) {
    var $TextboxValueProperty = $('#' + propObject.id);
    if (propObject.dfltValue != undefined && propObject.dfltValue != null && propObject.dfltValue.length > 0) {
        $($TextboxValueProperty).css('color', 'rgb(0, 0, 0)');
        $($TextboxValueProperty).val(propObject.dfltValue);
    }
    else {
        if (propObject.placeholder != undefined && propObject.placeholder != null) {
            $($TextboxValueProperty).css('color', 'rgb(154, 154, 154)');
            $($TextboxValueProperty).val(propObject.placeholder);
        }
    }

}

function bindAdvanceScripts(propObject, scriptOnBlur, scriptOnChange, scriptOnFocus) {
    var script = _jsAceEditorOnChang.getText();
    switch ($('#drpCntrlEvents')[0].value) {
        case "onBlur":
            if (script != null && script != undefined) {
                scriptOnBlur = Base64.encode(script);
            }
            break;
        case "onChange":
            if (script != null && script != undefined) {
                scriptOnChange = Base64.encode(script);
            }
            break;
        case "onFocus":
            if (script != null && script != undefined) {
                scriptOnFocus = Base64.encode(script);
            }
            break;
    }

    if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = scriptOnChange;
    if (scriptOnBlur != null && scriptOnBlur != undefined) propObject.onBlur = scriptOnBlur;
    if (scriptOnFocus != null && scriptOnFocus != undefined) propObject.onFocus = scriptOnFocus;
}
/*---------------------------------------------------Label---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Alignment: function (control, appObj) {
                $(control).val(appObj.fontAlign);
            },
            FontSize: function (control, appObj) {
                $(control).val(appObj.fontSize);
                setTitleAndTextCss(appObj);
            },
            FontStyle: function (control, appObj) {
                $(control).val(appObj.fontStyle);
                setTitleAndTextCss(appObj);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            BackgroundColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Appearance.getBackgroundColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_LabelDiv');
                if (appObj.backgroundColor != undefined || appObj.backgroundColor != null) {
                    $(currentCntrl.control).val(appObj.backgroundColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.backgroundColor);
                    $($LabelValueProperty[0]).css('background-color', '#' + appObj.backgroundColor);
                }
                // else {
                // $(currentCntrl.browseBtn).css('background-color', '#ffffff');
                // $($LabelValueProperty[0]).css('background-color', '#ffffff');
                //}

                var bckColor = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    color: '',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            bckColor = '#' + hex;
                        }

                        // if ($(currentCntrl.control).val() == "" || $(currentCntrl.control).val() == undefined)
                        // bckColor = '#ffffff';
                        $(currentCntrl.control).val(bckColor);
                        $(currentCntrl.browseBtn).css('background-color', bckColor);
                        appObj.backgroundColor = $(currentCntrl.control).val();
                        if (bckColor.length > 0) $($LabelValueProperty[0]).css('background-color', bckColor);
                        else $($LabelValueProperty[0]).css('background-color', '#ffffff');
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.backgroundColor = evnt.target.value;
                    bckColor = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.backgroundColor = evnt.target.value;
                    bckColor = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.backgroundColor = evnt.target.value;
                    bckColor = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            BorderColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Appearance.getBorderColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_LabelDiv');
                if (appObj.borderColor != undefined || appObj.borderColor != null) {
                    $(currentCntrl.control).val(appObj.borderColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.borderColor);
                    $($LabelValueProperty[0]).css('border-color', appObj.borderColor);
                }
                else {
                    $(currentCntrl.browseBtn).css('background-color', '#000000');
                    $($LabelValueProperty[0]).css('border-color', '#000000');
                }

                var borderCol = '';
                var borderColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            borderCol = '#' + hex;
                        }

                        if ($(currentCntrl.control).val() == "" || $(currentCntrl.control).val() == undefined)
                            borderCol = '#000000';

                        $(currentCntrl.control).val(borderCol);
                        $(currentCntrl.browseBtn).css('background-color', borderCol);
                        appObj.borderColor = $(currentCntrl.control).val();
                        setContainerBorder(appObj);
                        $($LabelValueProperty[0]).css('border-color', borderCol);
                    }
                })
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.borderColor = evnt.target.value;
                    borderCol = evnt.target.value;
                    $(borderColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.borderColor = evnt.target.value;
                    borderCol = evnt.target.value;
                    $(borderColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.borderColor = evnt.target.value;
                    borderCol = evnt.target.value;
                    $(borderColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Border: function (control, appObj) {
                $(control).val(appObj.border);
                setContainerBorder(appObj);
                if (appObj.border != "0") {
                    var $LabelValueProperty = $('#' + appObj.id + '_LabelDiv');
                    $($LabelValueProperty[0]).css('border-width', appObj.border + "px");
                }
            },
            WordWrap: function (control, appObj) {
                $(control).val(appObj.wrap);
                setTitleAndTextCss(appObj);
            },
            HideifValueBlank: function (control, appObj) {
                $(control).val(appObj.hideIfBlank)
            },
            Underline: function (control, appObj) {
                $(control).val(appObj.underline);
                setTitleAndTextCss(appObj);
            },
            Title: function (control, appObj) {
                $(control).val(appObj.title);
                if (appObj.title != undefined && appObj.title != null && appObj.title.length > 0) {
                    $('#' + appObj.id + '_Title').html(appObj.title);
                    $('#' + appObj.id + '_LabelDiv').css('margin-top', '6px');
                }
                else {
                    $('#' + appObj.id + '_Title').html("");
                    $('#' + appObj.id + '_LabelDiv').css('margin-top', '');
                }

            },
            TitlePosition: function (control, appObj) {
                $(control).val(appObj.titlePosition);
                setTitleAndTextCss(appObj);
            },
            TitleFontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Appearance.getTitleFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelTitleProperty = $('#' + appObj.id + '_Title');
                if (appObj.fontColor != undefined || appObj.titleColor != null) {
                    $(currentCntrl.control).val(appObj.titleColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.titleColor);
                    $($LabelTitleProperty[0]).css('color', '#' + appObj.titleColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.titleColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelTitleProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelTitleProperty[0]).css('color', fontCol + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.titleColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.titleColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.titleColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            eventHandlers: {
                Alignment: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontAlign = evnt.target.value;
                    setTitleAndTextCss(propObject);
                },
                LblFontSize: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.fontSize = evnt.target.value;
                    setTitleAndTextCss(propObject);
                },
                LblFontStyle: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    setTitleAndTextCss(propObject);
                },
                LblFontColor: function (evnt) {
                    var propObject = evnt.data.propObject;
                },
                LblBackgroundColor: function (evnt) {
                    var propObject = evnt.data.propObject;
                    setContainerBorder(propObject);
                },
                LblBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    setContainerBorder(propObject);
                    if (propObject.border != "0") {
                        var $LabelValueProperty = $('#' + propObject.id + '_LabelDiv');
                        $($LabelValueProperty[0]).css('border-width', propObject.border + "px");
                    }
                },
                LblBorderColor: function (evnt) {
                    var propObject = evnt.data.propObject;
                },
                LblWordWrap: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.wrap = evnt.target.value;
                    setTitleAndTextCss(propObject);
                },
                LblUnderline: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.underline = evnt.target.value;
                    setTitleAndTextCss(propObject);
                },
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.title = evnt.target.value;

                    if (evnt.target.value.length > 0) {
                        $('#' + propObject.id + '_Title').html(evnt.target.value);
                        $('#' + propObject.id + '_LabelDiv').css('margin-top', '6px');
                    }
                    else {
                        $('#' + propObject.id + '_Title').html("");
                        $('#' + propObject.id + '_LabelDiv').css('margin-top', '');
                    }
                },
                LblTitlePosition: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.titlePosition = evnt.target.value;
                    setTitleAndTextCss(propObject);
                },
                HideifValueBlank: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.hideIfBlank = evnt.target.value;
                }
            }
        },
        Data: {
            BindToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Data.getBindToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            DataBinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            Text: function (control, appObj, options) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var text = appObj.text;
                if (text) {
                    $(control).val(text);

                    if (appObj.text != undefined && appObj.text != null && appObj.text.length > 0)
                        $('#' + appObj.id + '_Label').html(appObj.text.replace(/\n\r?/g, '<br />'));
                    else
                        $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");

                    if (text && text.length > 25) {
                        $textAreaLabel.val(text.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(text);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                    $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
                }
            },
            ParameterFormats: function (control, appObj) {

            },
            eventHandlers: {
                BindToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Data.getBindToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty, propObject))
                        propObject.bindToProperty = evnt.target.value;
                    else {
                        if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                changeLabelObjectBinding(propObject);
                                //propObject.labelDataObjs = [];
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        changeLabelObjectBinding(propObject);
                        //propObject.labelDataObjs = [];
                        $($DataObjectControl).val("Select Object");
                    }
                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:

                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.LABEL,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 680, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                },
                LblText: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.text = evnt.target.value;
                    var $LabelValueProperty = $('#' + propObject.id + '_Label');
                    if (evnt.target.value.length > 0)
                        $($LabelValueProperty).html(evnt.target.value.replace(/\n\r?/g, '<br />'));
                    else
                        $($LabelValueProperty).html("&nbsp;&nbsp;&nbsp;&nbsp;");
                },
                ParameterFormatsChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    initializeLabelControlFormat(propObject);
                    showModalPopUpWithOutHeader('SubProcLabelCtrlFormat', 'Parameter Formats', 460, false);
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0") $(currentCntrl.wrapperDiv).show();
                else $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getBorderColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Appearance", "BorderColor");
            }
            return objControl;
        }
        function _getBackgroundColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Appearance", "BackgroundColor");
            }
            return objControl;
        }
        function _getTitleFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Appearance", "TitleFontColor");
            }
            return objControl;
        }
        function _getBindToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Data", "BindToProperty");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LABEL.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }

        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl,
                getBorderColorCntrl: _getBorderColorCntrl,
                getBackgroundColorCntrl: _getBackgroundColorCntrl,
                getTitleFontColorCntrl: _getTitleFontColorCntrl
            },
            Data: {
                getBindToPropertyCntrl: _getBindToPropertyCntrl,
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};


mFicientIde.PropertySheetJson.Label = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Label",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Label.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Label.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Alignment",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Alignment",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.Alignment,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Left",
                      "value": "0"
                  },
                  {
                      "text": "Center",
                      "value": "1"
                  },
                  {
                      "text": "Right",
                      "value": "2"
                  }
               ]
            },
            {
                "text": "Font Size",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontSize",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblFontSize,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Small",
                      "value": "0"
                  },
                  {
                      "text": "Medium",
                      "value": "1"
                  },
                  {
                      "text": "Large",
                      "value": "2"
                  }
               ]
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblFontStyle,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblFontColor,
                      "context": "",
                      "arguments": {}
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblFontColor,
                      "context": "",
                      "arguments": {}
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Background Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "BackgroundColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblBackgroundColor,
                      "context": "",
                      "arguments": {}
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Border",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Border",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblBorder,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "0px",
                      "value": "0"
                  },
                  {
                      "text": "1px",
                      "value": "1"
                  },
                  {
                      "text": "2px",
                      "value": "2"
                  },
                  {
                      "text": "3px",
                      "value": "3"
                  },
                  {
                      "text": "4px",
                      "value": "4"
                  },
                  {
                      "text": "5px",
                      "value": "5"
                  }
               ]
              },
              {
                  "text": "Border Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "BorderColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblBorderColor,
                      "context": "",
                      "arguments": {}
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Word Wrap",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "WordWrap",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblWordWrap,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
              },
              {
                  "text": "Underline",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Underline",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblUnderline,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
              },
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Title Position",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "TitlePosition",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.LblTitlePosition,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Top",
                      "value": "0"
                  },
                  {
                      "text": "Left",
                      "value": "1"
                  }
               ]
            },
              {
                  "text": "Title Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "TitleFontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Hide if Value Blank",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "HideifValueBlank",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Appearance.eventHandlers.HideifValueBlank,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
              }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Label.groups.Data.eventHandlers.BindToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
              {
                  "text": "Text Template",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                  "prefixText": "Text",
                  "noOfLines": "false",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Data.eventHandlers.LblText,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Label Text",
                      "hidden": false
                  }
              },
              {
                  "text": "Parameter Formats",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "ParameterFormats",
                  "disabled": false,
                  "hidden": false,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Data.eventHandlers.ParameterFormatsChange,
                      "context": "",
                      "arguments": {}
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Define Formats",
                      "hidden": false
                  }
              },
            {
                "text": "Data Binding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "DataBinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Label.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};

function Label(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Label;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.text = opts.text;
    this.fontAlign = opts.fontAlign;
    this.fontSize = opts.fontSize;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.backgroundColor = opts.backgroundColor;
    this.border = opts.border;
    this.borderColor = opts.borderColor;
    this.wrap = opts.wrap;
    this.underline = opts.underline;
    this.titlePosition = opts.titlePosition;
    this.title = opts.title;
    this.titleColor = opts.titleColor;
    this.hideIfBlank = opts.hideIfBlank;
    this.labelDataObjs = opts.labelDataObjs;
    this.databindObjs = opts.databindObjs;
    this.bindToProperty = opts.bindToProperty;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
}

Label.prototype = new ControlNew();
Label.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Label;
Label.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
Label.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.text = "Label Text";
    this.fontAlign = "0";
    this.fontSize = "1";
    this.fontStyle = "0";
    this.wrap = "1";
    this.border = "0";
    this.underline = "0";
    this.titlePosition = "0";
    this.hideIfBlank = "0";
    this.conditionalDisplay = "1";
    this.isDeleted = false;
    this.title = "Title";
    this.fnSetDefaultLabelDataObjs();
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
};
Label.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Label.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};

//shalini Reset Function
Label.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
    var labelDataObjs = this.labelDataObjs;
    this.labelDataObjs = [];
    i = 0;
    if (labelDataObjs && $.isArray(labelDataObjs) && labelDataObjs.length > 0) {
        for (i = 0; i <= labelDataObjs.length - 1; i++) {
            var objlabelDataObj = new LabelDataObj(labelDataObjs[i]);
            this.fnAddLabelDataObj(objlabelDataObj);
        }
    }
}
Label.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
Label.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}

Label.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
Label.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
Label.prototype.fnSetDefaultLabelDataObjs = function () {
    if (!this.labelDataObjs || !$.isArray(this.labelDataObjs)) {
        this.labelDataObjs = [];
    }
    var labelDataObj = new LabelDataObj();
    for (var i = 1; i <= 5; i++) {
        labelDataObj = new LabelDataObj();
        labelDataObj.para = i + "%";
        labelDataObj.dataType = "0";
        labelDataObj.noOfDecimalPlaces = "0";
        labelDataObj.mulFactor = "1";
        labelDataObj.selectedCol = "-1";

        this.fnAddLabelDataObj(labelDataObj);
    }
}
Label.prototype.fnAddLabelDataObjs = function (labelDataObjs/*LabelDataObj object array*/) {
    if (!this.labelDataObjs || !$.isArray(this.labelDataObjs)) {
        this.labelDataObjs = [];
    }
    if ($.isArray(labelDataObjs)) {
        var self = this;
        $.each(labelDataObjs, function (index, value) {
            if (value && value instanceof LabelDataObj) {
                self.labelDataObjs.push(value);
            }
        });
    }
}
Label.prototype.fnAddLabelDataObj = function (labelDataObj/*LabelDataObj object*/) {
    if (!this.labelDataObjs || !$.isArray(this.labelDataObjs)) {
        this.labelDataObjs = [];
    }
    if (labelDataObj && labelDataObj instanceof LabelDataObj) {
        this.labelDataObjs.push(labelDataObj);
    }
}
Label.prototype.fnDeleteLabelDataObj = function (para) {
    var aryNewLabelDataOObjs = $.grep(this.labelDataObjs, function (labelDataObj) {
        if (labelDataObj instanceof LabelDataObj) {
            return labelDataObj.para !== para;
        }
    });
    this.labelDataObjs = aryNewLabelDataOObjs;
}
Label.prototype.fnClearAllLabelDataObjs = function (labelDataObj/*LabelDataObj object*/) {
    this.labelDataObjs = [];
}
Label.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Label.prototype.fnGetLabelDataObjs = function () {
    return this.labelDataObjs;
};
function LabelDataObj(opts) {
    if (!opts) opts = {};
    this.para = opts.para;
    this.dataType = opts.dataType;
    this.dataFormat = opts.dataFormat;
    this.noOfDecimalPlaces = opts.noOfDecimalPlaces;
    this.mulFactor = opts.mulFactor;
    this.selectedCol = opts.selectedCol;
}

LabelDataObj.prototype = new LabelDataObj();

function setTitleAndTextCss(lblObj) {
    var titleCss = "node " + getTitlePosition(lblObj) + " " + getTitleFontSize(lblObj);
    var textCss = "node " + getTextAlignment(lblObj) + " " + getTextFontSize(lblObj) + " " + getTextFontStyle(lblObj) + " " + getTextUnderline(lblObj) + " " + getTextWordWrap(lblObj);

    $('#' + lblObj.id + '_Title').removeClass();
    $('#' + lblObj.id + '_Title').addClass(titleCss);
    $('#' + lblObj.id + '_Label').removeClass();
    $('#' + lblObj.id + '_Label').addClass(textCss);

}

function getTitlePosition(lblObj) {
    var styleClass = "";
    if (lblObj.titlePosition == "0") {
        switch (lblObj.fontAlign) {
            case "0":
                styleClass = "title-ttl-top-txt-left";
                break;
            case "1":
                styleClass = "title-ttl-top-txt-center";
                break;
            case "2":
                styleClass = "title-ttl-top-txt-right";
                break;
        }
    }
    else if (lblObj.titlePosition == "1") {
        switch (lblObj.fontAlign) {
            case "0":
                styleClass = "title-ttl-left-txt-left";
                break;
            case "1":
                styleClass = "title-ttl-left-txt-center";
                break;
            case "2":
                styleClass = "title-ttl-left-txt-right";
                break;

        }
    }

    return styleClass;
}

function getTextAlignment(lblObj) {
    var styleClass = "";
    switch (lblObj.fontAlign) {
        case "0":
            if (lblObj.titlePosition == "0") {
                styleClass = "text-ttl-top-txt-left";
            }
            else if (lblObj.titlePosition == "1") {
                styleClass = "text-ttl-left-txt-left";
            }
            break;
        case "1":
            if (lblObj.titlePosition == "0") {
                styleClass = "text-ttl-top-txt-center";
            }
            else if (lblObj.titlePosition == "1") {
                styleClass = "text-ttl-left-txt-center";
            }
            break;
        case "2":
            if (lblObj.titlePosition == "0") {
                styleClass = "text-ttl-top-txt-right";
            }
            else if (lblObj.titlePosition == "1") {
                styleClass = "text-ttl-left-txt-right";
            }

    }
    return styleClass;
}

function getTitleFontSize(lblObj) {
    var styleClass = "";
    switch (lblObj.fontSize) {
        case "0":
            styleClass = "title-font-small";
            break;
        case "1":
            styleClass = "title-font-normal";
            break;
        case "2":
            styleClass = "title-font-large";
            break;
    }
    return styleClass;
}

function getTextFontSize(lblObj) {
    var styleClass = "";
    switch (lblObj.fontSize) {
        case "0":
            styleClass = "text-font-small";
            break;
        case "1":
            styleClass = "text-font-normal";
            break;
        case "2":
            styleClass = "text-font-large";
            break;
    }
    return styleClass;
}

function getTextFontStyle(lblObj) {
    var styleClass = "";
    switch (lblObj.fontStyle) {
        case "0":
            styleClass = "text-style-normal";
            break;
        case "1":
            styleClass = "text-style-bold";
            break;
        case "2":
            styleClass = "text-style-italic";
            break;
        case "3":
            styleClass = "text-style-bold-italic";
            break;
    }
    return styleClass;
}

function getTextUnderline(lblObj) {
    var styleClass = "";
    switch (lblObj.underline) {
        case "0":
            styleClass = "";
            break;
        case "1":
            styleClass = "text-style-underline";
            break;
    }
    return styleClass;
}

function getTextWordWrap(lblObj) {
    var styleClass = "";
    switch (lblObj.wrap) {
        case "0":
            styleClass = "text-nowrap";
            break;
        case "1":
            styleClass = "text-wrap";
            break;
    }
    return styleClass;
}

function setContainerBorder(lblObj) {
    var containerCss = "";
    if (lblObj.border != "0") {
        containerCss = "container-border";
    }
    else if (lblObj.border == "0" && (lblObj.backgroundColor != undefined && lblObj.backgroundColor.trim().length > 0)) {
        containerCss = "container";
    }
    else if (lblObj.border == "0" && (lblObj.backgroundColor == undefined || lblObj.backgroundColor.trim().length <= 0)) {
        containerCss = "container-none";
    }
    $('#' + lblObj.id + '_LabelDiv').removeClass();
    $('#' + lblObj.id + '_LabelDiv').addClass("node " + containerCss);
}


/*--------------------------------------------------------------------Seperator-------------------------------------------------------------------------------*/
mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Separator = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                }
            }
        },
        Appearance: {
            Inset: function (control, appObj) {
                $(control).val(appObj.inset);
                if (appObj.inset == "0") {
                    $('#SeparatorDiv_' + appObj.id.split('_')[1]).css('width', '');
                    $('#SeparatorDiv_' + appObj.id.split('_')[1]).css('margin-left', '-8px');
                }
                else {
                    $('#SeparatorDiv_' + appObj.id.split('_')[1]).css('width', '98%');
                    $('#SeparatorDiv_' + appObj.id.split('_')[1]).css('margin-left', '');
                }
            },
            Thickness: function (control, cntrlObj) {
                var strThickness = cntrlObj.fnGetThickness();

                if (!mfUtil.isNullOrUndefined(strThickness)
                && !mfUtil.isEmptyString(strThickness)) {

                    $(control).val(strThickness);
                    switch (strThickness) {
                        case "1":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-width', '1px');
                            break;
                        case "2":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-width', '2px');
                            break;
                        case "3":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-width', '3px');
                            break;
                        case "4":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-width', '4px');
                            break;
                        case "5":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-width', '5px');
                            break;
                    }
                }
                else {
                    $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-width', '1px');
                }
            },
            BorderStyle: function (control, cntrlObj) {
                var strBorderStyle = cntrlObj.fnGetBorderStyle();

                if (!mfUtil.isNullOrUndefined(strBorderStyle)
                && !mfUtil.isEmptyString(strBorderStyle)) {

                    $(control).val(strBorderStyle);

                    switch (strBorderStyle) {
                        case "0":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-style', 'solid');
                            break;
                        case "1":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-style', 'dashed');
                            break;
                        case "2":
                            $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-style', 'dotted');
                            break;
                    }
                }
                else {
                    $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('border-bottom-style', 'solid');
                }
            },
            TopMargin: function (control, cntrlObj) {
                $(control).val(cntrlObj.topMargin);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(event);
                });
                $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('margin-top', cntrlObj.topMargin + 'px');
            },
            BottomMargin: function (control, cntrlObj) {
                $(control).val(cntrlObj.bottomMargin);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(event);
                });
                $('#SeparatorDiv_' + cntrlObj.id.split('_')[1]).css('margin-bottom', cntrlObj.bottomMargin + 'px');
            },
            eventHandlers: {
                Inset: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.inset = evnt.target.value;
                    if (evnt.target.value == "0") {
                        $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('width', '');
                        $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('margin-left', '-8px');
                    }
                    else {
                        $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('width', '98%');
                        $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('margin-left', '');
                    }
                },
                TopMargin: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.topMargin = evnt.target.value;
                    $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('margin-top', propObject.topMargin + 'px');
                },
                BottomMargin: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.bottomMargin = evnt.target.value;
                    $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('margin-bottom', propObject.bottomMargin + 'px');
                },
                Thickness: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.fnSetThickness($self.val());
                    switch ($self.val()) {
                        case "1":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-width', '1px');
                            break;
                        case "2":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-width', '2px');
                            break;
                        case "3":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-width', '3px');
                            break;
                        case "4":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-width', '4px');
                            break;
                        case "5":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-width', '5px');
                            break;
                    }
                },
                BorderStyle: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    //propObject.inset = evnt.target.value;
                    propObject.fnSetBorderStyle($self.val());
                    switch ($self.val()) {
                        case "0":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-style', 'solid');
                            break;
                        case "1":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-style', 'dashed');
                            break;
                        case "2":
                            $('#SeparatorDiv_' + propObject.id.split('_')[1]).css('border-bottom-style', 'dotted');
                            break;
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, cntrlObj) {
                if (cntrlObj.conditionalDisplay) {
                    $(control).val(cntrlObj.conditionalDisplay);
                }
            },
            Conditions: function (control, cntrlObj, options) {
                //var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Behaviour.getConditionsCntrl();
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.conditionalDisplay == "0")
                    currentCntrl.wrapperDiv.show();
                else
                    currentCntrl.wrapperDiv.hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl(
                        {
                            FirstDropDownOptions: json,
                            ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(),
                            IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)),
                            IsEdit: (propObject.condDisplayCntrlProps) ? true : false,
                            SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps)
                        }
                    );
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 480, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    }
};


mFicientIde.PropertySheetJson.Separator = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Seperator",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
          {
              "text": "Inset",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Inset",
              "defltValue": "1",
              "validations": [

               ],
              "customValidations": [

               ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Appearance.eventHandlers.Inset,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
          },
          {
              "text": "Thickness",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Thickness",
              "defltValue": "1",
              "validations": [
                ],
              "customValidations": [
                ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Appearance.eventHandlers.Thickness,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "1px",
                      "value": "1"
                  },
                  {
                      "text": "2px",
                      "value": "2"
                  },
                   {
                       "text": "3px",
                       "value": "3"
                   },
                    {
                        "text": "4px",
                        "value": "4"
                    },
                     {
                         "text": "5px",
                         "value": "5"
                     }
               ]
          },
          {
              "text": "Style",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "BorderStyle",
              "defltValue": "1",
              "validations": [
                ],
              "customValidations": [
                ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Appearance.eventHandlers.BorderStyle,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "Solid",
                      "value": "0"
                  },
                  {
                      "text": "Dashed",
                      "value": "1"
                  },
                  {
                      "text": "Dotted",
                      "value": "2"
                  }
               ]
          },
            {
                "text": "Top Margin(px)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "TopMargin",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Appearance.eventHandlers.TopMargin,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Bottom Margin(px)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "BottomMargin",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Appearance.eventHandlers.BottomMargin,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
        ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [
                ],
                "customValidations": [
                ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [
                ],
                "customValidations": [
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Separator.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Separator(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Separator;
    this.description = opts.description;
    this.type = opts.type;
    this.inset = opts.inset;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;

    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.thickness = opts.thickness;
    this.borderStyle = opts.borderStyle;
    this.topMargin = this.topMargin;
    this.bottomMargin = this.bottomMargin;
}

Separator.prototype = new ControlNew();
Separator.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Separator;
Separator.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.isDeleted = false;
    this.inset = "1";
    this.fnSetBorderStyle("0");
    this.fnSetThickness("1");
    this.topMargin = "0";
    this.bottomMargin = "0";
};
Separator.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Separator.prototype.fnGetThickness = function () {
    return this.thickness;
};
Separator.prototype.fnSetThickness = function (value) {
    this.thickness = value;
};
Separator.prototype.fnGetBorderStyle = function () {
    return this.borderStyle;
};
Separator.prototype.fnSetBorderStyle = function (value) {
    this.borderStyle = value;
};
/*--------------------------------------------------------------DateTime Picker---------------------------------------------------------------------------*/


mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                setDateTimeLabel(appObj);
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                setDateTimeStyle(appObj);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $LabelValueProperty.css('color', '#' + appObj.fontColor + ' !important');
                }
                else
                    $LabelValueProperty.css('color', '#000000' + ' !important');

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        var $LabelProp = $('#' + appObj.id + '_Label');
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $LabelProp.css('color', fontCol + ' !important');
                        }
                        else {
                            $LabelProp.css('color', '#000000' + ' !important');
                        }
                    }
                });

                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).on("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Placeholder: function (control, appObj) {
                $(control).val(appObj.placeholder);
                setDateTimePlaceholder(appObj);
            },
            Inline: function (control, appObj) {
                $(control).val(appObj.inline);
                getDateTimeHtml(appObj);
            },
            Icon: function (control, cntrlObj, options) {
                var aryOptionsForDdl = [{ text: "None", val: "0"}];
                aryOptionsForDdl = $.merge(aryOptionsForDdl,
                                    MF_HELPERS.jqueryMobileIconsHelper.getAsTextValPairList());
                MF_HELPERS.bindDropdowns([$(control)], aryOptionsForDdl);
                var strIcon = cntrlObj.fnGetIcon();
                if (!mfUtil.isNullOrUndefined(strIcon)) {
                    if (mfUtil.isEmptyString(strIcon)) {
                        $(control).val("0");
                    }
                    else {
                        $(control).val(strIcon);
                    }
                }
                else {
                    $(control).val("0");
                }

                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.inline && cntrlObj.inline === "1") {
                    currentCntrl.wrapperDiv.show();
                    setDateTimeIcon(cntrlObj);
                }
                else {
                    currentCntrl.wrapperDiv.hide();
                }
            },
            Type: function (control, appObj) {
                $(control).val(appObj.datePickType);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;
                    setDateTimeLabel(propObject);
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    setDateTimeStyle(propObject);
                },
                PlaceholderValChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.placeholder = evnt.target.value;
                    setDateTimePlaceholder(propObject);
                },
                InlineChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.inline = evnt.target.value;
                    var cntrls = evnt.data.cntrls;
                    getDateTimeHtml(propObject);

                    var $IconWrapper = $('#' + cntrls.Icon.wrapperDiv);
                    var $IconControl = $('#' + cntrls.Icon.controlId);
                    if (propObject.inline === "1") {
                        propObject.fnSetIcon("0");
                        $IconWrapper.show();
                    }
                    else {
                        propObject.icon = "0";
                        $IconControl.val("0");
                        $IconWrapper.hide();
                    }
                },
                Icon: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var cntrls = evntData.cntrls;
                    propObject.fnSetIcon($self.val());

                    setDateTimeIcon(propObject);
                },
                DTPTypeChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.datePickType = evnt.target.value;
                }
            }
        },
        Data: {
            BindToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Data.getBindToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            DefaultValue: function (control, appObj) {
                $(control).val(appObj.dfltValue);
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Data.getDefaultValueCntrl();
                $(currentCntrl.control).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: new Date(1900, 1 - 1, 1),
                    maxDate: new Date(2100, 12 - 1, 31),
                    yearRange: "c-30:c+30"
                });
                if (currentCntrl.control) {
                    currentCntrl.control[0].addEventListener(
                                        'keydown', stopDefAction, false
                                    );
                }
                $(currentCntrl.control).change(function (evnt) {
                    appObj.dfltValue = evnt.target.value;
                });
                $(currentCntrl.control).blur(function (evnt) {
                    appObj.dfltValue = evnt.target.value;
                });
            },
            MinValue: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                $(control).change(function (event) {
                    if (parseFloat(event.target.value) < 1900 || parseFloat(event.target.value) > appObj.maxVal) {
                        showMessage('Minimum year cannot be less than 1900 and greater than Maximum Year', DialogType.Error);
                        $(control).val(appObj.minVal);
                    }
                    else if ($(control).val().trim().length <= 0) {
                        showMessage('Please enter minimum year', DialogType.Error);
                        $(control).val(appObj.minVal);
                    }
                });
                $(control).blur(function (event) {
                    if (parseFloat(event.target.value) < 1900 || parseFloat(event.target.value) > appObj.maxVal) {
                        showMessage('Minimum year cannot be less than 1900 and greater than Maximum Year', DialogType.Error);
                        $(control).val(appObj.minVal);
                    }
                    else if ($(control).val().trim().length <= 0) {
                        showMessage('Please enter minimum year', DialogType.Error);
                        $(control).val(appObj.minVal);
                    }
                });
                $(control).val(appObj.minVal);
            },
            MaxValue: function (control, appObj) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                $(control).change(function (event) {
                    if (parseFloat(event.target.value) > 2100 || parseFloat(event.target.value) < appObj.minVal) {
                        showMessage('Maximum year cannot be greater than 2100 and less than Minimum Year', DialogType.Error);
                        $(control).val(appObj.maxVal);
                    }
                    else if ($(control).val().trim().length <= 0) {
                        showMessage('Please enter maximum year', DialogType.Error);
                        $(control).val(appObj.maxVal);
                    }
                });
                $(control).blur(function (event) {
                    if (parseFloat(event.target.value) > 2100 || parseFloat(event.target.value) < appObj.minVal) {
                        showMessage('Maximum year cannot be greater than 2100 and less than Minimum Year ', DialogType.Error);
                        $(control).val(appObj.maxVal);
                    }
                    else if ($(control).val().trim().length <= 0) {
                        showMessage('Please enter maximum year', DialogType.Error);
                        $(control).val(appObj.maxVal);
                    }
                });
                $(control).val(appObj.maxVal);
            },
            eventHandlers: {
                BindToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Data.getBindToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty, propObject))
                        propObject.bindToProperty = evnt.target.value;
                    else {
                        if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                DefaultValChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var $TextboxValueProperty = $('#' + propObject.id);
                    $($TextboxValueProperty).val(evnt.target.value);
                    propObject.dfltValue = evnt.target.value;
                },
                MinValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.minVal = evnt.target.value;
                },
                MaxValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.maxVal = evnt.target.value;
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            MinDate: function (control, appObj) {
                $(control).val(appObj.minDate);
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Validation.getMinDateCntrl();
                $(currentCntrl.control).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: new Date(1900, 1 - 1, 1),
                    maxDate: new Date(2100, 12 - 1, 31),
                    yearRange: "c-30:c+30"
                });
                if (currentCntrl.control) {
                    currentCntrl.control[0].addEventListener(
                                        'keydown', stopDefAction, false
                                    );
                }
                $(currentCntrl.control).change(function (evnt) {
                    appObj.minDate = evnt.target.value;
                });
                $(currentCntrl.control).blur(function (evnt) {
                    appObj.minDate = evnt.target.value;
                });
            },
            MaxDate: function (control, appObj) {
                $(control).val(appObj.maxDate);
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Validation.getMaxDateCntrl();
                $(currentCntrl.control).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    minDate: new Date(1900, 1 - 1, 1),
                    maxDate: new Date(2100, 12 - 1, 31),
                    yearRange: "c-30:c+30"
                });
                if (currentCntrl.control) {
                    currentCntrl.control[0].addEventListener(
                                        'keydown', stopDefAction, false
                                    );
                }
                $(currentCntrl.control).change(function (evnt) {
                    appObj.maxDate = evnt.target.value;
                });
                $(currentCntrl.control).blur(function (evnt) {
                    appObj.maxDate = evnt.target.value;
                });
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                },
                MinDate: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.minDate = evnt.target.value;
                },
                MaxDate: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.maxDate = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);

                    var scriptOnBlur = '', scriptOnChange = '', scriptOnFocus = '', script = '';

                    _jsAceEditorOnChang.setTextInEditor("");
                    var options = '<option value="onBlur">onBlur</option>'
                             + '<option value="onChange">onChange</option>'
                                + '<option value="onFocus">onFocus</option>';

                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onChange");
                    $.uniform.update('#drpCntrlEvents');
                    var prevSelectedVal = $('#drpCntrlEvents')[0].value;

                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
                    if (propObject.onBlur != null && propObject.onBlur != undefined) scriptOnBlur = propObject.onBlur;
                    if (propObject.onFocus != null && propObject.onFocus != undefined) scriptOnFocus = propObject.onFocus;
                    switch ($('#drpCntrlEvents')[0].value) {
                        case "onBlur":
                            if (scriptOnBlur != null && scriptOnBlur != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnBlur));
                            }
                            break;
                        case "onChange":
                            if (scriptOnChange != null && scriptOnChange != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                            }
                            break;
                        case "onFocus":
                            if (scriptOnFocus != null && scriptOnFocus != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnFocus));
                            }
                            break;
                    }
                    $('#drpCntrlEvents').unbind('change');
                    $('#drpCntrlEvents').bind('change', function () {
                        $.uniform.update('#drpCntrlEvents');
                        script = _jsAceEditorOnChang.getText();
                        switch (prevSelectedVal) {
                            case "onBlur":
                                if (script != null && script != undefined) {
                                    scriptOnBlur = Base64.encode(script);
                                }
                                break;
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onFocus":
                                if (script != null && script != undefined) {
                                    scriptOnFocus = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditorOnChang.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "onBlur":
                                if (scriptOnBlur != null && scriptOnBlur != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnBlur));
                                }
                                break;
                            case "onChange":
                                if (scriptOnChange != null && scriptOnChange != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                                }
                                break;
                            case "onFocus":
                                if (scriptOnFocus != null && scriptOnFocus != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnFocus));
                                }
                                break;
                        }
                    });

                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        bindAdvanceScripts(propObject, scriptOnBlur, scriptOnChange, scriptOnFocus);
                        return false;
                    });
//                    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//                    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//                        bindAdvanceScripts(propObject, scriptOnBlur, scriptOnChange, scriptOnFocus);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        scriptOnBlur = '', scriptOnChange = '', scriptOnFocus = ''
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0") $(currentCntrl.wrapperDiv).show();
                else $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getBindToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Data", "BindToProperty");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getDefaultValueCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Data", "DefaultValue");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        function _getMinDateCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Validation", "MinDate");
            }
            return objControl;
        }
        function _getMaxDateCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propPluginPrefix,
                    "Validation", "MaxDate");
            }
            return objControl;
        }

        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getBindToPropertyCntrl: _getBindToPropertyCntrl,
                getDataObjectCntrl: _getDataObjectCntrl,
                getDefaultValueCntrl: _getDefaultValueCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            },
            Validation: {
                getMinDateCntrl: _getMinDateCntrl,
                getMaxDateCntrl: _getMaxDateCntrl
            }
        };
    })()
};


mFicientIde.PropertySheetJson.DateTimePicker = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Textbox",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Date",
                    "hidden": false
                }
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Placeholder",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "Placeholder",
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Appearance.eventHandlers.PlaceholderValChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Inline",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Inline",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Appearance.eventHandlers.InlineChanged,
                      "context": "",
                      "arguments": {
                          "cntrls": [{
                              grpPrefText: "Appearance",
                              cntrlPrefText: "Icon",
                              rtrnPropNm: "Icon"
                          }]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Icon",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Icon",
                "defltValue": "",
                "validations": [],
                "customValidations": [],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.Textbox.groups.Appearance.eventHandlers.Icon,
                    "context": "",
                    "arguments": {
                    }
                }],
                "CntrlProp": "",
                "helpText": {},
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": []
            },
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Type",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Appearance.eventHandlers.DTPTypeChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Date (Day, Month, Year)",
                      "value": "1"
                  },
                  {
                      "text": "Date (Month, Year)",
                      "value": "4"
                  },
                  {
                      "text": "Time",
                      "value": "2"
                  },
                  {
                      "text": "Date and Time",
                      "value": "3"
                  }
               ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Data.eventHandlers.BindToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
            {
                "text": "Default Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "DefaultValue",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Data.eventHandlers.DefaultValChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Min Year",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinValue",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Data.eventHandlers.MinValue,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Max Year",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxValue",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Data.eventHandlers.MaxValue,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Validation",
          "prefixText": "Validation",
          "hidden": false,
          "properties": [
          {
              "text": "Required",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Required",
              "defltValue": "0",
              "validations": [

               ],
              "customValidations": [

               ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
          },
              {
                  "text": "Min Date",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "MinDate",
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Validation.eventHandlers.MinDate,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Max Date",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxDate",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Validation.eventHandlers.MaxDate,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
          ]
      },
        {
            "name": "Advance",
            "prefixText": "Advance",
            "hidden": false,
            "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
        },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.DateTimePicker.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};

function DateTimePicker(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.DatetimePicker;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.placeholder = opts.placeholder;
    this.datePickType = opts.datePickType;
    this.dfltValue = opts.dfltValue;
    this.minVal = opts.minVal;
    this.maxVal = opts.maxVal;
    this.minDate = opts.minDate;
    this.maxDate = opts.maxDate;
    this.required = opts.required;
    this.bindToProperty = opts.bindToProperty;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.inline = opts.inline;
    this.icon = opts.icon;
    this.onChange = opts.onChange;
    this.onBlur = opts.onBlur;
    this.onFocus = opts.onFocus;
}

DateTimePicker.prototype = new ControlNew();
DateTimePicker.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DateTimePicker;
DateTimePicker.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Select Date";
    this.datePickType = "1";
    this.required = "1";
    this.conditionalDisplay = "1";
    this.minVal = "1900";
    this.maxVal = "2100";
    this.fontStyle = "0";
    this.placeholder = "DateTime Picker";
    this.isDeleted = false;
    this.inline = "0";
}
DateTimePicker.prototype.fnGetIcon = function () {
    return this.icon;
};
DateTimePicker.prototype.fnSetIcon = function (value) {
    this.icon = value;
};
DateTimePicker.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};

function stopDefAction(evt) {
    evt.preventDefault();
}

function getDateTimeHtml(propObject) {
    var strHtml = '';
    if (propObject) {
        var idCount = propObject.id.split('_')[1];
        if (propObject.inline === "1") {
            strHtml = '<div id="dateTimePickerDiv_' + idCount + '" class="node hide">'
                            + '<a id="DTP_' + idCount + '" type="dateTimePicker">'
                                + '<img id="imgDTPicker_' + idCount + '" alt="item"/>'
                            + '</a>'
                        + '</div>'
                        + '<div id="dateTimePickerLabelOuterDiv_' + idCount + '" class="node Ctrl_Width96 clearfix" style="border-bottom:1px solid black;min-height:25px;">'
                            + '<div id="DTP_' + idCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node">'
                                + '<img id="DTP_' + idCount + '_Img" src=""/>'
                            + '</div>'
                           + '<div class="clear hide"></div>'
                            + '<div id="DTP_' + idCount + '_Label" class="node" style="font-size:12.5px; float:left;padding-top:8px;padding-right:6px;min-width:10px;max-width:80px; overflow:hidden; white-space: nowrap; -ms-text-overflow: ellipsis;">Label Text'
                            + '</div>'
                            + '<div class="node Ctrl_Width96">'
                                + '<div style="color:#9a9a9a;white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;padding-top:8px;font-size:12.5px;" class="node" id="DTP_' + idCount + '_PlaceHolder"> DateTime Picker </div> '
                            + '</div>'
                        + '</div>';
        }
        else {
            strHtml = '<div id="dateTimePickerDiv_' + idCount + '" class="node hide">'
                            + '<a id="DTP_' + idCount + '" type="dateTimePicker">'
                                + '<img id="imgDTPicker_' + idCount + '" alt="item"/>'
                            + '</a>'
                        + '</div>'
                            + '<div id="dateTimePickerLabelOuterDiv_' + idCount + '" class="node">'
                            + '<div id="DTP_' + idCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node hide">'
                                + '<img id="DTP_' + idCount + '_Img" src=""/>'
                            + '</div>'
                            + '<div id="DTP_' + idCount + '_Label" class="node" style="font-size:12.5px; white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;" >Label Text'
                            + '</div>'
                           + '<div class="clear"></div>'
                            + '<div class="node Ctrl_Width96 IdeUi-input-text IdeUi-shadow-inset IdeUi-corner-all IdeUi-btn-shadow IdeUi-body-c IdeUi-input-has-clear">'
                                + '<div style="color:#9a9a9a;white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;" class="node IdeUi-input-text IdeUi-body-c Ctrl_Height " id="DTP_' + idCount + '_PlaceHolder"> DateTime Picker </div> '
                            + '</div>'
                        + '</div>'
        }

        $('#outerDiv' + idCount).html('');
        $('#outerDiv' + idCount).append(strHtml);
        setDateTimeIcon(propObject);
        setDateTimeLabel(propObject);
        setDateTimeStyle(propObject);
        setDateTimePlaceholder(propObject);
    }
}

function setDateTimeLabel(appObj) {
    if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
        $('#' + appObj.id + '_Label').html(appObj.labelText);
    }
    else {
        $('#' + appObj.id + '_Label').html("");
    }
}

function setDateTimePlaceholder(propObject) {
    if (propObject.placeholder != undefined && propObject.placeholder != null && propObject.placeholder.length > 0)
        $('#' + propObject.id + '_PlaceHolder').html(propObject.placeholder);
    else
        $('#' + propObject.id + '_PlaceHolder').html(" DateTime Picker ");
}

function setDateTimeStyle(appObj) {
    var lblStyle = getTextFontStyle(appObj);
    $('#' + appObj.id + '_Label').removeClass();
    $('#' + appObj.id + '_Label').addClass(lblStyle + ' node');
}

function setDateTimeIcon(propObject) {
    if (propObject.inline && propObject.inline === "1") {
        var iconName = propObject.fnGetIcon();
        var iconPath = MF_HELPERS.jqueryMobileIconsHelper.getCompleteIconPath(iconName);
        var iconNameSuffix = "-black.png";
        if (iconName != undefined && iconName != "0" && parseInt(iconPath.length) > 0 && iconPath != undefined) {
            $('#' + propObject.id + '_ImgDiv').removeClass('hide');
            $('#' + propObject.id + '_Img').attr("src", iconPath + iconNameSuffix);
        }
        else
            $('#' + propObject.id + '_ImgDiv').addClass('hide');
    }
    else
        $('#' + propObject.id + '_ImgDiv').addClass('hide');
}

/*------------------------------------------------------------------------Slider------------------------------------------------------------------------------------------------*/
mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                    $('#' + appObj.id + '_Label').css('margin-bottom', '');
                }
                else {
                    $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
                    $('#' + appObj.id + '_Label').css('margin-bottom', '-24px');
                }
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Disabled: function (control, appObj) {
                if (appObj.disabled) {
                    $(control).val(appObj.disabled);
                }
            },
            Size: function (control, cntrlObj) {
                if (!cntrlObj.dataMini)
                    cntrlObj.dataMini = "0";
                var size = cntrlObj.fnGetDataMini();
                $(control).val(size);
                setDataMiniSliderUI(cntrlObj);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;
                    if (evnt.target.value.length > 0) {
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                        $('#' + propObject.id + '_Label').css('margin-bottom', '');
                    }
                    else {
                        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
                        $('#' + propObject.id + '_Label').css('margin-bottom', '-24px');
                    }
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                Disabled: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.disabled = evnt.target.value;
                },
                Size: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fnSetDataMini(evnt.target.value);
                    setDataMiniSliderUI(propObject);
                }
            }
        },
        Data: {
            BindToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider.propSheetCntrl.Data.getBindToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            Databinding: function (control, appObj) {
                //$(control).val(appObj.bindType.id);
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            DefaultValue: function (control, appObj) {
                $(control).val(appObj.dfltValue);
                $('#' + appObj.id + '_Val').html(appObj.dfltValue);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            MinValue: function (control, appObj) {
                $(control).val(appObj.minVal);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(event);
                });
            },
            MaxValue: function (control, appObj) {
                $(control).val(appObj.maxVal);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(event);
                });
            },
            Increment: function (control, appObj) {
                $(control).val(appObj.incrementOnScroll);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(event);
                });
            },
            AllowManual: function (control, appObj) {
                $(control).val(appObj.manualEntry);
            },
            eventHandlers: {
                BindToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider.propSheetCntrl.Data.getBindToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty, propObject))
                        propObject.bindToProperty = evnt.target.value;
                    else {
                        if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.value = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }
                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                    intlJson = [],
                    isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                    .intlsenseHelper
                                    //                                                    .getControlDatabindingIntellisenseJson(
                                    //                                                        objCurrentForm, propObject.id, true
                                    //                                                    );
                                    if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.SLIDER,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                },
                DefaultValChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (propObject.minVal != undefined || propObject.minVal != null) {
                        if (parseFloat(evnt.target.value) < parseFloat(propObject.minVal)) {
                            if (propObject.dfltValue != undefined) evnt.target.value = propObject.dfltValue;
                            else evnt.target.value = "";
                            showMessage('Default value cannot be less than minimum value.', DialogType.Error);
                            return;
                        }
                    }

                    if (propObject.maxVal != undefined || propObject.maxVal != null) {
                        if (parseFloat(evnt.target.value) > parseInt(propObject.maxVal)) {
                            if (propObject.dfltValue != undefined) evnt.target.value = propObject.dfltValue;
                            else evnt.target.value = "";
                            showMessage('Default value cannot be greater than maximum value.', DialogType.Error);
                            return;
                        }
                    }

                    var $TextboxValueProperty = $('#' + propObject.id + '_Val');
                    $($TextboxValueProperty).html(evnt.target.value);
                    propObject.dfltValue = evnt.target.value;
                },
                SliderMinValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (propObject.maxVal != undefined || propObject.maxVal != null) {
                        if (parseFloat(evnt.target.value) >= parseFloat(propObject.maxVal)) {
                            if (propObject.minVal != undefined) evnt.target.value = propObject.minVal;
                            else evnt.target.value = "";
                            showMessage('Minimum value cannot be greater than maximum value.', DialogType.Error);
                            return;
                        }
                    }
                    if (propObject.dfltValue != undefined || propObject.dfltValue != null) {
                        if (parseFloat(propObject.dfltValue) < parseFloat(evnt.target.value)) {
                            if (propObject.minVal != undefined) evnt.target.value = propObject.minVal;
                            else evnt.target.value = "";
                            showMessage('Default cannot be less than minimum value.', DialogType.Error);
                            return;
                        }
                    }
                    propObject.minVal = evnt.target.value;
                },
                SliderMaxValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (propObject.minVal != undefined || propObject.minVal != null) {
                        if (parseFloat(evnt.target.value) <= parseFloat(propObject.minVal)) {
                            if (propObject.maxVal != undefined) evnt.target.value = propObject.maxVal;
                            else evnt.target.value = "";
                            showMessage('Maximum value cannot be less than minimum value.', DialogType.Error);
                            return;
                        }
                    }
                    if (propObject.dfltValue != undefined || propObject.dfltValue != null) {
                        if (parseFloat(evnt.target.value) < parseFloat(propObject.dfltValue)) {
                            if (propObject.maxVal != undefined) evnt.target.value = propObject.maxVal;
                            else evnt.target.value = "";
                            showMessage('Maximum value cannot be less than default value.', DialogType.Error);
                            return;
                        }
                    }
                    propObject.maxVal = evnt.target.value;
                },
                Increment: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.incrementOnScroll = evnt.target.value;
                },
                AllowManual: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.manualEntry = evnt.target.value;
                },
                ValidateKeyPressed: function (evnt) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(evnt);
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);
                    _jsAceEditorOnChang.setTextInEditor("");
                    var scriptOnChange = '';
                    var options = '<option value="onChange">onChange</option>';
                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onChange");
                    $.uniform.update('#drpCntrlEvents');

                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));

                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        scriptOnChange = _jsAceEditorOnChang.getText();
                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
                        return false;
                    });
//                    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//                    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//                        scriptOnChange = _jsAceEditorOnChang.getText();
//                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });

                    //mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0") $(currentCntrl.wrapperDiv).show();
                else $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.SLIDER.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getBindToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.SLIDER.propPluginPrefix,
                    "Data", "BindToProperty");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.SLIDER.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.SLIDER.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }

        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getBindToPropertyCntrl: _getBindToPropertyCntrl,
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};


mFicientIde.PropertySheetJson.Slider = {
    "type": MF_IDE_CONSTANTS.CONTROLS.SLIDER,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Slider",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Disabled",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Disabled",
                "defltValue": "0",
                "validations": [
                    ],
                "customValidations": [

                       ],
                "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Slider.groups.Appearance.eventHandlers.Disabled,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                  ]
            },
            {
                "text": "Size",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Size",
                "defltValue": "0",
                "validations": [
                    ],
                "customValidations": [

                       ],
                "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Slider.groups.Appearance.eventHandlers.Size,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "Normal",
                        "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
                    },
                    {
                        "text": "Mini",
                        "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
                    }
                  ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.BindToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                  {
                      "text": "Offline Database",
                      "value": "6"
                  }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                "text": "Default Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "DefaultValue",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.DefaultValChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Min Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinValue",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.SliderMinValue,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.ValidateKeyPressed,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Max Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxValue",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.SliderMaxValue,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.ValidateKeyPressed,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Increment",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Increment",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.Increment,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.ValidateKeyPressed,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Allow Manual",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "AllowManual",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Data.eventHandlers.AllowManual,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            }
         ]
      },
          {
              "name": "Advance",
              "prefixText": "Advance",
              "hidden": false,
              "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
          },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Slider.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Slider(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Slider;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.disabled = opts.disabled;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.minVal = opts.minVal;
    this.maxVal = opts.maxVal;
    this.incrementOnScroll = opts.incrementOnScroll;
    this.manualEntry = opts.manualEntry;
    this.value = opts.value;
    this.dfltValue = opts.dfltValue;
    this.displayText = opts.displayText;
    this.required = opts.required;
    this.databindObjs = opts.databindObjs;
    this.bindToProperty = opts.bindToProperty;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.dataMini = opts.dataMini;
    this.onChange = opts.onChange;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
}

Slider.prototype = new ControlNew();
Slider.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Slider;
Slider.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Label Text";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    this.manualEntry = "0";
    this.dfltValue = "500";
    this.minVal = "500";
    this.conditionalDisplay = "1";
    this.required = "1";
    this.disabled = "0";
    this.fontStyle = "0";
    this.isDeleted = false;
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
}
Slider.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Slider.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
Slider.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
Slider.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
};
Slider.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
};
//shalini Reset Function
Slider.prototype.fnResetObjectDetails = function () {
    if (mfUtil.isNullOrUndefined(this.fnGetDataMini())) {
        this.fnSetDataMini(MF_IDE_CONSTANTS.CNTRL_SIZES.normal);
    }

    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
Slider.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
};
Slider.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
};
Slider.prototype.fnGetValue = function () {
    return this.value;
};
Slider.prototype.fnSetValue = function (value) {
    this.value = value;
};
Slider.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Slider.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
Slider.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};

function setDataMiniSliderUI(cntrlObj) {
    if (cntrlObj.dataMini === "0") {
        $('#' + cntrlObj.id + '_Btn').removeAttr('style').removeClass('mini-slider-btn').addClass('normal-slider-btn');
        $('#' + cntrlObj.id + '_Slider').css('height', '');
        $('#' + cntrlObj.id + '_Label').css('font-size', '');
        $('#' + cntrlObj.id + '_Val').removeAttr('style').removeClass('mini-slider-val').addClass('normal-slider-val');
    }
    else if (cntrlObj.dataMini === "1") {
        $('#' + cntrlObj.id + '_Btn').removeAttr('style').removeClass('normal-slider-btn').addClass('mini-slider-btn');
        $('#' + cntrlObj.id + '_Slider').css('height', '11px');
        $('#' + cntrlObj.id + '_Label').css('font-size', '11px');
        $('#' + cntrlObj.id + '_Val').removeAttr('style').removeClass('normal-slider-val').addClass('mini-slider-val');
    }
}
/*---------------------------------------------------Image---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName);
                mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(appObj, null);
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Alignment: function (control, appObj) {
                $(control).val(appObj.imgAlignment);
                if (appObj.imgAlignment != undefined) {
                    switch (appObj.imgAlignment) {
                        case "0":
                            $('#' + appObj.id + "_OuterDiv").css("text-align", "left");
                            break;
                        case "1":
                            $('#' + appObj.id + "_OuterDiv").css("text-align", "center");
                            break;
                        case "2":
                            $('#' + appObj.id + "_OuterDiv").css("text-align", "right");
                            break;
                    }
                }
            },
            Inset: function (control, appObj) {
                if (appObj.inset != null && appObj.inset != undefined)
                    $(control).val(appObj.inset);
                else {
                    appObj.inset = "1";
                    $(control).val("1");
                }
            },
            Width: function (control, appObj) {
                if (appObj.imageWidth != null && appObj.imageWidth != undefined && appObj.imageWidth != "")
                    $(control).val(appObj.imageWidth);
                else {
                    appObj.imageWidth = "100";
                    $(control).val("100");
                }
            },
            Overlay: function (control, appObj) {
                if (appObj.overlay == null || appObj.overlay == undefined)
                    appObj.overlay = "0";

                $(control).val(appObj.overlay);
                if (appObj.overlay && appObj.overlay === "1") {
                    $('#' + appObj.id + '_DescDiv').addClass("img-cntrl-caption-div");
                    if (!$('#' + appObj.id + '_Label').hasClass("img-cntrl-overlay")) $('#' + appObj.id + '_Label').addClass("img-cntrl-overlay");
                }
                else {
                    $('#' + appObj.id + '_DescDiv').removeClass("img-cntrl-caption-div");
                    $('#' + appObj.id + '_Label').removeClass("img-cntrl-overlay");
                }
                setOverlayDivHeight(appObj.id, appObj.overlay);
            },
            Title: function (control, appObj, options) {
                $(control).val(appObj.title);
                $('#' + appObj.id + '_Label').html(appObj.title);
                var currentCntrl = options.cntrlHtmlObjects;
                if (appObj.imageBindType && appObj.imageBindType === "0")
                    currentCntrl.wrapperDiv.show();
                else
                    currentCntrl.wrapperDiv.hide();
            },
            FontSize: function (control, appObj) {
                if (appObj.fontSize == null || appObj.fontSize == undefined)
                    appObj.fontSize = "1";
                $(control).val(appObj.fontSize);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(getTitleFontSize(appObj) + " " + getTextFontStyle(appObj));
                if (appObj.overlay === "1")
                    $('#' + appObj.id + '_Label').addClass("img-cntrl-overlay");
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(getTitleFontSize(appObj) + " " + lblStyle);
                if (appObj.overlay === "1")
                    $('#' + appObj.id + '_Label').addClass("img-cntrl-overlay");
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty).css('color', appObj.fontColor + ' !important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }

                        var $LabelProp = $('#' + appObj.id + '_Label');
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            ($LabelProp).css('color', fontCol + ' !important');
                        }
                        else {
                            ($LabelProp).css('color', '#000000 !important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            eventHandlers: {
                Alignment: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.imgAlignment = evnt.target.value;
                    if (propObject.inset != undefined && propObject.inset == "0") {
                        propObject.imgAlignment = "1";
                        evnt.target.value = "1";
                    }
                    switch (propObject.imgAlignment) {
                        case "0":
                            $('#' + propObject.id + "_OuterDiv").css("text-align", "left");
                            break;
                        case "1":
                            $('#' + propObject.id + "_OuterDiv").css("text-align", "center");
                            break;
                        case "2":
                            $('#' + propObject.id + "_OuterDiv").css("text-align", "right");
                            break;
                    }
                },
                SetOverlay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.overlay = evnt.target.value;
                    if (propObject.overlay && propObject.overlay === "1") {
                        $('#' + propObject.id + '_DescDiv').addClass("img-cntrl-caption-div");
                        if (!$('#' + propObject.id + '_Label').hasClass("img-cntrl-overlay")) $('#' + propObject.id + '_Label').addClass("img-cntrl-overlay");
                    }
                    else {
                        $('#' + propObject.id + '_DescDiv').removeClass("img-cntrl-caption-div");
                        $('#' + propObject.id + '_Label').removeClass("img-cntrl-overlay");
                    }
                    setOverlayDivHeight(propObject.id, propObject.overlay);
                },
                TitleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.title = evnt.target.value;
                    $('#' + propObject.id + '_Label').html(propObject.title);
                },
                TitleSizeChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontSize = evnt.target.value;
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(getTitleFontSize(propObject) + " " + getTextFontStyle(propObject));
                    if (propObject.overlay === "1")
                        $('#' + propObject.id + '_Label').addClass("img-cntrl-overlay");
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(getTitleFontSize(propObject) + " " + lblStyle);
                    if (propObject.overlay === "1")
                        $('#' + propObject.id + '_Label').addClass("img-cntrl-overlay");
                },
                ImageWidth: function (evnt) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(evnt);
                },
                ImageWidthChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var cntrls = evnt.data.cntrls;
                    var $Width = evnt.target;
                    if (evnt.target.value == "") {
                        propObject.imageWidth = "100";
                        evnt.target.value = "100";
                    }
                    else if (parseFloat(evnt.target.value) < 25) {
                        evnt.target.value = propObject.imageWidth;
                        showMessage('Width cannot be less than 25%.', DialogType.Error);
                        return;
                    }
                    else if (parseFloat(evnt.target.value) > 100) {
                        evnt.target.value = propObject.imageWidth;
                        showMessage('Width cannot be greater than 100%.', DialogType.Error);
                        return;
                    }
                    propObject.imageWidth = evnt.target.value;
                    $($Width).val(propObject.imageWidth);
                    if (propObject.inset != undefined && propObject.inset == "0") {
                        propObject.imageWidth = "100";
                        evnt.target.value = "100";
                    }
                    mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(propObject, null);
                },
                Inset: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.inset = evnt.target.value;
                    var cntrls = evnt.data.cntrls;
                    var $WidthControl = $('#' + cntrls.Width.controlId);
                    var $AlignmentControl = $('#' + cntrls.Alignment.controlId);
                    if (propObject.inset != undefined && propObject.inset == "0") {
                        $WidthControl.val("100");
                        $AlignmentControl.val("1");
                        propObject.imageWidth = "100";
                        propObject.imgAlignment = "1";
                        mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(propObject, null);
                        $('#' + propObject.id + "_OuterDiv").css("text-align", "center");
                    }
                    else {
                        mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(propObject, null);
                        if (propObject.imgAlignment != undefined) {
                            switch (propObject.imgAlignment) {
                                case "0":
                                    $('#' + propObject.id + "_OuterDiv").css("text-align", "left");
                                    break;
                                case "1":
                                    $('#' + propObject.id + "_OuterDiv").css("text-align", "center");
                                    break;
                                case "2":
                                    $('#' + propObject.id + "_OuterDiv").css("text-align", "right");
                                    break;
                            }
                        }
                    }
                }
            }
        },
        Data: {
            ImageBinding: function (control, appObj) {
                $(control).val(appObj.imageBindType);
            },
            FixedSourceType: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getFixedSourceTypeCntrl();
                $(control).val(appObj.imageFixedType);
                if (appObj.imageBindType == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            Cache: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getCacheCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined && appObj.imageBindType === "1")
                    $(currentCntrl.wrapperDiv).hide();
                else
                    $(currentCntrl.wrapperDiv).show();
                $(control).val(appObj.cache);
            },
            Databinding: function (control, appObj) {
                //$(control).val(appObj.bindType.id);
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getDataBindingCntrl();
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
                if (appObj.imageBindType == "1")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();

            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined && appObj.imageBindType === "1")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            ImageSrcMedia: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getImageSrcMediaCntrl();
                var images = $('[id$=hdfImages]').val().split(',');
                var options = "",
                    strCompletePathOfImgSrc = "";

                var imageExist = false;
                $('#img_tool').attr("src", "");

                options += "<option value='-1'>Select Image</option>";
                $.each(images, function (val) {
                    options += "<option value='" + ImageFolderPath + this + "'>" + this + "</option>";
                });
                $(control).html(options);

                if (appObj.imageSource != undefined || appObj.imageSource != null) {
                    strCompletePathOfImgSrc = MF_HELPERS.amazonS3.getCompletePathForMediaFile(
                        appObj.imageSource);
                    $('option[value="' + strCompletePathOfImgSrc + '"]', $(currentCntrl.control)).length > 0 ?
                        $(control).val(strCompletePathOfImgSrc) : $(control).val('-1');

                    if (appObj.imageFixedType == "1" && appObj.imageBindType == "0") {
                        mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(appObj, strCompletePathOfImgSrc);
                    }
                }
                else {
                    $(control).val('-1');
                }

                if (appObj.imageFixedType == "1" && appObj.imageBindType == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            ImageSrcUrl: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getImageSrcUrlCntrl();
                $(control).val(appObj.imageSource);
                if (appObj.imageFixedType != "1" && appObj.imageBindType == "0") {
                    $(currentCntrl.wrapperDiv).show();
                }
                else {
                    $(currentCntrl.wrapperDiv).hide();
                }
            },
            MPlugin: function (control, appObj) {
                var strOptionsForDdl = MF_HELPERS.dropDownOptionsHelpers.getmPluginAgents(false, "0");
                $(control).html('');
                if (strOptionsForDdl) {
                    $(control).html(strOptionsForDdl);
                }
                $(control)[0].selectedIndex = 0;
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getMPluginCntrl();
                if (appObj.imageBindType == "0" && (appObj.imageFixedType == "3" || appObj.imageFixedType == "4")) {
                    var mPlugin = appObj.fnGetmPlugin();
                    if (mPlugin) {
                        $(control).val(mPlugin);
                    }
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                    if (propObject.bindType != mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None)
                        $ViewProperty.show();
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getDataObjectCntrl();
                    var intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.IMAGE,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                },
                ImageBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var cntrls = evnt.data.cntrls;
                    var $FixedSourceType = $('#' + cntrls.FixedSourceType.wrapperDiv);
                    var $ImageSrcMedia = $('#' + cntrls.ImageSrcMedia.wrapperDiv);
                    var $ImageSrcUrl = $('#' + cntrls.ImageSrcUrl.wrapperDiv);
                    var $Databinding = $('#' + cntrls.Databinding.wrapperDiv);
                    var $DataObject = $('#' + cntrls.DataObject.wrapperDiv);
                    var $MPlugin = $('#' + cntrls.MPlugin.wrapperDiv);
                    var $Cache = $('#' + cntrls.Cache.wrapperDiv);
                    var $Title = $('#' + cntrls.Title.wrapperDiv);
                    var $MPluginId = $('#' + cntrls.MPlugin.controlId);
                    var $FixedSourceTypeId = $('#' + cntrls.FixedSourceType.controlId);
                    var $ImageSrcMediaId = $('#' + cntrls.ImageSrcMedia.controlId);
                    var $ImageSrcUrlId = $('#' + cntrls.ImageSrcUrl.controlId);
                    var $CacheId = $('#' + cntrls.Cache.controlId);
                    var $TitleId = $('#' + cntrls.Title.controlId);

                    $MPlugin.hide();
                    $ImageSrcUrl.hide();
                    $Databinding.hide();
                    $DataObject.hide();

                    propObject.imageFixedType = "1";
                    propObject.imageSource = "";
                    propObject.title = "";

                    $FixedSourceTypeId.val(propObject.imageFixedType);
                    $ImageSrcMediaId.val("-1");
                    $ImageSrcUrlId.val("");
                    $MPluginId.val("-1");
                    $Title.val("");


                    if (evnt.target.value == "0") {
                        if (propObject.bindType.id != mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id) {
                            var blnConfirm = confirm("Databinding will be removed. Do you want to Proceed ?");
                            if (blnConfirm) {
                                propObject.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                                propObject.databindObjs = [];
                                propObject.imageBindType = evnt.target.value;
                                $('#' + cntrls.Databinding.controlId).val('1');
                            }
                            else {
                                propObject.imageBindType = "1";
                                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Data.getImageBindingCntrl();
                                $(currentCntrl.control).val("1");
                                //return;
                            }
                        }
                    }

                    propObject.imageBindType = evnt.target.value;

                    if (evnt.target.value == "0") {
                        $FixedSourceType.show();
                        $ImageSrcMedia.show();
                        $Cache.show();
                        $Title.show();
                    }
                    else {
                        $Title.hide();
                        $Databinding.show();
                        $FixedSourceType.hide();
                        $ImageSrcMedia.hide();
                        $ImageSrcUrl.hide();
                        propObject.cache = "0";
                        $CacheId.val("0");
                        $Cache.hide();
                        if (propObject.bindType.id != mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id)
                            $DataObject.show();
                        else
                            $DataObject.hide();
                    }
                },
                FixedSourceType: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var cntrls = evnt.data.cntrls;
                    var $ImageSrcMedia = $('#' + cntrls.ImageSrcMedia.wrapperDiv);
                    var $ImageSrcUrl = $('#' + cntrls.ImageSrcUrl.wrapperDiv);
                    var $ImageSrcMediaId = $('#' + cntrls.ImageSrcMedia.controlId);
                    var $ImageSrcUrlId = $('#' + cntrls.ImageSrcUrl.controlId);
                    var $MPlugin = $('#' + cntrls.MPlugin.wrapperDiv);
                    var $MPluginId = $('#' + cntrls.MPlugin.controlId);

                    propObject.imageSource = "";
                    mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(propObject, null);
                    $MPluginId[0].selectedIndex = 0;
                    propObject.fnSetmPlugin("");
                    $ImageSrcMediaId.val("-1");
                    $ImageSrcUrlId.val("");
                    propObject.imageSource = "";

                    if (evnt.target.value == "1") {
                        $ImageSrcMedia.show();
                        $ImageSrcUrl.hide();
                        $MPlugin.hide();
                    }
                    else if (evnt.target.value == "3" || evnt.target.value == "4") {
                        var arryMPluginagents = MF_HELPERS.dropDownOptionsHelpers.getmPluginAgentsArray();
                        if (arryMPluginagents != null && arryMPluginagents.length) {
                            $ImageSrcMedia.hide();
                            $ImageSrcUrl.show();
                            $MPlugin.show();
                            propObject.fnSetmPlugin($MPluginId[0].value);
                        }
                        else {
                            evnt.target.value = propObject.imageFixedType;
                            showMessage('No mPlugin agents are defined. For Local Url and Local File Path mPlugin Agent is required.', DialogType.Error);
                        }
                    }
                    else {
                        $ImageSrcMedia.hide();
                        $ImageSrcUrl.show();
                        $MPlugin.hide();
                    }
                    propObject.imageFixedType = evnt.target.value;
                },
                Cache: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.cache = evnt.target.value;
                },
                ImageSrcUrl: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.imageSource = evnt.target.value;
                    mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(propObject, null);
                },
                ImageSrcMediaChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var cntrls = evnt.data.cntrls;
                    var imgExist = false,
                        strCompletePath = "";

                    strCompletePath = evnt.target.value;
                    if (propObject.fnGetImageFixedType() === "1" &&
                        propObject.fnGetImageBindType() === "0") {

                        propObject.imageSource = MF_HELPERS
                                                .amazonS3
                                                .getKeyFromCompletePathForMediaFile(strCompletePath);
                    }
                    else {
                        propObject.imageSource = strCompletePath;
                    }


                    mFicientIde.MF_SIMPLE_CONTROLS_HELPER.setImage(propObject, strCompletePath);
                },
                MPlugin: function (evnt) {
                    var propObject = evnt.data.propObject,
                        $self = $(this);
                    if ($self.val() === "-1") {
                        propObject.fnSetmPlugin("");
                    }
                    else {
                        propObject.fnSetmPlugin($self.val());
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0") $(currentCntrl.wrapperDiv).show();
                else $(currentCntrl.wrapper).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getImageBindingCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "ImageBinding");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getDataBindingCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "Databinding");
            }
            return objControl;
        }
        function _getFixedSourceTypeCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "FixedSourceType");
            }
            return objControl;
        }
        function _getImageSrcMediaCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "ImageSrcMedia");
            }
            return objControl;
        }
        function _getCacheCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "Cache");
            }
            return objControl;
        }
        function _getImageSrcUrlCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "ImageSrcUrl");
            }
            return objControl;
        }
        function _getMPluginCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Data", "MPlugin");
            }
            return objControl;
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.IMAGE.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }

        return {
            Data: {
                getImageBindingCntrl: _getImageBindingCntrl,
                getDataObjectCntrl: _getDataObjectCntrl,
                getDataBindingCntrl: _getDataBindingCntrl,
                getFixedSourceTypeCntrl: _getFixedSourceTypeCntrl,
                getImageSrcMediaCntrl: _getImageSrcMediaCntrl,
                getImageSrcUrlCntrl: _getImageSrcUrlCntrl,
                getMPluginCntrl: _getMPluginCntrl,
                getCacheCntrl: _getCacheCntrl
            },
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Image = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Image",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Image.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Image.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Image Binding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ImageBinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.ImageBinding,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "FixedSourceType",
                               rtrnPropNm: "FixedSourceType"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "ImageSrcMedia",
                               rtrnPropNm: "ImageSrcMedia"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "ImageSrcUrl",
                               rtrnPropNm: "ImageSrcUrl"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "Databinding",
                               rtrnPropNm: "Databinding"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "UseOriginalSize",
                               rtrnPropNm: "UseOriginalSize"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "FitToWidth",
                               rtrnPropNm: "FitToWidth"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Height",
                               rtrnPropNm: "Height"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Width",
                               rtrnPropNm: "Width"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "MPlugin",
                               rtrnPropNm: "MPlugin"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "Cache",
                               rtrnPropNm: "Cache"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Title",
                               rtrnPropNm: "Title"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Fixed",
                      "value": "0"
                  },
                  {
                      "text": "Data Bind",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Source Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FixedSourceType",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.FixedSourceType,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "ImageSrcMedia",
                               rtrnPropNm: "ImageSrcMedia"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "ImageSrcUrl",
                               rtrnPropNm: "ImageSrcUrl"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "MPlugin",
                               rtrnPropNm: "MPlugin"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Media File",
                      "value": "1"
                  },
                  {
                      "text": "Public URL",
                      "value": "2"
                  },
                  {
                      "text": "Local URL",
                      "value": "3"
                  },
                  {
                      "text": "Local File Path",
                      "value": "4"
                  }
               ]
            },
           {
               "text": "mPlugin Agent",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
               "prefixText": "MPlugin",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.MPlugin,
                       "context": "",
                       "arguments": {

                       }
                   },
                   {
                       "name": "blur",
                       "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.MPlugin,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": true
               },
               "options": [
                ]
           },
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                  {
                      "text": "Offline Database",
                      "value": "6"
                  }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                "text": "Image Src",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "ImageSrcUrl",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.ImageSrcUrl,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 1000,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Image Src",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ImageSrcMedia",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.ImageSrcMediaChange,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "FitToWidth",
                               rtrnPropNm: "FitToWidth"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Height",
                               rtrnPropNm: "Height"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Width",
                               rtrnPropNm: "Width"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "UseOriginalSize",
                               rtrnPropNm: "UseOriginalSize"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [

               ]
            },
			{
			    "text": "Cache",
			    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
			    "prefixText": "Cache",
			    "noOfLines": "0",
			    "validations": [

               ],
			    "customValidations": [

               ],
			    "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Data.eventHandlers.Cache,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                        ]
                      }
                  }
               ],
			    "CntrlProp": "",
			    "HelpText": "",
			    "init": {
			        "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
			        "disabled": false,
			        "hidden": false
			    },
			    "options": [
                  {
                      "text": "Yes",
                      "value": "1"
                  },
                  {
                      "text": "No",
                      "value": "0"
                  }
               ]
			}
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Alignment",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Alignment",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.Alignment,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Left",
                      "value": "0"
                  },
                  {
                      "text": "Center",
                      "value": "1"
                  },
                  {
                      "text": "Right",
                      "value": "2"
                  }
               ]
            },
            {
                "text": "Width(%)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Width",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.ImageWidthChange,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Height",
                               rtrnPropNm: "Height"
                           }
                        ]
                      }
                  },
                  {
                      "name": "keypress",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.ImageWidth,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "50",
                    "hidden": false
                }
            },
            {
                "text": "Inset",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Inset",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.Inset,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Width",
                               rtrnPropNm: "Width"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "Alignment",
                               rtrnPropNm: "Alignment"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Yes",
                      "value": "1"
                  },
                  {
                      "text": "No",
                      "value": "0"
                  }
               ]
            },
              {
                  "text": "Overlay",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Overlay",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.SetOverlay,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                           ]
                        }
                    }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "0"
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.TitleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": "",
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Title Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
              {
                  "text": "Title Size",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "FontSize",
                  "defltValue": "1",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Appearance.eventHandlers.TitleSizeChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "Small",
                      "value": "0"
                  },
                  {
                      "text": "Medium",
                      "value": "1"
                  },
                  {
                      "text": "Large",
                      "value": "2"
                  }
               ]
              },
            {
                "text": "Title Color",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "FontColor",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Image.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Image(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Image;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.imageBindType = opts.imageBindType;
    this.imageFixedType = opts.imageFixedType;
    this.imageDataBindType = opts.imageDataBindType;
    this.imageSource = opts.imageSource;
    this.imageTitle = opts.imageTitle;
    this.imgAlignment = opts.imgAlignment;
    this.imageWidth = opts.imageWidth;
    this.cache = opts.cache;
    this.mPlugin = opts.mPlugin;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.inset = opts.inset;
    this.overlay = opts.overlay;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.title = opts.title;
    this.fontSize = opts.fontSize;
}

Image.prototype = new ControlNew();
Image.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Image;
Image.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.imageBindType = "0";
    this.imageFixedType = "1";
    this.imageDataBindType = "-1";
    this.imgAlignment = "1";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);
    $('#img_tool').attr("src", defaultImage.src);
    this.imageWidth = "100";
    this.cache = "1";
    this.conditionalDisplay = "1";
    this.isDeleted = false;
    this.inset = "1";
    this.overlay = "0";
    this.fontStyle = "0";
    this.fontSize = "1";
}
Image.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Image.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
Image.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
//shalini Reset Function
Image.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
}
Image.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
Image.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}

Image.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
Image.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
Image.prototype.fnGetSource = function () {
    return this.imageSource;
};
Image.prototype.fnSetSource = function (value) {
    this.imageSource = value;
};
Image.prototype.fnGetTitle = function () {
    return this.imageTitle;
};
Image.prototype.fnSetTitle = function (value) {
    this.imageTitle = value;
};
Image.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Image.prototype.fnGetmPlugin = function () {
    return this.mPlugin;
};
Image.prototype.fnSetmPlugin = function (value) {
    this.mPlugin = value;
};
Image.prototype.fnGetImageFixedType = function () {
    return this.imageFixedType;
};
Image.prototype.fnGetImageBindType = function () {
    return this.imageBindType;
};

var MF_SIMPLE_CONTROLS_HELPER = mFicientIde.MF_SIMPLE_CONTROLS_HELPER;
mFicientIde.MF_SIMPLE_CONTROLS_HELPER = (function () {
    //get image width
    function _getWidth(width, imagePercentage, imageOriginalWidth) {
        if (imageOriginalWidth < width)
            return imageOriginalWidth;
        else {
            var newWidth = (imagePercentage * width) / 100;
            return newWidth;
        }
    }

    function _setImage(appObj, imgSource) {
        var imgSrc = "";
        if (imgSource !== null)
            imgSrc = imgSource;
        else if (appObj.imageSource != null && appObj.imageSource != undefined) {
            if (appObj.imageFixedType == "1" && appObj.imageBindType == "0") {
                var strCompletePathOfImgSrc = MF_HELPERS.amazonS3.getCompletePathForMediaFile(
                        appObj.imageSource);
                imgSrc = strCompletePathOfImgSrc;
            }
            else {
                imgSrc = appObj.imageSource;
            }
        }
        else
            imgSrc = defaultImage.src;

        setOverlayDivHeight(appObj.id, appObj.overlay);

        $('#' + appObj.id).one("load", function (event) {
            $('#img_tool').attr("src", $('#' + event.target.id)[0].src);
            var reqWidth = parseFloat($('#' + event.target.id + '_OuterDiv').data("srcWidth")).toFixed(2);
            if (reqWidth !== 0) $('#' + event.target.id).attr("width", mFicientIde.MF_SIMPLE_CONTROLS_HELPER.getWidth($('#' + event.target.id + '_OuterDiv').width(), reqWidth, $('#img_tool')[0].width));
            else $('#' + event.target.id).attr("width", mFicientIde.MF_SIMPLE_CONTROLS_HELPER.getWidth($('#' + event.target.id + '_OuterDiv').width(), parseFloat("100"), $('#img_tool')[0].width));
            setOverlayDivHeight(appObj.id, $('#' + event.target.id + '_OuterDiv').data("overlay"));
        });
        $('#' + appObj.id).error(function (event) {
            imgSrc = defaultImage.src;
            $('#img_tool').attr("src", imgSrc);
            $('#' + event.target.id).attr("src", imgSrc);
            $('#' + event.target.id).attr("width", mFicientIde.MF_SIMPLE_CONTROLS_HELPER.getWidth($('#' + event.target.id + '_OuterDiv').width(), parseFloat("100"), $('#img_tool')[0].width));
            $('#' + event.target.id).onload = null;
            setOverlayDivHeight(appObj.id, $('#' + event.target.id + '_OuterDiv').data("overlay"));
        });

        $('#img_tool').attr("src", imgSrc);
        $('#' + appObj.id).attr("src", imgSrc);
        $('#' + appObj.id + '_OuterDiv').data("srcWidth", appObj.imageWidth);
        $('#' + appObj.id + '_OuterDiv').data("overlay", appObj.overlay);
        if (parseFloat($('#img_tool')[0].width).toFixed(2) > 0)
            $('#' + appObj.id).attr("width", mFicientIde.MF_SIMPLE_CONTROLS_HELPER.getWidth($('#' + appObj.id + '_OuterDiv').width(), appObj.imageWidth, $('#img_tool')[0].width));
        setOverlayDivHeight(appObj.id, appObj.overlay);
    }

    //get image height
    function _getHeight(newWidth, object) {
        if (object.imageBindType == "0") {
            var orginalWidth = $('#img_tool')[0].width;
            var originalHeight = $('#img_tool')[0].height;
            if (originalHeight == 0) return newWidth;
            var aspectRatio = orginalWidth / originalHeight;
            return newWidth / aspectRatio;
        }
        else {
            return object.imageHeight;
        }
    }
    return {
        getWidth: function (width, imagePercentage, imageOriginalWidth) {
            return _getWidth(width, imagePercentage, imageOriginalWidth);
        },
        getHeight: function (newWidth, object) {
            return _getHeight(newWidth, object);
        },
        setImage: function (appObj, imgSource) {
            return _setImage(appObj, imgSource);
        }
    }
})();

function setOverlayDivHeight(cntrlId, overlay) {
    var captionDivHeight = parseFloat($('#' + cntrlId + "_OuterDiv").height()).toFixed(2) / 2;
    if (overlay && overlay === "1") $('#' + cntrlId + "_DescDiv").css('height', captionDivHeight);
    else $('#' + cntrlId + "_DescDiv").css('height', '');
}

/*------------------------------------------------------------------------Spacer---------------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Spacer = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, cntrlObj) {
                $(control).val(cntrlObj.type.UIName)
            },
            Name: function (control, cntrlObj) {
                $(control).val(cntrlObj.userDefinedName)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName)
                            evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                }
            }
        },
        Appearance: {
            Height: function (control, cntrlObj) {
                if (parseFloat(cntrlObj.height) < 3) {
                    cntrlObj.height = 3;
                }
                $(control).val(cntrlObj.height);
                $('#SpacerOuterDiv_' + cntrlObj.id.split('_')[1]).css('height', cntrlObj.height + 'px');
            },
            eventHandlers: {
                Height: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (parseFloat($(this).val()) < 3) {
                        $(this).val("3");
                        showMessage('Height cannot be less than 3px.', DialogType.Error);
                        return;
                    }
                    propObject.height = $(this).val();
                    $('#SpacerOuterDiv_' + propObject.id.split('_')[1]).css('height', propObject.height + 'px');
                }
            }
        },
        Behaviour: {
            Display: function (control, cntrlObj) {
                if (cntrlObj.conditionalDisplay) {
                    $(control).val(cntrlObj.conditionalDisplay);
                }
            },
            Conditions: function (control, cntrlObj, options) {
                //var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Behaviour.getConditionsCntrl();
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.conditionalDisplay == "0")
                    currentCntrl.wrapperDiv.show();
                else
                    currentCntrl.wrapperDiv.hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl(
                        {
                            FirstDropDownOptions: json,
                            ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(),
                            IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)),
                            IsEdit: (propObject.condDisplayCntrlProps) ? true : false,
                            SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps)
                        }
                    );
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 450, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    }
};
mFicientIde.PropertySheetJson.Spacer = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER,
    "groups": [
       {
           "name": "Control Properties",
           "prefixText": "ControlProperties",
           "hidden": true,
           "properties": [
             {
                 "text": "Type",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                 "prefixText": "Type",
                 "noOfLines": "0",
                 "validations": [

                ],
                 "customValidations": [

                ],
                 "events": [

                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "maxlength": 50,
                     "disabled": true,
                     "defltValue": "Spacer",
                     "hidden": false
                 }
             },
             {
                 "text": "Name",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                 "prefixText": "Name",
                 "noOfLines": "0",
                 "validations": [

                ],
                 "customValidations": [

                ],
                 "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Spacer.groups.ControlProperties.eventHandlers.NameChanged,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "maxlength": 50,
                     "disabled": false,
                     "defltValue": "",
                     "hidden": false
                 }
             }
          ]
       },
       {
           "name": "Appearance",
           "prefixText": "Appearance",
           "hidden": false,
           "properties": [
           {
               "text": "Height(px)",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
               "prefixText": "Height",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Spacer.groups.Appearance.eventHandlers.Height,
                       "context": "",
                       "arguments": {

                       }
                   },
                   {
                       "name": "blur",
                       "func": PROP_JSON_HTML_MAP.Spacer.groups.Appearance.eventHandlers.Height,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": false
               },
               "options": [
                ]
           }
         ]
       },
       {
           "name": "Behaviour",
           "prefixText": "Behaviour",
           "hidden": false,
           "properties": [
             {
                 "text": "Display",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Display",
                 "defltValue": "1",
                 "validations": [
                 ],
                 "customValidations": [
                 ],
                 "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Spacer.groups.Behaviour.eventHandlers.Display,
                       "context": "",
                       "arguments": {
                           "cntrls": [
                            {
                                grpPrefText: "Behaviour",
                                cntrlPrefText: "Conditions",
                                rtrnPropNm: "Conditions"
                            }
                         ]
                       }
                   }

                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                   {
                       "text": "Always",
                       "value": "1"
                   },
                   {
                       "text": "Conditional",
                       "value": "0"
                   }
                ]
             },
             {
                 "text": "Conditions",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                 "prefixText": "Conditions",
                 "disabled": false,
                 "noOfLines": "0",
                 "validations": [
                 ],
                 "customValidations": [
                ],
                 "events": [
                   {
                       "name": "click",
                       "func": PROP_JSON_HTML_MAP.Spacer.groups.Behaviour.eventHandlers.ConditionalDisplay,
                       "context": "",
                       "arguments": {}
                   }
                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "disabled": false,
                     "defltValue": "Select Conditions",
                     "hidden": true
                 }
             }
          ]
       }
    ]
};

function Spacer(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Spacer;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.height = opts.height;
}

Spacer.prototype = new ControlNew();
Spacer.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Spacer;
Spacer.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.height = 3;
    this.conditionalDisplay = "1";
    this.isDeleted = false;
}
Spacer.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};




var mfIdeSimpleCntrls = "mfIdeSimpleCntrls";


//-----------------------------------------------------MOHAN--------------------------------------------------//
//-----------------------------------------------------Hidden Field ------------------------------------------//
//function HiddenField(options) {
//    if (!options) options = {};
//    this.id = options.id;
//    this.userDefinedName = options.userDefinedName;
//    this.description = options.description;
//    this.type = options.type;
//    this.databindObjs = options.databindObjs; //array of Databinding Objs;
//    this.displayText = options.displayText;
//    this.oldName = options.oldName;
//    this.isDeleted = options.isDeleted;
//    this.isNew = options.isNew;
//}
//HiddenField.prototype = new ControlNew();
//HiddenField.prototype.fnRemoveDatabindObjs = function () {
//    this.databindObjs = [];
//};
//HiddenField.prototype.fnAddDatabindObj = function (databindObj) {
//    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
//        this.databindObjs = [];
//    }
//    if (databindObj && databindObj instanceof DatabindingObj) {
//        this.databindObjs.push(databindObj);
//    }
//};
//HiddenField.prototype.fnDeleteDatabindObj = function (name) {
//    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
//        return databindObj.name !== name;
//    });
//    this.databindObjs = aryNewDatabindObjs;
//};
//HiddenField.prototype.fnSetDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
//    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
//        this.databindObjs = [];
//    }
//    if ($.isArray(databindObjs)) {
//        var self = this;
//        $.each(databindObjs, function (index, value) {
//            if (value && value instanceof DatabindingObj) {
//                self.databindObjs.push(value);
//            }
//        });
//    }
//};
//HiddenField.prototype.fnGetDatabindingObj = function () {
//    return this.databindObjs && this.databindObjs[0]; //only one element in array
//};
//HiddenField.prototype.fnGetBindTypeOfDatabindObj = function () {
//    var objDatabindingObject = this.fnGetDatabindingObj();
//    var objBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
//    if (objDatabindingObject) {
//        objBindType = objDatabindingObject.fnGetBindTypeObject();
//    }
//    return objBindType;
//};
//HiddenField.prototype.fnSetUserDefinedName = function (name) {
//    this.userDefinedName = name;
//};
//HiddenField.prototype.fnIsNewlyAdded = function () {
//    return this.isNew;
//}
//HiddenField.prototype.fnMarkDeleted = function () {
//    this.isDeleted = true;
//}
//HiddenField.prototype.fnRename = function (name) {
//    if (!this.fnIsNewlyAdded() &&
//        !this.oldName &&
//        this.userDefinedName !== name) {

//        this.oldName = this.userDefinedName;
//        this.userDefinedName = name;
//    }
//    else if (this.userDefinedName != name) {
//        this.userDefinedName = name;
//    }
//};
//HiddenField.prototype.fnResetObjectDetails = function () {
//    var databindObjs = this.databindObjs;
//    this.databindObjs = [];
//    var i = 0;
//    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
//        for (i = 0; i <= databindObjs.length - 1; i++) {
//            var objdatabindObj = new DatabindingObj(
//                     databindObjs[i].id,
//                     databindObjs[i].cmdParams,
//                     databindObjs[i].index,
//                     databindObjs[i].bindObjType,
//                     databindObjs[i].name,
//                     databindObjs[i].dsName,
//                     databindObjs[i].tempUiIndex,
//                     databindObjs[i].usrName,
//                     databindObjs[i].pwd
//            );
//            objdatabindObj.fnResetObjectDetails();
//            this.fnAddDatabindObj(objdatabindObj);
//        }
//    }
//};
//mFicientIde.HiddenFieldControlHelper = (function () {
//    var _currentLargestIdOfHidField = 0;
//    var _hiddenFieldsSection = "Sec_hd";
//    var _hiddenFieldsColumnPanel = "Sec_hd_Col";
//    var _jqueryKeyForContDiv = "contDivDataKey";
//    var _jqueryKeyForTableRow = "tableRowDataKey";
//    var _jqueryKeyForTable = "tableDataKey";
//    //var _jqueryKeyForAdditionRow = "additionRowDataKey";
//    //MANAGE GLOBAL VARIABLE
//    function _setCurrentLargestIdOfHidField(cntrls) {
//        var largestValue = 0;
//        if (cntrls && $.isArray(cntrls) && cntrls.length > 0) {
//            for (var i = 0; i <= cntrls.length - 1; i++) {
//                var cntrl = cntrls[i];
//                var idNumber = cntrl.id.split('_')[1];
//                if (idNumber > largestValue) {
//                    largestValue = idNumber;
//                }
//            }
//        }
//        else {
//            _currentLargestIdOfHidField = 0;
//        }
//    }
//    function _incrementCurrentLargestId() {
//        _currentLargestIdOfHidField = _currentLargestIdOfHidField + 1;
//    }
//    function _resetCurrentLargestIdOfHidField() {
//        _currentLargestIdOfHidField = 0;
//    }
//    //MANAGE GLOBAL VARIABLE

//    function _getDivContainerOfHidFieldDtl() {
//        return $('#divDetailsTableContainer');
//    }
//    function _getHidFieldsDetailTable() {
//        var $containerDiv = _getDivContainerOfHidFieldDtl();
//        return $containerDiv.find('table');
//    }


//    //Bind Details Table
//    function _getTableStartTag() {
//        return '<table class="detailsTable">';
//    }
//    function _getTableEndTag() {
//        return '</table>';
//    }
//    function _getMappingTableHeadRow() {
//        var strHtml = '<thead><tr>';
//        strHtml += '<th >Name</th>';
//        strHtml += '<th>Binding Type</th>';
//        strHtml += '<th >Databinding</th><th class="w_30p"></th></tr></thead>';
//        return strHtml;
//    }
//    function _getTBodyStartTag() {
//        return '<tbody>';
//    }
//    function _getTBodyEndTag() {
//        return '</tbody>';
//    }
//    function _getColumnsOfRow(isAdditionRow) {
//        var strHtml = "";
//        strHtml += '<td ><input type="text" class="HidFieldName"></td>';
//        strHtml += '<td ><select  class="BindingType">';
//        strHtml += _getOptionsForBindingType() + '</select></td>';
//        strHtml += '<td ><input type="text" value="None" maxlength="50" class="txtPropSmall propDisable ObjectName" disabled="disabled" readonly="readonly">';
//        strHtml += '<input type="button" class="BindButton w_100p" value="..."/></td>';
//        if (!isAdditionRow) {
//            strHtml += "<td class=\"w_30p\"><span class=\"crossImg16x16\"></span></td>";
//        }
//        else {
//            strHtml += "<td class=\"w_30p\"><span class=\"addImg16x16\"></span></td>";
//        }
//        return strHtml;
//    }
//    function _getHidDtlsTableRowWithCross() {
//        var strHtml = '<tr>';
//        strHtml += _getColumnsOfRow(false);
//        strHtml += '</tr>';
//        return strHtml;
//    }
//    function _getHidDtlsTableRowOnlyAdd() {
//        var strHtml = '<tr class="AdditionRow">';
//        strHtml += _getColumnsOfRow(true);
//        strHtml += '</tr>';
//        return strHtml;
//    }
//    function _getOptionsForBindingType() {
//        var options = '<option value="0">None</option>';
//        options += '<option value="1">Database Object</option>';
//        options += '<option value="2">Web Service Object</option>';
//        options += '<option value="5">OData Object</option>';
//        return options;
//    }
//    function _addAdditionRowToTable() {
//        var $mappingTable = _getHidFieldsDetailTable();
//        var $tbody = $mappingTable.find('tbody');
//        var $tr = $tbody.find('tr');

//        if ($tr.length > 0) {
//            $tbody.find('tr:last').after(_getHidDtlsTableRowOnlyAdd());
//        }
//        else {
//            $tbody.append(_getHidDtlsTableRowOnlyAdd());
//        }
//    }
//    function _addDetailsRowToTable(detailsTable/*jquery object*/) {
//        var $detailsTable = null;
//        if (detailsTable) {
//            $detailsTable = detailsTable;
//        }
//        else {
//            $detailsTable = _getHidFieldsDetailTable();
//        }
//        var $tbody = $detailsTable.find('tbody');
//        var $tr = $tbody.find('tr').not('.AdditionRow');

//        if ($tr.length > 0) {
//            $tr.last().after(_getHidDtlsTableRowWithCross());
//        }
//        else {
//            $tbody.find('tr.AdditionRow').before(_getHidDtlsTableRowWithCross());
//        }
//    }

//    //BIND DETAILS TABLE

//    //JQUERY DATA GET SET
//    function _setDataOfContainerDiv(formId, cntrls/*array of hidden controls*/) {
//        var $containerDiv = _getDivContainerOfHidFieldDtl();
//        var aryCopyOfHidFields = [];
//        if (cntrls && $.isArray(cntrls)) {
//            $.extend(true, aryCopyOfHidFields, cntrls);
//        }
//        $containerDiv.data(_jqueryKeyForContDiv, {
//            previousSavedDataCopy: aryCopyOfHidFields,
//            formId: formId
//        });
//    }
//    function _getDataOfContainerDiv() {
//        var $containerDiv = _getDivContainerOfHidFieldDtl();
//        return $containerDiv.data(_jqueryKeyForContDiv);
//    }
//    function _setDataOfAdditionRow() {
//        var $detailsTable = _getHidFieldsDetailTable();
//        var objHiddenField = new HiddenField({
//            id: "HDF_" + _currentLargestIdOfHidField,
//            userDefinedName: "",
//            type: MF_IDE_CONSTANTS.CONTROLS.HIDDEN,
//            databindObjs: [],
//            isNew: true,
//            oldName: "",
//            isDeleted: false
//        });

//        var objDataObject = new DatabindingObj("", [], "0",
//                            MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id,
//                            "", "");

//        objHiddenField.fnAddDatabindObj(objDataObject);

//        $detailsTable.find('tbody tr.AdditionRow')
//        .data(_jqueryKeyForTableRow, objHiddenField);
//    }
//    function _setDataOfTableRow(tr/*jquery object*/, data) {
//        if (!tr) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
//        tr.data(_jqueryKeyForTableRow, data);
//    }
//    function _getDataOfTableRow(tr/*jquery key*/) {
//        return tr.data(_jqueryKeyForTableRow);
//    }
//    function _setDataOfContTable(data) {
//        var $detailsTbl = _getHidFieldsDetailTable();
//        $detailsTbl.data(_jqueryKeyForTable, data);
//    }
//    function _getDataOfContTable() {
//        var $detailsTbl = _getHidFieldsDetailTable();
//        return $detailsTbl.data(_jqueryKeyForTable);
//    }
//    function _removeDataOfContTable() {
//        var $detailsTbl = _getHidFieldsDetailTable();
//        $detailsTbl.removeData(_jqueryKeyForTable);
//    }
//    function _removeDataOfContDiv() {
//        var $contDiv = _getDivContainerOfHidFieldDtl();
//        $contDiv.removeData(_jqueryKeyForContDiv);
//    }
//    //JQUERY DATA GET SET

//    //Events 
//    function _setEventsOfRow(tblRow/*jquery object*/) {
//        tblRow.find('.BindButton').on('click', function (evnt) {
//            var $self = $(this);
//            var $tr = $self.closest('tr');
//            var objHiddenField = _getDataOfTableRow($tr);
//            var objDatabindObject = objHiddenField.fnGetDatabindingObj();
//            var objDatabindObjectCopy = $.extend(true, {}, objDatabindObject);
//            switch (objHiddenField.fnGetBindTypeOfDatabindObj()) {
//                case MF_IDE_CONSTANTS.ideCommandObjectTypes.None:
//                    showMessage("Object bind type is set as none.For databinding bind type should be other than none.", DialogType.Info);
//                    break;
//                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
//                    mFicientIde.MF_DATA_BINDING
//                        .databaseObjectBinding(objHiddenField, objDatabindObjectCopy);
//                    _setDataOfContTable({
//                        rowIndex: $tr.index()
//                    });
//                    showModalPopUpWithOutHeader(
//                            'SubProcCtrlDbCmd', 'Database Object', 500);
//                    break;
//                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
//                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:

//                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
//                    mFicientIde.MF_DATA_BINDING
//                    .webserviceObjectBinding(objHiddenField, objDatabindObjectCopy);
//                    _setDataOfContTable({
//                        rowIndex: $tr.index()
//                    });
//                    showModalPopUpWithOutHeader(
//                        'SubProcCtrlWsCmd', 'Webservice Object', 500);
//                    break;
//                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
//                    mFicientIde.MF_DATA_BINDING
//                    .oDataObjectBinding(objHiddenField, objDatabindObjectCopy);
//                    _setDataOfContTable({
//                        rowIndex: $tr.index()
//                    });
//                    showModalPopUpWithOutHeader(
//                        'SubProcCtrlODataCmd', 'OData Object', 500);
//                    break;
//            }
//        });
//        tblRow.find('.crossImg16x16').on('click', function (evnt) {
//            var blnConfirm = confirm('Are you sure you want to remove this property.');
//            if (blnConfirm) {
//                var $tr = $(this).closest('tr');
//                var objDataOfContDiv = _getDataOfContainerDiv();
//                var objHiddenField = _getDataOfTableRow($tr);
//                if (objHiddenField.fnIsNewlyAdded()) {
//                    _removeHidFieldFromContDiv(objHiddenField);
//                }
//                else {
//                    objHiddenField.fnMarkDeleted();
//                }
//                $tr.remove();
//            }
//        });
//        tblRow.find('.addImg16x16').on('click', _processAddRowToTable);
//        tblRow.find('.BindingType').on('change', function (evnt) {
//            var $self = $(this);
//            var $tr = $self.closest('tr');
//            var objHiddenField = _getDataOfTableRow($tr);
//            objHiddenField.fnRemoveDatabindObjs();
//            var dataObject = new DatabindingObj("", [], "0", $self.val(), "", "");
//            objHiddenField.fnAddDatabindObj(dataObject);
//            //objHiddenField.fnRemoveAllDataBindingProperties();
//        });
//    }
//    function _setEventsOfCompleteTable() {
//        var $tbl = _getHidFieldsDetailTable();
//        $tbl.find('tbody tr').each(function (index) {
//            _setEventsOfRow($(this));
//        });
//    }
//    function _setEventOfLastAddedRow() {
//        var $tbl = _getHidFieldsDetailTable();
//        _setEventsOfRow($tbl.find('tbody tr').not('.AdditionRow').last());
//    }
//    //Events

//    //PROCESS
//    function _formHidDtlsHtml(formId, cntrls/*list of hid cntrl*/) {

//        if (!cntrls) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
//        _setDataOfContainerDiv(formId, cntrls);
//        var i = 0;
//        var $hidFieldsContDiv = _getDivContainerOfHidFieldDtl();

//        var strHtml = _getTableStartTag();
//        strHtml += _getMappingTableHeadRow();
//        strHtml += _getTBodyStartTag();
//        strHtml += _getTBodyEndTag();

//        strHtml += _getTableEndTag();

//        $hidFieldsContDiv.html('');

//        $hidFieldsContDiv.html(strHtml);

//        var $detailsTable = _getHidFieldsDetailTable();
//        _addAdditionRowToTable();
//        if (cntrls && $.isArray(cntrls) &&
//            cntrls.length > 0) {
//            for (i = 0; i <= cntrls.length - 1; i++) {
//                _addDetailsRowToTable($detailsTable);
//            }
//        }
//        _setCurrentLargestIdOfHidField(cntrls);
//        _fillDataToHtml(cntrls);
//        _setDataOfAdditionRow();
//        _setEventsOfCompleteTable();
//    }
//    function _fillDataToHtml(cntrls) {
//        if (cntrls && $.isArray(cntrls) && cntrls.length > 0) {
//            var $detailsTable = _getHidFieldsDetailTable();
//            var objContDivData = _getDataOfContainerDiv();
//            var aryHidFields = [];
//            if (objContDivData) {
//                aryHidFields = objContDivData.previousSavedDataCopy; //should get this.
//            }
//            $detailsTable.find('tbody tr')
//           .not('.AdditionRow')
//           .each(function (index) {
//               _fillDataToHtmlTblRow($(this), aryHidFields[index]);
//           });
//        }
//    }
//    function _fillDataToHtmlTblRow(tblRow/*jquery object*/,
//                dataObject/*HiddenField Object*/) {

//        if (!tblRow)
//            throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
//        if (dataObject) {
//            var $row = tblRow;
//            var $txtHidFieldName = $row.find('.HidFieldName');
//            var $ddlBindType = $row.find('.BindingType');
//            var $txtObjectName = $row.find('.ObjectName');
//            var objDatabindingObj = dataObject.fnGetDatabindingObj();
//            $txtHidFieldName.val(dataObject.userDefinedName);
//            $ddlBindType.val(dataObject.fnGetBindTypeOfDatabindObj().id);
//            $txtObjectName.val(objDatabindingObj.name);
//            _setDataOfTableRow(tblRow, dataObject);
//        }
//    }
//    function _getAllHidFieldIdsFromContDiv() {
//        var objContDivData = _getDataOfContainerDiv();
//        var aryHidFields = objContDivData.previousSavedDataCopy;
//        var aryIds = [];
//        if (aryHidFields && $.isArray(aryHidFields) && aryHidFields.length > 0) {
//            for (var i = 0; i <= aryHidFields.length - 1; i++) {
//                aryIds.push(aryHidFields[i].id);
//            }
//        }
//        return aryIds;
//    }
//    function _getAllOldHidFieldsDeleted() {
//        var objContDivData = _getDataOfContainerDiv();
//        var aryHidFields = objContDivData.previousSavedDataCopy;
//        var aryFilteredHidfields = [];
//        if (aryHidFields && $.isArray(aryHidFields) && aryHidFields.length > 0) {
//            aryFilteredHidfields = $.grep(aryHidFields, function (value, index) {
//                return value.isDeleted !== false;
//            });
//        }
//        return aryFilteredHidfields;
//    }
//    function _removeHidFieldFromContDiv(hiddenField) {
//        if (hiddenField) {
//            var objContDivData = _getDataOfContainerDiv();
//            var arySavedHiddenFields = objContDivData.previousSavedDataCopy;
//            var aryFilteredHidField = [];
//            if (arySavedHiddenFields && $.isArray(arySavedHiddenFields) &&
//                arySavedHiddenFields.length > 0) {
//                arySavedHiddenFields = $.grep(arySavedHiddenFields,
//                        function (value, index) {
//                            return value.id !== hiddenField.id;
//                        });
//            }
//            objContDivData.previousSavedDataCopy = arySavedHiddenFields;
//        }
//    }
//    //PROCESS

//    // function _getAllHidFieldNamesFromTable(){
//    //     var aryNames = [];
//    //     var $detailsTable = _getHidFieldsDetailTable();
//    //     $detailsTable.find('tbody tr')
//    //     .not('.AdditionRow')
//    //     .each(function (index) {
//    //         var $txtName = $(this).find('.HidFieldName');
//    //         aryNames.push($txtName.val());
//    //     });
//    //     return aryNames;
//    // }
//    function _validateSaving() {
//        var aryNamesAlreadyChecked = [];
//        var aryErrors = [];
//        var $detailsTable = _getHidFieldsDetailTable();
//        var blnDoesNameExists = false;
//        var objContDivData = _getDataOfContainerDiv();
//        var objForm = MF_HELPERS.currentObjectHelper.getFormFromApp(
//            objContDivData.formId);
//        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
//        var aryOldHidFieldsDeleted = _getAllOldHidFieldsDeleted();
//        $detailsTable.find('tbody tr')
//        .not('.AdditionRow')
//        .each(function (index) {
//            var strIndex = "At Row Index :" + index + 1 + "</br>";
//            var strError = "";
//            var $txtName = $(this).find('.HidFieldName');
//            var strName = $txtName.val();
//            if (!strName) {
//                strError += "Please enter name for hidden field.<br/>";
//            }
//            else {
//                //first check in html table.then check for other controls in form
//                blnDoesNameExists =
//                $.inArray(strName, aryNamesAlreadyChecked) === -1 ? false : true;
//                if (!blnDoesNameExists) {
//                    blnDoesNameExists = objForm.fnControlNameExist($txtName.val()
//                       , _getAllHidFieldIdsFromContDiv());
//                    if (blnDoesNameExists) {
//                        strError += 'Control name already exist. Please change name of control.</br>';
//                    }
//                    else {
//                        var aryHidFieldWithSameName = $.grep(aryOldHidFieldsDeleted, function (value, index) {
//                            return value.userDefinedName === strName;
//                        });
//                        if (aryHidFieldWithSameName.length > 0) {
//                            aryErrors.push('Control name already exist. Please change name of control.');
//                        }
//                    }
//                }
//                else {
//                    strError += 'Control name already exist. Please change name of control.</br>';
//                }

//            }
//            if (strError.length > 0) {
//                aryErrors.push(strIndex + strError);
//            }
//            aryNamesAlreadyChecked.push(strName);
//        });
//        return aryErrors;
//    }
//    function _nameExistsInDtlsTable(name) {
//        var blnNameExists = false;
//        var $detailsTable = _getHidFieldsDetailTable();
//        $detailsTable.find('tbody tr')
//       .not('.AdditionRow')
//       .each(function (index) {
//           var $txtName = $(this).find('.HidFieldName');
//           if ($txtName.val() === name) {
//               blnNameExists = true;
//               return false;
//           }
//       })
//        return blnNameExists;
//    }
//    function _validateColumnAddition(tblRow/*jquery object table row*/) {
//        if (!tblRow) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
//        var aryErrors = [];
//        var objContDivData = _getDataOfContainerDiv();
//        var $detailsTable = _getHidFieldsDetailTable();
//        var $row = tblRow;
//        var blnDoesNameExists = false;
//        var strName = $row.find('.HidFieldName').val();
//        var aryOldHidFieldsDeleted = [];
//        //var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();

//        var objForm = MF_HELPERS.currentObjectHelper.getFormFromApp(
//            objContDivData.formId);
//        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;

//        if (!strName) {
//            aryErrors.push("Please enter name for hidden field.");
//        }
//        else {
//            //first check in html table.then check for other controls in form
//            blnDoesNameExists = _nameExistsInDtlsTable(strName);
//            if (!blnDoesNameExists) {
//                blnDoesNameExists = objForm.fnControlNameExist(strName
//                    , _getAllHidFieldIdsFromContDiv());

//                if (blnDoesNameExists) {
//                    aryErrors.push('Control name already exist. Please change name of control.');
//                }
//                else {
//                    aryOldHidFieldsDeleted = _getAllOldHidFieldsDeleted();
//                    var aryHidFieldWithSameName = $.grep(aryOldHidFieldsDeleted, function (value, index) {
//                        return value.userDefinedName === strName;
//                    });
//                    if (aryHidFieldWithSameName.length > 0) {
//                        aryErrors.push('Control name already exist. Please change name of control.');
//                    }
//                }
//            }
//            else {
//                aryErrors.push('Control name already exist. Please change name of control.');
//            }
//        }
//        return aryErrors;
//    }
//    function _processAddRowToTable(evnt) {
//        try {
//            var $row = $(this).closest('tr');
//            var errors = _validateColumnAddition($row);
//            if (errors.length > 0)
//                throw new MfError(errors);

//            var objHiddenField = _getDataOfTableRow($row);
//            objHiddenField.fnSetUserDefinedName($row.find('.HidFieldName').val());
//            var $containerDiv = _getDivContainerOfHidFieldDtl();
//            var objContainerDivData = _getDataOfContainerDiv();
//            var $detailsTable = _getHidFieldsDetailTable();

//            _addDetailsRowToTable($detailsTable);


//            var $tbody = $detailsTable.find('tbody');
//            var $tr = $tbody.find('tr').not('.AdditionRow');

//            _fillDataToHtmlTblRow($tr.last(), objHiddenField);
//            objContainerDivData.previousSavedDataCopy.push(objHiddenField);
//            _setEventOfLastAddedRow();
//            _incrementCurrentLargestId();
//            _resetAdditionRow();
//        }
//        catch (error) {
//            if (error instanceof MfError) {
//                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
//            } else {
//                console.log(error);
//            }
//        }

//    }
//    function _resetAdditionRow() {
//        var $detailsTable = _getHidFieldsDetailTable();
//        var $additionRow = $detailsTable.find('tr.AdditionRow');
//        $additionRow.find('.HidFieldName').val("");
//        $additionRow.find('.BindingType').val("0");
//        $additionRow.find('.ObjectName').val("None");
//        _setDataOfAdditionRow();
//    }
//    function _processSaveHiddenFields() {
//        var aryErrors = _validateSaving();
//        if (aryErrors.length > 0) {
//            throw new MfError(aryErrors);
//        }
//        var aryControls = [];
//        var $detailsTable = _getHidFieldsDetailTable();
//        $detailsTable.find('tbody tr')
//        .not('.AdditionRow')
//        .each(function (index) {
//            var $txtName = $(this).find('.HidFieldName');
//            var objHiddenField = _getDataOfTableRow($(this));
//            objHiddenField.fnRename($txtName.val());
//            objHiddenField.fnSetUserDefinedName($txtName.val());
//            aryControls.push(objHiddenField);
//        });
//        var objContDivData = _getDataOfContainerDiv();
//        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
//        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
//        objForm.fnSetControlsOfColumnPanel(_hiddenFieldsSection,
//                _hiddenFieldsColumnPanel, aryControls);
//        var objPropSheetCntrl = PROP_JSON_HTML_MAP.Form.propSheetCntrl.Properties.getHiddenVariables();
//        var aryHidFields = objForm.fnGetAllControlsofSection(_hiddenFieldsSection);
//        if (objPropSheetCntrl) {
//            objPropSheetCntrl.control.val(aryHidFields.length + " " + "Hidden Fields");
//        }
//    }
//    function _processDatabindingObjectSaved() {
//        var objContTblData = _getDataOfContTable();
//        var rowIndex = objContTblData.rowIndex;
//        var $table = _getHidFieldsDetailTable();
//        var $tr = $table.find('tbody tr').eq(rowIndex);
//        var objHidField = _getDataOfTableRow($tr);
//        var objDatabinding = objHidField.fnGetDatabindingObj();
//        if (objDatabinding) {
//            $tr.find('.ObjectName').val(objDatabinding.name);
//        }

//        _removeDataOfContTable();
//    }
//    return {
//        formDetailsHtml: function (form) {
//            if (!form) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
//            var aryHidFields = form.fnGetAllControlsofSection(_hiddenFieldsSection);
//            _formHidDtlsHtml(form.id, aryHidFields);
//            showModalPopUp("divFormHiddenFieldContainer", "Hidden Fields", 600, false, "");
//        },
//        processSaveHiddenFieldsInForm: function (evnt) {
//            _processSaveHiddenFields();
//            _removeDataOfContDiv();
//            closeModalPopUp("divFormHiddenFieldContainer");
//        },
//        processDatabindingObjectSaved: function () {
//            _processDatabindingObjectSaved();
//        },
//        processCloseHiddenFieldDetails: function (evnt) {
//            _removeDataOfContDiv();
//            closeModalPopUp("divFormHiddenFieldContainer");
//        }
//    };
//})();
//$(document).ready(function () {
//    $('#btnAddHiddenFieldsToForm').on('click', function (evnt) {
//        try {
//            mFicientIde.HiddenFieldControlHelper.processSaveHiddenFieldsInForm(evnt);
//        }
//        catch (error) {
//            if (error instanceof MfError) {
//                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
//            } else {
//                console.log(error);
//            }
//        }
//    });
//    $('#btnHidFieldsAdditionCancel').on('click', function (evnt) {
//        try {
//            mFicientIde.HiddenFieldControlHelper.processCloseHiddenFieldDetails(evnt);
//        }
//        catch (error) {
//            if (error instanceof MfError) {
//                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
//            } else {
//                console.log(error);
//            }
//        }
//    });
//});