﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;
namespace mFicientCP
{
    public partial class Setting : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string _maxDeviceForVIPUsers = "1000";
        enum SHOW_PASSWORD_SETTING_PANEL_CONDITION
        {
            NEW,
            CHANGE
        }
        enum SETTING_ERROR_CODES
        {
            MpluginServerNotFound,
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {

                if (hfsValue != string.Empty)
                {
                    hidCId.Value = hfsPart3;
                    bindMaxUserDropDown();
                    fillHidFieldOfAlreadyExistingVIPUsers();
                    fillDeviceautoapprovallJsonlist();
                    bindCompanyGroupsdetails();
                    setTheAccountSettingsOfCompany(false, null);
                    fillUserDetailJsonForHidField();
                    getUserbasedCompany(hfsPart3);
                    SelectTesterUser(hfsPart3);
                    AdditionalPropertyTable();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "document.getElementById('PageCanvasContent').style.display = 'block';$(\"input\").uniform();makeUsergroupAutoComplete();makeTextBoxAutoComplete();makeUserTextBoxAutoComplete();", true);
            }
            else
            {

                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    fillHidFieldOfAlreadyExistingVIPUsers();
                    setTheAccountSettingsOfCompany(false, null);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "showSelectedTabOnPostBack();makeTabAfterPostBack();makeTextBoxAutoComplete();LoadProperyPage();Uniform();");
                    return;
                }
                //  ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "document.getElementById('PageCanvasContent').style.display = 'block';makeTextBoxAutoComplete();showSelectedTabOnPostBack();makeTabAfterPostBack();LoadProperyPage();Uniform();");
            }
        }

        void bindMaxUserDropDown()
        {
            for (int i = 0; i < 5; i++)
            {
                ddlMaxDevicePerUser.Items.Add(new ListItem((i + 1).ToString(), (i + 1).ToString()));
            }
        }

        void bindCompanyGroupsdetails()
        {
            GetGroupDetails objgroupUsers = new GetGroupDetails(hfsPart3);
            objgroupUsers.Process();
            if (objgroupUsers.StatusCode == 0)
            {
                if (objgroupUsers.ResultTable != null && objgroupUsers.ResultTable.Rows.Count > 0)
                {
                    List<GroupDetails> lstUserDetailResponse = new List<GroupDetails>();
                    bool setting = false;
                    string strnullcheck = "";
                    foreach (DataRow user in objgroupUsers.ResultTable.Rows)
                    {
                        GroupDetails objUserDetailMembers = new GroupDetails();
                        objUserDetailMembers.id = Convert.ToString(user["GROUP_ID"]);
                        objUserDetailMembers.name = Convert.ToString(user["GROUP_NAME"]);
                        objUserDetailMembers.fnm = Convert.ToString(user["GROUP_NAME"]);
                        objUserDetailMembers.lnm = Convert.ToString(user["GROUP_NAME"]);
                        lstUserDetailResponse.Add(objUserDetailMembers);

                        strnullcheck = Convert.ToString(user["REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION"]);
                        if (strnullcheck == "") setting = false;
                        else setting = true;

                    }
                    if (setting == true) drpregistrationsetting.SelectedIndex = 1;
                    else drpregistrationsetting.SelectedIndex = 0;
                    GroupDetailsAutoCompleteTextBox objgroupdetailresponse = new GroupDetailsAutoCompleteTextBox();
                    objgroupdetailresponse.data = lstUserDetailResponse;
                    hidgroupid.Value = Utilities.getFinalJsonForAutoCompleteTextBox(
                    Utilities.SerializeJson<GroupDetailsAutoCompleteTextBox>(objgroupdetailresponse)
                    );

                }
            }
        }


        protected void btnSaveVipUsers_Click(object sender, EventArgs e)
        {
            //if (hidUserId.Value != "" && hidUserId.Value != ",")
            //{
            string strUserIds = hidUserId.Value;
            SaveVIPUsersAndDeviceSettings objDeviceSettings = new
                SaveVIPUsersAndDeviceSettings(hfsPart3,
                strUserIds,
               Convert.ToInt32(_maxDeviceForVIPUsers));
            objDeviceSettings.Process();
            if (objDeviceSettings.StatusCode == 0)
            {
                // Utilities.showMessage("User saved successfully", updContainer, DIALOG_TYPE.Info);
                fillHidFieldOfAlreadyExistingVIPUsers();
            }
            else
            {
                Utilities.showMessage(objDeviceSettings.StatusDescription, updContainer, DIALOG_TYPE.Error);
            }
            // }
            //else
            //{
            //    Utilities.showMessage("Please select atleast one user", updContainer, DIALOG_TYPE.Error);
            //}
            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);


        }

        protected void ddlMaxDevices_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void rptUsersDevicesCount_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserId = (Label)e.Item.FindControl("lblUserID");
                Label lblUserName = (Label)e.Item.FindControl("lblUserName");
                Label lblNoOfDevices = (Label)e.Item.FindControl("lblNoOfDevices");
                lblUserId.Text = Convert.ToString(((DataRow)e.Item.DataItem)["USER_ID"]);
                lblUserName.Text = Convert.ToString(((DataRow)e.Item.DataItem)["USER_NAME"]);
                lblNoOfDevices.Text = Convert.ToString(((DataRow)e.Item.DataItem)["No_Of_Devices"]);
            }
        }



        void fillHidFieldOfAlreadyExistingVIPUsers()
        {
            GetVIPUsers objVipUsers = new GetVIPUsers(hfsPart3);
            objVipUsers.Process();
            if (objVipUsers.StatusCode == 0)
            {
                if (objVipUsers.ResultTables != null && objVipUsers.ResultTables.Rows.Count > 0)
                {
                    List<UserDetailMembersForAutoCmptTextBox> lstUserDetailResponse = new List<UserDetailMembersForAutoCmptTextBox>();
                    foreach (DataRow user in objVipUsers.ResultTables.Rows)
                    {

                        UserDetailMembersForAutoCmptTextBox objUserDetailMembers = new UserDetailMembersForAutoCmptTextBox();
                        objUserDetailMembers.id = Convert.ToString(user["USER_ID"]);
                        objUserDetailMembers.name = Convert.ToString(user["FIRST_NAME"]) + " " + Convert.ToString(user["LAST_NAME"]);
                        objUserDetailMembers.fnm = Convert.ToString(user["FIRST_NAME"]);
                        objUserDetailMembers.lnm = Convert.ToString(user["LAST_NAME"]);
                        lstUserDetailResponse.Add(objUserDetailMembers);
                    }
                    UserDetailResponseForAutoCompleteTextBox objUserDetailResponse = new UserDetailResponseForAutoCompleteTextBox();
                    objUserDetailResponse.data = lstUserDetailResponse;

                    hidAlreadySelectedVIPUsers.Value = Utilities.getFinalJsonForAutoCompleteTextBox(
                        Utilities.SerializeJson<UserDetailResponseForAutoCompleteTextBox>(objUserDetailResponse)
                        );
                    //rptVipUserDetails.DataSource = objVipUsers.ResultTables;
                    //rptVipUserDetails.DataBind();
                }
                else
                {
                    hidAlreadySelectedVIPUsers.Value = "[]";
                }
                ddlMaxDevicePerUser.SelectedIndex = ddlMaxDevicePerUser.Items.IndexOf(ddlMaxDevicePerUser.Items.FindByValue(Convert.ToString(objVipUsers.CompanyId)));
                //bindVIPUsersDevDropDownByMaxDevicePerUser();
            }
        }

        protected void btnMaxDeviceSave_Click(object sender, EventArgs e)
        {

            AccountSettings objAccountSettings =
                new AccountSettings(hfsPart3, Convert.ToInt32(ddlMaxDevicePerUser.SelectedValue));
            objAccountSettings.Process();
            if (objAccountSettings.StatusCode == 0)
            {

                // Utilities.showMessage("Max Device Setting saved successfully", updContainer, DIALOG_TYPE.Info);
            }
            else
            {
                Utilities.showMessage(objAccountSettings.StatusDescription, updContainer, DIALOG_TYPE.Error);
            }
            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);

        }


        protected void btn_saveapprovalldetails(object sender, EventArgs e)
        {

            SaveAutoApproval objApproval = new SaveAutoApproval(hfsPart3, hidUserIdautoapproval.Value, hidgroupIdautoapproval.Value, drpregistrationsetting.SelectedValue.ToString());
            objApproval.Process();
            if (objApproval.StatusCode == 0)
            {

                //   Utilities.showMessage("Device auto Approved  saved successfully", updContainer, DIALOG_TYPE.Info);
            }
            else
            {
                Utilities.showMessage(objApproval.StatusDescription, updContainer, DIALOG_TYPE.Error);
            }
            fillDeviceautoapprovallJsonlist();
            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);

        }
        protected void btnAllowDelCmdSave_Click(object sender, EventArgs e)
        {
            if (!chkAllowDeleteCommand.Checked)
            {
                try
                {
                    if (!canDisableChkAllowDelete())
                    {
                        Utilities.showMessage("Delete object already exists.Please delete those objects before disabling Allow Delete Object.",
                            updContainer,
                            DIALOG_TYPE.Error);
                        return;
                    }
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updContainer, DIALOG_TYPE.Error);
                    return;
                }
            }
            AccountSettings objAccountSettings =
                new AccountSettings(hfsPart3,
                    AccountSettings.RECORD_TO_CHANGE.AllowDeleteCmd,
                    chkAllowDeleteCommand.Checked);
            objAccountSettings.Process();
            if (objAccountSettings.StatusCode == 0)
            {
                Utilities.showMessage("Allow delete object setting saved successfully", updContainer, DIALOG_TYPE.Info);
            }
            else
            {
                Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
            }
            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);


        }
        GetAccountSettingsByCompanyId getAccountSettingsOfCompany()
        {
            GetAccountSettingsByCompanyId objAccSettings = new GetAccountSettingsByCompanyId(hfsPart3);
            objAccSettings.Process();
            return objAccSettings;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isPostBack"></param>
        /// <param name="updPanelIfPostBack">If not post back pass null</param>
        void setTheAccountSettingsOfCompany(bool isPostBack, UpdatePanel updPanelIfPostBack)
        {
            GetAccountSettingsByCompanyId objAccSettings = getAccountSettingsOfCompany();
            if (objAccSettings.StatusCode == 0)
            {
                if (objAccSettings.MaxDevicePerUser > 0)
                {
                    ddlMaxDevicePerUser.SelectedIndex = ddlMaxDevicePerUser.Items.IndexOf(
                                                           ddlMaxDevicePerUser.Items.FindByValue(objAccSettings.MaxDevicePerUser.ToString()));
                }
                else
                {
                    ddlMaxDevicePerUser.SelectedIndex = ddlMaxDevicePerUser.Items.IndexOf(
                                                           ddlMaxDevicePerUser.Items.FindByValue("1"));
                }
                chkAllowDeleteCommand.Checked = objAccSettings.AllowDeleteCommand;
                chkAllowActiveDirUsers.Checked = objAccSettings.AllowActiveDirectoryUser;
                chkAutoLogin.Checked = objAccSettings.IsAutoLoginAllowed;
                if (objAccSettings.AllowActiveDirectoryUser == true)
                {
                    bindRptActvDirSettings();
                    showHideActvDirSettingRpt(true);
                }
            }
            else
            {
                if (isPostBack)
                {
                    Utilities.showMessage("Internal server error.Record not found.", updPanelIfPostBack, DIALOG_TYPE.Error);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType()
                        , this.ClientID
                        , @"$.wl_Alert(" + "'" + "Internal server error.Record not found." + "'" + ",'info','#" + "divAlert" + "'" + ");"
                        , true);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        bool canDisableChkAllowDelete()
        {
            GetDBCommandByCompanyId objDbCommand = new GetDBCommandByCompanyId(hfsPart3);
            if (objDbCommand.doesDeleteCommandExists())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void chkAutoLogin_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                AccountSettings objAccountSettings =
                new AccountSettings(hfsPart3,
                    AccountSettings.RECORD_TO_CHANGE.AutoLogin,
                    chkAutoLogin.Checked);
                objAccountSettings.Process();
                if (objAccountSettings.StatusCode == 0)
                {
                    showHideActvDirSettingRpt(true);
                }
                else
                {
                    Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
                }
            }
            catch (MficientException mfex)
            {
                Utilities.showMessage(mfex.Message, updContainer, DIALOG_TYPE.Error);
            }
            catch (Exception)
            {
                Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
            }
            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);
        }
        protected void btnAllowActvDirUserSave_Click(object sender, EventArgs e)
        {
            if (chkAllowActiveDirUsers.Checked)
            {
                AccountSettings objAccountSettings =
                new AccountSettings(hfsPart3,
                    AccountSettings.RECORD_TO_CHANGE.AllowActvDirUser,
                    chkAllowActiveDirUsers.Checked);
                objAccountSettings.Process();
                if (objAccountSettings.StatusCode == 0)
                {
                    // Utilities.showMessage("Allow active directory user setting saved successfully", updContainer, DIALOG_TYPE.Info);
                    showHideActvDirSettingRpt(true);
                }
                else
                {
                    Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
                }

            }
            else
            {
                try
                {
                    List<string> lstDomains = getDomainListFromActDirSettingsRpt();
                    bool anyUserWithDomainIdExists = false;
                    if (lstDomains.Count > 0)
                    {
                        GetUserDetail.getUserDetlByDomainId(lstDomains,
                            hfsPart3, out anyUserWithDomainIdExists);
                        if (anyUserWithDomainIdExists)
                        {
                            Utilities.showMessage("Users exist for Active Directory Settings saved.You must delete those users first before making this change.",
                                updContainer, DIALOG_TYPE.Info);
                            return;
                        }
                    }

                    AccountSettings objAccSettings =
                        new AccountSettings(hfsPart3,
                            AccountSettings.RECORD_TO_CHANGE.AllowActvDirUser,
                            chkAllowActiveDirUsers.Checked);
                    objAccSettings.Process();
                    if (objAccSettings.StatusCode == 0)
                    {
                        if (lstDomains.Count > 0)
                        {
                            DeleteActvDirSetting objDelete =
                                new DeleteActvDirSetting(hfsPart3);
                            objDelete.Process();
                            if (objDelete.StatusCode == 0)
                            {
                                bindRptActvDirSettings();
                                showHideActvDirSettingRpt(false);
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }
                        else
                        {
                            bindRptActvDirSettings();
                            showHideActvDirSettingRpt(false);
                        }
                    }
                    else throw new Exception();

                }
                catch
                {
                    Utilities.showMessage("Internal server error.",
                        updContainer, DIALOG_TYPE.Error);
                }
            }

            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);


        }
        protected void btnConfirmYes_Click(object sender, EventArgs e)
        {

        }
        List<string> getDomainListFromActDirSettingsRpt()
        {
            List<string> lstDomains = new List<string>();
            foreach (RepeaterItem item in rptActvDirSettings.Items)
            {
                lstDomains.Add(((Label)item.FindControl("lblDomainId")).Text);
            }
            return lstDomains;
        }
        void showActDirSttngsPopUp()
        {
            DataSet dsMpluginAgents;
            if (mPluginAgentsExistForCompany(out dsMpluginAgents))
            {
                bindMpluginDropDown(dsMpluginAgents.Tables[0], true);
                Utilities.showModalPopup("divSelectAgentAndDomain",
                    updContainer, "Active Directory Settings",
                    "400", false);
                clearItemsOfDomainDrpDwn();
            }
            else
            {
                //chkAllowActiveDirUsers.Checked = false;
                Utilities.showMessage("Please create an mPlugin Agent before allowing active directory users.",
                    updContainer, DIALOG_TYPE.Info);
            }
        }
        void showActDirSttngsPopUpForEditSetting(string mplugInAgentId,
            string domainName)
        {
            DataSet dsMpluginAgents;
            if (mPluginAgentsExistForCompany(out dsMpluginAgents))
            {
                bindMpluginDropDown(dsMpluginAgents.Tables[0], true);
                ddlMpluginAgents.SelectedIndex =
                    ddlMpluginAgents.Items.IndexOf(ddlMpluginAgents.Items.FindByValue(mplugInAgentId));
                bindDomainListDropdown();
                ddlDomains.SelectedIndex =
                    ddlDomains.Items.IndexOf(ddlDomains.Items.FindByText(domainName));
                Utilities.showModalPopup("divSelectAgentAndDomain",
                    updContainer, "Active Directory Settings",
                    "300", false);
            }
            else
            {
                Utilities.showMessage("Internal server error.",
                    updContainer, DIALOG_TYPE.Info);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="System.Exception">Thrown when no record is found</exception>
        bool mPluginAgentsExistForCompany(out DataSet mPluginAgentsForCompany)
        {

            mPluginAgents objAgents = new mPluginAgents();

            mPluginAgentsForCompany = objAgents.GetMpluginAgents(hfsPart3);

            if (mPluginAgentsForCompany == null) throw new Exception("Record not found");

            if (mPluginAgentsForCompany.Tables[0].Rows.Count > 0) return true;
            else return false;

        }

        protected void rptActvDirSettings_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "delete":
                    processDeleteActvDirSetting(e);
                    break;
                case "edit":
                    processEditActvDirSetting(e);
                    break;
            }
        }
        void processDeleteActvDirSetting(RepeaterCommandEventArgs e)
        {
            try
            {
                string domainId = Convert.ToString(e.CommandArgument);
                bool anyUserWithDomainIdExists = false;
                GetUserDetail.getUserDetlByDomainId(domainId,
                    hfsPart3, out anyUserWithDomainIdExists);
                if (anyUserWithDomainIdExists)
                {
                    Utilities.showMessage("Users exists for this Active Directory Setting.You must delete those users first before deleting the setting.",
                        updContainer, DIALOG_TYPE.Info);
                }
                else
                {
                    DeleteActvDirSetting objDeleteSetting =
                        new DeleteActvDirSetting(domainId, hfsPart3);
                    objDeleteSetting.Process();
                    if (objDeleteSetting.StatusCode == 0)
                    {
                        //Utilities.showMessage("Settings deleted successfully.",
                        //  updSelectAgntAndDomain, DIALOG_TYPE.Info);
                        bindRptActvDirSettings();
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.showMessage("Internal server error",
                    updContainer, DIALOG_TYPE.Error);
            }
        }
        void processEditActvDirSetting(RepeaterCommandEventArgs e)
        {
            try
            {
                string strMPluginId = ((Label)e.Item.FindControl("lblMpluginId")).Text;
                string strDomainName = ((Label)e.Item.FindControl("lblDomainName")).Text;
                showActDirSttngsPopUpForEditSetting(strMPluginId, strDomainName);
            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    if (ex.Message == ((int)SETTING_ERROR_CODES.MpluginServerNotFound).ToString())
                    {
                        Utilities.showMessage(this.getErrorMsgFromErrorCode(ex.Message),
                            updContainer, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessage(this.getErrorMsgFromErrorCode(ex.Message),
                            updContainer, DIALOG_TYPE.Error);
                    }
                }
                else
                {
                    Utilities.showMessage(ex.Message,
                        updContainer, DIALOG_TYPE.Error);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                    updContainer, DIALOG_TYPE.Info);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="System.Exception">Thrown when no record is found</exception>
        void bindRptActvDirSettings()
        {
            GetActvDirDomainSettings objActvDirDomainSettings =
                new GetActvDirDomainSettings(hfsPart3);
            objActvDirDomainSettings.Process();
            if (objActvDirDomainSettings.StatusCode == 0)
            {
                rptActvDirSettings.DataSource
                    = objActvDirDomainSettings.ActDirDomainSettings;
                rptActvDirSettings.DataBind();
            }
            else
            {
                rptActvDirSettings.DataSource = null;
                rptActvDirSettings.DataBind();
                throw new Exception("Record not found.");
            }

        }
        void bindMpluginDropDown(DataTable mPluginTbl, bool showSelectText)
        {
            if (mPluginTbl == null) throw new ArgumentNullException();
            ddlMpluginAgents.Items.Clear();
            ddlMpluginAgents.DataSource = mPluginTbl;
            ddlMpluginAgents.DataTextField = "MP_AGENT_NAME";
            ddlMpluginAgents.DataValueField = "MP_AGENT_ID";
            ddlMpluginAgents.DataBind();
            if (showSelectText)
            {
                ddlMpluginAgents.Items.Insert(0,
                    new ListItem("Select", "-1"));
            }
        }
        void bindDomainListDropdown()
        {
            string strRqst, strUrl = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            mPluginAgents objAgent = new mPluginAgents();
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + hfsPart3 + "\",\"agtnm\":\""
                + ddlMpluginAgents.SelectedItem.Text.Trim() + "\",\"agtpwd\":\""
                + objAgent.GetMpluginAgentPassword(hfsPart3, ddlMpluginAgents.SelectedItem.Text.Trim())
                + "\"}}";

            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(hfsPart3);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    strUrl = strUrl + "/GetDomainList.aspx?d=" + Utilities.UrlEncode(strRqst);
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        List<string> lstDomains = new List<string>();
                        GetDomainListResp obj = new GetDomainListResp(oResponse.ResponseText);
                        if (obj.Code == "0")
                        {
                            lstDomains = obj.Data;
                            ddlDomains.DataSource = lstDomains;
                            ddlDomains.DataBind();
                            ddlDomains.Items.Insert(0, new ListItem("Select", "-1"));
                            if (lstDomains.Count == 0)
                            {
                                throw new MficientException("Domains not found");
                            }
                        }
                        else
                        {
                            throw new MficientException("Internal server error.");
                        }
                    }
                    else if (oResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        throw new MficientException(((int)SETTING_ERROR_CODES.MpluginServerNotFound).ToString());

                    }
                }
            }
            else if (objServerUrl.StatusCode != 0 &&
                objServerUrl.ResultTable != null)
            {
                throw new MficientException("Internal server error.");
            }
            else
            {
                throw new MficientException("Internal server error.");
            }
        }
        protected void ddlMpluginAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlMpluginAgents.SelectedValue == "-1")
                {
                    clearItemsOfDomainDrpDwn();
                }
                else
                {
                    bindDomainListDropdown();
                }
            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    if (ex.Message == ((int)SETTING_ERROR_CODES.MpluginServerNotFound).ToString())
                    {
                        Utilities.showMessage(this.getErrorMsgFromErrorCode(ex.Message),
                            updContainer, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessage(this.getErrorMsgFromErrorCode(ex.Message),
                            updContainer, DIALOG_TYPE.Error);
                    }
                }
                else
                {
                    Utilities.showMessage(ex.Message,
                        updContainer, DIALOG_TYPE.Error);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error",
                    updSelectAgntAndDomain, DIALOG_TYPE.Error);
            }
        }
        protected void btnActDirSttngsSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strValidateError = validateActDirSttngsForm();
                if (String.IsNullOrEmpty(strValidateError))
                {
                    if (isTheCurrntSettingAlreadySaved(ddlMpluginAgents.SelectedValue,
                        ddlDomains.SelectedItem.Text))
                    {
                        Utilities.showAlert("Setting already saved.",
                        "divSelectAgntDomError", updSelectAgntAndDomain);
                        return;
                    }
                    else
                    {
                        SaveActvDirSettings objSaveActDirSettings =
                            new SaveActvDirSettings(ddlDomains.SelectedItem.Text,
                                ddlMpluginAgents.SelectedValue, hfsPart3);
                        objSaveActDirSettings.Process();
                        if (objSaveActDirSettings.StatusCode == 0)
                        {
                            Utilities.showMessage("Setting saved successfully.",
                                updSelectAgntAndDomain, DIALOG_TYPE.Info);
                            Utilities.closeModalPopUp("divSelectAgentAndDomain", updSelectAgntAndDomain);
                            bindRptActvDirSettings();
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                }
                else
                {
                    Utilities.showAlert(strValidateError,
                        "divSelectAgntDomError", updSelectAgntAndDomain);
                }
            }
            catch
            {
                Utilities.showAlert("Internal server error.",
                    "divSelectAgntDomError", updSelectAgntAndDomain);
            }
        }
        public string validateActDirSttngsForm()
        {
            string strMessage = "";
            if (ddlMpluginAgents.SelectedValue == "-1")
            {
                strMessage += "Please select an agent." + "<br />";
            }
            if (ddlDomains.Items.Count == 0 || ddlDomains.SelectedValue == "-1")
            {
                strMessage += "Please select a domain." + "<br />";
            }
            return strMessage;

        }
        bool isTheCurrntSettingAlreadySaved(string mpluginId,
            string domainName)
        {
            GetActvDirDomainSettings objActDirSettngs =
                new GetActvDirDomainSettings(domainName, mpluginId, hfsPart3);
            objActDirSettngs.Process();
            if (objActDirSettngs.StatusCode != 0) throw new Exception();
            if (objActDirSettngs.ActDirDomainSettings.Rows.Count > 0) return true;
            else return false;
        }
        protected void lnkActvDirAddNewDomain_Click(object sender, EventArgs e)
        {
            try
            {

                showActDirSttngsPopUp();
            }
            catch (MficientException ex)
            {
                Utilities.showMessage(ex.Message,
                        updContainer,
                        DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                    updContainer,
                    DIALOG_TYPE.Error);
            }
        }
        void showHideActvDirSettingRpt(bool show)
        {
            if (show) pnlActiveDirSettingsRptBox.Visible = true;
            else pnlActiveDirSettingsRptBox.Visible = false;
        }
        void clearItemsOfDomainDrpDwn()
        {
            ddlDomains.Items.Clear();
            ddlDomains.Items.Insert(0,
                new ListItem("Select", "-1"));
        }

        string getJsonForUserList()
        {
            string strJsonForAutoCompleteText = "";
            GetUserDetail objUsrDetail = new GetUserDetail();
            List<MFEMobileUser> lstUsers = objUsrDetail.getUserDetail(hfsPart3);
            if (objUsrDetail.StatusCode == 0)
            {
                if (lstUsers != null && lstUsers.Count > 0)
                {
                    List<UserDetailMembersForAutoCmptTextBox> lstUserDetailResponse = new List<UserDetailMembersForAutoCmptTextBox>();
                    foreach (MFEMobileUser user in lstUsers)
                    {

                        UserDetailMembersForAutoCmptTextBox objUserDetailMembers = new UserDetailMembersForAutoCmptTextBox();
                        objUserDetailMembers.id = user.UserId;
                        objUserDetailMembers.name = user.FirstName + " " + user.LastName;
                        objUserDetailMembers.fnm = user.FirstName;
                        objUserDetailMembers.lnm = user.LastName;
                        lstUserDetailResponse.Add(objUserDetailMembers);
                    }
                    UserDetailResponseForAutoCompleteTextBox objUserDetailResponse = new UserDetailResponseForAutoCompleteTextBox();
                    objUserDetailResponse.data = lstUserDetailResponse;
                    strJsonForAutoCompleteText = Utilities.SerializeJson<UserDetailResponseForAutoCompleteTextBox>(objUserDetailResponse);
                    strJsonForAutoCompleteText = Utilities.getFinalJsonForAutoCompleteTextBox(strJsonForAutoCompleteText);
                }
            }
            return strJsonForAutoCompleteText;
        }


        string getJsonForAutoupdateUserList()
        {
            string strJsonForAutoCompleteText = "";
            GetUserDetail objUsrDetail = new GetUserDetail(true, hfsPart3);
            if (objUsrDetail.StatusCode == 0)
            {
                if (objUsrDetail.ResultTable != null && objUsrDetail.ResultTable.Rows.Count > 0)
                {
                    List<UserDetailMembersForAutoCmptTextBox> lstUserDetailResponse = new List<UserDetailMembersForAutoCmptTextBox>();
                    foreach (DataRow user in objUsrDetail.ResultTable.Rows)
                    {

                        UserDetailMembersForAutoCmptTextBox objUserDetailMembers = new UserDetailMembersForAutoCmptTextBox();
                        objUserDetailMembers.id = Convert.ToString(user["USER_IDS"]);
                        objUserDetailMembers.name = Convert.ToString(user["FIRST_NAME"]) + " " + Convert.ToString(user["LAST_NAME"]);
                        objUserDetailMembers.fnm = Convert.ToString(user["FIRST_NAME"]);
                        objUserDetailMembers.lnm = Convert.ToString(user["LAST_NAME"]);
                        lstUserDetailResponse.Add(objUserDetailMembers);
                    }
                    UserDetailResponseForAutoCompleteTextBox objUserDetailResponse = new UserDetailResponseForAutoCompleteTextBox();
                    objUserDetailResponse.data = lstUserDetailResponse;

                    strJsonForAutoCompleteText = Utilities.getFinalJsonForAutoCompleteTextBox(
                        Utilities.SerializeJson<UserDetailResponseForAutoCompleteTextBox>(objUserDetailResponse)
                        );

                }
            }
            return strJsonForAutoCompleteText;
        }

        string getJsonForAutoupdateGroupList()
        {
            string strJsonForAutoCompleteText = "";
            GetGroupDetails objgroupUser = new GetGroupDetails(hfsPart3, true);
            bool setting = false;
            if (objgroupUser.StatusCode == 0)
            {
                if (objgroupUser.ResultTable != null && objgroupUser.ResultTable.Rows.Count > 0)
                {
                    List<GroupDetails> lstUserDetailResponse = new List<GroupDetails>();
                    foreach (DataRow user in objgroupUser.ResultTable.Rows)
                    {
                        GroupDetails objUserDetailMembers = new GroupDetails();
                        objUserDetailMembers.id = Convert.ToString(user["GROUP_IDS"]);
                        objUserDetailMembers.name = Convert.ToString(user["GROUP_NAME"]);
                        objUserDetailMembers.fnm = Convert.ToString(user["GROUP_NAME"]);
                        objUserDetailMembers.lnm = Convert.ToString(user["GROUP_NAME"]);
                        setting = Convert.ToBoolean(user["REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION"]);
                        lstUserDetailResponse.Add(objUserDetailMembers);
                    }
                    if (setting == true) drpregistrationsetting.SelectedIndex = 1;
                    else drpregistrationsetting.SelectedIndex = 0;
                    GroupDetailsAutoCompleteTextBox objgroupdetailresponse = new GroupDetailsAutoCompleteTextBox();
                    objgroupdetailresponse.data = lstUserDetailResponse;
                    strJsonForAutoCompleteText = Utilities.getFinalJsonForAutoCompleteTextBox(
                    Utilities.SerializeJson<GroupDetailsAutoCompleteTextBox>(objgroupdetailresponse)
                    );

                }
            }
            return strJsonForAutoCompleteText;
        }
        void fillUserDetailJsonForHidField()
        {
            hidAutoCmpltAllUserList.Value = getJsonForUserList();
        }

        void fillDeviceautoapprovallJsonlist()
        {
            hidexistingautoappoveduserlist.Value = getJsonForAutoupdateUserList();
            hidexistingautoappovedgrouplist.Value = getJsonForAutoupdateGroupList();
        }

        private void getUserbasedCompany(string CompanyId)
        {
            drpuserid.Items.Clear();
            GetUserDetail objgetdtls = new GetUserDetail(CompanyId);
            if (objgetdtls.StatusCode == 0)
            {
                drpuserid.DataSource = objgetdtls.ResultTable;
                drpuserid.DataTextField = "username";
                drpuserid.DataValueField = "USER_ID";
                drpuserid.DataBind();
                drpuserid.Items.Insert(0, new ListItem("Select", "-1"));
            }
            else
            {
                drpuserid.Items.Insert(0, new ListItem("Select", "-1"));

            }

        }

        private void SelectTesterUser(string CompanyId)
        {
            GetUserDetail objgetdtls1 = new GetUserDetail(CompanyId, true);
            if (objgetdtls1.StatusCode == 0)
            {
                string struserId = "";
                if (objgetdtls1.ResultTable.Rows.Count > 0)
                {
                    foreach (DataRow row in objgetdtls1.ResultTable.Rows)
                    {
                        struserId = Convert.ToString(row["USER_ID"]);
                        break;
                    }
                    drpuserid.SelectedValue = struserId;
                }
            }
        }

        #region Helper function For Error Messages
        string getErrorMsgFromErrorCode(string errorCode)
        {
            string strErrorMsg = String.Empty;
            SETTING_ERROR_CODES eErrorCode =
                (SETTING_ERROR_CODES)Enum.Parse(typeof(SETTING_ERROR_CODES), errorCode);
            switch (eErrorCode)
            {
                case SETTING_ERROR_CODES.MpluginServerNotFound:
                    strErrorMsg = "mPlugin server not found.";
                    break;
                default:
                    strErrorMsg = String.Empty;
                    break;
            }
            return strErrorMsg;
        }
        bool isErrorADefinedErrorCode(string errorCode)
        {
            SETTING_ERROR_CODES eErrorCode;
            try
            {
                eErrorCode = (SETTING_ERROR_CODES)Enum.Parse(typeof(SETTING_ERROR_CODES), errorCode);
                if (Enum.IsDefined(typeof(SETTING_ERROR_CODES), eErrorCode))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (ArgumentException)
            {
                return false;
            }
        }
        #endregion

        protected void btntest_Click(object sender, EventArgs e)
        {
            if (drpuserid.SelectedValue == "-1")
            {
                Utilities.showMessage("Please select atleast one user", updContainer, DIALOG_TYPE.Error);
            }
            else
            {
                UpdateUserCompleteDetail objupdatedtls = new UpdateUserCompleteDetail(hfsPart3, drpuserid.SelectedValue.ToString(), true);
                if (objupdatedtls.StatusCode == 0)
                {
                    drpuserid.SelectedValue = "-1";
                    // Utilities.showMessage("Tested user successfully", updContainer, DIALOG_TYPE.Info);
                    SelectTesterUser(hfsPart3);

                }
                else
                {
                    Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
                }

            }
            ScriptManager.RegisterStartupScript(updContainer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);
        }


        private void AdditionalPropertyTable()
        {

            GetEnterpriseAdditionalDefinition objGetWsCommand = new GetEnterpriseAdditionalDefinition(hfsPart3);
            if (objGetWsCommand.StatusCode == 0)
            {
                if (objGetWsCommand.ResultTable != null)
                {
                    if (objGetWsCommand.ResultTable.Rows.Count > 0)
                    {
                        hidData.Value = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["ADDITIONAL_PARAMETERS_META"]);
                    }
                    // ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"BindforAll();", true);
                }
                else
                {
                    hidData.Value = "";
                    //hidAuthenticationData.Value = "";
                }
            }

        }



        protected void btnDbConnRowIndex_Click(object sender, EventArgs e)
        {
            if (this.hidUpdateTag.Value.Length == 0) return;
            string[] stroperation = hidUpdateTag.Value.Split('/');
            if (stroperation[2] == "1" || stroperation[2] == "2")
            {
                SaveAdditionalProperties(hfsPart3, hidData.Value, stroperation);
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "LoadProperyPage();Uniform();makeUsergroupAutoComplete();makeUserTextBoxAutoComplete();", true);

            }
        }

        private void SaveAdditionalProperties(string CompanyId, string _JsonData, string[] Opration)
        {
            SaveEnterpriseAdditionalDefinition objsave = new SaveEnterpriseAdditionalDefinition(CompanyId, _JsonData, EnterpriseAdditionalDefinitionType.Properties);
            if (objsave.StatusCode != 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCfmMessage').html('Data cannot be saved.Internal server error.');$('#aCFmessage').html('');showModalPopUp('SubProcConfirmBoxMessage','Error','350',false);Uniform();", true);
            }
        }

    }
}