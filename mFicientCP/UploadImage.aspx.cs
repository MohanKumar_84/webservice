﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;

namespace mFicientCP
{
    public partial class UploadImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdf.Value = Convert.ToString(Request.QueryString["id"]);
        }
        protected void btnImportFile_Click(object sender, EventArgs e)
        {
            if (fupd.HasFile)
            {
                try
                {
                    string fileName = fupd.FileName;
                    string fileExtension = fileName.Split('.')[1].ToLower();

                    if (!(fileExtension == @"jpg" || fileExtension == @"jpeg" || fileExtension == @"png"))
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.uploadImagesError(6);parent.hideWaitModal();", true);
                        return;
                    }

                    if (fupd.PostedFile.ContentLength > (1024 * 1024))
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.uploadImagesError(0);parent.hideWaitModal();", true);
                        return;
                    }

                    using (Stream memStream = new MemoryStream(fupd.FileBytes))
                    {
                        using (System.Drawing.Image img = System.Drawing.Image.FromStream(memStream))
                        {
                            if (img.Width > 2048)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.uploadImagesError(2);parent.hideWaitModal();", true);
                                return;
                            }
                            if (img.Height > 2048)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.uploadImagesError(4);parent.hideWaitModal();", true);
                                return;
                            }
                        }
                        //string key = @"Apps\CompanyImages\" + Utilities.DecryptString(hdf.Value).Split(',')[3] + @"\mediafiles\" + fileName;
                        //MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
                        //S3BucketFileManager objS3FileManager = new S3BucketFileManager("mficient", objS3BucketDtl.AccessKey, objS3BucketDtl.SecretAccessKey);
                        //objS3FileManager.UploadFile(key + "/", memStream);


                    }

                    //string strPath = Server.MapPath(@"~\CompanyImages\" + Utilities.DecryptString(hdf.Value).Split(',')[3] + @"\mediafiles");

                    //if (!System.IO.Directory.Exists(strPath))
                    // System.IO.Directory.CreateDirectory(strPath);

                    //fupd.SaveAs(strPath + @"\" + fileName);

                    MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
                    S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                            MficientConstants.MF_S3_MEDIA_FILES_BUCKET_NAME,
                            objS3BucketDtl.AccessKey,
                            objS3BucketDtl.SecretAccessKey);

                    objS3FileManager.UploadFile(
                        S3Bucket.GetCompanyMediaFilesFolderPath(
                            Utilities.DecryptString(hdf.Value).Split(',')[3].ToLower(),
                            fileName
                        ),
                        fupd.PostedFile.InputStream
                    );

                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.ImageShow();parent.hideWaitModal();", true);
                }
                catch
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.uploadImagesError(5);parent.hideWaitModal();", true);
                }
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.hideWaitModal();", true);
        }
    }
}