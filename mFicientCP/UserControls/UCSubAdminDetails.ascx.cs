﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientCP.UserControls
{
    public partial class UCSubAdminDetails : System.Web.UI.UserControl
    {
        string _subAdminId, _adminId, _userName, _companyID, _mobileuser;
        bool _isBlocked;

        public string AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }

        public string CompanyID
        {
            get { return _companyID; }
            set { _companyID = value; }
        }

        public string SubAdminId
        {
            get { return _subAdminId; }
            set { _subAdminId = value; }
        }
        public bool IsBlocked
        {
            get { return _isBlocked; }
            set { _isBlocked = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string Mobileuser
        {
            get { return _mobileuser; }
            set { _mobileuser = value; }
        }



        public delegate void ActionClick(object sender, EventArgs e);
        public event ActionClick OnEditClick;
        public event ActionClick OnBlockClick;
        public event ActionClick OnUnBlockClick;
        public event ActionClick OnDivDeptEditClick;
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        public void rememberSubAdminId()
        {
            try
            {
                if (!String.IsNullOrEmpty(SubAdminId))
                {
                    hidSubAdminId.Value = SubAdminId;
                }
                if (!String.IsNullOrEmpty(AdminId))
                {
                    hidAdminId.Value = AdminId;
                }
                if (!String.IsNullOrEmpty(CompanyID))
                {
                    hidCompany.Value = CompanyID;
                }
                if (IsBlocked)
                {
                    lnkUnblockSubAdmin.Visible = true;
                    lnkBlockSubAdmin.Visible = false;
                }
                else
                {
                    lnkBlockSubAdmin.Visible = true;
                    lnkUnblockSubAdmin.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void lnkEditSubAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                if (OnEditClick != null)
                {
                    LinkButton lnkEdit = (LinkButton)sender;
                    //subadminid,UserName,isBlocked
                    lnkEdit.CommandArgument = hidSubAdminId.Value + ";" + lblUserName.Text.Trim() + ";" + (lblAccountType.Text.ToLower() == "blocked" ? true : false).ToString();
                    OnEditClick(sender, e);
                }
            }
            catch 
            {
                //Utilities.showMessage(ex.Message, this.Page, DIALOG_TYPE.Error);
            }
        }
        protected void lnkBlockSubAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                if (OnBlockClick != null)
                {
                    LinkButton lnkBlock = (LinkButton)sender;
                    lnkBlock.CommandArgument = hidSubAdminId.Value + ";" + lblUserName.Text.Trim() + ";" + lblName.Text.Trim();
                    OnBlockClick(sender, e);
                }
            }
            catch 
            {
                //Utilities.showMessage(ex.Message, this.Page, DIALOG_TYPE.Error);
            }
        }
        protected void lnkUnblockSubAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                if (OnUnBlockClick != null)
                {
                    LinkButton lnkUnBlock = (LinkButton)sender;
                    lnkUnBlock.CommandArgument = hidSubAdminId.Value + ";" + lblUserName.Text.Trim() + ";" + lblName.Text.Trim();
                    OnUnBlockClick(sender, e);
                }
            }
            catch 
            {
                //Utilities.showMessage(ex.Message, this.Page, DIALOG_TYPE.Error);
            }
        }
        protected void lnkDivisionDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                if (OnDivDeptEditClick != null)
                {
                    LinkButton lnkDivDept = (LinkButton)sender;
                    lnkDivDept.CommandArgument = hidSubAdminId.Value;
                    OnDivDeptEditClick(sender, e);
                }
            }
            catch
            {
                //Utilities.showMessage(ex.Message, this.Page, DIALOG_TYPE.Error);
            }
        }
        public void getSubAdminDetailsAndFillData()
        {
            try
            {

                GetSubAdminCompleteDetail objSubAdminDetail = new GetSubAdminCompleteDetail(hidAdminId.Value, hidSubAdminId.Value, hidCompany.Value);
                objSubAdminDetail.Process();
                if (objSubAdminDetail.StatusCode == 0)
                {
                    DataSet dsSubAdminDetails = objSubAdminDetail.ResultTables;
                    if (dsSubAdminDetails != null && dsSubAdminDetails.Tables.Count > 0)
                    {
                        fillSubAdminDetails(dsSubAdminDetails);
                    }
                    else
                    {
                        throw new Exception("Result not found.Internal server error");
                    }
                }
                else
                {
                    throw new Exception(objSubAdminDetail.StatusDescription);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void fillSubAdminDetails(DataSet subAdminDtlsDS)
        {
            try
            {
                if (subAdminDtlsDS.Tables[0].Rows.Count > 0)
                {
                    lblmappedtheuser.Text = "";
                    lblName.Text = (string)subAdminDtlsDS.Tables[0].Rows[0]["FULL_NAME"];
                    lblUserName.Text = (string)subAdminDtlsDS.Tables[0].Rows[0]["USER_NAME"];
                    lblEmail.Text = (string)subAdminDtlsDS.Tables[0].Rows[0]["EMAIL"];
                    lblAccountType.Text = (Convert.ToBoolean(subAdminDtlsDS.Tables[0].Rows[0]["IS_BLOCKED"]) == true) ? "Blocked" : "Active";
                    lblPushMessage.Text = (Convert.ToBoolean(subAdminDtlsDS.Tables[0].Rows[0]["PUSH_MESSAGE"])) ? "Yes" : "No";

                    if (subAdminDtlsDS.Tables[2].Rows.Count > 0)
                    {
                        lblUserManagement.Text = "";
                        lblFormManagement.Text = "";
                        foreach (DataRow row in subAdminDtlsDS.Tables[2].Rows)
                        {
                            if (((string)row["ROLE_ID"]).ToLower() == "fm")
                            {
                                lblFormManagement.Text = "Yes";
                            }
                            if (((string)row["ROLE_ID"]).ToLower() == "um")
                            {
                                lblUserManagement.Text = "Yes";
                            }
                        }
                        if (lblUserManagement.Text == "")
                        {
                            lblUserManagement.Text = "No";
                        }
                        if (lblFormManagement.Text == "")
                        {
                            lblFormManagement.Text = "No";
                        }
                    }
                    lblmappedtheuser.Text = "No user mapped";
                    foreach (DataRow row in subAdminDtlsDS.Tables[3].Rows)
                    {
                        string cellData = row["USER_ID"].ToString();
                        string cellData1 = row["username"].ToString();
                        if (Convert.ToString(subAdminDtlsDS.Tables[0].Rows[0]["MOBILE_USER"]) == cellData)
                        {
                            lblmappedtheuser.Text = cellData1;
                            break;
                        }
                    }
                }
                else
                {
                    throw new Exception("Record not found.Internal server error");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}