﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs"
    Inherits="mFicientCP.UserControls.UserDetails" %>
<style type="text/css">
    .align
    {
        text-align: left;
    }
    
    .gvclass table th
    {
        text-align: left;
    }
</style>
<div id="divUserDetails" style="width: 100%;">
    <div class="modalPopUpDetails">
        <div id="DivUserDetails" style="width: 100%;">
            <div id="divUserDtlsHeader" class="innerHeader" style="border-bottom: 1px solid">
                <asp:Label ID="lblUserDtlsHeader" runat="server" Text="Details"></asp:Label>
                <div id="divUCActivityButtons" style="float: right; margin-bottom: 2px;">
                    <asp:LinkButton ID="lnkEditUser" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                        OnClick="lnkEditUser_Click">edit</asp:LinkButton>&nbsp;<span id="ucSeparatorOne">|</span>&nbsp;
                    <asp:LinkButton ID="lnkBlockUser" runat="server" OnClick="lnkBlockUser_Click" OnClientClick="return showConfirmation('Are you sure you want to block this User');">block</asp:LinkButton>
                    <asp:LinkButton ID="lnkUnblockUser" runat="server" OnClick="lnkUnblockUser_Click"
                        OnClientClick="return showConfirmation('Are you sure you want to unblock this User');">unblock</asp:LinkButton>
                </div>
                <div class="clear">
                </div>
            </div>
            <div>
                <fieldset style="padding-bottom: 0px;">
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForName" runat="server" Text="Name"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width: auto;">
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForEmail" runat="server" Text="Email"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label3" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width: auto;">
                            <asp:Label ID="lblEmail" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForMobile" runat="server" Text="Mobile"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label4" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width: auto;">
                            <asp:Label ID="lblMobile" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForUserName" runat="server" Text="Username"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label6" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width: auto;">
                            <asp:Label ID="lblUserName" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForAccountType" runat="server" Text="Account Type"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label18" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width: auto;">
                            <asp:Label ID="lblAccountType" runat="server"></asp:Label>
                        </div>
                    </section>
                    <%--<section>
                        <div class="column1">
                            <asp:Label ID="lblForMessenger" runat="server" Text="Messenger"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label19" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width:auto;">
                            <asp:Label ID="lblMessenger" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForDesktopMessenger" runat="server" Text="Desktop Messenger"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label21" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width:auto;">
                            <asp:Label ID="lblDesktopMessenger" runat="server"></asp:Label>
                        </div>
                    </section>--%>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForGroups" runat="server" Text="Groups"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label7" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3" style="width: auto;">
                            <asp:Label ID="lblGroups" runat="server" Width="505px"></asp:Label>
                        </div>
                    </section>
                </fieldset>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="divappdetails" style="width: 100%;">
            <div id="div3" class="innerHeader" style="border-bottom: 1px solid;">
                <asp:Label ID="lblAppDeatils" runat="server" Text="App Details"></asp:Label>
            </div>
            <fieldset>
                <section style="max-height: 200px; overflow: auto; text-align: left">
                    <div class="gvclass" style="font-family: inherit; font-family: inherit">
                        <asp:GridView ID="gvdeatails" runat="server" AutoGenerateColumns="false" PageSize="100"
                            Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="App">
                                    <ItemTemplate>
                                        <asp:Label ID="lblapp" runat="server" Text='<%#Eval("WF_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" CssClass="align" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Groups" HeaderStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblgroupname" runat="server" Text='<%#Eval("GROUP_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="align" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </section>
            </fieldset>
        </div>
        <div class="clear" style="margin-top: 2px;">
        </div>
    </div>
    <asp:HiddenField ID="hidUserId" runat="server" />
</div>
