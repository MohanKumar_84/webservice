﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientCP.UserControls
{
    public partial class UserDetails : System.Web.UI.UserControl
    {
        string _userId, _userName, _companyId;
        bool _isBlocked;

        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public bool IsBlocked
        {
            get { return _isBlocked; }
            set { _isBlocked = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        public delegate void ActionClick(object sender, EventArgs e);
        public event ActionClick OnEditClick;
        public event ActionClick OnBlockClick;
        public event ActionClick OnUnBlockClick;

        protected void Page_Load(object sender, EventArgs e)
        {
            //the command argument of both the buttons should be 
            //if (!Page.IsPostBack)
            //{
            //    lnkEditUser.CommandArgument = UserId;
            //    lnkBlockUser.CommandArgument = UserId;
            //}
            //if(!String.IsNullOrEmpty(UserId))
            //{
            //    lnkEditUser.CommandArgument = UserId;
            //    lnkBlockUser.CommandArgument = UserId;
            //}
        }
        public void rememberUserId()
        {
            if (!String.IsNullOrEmpty(UserId) && !String.IsNullOrEmpty(UserName))
            {
                hidUserId.Value = UserId + ',' + UserName;
            }
            if (IsBlocked)
            {
                lnkUnblockUser.Visible = true;
                lnkBlockUser.Visible = false;
            }
            else
            {
                lnkBlockUser.Visible = true;
                lnkUnblockUser.Visible = false;
            }
        }
        protected void lnkEditUser_Click(object sender, EventArgs e)
        {
            if (OnEditClick != null)
            {
                LinkButton lnkEdit = (LinkButton)sender;
                lnkEdit.CommandArgument = hidUserId.Value;
                OnEditClick(sender, e);
            }
        }

        protected void lnkBlockUser_Click(object sender, EventArgs e)
        {
            if (OnBlockClick != null)
            {
                LinkButton lnkBlock = (LinkButton)sender;
                lnkBlock.CommandArgument = hidUserId.Value;
                OnBlockClick(sender, e);
            }
        }

        protected void lnkUnblockUser_Click(object sender, EventArgs e)
        {
            if (OnUnBlockClick != null)
            {
                LinkButton lnkUnBlock = (LinkButton)sender;
                lnkUnBlock.CommandArgument = hidUserId.Value;
                OnUnBlockClick(sender, e);
            }
        }

        public void getUserDetailsAndFillData(string userId, string companyId)
        {
            GetUserCompleteDetails objUserDetails = new GetUserCompleteDetails(userId, companyId);
            objUserDetails.getUsrDtlDeptDivAndGroup();
            DataSet dsUserDetails = objUserDetails.UserDtlWithDivDeptAndGroups;
            if (dsUserDetails != null && dsUserDetails.Tables.Count > 0)
            {
                if (dsUserDetails.Tables[0] != null && dsUserDetails.Tables[1] != null && dsUserDetails.Tables[2] != null && dsUserDetails.Tables[0].Rows.Count > 0)
                {
                    fillUserDetails(dsUserDetails);
                }
            }
        }
        void fillUserDetails(DataSet dsUserDetails)
        {
            DataTable dtblUserDtl = dsUserDetails.Tables[0];
            DataTable dtblUserDivDept = dsUserDetails.Tables[1];
            DataTable dtblUserGroups = dsUserDetails.Tables[2];
            //  DataTable dtblApps=dsUserDetails.Tables[3];
            DataTable dtblApps1 = dsUserDetails.Tables[3];

            lblName.Text = Convert.ToString(dtblUserDtl.Rows[0]["FIRST_NAME"]) + " " + Convert.ToString(dtblUserDtl.Rows[0]["LAST_NAME"]);
            lblEmail.Text = Convert.ToString(dtblUserDtl.Rows[0]["EMAIL_ID"]);
            lblMobile.Text = Convert.ToString(dtblUserDtl.Rows[0]["MOBILE"]);
            DateTime dtDateOfBirth = new DateTime(Convert.ToInt64(dtblUserDtl.Rows[0]["DATE_OF_BIRTH"]));

            //lblDateOfBirth.Text = dtDateOfBirth.ToString("dd-MMMM-yyyy");

            lblUserName.Text = Convert.ToString(dtblUserDtl.Rows[0]["USER_NAME"]);
            //lblEmployeeNo.Text = Convert.ToString(dtblUserDtl.Rows[0]["EMPLOYEE_NO"]);
            if (dtblApps1.Rows.Count > 0)
            {
                gvdeatails.DataSource = dtblApps1;
                gvdeatails.DataBind();
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("WF_NAME", typeof(string));
                dt.Columns.Add("GROUP_NAME", typeof(string));
                string v1, v2 = "";
                v1 = "No App Assign";
                v2 = "No Group Assign";
                dt.Rows.Add(v1, v2);
                gvdeatails.DataSource = dt;
                gvdeatails.DataBind();

            }
            if (Convert.ToBoolean(dtblUserDtl.Rows[0]["IS_BLOCKED"]))
            {
                lblAccountType.Text = "Blocked";
            }
            else
            {
                lblAccountType.Text = "Active";
            }
            //if (Convert.ToBoolean(dtblUserDtl.Rows[0]["ALLOW_MESSENGER"]))
            //{
            //    lblMessenger.Text = "Yes";
            //}
            //else
            //{
            //    lblMessenger.Text = "No";
            //}
            //if (Convert.ToBoolean(dtblUserDtl.Rows[0]["DESKTOP_MESSENGER"]))
            //{
            //    lblDesktopMessenger.Text = "Yes";
            //}
            //else
            //{
            //    lblDesktopMessenger.Text = "No";
            //}
            if (dtblUserGroups.Rows.Count > 0)
            {
                lblGroups.Text = Convert.ToString(dtblUserGroups.Rows[0]["GROUPS"]) == String.Empty ? "Group not allotted yet" : Convert.ToString(dtblUserGroups.Rows[0]["GROUPS"]);
            }
            else
            {
                lblGroups.Text = "Group not allotted yet";
            }

        }

    }
}