﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
namespace mFicientCP
{
    public partial class WFStatisticsDetail : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                bool isPreviousPageIDE = false;
                if (((System.Web.UI.TemplateControl)(Page.PreviousPage)).AppRelativeVirtualPath == "~/ide.aspx")
                {
                    isPreviousPageIDE = true;
                }
                if (isPreviousPageIDE)
                {
                    hfsValue = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("hfs")).Value;
                    hfbidValue = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("hfbid")).Value;
                }
                else
                {
                    hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                    hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                }
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                bindDropDownForMenu(null);
                processShowSatisticsInPieChart(true, null);
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "makeDatePickerWithMonthYear();makeLegendTableIfOverFlowing();", true);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "makeDatePickerWithMonthYear();");
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "makeDatePickerWithMonthYear();makeLegendTableIfOverFlowing();");
            }
        }

        void bindDropDownForMenu(UpdatePanel updPanel)
        {
            GetMenuCategoryNew objMenuCategory = new GetMenuCategoryNew(hfsPart3);
            objMenuCategory.Process();
            if (objMenuCategory.ResultTable != null)
            {
                if (objMenuCategory.ResultTable.Rows.Count > 0)
                {
                    ddlMenu.DataSource = objMenuCategory.ResultTable;
                    ddlMenu.DataTextField = "CATEGORY";
                    ddlMenu.DataValueField = "MENU_CATEGORY_ID";
                    ddlMenu.DataBind();
                }
                else
                {
                    if (updPanel != null)
                    {
                        Utilities.showMessage("There is no menu category."
                            , updPanel, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessageUsingPageStartUp(this.Page
                            , "InfoForNoMenuCategory", "There is no menu category."
                            , DIALOG_TYPE.Info);
                    }
                }
            }
            else
            {
                if (updPanel != null)
                    Utilities.showMessage("Internal server error",
                        updPanel, DIALOG_TYPE.Error);
                else
                    Utilities.showMessageUsingPageStartUp(this.Page,
                        "MenuCategoryBoundError", "Internal server error"
                        , DIALOG_TYPE.Error);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="menuId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="fromDateTicks"></param>
        /// <param name="toDateTicks"></param>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when there is some internal error</exception>
        DataTable getStatisticsDetails(string companyId, string menuId, string subAdminId
          , bool searchWithDate, long fromDateTicks, long toDateTicks)
        {
            GetWFDetailsForChart objWFDetails;
            if (!String.IsNullOrEmpty(companyId) && !String.IsNullOrEmpty(menuId) && !String.IsNullOrEmpty(subAdminId))
            {
                objWFDetails = new GetWFDetailsForChart(companyId, menuId, subAdminId);
            }
            else if (!String.IsNullOrEmpty(companyId) && !String.IsNullOrEmpty(menuId) && String.IsNullOrEmpty(subAdminId))
            {
                objWFDetails = new GetWFDetailsForChart(companyId, menuId);
            }
            else
            {
                objWFDetails = new GetWFDetailsForChart(companyId);
            }

            if (searchWithDate) objWFDetails.Process(true, fromDateTicks, toDateTicks);
            else objWFDetails.Process(true);

            if (objWFDetails.StatusCode == 0)
            {
                return objWFDetails.WFDetails;
            }
            else
            {
                throw new Exception("Internal server error");
            }
        }

        void processShowSatisticsInPieChart(bool isAtPageLoad, UpdatePanel updPanel)
        {
            try
            {
                string strError = validateDateFields();
                if (!String.IsNullOrEmpty(strError))
                {
                    if (updPanel != null)
                    {
                        Utilities.showAlert(strError, "divErrorAlert", updPanel);
                    }
                    else
                    {
                        Utilities.showAlert(strError, "divErrorAlert", this.Page, "DateSelectionError");
                    }
                }
                string strMenuId = String.Empty, strMethMessage = String.Empty;
                long lngFromDate = long.MinValue;
                long lngToDate = long.MinValue;
                bool blnSearchWithDate = false;

                //Menu DropDown is having a option for all where statistics of all the Menu Is Shown.
                //For that case we are passing Empty string to the class.
                if (!isAtPageLoad && ddlMenu.SelectedValue.ToLower() != "all") strMenuId = ddlMenu.SelectedValue;

                if (!String.IsNullOrEmpty(txtFromDate.Text)
                    && !String.IsNullOrEmpty(txtToDate.Text))
                {
                    blnSearchWithDate = true;
                }

                if (blnSearchWithDate)
                {
                    lngFromDate = new DateTime(Convert.ToInt32(txtFromDate.Text.Split('-')[2]),
                        Convert.ToInt32(txtFromDate.Text.Split('-')[1]),
                        Convert.ToInt32(txtFromDate.Text.Split('-')[0])).ToUniversalTime().Ticks;//0 hrs 0 mins
                    lngToDate = new DateTime(Convert.ToInt32(txtToDate.Text.Split('-')[2]),//23 hrs 59 min
                        Convert.ToInt32(txtToDate.Text.Split('-')[1]),
                        Convert.ToInt32(txtToDate.Text.Split('-')[0])).AddHours(23).AddMinutes(59).ToUniversalTime().Ticks;
                }
                bindTheHTMLTableForPieChart(updPanel, strMenuId
                    , String.Empty, blnSearchWithDate, lngFromDate, lngToDate, out strMethMessage);
                if (!String.IsNullOrEmpty(strMethMessage))
                {
                    Utilities.showMessage(strMethMessage
                        , updPanel, DIALOG_TYPE.Info);
                    hideShowChartContainer(true);
                }
                else
                {
                    hideShowChartContainer(false);
                    callScriptToMakeChart(updPanel);
                }

            }
            catch
            {
                if (updPanel != null)
                {
                    Utilities.showMessage("Internal server error"
                        , updPanel, DIALOG_TYPE.Error);
                }
                else
                {
                    Utilities.showMessageUsingPageStartUp(this.Page
                        , "ErrorShowingStatistics", "Internal server error", DIALOG_TYPE.Error);
                }
            }
        }
        string validateDateFields()
        {
            string strError = String.Empty;
            if (!String.IsNullOrEmpty(txtFromDate.Text)
                && String.IsNullOrEmpty((txtToDate.Text)))
            {
                strError = "Please select to date";

            }
            else if (String.IsNullOrEmpty(txtFromDate.Text)
                            && !String.IsNullOrEmpty((txtToDate.Text)))
            {
                strError = "Please select from date";

            }
            else if (!String.IsNullOrEmpty(txtFromDate.Text) && !String.IsNullOrEmpty(txtToDate.Text))
            {
                DateTime fromDate = new DateTime(Convert.ToInt32(txtFromDate.Text.Split('-')[2]), Convert.ToInt32(txtFromDate.Text.Split('-')[1]), Convert.ToInt32(txtFromDate.Text.Split('-')[0]));
                DateTime toDate = new DateTime(Convert.ToInt32(txtToDate.Text.Split('-')[2]), Convert.ToInt32(txtToDate.Text.Split('-')[1]), Convert.ToInt32(txtToDate.Text.Split('-')[0]));
                if (fromDate > toDate)
                {
                    strError = "Please enter valid date.From Date cannot be after to date.";
                }
            }
            return strError;
        }
        void bindTheHTMLTableForPieChart(UpdatePanel updPanel, string menuId
            , string subAdminId, bool searchWithDate, long fromDateTicks
            , long toDateTicks, out string strMessage)
        {
            strMessage = String.Empty;
            DataTable dtblWFDetails = getStatisticsDetails(hfsPart3
                , menuId, subAdminId, searchWithDate, fromDateTicks, toDateTicks);
            if (dtblWFDetails != null)
            {
                if (dtblWFDetails.Rows.Count > 0)
                {
                    divChartContainer.InnerHtml = getHTMLTableFromDatasetForPieChart(dtblWFDetails);
                }
                else
                {
                    strMessage = "No record found";
                }
            }
            else
            {
                throw new Exception("Internal server error");
            }

        }
        string getHTMLTableFromDatasetForPieChart(DataTable wfDetailsDTbl)
        {
            if (wfDetailsDTbl == null) throw new ArgumentException();
            if (wfDetailsDTbl.Rows.Count > 0)
            {
                StringWriter strWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(strWriter);

                //Create a table
                Table tbl = new Table();
                tbl.Attributes.Add("class", "chart");
                tbl.Attributes.Add("data-type", "pie");
                //Create column header row
                TableHeaderRow thr = new TableHeaderRow();
                for (int i = 0; i <= 1; i++)
                {
                    TableHeaderCell th = new TableHeaderCell();
                    if (i != 0)
                        th.Text = "USAGE STATISTIC";
                    else th.Text = "MOBILE APPS";
                    thr.Controls.Add(th);
                }
                tbl.Controls.Add(thr);
                thr.TableSection = TableRowSection.TableHeader;
                foreach (DataRow row in wfDetailsDTbl.Rows)
                {
                    TableRow tr = new TableRow();
                    TableHeaderCell th = new TableHeaderCell();
                    th.Text = Convert.ToString(row["WF_NAME"]); 
                    tr.Controls.Add(th);
                    tr.TableSection = TableRowSection.TableBody;

                    TableCell td = new TableCell();
                    td = new TableCell();
                    td.Text = Convert.ToString(row["COUNT_WF"]);
                    tr.Controls.Add(td);

                    tbl.Controls.Add(tr);
                }
                tbl.RenderControl(htmlWriter);
                return strWriter.ToString();
            }
            else
            {
                return String.Empty;
            }
        }
        void hideShowChartContainer(bool hide)
        {
            if (hide) divChartContainer.Visible = false;
            else divChartContainer.Visible = true;
        }
        void callScriptToMakeChart(UpdatePanel updPanel)
        {
            if (updPanel != null)
                Utilities.runPostBackScript("makeTheChart();", updPanel, "FormChartFromHTMLTable");
            else
                Utilities.runPageStartUpScript(this.Page, "FormChartFromHTMLTable", "makeTheChart();");
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            processShowSatisticsInPieChart(false, updWFStatistics);
        }
    }
}