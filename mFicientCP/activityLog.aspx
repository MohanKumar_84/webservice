﻿<%@ Page Title="mFicient | Activity Log" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="activityLog.aspx.cs" Inherits="mFicientCP.activityLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function makeDatePickerWithMonthYear() {
            var datePickerFrom = document.getElementById('<%=txtFromDate.ClientID %>');
            var datePickerTo = document.getElementById('<%=txtToDate.ClientID %>');
            $(datePickerFrom).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                 maxDate: "+0D",
                onClose: function (selectedDate) {
                    $(datePickerTo).datepicker("option", "minDate", selectedDate);
                }
            });
            //$.datepicker.formatDate('yy-mm-dd', new Date(2007, 1 - 1, 26));
            if (datePickerFrom) {
                stopDefualtOfDateText(datePickerFrom);
            }

            $(datePickerTo).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                 maxDate: "+0D",
                onClose: function (selectedDate) {
                    $(datePickerFrom).datepicker("option", "maxDate", selectedDate);
                }
            });
            //$.datepicker.formatDate('yy-mm-dd', new Date(2007, 1 - 1, 26));
            if (datePickerTo) {
                stopDefualtOfDateText(datePickerTo);
            }
        }
        function setDateAfterPostBack() {
            var datePickerFrom = document.getElementById('<%=txtFromDate.ClientID %>');
            var datePickerTo = document.getElementById('<%=txtToDate.ClientID %>');
            var toCurrentDate = $(datePickerTo).datepicker("getDate");
            var fromCurrentDate = $(datePickerFrom).datepicker("getDate");
            if (toCurrentDate) {
                $(datePickerFrom).datepicker("option", "maxDate", toCurrentDate);
            }
            if (fromCurrentDate) {
                $(datePickerTo).datepicker("option", "minDate", fromCurrentDate);
            }
        }
        function stopDefualtOfDateText(txtDateField) {
            if (txtDateField) {
                txtDateField.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
            }
        }
    </script>
    <style type="text/css">
        .searchRow div.selector span
        {
            width:180px;    
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div class="widget" id="divInfoAndSelectionWidget">
                    <div class="customWidgetDirectDiv">
                        <div id="divAlert">
                            <div class="searchRow g12" style="padding: 0px;">
                                <div class="g5">
                                    <div>
                                        <asp:Label ID="lblForDdlSubAdmin" runat="server" Text="Sub Admin :" CssClass="label"
                                            AssociatedControlID="ddlSubAdmin"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlSubAdmin" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="g5">
                                    <div>
                                        <asp:Label ID="lblForDdlLogType" runat="server" Text="Log type :" CssClass="label"
                                            AssociatedControlID="ddlLogType"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlLogType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="searchRow g12" style="padding: 0px;">
                                <div class="g5">
                                    <div style="float:left;">
                                        <asp:Label ID="lblForFromDate" runat="server" Text="From :" CssClass="label" AssociatedControlID="txtFromDate"></asp:Label>
                                    </div>
                                    <div style="float:left;margin-left:40px;">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="200"></asp:TextBox></div>
                                </div>
                                <div class="g5">
                                    <div style="float:left;">
                                        <asp:Label ID="lblForToDate" runat="server" Text="To :" CssClass="label" AssociatedControlID="txtToDate"></asp:Label>
                                    </div>
                                    <div style="float:left;margin-left:43px;">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="200"></asp:TextBox></div>
                                </div>
                                <div class="g1" style="margin-left: 36px;">
                                    <asp:Button ID="btnShow" runat="server" Text="Show Logs" OnClick="btnShow_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="divRepeater" style="margin: 5px;">
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Activity Log</h1>"></asp:Label>
                                    </div>
                                    <div style="position: relative; top: 10px; right: 15px;">
                                    </div>
                                    <div style="height: 0px; clear: both">
                                    </div>
                                </asp:Panel>
                                <asp:Repeater ID="rptActivityLogDtls" runat="server" OnItemDataBound="rptActivityLogDtls_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr><th>
                                                        Date
                                                    </th>
                                                    <th>
                                                        Log
                                                    </th>
                                                    
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td>
                                                    <asp:Label ID="LblLogDate" runat="server" Text='<%# Eval("DATE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLog" runat="server" Text='<%# Eval("LOG") %>'></asp:Label>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td>
                                                    <asp:Label ID="LblLogDate" runat="server" Text='<%# Eval("DATE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLog" runat="server" Text='<%# Eval("LOG") %>'></asp:Label>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
            setDateAfterPostBack();
        }
    </script>
</asp:Content>
