﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public partial class activityLog : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;

                
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;


            if (!Page.IsPostBack)
            {
                bindSubAdminDropDown();
                bindActivityLogProcessType();
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd-MM-yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
                //bindActivityRepeater();
                bindActivityLog();
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "makeDatePickerWithMonthYear();", true);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    bindSubAdminDropDown();
                    bindActivityLogProcessType();
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "makeDatePickerWithMonthYear();");
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "makeDatePickerWithMonthYear();");
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", "$(\"select:not(div.selector select)\").uniform();$(\"input:not(div.checker input)\").uniform();makeDatePickerWithMonthYear();", true);
            }
        }

        void bindActivityLog()
        {
            string toDateClientId = txtToDate.ClientID;
            string fromDateClientId = txtFromDate.ClientID;
            if (txtToDate.Text != "" && txtFromDate.Text != "")
            {
                DateTime fromDate =
                    new DateTime(Convert.ToInt32(txtFromDate.Text.Split('-')[2]),
                        Convert.ToInt32(txtFromDate.Text.Split('-')[1]),
                        Convert.ToInt32(txtFromDate.Text.Split('-')[0]));//0 hrs 0 mins

                DateTime toDate =
                    new DateTime(Convert.ToInt32(txtToDate.Text.Split('-')[2]),
                        Convert.ToInt32(txtToDate.Text.Split('-')[1]),
                        Convert.ToInt32(txtToDate.Text.Split('-')[0])).AddHours(23).AddMinutes(59);//23 hrs 59 min

                if (fromDate > toDate)
                {
                    Utilities.showAlert("Please enter valid date.From Date cannot be after to date.", "divAlert", upd);
                    return;
                }
                DataTable dtblActivityLog = new DataTable();
                SqlConnection con;
                MSSqlClient.SqlConnectionOpen(out con);
                mFicientCommonProcess.SaveActivityLog.GetActivityLog(con, hfsPart3, toDate.ToUniversalTime().Ticks, fromDate.ToUniversalTime().Ticks,ddlSubAdmin.SelectedItem.Value,Convert.ToInt32(ddlLogType.SelectedItem.Value),out dtblActivityLog,"");
                if (dtblActivityLog.Rows.Count > 0)
                {
                    rptActivityLogDtls.DataSource = dtblActivityLog;
                    rptActivityLogDtls.DataBind();
                    lblHeaderInfo.Text = "<h1>Activity Log</h1>";
                }
                else
                {
                    lblHeaderInfo.Text = "<h1>No activity log found</h1>";
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    rptActivityLogDtls.DataSource = null;
                    rptActivityLogDtls.DataBind();
                }
            }
        }

        //void bindActivityRepeater()
        //{
        //    string toDateClientId = txtToDate.ClientID;
        //    string fromDateClientId = txtFromDate.ClientID;
        //    if (txtToDate.Text != "" && txtFromDate.Text != "")
        //    {
        //        DateTime fromDate = 
        //            new DateTime(Convert.ToInt32(txtFromDate.Text.Split('-')[2]), 
        //                Convert.ToInt32(txtFromDate.Text.Split('-')[1]),
        //                Convert.ToInt32(txtFromDate.Text.Split('-')[0]));//0 hrs 0 mins
                
        //        DateTime toDate =
        //            new DateTime(Convert.ToInt32(txtToDate.Text.Split('-')[2]),
        //                Convert.ToInt32(txtToDate.Text.Split('-')[1]),
        //                Convert.ToInt32(txtToDate.Text.Split('-')[0])).AddHours(23).AddMinutes(59);//23 hrs 59 min

        //        if (fromDate > toDate)
        //        {
        //            Utilities.showAlert("Please enter valid date.From Date cannot be after to date.", "divAlert", upd);
        //            return;
        //        }
        //        DataTable dtblActivityLog = new DataTable();
        //        DataSet dsSubAdminActivityLog = SubAdminLog.getSubAdminActivityLogModified(ddlSubAdmin.SelectedValue, hfsPart3, (SUBADMIN_ACTIVITY_LOG)Enum.Parse(typeof(SUBADMIN_ACTIVITY_LOG), ddlLogType.SelectedValue), fromDate.ToUniversalTime().Ticks, toDate.ToUniversalTime().Ticks);
        //        if (dsSubAdminActivityLog != null)
        //        {
        //            dtblActivityLog = dsSubAdminActivityLog.Tables[0];
        //        }
        //        else
        //        {
        //            Utilities.showMessage("Internal server error", upd, DIALOG_TYPE.Error);
        //        }
        //        if (dtblActivityLog.Rows.Count > 0)
        //        {
        //            rptActivityLogDtls.DataSource = dtblActivityLog;
        //            rptActivityLogDtls.DataBind();
        //            lblHeaderInfo.Text = "<h1>Activity Log</h1>";
        //        }
        //        else
        //        {
        //            lblHeaderInfo.Text = "<h1>No activity log found</h1>";
        //            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        //            rptActivityLogDtls.DataSource = null;
        //            rptActivityLogDtls.DataBind();
        //        }
        //    }
        //    else
        //    {
        //        string strErrorMsg = "";
        //        if (txtFromDate.Text == "" && txtToDate.Text != "")
        //        {
        //            strErrorMsg = "Please enter from date";
        //        }
        //        else if (txtFromDate.Text != "" && txtToDate.Text == "")
        //        {
        //            strErrorMsg = "Please enter to date";
        //        }
        //        else if (txtFromDate.Text == "" && txtToDate.Text == "")
        //        {
        //            strErrorMsg = "Please enter from date </br> Please enter to date";
        //        }

        //        Utilities.showAlert(strErrorMsg, "divAlert", upd);
        //    }
        //    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        //}
        void bindSubAdminDropDown()
        {
            //GetSubAdminDetail objSubAdminDetail = new GetSubAdminDetail(true, false, hfsPart4, "", "", hfsPart3);
            //objSubAdminDetail.Process();
            GetSubAdminDetail objGetSubAdminDetail =
                new GetSubAdminDetail(hfsPart4, hfsPart3);
            objGetSubAdminDetail.getAllSubAdminsOfCmpByAdminId();
            DataTable dtblSubAdmins = objGetSubAdminDetail.ResultTable;
            if (dtblSubAdmins != null && dtblSubAdmins.Rows.Count > 0)
            {
                ddlSubAdmin.Items.Clear();
                ddlSubAdmin.DataSource = dtblSubAdmins;
                ddlSubAdmin.DataValueField = "SUBADMIN_ID";
                ddlSubAdmin.DataTextField = "FULL_NAME";
                ddlSubAdmin.DataBind();                
            }
            ddlSubAdmin.Items.Insert(0, new ListItem("All", String.Empty));
        }
        void bindActivityLogProcessType()
        {
            ddlLogType.Items.Clear();
            ddlLogType.Items.Add(new ListItem("All", "0"));
            ddlLogType.Items.Add(new ListItem("User Added", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_CREATED).ToString()));
            ddlLogType.Items.Add(new ListItem("User Modified", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_MODIFIED).ToString()));
            ddlLogType.Items.Add(new ListItem("User Removed", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_DELETE).ToString()));
            ddlLogType.Items.Add(new ListItem("User Blocked", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_BLOCKED).ToString()));
            ddlLogType.Items.Add(new ListItem("User Unblocked", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_UNBLOCK).ToString()));
            ddlLogType.Items.Add(new ListItem("Devices Approved", ((int)mFicientCommonProcess.ACTIVITYENUM.DEVICE_APPROVED).ToString()));
            ddlLogType.Items.Add(new ListItem("Device Removed", ((int)mFicientCommonProcess.ACTIVITYENUM.DEVICE_DELETE).ToString()));
            ddlLogType.Items.Add(new ListItem("Device Denied", ((int)mFicientCommonProcess.ACTIVITYENUM.DEVICE_DENY).ToString()));
            ddlLogType.Items.Add(new ListItem("Group Added", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_ADDED).ToString()));
            ddlLogType.Items.Add(new ListItem("Group Updated", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_RENAMED).ToString()));
            ddlLogType.Items.Add(new ListItem("Group Removed", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_REMOVED).ToString()));
            ddlLogType.Items.Add(new ListItem("User(s) Added in Group", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_USER_ADDED).ToString()));
            ddlLogType.Items.Add(new ListItem("User(s) Removed from Group", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_USER_DELETED).ToString()));
            ddlLogType.Items.Add(new ListItem("App(s) Added in Group", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_APP_ADDED).ToString()));
            ddlLogType.Items.Add(new ListItem("App(s) Removed from Group", ((int)mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_APP_DELETED).ToString()));
            ddlLogType.Items.Add(new ListItem("Catgory Added", ((int)mFicientCommonProcess.ACTIVITYENUM.CATEGORY_ADDED).ToString()));
            ddlLogType.Items.Add(new ListItem("Catgory Updated", ((int)mFicientCommonProcess.ACTIVITYENUM.CATEGORY_RENAMED).ToString()));
            ddlLogType.Items.Add(new ListItem("Catgory Removed", ((int)mFicientCommonProcess.ACTIVITYENUM.CATEGORY_REMOVED).ToString()));
            ddlLogType.Items.Add(new ListItem("App(s) Added in Catgory", ((int)mFicientCommonProcess.ACTIVITYENUM.CATEGORY_MODIFIED_APPADDED).ToString()));
            ddlLogType.Items.Add(new ListItem("App(s) Removed from Catgory", ((int)mFicientCommonProcess.ACTIVITYENUM.CATEGORY_MODIFIED_APP_DELETED).ToString()));
            ddlLogType.Items.Add(new ListItem("App Created/Modified", ((int)mFicientCommonProcess.ACTIVITYENUM.APP_SAVE_MODIFIED).ToString()));
            ddlLogType.Items.Add(new ListItem("App Removed", ((int)mFicientCommonProcess.ACTIVITYENUM.APP_DELETE).ToString()));
            ddlLogType.Items.Add(new ListItem("App Publish", ((int)mFicientCommonProcess.ACTIVITYENUM.APP_PUBLISH).ToString()));
            ddlLogType.Items.Add(new ListItem("App Unpublish", ((int)mFicientCommonProcess.ACTIVITYENUM.APP_ROLLBACK).ToString()));
            ddlLogType.Items.Add(new ListItem("App Usage", ((int)mFicientCommonProcess.ACTIVITYENUM.APP_USED).ToString()));
            ddlLogType.Items.Add(new ListItem("Object Usage", ((int)mFicientCommonProcess.ACTIVITYENUM.OBJECT_EXECUTE).ToString()));
            ddlLogType.Items.Add(new ListItem("User Loged in", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_LOGIN).ToString()));
            ddlLogType.Items.Add(new ListItem("User Logout", ((int)mFicientCommonProcess.ACTIVITYENUM.SESSTION_RENEW).ToString()));
            ddlLogType.Items.Add(new ListItem("Session Renew", ((int)mFicientCommonProcess.ACTIVITYENUM.USER_LOGOUT).ToString()));
            ddlLogType.Items.Add(new ListItem("mPlugin Connected", ((int)mFicientCommonProcess.ACTIVITYENUM.MPLUGIN_CONNECTED).ToString()));
            ddlLogType.Items.Add(new ListItem("mGram Message", ((int)mFicientCommonProcess.ACTIVITYENUM.PUSHMESSAGE_SEND).ToString()));
            ddlLogType.Items.Add(new ListItem("User Added", ((int)mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_CREATED).ToString()));
            ddlLogType.Items.Add(new ListItem("User Modified", ((int)mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_MODIFIED).ToString()));
            ddlLogType.Items.Add(new ListItem("User Removed", ((int)mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_DELETE).ToString()));
            ddlLogType.Items.Add(new ListItem("User Blocked", ((int)mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_BLOCKED).ToString()));
            ddlLogType.Items.Add(new ListItem("User Unblocked", ((int)mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_UNBLOCK).ToString()));
        }
        protected void rptActivityLogDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void rdlLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bindActivityRepeater();
            bindActivityLog();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //bindActivityRepeater();
            bindActivityLog();
        }
    }
}