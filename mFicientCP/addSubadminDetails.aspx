﻿<%@ Page Title="mFicient | Sub Admins" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="addSubadminDetails.aspx.cs" Inherits="mFicientCP.addSubadminDetails" %>

<%@ Register TagPrefix="subAdminDetails" TagName="Details" Src="~/UserControls/UCSubAdminDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function rememberPasswordEntered() {
            var hidPassword = $('#' + '<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            var txtRetypePassword = $('#' + '<%=txtRetypePassword.ClientID %>');
            $(hidPassword).val($(txtPassword).val());
            $(hidReTypePasswordEntered).val($(txtRetypePassword).val());
        }
        function fillPasswordEntered() {
            var hidPassword = document.getElementById('<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = document.getElementById('<%=txtPassword.ClientID %>');
            var txtRetypePassword = document.getElementById('<%=txtRetypePassword.ClientID %>');
            if (hidPassword && hidReTypePasswordEntered) {
                if (hidPassword.value !== "") {
                    if (txtPassword)
                        $(txtPassword).val($(hidPassword).val());
                }
                else {
                    if (txtPassword)
                        $(txtPassword).val(""); //done for adding trimmed value.
                }
                if ($(hidReTypePasswordEntered).val() !== "") {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val($(hidReTypePasswordEntered).val());
                }
                else {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val(""); //done for adding trimmed value
                }
            }
        }
        function checkPasswordMatches() {
            var txtPassword = document.getElementById('<%=txtPassword.ClientID %>');
            var txtRetypePassword = document.getElementById('<%=txtRetypePassword.ClientID %>');
            if (txtPassword.value == txtRetypePassword.value) {
                return;
            }
            else {

            }
        }
        function hideDivisionListTblHead() {
            $('#divisionListTableHead').hide();
        }
        function showDivisionListTblHead() {
            $('#divisionListTableHead').show();
        }
        function confirmBlockUnblock(lnkButton) {
            var strlnkButtonText = "";
            if ($.browser.msie) {
                strlnkButtonText = lnkButton.innerHTML;
            }
            else {
                strlnkButtonText = lnkButton.text;
            }
            var blockUnblockText = 'Are you sure you want to ' + strlnkButtonText + ' the user';
            //return confirm(blockUnblockText);
            return showConfirmation(blockUnblockText);
        }

        function processSelectDeselectAll(checkBox, idOfTemplate, chkCheckBoxList) {
            var chkCheckBoxListItems = $(chkCheckBoxList)[0].getElementsByTagName("input");
            var iNoOfCheckBoxesExceptAll = chkCheckBoxListItems.length - 1;
            var iNoOfCheckBoxesSelected = 0;
            if (!(checkBox === chkCheckBoxListItems[0])) {
                var isAllSelectedInTemplate = false;
                for (var intCounter = 1; intCounter <= chkCheckBoxListItems.length - 1; intCounter++) {
                    if ($(chkCheckBoxListItems[intCounter]).is(':checked')) {
                        // Item is selected
                        iNoOfCheckBoxesSelected++;
                    }
                }
                if (iNoOfCheckBoxesExceptAll == iNoOfCheckBoxesSelected) {
                    isAllSelectedInTemplate = true;
                }
                if (isAllSelectedInTemplate) {
                    $(chkCheckBoxListItems[0]).attr('checked', 'checked');
                    var immediateSpanContainer = $(chkCheckBoxListItems[0]).parent();
                    var outerSpanContainer = $(chkCheckBoxListItems[0]).parent().parent().parent();
                    immediateSpanContainer.attr("class", "checked");
                    outerSpanContainer.attr("class", "checked");
                }
                else {
                    $(chkCheckBoxListItems[0]).removeAttr('checked');
                    var immediateSpanContainer = $(chkCheckBoxListItems[0]).parent();
                    var outerSpanContainer = $(chkCheckBoxListItems[intCounter]).parent().parent().parent();
                    immediateSpanContainer.removeAttr("class");
                    outerSpanContainer.removeAttr("class");
                }
            }
            else {
                if ($(chkCheckBoxListItems[0]).is(':checked')) {
                    for (var intCounter = 1; intCounter <= chkCheckBoxListItems.length - 1; intCounter++) {

                        $(chkCheckBoxListItems[intCounter]).attr('checked', 'checked');
                        var immediateSpanContainer = $(chkCheckBoxListItems[intCounter]).parent();
                        var outerSpanContainer = $(chkCheckBoxListItems[intCounter]).parent().parent().parent();
                        immediateSpanContainer.attr("class", "checked");
                        outerSpanContainer.attr("class", "checked");
                    }

                }
                else {
                    for (var intCounter = 1; intCounter <= chkCheckBoxListItems.length - 1; intCounter++) {
                        if ($(chkCheckBoxListItems[intCounter]).is(':checked')) {
                            // Item is selected
                            $(chkCheckBoxListItems[intCounter]).removeAttr('checked');
                            var immediateSpanContainer = $(chkCheckBoxListItems[intCounter]).parent();
                            var outerSpanContainer = $(chkCheckBoxListItems[intCounter]).parent().parent().parent();
                            immediateSpanContainer.removeAttr("class");
                            outerSpanContainer.removeAttr("class");
                        }
                    }
                }
            }
            checkDivisionSelectAll();

        }


        function checkDivisionSelectAll() {
            var hidCheckBoxListFormed = document.getElementById('<%=hidDivisionCheckBoxListFormed.ClientID %>');
            var aryCheckBoxList = hidCheckBoxListFormed.value.split('|');
            var iNoOfAllCheckbox = aryCheckBoxList.length;
            var iNoOfAllSelected = 0;
            var strValue = "";
            for (var i = 0; i <= aryCheckBoxList.length - 1; i++) {
                var chkCheckBoxListItems = $("#" + aryCheckBoxList[i])[0].getElementsByTagName("input");
                if ($(chkCheckBoxListItems[0]).is(':checked')) {
                    iNoOfAllSelected++;
                }
            }
            var hidDivisionSelectAll = document.getElementById('<%=hidClientIDOfDivisionSelectAll.ClientID %>');
            var chkSelectAll = $("#" + hidDivisionSelectAll.value);
            var divContainer = $(chkSelectAll).parent();
            if (iNoOfAllCheckbox == iNoOfAllSelected) {
                $(chkSelectAll).attr('checked', 'checked');
                if ($(divContainer).is("span")) {
                    $(divContainer).attr("class", "checked");
                }
            }
            else {
                $(chkSelectAll).removeAttr('checked');
                if ($(divContainer).is("span")) {
                    $(divContainer).removeAttr("class");
                }
            }
        }

        function deselectAll() {
            var hidCheckBoxListFormed = document.getElementById('<%=hidDivisionCheckBoxListFormed.ClientID %>');
            var aryCheckBoxList = hidCheckBoxListFormed.value.split('|');
            var strValue = "";
            for (var i = 0; i <= aryCheckBoxList.length - 1; i++) {
                var chkCheckBoxListItems = $("#" + aryCheckBoxList[i])[0].getElementsByTagName("input");
                for (var j = 0; j <= chkCheckBoxListItems.length - 1; j++) {
                    $(chkCheckBoxListItems[j]).removeAttr('checked');
                    var divConatainer = $(chkCheckBoxListItems[j]).parent();
                    if ($(divConatainer).is("span")) {
                        $(divConatainer).removeAttr('class');
                    }
                }
            }
        }

        function selectAll() {
            var hidCheckBoxListFormed = document.getElementById('<%=hidDivisionCheckBoxListFormed.ClientID %>');
            var aryCheckBoxList = hidCheckBoxListFormed.value.split('|');
       
            var strValue = "";
            for (var i = 0; i <= aryCheckBoxList.length - 1; i++) {


                var chkCheckBoxListItems = $("#" + aryCheckBoxList[i]).find('input');
               // var chkCheckBoxListItems = $("#" + aryCheckBoxList[i])[0].getElementsByTagName("input");
                for (var j = 0; j <= chkCheckBoxListItems.length - 1; j++) {
                    $(chkCheckBoxListItems[j]).attr('checked', 'checked');
                    var divConatainer = $(chkCheckBoxListItems[j]).parent();
                    if ($(divConatainer).is("span")) {
                        $(divConatainer).attr('class', 'checked');
                    }
                }
            }
        }

        function processSelectOrDeselectAll(sender) {
            var isSenderChecked = $(sender).is(':checked') ? true : false;
            if (isSenderChecked) {
                selectAll();
            }
            else {
                deselectAll();
            }
        }

        Panels = { Repeater: 0, Form: 1 }
        function hideShowPanelForCancel(panelToShow) {
            var repeater = $('#' + '<%= pnlRepeaterBox.ClientID%>');
            var form = $('#' + '<%=pnlSubAdminForm.ClientID %>');
            switch (panelToShow) {
                case Panels.Repeater:
                    repeater.show();
                    form.hide();
                    break;
                case Panels.Form:
                    repeater.hide();
                    form.show();
                    break;
            }
        }
        function doProcessForFormHelp() {
            $.formHelp(
            { classPrefix: 'myprefix', pushpinEnabled: false }
    );
        }


        function test() {
            $('[id$=rptDivisionList_chkSelectAll]').prop('checked', true);
            selectAll();
        }
    </script>
    <style type="text/css">
        form div.modalPopUpDetails fieldset section div.column1
        {
            width: 120px;
        }
        form div .modalPopUpDetails div.singleSection fieldset section div.column3
        {
            width: 230px;
        }
        div.formCheckBox
        {
           padding: 4px 0px 0px 1px;
        }
        #formDiv fieldset>div>section label ,#formDiv fieldset > section label
        {
            width:17%;    
        }
        #formDiv fieldset>div>section>div , #formDiv fieldset > section > div
        {
            width:77%;    
        }
        div.containingEditImg
        {
            padding-top:10px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="updRepeater" runat="server">
                    <ContentTemplate>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Sub Admin</h1>"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="lnkAddNewSubAdmin" runat="server" Text="add" CssClass="repeaterLink fr"
                                                OnClick="lnkAddNewSubAdmin_Click" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                        </div>
                                        <div style="float: right; margin-right: 80px; margin-top: 10px;">
                                            <asp:CheckBox ID="chkShowBlockedUser" Text="Show blocked Sub Admin" runat="server"
                                                OnCheckedChanged="chkShowBlockedUser_CheckedChanged" AutoPostBack="true" />
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptSubAdminDtls" runat="server" OnItemDataBound="rptSubAdminDtls_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable rowClickable rptWithoutImageAndButton">
                                            <thead>
                                                <tr>
                                                    <th style="display: none">
                                                        SUB ADMIN ID
                                                    </th>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Login Name
                                                    </th>
                                                    <th>
                                                        Email
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem" onclick="rptRowClick(this);">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblSubAdminId" runat="server" Text='<%#Eval("SUBADMIN_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblIsBlocked" runat="server" Text='<%#Eval("IS_BLOCKED") %>'></asp:Label>
                                                    <asp:Label ID="lblPushMessage" runat="server" Text='<%#Eval("PUSH_MESSAGE") %>'></asp:Label>
                                                    <asp:Label ID="lblUserNm" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblSubAdminFullName" runat="server" Text='<%#Eval("FULL_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem" onclick="rptRowClick(this);">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblSubAdminId" runat="server" Text='<%#Eval("SUBADMIN_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblIsBlocked" runat="server" Text='<%#Eval("IS_BLOCKED") %>'></asp:Label>
                                                    <asp:Label ID="lblPushMessage" runat="server" Text='<%#Eval("PUSH_MESSAGE") %>'></asp:Label>
                                                    <asp:Label ID="lblUserNm" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblSubAdminFullName" runat="server" Text='<%#Eval("FULL_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </section>
                        <section>
                            <asp:Panel ID="pnlSubAdminForm" Visible="false" runat="server">
                                <div class="g12">
                                    <div id="formDiv">
                                        <fieldset>
                                            <label>
                                                Details &nbsp;&nbsp;<asp:Label ID="lblHeaderUserName" runat="server" Visible="false"></asp:Label>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="delete" OnClick="lnkDelete_Click"
                                                    CssClass="FormHeaderActionLinks" OnClientClick="return showConfirmation('Are you sure you want to delete this sub admin');"></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:Label ID="lblSeparatorOne" runat="server" Text="&nbsp;&nbsp; | &nbsp;&nbsp;"
                                                    CssClass="FormHeaderActionLinksSeparator"></asp:Label>&nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkBlockUnblock" runat="server" Text="block" OnClick="lnkBlockUnblock_Click"
                                                    CssClass="FormHeaderActionLinks" OnClientClick="return confirmBlockUnblock(this);"></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:Label ID="lblSeparatorTwo" runat="server" Text="&nbsp;&nbsp; | &nbsp;&nbsp;"
                                                    CssClass="FormHeaderActionLinksSeparator"></asp:Label>
                                                <asp:LinkButton ID="lnkResetPassword" runat="server" OnClientClick="return showConfirmation('Are you sure you want to reset password of the sub admin.');"
                                                    Text="reset password" OnClick="lnkResetPassword_Click" CssClass="FormHeaderActionLinks"></asp:LinkButton>
                                            </label>
                                            <section>
                                                <label for="<%= txtFirstName.ClientID%>">
                                                    Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="required " Width="50%" MaxLength="50"></asp:TextBox>
                                                    <span class="myprefix-helptext" data-for='#<%= txtFirstName.ClientID%>'>
                                                        <ul>
                                                            <li>You can use letters</li>
                                                            <li>Allowed special characters - space</li>
                                                        </ul>
                                                    </span>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%= txtEmailId.ClientID%>">
                                                    Email</label>
                                                <div>
                                                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="required" Width="50%" MaxLength="50"></asp:TextBox>
                                                    <span class="myprefix-helptext" data-for='#<%= txtEmailId.ClientID%>'>eg. abc@xyz.com
                                                    </span>
                                                </div>
                                            </section>
                                            <asp:Panel ID="pnlUserName" runat="server">
                                                <section>
                                                    <label for="<%= txtUserName.ClientID%>">
                                                        Username</label>
                                                    <div>
                                                        <asp:TextBox ID="txtUserName" runat="server" CssClass="required " Width="50%" MaxLength="50"></asp:TextBox>
                                                        <span class="myprefix-helptext" data-for='#<%= txtUserName.ClientID%>'>
                                                            <ul>
                                                                <li>You can use lettes and numbers</li>
                                                                <li>Username should start with a letter</li>
                                                                <li>Allowed special characters ! # $ % ^ * ( ) _ -</li>
                                                            </ul>
                                                        </span>
                                                    </div>
                                                </section>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlPassword" runat="server">
                                                <section>
                                                    <label for="<%= txtPassword.ClientID%>">
                                                        Password</label>
                                                    <div>
                                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="required "
                                                            Width="25%" onblur="rememberPasswordEntered();"></asp:TextBox>&nbsp;&nbsp; <span
                                                                class="myprefix-helptext" data-for='#<%= txtPassword.ClientID%>'>
                                                                <ul>
                                                                    <li>Use atleast 6 characters</li>
                                                                    <li>Allowed special characters ! @ # $ % ^ * ( ) _ -</li>
                                                                </ul>
                                                            </span>
                                                    </div>
                                                </section>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlRetypePassword" runat="server">
                                                <section>
                                                    <label for="<%= txtRetypePassword.ClientID%>">
                                                        Retype Password</label>
                                                    <div>
                                                        <asp:TextBox ID="txtRetypePassword" runat="server" CssClass="required " TextMode="Password"
                                                            Width="25%" onblur="rememberPasswordEntered();"></asp:TextBox>&nbsp;
                                                    </div>
                                                </section>
                                            </asp:Panel>
                                            <section>
                                                <label for="<%= chkRoles.ClientID%>">
                                                    Roles</label>
                                                <div class="radioButtonList">
                                                    <asp:CheckBoxList ID="chkRoles" runat="server">
                                                    </asp:CheckBoxList>
                                                    <div class="formCheckBox">
                                                        <asp:CheckBox ID="chkPushMessage" Text="mGram message" runat="server" />
                                                    </div>
                                                </div>
                                            </section>
                                            <section style="display:none">
                                                <label for="lnkDivDeptOfSubAdmin" >
                                                    Division / Department</label>
                                                <div class="containingLabel containingEditImg">
                                                    <%--<asp:LinkButton ID="lnkDivDeptOfSubAdmin" runat="server" Text="edit" OnClientClick="showModalPopUpWithOutHeader('divModalContainer','Division / Department','600');return false;"></asp:LinkButton>--%>
                                                    <span class="fl i_pencil" id="lnkDivDeptOfSubAdmin" style="margin-left:4px" title="edit" onclick="showModalPopUpWithOutHeader('divModalContainer','Division / Department','600');return false;">
                                                </div>
                                            </section>

                                            <section >
                                                <label for="drpmappeduser">
                                                    Mapped to User</label>
                                                <div class="containingLabel containingEditImg">
                                                <asp:DropDownList ID="drpmappeduser" runat="server">
                                                <asp:ListItem>Select</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hidmapped" runat="server" />
                                                </div>
                                            </section>

                                        </fieldset>
                                       
                                        <section id="buttons">
                                            <div id="divButtonCont" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px;
                                                text-align: center;">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click" />
                                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="hideShowPanelForCancel(Panels.Repeater);return false;"
                                                    OnClick="btnCancel_Click" />
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </asp:Panel>
                        </section>
                        <div>
                            <asp:HiddenField ID="hidEditSubAdminId" runat="server" />
                            <asp:HiddenField ID="hidProcess" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                            <asp:HiddenField ID="hidReTypePasswordEntered" runat="server" />
                            <asp:HiddenField ID="hidRowValuesUsedAfterPostBack" runat="server" />
                            <asp:HiddenField ID="hidDivisionCheckBoxListFormed" runat="server" />
                            <asp:HiddenField ID="hidClientIDOfDivisionSelectAll" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRowClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnRowClickPostBack" runat="server" OnClick="btnRowClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
   <%-- <div id="divModalContainer" style="height: 400px;">
        <asp:UpdatePanel ID="updModalContainer" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divDivDeptContainer" style="display: block;">
                    <div id="modalPopUpContent">
                        <asp:Panel ID="pnlDivisionRptBox" CssClass="repeater modalPopUp" runat="server" ScrollBars="Vertical"
                            Height="300" Style="margin-bottom: 30px;">
                            <asp:Repeater ID="rptDivisionList" runat="server" OnItemDataBound="rptDivisionList_ItemDataBound">
                                <HeaderTemplate>
                                    <table style="width: 100%;">
                                        <thead id="divisionListTableHead">
                                            <tr>
                                                <th>
                                                    <div style="position: relative; left: -1px;">
                                                        <asp:CheckBox ID="chkSelectAll" Text="Select All" Checked="true" runat="server" onclick="processSelectOrDeselectAll(this)" />
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem" style="background-color: #F3F3F3; margin-bottom: 1px;">
                                            <td>
                                                <div>
                                                    <asp:Label ID="lblDivisionId" runat="server" Visible="false"></asp:Label>
                                                    <asp:CheckBox ID="chkDivision" Style="display: none;" runat="server" />
                                                    <asp:Label ID="lblDivisionName" runat="server"></asp:Label>
                                                </div>
                                                <div style="margin-top: 2px; margin-left: 5px;">
                                                    <asp:CheckBoxList ID="chkDepartmentList" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                        Style="margin-bottom: 1px;">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem" style="background-color: #F7F7F7; margin-bottom: 1px;">
                                            <td>
                                                <div>
                                                    <asp:Label ID="lblDivisionId" runat="server" Visible="false"></asp:Label>
                                                    <asp:CheckBox ID="chkDivision" Style="display: none;" runat="server" />
                                                    <asp:Label ID="lblDivisionName" runat="server"></asp:Label>
                                                </div>
                                                <div style="margin-top: 2px; margin-left: 5px;">
                                                    <asp:CheckBoxList ID="chkDepartmentList" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                        Style="margin-bottom: 1px;">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                        <div class="modalPopUpAction">
                            <div id="divPopUpActionCont" class="popUpActionCont" style="width: 10%">
                                <asp:Button ID="btnOk" runat="server" Text="OK" OnClientClick="$('#divModalContainer').dialog('close');return false;" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div id="divSubAdminDtlsModal" style="display: none;">
        <asp:UpdatePanel runat="server" ID="updSubAdminDtlsModal" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div id="divSubAdminDetailUC">
                        <subAdminDetails:Details ID="ucSubAdminDetails" runat="server" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {

            hideWaitModal();
            isCookieCleanUpRequired('true');
            doProcessForFormHelp();
        }
    </script>
</asp:Content>
