﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace mFicientCP
{
    public partial class ideDataSource : System.Web.UI.Page
    {
        string SubAdminid = string.Empty;
        string strSessionId = string.Empty;
        string CompanyId = string.Empty;
        string strAdminId = string.Empty, strPostbackPageMode = "";
        string strhfs, strhfbid;
        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPage mainMaster = Page.Master;
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                strPostbackPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEditSet")).Value;
                ((HiddenField)mainMaster.FindControl("hidAppNewOrEditSet")).Value = "";

            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }
                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hdfPostBackPageMode.Value = ((HiddenField)previousPage.Master.FindControl("hidAppNewOrEditSet")).Value;
                ((HiddenField)previousPage.Master.FindControl("hidAppNewOrEditSet")).Value = "";

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;

                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;

            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid)) return;

            Utilities.SaveInContext(context, strhfs, strhfbid, out SubAdminid, out strSessionId, out CompanyId, out strAdminId);
         //   Utilities.makedropdownFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "");
            if (!IsPostBack)
            {
                bindMpluginAgentDropdown();
                bindPridefinedCreadential();
            }
            if (!IsPostBack || (IsPostBack && !string.IsNullOrEmpty(strPostbackPageMode)))
            {
                if (!IsPostBack)
                    strPostbackPageMode = hdfPostBackPageMode.Value;
                else
                    hdfPostBackPageMode.Value = strPostbackPageMode;
                if (hdfPostBackPageMode.Value == "dbsource")
                {
                    intializeDBSources();
                }
                else if (hdfPostBackPageMode.Value == "wssource")
                {
                    initializeWSSources();

                }
                else if (hdfPostBackPageMode.Value == "odatasource")
                {
                    initializeOdataSources();

                }
            }
            Iframe1.Attributes["src"] = "UploadWsdlFile.aspx?id=" + this.SubAdminid + "&cid=" + this.CompanyId;
        }



        #region BindDatabaseJsonFormat/BindODatabaseJsonFormat//BindWedDataSourceJsonFormat
        public void getDBSources()
        {
            try
            {
                DataTable dt = new DataTable();
                ArrayList rows = new ArrayList();
                    
                GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(true,  "", "", CompanyId);
                if (objGetDatabaseConnection.StatusCode == 0)
                {
                    dt = objGetDatabaseConnection.ResultTable;
                    DataColumn workCol = dt.Columns.Add("DateTime", typeof(string));
                    foreach (DataRow row in dt.Rows)
                    {
                        row["DATABASE_NAME"] = Convert.ToString(AesEncryption.AESDecrypt(CompanyId, Convert.ToString(row["DATABASE_NAME"])));
                        row["DateTime"] = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(row["UPDATED_ON"]));
                    }
                    DataTable dtSelectedColumns = dt.DefaultView.ToTable(false, "CONNECTION_NAME", "DATABASE_NAME", "DB", "MPLUGIN_AGENT");
                    foreach (DataRow dataRow in dtSelectedColumns.Rows)
                    {
                        rows.Add(dataRow.ItemArray);
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* Server not reachable');", true);

                hiddatasource.Value = Utilities.SerializeJson<ArrayList>(rows);
                hiddataset.Value = Utilities.ConvertdatetabletoString(dt);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Internal Server Error" + "');", true);
            }

        }
        public void OdataTable()
        {
            try
            {
                GetODataSrviceConnector objodata = new GetODataSrviceConnector(true, this.SubAdminid, "", "", this.CompanyId, 1);
                DataTable dtodata = new DataTable();
                ArrayList odatarows = new ArrayList();                    
                if (objodata.StatusCode == 0)
                {
                    dtodata = objodata.ResultTable;
                
                    DataColumn workCol = dtodata.Columns.Add("DateTime", typeof(string));
                    foreach (DataRow row in dtodata.Rows)
                    {
                        row["DateTime"] = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(row["UPDATED_ON"]));
                    }
                    DataTable dtOSelectedColumns = dtodata.DefaultView.ToTable(false, "CONNECTION_NAME", "MPLUGIN_AGENT", "ODATA_ENDPOINT");
                    foreach (DataRow dataRow in dtOSelectedColumns.Rows)
                    {
                        odatarows.Add(dataRow.ItemArray);
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* Server not reachable');", true);

                hidodataset.Value = Utilities.SerializeJson<ArrayList>(odatarows);
                hidodatacompletedataset.Value = Utilities.ConvertdatetabletoString(dtodata);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "Internal Server Error" + "');BindOdata();", true);
            }

        }
        public void getWSSources()
        {
            try
            {
                GetWebserviceConnection objGetWebserviceConnection = new GetWebserviceConnection(true, this.SubAdminid, "", "", this.CompanyId);
                objGetWebserviceConnection.Process1();
                ArrayList wsrows = new ArrayList();
                DataTable dtws = new DataTable();
                if (objGetWebserviceConnection.StatusCode == 0)
                {
                    dtws = objGetWebserviceConnection.ResultTable;
                    DataColumn workCol = dtws.Columns.Add("DateTime", typeof(string));
                    foreach (DataRow row in dtws.Rows)
                    {
                        row["DateTime"] = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(row["UPDATED_ON"]));
                    }
                    DataTable dtSelectedColumns = dtws.DefaultView.ToTable(false, "CONNECTION_NAME", "WEBSERVICE_TYPEALIES", "WEBSERVICE_URL", "MPLUGIN_AGENT");
                    foreach (DataRow dataRow in dtSelectedColumns.Rows)
                    {
                        wsrows.Add(dataRow.ItemArray);
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* Server not reachable');", true);

                hidwsdataset.Value = Utilities.SerializeJson<ArrayList>(wsrows);
                hidwscdataset.Value = Utilities.ConvertdatetabletoString(dtws);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);" + "$('#aMessage').html('" + "* Internal Server Error" + "');BindWsConnectors();", true);
            }

        }

        #endregion


        #region Instialize DataSources/WebService/ Odata Sources and Call JavaScipt Accordingly
        private void intializeDBSources()
        {
            Page.ClientScript.RegisterClientScriptInclude("script", "Scripts/IdeDataSource.js");
            try
            {
                getDBSources();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"initializeDbcontrol();bindConnector();", true);
                pnlwsRepeterBox.Visible = false;
                pnlOdataRepeterBox.Visible = false;
                pnlRepeaterBox.Visible = true;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "');", true);
            }

        }

        private void initializeWSSources()
        {
            try
            {
                Page.ClientScript.RegisterClientScriptInclude("script", "Scripts/IdeWebservicedataSources.js");
                getWSSources();
                ScriptManager.RegisterStartupScript(updateaddwebservice, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"WsAddNewConnector();", true);
                pnlRepeaterBox.Visible = false;
                pnlOdataRepeterBox.Visible = false;
                pnlwsRepeterBox.Visible = true;
            }

            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);" + "$('#aMessage').html('" + ex.ToString() + "');", true);
            }


        }

        private void initializeOdataSources()
        {
            Page.ClientScript.RegisterClientScriptInclude("script", "Scripts/IdeOdataSource.js");
            try
            {
                OdataTable();
                pnlwsRepeterBox.Visible = false;
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"intializeOdataSource();", true);
                pnlRepeaterBox.Visible = false;
                pnlOdataRepeterBox.Visible = true;
                RefreshOdataTable();
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + "* Internal Server Error" + "');", true);
            }

        }


        #endregion

        #region DataBase Server Side Validation
        public bool ValidateDbConnector(out string strError)
        {
            strError = "";
            bool isValid = true;
            if (txtConnectionName.Text.Trim().Length == 0)
            {
                strError += " * Please enter database source name.</br>";
            }
            if (ddlDbConnectorDbType.SelectedValue == "-1")
            {
                if (hiddrpconnectortype.Value == "" && ddlDbConnectorDbType.SelectedValue == "-1")
                {
                    strError += " * Please select database type.</br>";
                }
                else
                {
                }

            }
            else if (ddlDbConnectorDbType.SelectedValue == "5")
            {
                if (ddlDbDsns.SelectedValue == "-1")
                {
                    strError += " * Please select DSN name </br>";
                }
            }
            else
            {
                if (txtConnectionName.Text.Trim().Length > 20)
                    strError += " * Source  name cannot be more than 20 characters. </br>";
                if (txtHostName.Text.Trim().Length == 0)
                    strError += " * Please enter host name.</br>";
                if (txtDatabase.Text.Trim().Length == 0)
                    strError += " * Please enter database name.</br>";
                if (txtHostName.Text.Trim().Length > 150)
                    strError += " * Host name cannot be more than 150 characters. </br>";
                if (txtDatabase.Text.Trim().Length > 50)
                    strError += " * Database name cannot be more than 50 characters. </br>";
                if (dbcredential.SelectedValue == "-1")
                {
                    strError += " *  Please select type of credential</br>";
                }
            }
            

            if (strError.Length > 0)
            {
                isValid = false;
            }

            return isValid;
        }

        #endregion

        #region BindDsn With Mplugin Request
        private void BindDbDsnDropdwon(string _Agent)
        {
            DataTable objTable = GetDsnList(_Agent);
            ddlDbDsns.Items.Clear();
            if (objTable != null)
            {
                ddlDbDsns.DataSource = objTable;
                ddlDbDsns.DataTextField = "DsName";
                ddlDbDsns.DataValueField = "DsName";
                ddlDbDsns.DataBind();
                ddlDbDsns.Items.Insert(0, new ListItem(" Select DSN ", "-1"));
            }
            else ddlDbDsns.Items.Insert(0, new ListItem(" Select DSN ", "-1"));
        }
        private DataTable GetDsnList(string _Agent)
        {
            string strRqst, strUrl = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objAgent = new mPluginAgents();
            strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + this.CompanyId + "\",\"agtnm\":\"" + _Agent + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(this.CompanyId, _Agent) + "\"}}";
            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(this.CompanyId);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    strUrl = strUrl + "/MPGetODBCDSNList.aspx?d=" + Utilities.UrlEncode(strRqst);
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        DataTable dt = new DataTable();
                        MP_GetOdbcDsnListResp obj = new MP_GetOdbcDsnListResp(oResponse.ResponseText);
                        if (obj.Code == "0")
                        {
                            dt = obj.Data;
                        }
                        else
                        {
                            return null;
                        }
                        return dt;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        #endregion
   
        #region Database
        protected void ddlDbConnectorDbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strScript = "";
            if (hiddrpconnectortype.Value != "" && hdfDelId.Value != "")
            {
                ddlDbConnectorDbType.SelectedValue = hiddrpconnectortype.Value;
            }
            if (ddlDbConnectorDbType.SelectedValue == "5")
            {
                strScript = "$('#ddlDbDsnDiv').show();$('#ddlDbNonDsnDiv').hide();";
                BindDbDsnDropdwon(ddlUseMplugin.SelectedValue);
                updDdlDbDsns.Update();
            }
            else
            {
                strScript = "$('#ddlDbDsnDiv').hide();$('#ddlDbNonDsnDiv').show();";
            }

            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript, true);
        }
       
        protected void btnnextdb_click(object sender, EventArgs e)
        {
            string strCancelButton = "";
            if (hidcreuid.Value == "" && hidcrepwd.Value == "")
            {
                
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                return;
            }
            else if (hidcrepwd.Value == "")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
                return;
            }
            else
            {
                if (hidtestconnection.Value == "1" || hidtestconnection.Value == "0")
                {
                    DbTestConnctionCredential(updatedb, 2);
                    if (hiddrpconnectortype.Value == "5")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "addDSN();dsnTestConnection();cancelButtonShow();", true);
                        ddlDbConnectorDbType.SelectedValue = hiddrpconnectortype.Value;
                    }
                }


                if (hdfDelId.Value != "")
                {
                    strCancelButton = "cancelButtonShow();";
                }
                else
                {
                    strCancelButton = "cancelButtonhide();";
                }
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "Clearcredential();" + strCancelButton, true);
            }
            
        }

        protected void ddlUseMplugin_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strScript = "";
            if (ddlUseMplugin.SelectedValue != "-1")
            {

                if (ddlDbConnectorDbType.Items.FindByValue("5") == null)
                {
                    ddlDbConnectorDbType.Items.Insert(5, new ListItem("DSN (ODBC)", "5"));
                    ddlDbConnectorDbType.SelectedValue = "-1";
                    updDdlDbConn.Update();
                }
            }
            else
            {
                RemoveDropdownListOption(ddlDbConnectorDbType, "5");
                ddlDbConnectorDbType.SelectedValue = "-1";
                ddlDbDsns.SelectedValue = "-1";
                updDdlDbConn.Update(); updDdlDbDsns.Update();
                strScript = "$('#ddlDbDsnDiv').hide();$('#ddlDbNonDsnDiv').show();";
            }
            ScriptManager.RegisterStartupScript(updDdlMplugin, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript, true);
            if (hdfDelId.Value != "" && hiddrpconnectortype.Value !="")
            {
                ddlDbConnectorDbType.SelectedIndex = ddlDbConnectorDbType.Items.IndexOf(ddlDbConnectorDbType.Items.FindByValue(hiddrpconnectortype.Value));
                strScript = "ddConectordesable()";
               
                ScriptManager.RegisterStartupScript(updDdlMplugin, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript, true);
            }
        }

        private string ReplaceSingleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceSingleCourse(ReplaceBackSlash(_Input));
            }
        }


        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"\\", @"\\");
            }
        }

        private string ReplaceSingleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"'", @"\'");
            }
        }

        private DataTable GetMsSqlDataBaseTables(string _ConnectionString, DatabaseType _DataBaseType, string _DatabaseName)
        {
            GetTablesDetails objGetTablesDetails = new GetTablesDetails(_ConnectionString, _DataBaseType, _DatabaseName);
            objGetTablesDetails.Process();
            return objGetTablesDetails.ResultTable;
        }

        protected void btnDeleteClick(object sender, EventArgs e)
        {
            if (hiddatabaseobject.Value != "")
            {

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"initializeDbcontrol();SubProcImageNotDelConfirmation(true);$('#aMessage').html(' * Please delete all data objects of this source first');viewConnection('" + hiddbconnectornames.Value + "');", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"DbConnectorDeleteClick();", true);

            }
        }

        //Delete All Type of Sources After Yes Click
        protected void btnDeleteCfm_Click(object sender, EventArgs e)
        {
            switch (hdfDelType.Value.Trim())
            {
                case "1":
                    DeleteDatabaseConnector(this.SubAdminid, hdfDelId.Value, this.CompanyId);
                    getDBSources();
                    ddlDbConnectorDbType.SelectedValue = "-1";
                    updateadddbconnector.Update();
                    updDdlMplugin.Update();
                    ScriptManager.RegisterStartupScript(UpdDeleteConfirm, typeof(UpdatePanel), Guid.NewGuid().ToString(), "afterDeleteHideView();initializeDbcontrol();bindConnector();", true);
                    break;
                case "2":
                    DeleteWsConnector(this.SubAdminid, hdfWsConnId.Value, UpdDeleteConfirm, this.CompanyId);
                    getWSSources();
                    updateaddwebservice.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();", true);
                    wsclear();
                    break;
                case "5":
                    string strScript = "";
                    IdeODataHelper objHelper = new IdeODataHelper();
                    if (objHelper.DeleteODataConnector(this.CompanyId, this.SubAdminid, hdfODataConnId.Value))
                    {
                        strScript = @"$('#aCFmessage').html('OData service source deleted successfully.');
                                            SubProcConfirmBoxMessage(true,'  Deleted',250);SubProcImageDelConfirnmation(false);";

                        if (ddlOdataMpluginConn.SelectedValue != "-1")
                            RefreshConnector(this.CompanyId, ddlOdataMpluginConn.SelectedItem.Value, ddlOdataMpluginConn.SelectedItem.Text);
                        OdataTable();
                        updateodataright.Update();
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript + "intializeOdataSource();AfterAddOdata();", true);
                        oDataClear();
                    }
                    else
                    {
                        // strScript = @"$('[id$=txtOdataPass]').val('" + txtOdataPass.Text + "');ShowODataConnEdit();$('#aMessage').html('Data cannot be deleted.Internal server error.');SubProcBoxMessage(true);";
                        strScript = @"ShowODataConnEdit();$('#aMessage').html('Data cannot be deleted.Internal server error.');SubProcBoxMessage(true);";
                        ScriptManager.RegisterStartupScript(UpdDeleteConfirm, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript, true);
                    }
                    break;


            }
        }


       

        protected void DeleteDatabaseConnector(string _SubAdminId, string _ConnectionId, string _CompanyId)
        {

            DeleteConnection objDeleteConnection = new DeleteConnection(_SubAdminId, _ConnectionId, true, _CompanyId);
            objDeleteConnection.Process();
            if (objDeleteConnection.StatusCode == 0)
            {
                if (ddlUseMplugin.SelectedValue != "-1")
                    RefreshConnector(this.CompanyId, ddlUseMplugin.SelectedValue, ddlUseMplugin.SelectedItem.Text);
                ScriptManager.RegisterStartupScript(UpdDeleteConfirm, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCFmessage').html('Database source  deleted successfully.');$('#aCfmUpdateMessage').html('');SubProcConfirmBoxMessage(true,'  Deleted',250);SubProcImageDelConfirnmation(false);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdDeleteConfirm, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('SubProcBoxMessage(true);$('#aCFmessage').html('');$('#aCFmessage').html('Data cannot be deleted.Internal server error.'); ", true);
            }

        }



        protected void btnSaveDbConnectorDetail_Click(object sender, EventArgs e)
        {
            string strErrorMsg = string.Empty;
            if (!ValidateDbConnector(out strErrorMsg))
            {
                if (hdfDelId.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');cancelButtonShow();", true);
                }
                return;
            }
            else
            {
                saveDatabaseConnector();
            }
        }



        #region Discard Command

        protected void btnSaveDb_Click(object sender, EventArgs e)
        {
            string strErrorMsg = string.Empty;
            string strDataBaseType = "";
            if (hiddiscardcommadtype.Value == "1")
            {
                if (!ValidateDbConnector(out strErrorMsg))
                {
                    if (hdfDelId.Value == "")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');cancelButtonShow();", true);
                    }
                    return;
                }
                else
                {
                    UpdateDbConnection objUpdateDbConnection = new UpdateDbConnection(this.SubAdminid,
                                                                                      hidupdateconid.Value,
                                                                                         txtHostName.Text,
                                                                                         AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), (strDataBaseType == "5" ? ddlDbDsns.SelectedValue : txtDatabase.Text)),
                                                                                         (strDataBaseType == "5" ? "" : txtDatabaseTimeOut.Text),
                                                                                         (strDataBaseType == "5" ? "" : txtDatabaseAdditionalString.Text),
                                                                                         (ddlUseMplugin.SelectedValue == "-1" ? "" : ddlUseMplugin.SelectedValue), Convert.ToInt64(DateTime.UtcNow.Ticks), this.CompanyId, dbcredential.SelectedValue.ToString());
                    objUpdateDbConnection.Process1();
                    if (objUpdateDbConnection.StatusCode == 0)
                    {
                        if (ddlUseMplugin.SelectedValue == "-1")
                        {
                            if (hdfUseMplugin.Value != "-1")
                                RefreshConnector(this.CompanyId, hdfUseMplugin.Value, hdfUseMplugin.Value);
                            else
                            {
                            }
                        }
                        else
                        {
                            if (hdfUseMplugin.Value != "-1")
                                RefreshConnector(this.CompanyId, hdfUseMplugin.Value, hdfUseMplugin.Value);
                            else
                                RefreshConnector(this.CompanyId, ddlUseMplugin.SelectedValue, ddlUseMplugin.SelectedItem.Text);
                        }
                        getDBSources();
                        updDbConnBtn.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');SubModifiction(false);", true);
                        return;
                    }
                    hiddiscardcommadtype.Value = "";
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"bindConnector();discardedit();$('[id$=hideditdone]').val('');SubModifiction(false);", true);
                }
            }
            else if (hiddiscardcommadtype.Value == "2")
            {
                if (!ValidateDbConnector(out strErrorMsg))
                {
                    if (hdfDelId.Value == "")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');cancelButtonShow();", true);
                    }
                    return;
                }
                else
                {
                    UpdateDbConnection objUpdateDbConnection = new UpdateDbConnection(this.SubAdminid,
                                                                                      hidupdateconid.Value,
                                                                                         txtHostName.Text,
                                                                                         AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), (strDataBaseType == "5" ? ddlDbDsns.SelectedValue : txtDatabase.Text)),
                                                                                         (strDataBaseType == "5" ? "" : txtDatabaseTimeOut.Text),
                                                                                         (strDataBaseType == "5" ? "" : txtDatabaseAdditionalString.Text),
                                                                                         (ddlUseMplugin.SelectedValue == "-1" ? "" : ddlUseMplugin.SelectedValue), Convert.ToInt64(DateTime.UtcNow.Ticks), this.CompanyId, dbcredential.SelectedValue.ToString());
                    objUpdateDbConnection.Process1();
                    if (objUpdateDbConnection.StatusCode == 0)
                    {
                        if (ddlUseMplugin.SelectedValue == "-1")
                        {
                            if (hdfUseMplugin.Value != "-1")
                                RefreshConnector(this.CompanyId, hdfUseMplugin.Value, hdfUseMplugin.Value);
                            else
                            {
                            }
                        }
                        else
                        {
                            if (hdfUseMplugin.Value != "-1")
                                RefreshConnector(this.CompanyId, hdfUseMplugin.Value, hdfUseMplugin.Value);
                            else
                                RefreshConnector(this.CompanyId, ddlUseMplugin.SelectedValue, ddlUseMplugin.SelectedItem.Text);
                        }
                        getDBSources();
                        updDbConnBtn.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                        return;
                    }
                }
                updateadddbconnector.Update();
                updatedbright.Update();
                hidupdateconid.Value = "";
                lbldbconnector.Text = "";
                updViewDbConnDetails.Update();
                ddlDbConnectorDbType.SelectedValue = "-1";
                ddlUseMplugin.SelectedValue = "-1";
                hideditdone.Value = "";
                hiddiscardcommadtype.Value = "";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "initializeDbcontrol();bindConnector();RemoveshowConndivOnEdit();SubModifiction(false);", true);
            }
            else if (hiddiscardcommadtype.Value == "3")
            {
                SaveDiscardChanges();

            }
            else if (hiddiscardcommadtype.Value == "4")
            {
                SaveDiscardChanges();
                ScriptManager.RegisterStartupScript(upadd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();BindWsConnectors();resetclictadd();WsAddNewConnector();", true);
            }
            else if (hiddiscardcommadtype.Value == "5")
            {
                DiscardSaveodata();
                if (hidodatasavediscard.Value != "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "chkODataHttpOnChange(this);AfterAddOdata();clearconId();AfterAddClick();intializeOdataSource();SubModifiction(false);discardedit();", true);
                }
                hidodatasavediscard.Value = "";
                hiddiscardcommadtype.Value = "";
            }
            else if (hiddiscardcommadtype.Value == "6")
            {
                DiscardSaveodata();
                oDataClear();
                OdataTable();
                updateodataright.Update();
                txtodatapredefinedpass.Text = "";
                hideditdone.Value = "";
                hiddiscardcommadtype.Value = "";
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "SubModifiction(true);intializeOdataSource();RemoveEdit();AfterAddOdata();SubModifiction(false);", true);
                //  ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "intializeOdataSource();AfterAddOdata();AddOdataConnector();RemoveEdit();AfterAddClick();", true);
            }
        }

        private void saveDatabaseConnector()
        {
            string strDataBaseType = ddlDbConnectorDbType.SelectedValue;
            if (hidupdateconid.Value.Trim().Length == 0)
            {
                GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, "", txtConnectionName.Text, this.CompanyId);
                if (objGetDatabaseConnection.StatusCode == 0 && objGetDatabaseConnection.ResultTable.Rows.Count>0)
                {
                    ScriptManager.RegisterStartupScript(updatedatasource, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"showConndivOnEdit();SubProcBoxMessage(true);$('#aMessage').html('Database source name already exist.');", true);
                    return;
                }
                else
                {
                    AddDatabaseConnection objAdddabaseconnection = new AddDatabaseConnection(this.SubAdminid,
                                                                        txtConnectionName.Text.Trim(),
                                                                        txtHostName.Text,
                                                                        AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), (strDataBaseType == "5" ? ddlDbDsns.SelectedValue : txtDatabase.Text)),
                                                                        strDataBaseType,
                                                                        (strDataBaseType == "5" ? "" : txtDatabaseTimeOut.Text),
                                                                        (strDataBaseType == "5" ? "" : txtDatabaseAdditionalString.Text),
                                                                        (ddlUseMplugin.SelectedValue == "-1" ? "" : ddlUseMplugin.SelectedValue),
                                                                        this.CompanyId, this.SubAdminid, Convert.ToInt64(DateTime.UtcNow.Ticks), Convert.ToInt64(DateTime.UtcNow.Ticks), dbcredential.SelectedValue.ToString());
                    objAdddabaseconnection.Process1();
                    if (objAdddabaseconnection.StatusCode == 0)
                    {
                        if (ddlUseMplugin.SelectedValue != "-1")
                            RefreshConnector(this.CompanyId, ddlUseMplugin.SelectedValue, ddlUseMplugin.SelectedItem.Text);
                        else
                        {
                        }
                        getDBSources();
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "initializeDbcontrol();bindConnector();", true);
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCFmessage').html('Database source saved successfully.');SubProcConfirmBoxMessageDB(true,'  Saved',250);", true);
                        AfterSuccessfullysaved();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('Data cannot be saved.Internal server error.');SubProcConfirmBoxMessageDB(true,'  Error',250);", true);
                        return;
                    }
                }
            }
            else
            {
                UpdateDbConnection objUpdateDbConnection = new UpdateDbConnection(this.SubAdminid,
                                                                                 hidupdateconid.Value,
                                                                                    txtHostName.Text,
                                                                                    AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), (strDataBaseType == "5" ? ddlDbDsns.SelectedValue : txtDatabase.Text)),
                                                                                    (strDataBaseType == "5" ? "" : txtDatabaseTimeOut.Text),
                                                                                    (strDataBaseType == "5" ? "" : txtDatabaseAdditionalString.Text),
                                                                                    (ddlUseMplugin.SelectedValue == "-1" ? "" : ddlUseMplugin.SelectedValue), Convert.ToInt64(DateTime.UtcNow.Ticks), this.CompanyId, dbcredential.SelectedValue.ToString());
                objUpdateDbConnection.Process1();
                if (objUpdateDbConnection.StatusCode == 0)
                {
                    if (ddlUseMplugin.SelectedIndex <=0)
                    {
                        if (hdfUseMplugin.Value != "" && hdfUseMplugin.Value != "-1")
                            RefreshConnector(this.CompanyId, hdfUseMplugin.Value, hdfUseMplugin.Value);
                    }
                    else
                    {
                        if (hdfUseMplugin.Value !="" && hdfUseMplugin.Value != "-1")
                            RefreshConnector(this.CompanyId, hdfUseMplugin.Value, hdfUseMplugin.Value);
                        RefreshConnector(this.CompanyId, ddlUseMplugin.SelectedValue, ddlUseMplugin.SelectedItem.Text);
                    }
                    getDBSources();
                    ScriptManager.RegisterStartupScript(updDbConnBtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), "initializeDbcontrol();bindConnector();", true);
                    ScriptManager.RegisterStartupScript(updDbConnBtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('Database source updated successfully.');SubProcConfirmBoxMessage(true,'  Saved',250);SubProcPriview(false);RemoveshowConndivOnEdit();", true);
                    AfterSuccessfullysaved();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(updDbConnBtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aCFmessage').html('Data cannot be saved.Internal server error.');", true);
                    return;
                }
            }
        }

        #endregion
        private void RefreshConnector(string _CompanyId, string _AgentId, string _AgentName)
        {
            string strRqst, strUrl = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objAgent = new mPluginAgents();
            strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + _CompanyId + "\",\"agtid\":\"" + _AgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(_CompanyId, _AgentName) + "\"}}";

            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    try
                    {
                        strUrl = strUrl + "/MPRefreshConnectors.aspx?d=" + Utilities.UrlEncode(strRqst);
                        HTTP oHttp = new HTTP(strUrl);
                        oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                        oHttp.Request();
                    }
                    catch
                    {
                    }
                }
                else
                {
                }
            }
            else { }
        }

        private void AfterSuccessfullysaved()
        {
            updatedatasource.Update();
            updateadddbconnector.Update();
            ddlUseMplugin.SelectedValue = "-1";
            drpCredentiail.SelectedValue = "-1";
            ddlUseMplugin_SelectedIndexChanged(null, null);
            updDdlMplugin.Update();
        }


        private void RemoveDropdownListOption(DropDownList _ddl, string _Value)
        {
            if (_ddl.Items.FindByValue(_Value) != null)
            {
                string itemText = _ddl.Items.FindByValue(_Value).Text;
                ListItem li = new ListItem();
                li.Text = itemText;
                li.Value = _Value;
                _ddl.Items.Remove(li);
            }
        }

        #endregion

        #region Database TestConnection Button Click
        protected void btnTestConnection_Click(object sender, EventArgs e)
        {
            string strCancelButton = "cancelButtonhide();";
            if (hidcreuid.Value == "" && hidcrepwd.Value == "" && hiddrpconnectortype.Value != "5")
            {
                txtdbusercreatial.Text = "";
                txtdbpasswordcreatial.Text = "";
                updatedb.Update();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);showConndivOnEdit();", true);
            }
            else if (hidcrepwd.Value == "" && hiddrpconnectortype.Value != "5")
            {

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);showConndivOnEdit();", true);
            }
            else
            {
                if (hidupdateconid.Value.Trim().Length > 0)
                    strCancelButton = "cancelButtonShow(); $('[id$=btnreset]').hide();";
                DbTestConnction(updDbConnBtn, 1);
            }
            if (hiddrpconnectortype.Value == "5")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "addDSN();dsnTestConnection();" + strCancelButton, true);
                ddlDbConnectorDbType.SelectedValue = hiddrpconnectortype.Value;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), strCancelButton, true);
            }
        }
        protected void btnDsnGet_Click(object sender, EventArgs e)
        {
            BindDbDsnDropdwon(ddlUseMplugin.SelectedValue);
            updDdlDbDsns.Update();

            ddlDbDsns.SelectedIndex = ddlDbDsns.Items.IndexOf(ddlDbDsns.Items.FindByText(txtDatabase.Text));
        }
        protected void btnTestConnectionOnEdit_Click(object sender, EventArgs e)
        {

            if (hidcreuid.Value == "" && hidcrepwd.Value == "" && hiddrpconnectortype.Value != "5")
            {
                txtdbusercreatial.Text = "";
                txtdbpasswordcreatial.Text = "";
                updatedb.Update();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
            }
            else if (hidcrepwd.Value == "" && hiddrpconnectortype.Value != "5")
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DataCredentialBy(true);", true);
            else
                DbTestConnction(updDbConnTestBtn, 2);
            if (hiddrpconnectortype.Value == "5")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "addDSN();dsnTestConnection();cancelButtonShow();", true);
            }
        }

        private void DbTestConnction(UpdatePanel _Upd, Int32 _Type)
        {
            string strErrorMsg = string.Empty;
            if (!ValidateDbConnector(out strErrorMsg))
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');", true);
                return;
            }
            else
            {
                try
                {
                    TestConnection objTestConn = new TestConnection(ddlDbConnectorDbType.SelectedValue, txtHostName.Text, (ddlDbConnectorDbType.SelectedValue.Trim() == "5" ? ddlDbDsns.SelectedValue : txtDatabase.Text), hidcreuid.Value, hidcrepwd.Value, (txtDatabaseTimeOut.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(txtDatabaseTimeOut.Text.Trim())), txtDatabaseAdditionalString.Text, this.CompanyId, (ddlUseMplugin.SelectedValue == "-1" ? "" : ddlUseMplugin.SelectedValue));
                    if (objTestConn.Process())
                    {
                        ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxConnectedMessage(true);$('#aCFmessage').html('');$('#aCFmessage').html('" + "* Database  connected Successfully." + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* Database not connected." + "');cancelButtonShow();", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "'); cancelButtonhide();", true);
                }

            }
        }

        private void DbTestConnctionCredential(UpdatePanel _Upd, Int32 _Type)
        {
            string strErrorMsg = string.Empty;
            if (!ValidateDbConnector(out strErrorMsg))
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorMsg + "');", true);
                return;
            }
            else
            {
                try
                {
                    TestConnection objTestConn = new TestConnection(ddlDbConnectorDbType.SelectedValue, txtHostName.Text, (ddlDbConnectorDbType.SelectedValue.Trim() == "5" ? ddlDbDsns.SelectedValue : txtDatabase.Text), hidcreuid.Value, hidcrepwd.Value, (txtDatabaseTimeOut.Text.Trim().Length == 0 ? 0 : Convert.ToInt32(txtDatabaseTimeOut.Text.Trim())), txtDatabaseAdditionalString.Text, this.CompanyId, (ddlUseMplugin.SelectedValue == "-1" ? "" : ddlUseMplugin.SelectedValue));
                    if (objTestConn.Process())
                    {
                        ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxConnectedMessage(true);$('#aCFmessage').html('');$('#aCFmessage').html('" + "* Database  connected Successfully." + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* Database not connected." + "');", true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + "* " + ex.Message + "'); cancelButtonhide();", true);
                }

            }
        }
        #endregion

        #region Webservice
        
        protected void bindPridefinedCreadential()
        {
            GetEnterpriseAdditionalDefinition objAuthMeta = new GetEnterpriseAdditionalDefinition(this.CompanyId);
            if (objAuthMeta.StatusCode == 0)
            {
                if (objAuthMeta.StatusCode == 0 && objAuthMeta.ResultTable.Rows.Count > 0)
                {
                    hidWsAuthencationMeta.Value = Convert.ToString(objAuthMeta.ResultTable.Rows[0]["AUTHENTICATION_META"]);
                    List<ReplaceUserCredentialForall> lst = Utilities.DeserialiseJson<List<ReplaceUserCredentialForall>>(hidWsAuthencationMeta.Value);
                    List<ReplaceUserCredentialForall> lst1 = new List<ReplaceUserCredentialForall>();
                    hidWsAuthencationMeta.Value = "";
                    if (lst.Count != 0)
                    {
                        foreach (ReplaceUserCredentialForall item in lst)
                        {
                            if (item.uid != "")
                                item.uid = AesEncryption.AESDecrypt(CompanyId, item.uid);
                            if (item.pwd != "")
                                item.pwd = AesEncryption.AESDecrypt(CompanyId, item.pwd);
                            lst1.Add(item);
                        }
                        var jsonSerialiser = new JavaScriptSerializer();
                        var json = jsonSerialiser.Serialize(lst1);
                        hidWsAuthencationMeta.Value = json;
                    }
                }
                DrpCredential(hidWsAuthencationMeta.Value, drpCredentiail);
                DrpCredential(hidWsAuthencationMeta.Value, drpOCredentiail);
                DrpCredential(hidWsAuthencationMeta.Value, dbcredential);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", @"SubProcBoxConnectedMessageqqq(true);$('#aMessage').html('" + "* " + "Internal Server Error" + "');", true);
                return;
            }
        }


        protected void btnSaveWsConnectorDetail1_Click(object sender, EventArgs e)
        {
            SaveWsConnectorWsdl(txtwsspredefined.Text, txtwspredefinedpassword.Text);
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(),"SubModifiction(false);", true);


        }
        protected void btnSaveWsConnectorDetail_Click(object sender, EventArgs e)
        {
            string username, password = "";
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append((txtWsConnectionName.Text.Trim().Length == 0) ? " * Please enter web service source  name.</br>" : "");
            sbMessage.Append((txtWsConnectorUrl.Text.Trim().Length == 0) ? " * Please enter web service url.</br>" : "");
            sbMessage.Append((ddlWebSrcConnectorType.SelectedValue == "-1") ? " * Please select web service type.</br>" : "");
            sbMessage.Append((txtWsConnectionName.Text.Trim().Length > 50) ? " * Web service source  name can not be more than 50 characters.</br>" : "");
            if (chkhttp.Checked)
            {
                if (drpCredentiail.SelectedValue == "-1")
                {
                    sbMessage.Append(" * Please select  credential.</br>");
                }
            }
            if (sbMessage.ToString().Length != 0)
            {
                string strScript = "";
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);" + "$('#aMessage').html('" + sbMessage.ToString() + "');" + "chkhttpOnChange(this);" + strScript, true);
                if (hdfWsConnId.Value.Trim().Length != 0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"ShowWsConnDivOnEdit();", true);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", sbMessage.ToString(), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", sbMessage.ToString(), true);
                }
                return;
            }
            else
            {
               
                username = hidwsusername.Value;
                password = hidwsusernamepassword.Value;
                if (chkhttp.Checked == true)
                {
                    if (hdfWsConnId.Value.Trim().Length != 0)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"ShowWsConnDivOnEdit();", true);
                    }

                    if (username == "" && password == "" && ddlWebSrcConnectorType.SelectedValue == "wsdl")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "WSDataCredentialBy(true);", true);
                        return;
                    }
                    else if (password == "" && ddlWebSrcConnectorType.SelectedValue == "wsdl")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "WSDataCredentialBy(true);", true);
                        return;
                    }

                    else
                    {
                        SaveWsConnector(username, password);
                    }
                }
                else
                {
                    SaveWsConnector(username, password);
                }
            }

        }

        private void SaveWsConnector(string _UserName, string _Password)
        {
            if (this.SubAdminid.Length == 0) return;
            string strWsdlContent = "", strErrorDesc = "";
            StringBuilder sbScript = new StringBuilder();
            Boolean bolChangeWsdlContent = true;
            if (ddlWebSrcConnectorType.SelectedValue == "wsdl")
            {

                if (hdfWsConnId.Value.Trim().Length != 0)
                    if (hdfWsSavedUrl.Value.Trim() == txtWsConnectorUrl.Text.Trim())
                        bolChangeWsdlContent = false;

                if (bolChangeWsdlContent)
                {
                    if (ddlWsUseMplugin.SelectedValue.Trim() == "-1")
                    {
                        strWsdlContent = GetWsdlFileOnline(chkhttp.Checked, _UserName, _Password);
                        if (strWsdlContent.Length == 0) strWsdlContent = GetWsdlFileMannual();
                        if (strWsdlContent.Length == 0)
                        {
                            Iframe1.Attributes["src"] = "UploadWsdlFile.aspx?id=" + this.SubAdminid + "&cid=" + this.CompanyId;
                            updWsdlIfrm.Update();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxUldWsdlCfm(true,400,1);", true);
                            if (hdfWsConnId.Value != "") ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('[id$=btnwsreset]').hide();$('[id$=btnHtmWsConnectorBack]').show();", true);
                            return;
                        }
                        else { }
                    }
                    else
                    {
                        strErrorDesc = "";
                        strWsdlContent = GetWsdlFromMetaData(this.CompanyId, ddlWsUseMplugin.SelectedValue.Trim(), txtWsConnectorUrl.Text.Trim(), _UserName, _Password, out strErrorDesc);
                        if (strWsdlContent.Length == 0)
                        {
                            strErrorDesc = (strErrorDesc.Length == 0 ? "Wsdl does not exists." : strErrorDesc);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('[SubProcBoxMessage(true);$('#aMessage').html('" + strErrorDesc + "');", true);

                            return;
                        }
                        else { }
                    }
                }
            }
            else
            {
                strWsdlContent = "";
            }
            if (hdfWsConnId.Value.Trim().Length == 0)
            {
                GetWebserviceConnection objGetWsConnection = new GetWebserviceConnection(false, this.SubAdminid, "", txtWsConnectionName.Text, this.CompanyId);
                objGetWsConnection.Process1();
                if (objGetWsConnection.StatusCode == 0 && objGetWsConnection.ResultTable.Rows.Count>0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Web service source name already exist.');", true);
                    return;
                }
                else
                {
                    AddWebserviceConnection objAdddabaseconnection =
                        new AddWebserviceConnection(this.SubAdminid, ddlWebSrcConnectorType.SelectedValue, txtWsConnectorUrl.Text,
                                                    (chkhttp.Checked ? "": ""),
                                                    (chkhttp.Checked ? "" : ""),
                                                   (chkhttp.Checked ? drphttpauthenticationtype.SelectedValue.ToString() : ""),
                                                    this.CompanyId, ddlWebSrcConnectorRsp.SelectedValue,
                                                    txtWsConnectionName.Text,
                                                    (ddlWebSrcConnectorType.SelectedValue == "wsdl" ? txtWsConnectorUrl.Text + "?wsdl" : ""),
                                                    strWsdlContent, (ddlWsUseMplugin.SelectedValue == "-1" ? "" : ddlWsUseMplugin.SelectedValue), drpCredentiail.SelectedValue);
                    objAdddabaseconnection.Process2();
                    if (objAdddabaseconnection.StatusCode == 0)
                    {
                        getWSSources();
                        wsclear();
                        updateaddwebservice.Update();
                        sbScript.Append("$('#aCFmessage').html('Web service connector saved successfully.');SubProcConfirmBoxMessage(true,'  Saved',350);");
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();InitializeControls();", true);
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), sbScript.ToString(), true);
                        if (ddlWsUseMplugin.SelectedValue != "-1")
                       RefreshConnector(this.CompanyId, ddlWsUseMplugin.SelectedValue, ddlWsUseMplugin.SelectedItem.Text);
    
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"ShowWsConnDivOnEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                        return;
                    }
                }
            }
            else
            {
                if (ddlWebSrcConnectorType.SelectedValue != "wsdl")
                {
                    bolChangeWsdlContent = false;
                }

                UpdateWsConnection objUpdateWsConnection =
                    new UpdateWsConnection(this.SubAdminid, hdfWsConnId.Value, txtWsConnectorUrl.Text,
                                          (chkhttp.Checked ? "" : ""),
                                          (chkhttp.Checked ? "" : ""),
                                          (chkhttp.Checked ? drphttpauthenticationtype.SelectedValue : ""),
                                          ddlWebSrcConnectorRsp.SelectedValue,
                                          (ddlWebSrcConnectorType.SelectedValue == "wsdl" ? txtWsConnectorUrl.Text + "?wsdl" : ""),
                                          strWsdlContent, bolChangeWsdlContent,
                                          (ddlWsUseMplugin.SelectedValue == "-1" ? "" : ddlWsUseMplugin.SelectedValue), this.CompanyId, drpCredentiail.SelectedValue);
                objUpdateWsConnection.Process2();

                if (objUpdateWsConnection.StatusCode == 0)
                {
                    RefreshConnector(this.CompanyId, ddlWsUseMplugin.SelectedValue, ddlWsUseMplugin.SelectedItem.Text);
                    UpdateWsConnectorOnUpdate(this.CompanyId);
                    getWSSources();
                    wsclear();
                    updateaddwebservice.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();CancelUpdate();test();$('[id$=hideditdone]').val('');InitializeControls();resetclictadd();", true);
                    sbScript.Append("$('#aCFmessage').html('Web service source updated successfully.');SubProcConfirmBoxMessage(true,'  Saved',350);");
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), sbScript.ToString(), true);
                }
                else
                {

                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('Data cannot be saved.Internal server error.');SubProcConfirmBoxMessage(true,' Error',350);", true);
                    return;
                }
            }
        }

        private void SaveWsConnectorWsdl(string _UserName, string _Password)
        {
            if (this.SubAdminid.Length == 0) return;
            string strWsdlContent = "", strErrorDesc = "";
            StringBuilder sbScript = new StringBuilder();
            Boolean bolChangeWsdlContent = true;
            if (ddlWebSrcConnectorType.SelectedValue == "wsdl")
            {

                if (hdfWsConnId.Value.Trim().Length != 0)
                    if (hdfWsSavedUrl.Value.Trim() == txtWsConnectorUrl.Text.Trim())
                        bolChangeWsdlContent = false;

                if (bolChangeWsdlContent)
                {
                    if (ddlWsUseMplugin.SelectedValue.Trim() == "-1")
                    {
                        strWsdlContent = GetWsdlFileOnline(chkhttp.Checked, _UserName, _Password);
                        if (strWsdlContent.Length == 0) strWsdlContent = GetWsdlFileMannual();
                        if (strWsdlContent.Length == 0)
                        {
                            Iframe1.Attributes["src"] = "UploadWsdlFile.aspx?id=" + this.SubAdminid + "&cid=" + this.CompanyId;
                            updWsdlIfrm.Update();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxUldWsdlCfm(true,400,1); ", true);
                            if (hdfWsConnId.Value != "") ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('[id$=btnwsreset]').hide();$('[id$=btnHtmWsConnectorBack]').show();", true);

                            return;
                        }
                        else { }
                    }
                    else
                    {
                        strErrorDesc = "";
                        strWsdlContent = GetWsdlFromMetaData(this.CompanyId, ddlWsUseMplugin.SelectedValue.Trim(), txtWsConnectorUrl.Text.Trim(), _UserName, _Password, out strErrorDesc);
                        if (strWsdlContent.Length == 0)
                        {
                            strErrorDesc = (strErrorDesc.Length == 0 ? "Wsdl does not exists." : strErrorDesc);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('[SubProcBoxMessage(true);$('#aMessage').html('" + strErrorDesc + "');", true);

                            return;
                        }
                        else { }
                    }
                }
            }
            else
            {
                strWsdlContent = "";
            }
            if (hdfWsConnId.Value.Trim().Length == 0)
            {
                GetWebserviceConnection objGetWsConnection = new GetWebserviceConnection(false, this.SubAdminid, "", txtWsConnectionName.Text, this.CompanyId);
                objGetWsConnection.Process1();
                if (objGetWsConnection.StatusCode == 0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('Web service source name already exist.');", true);
                    return;
                }
                else
                {
                    AddWebserviceConnection objAdddabaseconnection =
                        new AddWebserviceConnection(this.SubAdminid, ddlWebSrcConnectorType.SelectedValue, txtWsConnectorUrl.Text,
                                                    (chkhttp.Checked ? "" : ""),
                                                    (chkhttp.Checked ? "" : ""),
                                                   (chkhttp.Checked ? drphttpauthenticationtype.SelectedValue.ToString() : ""),
                                                    this.CompanyId, ddlWebSrcConnectorRsp.SelectedValue,
                                                    txtWsConnectionName.Text,
                                                    (ddlWebSrcConnectorType.SelectedValue == "wsdl" ? txtWsConnectorUrl.Text + "?wsdl" : ""),
                                                    strWsdlContent, (ddlWsUseMplugin.SelectedValue == "-1" ? "" : ddlWsUseMplugin.SelectedValue), drpCredentiail.SelectedValue);
                    objAdddabaseconnection.Process2();
                    if (objAdddabaseconnection.StatusCode == 0)
                    {
                        getWSSources();
                        wsclear();
                        updateaddwebservice.Update();
                        sbScript.Append("$('#aCFmessage').html('Web service connector saved successfully.');SubProcConfirmBoxMessage(true,'  Saved',350);");
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();", true);
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), sbScript.ToString(), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"ShowWsConnDivOnEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.');", true);
                        return;
                    }
                }
            }
            else
            {


                UpdateWsConnection objUpdateWsConnection =
                    new UpdateWsConnection(this.SubAdminid, hdfWsConnId.Value, txtWsConnectorUrl.Text,
                                          (chkhttp.Checked ? "" : ""),
                                          (chkhttp.Checked ? "" : ""),
                                          (chkhttp.Checked ? drphttpauthenticationtype.SelectedValue : ""),
                                          ddlWebSrcConnectorRsp.SelectedValue,
                                          (ddlWebSrcConnectorType.SelectedValue == "wsdl" ? txtWsConnectorUrl.Text + "?wsdl" : ""),
                                          strWsdlContent, bolChangeWsdlContent,
                                          (ddlWsUseMplugin.SelectedValue == "-1" ? "" : ddlWsUseMplugin.SelectedValue), this.CompanyId, drpCredentiail.SelectedValue);
                objUpdateWsConnection.Process2();

                if (objUpdateWsConnection.StatusCode == 0)
                {
                    getWSSources();
                    wsclear();
                    updateaddwebservice.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeControls();InitializeWsConnectors();CancelUpdate();test();InitializeControls();", true);
                    sbScript.Append("$('#aCFmessage').html('Web service source updated successfully.');SubProcConfirmBoxMessage(true,'  Saved',350);$('[id$=hideditdone]').val('');");
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), sbScript.ToString(), true);
                }
                else
                {

                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('Data cannot be saved.Internal server error.');SubProcConfirmBoxMessage(true,' Error',350);", true);
                    return;
                }
            }
        }

        private void SaveDiscardChanges()
        {
            string username, password = "";
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.Append((txtWsConnectionName.Text.Trim().Length == 0) ? " * Please enter web service source  name.</br>" : "");
            sbMessage.Append((txtWsConnectorUrl.Text.Trim().Length == 0) ? " * Please enter web service url.</br>" : "");
            sbMessage.Append((ddlWebSrcConnectorType.SelectedValue == "-1") ? " * Please select web service type.</br>" : "");
            sbMessage.Append((txtWsConnectionName.Text.Trim().Length > 50) ? " * Web service source  name can not be more than 50 characters.</br>" : "");
            if (chkhttp.Checked)
            {
                if (drpCredentiail.SelectedValue == "-1")
                {
                    sbMessage.Append(" * Please select  credential.</br>");
                }
            }
            if (sbMessage.ToString().Length != 0)
            {
                string strScript = "";
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);" + "$('#aMessage').html('" + sbMessage.ToString() + "');" + "chkhttpOnChange(this);" + strScript, true);
                if (hdfWsConnId.Value.Trim().Length != 0)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"ShowWsConnDivOnEdit();", true);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", sbMessage.ToString(), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", sbMessage.ToString(), true);
                }
                return;
            }
            else
            {
                username = hidwsusername.Value;
                password = hidwsusernamepassword.Value;
                if (chkhttp.Checked == true)
                {
                    if (username == "" && password == "" && ddlWebSrcConnectorType.SelectedValue == "wsdl")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "WSDataCredentialBy(true);;", true);
                        return;
                    }
                    else
                    {
                        DiscardSaveWsConnector(username, password);
                        wsclear();
                        getWSSources();
                        updateaddwebservice.Update();
                        hideditdone.Value = "";
                        hiddiscardcommadtype.Value = "";

                    }
                }
                else
                {
                    DiscardSaveWsConnector(username, password);
                    wsclear();
                    getWSSources();
                    updateaddwebservice.Update();
                    hideditdone.Value = "";
                    hiddiscardcommadtype.Value = "";

                }
            }

        }




        private void UpdateWsConnectorOnUpdate(string _CompanyId)
        {
            if (ddlWsUseMplugin.SelectedValue == "-1")
            {
                if (hdfWsUseMplugin.Value != "-1")
                    RefreshConnector(_CompanyId, hdfWsUseMplugin.Value, hdfWsUseMplugin.Value);
                else { }
            }
            else
            {
                if (hdfWsUseMplugin.Value != "-1")
                    RefreshConnector(_CompanyId, hdfWsUseMplugin.Value, hdfWsUseMplugin.Value);
                else
                    RefreshConnector(_CompanyId, ddlWsUseMplugin.SelectedValue, ddlWsUseMplugin.SelectedItem.Text);
            }
        }

      


        private void DiscardSaveWsConnector(string _UserName, string _Password)
        {
            if (this.SubAdminid.Length == 0) return;
            string strWsdlContent = "", strErrorDesc = "";
            StringBuilder sbScript = new StringBuilder();
            Boolean bolChangeWsdlContent = true;
            if (ddlWebSrcConnectorType.SelectedValue == "wsdl")
            {

                if (hdfWsConnId.Value.Trim().Length != 0)
                    if (hdfWsSavedUrl.Value.Trim() == txtWsConnectorUrl.Text.Trim())
                        bolChangeWsdlContent = false;

                if (bolChangeWsdlContent)
                {
                    if (ddlWsUseMplugin.SelectedValue.Trim() == "-1")
                    {
                        strWsdlContent = GetWsdlFileOnline(chkhttp.Checked, _UserName, _Password);
                        if (strWsdlContent.Length == 0) strWsdlContent = GetWsdlFileMannual();
                        if (strWsdlContent.Length == 0)
                        {
                            Iframe1.Attributes["src"] = "UploadWsdlFile.aspx?id=" + this.SubAdminid + "&cid=" + this.CompanyId;
                            updWsdlIfrm.Update();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxUldWsdlCfm(true,400,1);", true);

                            return;
                        }
                        else { }
                    }
                    else
                    {
                        strErrorDesc = "";
                        strWsdlContent = GetWsdlFromMetaData(this.CompanyId, ddlWsUseMplugin.SelectedValue.Trim(), txtWsConnectorUrl.Text.Trim(), _UserName, _Password, out strErrorDesc);
                        if (strWsdlContent.Length == 0)
                        {
                            strErrorDesc = (strErrorDesc.Length == 0 ? "Wsdl does not exists." : strErrorDesc);
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('[SubProcBoxMessage(true);$('#aMessage').html('" + strErrorDesc + "');", true);

                            return;
                        }
                        else { }
                    }
                }
            }
            else
            {
                strWsdlContent = "";
            }
          
                if (ddlWebSrcConnectorType.SelectedValue != "wsdl")
                {
                    bolChangeWsdlContent = false;
                }

                UpdateWsConnection objUpdateWsConnection =
                    new UpdateWsConnection(this.SubAdminid, hdfWsConnId.Value, txtWsConnectorUrl.Text,
                                          (chkhttp.Checked ? "" : ""),
                                          (chkhttp.Checked ? "" : ""),
                                          (chkhttp.Checked ? drphttpauthenticationtype.SelectedValue : ""),
                                          ddlWebSrcConnectorRsp.SelectedValue,
                                          (ddlWebSrcConnectorType.SelectedValue == "wsdl" ? txtWsConnectorUrl.Text + "?wsdl" : ""),
                                          strWsdlContent, bolChangeWsdlContent,
                                          (ddlWsUseMplugin.SelectedValue == "-1" ? "" : ddlWsUseMplugin.SelectedValue), this.CompanyId, drpCredentiail.SelectedValue);
                objUpdateWsConnection.Process2();

                if (objUpdateWsConnection.StatusCode == 0)
                {
                    RefreshConnector(this.CompanyId, ddlWsUseMplugin.SelectedValue, ddlWsUseMplugin.SelectedItem.Text);
                    UpdateWsConnectorOnUpdate(this.CompanyId);
                    getWSSources();
                    wsclear();
                    updateaddwebservice.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();CancelUpdate();test();$('[id$=hideditdone]').val('');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('Data cannot be saved.Internal server error.');SubProcConfirmBoxMessage(true,' Error',350);", true);
                    return;
                }
                hiddiscardcommadtype.Value = "";

                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"InitializeWsConnectors();discardedit();$('[id$=hideditdone]').val('');SubModifiction(false);", true);
            
        }


        #region WSDL Service MetaData fetching

        private string GetWsdlFromMetaData(string _CompanyId, string _AgentName, string _Url, string _UserName, string _PassWord, out string _Desc)
        {
            string strRqst, strUrl = "", stWsdl = "";
            _Desc = "";
            try
            {
                string strTicks = Convert.ToString(DateTime.Now.Ticks);
                string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
                mPluginAgents objAgent = new mPluginAgents();
                strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + _CompanyId + "\",\"agtnm\":\"" + _AgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(this.CompanyId, _AgentName) + "\",\"wsurl\":\"" + _Url + "\",\"hunm\":\"" + _UserName + "\",\"hpwd\":\"" + _PassWord + "\"}}";

                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
                objServerUrl.Process();
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (strUrl.Length > 0)
                    {
                        strUrl = strUrl + "/MPGetWSDLInformation.aspx?d=" + Utilities.UrlEncode(strRqst);
                        HTTP oHttp = new HTTP(strUrl);
                        oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                        HttpResponseStatus oResponse = oHttp.Request();
                        if (oResponse.StatusCode == HttpStatusCode.OK)
                        {
                            MP_GetWsdlResp obj = new MP_GetWsdlResp(oResponse.ResponseText);
                            if (obj.StatusCode == 0)
                            {
                                stWsdl = obj.Data;
                            }
                            else
                            {
                                _Desc = obj.StatusDescription;
                                return stWsdl;

                            }
                            return stWsdl;
                        }
                        else
                        {
                            return stWsdl;
                        }
                    }
                    else
                    {
                        return stWsdl;
                    }
                }
                else
                {
                    return stWsdl;
                }
            }
            catch
            {
                return "";
            }
        }

        protected string GetWsdlFileMannual()
        {
            try
            {
                string strWsdlData = "";
                WsdlFileContent objContent = new WsdlFileContent(false, hdfWsdlUploadId.Value, "", "wsdl", this.SubAdminid, this.CompanyId);
                objContent.Process();
                if (objContent.StatusCode == 0)
                {
                    strWsdlData = Convert.ToString(objContent.ResultTable.Rows[0]["FILE_CONTENT"]);
                    WSDL objWSDL = new WSDL(strWsdlData);
                }
                else { }
                return strWsdlData;

            }
            catch
            {
                return "";
            }
        }

        protected string GetWsdlFileOnline(Boolean _AddCredentials, string _UserId, string _Password)
        {
            try
            {
                HTTP oHttp = new HTTP(txtWsConnectorUrl.Text + "?wsdl");
                if (_AddCredentials) oHttp.SetHttpCredentials(_UserId, _Password);
                else { }
                oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                HttpResponseStatus oResponse = oHttp.Request();

                WSDL objWSDL = new WSDL(oResponse.ResponseText);
                return oResponse.ResponseText;
            }
            catch 
            {
                return "";
            }
        }

        #endregion


        protected void DeleteWsConnector(string _SubAdminId, string _ConnectionId, UpdatePanel _Upd, string _CompanyId)
        {
            DeleteConnection objDeleteConnection = new DeleteConnection(_SubAdminId, _ConnectionId, false, _CompanyId);
            objDeleteConnection.Process();
            if (objDeleteConnection.StatusCode == 0)
            {
                if (ddlWsUseMplugin.SelectedValue != "-1")
                    RefreshConnector(this.CompanyId, ddlWsUseMplugin.SelectedValue, ddlWsUseMplugin.SelectedItem.Text);
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aCFmessage').html('Web service source deleted successfully.');SubProcConfirmBoxMessage(true,'  Deleted',250);SubProcImageDelConfirnmation(false);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html('Data cannot be deleted.Internal server error.'); SubProcBoxMessage(true);SubProcImageDelConfirnmation(false);", true);
            }
        }

        private void wsclear()
        {
            hidconnames.Value = "";
            txtWsConnectionName.Text = "";
            ddlWebSrcConnectorType.SelectedValue = "-1";
            txtWsConnectorUrl.Text = "";
            ddlWsUseMplugin.SelectedValue = "-1";
            lblwsconname.Text = "";
            hdfWsUseMplugin.Value = "";
            hdfWsConnId.Value = "";
            chkhttp.Checked = false;
            hideditdone.Value = "";
            drphttpauthenticationtype.SelectedValue = "0";
            drpCredentiail.SelectedIndex = -1;
            hiddiscardcommadtype.Value = "";
            hideditdone.Value = "";
        }

        private void LoadWSData()
        {
            ScriptManager.RegisterStartupScript(updateaddwebservice, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"WsAddNewConnector();", true);
        }


        protected void btnWs_DeleteClick(object sender, EventArgs e)
        {
            if (hidwebdataobj.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcImageNotDelConfirmation(true);$('#aMessage').html(' * Please delete all data objects of this source first');DeletetestClick();WDataDisplay('" + hidconnames.Value + "');", true);
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"WsConnectorDeleteClick();DeletetestClick();WDataDisplay('" + hidconnames.Value + "');", true);
            }
        }

        #endregion

        #region  OData
        
        protected void DrpCredential(string data, DropDownList ddr)
        {

            try
            {
                ddr.Items.Clear();
              
                if (data != string.Empty)
                {
                    List<UseCredential> lstAllAps = Utilities.DeserialiseJson<List<UseCredential>>(data);
                    DataTable dtCre = new DataTable();
                    dtCre.Columns.Add("Typeval", typeof(int));
                    dtCre.Columns.Add("Creadential", typeof(string));
                    dtCre.Columns.Add("CreadentialId", typeof(string));
                    dtCre.Columns.Add("UserId", typeof(string));
                    dtCre.Columns.Add("Password", typeof(string));
                    foreach (var item in lstAllAps)
                    {
                        dtCre.Rows.Add(item.typeval, item.name, item.tag, item.uid, item.pwd);
                    }
                    string str = Utilities.ConvertdatetabletoString(dtCre);
                    hidallcredential.Value = str;
                    hidwscreatedtial.Value = str;
                    hidodatacredential.Value = str;
                    ddr.DataSource = dtCre;
                    ddr.DataTextField = "Creadential";
                    ddr.DataValueField = "CreadentialId";
                    ddr.DataBind();
                    ddr.Items.Insert(0, new ListItem("Select Credential", "-1"));
                }
                else
                {
                    ddr.Items.Insert(0, new ListItem("Select Credential", "-1"));
                }
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ConfirmationScript", @"SubProcBoxConnectedMessageqqq(true);$('#aMessage').html('" + "* " + "Internal Server Error Invalid Credential " + "');", true);
            }
        }

        private void bindMpluginAgentDropdown()
        {
            mPluginAgents objAgent = new mPluginAgents();
            DataSet ds = objAgent.GetMpluginAgents(this.CompanyId);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlUseMplugin.DataSource = ds.Tables[0];
                    ddlUseMplugin.DataTextField = "MP_AGENT_NAME";
                    ddlUseMplugin.DataValueField = "MP_AGENT_NAME";
                    ddlUseMplugin.DataBind();
                    updDdlMplugin.Update();
                    ddlOdataMpluginConn.DataSource = ds.Tables[0];
                    ddlOdataMpluginConn.DataTextField = "MP_AGENT_NAME";
                    ddlOdataMpluginConn.DataValueField = "MP_AGENT_NAME";
                    ddlOdataMpluginConn.DataBind();
                    ddlWsUseMplugin.DataSource = ds.Tables[0];
                    ddlWsUseMplugin.DataTextField = "MP_AGENT_NAME";
                    ddlWsUseMplugin.DataValueField = "MP_AGENT_NAME";
                    ddlWsUseMplugin.DataBind();
                }
            }
            ddlUseMplugin.Items.Insert(0, new ListItem("None", "-1"));
            ddlOdataMpluginConn.Items.Insert(0, new ListItem("None", "-1"));
            ddlWsUseMplugin.Items.Insert(0, new ListItem("None", "-1"));
        }



        private void RefreshOdataTable()
        {
            ScriptManager.RegisterStartupScript(updateodataright, typeof(UpdatePanel), Guid.NewGuid().ToString(), "AddNewODataConn();AfterAddOdata();", true);
        }


        protected void btnSaveODataConn_Click(object sender, EventArgs e)
        {
            string username, password = "";
            string strOutScript = "";
            username = hidodatacredentialusername.Value;
            password = hidodatacredentialpassword.Value;
            StringBuilder sbMessage = new StringBuilder();
            IdeODataHelper oDataHelper = new IdeODataHelper();
            sbMessage.Append((txtOdataConnName.Text.Trim().Length == 0) ? " * Please enter odata source name.</br>" : "");
            sbMessage.Append((txtOdataService.Text.Trim().Length == 0) ? " * Please enter service endpoint.</br>" : "");
            sbMessage.Append((txtOdataConnName.Text.Trim().Length > 50) ? " * OData service source name can not be more than 50 characters.</br>" : "");
            if (chkODataHttp.Checked && drpOCredentiail.SelectedValue == "-1")
            {
                sbMessage.Append(" * Please select credential.</br>");
            }
            string strScript = "CancelButtonhide();";
          
            if (hdfODataConnId.Value.Trim().Length > 0)
            {
                lblOdataConnName.Text = txtOdataConnName.Text;
              
                strScript = "CancelButtonshow();ShowODataConnEdit();";
            }
            else
            {
            }
            if (sbMessage.ToString().Length != 0)
            {
                ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + sbMessage.ToString() + "');chkODataHttpOnChange(this);" + strScript, true);
                return;
            }
            else
            {
                bool _IsBool = false;
                if (chkODataHttp.Checked == true && username == "" && password == "" && hdfDelId.Value != "")
                {
                    ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);ShowODataConnEdit();chkODataHttpOnChange(this);", true);
                    return;
                }
                else if (chkODataHttp.Checked == true && username == "" && password == "")
                {
                    ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);chkODataHttpOnChange(this);", true);
                    return;
                }
                else if (chkODataHttp.Checked == true && password == "" && hdfDelId.Value != "")
                {
                    ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);chkODataHttpOnChange(this);", true);
                    return;
                }
                else if (chkODataHttp.Checked == true && password == "")
                {
                    ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);chkODataHttpOnChange(this);", true);
                    return;
                }
                
                else
                {
                    if (hdfODataConnId.Value.Trim().Length > 0)
                    {
                        _IsBool = oDataHelper.SaveOdataConnector(this.CompanyId.ToUpper(), this.SubAdminid, hdfODataConnId.Value, txtOdataConnName.Text.Trim(),
                         chkODataHttp.Checked, username, password, drpOdataAutenticationtype.SelectedValue, txtOdataService.Text,
                         ddlOdataMpluginConn.SelectedValue, drpOCredentiail.SelectedValue, out strOutScript);
                    }

                    else
                    {
                        _IsBool = oDataHelper.SaveOdataConnector(this.CompanyId.ToUpper(), this.SubAdminid, hdfODataConnId.Value, txtOdataConnName.Text.Trim(),
                         chkODataHttp.Checked, username, password, drpOdataAutenticationtype.SelectedValue, txtOdataService.Text,
                         ddlOdataMpluginConn.SelectedValue, drpOCredentiail.SelectedValue, out strOutScript);

                    }
                    if (_IsBool)
                    {
                        RefreshConnector(this.CompanyId, ddlOdataMpluginConn.SelectedItem.Value, ddlOdataMpluginConn.SelectedItem.Text);
                        updateodataright.Update();
                        OdataTable();
                        oDataClear();

                        ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript + "chkODataHttpOnChange(this);AfterAddOdata();clearconId();AfterAddClick();intializeOdataSource();RemoveEdit();", true);
                    }
                    else
                    {
                        if (hdfDelId.Value != "")
                        {
                            ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript + "chkODataHttpOnChange(this);", true);
                        }
                        ScriptManager.RegisterStartupScript(updateodataright, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript, true);
                    }
                  
                }
            }
        }

        protected void btnSaveODataConn1_Click(object sender, EventArgs e)
        {
            string strScript = "ODataCredentialBy(false);";
            string strOutScript = "";
            bool _IsBool = false;
            string strdrpOcre = drpOCredentiail.SelectedValue;
            IdeODataHelper oDataHelper = new IdeODataHelper();
            if (txtodatapredefinedusername.Text.Trim().Length > 0 && txtodatapredefinedpass.Text.Trim().Length > 0)
            {
                _IsBool = oDataHelper.SaveOdataConnector(this.CompanyId.ToUpper(), this.SubAdminid, hdfODataConnId.Value, txtOdataConnName.Text.Trim(),
                                 chkODataHttp.Checked, txtodatapredefinedusername.Text, txtodatapredefinedpass.Text, drpOdataAutenticationtype.SelectedValue, txtOdataService.Text,
                                 ddlOdataMpluginConn.SelectedValue, drpOCredentiail.SelectedValue,  out strOutScript);

                if (_IsBool)
                {
                    RefreshConnector(this.CompanyId, ddlOdataMpluginConn.SelectedItem.Value, ddlOdataMpluginConn.SelectedItem.Text);
                    updateodataright.Update();
                    OdataTable();
                    oDataClear();
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript + "chkODataHttpOnChange(this);AfterAddOdata();RemoveEdit();", true);
                }
                else
                {
                    if (hidviewconnector.Value != "")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "$('[id$=txtodatapredefinedusername]').val('');ShowODataConnEdit();SubModifiction(false);", true);
                    }
                    
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript + "chkODataHttpOnChange(this);", true);
                }

            }

        }

        private void DiscardSaveodata()
        {
           
            string username, password = "";
            string strOutScript = "";
            hidodatasavediscard.Value = "";
            username = hidodatacredentialusername.Value;
            password = hidodatacredentialpassword.Value;
            StringBuilder sbMessage = new StringBuilder();
            IdeODataHelper oDataHelper = new IdeODataHelper();
            sbMessage.Append((txtOdataConnName.Text.Trim().Length == 0) ? " * Please enter odata source name.</br>" : "");
            sbMessage.Append((txtOdataService.Text.Trim().Length == 0) ? " * Please enter service endpoint.</br>" : "");
            sbMessage.Append((txtOdataConnName.Text.Trim().Length > 50) ? " * OData service source name can not be more than 50 characters.</br>" : "");
            if (chkODataHttp.Checked && drpOCredentiail.SelectedValue == "-1")
            {
                sbMessage.Append(" * Please select credential.</br>");
            }
            string strScript = "CancelButtonhide();";
         
            if (hdfODataConnId.Value.Trim().Length > 0)
            {
                lblOdataConnName.Text = txtOdataConnName.Text;
                strScript = "CancelButtonshow();ShowODataConnEdit();";
            }
            if (sbMessage.ToString().Length != 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"SubProcBoxMessage(true);$('#aMessage').html('" + sbMessage.ToString() + "');chkODataHttpOnChange(this);" + strScript, true);
                return;
            }
            else
            {
                bool _IsBool = false;
                if (chkODataHttp.Checked == true && username == "" && password == "" && hdfDelId.Value != "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);ShowODataConnEdit();chkODataHttpOnChange(this);", true);
                    return;
                }
                else if (chkODataHttp.Checked == true && username == "" && password == "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);chkODataHttpOnChange(this);", true);
                    return;
                }
                else if (chkODataHttp.Checked == true && password == "" && hdfDelId.Value != "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);chkODataHttpOnChange(this);", true);
                    return;
                }
                else if (chkODataHttp.Checked == true && password == "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "ODataCredentialBy(true);chkODataHttpOnChange(this);", true);
                    return;
                }

                else
                {
                    if (hdfODataConnId.Value.Trim().Length > 0)
                    {
                        _IsBool = oDataHelper.SaveOdataConnector(this.CompanyId.ToUpper(), this.SubAdminid, hdfODataConnId.Value, txtOdataConnName.Text.Trim(),
                         chkODataHttp.Checked, username, password, drpOdataAutenticationtype.SelectedValue, txtOdataService.Text,
                         ddlOdataMpluginConn.SelectedValue, drpOCredentiail.SelectedValue, out strOutScript);
                    }
                    if (_IsBool)
                    {
                        RefreshConnector(this.CompanyId, ddlOdataMpluginConn.SelectedItem.Value, ddlOdataMpluginConn.SelectedItem.Text);
                        updateodataright.Update();
                        OdataTable();
                        oDataClear();
                        strOutScript = "";
                    }
                    else
                    {
                        if (hdfDelId.Value != "")
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript + "chkODataHttpOnChange(this);", true);
                        }
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), strOutScript + strScript + "SubModifiction(false);", true);
                    }
                    

                }
            }
          
        }



        protected void Odata_cancel(object sender, EventArgs e)
        {
            updateodataright.Update();
            oDataClear();
            RefreshOdataTable();
        }

        private void oDataClear()
        {
            txtOdataConnName.Text = "";
            hdfODataConnId.Value = "";
            txtOdataService.Text = "";
            lblOdataConnName.Text = "";
            ddlOdataMpluginConn.SelectedValue = "-1";
            drpOdataAutenticationtype.SelectedValue = "0";
            chkODataHttp.Checked = false;
            drpOCredentiail.SelectedValue = "-1";
            hidodatasavediscard.Value = "1";
        }

        protected void btnOdata_DeleteClick(object sender, EventArgs e)
        {
            if (hidOdataobjdts.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubProcImageNotDelConfirmation(true);$('#aMessage').html(' * Please delete all data objects of this source first');DataDisplay('" + hidviewconnector.Value + "');Cancelupdate();", true);
               // oDataClear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ODataConnectorDeleteClick(); ODeleteClick();DataDisplay('" + hidviewconnector.Value + "');", true);
            }
        }




        # endregion
      
        #region  All Sources Add Button Click Defined
        protected void lnkAddodata_Click(object sender, EventArgs e)
        {
            if (hideditdone.Value != "")
            {
                ScriptManager.RegisterStartupScript(upaddbtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), "SubModifiction(true);", true);
                return;
            }
            else
            {
                oDataClear();
                updateodataright.Update();
                txtodatapredefinedpass.Text = "";
                txtodatapredefinedusername.Text = "";
                updatecredentialwithoutsave.Update();
                OdataTable();
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "intializeOdataSource();AfterAddOdata();AddNewODataConn();BindOdata();RemoveEdit();", true);
            }
        }
        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            if (hideditdone.Value != "")
            {
                ScriptManager.RegisterStartupScript(upaddbtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), "SubModifiction(true);", true);
                return;
            }
            else
            {
                updateadddbconnector.Update();
                updatedbright.Update();
                hidupdateconid.Value = "";
                lbldbconnector.Text = "";
                updViewDbConnDetails.Update();
                ddlDbConnectorDbType.SelectedValue = "-1";
                ddlUseMplugin.SelectedValue = "-1";
                hideditdone.Value = "";
                ScriptManager.RegisterStartupScript(upaddbtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), "initializeDbcontrol();bindConnector();RemoveshowConndivOnEdit();", true);
            
            }
        }

        protected void WsAddNewConnector_Click(object sender, EventArgs e)
        {
            if (hideditdone.Value != "")
            {
                ScriptManager.RegisterStartupScript(upaddbtn, typeof(UpdatePanel), Guid.NewGuid().ToString(), "SubModifiction(true);$('[id$=btnwsreset]').hide();$('[id$=btnHtmWsConnectorBack]').show();", true);
                return;
            }
            else
            {
                wsclear();
                getWSSources();
                updateaddwebservice.Update();
                drphttpauthenticationtype.SelectedValue = "0";
                drpCredentiail.SelectedIndex = -1;
                ScriptManager.RegisterStartupScript(upadd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "InitializeWsConnectors();BindWsConnectors();resetclictadd();WsAddNewConnector();", true);
            }
        }



        #endregion

        #region Credential All Page Comman Class

        public class UseCredential
        {
            public string name { get; set; }
            public string tag { get; set; }
            public string uid { get; set; }
            public string pwd { get; set; }
            public int typeval { get; set; }
        }

        public class ReplaceUserCredentialForall
        {
            public string uid { get; set; }
            public string pwd { get; set; }
            public string type { get; set; }
            public int typeval { get; set; }
            public string name { get; set; }
            public string tag { get; set; }
            public string life { get; set; }
            public int lifeval { get; set; }
        }
        #endregion

    }

}












