﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/IdeMaster.Master" AutoEventWireup="true"
    CodeBehind="ideOfflineData.aspx.cs" Inherits="mFicientCP.ideOfflineData" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="css/Jquerydatatablerowclick.css" />
    <script type="text/javascript" src="../Scripts/mfIdeOfflineDatatables.js" />
    <script type="text/javascript">
    </script>
    <script type="text/javascript">
        var postBackByHtmlProcess = {
            "SaveNewOfflineDt": "1",
            "UpdateOfflineDt": "2",
            "DeleteOfflineDt": "3",
            "SaveAndOpenNewTbl": "4",
            "SaveAndOpenSameTbl": "5"
        }
        function bindOfflineDtRpt() {
            var $hidOfflineDtList = $(':hidden[id$="hidOfflineDtList"]');
            var strOflnDatatableJson = $hidOfflineDtList.val();
            var aryOflnDatatables = [];
            if (strOflnDatatableJson) {
                aryOflnDatatables = $.parseJSON(strOflnDatatableJson);
            }

            var table = $('#tblOfflineDts').DataTable();
            if (table) {
                table.destroy();
            }
            $('#tblOfflineDts tbody').html('');
            table = $('#tblOfflineDts').DataTable({
                dom: 'T<"clear">lfrtip',
                tableTools: {
                    "aButtons": []
                },
                data: aryOflnDatatables,
                lengthMenu: [[15, 20, 25, 30, 45, 55, -1], [15, 20, 25, 30, 45, 55, "All"]],
                columns: [
                    { data: 'TableName' },
                    { data: 'TableType' },
                //{ data: 'TableSyncOn'}//celldata which is null
                ],
                "columnDefs": [{
                    "targets": "OfflineDtType",
                    "createdCell": function (td, cellData, rowData, row, col) {
                        var strOflDtType = mfOflnTblHelpers.getOflnTblTypeTextByValue(cellData.toString());
                        $(td).html(strOflDtType);
                    }
                }
                ],
                "createdRow": function (row, data, index) {
                }
            });
            searchText("tblOfflineDts");
            $('#tblOfflineDts tbody').off('click', 'tr');
            $('#tblOfflineDts tbody').on('click', 'tr', function () {
                var trData = table.row(this).data();
                var strFormDisplayMode = MFODB.globalData.getFormDisplayPageMode();
                if (strFormDisplayMode) {
                    if (strFormDisplayMode === OFFLINE_FORM_DISPLAY_MODE.Edit) {
                        MFODB.editDiscardConfirmationProcs.globalData.setData({
                            oldTableId: MFODB.globalData.getTableId(),
                            newTableId: trData.TableId,
                            newTableData: trData
                        });
                        showModalPopUp('divOflnTblEditDiscardCont', "Please Confirm", "300", false, null, DialogType.Warning, { minHeight: "300px" });
                        highlightDatatableRowByOflnTblId(trData.TableId);
                    }
                    else {
                        MFODB.processEditOfflineTable(trData);
                        highlightDatatableRowByOflnTblId(trData.TableId);
                    }
                }
                else {
                    MFODB.processEditOfflineTable(trData);
                    highlightDatatableRowByOflnTblId(trData.TableId);
                }
            });
            $('#divMainContainer').find('div.OfflineDT_Tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        }
        function highlightDatatableRowByOflnTblId(oflnTblId) {
            var oTable = $('#tblOfflineDts').DataTable(),
                row = [];
            if (oTable && oflnTblId) {
                var oTT = TableTools.fnGetInstance('tblOfflineDts');
                oTable.rows().indexes().each(
                     function (idx) {
                         oTT.fnDeselect(oTable.rows(idx).nodes().to$());
                     }
                );
                oTable.rows().indexes().each(
                     function (idx) {
                         if (oTable.row(idx).data().TableId === oflnTblId) {
                             oTT.fnSelect(oTable.rows(idx).nodes().to$());
                         }
                     }
                );
                var paginationInfo = oTable.page.info();
                var selectedNode = oTable.row(".selected").node();
                var nodePosition = oTable.rows({ order: 'current' }).nodes().indexOf(selectedNode);
                var pageNumber = Math.floor(nodePosition / paginationInfo.length);
                oTable.page(pageNumber).draw(false); //move to page with the element
            }
        }
        function removeHighlightOfAllRows(oflnTblId) {
            var oTable = $('#tblOfflineDts').DataTable(),
                row = [];
            if (oTable && oflnTblId) {
                var oTT = TableTools.fnGetInstance('tblOfflineDts');
                oTable.rows().indexes().each(
                     function (idx) {
                         oTT.fnDeselect(oTable.rows(idx).nodes().to$());
                     }
                );
                oTable.page(0).draw(false); //move to page with the element
            }
        }
    </script>
    <style type="text/css">
        .chkLabel, .checker + label
        {
            position: relative;
            top: 4px;
        }
        .OfflineFieldSet .lblForDropdown
        {
            position : relative;
            top:10px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div id="divMainContainer" class="g12">
        <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
            <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                <div>
                    <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Offline Datatables</h1>">
                    </asp:Label>
                </div>
                <div style="position: relative; top: 15px; right: 15px;">
                    <%--<asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="Add" CssClass="repeaterLink fr"
                        OnClientClick="return MFODB.processAddNewTable();" />--%>
                    <a id="lnkAddNewOfflineTable" class="repeaterLink fr">Add</a>
                </div>
            </asp:Panel>
            <div id="divOfflineDatatablesContainer" class="g4">
                <div id="divOfflineDatatables" class="g12" style="max-height:500px;overflow-y:auto;overflow-x:hidden;">
                    <table id="tblOfflineDts" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th class="OfflineDtType">
                                    Type
                                </th>
                                <%--<th class="OfflineDtSyncOn">
                                    Sync On
                                </th>--%>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="divOflnDtAddEditContForm" class="g8 hide">
                <div id="divOfflineTabCont" class="divOfflinedatables">
                    <div id="divOfllineDatatableTab" class="OfflineDT_Tab" style="margin: auto; max-height: 450px;
                        overflow: auto;">
                        <ul id="tabList">
                            <li id="liTabTable"><a id="lnkTabTable" href="#TabTableContent">Tables</a></li>
                            <li id="liTabColumns"><a id="lnkTabColumns" href="#TabColumnsContent">Columns</a></li>
                            <li id="liTabSync"><a id="lnkTabSync" href="#TabSyncContent">Sync</a></li>
                        </ul>
                        <div id="TabTableContent" class="tabContent">
                            <div style="margin-top: 10px; margin-bottom: 10px;">
                                <div class="OfflineFieldSet g12" style="padding: 0px;">
                                    <div class="g12">
                                        <h6 style="font-weight: bold">
                                            Table Settings</h6>
                                        <div id="divTblName" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                                <asp:Label ID="lblForOfflineTblName" runat="server" Text="Name"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="lblTblNmSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <asp:TextBox ID="txtOfflineTblName" runat="server"></asp:TextBox>
                                                <asp:Label ID="lblOfflineTblName" Style="position: relative; top: 4px;" runat="server"
                                                    Text="Label" CssClass="hide"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divOfflineType" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p lblForDropdown">
                                                <asp:Label ID="lblOfflineTblType" runat="server" Text="Type"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn lblForDropdown">
                                                <asp:Label ID="lblOfflineTblTypeSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn">
                                                <asp:DropDownList ID="ddlOfflineTblType" runat="server">
                                                    <asp:ListItem Text="Upload Data" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Download Data" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divOfflineSyncOn" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                                <asp:Label ID="lblSyncOn" runat="server" Text="Sync On"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="lblSyncOnSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn">
                                                <div class="fl mrgr10">
                                                    <input id="chkSyncLogin" type="checkbox" value="1" />
                                                    <label for="chkSyncLogin" class="chkLabel">
                                                        On Login</label>
                                                </div>
                                                <div class="fl mrgr10">
                                                    <input id="chkSyncAppStartup" type="checkbox" value="2" />
                                                    <label for="chkSyncAppStartup" class="chkLabel">
                                                        On App Startup</label></div>
                                                <div class="fl mrgr10">
                                                    <input id="chkSyncAppClose" type="checkbox" value="3" />
                                                    <label for="chkSyncAppClose" class="chkLabel">
                                                        On App Close</label></div>
                                                <div class="fl mrgr10">
                                                    <input id="chkSyncManual" type="checkbox" value="4" />
                                                    <label for="chkSyncManual" class="chkLabel">
                                                        Manual</label></div>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divUserConfirmation" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                            </div>
                                            <div class="formMiddleClmn">
                                            </div>
                                            <div class="formRightColumn" style="margin-left: 120px">
                                                <asp:CheckBox ID="chkOfflineUserConfirmation" Text="Sync requires user confirmation"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divUploadingSeqAndSizeInfoCont">
                                            <div id="divUploadSeq" class="singleColumn fieldsCont">
                                                <div class="formLeftColumn w_105p lblForDropdown">
                                                    <asp:Label ID="lblUploadSeq" runat="server" Text="Upload Sequence"></asp:Label>
                                                </div>
                                                <div class="formMiddleClmn lblForDropdown">
                                                    <asp:Label ID="lblUploadSeqSeparator" runat="server" Text=":"></asp:Label>
                                                </div>
                                                <div class="formRightColumn">
                                                    <asp:DropDownList ID="ddlUploadSeq" runat="server">
                                                        <asp:ListItem Text="Last In First Out" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="First In First Out" Value="1"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div id="divBlockSize" class="singleColumn fieldsCont">
                                                <div class="formLeftColumn w_105p">
                                                    <asp:Label ID="lblBlockSize" runat="server" Text="Transfer block size"></asp:Label>
                                                </div>
                                                <div class="formMiddleClmn">
                                                    <asp:Label ID="lblBlockSizeSeparator" runat="server" Text=":"></asp:Label>
                                                </div>
                                                <div class="formRightColumn withInput">
                                                    <asp:TextBox ID="txtBlockSize" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div id="TabColumnsContent" class="tabContent">
                            <div class="OfflineFieldSet g12" style="padding: 0px;">
                                <div class="g12">
                                    <h6 style="font-weight: bold">
                                        Columns</h6>
                                    <div style="margin-top: 10px; max-height: 300px; overflow: auto">
                                        <table id="OfflineDTClmnDefinition" class="detailsTable">
                                            <thead>
                                                <tr>
                                                    <%--<th style="width: 20px;">
                                                        Primary
                                                    </th>--%>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Data Type
                                                    </th>
                                                    <th>
                                                        Default Value
                                                    </th>
                                                    <%--<th>
                                                        Min Length/Value
                                                    </th>
                                                    <th>
                                                        Max Length/Value
                                                    </th>--%>
                                                    <th>
                                                        Decimal places
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="dynamicRow">
                                                    <%--<td style="width: 20px;">
                                                        <input type="checkbox" name="Primary" class="Primary notUniform" />
                                                    </td>--%>
                                                    <td>
                                                        <input type="text" name="ColumnNm" style="width: 90%;"/>
                                                    </td>
                                                    <td>
                                                        <select id="DataType" name="DataType" onchange="MFODB.setDecimalPlacesInputAndDefaultVal(this);"
                                                            class="notUniform">
                                                            <option value="0">String</option>
                                                            <option value="1">Number</option>
                                                            <option value="2">Decimal</option>
                                                            <option value="3">File</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="DefaultVal" style="width: 60%;" />
                                                    </td>
                                                    <%--<td>
                                                        <input type="text" name="MinValue" style="width: 80%" />
                                                    </td>
                                                    <td>
                                                        <input type="text" name="MaxValue" style="width: 80%" />
                                                    </td>--%>
                                                    <td>
                                                        <select name="DecimalPlaces" class="notUniform">
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                        </select>
                                                        <span id="spanNotApplicable">NA</span>
                                                    </td>
                                                    <td>
                                                        <span class="addImg16x16" id="spanAddColumnToTable"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="p10p">
                                        * If you remove an existing column and add a new column with same name, data in existing offline table on mobile device may be lost.
                                    </div>
                                </div>
                                <div style="height: 0px;">
                                    <input type="hidden" name="hidColumnNamesAdded" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div id="TabSyncContent" class="tabContent">
                            <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 10px;">
                                <div class="OfflineFieldSet g12" style="padding: 0px;">
                                    <div class="g12">
                                        <h6 style="font-weight: bold">
                                            Sync</h6>
                                        <div id="divSyncCommandType" class="singleColumn fieldsCont" style="padding-left: 15px;">
                                            <div class="formLeftColumn lblForDropdown">
                                                <asp:Label ID="lblSyncCommandType" runat="server" Text="Source Type"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn lblForDropdown">
                                                <asp:Label ID="lblSyncCmdTypeSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <select id="ddlSyncCommandType">
                                                    <option value="-1">Select</option>
                                                    <option value="1">Database</option>
                                                    <option value="2">Webservice(WSDL)</option>
                                                    <option value="3">Webservice(HTTP)</option>
                                                    <option value="4">Webservice(XML-RPC)</option>
                                                    <option value="5">OData</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divSyncOnlineDB" class="singleColumn fieldsCont" style="padding-left: 15px;">
                                            <div class="formLeftColumn lblForDropdown">
                                                <asp:Label ID="lblSyncOnlineDBConn" runat="server" Text="Data Source"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn lblForDropdown">
                                                <asp:Label ID="lblSyncOnlineDBSep" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <select id="ddlSyncOnlineConn">
                                                    <option value="-1">Select</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divInitialDownloadCont" class="hide" style="margin-left: 11px;">
                                            <input id="chkInitialDownload" type="checkbox" />
                                            <label for="chkInitialDownload">
                                                Initial Download</label>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div class="OfflineFieldSet g12" id="divOfflineSyncDwnLdFieldsCont" style="padding: 0px;">
                                            <div class="g12">
                                                <h6 style="font-weight: bold">
                                                    Download</h6>
                                                <div id="divSyncOnlnOfflnCommand" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn lblForDropdown">
                                                        <asp:Label ID="lblSyncOnlnOfflnCommand" runat="server" Text="Object"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn lblForDropdown">
                                                        <asp:Label ID="lblSyncOnlnOfflnCommandSep" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <asp:DropDownList ID="ddlSyncOnlnOfflnCommand" runat="server">
                                                            <asp:ListItem Value="-1">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divSyncOnlnOfflnData" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn">
                                                        <asp:Label ID="lblSyncOnlnOfflnDataInput" runat="server" Text="Input"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn">
                                                        <asp:Label ID="lblSyncOnlnOfflnInpotSep" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <span class="fl i_pencil" title="edit" id="spanEditOnlnOfflnInput"></span>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divSyncOnlnOfflnOutput" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn">
                                                        <asp:Label ID="lblSyncOnlnOfflnOutput" runat="server" Text="Output"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn lblForDropdown">
                                                        <asp:Label ID="lblSyncOnlnOfflnOutputSep" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <span class="fl i_pencil" title="edit" id="spanEditOnlnOfflnOutput"></span>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divDataCache">
                                                    <div id="divDataCacheType" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn lblForDropdown">
                                                            <span>Data Cache</span>
                                                        </div>
                                                        <div class="formMiddleClmn lblForDropdown">
                                                            <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn">
                                                            <asp:DropDownList ID="ddl_DataCache_type" runat="server" onchange="ddl_DataCache_type_Changed(this);">
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes (Absolute Time)</asp:ListItem>
                                                                <asp:ListItem Value="2">Yes (Relative Time)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="divAbsoluteTime" class="hide w_100 fieldsCont">
                                                        <div id="divAbsoluteCachePeriod" class="DbCmdRow">
                                                            <div class="formLeftColumn lblForDropdown">
                                                                <span>Cache Frequency</span>
                                                            </div>
                                                            <div class="formMiddleClmn lblForDropdown">
                                                                <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                                                            </div>
                                                            <div class="formRightColumn withInput">
                                                                <div style="float: left;">
                                                                    <div id="divAbsoluteMinutes" class="hide">
                                                                        <asp:DropDownList ID="ddl_AbsoluteMinutes" runat="server" class="UseUniformCss">
                                                                            <asp:ListItem Value="0">0</asp:ListItem>
                                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                                            <asp:ListItem Value="15">15</asp:ListItem>
                                                                            <asp:ListItem Value="30">30</asp:ListItem>
                                                                            <asp:ListItem Value="45">45</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divAbsoluteDay" class="hide">
                                                                        <div style="float: left;">
                                                                            <asp:DropDownList ID="ddl_AbsoluteDayHour" runat="server" class="UseUniformCss">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div style="float: left; margin-top: 10px;">
                                                                            :</div>
                                                                        <div style="float: left;">
                                                                            <asp:DropDownList ID="ddl_AbsoluteDayMin" runat="server" class="UseUniformCss">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div id="divAbsoluteWeek" class="hide">
                                                                        <asp:DropDownList ID="ddl_AbsoluteWeek" runat="server" class="UseUniformCss">
                                                                            <asp:ListItem Value="1">MON</asp:ListItem>
                                                                            <asp:ListItem Value="2">TUE</asp:ListItem>
                                                                            <asp:ListItem Value="3">WED</asp:ListItem>
                                                                            <asp:ListItem Value="4">THU</asp:ListItem>
                                                                            <asp:ListItem Value="5">FRI</asp:ListItem>
                                                                            <asp:ListItem Value="6">SAT</asp:ListItem>
                                                                            <asp:ListItem Value="7">SUN</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divAbsoluteDate" class="hide">
                                                                        <asp:DropDownList ID="ddl_AbsoluteDate" runat="server" class="UseUniformCss">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divAbsoluteYear" class="hide">
                                                                        <div style="float: left;">
                                                                            <asp:DropDownList ID="ddl_AbsoluteYear_Day" runat="server" class="UseUniformCss">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                        <div style="float: left; margin-top: 10px;">
                                                                            :</div>
                                                                        <div style="float: left;">
                                                                            <asp:DropDownList ID="ddl_AbsoluteYear_Month" runat="server" class="UseUniformCss">
                                                                                <asp:ListItem Value="1">January</asp:ListItem>
                                                                                <asp:ListItem Value="2">February</asp:ListItem>
                                                                                <asp:ListItem Value="3">March</asp:ListItem>
                                                                                <asp:ListItem Value="4">April</asp:ListItem>
                                                                                <asp:ListItem Value="5">May</asp:ListItem>
                                                                                <asp:ListItem Value="6">June</asp:ListItem>
                                                                                <asp:ListItem Value="7">July</asp:ListItem>
                                                                                <asp:ListItem Value="8">August</asp:ListItem>
                                                                                <asp:ListItem Value="9">September</asp:ListItem>
                                                                                <asp:ListItem Value="10">October</asp:ListItem>
                                                                                <asp:ListItem Value="11">November</asp:ListItem>
                                                                                <asp:ListItem Value="12">December</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:DropDownList ID="ddl_AbsoluteCachePeriod" runat="server" class="UseUniformCss"
                                                                        onchange="ddl_AbsoluteCachePeriod_Changed(this);">
                                                                        <asp:ListItem Value="0">Hour</asp:ListItem>
                                                                        <asp:ListItem Value="1">Day(Hr./Min.)</asp:ListItem>
                                                                        <asp:ListItem Value="2">Week</asp:ListItem>
                                                                        <asp:ListItem Value="3">Month</asp:ListItem>
                                                                        <asp:ListItem Value="4">Year(Day/Mnth)</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divRelativeTime" class="hide w_100 fieldsCont">
                                                        <div id="divRelativeCachePeriod" class="DbCmdRow">
                                                            <div class="formLeftColumn lblForDropdown">
                                                                <span>Cache Frequency</span>
                                                            </div>
                                                            <div class="formMiddleClmn lblForDropdown">
                                                                <asp:Label ID="Label3" runat="server" Text=":"></asp:Label>
                                                            </div>
                                                            <div class="formRightColumn">
                                                                <div style="float: left;">
                                                                    <div id="divRelativeMinute" class="hide">
                                                                        <asp:DropDownList ID="ddl_RelativeMinute" runat="server" class="UseUniformCss">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divRelativeHour" class="hide">
                                                                        <asp:DropDownList ID="ddl_RelativeHour" runat="server" class="UseUniformCss">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divRelativeDay" class="hide">
                                                                        <asp:DropDownList ID="ddl_RelativeDay" runat="server" class="UseUniformCss">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divRelativeWeek" class="hide">
                                                                        <asp:DropDownList ID="ddl_RelativeWeek" runat="server" class="UseUniformCss">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divRelativeMonth" class="hide">
                                                                        <asp:DropDownList ID="ddl_RelativeMonth" runat="server" class="UseUniformCss">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:DropDownList ID="ddl_RelativeCachePeriod" runat="server" class="UseUniformCss"
                                                                        onchange="ddl_RelativeCachePeriod_Changed(this);">
                                                                        <asp:ListItem Value="0">Minute</asp:ListItem>
                                                                        <asp:ListItem Value="1">Hour</asp:ListItem>
                                                                        <asp:ListItem Value="2">Day</asp:ListItem>
                                                                        <asp:ListItem Value="3">Week</asp:ListItem>
                                                                        <asp:ListItem Value="4">Month</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divDisallowDataAfterExpiryCont" class="singleColumn" style="margin-left: 110px;">
                                                    <input id="chkDisallowDataAfterExpiry" type="checkbox" />
                                                    <label for="chkDisallowDataAfterExpiry">
                                                        Disallow Data After Expiry</label>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divCleanRecBeforeDownloadCont" class="singleColumn" style="margin-left: 110px;">
                                                    <input id="chkCleanRecBeforeDownload" type="checkbox" />
                                                    <label for="chkCleanRecBeforeDownload">
                                                        Clean Record Before Download</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div class="g12 OfflineFieldSet" id="divOfflineSyncUpLdFieldsCont" style="padding: 0px;">
                                            <div class="OfflineFieldSet g12">
                                                <div class="g12">
                                                    <h6 style="font-weight: bold">
                                                        Upload New Record</h6>
                                                    <div id="divOfflnOnlnCommandCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn lblForDropdown">
                                                            <asp:Label ID="lblOfflnOnlnCommand" runat="server" Text="Object"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn lblForDropdown">
                                                            <asp:Label ID="lblOfflnOnlnCommandSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <asp:DropDownList ID="ddlSyncOfflnOnlnCommand" runat="server">
                                                                <asp:ListItem Value="-1">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div id="divOfflnOnlnInputCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn">
                                                            <asp:Label ID="lblOfflnOnlnInput" runat="server" Text="Input"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblOfflnOnlnInputSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <span class="fl i_pencil" title="edit" id="spanOfflnOnlnInput"></span>
                                                        </div>
                                                    </div>
                                                    <div id="divUploadDeleteRecCont" class="singleColumn fl" style="margin-left: 120px;">
                                                        <input id="chkUploadDeleteRec" type="checkbox" />
                                                        <label for="chkUploadDeleteRec">
                                                            Do not delete records after upload</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="OfflineFieldSet g12 hide" id="divUploadModifiedRecCont">
                                                <div class="g12">
                                                    <div class="fl">
                                                        <h6 style="font-weight: bold">
                                                            Upload Modified Record</h6>
                                                    </div>
                                                    <div id="divModifiedRecSameAsNewRecCont" class="fl mrgl20">
                                                        <input id="chkModifiedRecSameAsNewRecord" type="checkbox" />
                                                        <label for="chkModifiedRecSameAsNewRecord">
                                                            Object same as new record</label>
                                                    </div>
                                                    <div class="clrBoth">
                                                    </div>
                                                    <div id="divModifiedRecUploadCmdSettingsCont" class="hide">
                                                        <div id="divModifiedRecUploadCommandCont" class="singleColumn fieldsCont">
                                                            <div class="formLeftColumn lblForDropdown">
                                                                <asp:Label ID="lblModifiedRecUploadCommand" runat="server" Text="Object"></asp:Label>
                                                            </div>
                                                            <div class="formMiddleClmn lblForDropdown">
                                                                <asp:Label ID="lblModifiedRecUploadCommandSep" runat="server" Text=":"></asp:Label>
                                                            </div>
                                                            <div class="formRightColumn withInput">
                                                                <select id="ddlModifiedRecUploadCommand">
                                                                    <option value="-1">Select</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="divModifiedRecUploadInputCont" class="singleColumn fieldsCont">
                                                            <div class="formLeftColumn">
                                                                <asp:Label ID="lblModifiedRecUploadInput" runat="server" Text="Input"></asp:Label>
                                                            </div>
                                                            <div class="formMiddleClmn">
                                                                <asp:Label ID="lblModifiedRecUploadInputSep" runat="server" Text=":"></asp:Label>
                                                            </div>
                                                            <div class="formRightColumn withInput">
                                                                <span class="fl i_pencil" title="edit" id="spanModifiedRecUploadInputParam"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="OfflineFieldSet g12">
                                                <div class="g12">
                                                    <div id="divUploadCriteriaCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn w_125p">
                                                            <asp:Label ID="lblUploadRecordCriteria" runat="server" Text="Upload record criteria"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblUploadRecCriteriaSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <asp:TextBox ID="txtUploadRecordCriteria" runat="server" style="width:90%;"/>
                                                        </div>
                                                    </div>
                                                    <div id="divShouldUploadWithinCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn w_125p">
                                                            <asp:Label ID="lblUploadShouldBeWithin" runat="server" Text="Should Upload within"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblUploadWithinSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                           <asp:TextBox ID="txtShouldUploadWithin" runat="server" />
                                                            <span style="position:relative;top:-1px;margin-left:4px;">days</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div id="divOfflineTblActionButtons" style="text-align: right; margin-right: 20px;
                        margin-bottom: 10px; margin-top: 30px;">
                        <input id="btnSaveOfflineTblSettings1" type="button" value=" Save " class="InputStyle" />
                        <input id="btnDeleteOfflineTblSettings" type="button" value=" Delete " class="InputStyle" />
                        <input id="btnCancelOfflineTblSettings" type="button" value=" Cancel " class="InputStyle" />
                        <input id="btnEditCancelOfflineTblSettings" type="button" value=" Cancel " class="InputStyle" />
                    </div>
                </div>
                <div id="divEditOfflineTabCont" class="hide divOfflinedatables">
                    <div id="divEditOfllineDatatableTab" class="OfflineDT_Tab" style="margin: auto; max-height: 500px;
                        overflow: auto;">
                        <ul id="EdittabList">
                            <li id="liTabTableEdit"><a id="lnkTabTableEdit" href="#TabTableContentEdit">Tables</a></li>
                            <li id="liTabColumnsEdit"><a id="lnkTabColumnsEdit" href="#TabColumnsContentEdit">Columns</a></li>
                            <li id="liTabSyncEdit"><a id="lnkTabSyncEdit" href="#TabSyncContentEdit">Sync</a></li>
                        </ul>
                        <div style="float: right; margin-right: 10px; position: relative; top: -25px;">
                            <a id="lnkEditViewEdit">edit</a>&nbsp;|&nbsp; <a id="lnkOfflineEditViewDelete">delete</a>&nbsp;|&nbsp;
                            <a id="lnkEditViewBack">cancel</a>
                        </div>
                        <div id="TabTableContentEdit" class="tabContent">
                            <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 30px;">
                                <div class="OfflineFieldSet g12" style="padding: 0px;">
                                    <div class="g12">
                                        <h6 style="font-weight: bold">
                                            Table Settings</h6>
                                        <div id="divTblNameEdit" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                                <asp:Label ID="lblForEditOfflineTblName" runat="server" Text="Name"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="lblEditTblNmSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <asp:Label ID="lblEditOfflineTblName" Style="position: relative; top: 4px;" runat="server"
                                                    Text="Label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divOfflineTypeEdit" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                                <asp:Label ID="lblForEditOfflineTblType" runat="server" Text="Type"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="lblEditOfflineTblTypeSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <asp:Label ID="lblEditOfflineTblType" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divOfflineSyncOnEdit" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                                <asp:Label ID="lblForEditSyncOn" runat="server" Text="Sync On"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="lblEditSyncOnSeparator" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <asp:Label ID="lblEditSyncOn" runat="server"></asp:Label>
                                                <div class="fl mrgr10">
                                                    <input id="chkEditSyncLogin" type="checkbox" value="1" disabled="disabled" />
                                                    <label>
                                                        On Login</label>
                                                </div>
                                                <div class="fl mrgr10">
                                                    <input id="chkEditSyncAppStartup" type="checkbox" value="2" disabled="disabled" />
                                                    <label>
                                                        On App Startup</label></div>
                                                <div class="fl mrgr10">
                                                    <input id="chkEditSyncAppClose" type="checkbox" value="3" disabled="disabled" />
                                                    <label>
                                                        On App Close</label></div>
                                                <div class="fl mrgr10">
                                                    <input id="chkEditSyncManual" type="checkbox" value="4" disabled="disabled" />
                                                    <label>
                                                        Manual</label></div>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divUserConfirmationEdit" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn w_105p">
                                            </div>
                                            <div class="formMiddleClmn">
                                            </div>
                                            <div class="formRightColumn" style="margin-left: 120px">
                                                <asp:CheckBox ID="chkEditOfflineUserConfirmation" Text="Sync requires user confirmation"
                                                    runat="server" Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divEditUploadingSeqAndSizeInfoCont">
                                            <div id="divUploadSeqEdit" class="singleColumn fieldsCont">
                                                <div class="formLeftColumn w_105p">
                                                    <asp:Label ID="lblForEditUploadSeq" runat="server" Text="Upload Sequence"></asp:Label>
                                                </div>
                                                <div class="formMiddleClmn">
                                                    <asp:Label ID="lblEditUploadSeqSeparator" runat="server" Text=":"></asp:Label>
                                                </div>
                                                <div class="formRightColumn withInput">
                                                    <asp:Label ID="lblEditUploadSeq" runat="server" Text="Upload Sequence"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="divBlockSizeEdit" class="singleColumn fieldsCont">
                                                <div class="formLeftColumn w_105p">
                                                    <asp:Label ID="lblForEditBlockSize" runat="server" Text="Transfer Block Size"></asp:Label>
                                                </div>
                                                <div class="formMiddleClmn">
                                                    <asp:Label ID="lblEditBlockSizeSeparator" runat="server" Text=":"></asp:Label>
                                                </div>
                                                <div class="formRightColumn withInput">
                                                    <asp:Label ID="lblEditBlockSize" runat="server" Text="Block Size"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <div id="TabColumnsContentEdit" class="tabContent">
                            <div class="OfflineFieldSet g12" style="padding: 0px;">
                                <div class="g12">
                                    <h6 style="font-weight: bold">
                                        Columns</h6>
                                    <div style="margin-top: 10px; max-height: 300px; overflow: auto">
                                        <table id="OfflineDTClmnDefinitionEdit" class="detailsTable w_100">
                                            <thead>
                                                <tr>
                                                    <%--<th>
                                                        Primary
                                                    </th>--%>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Data Type
                                                    </th>
                                                    <th>
                                                        Default Value
                                                    </th>
                                                    <%--<th>
                                                        Min Length/Value
                                                    </th>
                                                    <th>
                                                        Max Length/Value
                                                    </th>--%>
                                                    <th>
                                                        Decimal places
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="dynamicEditRow" style="display: none">
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div style="height: 0px;">
                                    <%--<input type="hidden" name="hidEditColumnNamesAdded">--%>
                                </div>
                            </div>
                            <div style="clear: both;">
                            </div>
                        </div>
                        <div id="TabSyncContentEdit" class="tabContent">
                            <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 30px;">
                                <div class="OfflineFieldSet g12" style="padding: 0px;">
                                    <div class="g12">
                                        <h6 style="font-weight: bold">
                                            Sync</h6>
                                        <div id="div75" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn">
                                                <asp:Label ID="lblForSyncCommandType" runat="server" Text="Source Type"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="Label5" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <asp:Label ID="lblEditSyncCommandType" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divEditSyncOnlineDB" class="singleColumn fieldsCont">
                                            <div class="formLeftColumn">
                                                <asp:Label ID="lblForEditSyncOnlineDBConn" runat="server" Text="Data Source"></asp:Label>
                                            </div>
                                            <div class="formMiddleClmn">
                                                <asp:Label ID="lblEditSyncOnlineDBSep" runat="server" Text=":"></asp:Label>
                                            </div>
                                            <div class="formRightColumn withInput">
                                                <asp:Label ID="lblEditSyncOnlineDBConn" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div id="divEditInitialDownloadCont" class="hide">
                                            <input id="chkEditInitialDownload" type="checkbox" disabled="disabled" />
                                            <label for="chkEditInitialDownload">
                                                Initial Download</label>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div class="OfflineFieldSet g12" id="divEditOfflineSyncDwnLdFieldsCont" style="padding: 0px;">
                                            <div class="g12">
                                                <h6 style="font-weight: bold">
                                                    Download</h6>
                                                <div id="divEditSyncOnlnOfflnCommand" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn">
                                                        <asp:Label ID="lblForEditSyncOnlnOfflnCommand" runat="server" Text="Object"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn">
                                                        <asp:Label ID="Label7" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <asp:Label ID="lblEditSyncOnlnOfflnCommand" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divEditSyncOnlnOfflnData" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn">
                                                        <asp:Label ID="lblForEditSyncOnlnOfflnDataInput" runat="server" Text="Input"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn">
                                                        <asp:Label ID="lblEditSyncOnlnOfflnInpotSep" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <span class="fl i_search" title="view" id="spanEditViewDownloadInput"></span>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divEditSyncOnlnOfflnOutput" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn">
                                                        <asp:Label ID="lblForEditSyncOnlnOfflnOutput" runat="server" Text="Output"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn">
                                                        <asp:Label ID="lblEditSyncOnlnOfflnOutputSep" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <span class="fl i_search" title="view" id="spanEditViewDownloadOutput"></span>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divEditSyncDownloadCacheCont" class="singleColumn fieldsCont">
                                                    <div class="formLeftColumn">
                                                        <asp:Label ID="lblForEditSyncDownloadCache" runat="server" Text="Data Cache"></asp:Label>
                                                    </div>
                                                    <div class="formMiddleClmn">
                                                        <asp:Label ID="lblEditSyncDownloadCacheSep" runat="server" Text=":"></asp:Label>
                                                    </div>
                                                    <div class="formRightColumn withInput">
                                                        <asp:Label ID="lblEditSyncDownloadCache" runat="server" Text="None"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divEditDisallowDataAfterExpiryCont" class="singleColumn" style="margin-left: 110px;">
                                                    <input id="chkEditDisallowDataAfterExpiry" type="checkbox" disabled="disabled" />
                                                    <label for="chkEditDisallowDataAfterExpiry">
                                                        Disallow data after expiry</label>
                                                </div>
                                                <div class="clrBoth">
                                                </div>
                                                <div id="divEditCleanRecBeforeDownloadCont" class="singleColumn" style="margin-left: 110px;">
                                                    <input id="chkEditCleanRecBeforeDownload" type="checkbox" disabled="disabled" />
                                                    <label for="chkEditCleanRecBeforeDownload">
                                                        Clean record before download</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clrBoth">
                                        </div>
                                        <div class="w_100 fl" id="divEditOfflineSyncUpLdFieldsCont" style="padding: 0px;">
                                            <div class="OfflineFieldSet g12">
                                                <div class="g12">
                                                    <h6 style="font-weight: bold">
                                                        Upload New Record</h6>
                                                    <div id="divEditOfflnOnlnCommandCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn">
                                                            <asp:Label ID="lblForEditOfflnOnlnCommand" runat="server" Text="Object"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblEditOfflnOnlnCommandSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <asp:Label ID="lblEditOfflnOnlnCommand" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="divEditOfflnOnlnInputCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn">
                                                            <asp:Label ID="lblForEditOfflnOnlnInput" runat="server" Text="Input"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblEditOfflnOnlnInputSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <span class="fl i_search" title="view" id="spanEditUploadInput"></span>
                                                        </div>
                                                    </div>
                                                    <div id="divEditUploadDeleteRecCont" class="singleColumn" style="margin-left: 110px;">
                                                        <input id="chkEditUploadDeleteRec" type="checkbox" disabled="disabled" />
                                                        <label for="chkEditUploadDeleteRec">
                                                            Do not delete records after upload</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="OfflineFieldSet g12 hide" id="divEditViewModifiedRecCont">
                                                <div class="g12">
                                                    <div class="fl">
                                                        <h6 style="font-weight: bold">
                                                            Upload Modified Record</h6>
                                                    </div>
                                                    <div id="divEditModifiedRecSameAsUploadCont" class="fl mrgl20">
                                                        <input id="chkEditModifiedRecSameAsUpload" type="checkbox" disabled="disabled" />
                                                        <label for="chkEditModifiedRecSameAsUpload">
                                                            Object same as new record</label>
                                                    </div>
                                                    <div class="clrBoth">
                                                    </div>
                                                    <div id="divEditModifiedRecUploadCmdSettingsCont" class="hide">
                                                        <div id="divEditModifiedRecUploadCommandCont" class="singleColumn fieldsCont">
                                                            <div class="formLeftColumn">
                                                                <asp:Label ID="lblForEditModifiedRecUploadCommand" runat="server" Text="Object"></asp:Label>
                                                            </div>
                                                            <div class="formMiddleClmn">
                                                                <asp:Label ID="lblEditModifiedRecUploadCommandSep" runat="server" Text=":"></asp:Label>
                                                            </div>
                                                            <div class="formRightColumn withInput">
                                                                <asp:Label ID="lblEditModifiedRecUploadCommand" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="divEditModifiedRecUploadInputCont" class="singleColumn fieldsCont">
                                                            <div class="formLeftColumn">
                                                                <asp:Label ID="lblEditModifiedRecUploadInput" runat="server" Text="Input"></asp:Label>
                                                            </div>
                                                            <div class="formMiddleClmn">
                                                                <asp:Label ID="lblEditModifiedRecUploadInputSep" runat="server" Text=":"></asp:Label>
                                                            </div>
                                                            <div class="formRightColumn withInput">
                                                                <span class="fl i_search" title="edit" id="spanEditModifiedRecUploadInputParam">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="OfflineFieldSet g12">
                                                <div class="g12">
                                                    <div id="divEditUploadRecordCriteriaCont" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn w_125p">
                                                            <asp:Label ID="lblForEditUploadRecordCriteria" runat="server" Text="Upload Record Criteria"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblEditUploadRecordCriteriaSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <asp:Label ID="lblEditUploadRecordCriteria" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="div1" class="singleColumn fieldsCont">
                                                        <div class="formLeftColumn w_125p">
                                                            <asp:Label ID="lblForEditShouldBeWithin" runat="server" Text="Should Upload Within"></asp:Label>
                                                        </div>
                                                        <div class="formMiddleClmn">
                                                            <asp:Label ID="lblEditShouldBeWithinSep" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="formRightColumn withInput">
                                                            <asp:Label ID="lblEditUploadShouldBeWithin" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div id="divOfflineDownloadCmdInParamCont" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblOfflineDownloadCmdInParam" class="popupTable" style="width: 100%; border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Input
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divDwnldInParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnDwnldInParamSave" type="button" value="  Save  " class="InputStyle" />
                <input id="btnDwnldInParamCancel" type="button" value="  Cancel  " onclick="closeModalPopUp('divOfflineDownloadCmdInParamCont');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divOfflineDownloadCmdOutput" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblOfflineDownloadCmdOutput" class="popupTable" style="width: 100%; border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Outputs
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divDwnldOutParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnDwnldOutParamSave" type="button" value="  Save  " class="InputStyle" />
                <input id="btnDwnldOutParamCancel" type="button" value="  Cancel  " onclick="closeModalPopUp('divOfflineDownloadCmdOutput');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divOfflineUploadCmdInpParamCont" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblOfflineUploadCmdInpParam" class="popupTable" style="width: 100%; border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Inputs
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divuploadInParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnUpldInParamSave" type="button" value="  Save  " class="InputStyle" />
                <input id="btnUpldInParamCancel" type="button" value="  Cancel  " onclick="closeModalPopUp('divOfflineUploadCmdInpParamCont');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divOflnModifiedRecInsertCmdInpParamCont" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblOflnModifiedRecInsertCmdInpParam" class="popupTable" style="width: 100%;
                border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Inputs
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divUploadModifiedRecInParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnUpldModifiedRecInParamSave" type="button" value="  Save  " class="InputStyle" />
                <input id="btnUpldModifiedRecInParamCancel" type="button" value="  Cancel  " class="InputStyle"
                    onclick="closeModalPopUp('divOflnModifiedRecInsertCmdInpParamCont');" />
            </div>
        </div>
    </div>
    <div id="divEditOfflineSelectCmdInParamCont" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblEditOfflineSelectCmdInParam" class="popupTable" style="width: 100%;
                border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Input
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divEditDwnldInParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnDwnldInParamOK" type="button" value="  OK  " onclick="closeModalPopUp('divEditOfflineSelectCmdInParamCont');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divEditOfflineSelectCmdOutput" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblEditOfflineSelectCmdOutput" class="popupTable" style="width: 100%;
                border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Outputs
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divEditDwnldOutParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnDwnldOutParamOK" type="button" value="  OK  " onclick="closeModalPopUp('divEditOfflineSelectCmdOutput');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divEditOfflineInsertCmdInpParamCont" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblEditOfflineInsertCmdInpParam" class="popupTable" style="width: 100%;
                border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Inputs
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divEdituploadInParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnUpldInParamOK" type="button" value="  OK  " onclick="closeModalPopUp('divEditOfflineInsertCmdInpParamCont');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divEditOflnModifiedRecInsertCmdInpParamCont" class="hide">
        <div style="margin-top: 10px;overflow:auto;max-height:300px;">
            <table id="tblEditOflnModifiedRecInsertCmdInpParam" class="popupTable" style="width: 100%;
                border-collapse: collapse">
                <thead>
                    <tr>
                        <th>
                            Inputs
                        </th>
                        <th>
                            Map
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div id="divEditUploadModifiedRecInParamActionBtns" class="SubProcborderDiv">
            <div class="SubProcBtnMrgn" align="center">
                <input id="btnEditViewModifiedUploadParamOK" type="button" value="  OK  " onclick="closeModalPopUp('divEditOflnModifiedRecInsertCmdInpParamCont');"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divOflnTblEditDiscardCont" class="hide">
        <div>
            <div class="MessageDiv">
                <a class="DefaultCursor" style="margin-left: 20px;">Discard any changes made ?</a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnOflnTblEditDiscardYes" class="InputStyle" type="button" value=" Yes " />
                    <input id="btnOflnTblEditDiscardNo" type="button" value=" No " class="InputStyle" />
                    <input id="btnOflnTblEditDiscardClose" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divHiddenFields" style="display: none">
        <asp:UpdatePanel ID="updHiddenFields" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hidOfflineDtList" runat="server" />
                <asp:HiddenField ID="hidOfflineInOutParamBySelectCommand" runat="server" />
                <asp:HiddenField ID="hidOfflineInOutParamByInsertCommand" runat="server" />
                <asp:HiddenField ID="hidOfflineCompleteTableJsonForCmd" runat="server" />
                <asp:HiddenField ID="hidSavedCommandIds" runat="server" />
                <asp:HiddenField ID="hidSavedOutputParamJson" runat="server" />
                <asp:HiddenField ID="hidClntColumnDetJson" runat="server" />
                <asp:HiddenField ID="hidClntOfflnCompleteTableJsonForCmd" runat="server" />
                <asp:HiddenField ID="hidTabIndexSelected" runat="server" />
                <asp:HiddenField ID="hidOfflinePageViewMode" runat="server" />
                <asp:HiddenField ID="hidOfflineFormViewMode" runat="server" />
                <asp:HiddenField ID="hidDwnldOutputParamJson" runat="server" />
                <asp:HiddenField ID="hidOfflineSyncDtlsForUI" runat="server" />
                <asp:HiddenField ID="hidOfflineSyncCmdAndConJsonForSaving" runat="server" />
                <asp:HiddenField ID="hidAllDbCommands" runat="server" Value="" />
                <asp:HiddenField ID="hidAllWsCommands" runat="server" Value="" />
                <asp:HiddenField ID="hidAllOdataCommands" runat="server" Value="" />
                <asp:HiddenField ID="hidAllDbConnections" runat="server" Value="" />
                <asp:HiddenField ID="hidAllWsConnections" runat="server" Value="" />
                <asp:HiddenField ID="hidAllOdataConnections" runat="server" Value="" />
                <asp:HiddenField ID="hidDtlsForPostBackByHtmlCntrl" runat="server" />
                <asp:HiddenField ID="hidFinalTableJsonForSaving" runat="server" />
                <asp:HiddenField ID="hidFinalSyncJsonForSavning" runat="server" />
                <asp:HiddenField ID="hidTblDataToOpenAfterSaveAndOpenNew" runat="server" />
                <asp:HiddenField ID="hidOldNewTblDtlForSaveAndOpenNew" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- End Hidden Fields -->
    <!-- POST BACK BUTTON FOR HTML CONTROLS-->
    <div class="hide">
        <asp:UpdatePanel ID="updPostbackByHtmlCntrl" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnPostbackByHtmlCntrl" runat="server" Text="" OnClick="btnPostbackByHtmlCntrl_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--End POST BACK BUTTON FOR HTML CONTROLS -->
    <!--WAIT BOX =-->
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <!-- END WAIT BOX -->
    <asp:HiddenField ID="hfs" runat="server" />
    <asp:HiddenField ID="hfbid" runat="server" />
    <script type="text/javascript">
        Sys.Application.add_init(application_init);

        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }

        $('#divOflnDtAddEditContForm').show();
    </script>
</asp:Content>
