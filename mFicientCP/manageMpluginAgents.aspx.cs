﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Net;
namespace mFicientCP
{
    public partial class manageMpluginAgents : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        private mPluginAgents manageAgents = new mPluginAgents();

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                bindMpluginAgentsRepeater(null);

                if (hfsValue.Length > 0)
                {
                    if (manageAgents.AllowAgentsToAdd(hfsPart3))
                        divAddNewAgent.Visible = true;
                    else
                        divAddNewAgent.Visible = false;
                }
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    bindMpluginAgentsRepeater(null);

                    if (hfsValue.Length > 0)
                    {
                        if (manageAgents.AllowAgentsToAdd(hfsPart3))
                            divAddNewAgent.Visible = true;
                        else
                            divAddNewAgent.Visible = false;
                    }
                }
            }
        }

        #region Control events

        protected void rptMpluginAgentsDtls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblAgentId = (Label)e.Item.FindControl("lblAgentId");
                Label lblAgentName = (Label)e.Item.FindControl("lblAgentName");
                Label lblAgentPassword = (Label)e.Item.FindControl("lblAgentPassword");
                hidmPluginAgentIdForEdit.Value = lblAgentId.Text;
                switch (e.CommandName)
                {
                    case "Edit":
                        string strMajorVersion = "", strMinorVersion = "";
                        hidPopUpMode.Value = "Edit";
                        txtAgentPass.Focus();
                        DisplayVersion(lblAgentName.Text,out strMajorVersion,out strMinorVersion);
                        lblEditAgentName.Text = lblAgentName.Text;
                        string strvertion = "&nbsp;&nbsp;&nbsp;&nbsp;" + strMinorVersion + "&nbsp;" + "-" + "&nbsp;" + strMajorVersion;
                       
                        if (strMajorVersion != string.Empty && strMinorVersion !=string.Empty)
                        {
                            lblversion.Text = ":" + strvertion;
                            Utilities.showModalPopup("divModalContainer",
                                   updRepeater, "<img src=\"css/images/online.png\" style=\"position:relative;top:2px\"></img>" + "&nbsp;<span style=\"position:relative;top:-2px\">" + lblAgentName.Text + "</span>",
                                   "280", false);
                            showHidePanelsForProcessUsingJS(false, updRepeater, false);
                        }
                        else
                        {
                            lblversion.Text =  ":" +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"  + "Not Connected";
                            Utilities.showModalPopup("divModalContainer",
                                  updRepeater, "<img src=\"css/images/offline.png\" style=\"position:relative;top:2px\"></img>" + "&nbsp;<span style=\"position:relative;top:-2px\">" + lblAgentName.Text + "</span>",
                                  "280", false);
                            showHidePanelsForProcessUsingJS(false, updRepeater, false);
                        }
                        break;
                    case "Delete":
                        try
                        {
                            removeMpluginAgent(hidmPluginAgentIdForEdit.Value, lblAgentName.Text);
                            bindMpluginAgentsRepeater(updModalContainer);
                        }
                        catch
                        {
                            Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);
                        }
                        break;
                }
            }
        }
      
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string validationError = "";
            if (hidPopUpMode.Value == "")
            {
                validationError = validateAgent();
                if (validationError != "")
                {
                    Utilities.showAlert(validationError, "divMPluginContError", updModalContainer);
                    showHidePanelsForProcessUsingJS(false, updModalContainer, true);
                    return;
                }
                addMpluginAgent();
            }
            else
            {
                validationError = validateNewPassword();
                if (validationError != "")
                {
                    Utilities.showAlert(validationError, "divMPluginContError", updModalContainer);
                    showHidePanelsForProcessUsingJS(false, updModalContainer, false);
                    return;
                }
                updateMpluginAgent(hidmPluginAgentIdForEdit.Value);
            }

            if (manageAgents.AllowAgentsToAdd(hfsPart3))
                divAddNewAgent.Visible = true;
            else
                divAddNewAgent.Visible = false;
        }



        

        protected void btnConfirmYes_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divPostBackConfirm", updConfirmation, "confirmBox", "divPostBackConfirm");
            updConfirmation.Update();
        }

        protected void btnConfirmNo_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divPostBackConfirm", updConfirmation, "confirmBox", "divPostBackConfirm");
            updConfirmation.Update();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Private Methods

        private void addMpluginAgent()
        {
            try
            {
                manageAgents.AddMpluginAgent(hfsPart3, txtAgentName.Text, Utilities.GetMd5Hash(txtAgentPass.Text));
            }
            catch
            {
            }
            if (manageAgents.StatusCode == 0)
            {
                Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.MPLUGIN_CREATE, hfsPart3, hfsPart4,manageAgents.mPluginAgentId, txtAgentName.Text,  "", "", "", "", "", "");
                
                bindMpluginAgentsRepeater(updModalContainer);
                closeModalPopUp();
                Utilities.showMessage("mPlugin Agent added successfully", updModalContainer, "Successfull Addition", DIALOG_TYPE.Info);
                clearControlsUsedInEdit();
                closeModalPopUp();
            }
            else
            {
                Utilities.showAlert(manageAgents.StatusDescription, "modalPopUpContent", updModalContainer);
                showHidePanelsForProcess(true);
            }
        }

        private void removeMpluginAgent(string mPluginAgentId,string name)
        {
            try
            {
                manageAgents.DeleteMpluginAgent(mPluginAgentId, hfsPart3);
            }
            catch
            {
            }
            if (manageAgents.StatusCode == 0)
            {
                bindMpluginAgentsRepeater(updModalContainer);
                Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.MPLUGIN_DELETE, hfsPart3, hfsPart4, mPluginAgentId, name, "", "", "", "", "", "");
                closeModalPopUp();
                Utilities.showMessage("mPlugin Agent deleted successfully", updModalContainer, "Delete Successfull", DIALOG_TYPE.Info);
            }
            else
            {
                // Utilities.showAlert(manageAgents.StatusDescription, "modalPopUpContent", updModalContainer);
                Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);

            }
        }

        private void updateMpluginAgent(string mPluginAgentId)
        {
            try
            {
                manageAgents.UpdateMpluginAgent(mPluginAgentId, Utilities.GetMd5Hash(txtAgentPassword.Text), hfsPart3);
            }
            catch
            {
            }
            if (manageAgents.StatusCode == 0)
            {
                string errorDescription;
                processMPluginNotification(hfsPart3, mPluginAgentId, out errorDescription);
                bindMpluginAgentsRepeater(updModalContainer);
                Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.MPLUGIN_CHANGE, hfsPart3, hfsPart4, mPluginAgentId, lblEditAgentName.Text, "", "", "", "", "", "");
                clearControlsUsedInEdit();
                closeModalPopUp();
                Utilities.showMessage("Password Changed successfully", updModalContainer, "Successfull Update", DIALOG_TYPE.Info);
            }
            else
            {
                Utilities.showAlert("Internal server error", "divMPluginContError", updModalContainer, "Alert");
                showHidePanelsForProcess(false);
            }
        }
        void processMPluginNotification(string _CompanyId, string agentId, out string _ErrorDescription)
        {
            _ErrorDescription = "";
            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
            objServerUrl.Process();
            string strUrl = "";
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    strUrl = strUrl + "/MPCredentialChanged.aspx?d=" + Utilities.UrlEncode(getMPlugInCallRequest(_CompanyId, agentId));
                }
            }
            strUrl = "http://localhost:50995/MPCredentialChanged.aspx?d=" + Utilities.UrlEncode(getMPlugInCallRequest(_CompanyId,agentId));
            if (strUrl.Length != 0)
            {

                callMPlugin(strUrl, out _ErrorDescription);
            }

        }
        string getMPlugInCallRequest(string companyId, string agentId)
        {
            string strRqst = "";

            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objMPlugInAgents = new mPluginAgents();
            DataSet dsMpluginAgents = objMPlugInAgents.GetMpluginAgents(hfsPart3);
            if (dsMpluginAgents != null && dsMpluginAgents.Tables[0].Rows.Count > 0)
            {
                string filter = String.Format("MP_AGENT_ID= '{0}'", agentId);
                DataRow[] rows = dsMpluginAgents.Tables[0].Select(filter);
                if (rows.Length > 0)
                {
                    strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + companyId + "\",\"agtid\":\"" + rows[0]["MP_AGENT_NAME"] + "\",\"agtpwd\":\"" + Utilities.GetMd5Hash(txtAgentPassword.Text) + "\"" + "}}}";
                }
            }
            return strRqst;
        }

        private void callMPlugin(string _WebServiceURL, out string _ErrorDescription)
        {
            _ErrorDescription = "";
            HTTP oHttp = new HTTP(_WebServiceURL);
            oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
            HttpResponseStatus oResponse = oHttp.Request();
            if (oResponse.StatusCode == HttpStatusCode.OK)
            {
                _ErrorDescription = String.Empty;
            }
            else if (oResponse.StatusCode == HttpStatusCode.NotFound)
            {
                _ErrorDescription = "mPlugin server not found.";
            }
        }
        private void bindMpluginAgentsRepeater(UpdatePanel updPanel)
        {
            try
            {
                DataTable dtMPluginAgents = manageAgents.GetMpluginAgents(hfsPart3).Tables[0];
                if (dtMPluginAgents != null)
                {
                    if (dtMPluginAgents.Rows.Count == 0)
                    {
                        initialSettingWhenNoAgentAdded(updPanel);
                        //used in page load 
                        if (manageAgents.AllowAgentsToAdd(hfsPart3))
                            divAddNewAgent.Visible = true;
                        else
                            divAddNewAgent.Visible = false;

                        rptMpluginAgentsDtls.DataSource = null;
                        rptMpluginAgentsDtls.DataBind();
                        rptMpluginAgentsDtls.Visible = false;
                    }
                    else
                    {
                        rptMpluginAgentsDtls.DataSource = dtMPluginAgents;
                        rptMpluginAgentsDtls.DataBind();
                        rptMpluginAgentsDtls.Visible = true;
                        setRepeaterHeader("Agent Details");
                    }
                }
            }
            catch
            {
            }
        }

        private void clearControlsUsedInEdit()
        {
            txtAgentName.Text = "";
            hidmPluginAgentIdForEdit.Value = "";
            hidPopUpMode.Value = "";
            txtAgentPassword.Text = "";

        }

        private string validateAgent()
        {
            string strMessage = "";
            if (!Utilities.IsValidString(txtAgentName.Text, true, true, true, "", 3, 20, false, false))
            {
                strMessage += "Please enter valid Agent name.Min 3 characters" + "<br />";
            }
            if (!Utilities.IsValidString(txtAgentPass.Text, true, true, true, @"!@#$%^&*()_-", 6, 20, false, false))
            {
                strMessage += "Please enter valid Agent Password." + "<br />";
            }
            if (txtAgentPass.Text != txtRePassword.Text)
            {
                strMessage += "Password Do not match." + "<br />";
            }
            return strMessage;
        }

        private string validateNewPassword()
        {
            string strMessage = "";
            if (!Utilities.IsValidString(txtAgentPassword.Text, true, true, true, @"!@#$%^&*()_-", 6, 20, false, false))
            {
                strMessage += "Please enter valid Agent Password." + "<br />";
            }
            if (txtAgentPassword.Text != txtReAgentPassword.Text)
            {
                strMessage += "Password Do not match." + "<br />";
            }
            return strMessage;
        }

        private void closeModalPopUp()
        {
            clearControlsUsedInEdit();
            Utilities.closeModalPopUp("divModalContainer", updModalContainer);
        }
        private void DisplayVersion( string _AgentName, out string _majorver,out string _minorver)
        {
            string strRqst, strUrl = "";
            string Pass = ""; _majorver = "";
            _minorver = "";
           
            mPluginAgents objAgent = new mPluginAgents();
            Pass = objAgent.GetMpluginAgentPassword(hfsPart3, _AgentName);
            string strRequestId = Utilities.GetMd5Hash(DateTime.UtcNow.Ticks + hfsPart3 + hfsPart4);
            strRqst = "{\"req\":{\"rid\":\"" + strRequestId + "\",\"eid\":\"" + hfsPart3 + "\",\"agtnm\":\"" + _AgentName + "\",\"agtpwd\":\"" + Pass + "\"}}";
            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(hfsPart3);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    strUrl = strUrl + "/GetMpluginAgentVersion.aspx?d=" + Utilities.UrlEncode(strRqst);
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        GetMpluginAgentVersionResp obj = new GetMpluginAgentVersionResp(oResponse.ResponseText);
                        _majorver = obj.Majversion;
                        _minorver = obj.Minversion;

                    }
                    else
                    {
                        _majorver = string.Empty;
                        _minorver = string.Empty;
                    }
                }
                else
                {
                    //  return stWsdl;
                }
            }
            else
            {
                //  return stWsdl;
            }

        }

        void initialSettingWhenNoAgentAdded(UpdatePanel updPanel)
        {
            setRepeaterHeader("No Agent is added yet");
            //if (updPanel == null)
            //    showHideDeleteButtonUsingJS(true, null, true);
            //else
            //    showHideDeleteButtonUsingJS(false, updPanel, true);
        }
        void setRepeaterHeader(string header)
        {
            lblHeaderInfo.Text = "<h1>" + header + "</h1>";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usePageForScript"></param>
        /// <param name="updPanel">Name of the update panel if usePageForScript is false</param>
        /// <param name="isNewAddition"></param>
        //void showHideDeleteButtonUsingJS(bool usePageForScript, UpdatePanel updPanel, bool isNewAddition)
        //{
        //    string strProcess = String.Empty;
        //    if (isNewAddition)
        //    {
        //        strProcess = "add";
        //    }
        //    else
        //    {
        //        strProcess = "edit";
        //    }
        //    if (usePageForScript) Utilities.runPostBackScript("showHideButtonsForProcess('" + strProcess + "')", this.Page, "showHideDeleteButton");
        //    else Utilities.runPostBackScript("showHideButtonsForProcess('" + strProcess + "')", updPanel, "ShowHideDeleteButton");
        //}
        void showHidePanelsForProcessUsingJS(bool usePageForScript, UpdatePanel updPanel, bool isNewAddition)
        {
            string strProcess = String.Empty;
            if (isNewAddition)
            {
                strProcess = "add";
            }
            else
            {
                strProcess = "edit";
            }
            if (usePageForScript) Utilities.runPostBackScript("showHidePanelsForProcess('" + strProcess + "')", this.Page, "showHidePanels");
            else Utilities.runPostBackScript("showHidePanelsForProcess('" + strProcess + "')", updPanel, "ShowHidePanels");
        }
        void showHidePanelsForProcess(bool isAddingNewAgent)
        {
            if (isAddingNewAgent)
            {
                pnlAddNewAgent.Visible = true;
                pnlUpdateAgent.Visible = false;
            }
            else
            {
                pnlUpdateAgent.Visible = true;
                pnlAddNewAgent.Visible = false;
            }
        }
        #endregion
          
        
           
    }
}
