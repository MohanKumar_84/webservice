﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Web.SessionState;
namespace mFicientCP
{
    public partial class home : System.Web.UI.MasterPage
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        //string hfsPart5 = string.Empty;
        //string hfsPart6 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            try
            {
                hfsValue = context.Items["hfs"].ToString();
                hfbidValue = context.Items["hfbid"].ToString();

                if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

                strUserId = context.Items["hfs1"].ToString();
                strSessionId = context.Items["hfs2"].ToString();
                hfsPart3 = context.Items["hfs3"].ToString();
                hfsPart4 = context.Items["hfs4"].ToString();

            }
            catch
            {
                return;
            }

            hfs.Value = hfsValue;
            hfbid.Value = hfbidValue;

            try
            {
                if (!Page.IsPostBack) Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", @"setExpiryDateOfCookie();makeSelectedPageMenuActive();", true);
                else ScriptManager.RegisterStartupScript(Page, typeof(Page), "Alert", @"setExpiryDateOfCookie();makeSelectedPageMenuActive();", true);
            }
            catch 
            {
            }
        }

        protected void lbLogout_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdmin = true;


                string[] hfsParts = Utilities.DecryptString(hfs.Value).Split(',');
                strUserId = hfsParts[0];
                strSessionId = hfsParts[1];
                hfsPart3 = hfsParts[2];
                hfsPart4 = hfsParts[3];
              


                if (hfsPart4.Trim().ToUpper() == strUserId.Trim().ToUpper()) isAdmin = true;
                else isAdmin = false;

                UserLogOut objUserLogOut = null;
                if (isAdmin) objUserLogOut = new UserLogOut(strUserId, strSessionId, hfsPart4, hfsPart3, Cache, LogOutReason.NORMAL);
                else objUserLogOut = new UserLogOut(strUserId, strSessionId, hfsPart3, hfsPart4, Cache, LogOutReason.NORMAL);

                if (objUserLogOut != null) objUserLogOut.Process();


                Utilities.removeCurrentCookie(String.Empty);
                Response.Redirect("~/Default.aspx");
            }
            catch
            {
            }
        }
    }
}