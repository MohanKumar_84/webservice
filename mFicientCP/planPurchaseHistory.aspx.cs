﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class planPurchaseHistory : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                    bindRepeater();
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    bindRepeater();
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, String.Empty);
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }
            if (String.IsNullOrEmpty(hfsValue))
            {
                try
                {
                    if (!String.IsNullOrEmpty(hfsValue))
                        CompanyTimezone.getTimezoneInfo(hfsPart3);
                }
                catch
                { }
            }
        }

        void bindRepeater()
        {
            GetPlanPurchaseHistoryList objPlanPurchaseHistory = new GetPlanPurchaseHistoryList(hfsPart3);
            objPlanPurchaseHistory.Process();
            DataTable dtblPlanPurchaseHistory = objPlanPurchaseHistory.PlanPurchaseHistoryDetails;
            if (objPlanPurchaseHistory.StatusCode == 0 && dtblPlanPurchaseHistory != null && dtblPlanPurchaseHistory.Rows.Count > 0)
            {
                rptPlanPurchaseHistoryDtls.DataSource = dtblPlanPurchaseHistory;
                rptPlanPurchaseHistoryDtls.DataBind();
            }
            else
            {
                lblHeaderInfo.Text = "<h1>No purchase record found</h1>";
            }
        }

        protected void rptPlanPurchaseHistoryDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblPurchaseDate = (Label)e.Item.FindControl("lblPurchaseDate");
                Label lblDate = (Label)e.Item.FindControl("lblDate");

                lblDate.Text =
                    Utilities.getCompanyLocalFormattedDate(
                    CompanyTimezone.CmpTimeZoneInfo,
                    Convert.ToInt64(lblPurchaseDate.Text)
                    );

                string strTransactionType = "";
                switch (
                    (PLAN_RENEW_OR_CHANGE_REQUESTS)
                    Enum.Parse(typeof(PLAN_RENEW_OR_CHANGE_REQUESTS),
                    Convert.ToString(DataBinder.Eval(e.Item.DataItem, "TRANSACTION_TYPE"))
                    )
                    )
                {
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE:
                        strTransactionType = "Plan Downgraded";
                        break;
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE:
                        strTransactionType = "PlanPurchased";
                        break;
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE:
                        strTransactionType = "Plan Upgraded";
                        break;
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW:
                        strTransactionType = "Plan Renewed";
                        break;
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.TRIAL:
                        strTransactionType = "Purchased trial Plan";
                        break;
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED:
                        strTransactionType = "Users Added";
                        break;
                }
                Label lblTransactionType = (Label)e.Item.FindControl("lblTransactionType");
                lblTransactionType.Text = strTransactionType;
            }
        }
    }
}