﻿<%@ Page Title="mFicient | Messaging" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="pushmessaging.aspx.cs" Inherits="mFicientCP.pushmessaging" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">

        var idPrependString = { "spanRegionName": "lblRegion", "spanLocationName": "lblLocation", "anchor": "lnk" };
        var idOfTemplate = { "idSpanRegionName": "@spanRegionNameId", "idSpanLocationName": "@spanLocationNameId", "idAtag": "@atagId", "spanRegionInnerHtml": "@spanRegionInnerHtml", "spanLocationInnerHtml": "@spanLocationInnerHtml" };
        var idOfTemplateForDivision = { "idSpanDivisionName": "@spanDivisionId", "idSpanDeprtName": "@spanDeprtNameId", "idAtag": "@atagId", "spanDivisionInnerHtml": "@spanDivisionInnerHtml", "spanDeprtInnerHtml": "@spanDeprtInnerHtml" };
        var templateForRegionLocation = "<div style=\"float: left; width: auto; margin-bottom: 4px; margin-left: 2px;\"><div style=\"float: left; width: auto;\"><span id=\"@spanRegionNameId\">@spanRegionInnerHtml</span>/<span id=\"@spanLocationNameId\">@spanLocationInnerHtml</span></div><div style=\"float: left;\"><span class=\"remove removeFacebookStyle\" id=\"@atagId\" onclick=\"processRemoveFromDivAndDeselectCheckBox(this);\">×</span></div></div>";
        var templateForDivisionDept = "<div style=\"float: left; width: auto; margin-bottom: 4px; margin-left: 2px;\"><div style=\"float: left; width: auto;\"><span id=\"@spanDivisionId\">@spanDivisionInnerHtml</span>/<span id=\"@spanDeprtNameId\">@spanDeprtInnerHtml</span></div><div style=\"float: left;\"><span class=\"remove removeFacebookStyle\" id=\"@atagId\" onclick=\"processRemoveFromDivAndDeselectCheckBox(this);\">×</span></div></div>";
        var templateForCategoryName = "<div class=\"withAutoSize\"><span>@categoryName</span><span class=\"remove removeFacebookStyle\" id=\"@atagId\" onclick=\"processRemoveCategoryMsg(this);\">×</span></div>";
        var templateForCategoryNameMiscellaneous = "<div class=\"withAutoSize\"><span>@categoryName</span></div>";
        var 
        ProcessFor =
                    {
                        REGION_LOCATION: 0,
                        DIVISION_DEPARTMENT: 1
                    }
        var ScriptPostBackFor = { RemoveMessageCategory: 0 }
        /**
        * Update counter widget with number of characters remaining
        * @param string counter id of counter element
        * @param string text_elem id of textarea or input element
        * @param int    max_len maximum allowed character length
        * @return void
        */
        function update_counter(counter, text_elem, max_len) {
            var counterElem = document.getElementById(counter);
            var textElem = document.getElementById('<%=txtMessage.ClientID %>');
            var len = textElem.value.length;
            //var val = max_len - textElem.value.length;
            if (checkMaxLen(textElem, max_len)) {
                var val = textElem.value.length;
                counterElem.innerHTML = val;
            }
        }

        /**
        * Verify that elem_id doesn't contain more than max_len characters
        * @param string elem_id id of elem to count characters in
        * @param int    max_len maxmimum input string (including spaces and returns)
        * @return boolean
        */
        function check_content_length(elem_id, max_len) {
            var elem = document.getElementById(elem_id);
            if (elem.value.length > max_len) {
                return false;
            }
            return true;
        }

        function checkMaxLen(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1)) {
                    var cont = txt.value;
                    txt.value = cont.substring(0, (maxLen - 1));
                    return false;
                }
                else {
                    return true;
                };
            } catch (e) {
            }
        }

        function changeDatePickerFormat() {
            var datePicker = $('#' + '<%=txtSendDate.ClientID %>');
            $(datePicker).datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: "+2D",
                minDate: "-0D",
                showOn: "button",
                buttonImage: "css/images/icons/dark/calendar.png",
                buttonImageOnly: true
            });
            if (datePicker) {
                preventDefaultOfDate();
            }
        }
        function makeDatePickerWithInitialDate() {
            var datePicker = $('#' + '<%=txtSendDate.ClientID %>');
            $(datePicker).datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: "+2D",
                minDate: "-0D",
                showOn: "button",
                buttonImage: "css/images/icons/dark/calendar.png",
                buttonImageOnly: true
            }).datepicker('setDate', '+1');
            if (datePicker) {
                preventDefaultOfDate();
            }
        }

        function getOffsetTimeOfClient() {
            x = new Date();
            var hidTimeOffset = document.getElementById('<%=hidTimeOffsetOfClient.ClientID %>');
            currentTimeZoneOffsetInMin = x.getTimezoneOffset();
            hidTimeOffset.value = currentTimeZoneOffsetInMin.toString();
        }
        function preventDefaultOfDate() {
            var datePicker = document.getElementById('<%=txtSendDate.ClientID %>');
            stopDefualtOfDateTxtBox(datePicker);
        }

        function validateDateTextField() {
            var dateTextField = document.getElementById('<%=txtSendDate.ClientID %>');
            if (dateTextField.value === "") {
                $.wl_Alert("Please enter a date", "info", "#sheduledTimeError");
                return false;
            }
            else {
                return true;
            }
        }
        function validateFormFields() {
            var errorMessage = "";
            var regionSelectedDiv = $('#divRegionLocationSelectedView');
            var divisionSelectedDiv = $('#divDivDeptSelectedView');
            var autoSelectUsers = $("#" + "<% = txtAutoCompleteUserName.ClientID %>");
            var txtMessage = document.getElementById('<%=txtMessage.ClientID %>');
            var chkSheduleImmediately = $('#' + '<%=chkSheduleImmediate.ClientID %>');
            var txtSendDate = $('#' + '<%=txtSendDate.ClientID %>');
            var anyUserSelected = false;
            if ($(autoSelectUsers).val().trim().length > 0) {
                anyUserSelected = true;
            }
            else {
                anyUserSelected = false;
            }


            if ($(txtMessage).val() === "") {
                errorMessage += "Please enter a message </br>";
            }
            if (!$(chkSheduleImmediately).is(':checked')) {
                if ($(txtSendDate).val() === "") {
                    errorMessage += "Please select a date to send message if not sheduled immediately. </br>";
                }
            }
            if (!(errorMessage === "")) {
                //remove previous alert
                $('#divContainer').children('.alert').remove();
                $.wl_Alert(errorMessage, 'info', '#divContainer');
                return false;
            }
            else {
                return true;
            }
        }


        $(document).ready(function () {
            makeTextBoxAutoComplete();

        });
        function makeTextBoxAutoComplete(prePopulateJsonObject) {
            var companyId = $('#' + '<%=hidCId.ClientID %>').val().toString();
            var hidAutoCmpltAllUserList = $('#' + '<%=hidAutoCmpltAllUserList.ClientID %>');
            if (hidAutoCmpltAllUserList) {
                var lstUserDetail = JSON.parse($(hidAutoCmpltAllUserList).val());
            }
            if (prePopulateJsonObject) {
                if (lstUserDetail) {
                    $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput(lstUserDetail, {
                        theme: "facebook",
                        prePopulate: prePopulateJsonObject,
                        preventDuplicates: true
                    });
                }
                else {
                    try {
                        $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput("GetUserByCompany.ashx?q=" + "&" + "CmpId=" + companyId, {
                            theme: "facebook",
                            prePopulate: prePopulateJsonObject,
                            preventDuplicates: true
                        });
                    } catch (Error) {
                        $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput("GetUserByCompany.ashx?q=" + "&" + "CmpId=" + companyId, {
                            theme: "facebook",
                            preventDuplicates: true
                        });
                    }
                }
            }
            else {
                if (lstUserDetail) {
                    $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput(lstUserDetail, {
                        theme: "facebook",
                        preventDuplicates: true
                    });
                }
                else {
                    $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput("GetUserByCompany.ashx?q=" + "&" + "CmpId=" + companyId, {
                        theme: "facebook",
                        preventDuplicates: true
                    });
                }
            }
        }

        function storeJsonOfUserSelectedForPostback() {
            var hidAutoCmpltUserIdsJson = $('#' + '<%=hidAutoCmpltUserIdsJson.ClientID %>');
            var autoCompleteUserSelected = [];
            var aryUserName = [];
            var aryUserId = $('#' + '<% = txtAutoCompleteUserName.ClientID %>').val().split(',');
            var tokenInput = $('.token-input-list-facebook');
            if (tokenInput) {
                var lstUnorderedUserList = $(tokenInput).children('.token-input-token-facebook');
                if (lstUnorderedUserList.length > 0) {
                    $(lstUnorderedUserList).each(function (index) {
                        aryUserName.push($(this).children('p').text());
                    });
                    if (aryUserId.length > 0) {
                        for (var i = 0; i <= aryUserId.length - 1; i++) {
                            var uid = aryUserId[i];
                            var unm = aryUserName[i];
                            var user = {
                                "id": uid,
                                "name": unm
                            }
                            autoCompleteUserSelected.push(user);
                        }
                    }
                    if (hidAutoCmpltUserIdsJson) {
                        $(hidAutoCmpltUserIdsJson).val((JSON.stringify(autoCompleteUserSelected)));
                    }
                }
            }
        }
        function fillAutoCmpTextOnPostBack() {
            var jsonObject = null;
            var hidAutoCmpltUserIdsJson = $('#' + '<%=hidAutoCmpltUserIdsJson.ClientID %>');
            if (hidAutoCmpltUserIdsJson && $(hidAutoCmpltUserIdsJson).val().length > 0) {
                jsonObject = jQuery.parseJSON($(hidAutoCmpltUserIdsJson).val());
                if (jsonObject !== null) {
                    makeTextBoxAutoComplete(jsonObject);
                }
                else {
                    makeTextBoxAutoComplete();
                }
            }
            else {
                makeTextBoxAutoComplete();
            }
        }
        function chkSendImmediatelyClicked(sender) {
            var divSheduledTimeView = document.getElementById('<%=pnlSheduledHrsMin.ClientID %>');
            var chkSheduledHrsMin = document.getElementById('<%=chkSheduledHrsMin.ClientID %>');
            if ($(sender).is(':checked')) {
                uncheckUniformedCheckBox($(chkSheduledHrsMin));
            }
            else {
                checkUniformedCheckBox($(chkSheduledHrsMin));
            }
        }
        function chkSheduledHrsMinClicked(sender) {
            var divSheduledTimeView = document.getElementById('<%=pnlSheduledHrsMin.ClientID %>');
            var chkSheduledHrsMin = document.getElementById('<%=chkSheduledHrsMin.ClientID %>');
            var chkSheduleImmediate = document.getElementById('<%=chkSheduleImmediate.ClientID %>');
            if (!($(sender).is(':checked'))) {
                checkUniformedCheckBox($(chkSheduleImmediate));
            }
            else {
                uncheckUniformedCheckBox($(chkSheduleImmediate));
            }
        }
        //For Category Add and Delete
        function processRemoveCategoryMsg(sender) {
            var confirmDelete = confirm('Are you sure you want to remove this category');
            if (confirmDelete) {
                storeJsonOfUserSelectedForPostback();
                var hidScriptPostBackFor = $('#' + '<%=hidPostBackFor.ClientID %>');
                $(hidScriptPostBackFor).val(ScriptPostBackFor.RemoveMessageCategory);
                var hidExistingCatToRemove = $('#' + '<%=hidExistingCatToRemove.ClientID %>');
                $(hidExistingCatToRemove).val(($(sender).attr("id")).split("_")[1]);
                clickButtonForPostBack("btnAddCategoryContPostBack");
                return true;
            }
            else {
                return false;
            }
        }
        function getCategoryMsgAryFromCategoryDropDown() {
            var ddlCategory = $('#' + '<%=ddlCategory.ClientID %>');
            var opts = $(ddlCategory)[0].options;
            if (opts) {
                var aryOptionTextAndValue = $.map(opts, function (elem) {
                    return formCategoryTextAndValueObject(elem.text, elem.value);
                });
            }
            return aryOptionTextAndValue;
        }
        function formCategoryTextAndValueObject(text, value) {
            var objTextValue = null;
            if (text && value) {
                objTextValue = {
                    txt: text,
                    id: value
                }
            }
            return objTextValue;
        }
        function addHtmlToExistingCategoriesDiv(categoryDetailJson) {
            if (categoryDetailJson) {
                var aryCategoryDetailObject = JSON.parse(categoryDetailJson);
                if (aryCategoryDetailObject) {
                    if (aryCategoryDetailObject.length > 0) {
                        $('#divExistingCategory').empty();
                        for (var i = 0; i <= aryCategoryDetailObject.length - 1; i++) {
                            var catDetObject = aryCategoryDetailObject[i];
                            if (catDetObject) {
                                var htmlToInsert = '';
                                var finalHtmlToInsert = '';
                                if (catDetObject.id != "-1") {
                                    htmlToInsert = templateForCategoryName;
                                    finalHtmlToInsert = new String(htmlToInsert.replace("@categoryName", catDetObject.txt));
                                    finalHtmlToInsert = finalHtmlToInsert.replace("@atagId", "lnk_" + catDetObject.id);
                                }
                                else {
                                    htmlToInsert = templateForCategoryNameMiscellaneous;
                                    finalHtmlToInsert = new String(htmlToInsert.replace("@categoryName", catDetObject.txt));
                                    finalHtmlToInsert = finalHtmlToInsert.toString();
                                }
                                showHideExistCategoryDivCont(true);
                                $('#divExistingCategory').append(finalHtmlToInsert);
                            }
                        }
                    }
                    else {
                        showHideExistCategoryDivCont(false);
                    }
                }
                else {
                    showHideExistCategoryDivCont(false);
                }

            }
        }
        function showHideExistCategoryDivCont(show) {
            if (show === true) {
                showHideHtmlElement(true, "divExistingCategoryCont");
            }
            else {
                showHideHtmlElement(false, "divExistingCategoryCont");
            }
        }
        function getMsgCatTxtValJsonFromDropDown() {
            return JSON.stringify(getCategoryMsgAryFromCategoryDropDown());
        }
        function processAddNewCategory() {
            removeErrorMsgFromDiv("divAddNewCatError");
            var strCategoryTxtValObjJson = getMsgCatTxtValJsonFromDropDown();
            if (strCategoryTxtValObjJson) {
                addHtmlToExistingCategoriesDiv(strCategoryTxtValObjJson)
            }
            showModalPopUp("divAddNewCategoryCont", "Add new category", "510", false); return false;
        }
        function showCategoriesAfterPostBack() {
            var strCategoryTxtValObjJson = getMsgCatTxtValJsonFromDropDown();
            if (strCategoryTxtValObjJson) {
                addHtmlToExistingCategoriesDiv(strCategoryTxtValObjJson)
            }
        }
        function validateAddCategoryForm() {
            var txtAddNewCategory = $('#' + '<%=txtAddNewCategory.ClientID %>');
            if (txtAddNewCategory) {
                var val = $(txtAddNewCategory).val().toUpperCase();
                if (val === "") {
                    removeErrorMsgFromDiv("divAddNewCatError");
                    $.wl_Alert("Please enter a message category.", 'info', '#divAddNewCatError');
                    return false;
                }
                else if (val === "MFICIENT" || val === "APP NOTIFICATION") {
                    removeErrorMsgFromDiv("divAddNewCatError");
                    $.wl_Alert("Please enter diffrent message category because this is fixed type category.", 'info', '#divAddNewCatError');
                    $(txtAddNewCategory).val("");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        function removeErrorMsgFromDiv(divId) {
            $('#' + divId).children('.alert').remove();
        }
        function hideShowEditRegionLocSpan(show/*boolean*/) {
            if (show) {
                $('#editRegionLoc').show();
            }
            else {
                $('#editRegionLoc').hide();
            }
        }
        function hideShowEditDivDeptSpan(show/*boolean*/) {
            if (show) {
                $('#editDivDept').show();
            }
            else {
                $('#editDivDept').hide();
            }
        }

        function makeUsergroupAutoComplete() {
            var hidAutoCmpltAllGroup = $('#' + '<%=hidgroupid.ClientID %>');
            var hidAlreadySelectedGroup = ''
            hidAlreadySelectedGroup = $('[id$=hidexistingautoappovedgrouplist]').val();
            if (hidAutoCmpltAllGroup && $(hidAutoCmpltAllGroup).val()) {
                var lstgrpDetail = JSON.parse($(hidAutoCmpltAllGroup).val());
            }
            if (hidAlreadySelectedGroup && hidAlreadySelectedGroup != '') {
                var lstAlreadySelectdgroup = JSON.parse(hidAlreadySelectedGroup);
            }
            if (lstgrpDetail) {
                if (hidAlreadySelectedGroup) {
                    $("#" + "<% = txtgroups.ClientID %>").tokenInput(lstgrpDetail, {
                        theme: "facebook",
                        preventDuplicates: true,
                        prePopulate: lstAlreadySelectdgroup
                    });
                }
                else {
                    $("#" + "<% = txtgroups.ClientID %>").tokenInput(lstgrpDetail, {
                        theme: "facebook",
                        preventDuplicates: true
                    });
                }
            }
        }


        function Groupuserhideshow(value) {

            if ($(value).is(':checked') == false) showdiv();
            else $('#divnotalluser').hide();
        };


        function showdiv() {
            $('#divnotalluser').show();
            $('#divgroup').find('ul').remove();
            makeUsergroupAutoComplete();
        }



        //For Category Add and Delete
        function processRemoveCategoryMsg(sender) {
            var confirmDelete = confirm('Are you sure you want to remove this category');
            if (confirmDelete) {
                storeJsonOfUserSelectedForPostback();
                var hidScriptPostBackFor = $('#' + '<%=hidPostBackFor.ClientID %>');
                $(hidScriptPostBackFor).val(ScriptPostBackFor.RemoveMessageCategory);
                var hidExistingCatToRemove = $('#' + '<%=hidExistingCatToRemove.ClientID %>');
                $(hidExistingCatToRemove).val(($(sender).attr("id")).split("_")[1]);
                clickButtonForPostBack("btnAddCategoryContPostBack");
                return true;
            }
            else {
                return false;
            }
        }
        function getCategoryMsgAryFromCategoryDropDown() {
            var ddlCategory = $('#' + '<%=ddlCategory.ClientID %>');
            var opts = $(ddlCategory)[0].options;
            if (opts) {
                var aryOptionTextAndValue = $.map(opts, function (elem) {
                    return formCategoryTextAndValueObject(elem.text, elem.value);
                });
            }
            return aryOptionTextAndValue;
        }
        function formCategoryTextAndValueObject(text, value) {
            var objTextValue = null;
            if (text && value) {
                objTextValue = {
                    txt: text,
                    id: value
                }
            }
            return objTextValue;
        }
        function addHtmlToExistingCategoriesDiv(categoryDetailJson) {
            if (categoryDetailJson) {
                var aryCategoryDetailObject = JSON.parse(categoryDetailJson);
                if (aryCategoryDetailObject) {
                    if (aryCategoryDetailObject.length > 0) {
                        $('#divExistingCategory').empty();
                        for (var i = 0; i <= aryCategoryDetailObject.length - 1; i++) {
                            var catDetObject = aryCategoryDetailObject[i];
                            if (catDetObject) {
                                var htmlToInsert = '';
                                var finalHtmlToInsert = '';
                                if (catDetObject.id != "-1") {
                                    htmlToInsert = templateForCategoryName;
                                    finalHtmlToInsert = new String(htmlToInsert.replace("@categoryName", catDetObject.txt));
                                    finalHtmlToInsert = finalHtmlToInsert.replace("@atagId", "lnk_" + catDetObject.id);
                                }
                                else {
                                    htmlToInsert = templateForCategoryNameMiscellaneous;
                                    finalHtmlToInsert = new String(htmlToInsert.replace("@categoryName", catDetObject.txt));
                                    finalHtmlToInsert = finalHtmlToInsert.toString();
                                }
                                showHideExistCategoryDivCont(true);
                                $('#divExistingCategory').append(finalHtmlToInsert);
                            }
                        }
                    }
                    else {
                        showHideExistCategoryDivCont(false);
                    }
                }
                else {
                    showHideExistCategoryDivCont(false);
                }

            }
        }
        function showHideExistCategoryDivCont(show) {
            if (show === true) {
                showHideHtmlElement(true, "divExistingCategoryCont");
            }
            else {
                showHideHtmlElement(false, "divExistingCategoryCont");
            }
        }
        function getMsgCatTxtValJsonFromDropDown() {
            return JSON.stringify(getCategoryMsgAryFromCategoryDropDown());
        }
        function processAddNewCategory() {
            removeErrorMsgFromDiv("divAddNewCatError");
            var strCategoryTxtValObjJson = getMsgCatTxtValJsonFromDropDown();
            if (strCategoryTxtValObjJson) {
                addHtmlToExistingCategoriesDiv(strCategoryTxtValObjJson)
            }
            showModalPopUp("divAddNewCategoryCont", "Add new category", "510", false); return false;
        }
        function showCategoriesAfterPostBack() {
            var strCategoryTxtValObjJson = getMsgCatTxtValJsonFromDropDown();
            if (strCategoryTxtValObjJson) {
                addHtmlToExistingCategoriesDiv(strCategoryTxtValObjJson)
            }
        }
        function validateAddCategoryForm() {
            var txtAddNewCategory = $('#' + '<%=txtAddNewCategory.ClientID %>');
            if (txtAddNewCategory) {
                var val = $(txtAddNewCategory).val().toUpperCase();
                if (val === "") {
                    removeErrorMsgFromDiv("divAddNewCatError");
                    $.wl_Alert("Please enter a message category.", 'info', '#divAddNewCatError');
                    return false;
                }
                else if (val === "MFICIENT" || val === "APP NOTIFICATION") {
                    removeErrorMsgFromDiv("divAddNewCatError");
                    $.wl_Alert("Please enter diffrent message category because this is fixed type category.", 'info', '#divAddNewCatError');
                    $(txtAddNewCategory).val("");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        function removeErrorMsgFromDiv(divId) {
            $('#' + divId).children('.alert').remove();
        }
        function hideShowEditRegionLocSpan(show/*boolean*/) {
            if (show) {
                $('#editRegionLoc').show();
            }
            else {
                $('#editRegionLoc').hide();
            }
        }
        function hideShowEditDivDeptSpan(show/*boolean*/) {
            if (show) {
                $('#editDivDept').show();
            }
            else {
                $('#editDivDept').hide();
            }
        }



        function showHideHtmlElement(show, elementId) {
            if (show) {
                $("#" + elementId).show();
            }
            else {
                $("#" + elementId).hide();
            }
        }





    </script>
    <style type="text/css">
        .pmHeaderLabel
        {
            font-weight: bold;
            font-size: 13px;
            color: #444444;
        }
        div.modalPopUp .checkboxListTbl table tr td
        {
            border: none;
            text-align: left;
            width: 150px;
        }
        div.modalPopUp .checkboxListTbl table tr td
        {
            padding: 1px 3px;
        }
        ul.token-input-list-facebook
        {
            margin: 0px 0px 0px 0px;
            width: 99%;
        }
        div.sendToSubParts
        {
            margin-left: 15px;
            padding: 3px 0;
        }
        div.sendToSubParts.withMarginTop
        {
            margin-top: 5px;
        }
        div.sendToSubParts .headerLabels
        {
            float: left;
            width: 150px;
        }
        div.sendToSubParts .headerLabelsColon
        {
            float: left;
            width: auto;
        }
        div.sendToSubParts .headerInfoSpan
        {
            float: left;
            width: 200px;
            margin-left: 10px;
        }
        div.combowrap.comboFacebookStyle
        {
            -webkit-border-radius: none !important;
            -moz-border-radius: none !important;
            border-radius: none !important;
            background: #ffffff;
        }
        .repeater.modalPopUp table tr th
        {
            border: none;
        }
        .sheduleHrs .spanLabel
        {
            float: left;
            position: relative;
            top: 6px;
        }
        .sheduleHrs div
        {
            float: left;
        }
        .sheduleHrs .divTextBox
        {
            float: left;
            position: relative;
            top: -7px;
        }
        .sheduleHrs .divDrpDwn
        {
            float: left;
            position: relative;
            top: -3px;
        }
        .sheduleHrs .ui-datepicker-trigger
        {
            position: relative;
            top: 8px;
        }
        #divMessageCategoryFormContainer .selector span
        {
            width: 200px;
        }
        #divDrpDownCategoryCont select
        {
            width: 240px !important;
        }
        #divMessageExpiresCont span.forLabel
        {
            position: relative;
            top: 9px;
        }
        #divSheduleImmediateCont .checker + label
        {
            top: 3px;
        }
        
        .sendToSubParts span.i_pencil
        {
            margin-left: 15px;
            margin-top: -3px;
        }
        div.token-input-dropdown-facebook
        {
            width: 57.2%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updPushMessageForm" runat="server">
        <ContentTemplate>
            <div class="widget">
                <div id="divContainer" class="customWidgetDirectDiv" style="padding: 3px;">
                    <div class="customFieldset">
                        <div class="g12" style="padding: 2px 2px 0px 2px;">
                            <label class="pmHeaderLabel">
                                <asp:CheckBox ID="chkall" Text=" Send to  All Users" onclick="Groupuserhideshow(this);"  runat="server" Checked="true" />
                            </label>
                        </div>
              
                        <div class="g12 sendToSubParts withMarginTop" id="divnotalluser" style="display:none">
                        <label class="pmHeaderLabel">
                                Groups
                            </label>
                            <div style="margin-top:3px;" id="divgroup">
                                <asp:TextBox ID="txtgroups" runat="server" Width="98%"></asp:TextBox></div>

                            <div class="clear">
                            </div>
                            <label class="pmHeaderLabel">
                                Users
                            </label>
                            <div style="margin-top:3px;" id="divuser">
                                <asp:TextBox ID="txtAutoCompleteUserName" runat="server" Width="98%"></asp:TextBox></div>
                        </div>
                    </div> 
                    <div class="customFieldset">
                        <div class="g12" id="divMessageCategoryFormContainer">
                            <div class="fl">
                                <label class="pmHeaderLabel">
                                    Message category :
                                </label>
                            </div>
                            <div class="fl" id="divDrpDownCategoryCont" style="margin-top: -7px; margin-left: 5px;">
                                <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true">
                                </asp:DropDownList>
                            </div>
                            <span class="fl i_pencil" title="edit" style="padding-top: 2px; margin-left: 30px;
                                width: 24px; height: 24px; margin-top: -2px;" onclick='return processAddNewCategory();'>
                            </span>
                            <div class="clear">
                            </div>
                            <section style="margin-top: 10px;">
                                <label class="pmHeaderLabel">
                                    Message text :
                                </label>
                            </section>
                            <div style="margin-top: 3px;">
                                <textarea id="txtMessage" runat="server" onkeyup="update_counter('text_counter', 'txtMessage', 1001)"></textarea><br />
                                <span id="text_counter">0</span> of 1000 characters<br />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div class="customFieldset">
                        <div class="g12">
                            <div class="fl" id="divSheduleImmediateCont" style="width: auto; margin: 0 0 0 -2px;">
                                <asp:CheckBox ID="chkSheduleImmediate" runat="server" Text="Send Immediately" onclick="chkSendImmediatelyClicked(this)" Checked="true" />
                            </div>
                            <asp:Panel ID="pnlSheduledHrsMin" runat="server" CssClass="fl sheduleHrs" Style="margin-left: 30px;
                                width: auto;">
                                <asp:CheckBox ID="chkSheduledHrsMin" runat="server" onclick="chkSheduledHrsMinClicked(this);" />
                                <asp:Label ID="lblTextOn" CssClass="spanLabel" Style="top: 5px;" runat="server" Text="On"></asp:Label>
                                <div id="divDateTextBox" class="fl divTextBox" style="margin-left: 4px;">
                                    <asp:TextBox ID="txtSendDate" runat="server" Width="75%"></asp:TextBox></div>
                                <div id="divHrsContainer" class="fl divDrpDwn">
                                    <asp:DropDownList ID="ddlHours" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <asp:Label ID="lblHrs" CssClass="spanLabel" runat="server" Text="Hrs"></asp:Label>
                                <div id="divMinConntainer" class="fl divDrpDwn">
                                    <asp:DropDownList ID="ddlMinutes" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <asp:Label ID="lblMin" CssClass="spanLabel" runat="server" Text="Min"></asp:Label>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                            <div id="divMessageExpiresCont" style="margin-top: 10px;">
                                <span class="fl forLabel">
                                    <label>
                                        Message expires in
                                    </label>
                                </span>
                                <div class="fl">
                                    <asp:DropDownList ID="ddlExpiryDays" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <span class="fl forLabel">
                                    <label>
                                        days
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="float: right; margin: 10px 2px 5px 0;">
                        <asp:Button ID="btnSend" runat="server" Text="Send" OnClientClick="storeJsonOfUserSelectedForPostback();return validateFormFields();"
                            OnClick="btnSend_Click" />
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div>
                <asp:HiddenField ID="hidTotalLocations" runat="server" />
                <asp:HiddenField ID="hidTotalDepartments" runat="server" />
                <asp:HiddenField ID="hidTimeOffsetOfClient" runat="server" />
                <asp:HiddenField ID="hidCId" runat="server" />
                <asp:HiddenField ID="hidAutoCmpltUserIdsJson" runat="server" />
                <asp:HiddenField ID="hidAutoCmpltAllUserList" runat="server" />
               <%-- <asp:HiddenField ID="hidExistingUsers" runat="server" />--%>
              <%--  <asp:HiddenField ID="hidmgarmusername" runat="server" />--%>
                <asp:HiddenField ID="hidgroupid" runat="server" />

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   
    <div id="divAddNewCategoryCont" class="hideElement">
        <asp:UpdatePanel ID="updAddCategoryCont" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divAddNewCatError">
                </div>
                <div id="divAddCategory" style="margin: 5px 0px;">
                    <span style="margin: 0 15px 0px 2px;">Category :</span>
                    <asp:TextBox ID="txtAddNewCategory" runat="server" MaxLength="25" Style="width: 65%"></asp:TextBox>
                    <span style="margin: 0px 0px 0px 15px;">
                        <asp:Button ID="btnAddNewCategory" runat="server" Text="Add" OnClientClick="storeJsonOfUserSelectedForPostback();return validateAddCategoryForm();"
                            OnClick="btnAddNewCategory_Click" />
                    </span>
                </div>
                <div style="border-bottom: 1px solid; margin: 15px 2px;">
                    <asp:Label ID="lblExistingCategoryText" runat="server" Text="Existing Categories ( you can add a max of 25 categories )"></asp:Label>
                </div>
                <div id="divRemoveCategory">
                    <div class="comboselectbox searchable  fl" id="divExistingCategoryCont" style="width: 100%;
                        margin: 5px 0px 5px 0px; min-height: 0px; display: none;">
                        <div class="combowrap comboFacebookStyle withoutRadius" style="width: 98%; max-width: 98%;">
                            <div class="comboRepeater" id="divExistingCategory">
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hidExistingCatToRemove" runat="server" />
                <div>
                    <asp:Button ID="btnAddCategoryContPostBack" runat="server" OnClick="btnAddCategoryContPostBack_Click"
                        Width="1px" Height="1px" Style="display: none;" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="updScriptClickPostBack" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnScriptClickPostBack" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnAddCategoryContPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <asp:HiddenField ID="hidPostBackFor" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnScriptClickPostBack" runat="server" OnClick="btnScriptClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            preventDefaultOfDate();
            isCookieCleanUpRequired('false');
            //storeJsonOfUserSelectedForPostback();
        }
        function prm_endRequest() {
            hideWaitModal();
            preventDefaultOfDate();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>
