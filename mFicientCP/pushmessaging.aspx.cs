﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class pushmessaging : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;
        int user_role;
        enum SCRIPT_POST_BACK_FOR
        {
            RemoveMessageCategory
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];
            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (strUserId == hfsPart4)
                user_role = Convert.ToInt16(USER_ROLES.ADMIN);
            else
                user_role = Convert.ToInt16(USER_ROLES.SUBADMIN);

            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                if (user_role == 2)
                {
                    hfsPart5 = hfsParts[4];
                    hfsPart6 = hfsParts[5];
                    context.Items["hfs5"] = hfsPart5;
                    context.Items["hfs6"] = hfsPart6;
                    ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
                }
                else
                {
                    ltFullNAME.Text = "";
                }
            }


            if (!Page.IsPostBack)
            {
                bindExpiryDropDown();
                bindMessageCategoryDropDown();
                bindCompanyGroupsdetails();
                setTheInitialSheduleTime(null);
                clearJsonOfUserSelected();
                fillUserDetailJsonForHidField();
                Page.ClientScript.RegisterStartupScript(
                    this.GetType(),
                    this.ClientID,
                    @"$(""input:not(div.checker input)"").uniform();
                    $('body').find('input.date, div.date').wl_Date();
                    changeDatePickerFormat();", true);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page,
                        @"$('body').find('input.date, div.date').wl_Date();
                            changeDatePickerFormat();
                            ");
                    setTheInitialSheduleTime(null);
                    clearJsonOfUserSelected();
                    return;
                }
                ScriptManager.RegisterStartupScript(this.Page,
                    typeof(Page),
                    "SetUniformDesignAfterPostback",
                    @"$(""div:not('.selector')>select"").uniform();
                    $(""input:not(div.checker input)"").uniform();
                    $('body').find('input.date, div.date').wl_Date();
                    changeDatePickerFormat();", true);
            }
            #region Get Company Timezone
            if (user_role == Convert.ToInt16(USER_ROLES.ADMIN))
            {
                try
                {
                    if (!String.IsNullOrEmpty(hfsValue))
                        CompanyTimezone.getTimezoneInfo(hfsPart3);
                }
                catch
                { }
                setHidCmpId(hfsPart3);
            }
            else
            {
                try
                {
                    if (!String.IsNullOrEmpty(hfsValue))
                        CompanyTimezone.getTimezoneInfo(hfsPart4);
                }
                catch
                { }
                setHidCmpId(hfsPart4);
            }
            #endregion
        }


        #region Manipulating HTML Elements Value
        void checkUncheckImmediateShedule(bool check)
        {
            if (check)
            {
                chkSheduleImmediate.Checked = true;
            }
            else
            {
                chkSheduleImmediate.Checked = false;
            }
        }
        void checkUncheckShedulingCheckbox(bool check)
        {
            if (check)
            {
                chkSheduledHrsMin.Checked = true;
            }
            else
            {
                chkSheduledHrsMin.Checked = false;
            }
        }

        void clearMessageTextBox()
        {
            txtMessage.Value = "";
        }

        void enableDisableBtnSendButton(bool enable)
        {
            if (enable)
            {
                btnSend.Enabled = true;
            }
            else
            {
                btnSend.Enabled = false;
            }
        }
        void enableDisableEditButtons(bool enable, bool isForPostBack)
        {
            if (enable)
            {
                //lnkSelectDivisionDept.Visible = true;
                //lnkSelectRegionLoc.Visible = true;
                if (isForPostBack)
                {
                    Utilities.runPostBackScript("hideShowEditRegionLocSpan(true);hideShowEditDivDeptSpan(true);", this.Page, "showEditButtonOnPostBack");
                }
                else
                {
                    Utilities.runPostBackScript("hideShowEditRegionLocSpan(true);hideShowEditDivDeptSpan(true);", this.Page, "showEditButtonOnPostBack");
                }
            }
            else
            {
                //lnkSelectDivisionDept.Visible = false;
                //lnkSelectRegionLoc.Visible = false;
                if (isForPostBack)
                {
                    Utilities.runPostBackScript("hideShowEdittRegionLocSpan(false);hideShowEditDivDeptSpan(false);", this.Page, "hideEditButtonOnPostBack");
                }
                else
                {
                    Utilities.runPageStartUpScript(this.Page, "HideEditButtonOnPageStartUp", "hideShowEditRegionLocSpan(false);hideShowEditDivDeptSpan(false);");
                }
            }
        }
        #endregion

        protected void btnSend_Click(object sender, EventArgs e)
        {
            string strScriptIfProcessFails = @"changeDatePickerFormat();
                                                fillAutoCmpTextOnPostBack();";
            try
            {
                DateTime dtSelectedTimeInUtc = DateTime.UtcNow;
                if (!chkSheduleImmediate.Checked && txtSendDate.Text != "")
                {
                    dtSelectedTimeInUtc = Utilities.getTimeInUtc(txtSendDate.Text, Convert.ToInt32(ddlHours.SelectedValue), Convert.ToInt32(ddlMinutes.SelectedValue), 0, '-');
                    if (!isTimeSelectedWithinTheStipulatedTime(dtSelectedTimeInUtc))
                    {
                        Utilities.showAlert("Time selected not within 48 hrs", "divContainer", updPushMessageForm);
                        Utilities.runPostBackScript(strScriptIfProcessFails, updPushMessageForm, "FormHtmlOnError");
                        return;
                    }
                }
                List<string> lstAdditionalUserSelected = new List<string>();
                if (chkall.Checked == true) 
                    lstAdditionalUserSelected = this.GetallUser(); 
                else 
                    lstAdditionalUserSelected = this.getAdditionalUsersSelected(); 

                string strValidationError = string.Empty;
                if (String.IsNullOrEmpty(strValidationError))
                {
                    SavePushMessageInfoAndLogs objSavePushMsgDetails =
                        new SavePushMessageInfoAndLogs(user_role == (int)USER_ROLES.ADMIN ? hfsPart3 : hfsPart4,
                            user_role == (int)USER_ROLES.ADMIN ? hfsPart4 : hfsPart3,
                            user_role, txtMessage.Value,
                            ddlCategory.SelectedItem.Text, Convert.ToInt32(ddlExpiryDays.SelectedValue),
                            chkSheduleImmediate.Checked ? DateTime.UtcNow.Ticks : dtSelectedTimeInUtc.Ticks,
                            lstAdditionalUserSelected);

                    objSavePushMsgDetails.Process();

                    if (objSavePushMsgDetails.StatusCode == 0)
                    {
                        clearcntrl();
                        Utilities.showMessage("Notification send sucessfully", updPushMessageForm, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error", updPushMessageForm, DIALOG_TYPE.Error);
                        throw new Exception();
                    }
                }
                else
                {
                    Utilities.showAlert(strValidationError, "divContainer", updPushMessageForm);
                    Utilities.runPostBackScript(strScriptIfProcessFails, updPushMessageForm, "FormHtmlOnError");
                }
            }
            catch
            {
                Utilities.runPostBackScript(strScriptIfProcessFails, updPushMessageForm, "FormHtmlOnError");
                Utilities.showMessage("Internal server error", updPushMessageForm, DIALOG_TYPE.Error);
            }
        }

        void clearcntrl()
        {
            txtAutoCompleteUserName.Text = "";
            txtAutoCompleteUserName.Text = "";
            txtgroups.Text = "";
            txtMessage.InnerText = "";
            txtSendDate.Text = "";
            chkall.Checked = true;
            chkSheduleImmediate.Checked = true;
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"makeTextBoxAutoComplete();makeUsergroupAutoComplete();", true);

        }

        /// <summary>
        /// Get UserIds From Users selected in Auto Complete text box
        /// </summary>
        /// <returns></returns>
        /// 

        private void datatablegetuser(string groupidList, out string getuserlist)
        {
            GetUsersofGroup getUsers = new GetUsersofGroup();
            getUsers.GroupId = groupidList;
            getUsers.ProcessByGroupIds();
            getuserlist = "";
            if (getUsers.StatusCode == 0)
            {
                DataView dvGroupUsers = getUsers.GroupUsers.DefaultView;
                dvGroupUsers.Sort = "USER_NAME";
                DataTable dtblGroupUsers = dvGroupUsers.ToTable();
                if (dtblGroupUsers.Rows.Count > 0)
                {
                    foreach (DataRow row in dtblGroupUsers.Rows)
                    {
                        if (string.IsNullOrEmpty(getuserlist))
                        {
                            getuserlist = Convert.ToString(row["USER_ID"]);
                        }
                        else
                        {
                            getuserlist += "," + Convert.ToString(row["USER_ID"]);
                        }
                    }
                }
            }
        }
        void fillalluerrsInhiddenfield(out string struserid)
        {
            GetUsersofGroup getUsers = new GetUsersofGroup();
            getUsers.GetAllUser(hfsPart4);
            struserid = "";
            string struserdetails = "";
            if (getUsers.StatusCode == 0)
            {
                DataView dvGroupUsers = getUsers.GroupUsers.DefaultView;
                dvGroupUsers.Sort = "USER_NAME";
                DataTable dtblGroupUsers = dvGroupUsers.ToTable();
                if (dtblGroupUsers.Rows.Count > 0)
                {
                    foreach (DataRow row in dtblGroupUsers.Rows)
                    {
                        if (string.IsNullOrEmpty(struserdetails))
                        {
                            struserdetails = Convert.ToString(row["USER_ID"]);

                        }
                        else
                        {
                            struserdetails += "," + Convert.ToString(row["USER_ID"]);
                        }
                    }
                }
                struserid = struserdetails;
            }
        }
        List<string> getAdditionalUsersSelected()
        {
            List<string> lstUserIdsSelected = new List<string>();
            if (!String.IsNullOrEmpty(txtgroups.Text))
            {
                string strgroupid = txtgroups.Text;
                string[] arr = strgroupid.Split(',');
                strgroupid = "";
                foreach (var item in arr)
                {
                    if (strgroupid == "") strgroupid = "'" + item + "'";
                    else strgroupid += "," + "'" + item + "'";
                }
                string existinguser = "";
                datatablegetuser(strgroupid, out existinguser);
                string[] userexiting = existinguser.Split(',');
                foreach (string usess in userexiting)
                {
                    if (usess != "")
                    {
                        if (!lstUserIdsSelected.Contains(usess)) lstUserIdsSelected.Add(usess);
                    }
                }
            }
            if (!String.IsNullOrEmpty(txtAutoCompleteUserName.Text))
            {
                string[] aryUserIdAfterSplit = txtAutoCompleteUserName.Text.Split(',');
                foreach (string userId in aryUserIdAfterSplit)
                {
                    if (!lstUserIdsSelected.Contains(userId)) lstUserIdsSelected.Add(userId);
                }
            }
            return lstUserIdsSelected;
        }
        List<string> GetallUser()
        {
            List<string> lstUserIdsSelected = new List<string>();

            string struserid = "";
            fillalluerrsInhiddenfield(out struserid);

            if (!String.IsNullOrEmpty(struserid))
            {
                string[] aryUserIdAfterSplit = struserid.Split(',');
                foreach (string userId in aryUserIdAfterSplit)
                {
                    if (!lstUserIdsSelected.Contains(userId)) lstUserIdsSelected.Add(userId);
                }
            }
            return lstUserIdsSelected;

        }

        string validatePushMessageForm(List<string> loc,
            List<string> dept,
            List<string> additionalUsersSelected)
        {
            string strError = String.Empty;

            if (String.IsNullOrEmpty(txtMessage.Value) ||
                txtMessage.Value.Length > 1000)
            {
                strError += @"Please enter valid message.<br/>";
            }
            if (!chkSheduleImmediate.Checked)
            {
                if (txtSendDate.Text == "")
                {
                    strError += @"Please select a date to send message if not sheduled immediately.<br/>";
                }
            }
            return strError;
        }


        protected int Count(string compString, char letter)
        {
            char[] split = compString.ToCharArray();
            int count = 0;
            foreach (char character in split)
            {
                if (character.Equals(letter))
                    count++;
            }
            return count;
        }
        #region sheduling

        protected void btnDeliveryTimeOk_Click(object sender, EventArgs e)
        {
            //string strCompanyId;

            //Utilities.runPostBackScript(
            //    "formTheHTMLForRegionSelectedComboBox();formTheHTMLForDivisionSelectedComboBox();fillAutoCmpTextOnPostBack();",
            //    updSheduledTime,
            //    "formHTMLAterPostBack");

            //if (txtSendDate.Text == "")
            //{
            //    Utilities.showAlert("Please enter a date", "sheduledTimeError", updSheduledTime);
            //    return;
            //}
            //if (user_role == (int)USER_ROLES.ADMIN)
            //{
            //    strCompanyId = hfsPart3;
            //}
            //else
            //{
            //    strCompanyId = hfsPart4;
            //}


            //DateTime dtSelectedTimeInUtc =
            //    Utilities.getTimeInUtc(
            //    txtSendDate.Text,
            //    Convert.ToInt32(ddlHours.SelectedValue),
            //    Convert.ToInt32(ddlMinutes.SelectedValue),
            //    0,
            //    '-');

            //if (!isTimeSelectedWithinTheStipulatedTime(dtSelectedTimeInUtc))
            //{
            //    Utilities.showAlert("Time selected not within 48 hrs", "sheduledTimeError", updSheduledTime);
            //    return;
            //}

            //pnlSheduledHrsMin.Visible = true;
            //chkSheduledHrsMin.Checked = true;
            //checkUncheckImmediateShedule(false);


            ////DateTime dtTimeSelected;
            ////getSplitValuesOfDateSelected(out dtTimeSelected);
            //setHidSheduleTime(dtSelectedTimeInUtc.Ticks.ToString());
            //string formatedDate = Utilities.getFormattedDateForUI(txtSendDate.Text, '-');
            //setTheTextOfShedulesCheckBox(formatedDate,
            //    ddlHours.SelectedItem.Text,
            //    ddlMinutes.SelectedItem.Text);
            //Utilities.closeModalPopUp("divSheduleTime", updSheduledTime);
            //updSheduledTime.Update();

        }
        void bindHoursDropDown()
        {
            ddlHours.Items.Clear();
            for (int i = 0; i <= 23; i++)
            {
                string text = i.ToString().Length == 1 ? "0" + i.ToString() : i.ToString();
                ddlHours.Items.Add(new ListItem(text, i.ToString()));
            }
        }
        void bindMinDropDown()
        {
            ddlMinutes.Items.Clear();
            ddlMinutes.Items.Add(new ListItem("00", "0"));
            ddlMinutes.Items.Add(new ListItem("15", "15"));
            ddlMinutes.Items.Add(new ListItem("30", "30"));
            ddlMinutes.Items.Add(new ListItem("45", "45"));
        }
        void bindExpiryDropDown()
        {
            ddlExpiryDays.Items.Clear();
            for (int i = 1; i <= 30; i++)
            {
                ddlExpiryDays.Items.Add(
                    new ListItem(
                        i.ToString().Length == 1 ? "0" + i.ToString() : i.ToString(),
                        i.ToString()
                        )
                        );
            }
        }
        #endregion

        /// <summary>
        /// The time should be within 48 hrs.
        /// </summary>
        bool isTimeSelectedWithinTheStipulatedTime()
        {

            DateTime rightNow = DateTime.UtcNow;
            DateTime dateSelectedForSendingMsg;
            getSplitValuesOfDateSelected(out dateSelectedForSendingMsg);
            DateTime dtUtcDateAfter48Hours =
                rightNow.AddHours(48);

            long lngTicksRightNow = rightNow.Ticks;
            long lngDateSelectedInUtc = dateSelectedForSendingMsg.Ticks;
            long lngDateAfter48HrsUtc = dtUtcDateAfter48Hours.Ticks;

            /*
             *Allow Selection of a date which is after the present date.
             *And should be within 48 hours after the present date.
            */
            if ((lngTicksRightNow < lngDateSelectedInUtc) &&
                (lngDateSelectedInUtc <= lngDateAfter48HrsUtc))
            {
                return true;
            }
            else
            {
                return false;
            }
            #region Previous Code
            //Remove after testing
            //long ticksOfDateSelectedWithClientOffset = dateSelectedForSendingMsg.
            //    AddHours(clientOffsetHrsFromUtc).
            //    AddMinutes(clientOffsetMinFromUtc).
            //    Ticks;

            //long ticksOfNowWithClientOffset = rightNow.AddHours(clientOffsetHrsFromUtc).
            //    AddMinutes(clientOffsetMinFromUtc).
            //    Ticks;
            //long ticksAfter48HrsWithClientOffset =
            //    (rightNow.AddHours((clientOffsetHrsFromUtc + 48)).
            //    AddMinutes(clientOffsetMinFromUtc)).
            //    Ticks;
            ////check for time is not less than now
            ////it is before 48 hours as well
            //if (ticksOfNowWithClientOffset < ticksOfDateSelectedWithClientOffset &&
            //    ticksOfDateSelectedWithClientOffset <= ticksAfter48HrsWithClientOffset)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            //Remove after complete testing
            //DateTime dateSelectedInUtc = dateSelectedForSendingMsg.ToUniversalTime();
            //long ticksOfDateSelectedWithOffset = rightNow.AddHours(clientOffsetHrsFromUtc).AddHours(clientOffsetMinFromUtc).Ticks;
            //this is not required
            //long ticksOfDateSelectedWithOffset = dateSelectedInUtc.Ticks;
            //long ticksOfDateTimeClientMin = rightNow.AddMinutes(15).Ticks;
            //if (ticksOfDateTimeClientMin <= ticksOfDateSelectedWithOffset && ticksOfDateSelectedWithOffset <= ticksAfter48HrsWithClientOffset)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            #endregion
        }
        bool isTimeSelectedWithinTheStipulatedTime(DateTime selectedDateInUtc)
        {
            DateTime rightNow = DateTime.UtcNow;
            DateTime dateSelectedForSendingMsg = selectedDateInUtc;
            DateTime dtUtcDateAfter48Hours =
                rightNow.AddHours(48);

            long lngTicksRightNow = rightNow.Ticks;
            long lngDateSelectedInUtc = dateSelectedForSendingMsg.Ticks;
            long lngDateAfter48HrsUtc = dtUtcDateAfter48Hours.Ticks;

            /*
             *Allow Selection of a date which is after the present date.
             *And should be within 48 hours after the present date.
            */
            if ((lngTicksRightNow < lngDateSelectedInUtc) &&
                (lngDateSelectedInUtc <= lngDateAfter48HrsUtc))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        void clearJsonOfUserSelected()
        {
            hidAutoCmpltUserIdsJson.Value = "";
        }

        void setHidCmpId(string value)
        {
            hidCId.Value = value;
        }

        string[] getSplitValuesOfDateSelected(out DateTime dateSelectedInUTC)
        {
            string[] aryDateSelected = txtSendDate.Text.Split('-');//dd-mm-yy
            dateSelectedInUTC = new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0]),
                Convert.ToInt32(ddlHours.SelectedValue),
                Convert.ToInt32(ddlMinutes.SelectedValue),
                0
                ).ToUniversalTime();
            return aryDateSelected;
        }

        void setTheInitialSheduleTime(UpdatePanel updPanel)
        {
            if (updPanel != null)
            {
                Utilities.runPostBackScript("makeDatePickerWithInitialDate();", updPanel, "Initialise Date");
            }
            else
            {
                Utilities.runPageStartUpScript(this.Page, "Initialise Date", "makeDatePickerWithInitialDate();");
            }
            bindHoursDropDown();
            bindMinDropDown();
            ddlHours.SelectedIndex = ddlHours.Items.IndexOf(ddlHours.Items.FindByValue("12"));
            ddlMinutes.SelectedIndex = ddlMinutes.Items.IndexOf(ddlMinutes.Items.FindByValue("0"));

        }

        void clearHiddenAutoCompleteTextAndMakeItAutoCmplete(UpdatePanel upd)
        {
            txtAutoCompleteUserName.Text = String.Empty;
            Utilities.runPostBackScript(
                "makeTextBoxAutoComplete();",
                upd,
                "formHTMLAterPostBack");
        }

        protected void btnScriptClickPostBack_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hidPostBackFor.Value))
            {
                SCRIPT_POST_BACK_FOR ePostBackFor;
                if (Enum.TryParse(hidPostBackFor.Value, out ePostBackFor))
                {
                    if (Enum.IsDefined(typeof(SCRIPT_POST_BACK_FOR), ePostBackFor))
                    {
                        switch (ePostBackFor)
                        {
                            case SCRIPT_POST_BACK_FOR.RemoveMessageCategory:
                                break;
                        }
                    }
                }

            }
        }
        protected void btnAddCategoryContPostBack_Click(object sender, EventArgs e)
        {

            string strOtherScriptsForHTMLFormation = @"changeDatePickerFormat();fillAutoCmpTextOnPostBack();";
            try
            {
                string strUserId = "";
                string strCompanyId = "";
                if (user_role == (int)USER_ROLES.ADMIN)
                {
                    strUserId = hfsPart4;
                    strCompanyId = hfsPart3;
                }
                else
                {
                    strUserId = hfsPart3;
                    strCompanyId = hfsPart4;
                }
                if (!String.IsNullOrEmpty(hidExistingCatToRemove.Value))
                {
                    DeletePushMsgMessageCategory objDeleteMsgCategory =
                        new DeletePushMsgMessageCategory(strCompanyId,
                            strUserId,
                            hidExistingCatToRemove.Value);
                    objDeleteMsgCategory.Process();
                    if (objDeleteMsgCategory.StatusCode == 0)
                    {
                        Utilities.showMessage("Message Category deleted successfully",
                                updAddCategoryCont,
                                DIALOG_TYPE.Info);
                        bindMessageCategoryDropDown();
                        Utilities.runPostBackScript("showCategoriesAfterPostBack();", updAddCategoryCont, "Show Categories");
                        Utilities.runPostBackScript(strOtherScriptsForHTMLFormation, updAddCategoryCont, "FormHtmlOnError");
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                Utilities.runPostBackScript(strOtherScriptsForHTMLFormation, updAddCategoryCont, "FormHtmlOnError");
                Utilities.showAlert("Internal server error",
                    "divAddNewCatError",
                    updAddCategoryCont);
            }
        }
        void bindMessageCategoryDropDown()
        {
            string strUserId = "";
            string strCompanyId = "";
            if (user_role == (int)USER_ROLES.ADMIN)
            {
                strUserId = hfsPart4;
                strCompanyId = hfsPart3;
            }
            else
            {
                strUserId = hfsPart3;
                strCompanyId = hfsPart4;
            }
            GetPushMsgMessageCategories objMsgCategories =
                new GetPushMsgMessageCategories(strCompanyId, strUserId);
            objMsgCategories.Process();
            if (objMsgCategories.StatusCode == 0)
            {
                ddlCategory.Items.Clear();
                if (objMsgCategories.ResultTable.Rows.Count > 0)
                {
                    ddlCategory.DataSource = objMsgCategories.ResultTable;
                    ddlCategory.DataTextField = "MSG_CATEGORY";
                    ddlCategory.DataValueField = "MSG_CATEGORY_ID";
                    ddlCategory.DataBind();
                    ddlCategory.Items.Insert(0, new ListItem("Miscellaneous", "-1"));
                }
                else
                {
                    ddlCategory.Items.Insert(0, new ListItem("Miscellaneous", "-1"));
                }
            }

        }
        void bindMessageCategoryDropDown(DataTable msgCategory)
        {
            if (msgCategory.Rows.Count > 0)
            {
                ddlCategory.DataSource = msgCategory;
                ddlCategory.DataTextField = "MSG_CATEGORY";
                ddlCategory.DataValueField = "MSG_CATEGORY_ID";
                ddlCategory.DataBind();
            }
            else
            {
                ddlCategory.Items.Insert(0, new ListItem("Miscellaneous", "-1"));
            }
        }
        protected void btnAddNewCategory_Click(object sender, EventArgs e)
        {
            //            string strOtherScriptsForHTMLFormation = @"processFillDivisionDeptSelectedHidField();
            //                                     formTheHTMLForDivisionSelectedComboBox();
            //                                     processFillRegionLocationSelectedHidField();
            //                                     formTheHTMLForRegionSelectedComboBox();
            //                                     changeDatePickerFormat();
            //                                    fillAutoCmpTextOnPostBack();
            //                                     ";

            string strOtherScriptsForHTMLFormation = @"changeDatePickerFormat();fillAutoCmpTextOnPostBack();";
            try
            {
                string strUserId = "";
                string strCompanyId = "";
                if (user_role == (int)USER_ROLES.ADMIN)
                {
                    strUserId = hfsPart4;
                    strCompanyId = hfsPart3;
                }
                else
                {
                    strUserId = hfsPart3;
                    strCompanyId = hfsPart4;
                }
                if (String.IsNullOrEmpty(txtAddNewCategory.Text) ||
                    txtAddNewCategory.Text.Length > 25)
                {
                    throw new MficientException("Please enter a new category.");
                }

                GetPushMsgMessageCategories objMsgCatCount =
                    new GetPushMsgMessageCategories(strCompanyId, strUserId);
                int iMsgCatCount = objMsgCatCount.ProcessGetMsgCategoriesCount();
                if (objMsgCatCount.StatusCode == 0)
                {
                    if (iMsgCatCount < 24)
                    {
                        InsertPushMsgMessageCategory objInsertMsgCategory =
                            new InsertPushMsgMessageCategory(strCompanyId, strUserId, txtAddNewCategory.Text);
                        objInsertMsgCategory.Process();
                        if (objInsertMsgCategory.StatusCode == 0)
                        {
                            Utilities.showMessage("Message Category saved successfully",
                                updAddCategoryCont,
                                DIALOG_TYPE.Info);
                            //DataTable dtblMsgCategories;
                            //string strJsonMsgCategories= getJsonForMessageCategoryUIBinding(out dtblMsgCategories);
                            bindMessageCategoryDropDown();
                            Utilities.runPostBackScript("showCategoriesAfterPostBack();", updAddCategoryCont, "Show Categories");
                            Utilities.runPostBackScript(strOtherScriptsForHTMLFormation, updAddCategoryCont, "FormHtmlOnError");
                            txtAddNewCategory.Text = "";
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    else
                    {
                        throw new MficientException("Only 25 message categories could be added.");
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (MficientException ex)
            {
                Utilities.runPostBackScript(strOtherScriptsForHTMLFormation, updAddCategoryCont, "FormHtmlOnError");
                Utilities.runPostBackScript("showCategoriesAfterPostBack();", updAddCategoryCont, "Show Categories");
                Utilities.showAlert(ex.Message,
                    "divAddNewCatError",
                    updAddCategoryCont);
            }
            catch
            {
                Utilities.runPostBackScript(strOtherScriptsForHTMLFormation, updAddCategoryCont, "FormHtmlOnError");
                Utilities.runPostBackScript("showCategoriesAfterPostBack();", updAddCategoryCont, "Show Categories");
                Utilities.showAlert("Internal server error",
                    "divAddNewCatError",
                    updAddCategoryCont);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.MficientException">Thrown when there is some internal error</exception>
        List<MFEPushMsgMessageCategory> getMessageCategories(out DataTable msgCategories)
        {
            string strUserId = "";
            string strCompanyId = "";
            msgCategories = null;
            if (user_role == (int)USER_ROLES.ADMIN)
            {
                strUserId = hfsPart4;
                strCompanyId = hfsPart3;
            }
            else
            {
                strUserId = hfsPart3;
                strCompanyId = hfsPart4;
            }
            GetPushMsgMessageCategories objMsgCategories =
                new GetPushMsgMessageCategories(strCompanyId, strUserId);
            objMsgCategories.Process();
            if (objMsgCategories.StatusCode == 0)
            {
                msgCategories = objMsgCategories.ResultTable;
                return objMsgCategories.MsgCategories;
            }
            else
            {
                throw new MficientException("Internal server error.");
            }
        }
        string getJsonForMessageCategoryUIBinding()
        {
            DataTable dtblMsgCategory = new DataTable();
            List<MFEPushMsgMessageCategory> lstMsgCategory =
                getMessageCategories(out dtblMsgCategory);
            List<MessageCategoryForPushMessage> lstJsonClassMsgCat =
                    new List<MessageCategoryForPushMessage>();
            if (lstMsgCategory.Count > 0)
            {
                foreach (MFEPushMsgMessageCategory mfeMsgCat in lstMsgCategory)
                {
                    MessageCategoryForPushMessage objMsgCatPushMsg =
                        new MessageCategoryForPushMessage();
                    objMsgCatPushMsg.id = mfeMsgCat.MsgCategoryId;
                    objMsgCatPushMsg.txt = mfeMsgCat.MessageCategory;
                    lstJsonClassMsgCat.Add(objMsgCatPushMsg);
                }
            }
            return Utilities.SerializeJson<List<MessageCategoryForPushMessage>>(lstJsonClassMsgCat);
        }
        string getJsonForMessageCategoryUIBinding(out DataTable msgCategory)
        {
            DataTable dtblMsgCategory = new DataTable();
            List<MFEPushMsgMessageCategory> lstMsgCategory =
                getMessageCategories(out dtblMsgCategory);
            List<MessageCategoryForPushMessage> lstJsonClassMsgCat =
                    new List<MessageCategoryForPushMessage>();
            if (lstMsgCategory.Count > 0)
            {
                foreach (MFEPushMsgMessageCategory mfeMsgCat in lstMsgCategory)
                {
                    MessageCategoryForPushMessage objMsgCatPushMsg =
                        new MessageCategoryForPushMessage();
                    objMsgCatPushMsg.id = mfeMsgCat.MsgCategoryId;
                    objMsgCatPushMsg.txt = mfeMsgCat.MessageCategory;
                    lstJsonClassMsgCat.Add(objMsgCatPushMsg);
                }
            }
            msgCategory = dtblMsgCategory;
            return Utilities.SerializeJson<List<MessageCategoryForPushMessage>>(lstJsonClassMsgCat);
        }
        string getJsonForUserList()
        {
            string strUserId = "";
            string strCompanyId = "", strJsonForAutoCompleteText = "";
            if (user_role == (int)USER_ROLES.ADMIN)
            {
                strUserId = hfsPart4;
                strCompanyId = hfsPart3;
            }
            else
            {
                strUserId = hfsPart3;
                strCompanyId = hfsPart4;
            }
            GetUserDetail objUsrDetail = new GetUserDetail();
            List<MFEMobileUser> lstUsers = objUsrDetail.getUserDetail(strCompanyId);
            if (objUsrDetail.StatusCode == 0)
            {
                if (lstUsers != null && lstUsers.Count > 0)
                {
                    List<UserDetailMembersForAutoCmptTextBox> lstUserDetailResponse = new List<UserDetailMembersForAutoCmptTextBox>();
                    foreach (MFEMobileUser user in lstUsers)
                    {

                        UserDetailMembersForAutoCmptTextBox objUserDetailMembers = new UserDetailMembersForAutoCmptTextBox();
                        objUserDetailMembers.id = user.UserId;
                        objUserDetailMembers.name = user.FirstName + " " + user.LastName;
                        objUserDetailMembers.fnm = user.FirstName;
                        objUserDetailMembers.lnm = user.LastName;
                        lstUserDetailResponse.Add(objUserDetailMembers);
                    }
                    UserDetailResponseForAutoCompleteTextBox objUserDetailResponse = new UserDetailResponseForAutoCompleteTextBox();
                    objUserDetailResponse.data = lstUserDetailResponse;
                    strJsonForAutoCompleteText = Utilities.SerializeJson<UserDetailResponseForAutoCompleteTextBox>(objUserDetailResponse);
                    strJsonForAutoCompleteText = Utilities.getFinalJsonForAutoCompleteTextBox(strJsonForAutoCompleteText);
                }
            }
            return strJsonForAutoCompleteText;
        }
        void fillUserDetailJsonForHidField()
        {
            hidAutoCmpltAllUserList.Value = getJsonForUserList();
        }

        void bindCompanyGroupsdetails()
        {
            string strCompanyId =  hfsPart4;
            if (user_role == (int)USER_ROLES.ADMIN) strCompanyId = hfsPart3;
            hidgroupid.Value = "[]";
            GetGroupDetails objgroupUsers = new GetGroupDetails(strCompanyId);
            objgroupUsers.Process();
            if (objgroupUsers.StatusCode == 0)
            {
                if (objgroupUsers.ResultTable != null && objgroupUsers.ResultTable.Rows.Count > 0)
                {
                    List<GroupDetails> lstUserDetailResponse = new List<GroupDetails>();
                    string strnullcheck = "";
                    foreach (DataRow user in objgroupUsers.ResultTable.Rows)
                    {
                        GroupDetails objUserDetailMembers = new GroupDetails();
                        objUserDetailMembers.id = Convert.ToString(user["GROUP_ID"]);
                        objUserDetailMembers.name = Convert.ToString(user["GROUP_NAME"]);
                        lstUserDetailResponse.Add(objUserDetailMembers);

                        strnullcheck = Convert.ToString(user["REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION"]);                        
                    }
                    GroupDetailsAutoCompleteTextBox objgroupdetailresponse = new GroupDetailsAutoCompleteTextBox();
                    objgroupdetailresponse.data = lstUserDetailResponse;
                    hidgroupid.Value = Utilities.getFinalJsonForAutoCompleteTextBox(
                    Utilities.SerializeJson<GroupDetailsAutoCompleteTextBox>(objgroupdetailresponse)
                    );

                }
            }
        }
    }
}