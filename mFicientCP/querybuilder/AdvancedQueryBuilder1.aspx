﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvancedQueryBuilder1.aspx.cs"
    Inherits="mFicientCP.AdvancedQueryBuilder1" %>

<%@ Register TagPrefix="uc" TagName="QueryBuilderUC" Src="~/querybuilder/QueryBuilderOLEDB.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head id="Head1" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Query Builder Page</title>
    <%--<script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>--%>
    <style type="text/css">
            .opaqueLayer
            {
                display:none;
                position:absolute;
                top:0px;
                left:0px;
                opacity:0.6  !important;
                filter:alpha(opacity=60);
                background-color: #333 !important;
                z-Index:1000;
            }
            .ui-widget-overlay
            {
                
                opacity:0.7  !important;
                background-color: #333 !important;
                background-image:none !important;
            }
            

            .questionLayer
            {
                position:absolute;
                top:0px;
                left:0px;
                margin-left:700px;
                margin-top:300px;
                width:50px;
                height:30px;
                display:none;
                z-Index:1001;
                background-color:#FFFFFF;
                text-align:center;
                vertical-align:middle;
                padding:5px;
            }
            .InputStyle
            {
                border-bottom-left-radius: 2px;
                border-top-left-radius: 2px;
                background: none repeat scroll 0 0 #EAEAEA;
                border: 1px solid #DDDDDD;
                border-radius: 0 0 0 0;
                box-shadow: none;
                color: #AAAAAA;
                font-size: 9px;
                height: 22px;
                line-height: 22px;
                margin: -4px -4px -4px 0;
                display: inline-block;
                font-family: "Tahoma" ,sans-serif;
                font-weight: bold;
                padding: 0 14px;
                text-decoration: none;
                text-shadow: 0 1px 0 #FFFFFF;
                text-transform: uppercase;
                margin-top:1px;
                cursor:pointer;
            }
        </style>
    <script type="text/javascript">
        var PageComplete = false;
        var UserCtrlUnLoad = false;
        var IMGprogress = new Image(32, 32);
        IMGprogress.src = '//enterprise.mficient.com/css/images/icons/dark/progress.gif';
        function CtrlUnLoad() {
            UserCtrlUnLoad = true;
        }
        function spinDiv(_bol) {
            if (_bol) {
                $("#spinDiv").dialog({
                    modal: true,
                    closeOnEscape: false,
                    resizable: false,
                    autoOpen: true
                });
            }
            else {
                    $('#spinDiv').dialog('close');
            }
        }

        function showWait(_bol) {
            if (_bol) {
                $('#QbcDiv').css('z-index', '1001');
                $('#QbcDiv').find('div').each(function () {
                    $(this).css('z-index', '1001');
                });
                spinDiv(true);
                if ($('#spinDiv').length > 0)
                    if ($($('#spinDiv')[0].parentElement).length > 0) {
                        $($('#spinDiv')[0].parentElement).width(50);
                        $($('#spinDiv')[0].parentElement).height(50);
                        setModalPosition($('#spinDiv').parent());
                        $($('#spinDiv')[0].previousElementSibling).hide();
                        $($('#spinDiv')[0].parentElement).addClass("spinDivCorner");
                    }
            }
            else {
                spinDiv(false);
            }
        }

        //Copied from Scripts.js
        function setModalPosition(divToSet) {

            PgDim = getPageSize();
            var vleft = ((PgDim[2] - divToSet.width()) / 2);
            var vtop = ((PgDim[3] - divToSet.height()) / 2);
            divToSet.offset({ top: vtop, left: vleft });
        }
        //Copied from Scripts.js
        function getPageSize() {
            var xScroll, yScroll;
            if (window.innerHeight && window.scrollMaxY) {
                xScroll = document.body.scrollWidth;
                yScroll = window.innerHeight + window.scrollMaxY;
            }
            // all but Explorer Mac 
            else if (document.body.scrollHeight > document.body.offsetHeight) {
                xScroll = document.body.scrollWidth;
                yScroll = document.body.scrollHeight;
            }
            // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari 
            else {
                xScroll = document.body.offsetWidth;
                yScroll = document.body.offsetHeight;
            }

            var windowWidth, windowHeight;
            // all except Explorer
            if (self.innerHeight) {
                windowWidth = self.innerWidth;
                windowHeight = self.innerHeight;
            }
            // Explorer 6 Strict Mode 
            else if (document.documentElement && document.documentElement.clientHeight) {
                windowWidth = document.documentElement.clientWidth;
                windowHeight = document.documentElement.clientHeight;
            }
            // other Explorers 
            else if (document.body) {
                windowWidth = document.body.clientWidth;
                windowHeight = document.body.clientHeight;
            }
            if (yScroll < windowHeight) pageHeight = windowHeight;
            else pageHeight = yScroll;
            // for small pages with total width less then width of the viewport
            if (xScroll < windowWidth) {
                pageWidth = windowWidth;
            }
            else {
                pageWidth = xScroll;
            }
            arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
            return arrayPageSize;
        }
        function CtrlClick() {
            $('[id$=btnGenerate]').click();
        }
        function btnClicked() {
            $('[id$=btnBackToSimpleQueryBuilder]').click();
        }
        function btnBackClick() {
            showWait(true); isCookieCleanUpRequired('false');
        }
    </script>
    <style type="text/css">
       
        body
        {
           /* background-image: url(../images/paper_02.png);
            margin: 0 20px;
            font-family: "PT Sans" , sans-serif;
            font-size: 12px;*/
        }
        
        .tt
        {
        }
        
        header
        {
            border-left: 1px solid;
            border-color: #d7d7d7 #c2c2c2 #c2c2c2 #d7d7d7;
            min-width: 1050px;
            position: relative;
            border-top: 1px solid;
            z-index: 10;
            -webkit-border-top-left-radius: 4px;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
            -webkit-border-top-right-radius: 4px;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-box-shadow: 3px 0 4px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 3px 0 4px rgba(0, 0, 0, 0.1);
            box-shadow: 3px 0 4px rgba(0, 0, 0, 0.1);
            border-top-color: #d7d7d7;
            background: #f3f3f3; /* the filter causes an overflow problem on IE9 http://stackoverflow.com/questions/3619383/internet-explorer-css-property-filter-ignores-overflowvisible 	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f6f6f6', endColorstr='#f1f1f1');*/
            background: -webkit-gradient(linear, left top, left bottom, from(#f6f6f6), to(#f1f1f1));
            background: -moz-linear-gradient(top,  #f6f6f6,  #f1f1f1);
            background: -o-linear-gradient(top,  #f6f6f6,  #f1f1f1);
        }
        #logo
        {
            height: 5px;
            padding: 10px 0 17px 0px;
            margin-left: 27px;
            width: 180px;
            border: 0;
            -webkit-border-top-left-radius: 4px;
            -moz-border-radius-topleft: 4px;
            border-top-left-radius: 4px;
           background:url(//enterprise.mficient.com/images/mficientLogo69x34.png) left center no-repeat;
        }
        #header
        {
            height: 30px;
            position: absolute;
            top: 0;
            left: 240px;
            right: 0;
            border: 0;
            -webkit-border-top-right-radius: 4px;
            -moz-border-radius-topright: 4px;
            border-top-right-radius: 4px;
        }
        .spinDivCorner
        {
            -webkit-border-top-left-radius: 6px;
            -moz-border-radius-topleft: 6px;
            border-top-left-radius: 6px;
            -webkit-border-top-right-radius: 6px;
            -moz-border-radius-topright: 6px;
            border-top-right-radius: 6px;
            
            -webkit-border-bottom-left-radius: 6px;
            -moz-border-radius-bottomleft: 6px;
            border-bottom-left-radius: 6px;
            -webkit-border-bottom-right-radius: 6px;
            -moz-border-radius-bottomright: 6px;
            border-bottom-right-radius: 6px;
            
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-box-shadow: 6px 0 6px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: 6px 0 6px rgba(0, 0, 0, 0.1);
            box-shadow: 6px 0 6px rgba(0, 0, 0, 0.1);
        }
        
    </style>
    <script type="text/javascript">

        COOKIE_MSID = "MFMSID";
        COOKIE_MUEID = "MFMUEID";

        function createCookie(cookieName, cookieValue, cookieExpiryMins) {
            if (cookieExpiryMins) {
                var date = new Date();
                date.setTime(date.getTime() + (cookieExpiryMins * 60 * 1000));
                var expires = "; expires=" + date.toUTCString();
            }
            else var expires = "";
            document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function eraseCookie(name) {
            createCookie(name, "", -1);
        }

        function setExpiryDateOfCookie() {
            var cookieValue = decodeURIComponent(readCookie(COOKIE_MSID));
            eraseCookie(COOKIE_MSID);
            createCookie(COOKIE_MSID, cookieValue, SESSION_TIMEOUT_MINUTES); //30 minutes
        }
        function BackToUiDetails() {
            $('[id$=lnkBackToUiDetails]').click();
        }

//        $(window).bind('beforeunload', function () {
//            clearBrowserWindowIdFromCookie();
//        });


        function clearBrowserWindowIdFromCookie() {
            if ($('[id$=hidIsCleanUpRequired]').val() !== 'false') {
                var cookieValue = decodeURIComponent(readCookie(COOKIE_MSID));
                var hidBid = $('[id$=hfbid]').val();
                var aryCookieValue = cookieValue.length > 0 ? cookieValue.split('|') : "";
                if (aryCookieValue.length == 3) {//no more window is open
                    eraseCookie(COOKIE_MSID);
                    return;
                }
                for (var i = 0; i <= aryCookieValue.length - 1; i++) {
                    if (hidBid == aryCookieValue[i]) {
                        aryCookieValue[i] = "";
                    }
                }
                cookieValue = "";
                for (var i = 0; i <= aryCookieValue.length - 1; i++) {
                    if (aryCookieValue[i] !== "") {
                        cookieValue += cookieValue.length == 0 ? aryCookieValue[i] : "|" + aryCookieValue[i]
                    }
                }
                eraseCookie(COOKIE_MSID);
                createCookie(COOKIE_MSID, cookieValue, SESSION_TIMEOUT_MINUTES);
            }
        }

        function isCookieCleanUpRequired(objectIdToFind) {
            $('[id$=hidIsCleanUpRequired]').val("false");
        }
        function ShowSchemaDropDown(_Hide) {
            if (_Hide) $('#qb-ui-tree-selects').hide();
            else $('#qb-ui-tree-selects').show();
        }
    </script>
</head>
<body style="overflow-x:none;overflow-y:auto;">
	<form id="Form1" runat="server">
        <div>
            <header>
            <div id="logo">
            </div>
            <div id="header">
                <div style="float:right;margin-top:5px;">
                    <div style="float:right;">
                         <%--<input id="Button1" type="button" value="button" onclick="showWait(true);" />
                         <input id="Button2" type="button" value="button" onclick="showWait(false);" />--%>
                         <asp:Button ID="btnBackToSimpleQueryBuilder" runat="server" CssClass="InputStyle" OnClientClick="btnBackClick();"  Text=" Back To Simple Query Editor "  OnClick="btnGenerate_Click"  />
                    </div>
            </div>
            </div>
        </header>
        </div>
        <div id="QbcDiv" style="height:730px;width:100%;display:block;max-width:1300">
		    <uc:QueryBuilderUC ID="queryBuilderControl" runat="server" EnableTheming="true" />
        </div>
        <div id="spinDiv" style="margin-top:5px;margin-left:4px;display:none;" class="spinDiv">
            <%--<div class="spinDivInner">--%>
                <img src="//enterprise.mficient.com/css/images/icons/dark/progress.gif" />
           <%-- </div>--%>
        </div>

        <div>
            <asp:HiddenField ID="hfs" runat="server" />
            <asp:HiddenField ID="hfSql" runat="server" />
            <asp:HiddenField ID="hfSqlPara" runat="server" />
            <asp:HiddenField ID="hfOutCol" runat="server" />
            <asp:HiddenField ID="hdfInputParam" runat="server" />
            <asp:HiddenField ID="hfBackSql" runat="server" />
            <asp:HiddenField ID="hfBackSqlPara" runat="server" />
            <asp:HiddenField ID="hfBackOutCol" runat="server" />
            <asp:HiddenField ID="hdfBackInputParam" runat="server" />
            <asp:HiddenField ID="hfbid" runat="server" />
            <asp:HiddenField ID="hidIsCleanUpRequired" runat="server" />
            <asp:HiddenField ID="hdfIsMplugIn" runat="server" />
            <asp:HiddenField ID="hdfDbType" runat="server" />
            <asp:HiddenField ID="hdfMannualParam" runat="server" />            
            <asp:HiddenField ID="hdobjectname" runat="server" />
            <asp:HiddenField ID="hdiscription" runat="server" />
            <asp:HiddenField ID="hidWsAuthencationMeta" runat="server" />
        </div>
        <div>
            <div id="shadow" class="opaqueLayer"> </div>
            <div id="question" class="questionLayer">
                <img src="//enterprise.mficient.com/css/images/icons/dark/progress.gif" />
            </div>

        </div>
	</form>
    <script type="text/javascript">
        $(window).bind('beforeunload', function () {
            clearBrowserWindowIdFromCookie();
        });
    </script>
</body>
</html>
