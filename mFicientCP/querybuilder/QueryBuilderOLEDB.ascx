<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QueryBuilderOLEDB.ascx.cs" Inherits="mFicientCP.QueryBuilderOLEDB" %>
<%@ Register Assembly="ActiveDatabaseSoftware.ActiveQueryBuilder.Web.Control" Namespace="ActiveDatabaseSoftware.ActiveQueryBuilder.Web.Control" TagPrefix="AQB" %>
<AQB:QueryBuilderControl ID="qbc" runat="server" OnInit="qbc_Init" onsyntaxproviderchanged="qbc_SyntaxProviderChanged" />
<div id="all">
	<%--<div class="header">
	</div>--%>
	<div id="content-container">
		<div id="qb-ui">
			<AQB:ObjectTree ID="qbcObjectTree" runat="server" ShowFields="false" 
                ondatabinding="qbcObjectTree_DataBinding" onload="qbcObjectTree_Load" 
                onprerender="qbcObjectTree_PreRender" onunload="qbcObjectTree_Unload"/>
			<div id="center">
				<AQB:Canvas ID="qbcCanvas" runat="server" />
				<AQB:Grid ID="qbcGrid" runat="server" />
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
    
	<AQB:SqlEditor ID="qbcSQLEditor" runat="server"   />
	<div class="foot">
	</div>
</div>