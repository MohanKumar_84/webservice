﻿<%@ Page Title="mFicient | Device Manager" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="registerDevice.aspx.cs" Inherits="mFicientCP.registerDevice" %>

<%@ Register TagPrefix="userDetails" TagName="Details" Src="~/UserControls/UserDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function makeTabAfterPostBack() {
            $('form').find('div.tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        }

        function hidePendingTab(hide) {
            var lnkPending = $('#lnkTabPending');
            var lstPending = $('#lstTabPending');
            if (hide) {
                lnkPending.addClass("hideElement");
                lstPending.addClass("hideElement");
            }
            else {
                lnkPending.removeClass("hideElement").addClass("showElement");
                lstPending.removeClass("hideElement").addClass("showElement");
            }
        }

        function changeRegisteredTabText(tabForProcessing) {
            if (tabForProcessing) {
                $('#lnkTabRegistered').text('Registered Devices');
            }
            else {
                $('#lnkTabRegistered').text('Registered');
            }
        }
        function hideCompleteTab(hide) {
            if (hide) {
                $('.tab').find($('.ui-tabs-nav')).hide();
            }
            else {
                $('.tab').find($('.ui-tabs-nav')).show();
            }
        }



        REGISTERED_DEVICES_COLUMNS =
        {
            NAME: 1,
            OS: 2,
            RequestDate: 3
        }
        PENDING_DEVICES_COLUMNS =
        {
            NAME: 1,
            OS: 2,
            Devices: 3,
            RequestDate: 4,
            RequestType: 5
        }
        REPEATER_NAME =
       {
           RegisteredDevices: 1,
           PendingRequests: 2
       }
        SORT_TYPE =
       {
           ASC: 0,
           DESC: 1,
           NONE: 3
       }
        function getIdOfTableToFind(RepeaterName) {
            var idOfTable = "";
            switch (RepeaterName) {
                case REPEATER_NAME.RegisteredDevices:
                    idOfTable = "tblRegisteredDevices";
                    break;
                case REPEATER_NAME.PendingRequests:
                    idOfTable = "tblPendingRequest";
            }
            return idOfTable;
        }
        function getSortTypeClass(sortType) {
            var clsName = "";
            switch (parseInt(sortType)) {
                case SORT_TYPE.ASC:
                    clsName = "sortable_asc";
                    break;
                case SORT_TYPE.DESC:
                    clsName = "sortable_desc";
            }

            return clsName;
        }
        function changeImageOfSortedCol() {
            var hidSortingDtl = document.getElementById('<%=hidSortingDetail.ClientID %>');
            var hidCurrentSort = document.getElementById('<%=hidCurrentSortOrder.ClientID %>');
            var arySortingDtl = "";
            if ($(hidSortingDtl).val() == "") {
                arySortingDtl = $(hidCurrentSort).val().split(',');
            }
            else {
                //split will give repeatername,columnIndex,sorttype
                arySortingDtl = $(hidSortingDtl).val().split(',');
            }
            var className = getSortTypeClass($.trim(arySortingDtl[2]));
            $($('#' + getIdOfTableToFind(parseInt($.trim(arySortingDtl[0])))).find('thead tr th')[parseInt($.trim(arySortingDtl[1]))]).addClass(className);
        }
        function setInfoReqAfterPostBackForSorting(header, columnIndex, rptName) {
            var hidSortingDtl = document.getElementById('<%=hidSortingDetail.ClientID %>');
            var sortType = $(header).hasClass('sortable_asc') ? SORT_TYPE.DESC : SORT_TYPE.ASC
            $(hidSortingDtl).val(rptName + "," + columnIndex + "," + sortType);
        }
        function clearSortingDtlHiddenField() {
            var hidSortingDtl = document.getElementById('<%=hidSortingDetail.ClientID %>');
            var hidCurrentSort = document.getElementById('<%=hidCurrentSortOrder.ClientID %>');
            if ($(hidSortingDtl).val() != "")
                $(hidCurrentSort).val($(hidSortingDtl).val());
            $(hidSortingDtl).val("");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updRepeater" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <asp:Panel ID="pnlInfoAndSelection" runat="server">
                    <div class="widget" id="divInfoAndSelectionWidget">
                        <div class="customWidgetDirectDiv">
                            <div class="g12" style="padding: 1px">
                                <div class="g4" style="margin-left: 0px; position: relative; left: -4px;">
                                    <table class="customMargin">
                                        <tr>
                                            <th>
                                                Max Allowed Registration
                                            </th>
                                            <td>
                                                <asp:Label ID="lblMaxRegisteredDev" runat="server">0</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="g4">
                                    <table class="customMargin">
                                        <tr>
                                            <th>
                                                Registered Devices
                                            </th>
                                            <td>
                                                <asp:Label ID="lblApprovedDevice" runat="server">0</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="g4">
                                    <table class="customMargin">
                                        <tr>
                                            <th>
                                                Pending Requests
                                            </th>
                                            <td>
                                                <asp:Label ID="lblPendingRequest" runat="server">0</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div style="clear: both;">
                            <div id="divSearchError"></div>
                            </div>
                            <div class="searchRow g12">
                                
                                <div class="g5 radioButtonList">
                                    <asp:RadioButtonList ID="radListSelection" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatLayout="Table" OnSelectedIndexChanged="radListSelection_SelectedIndexChanged1">
                                        <asp:ListItem Text="Registered Devices" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Pending Requests" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                
                                <div class="g5" style="float: right; padding: 0px; margin: 0px;">
                                    <div style="float: right; width: 70%;" class="searchImageCont">
                                        <asp:LinkButton ID="lnkImageSearch" ToolTip="search" Style="margin: 5px 4px 0px 4px;"
                                            CssClass="searchLink16by16 fr" OnClientClick="isCookieCleanUpRequired('false');"
                                            OnClick="lnkImageSearch_Click" runat="server"></asp:LinkButton>
                                        &nbsp;
                                        <asp:TextBox ID="txtFilterSearch" runat="server" Width="80%" Style="float: right"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="divInfomationForApproveDisable">
                            </div>
                            <div id="divRepeater" style="margin: 5px;">
                                <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px; float: right;">
                                            <asp:LinkButton ID="lnkRefreshList" runat="server" Text="Refresh" CssClass="repeaterLink fr"
                                                OnClick="lnkRefreshList_Click" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                        </div>
                                        <div style="height: 0px; clear: both">
                                        </div>
                                    </asp:Panel>
                                    <asp:Repeater ID="rptRegisteredDeviceDtls" runat="server" OnItemDataBound="rptRegisteredDeviceDtls_ItemDataBound"
                                        OnItemCommand="rptRegisteredDeviceDtls_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="tblRegisteredDevices" class="repeaterTable" style="table-layout: fixed;
                                                overflow: scroll">
                                                <thead class="sortable">
                                                    <tr>
                                                        <th style="display: none">
                                                            USER_ID
                                                        </th>
                                                        <th onclick="setInfoReqAfterPostBackForSorting(this,REGISTERED_DEVICES_COLUMNS.NAME,REPEATER_NAME.RegisteredDevices);rptHeaderClickForSorting(this);">
                                                            Name
                                                        </th>
                                                        <th style="width: 350px;" onclick="setInfoReqAfterPostBackForSorting(this,REGISTERED_DEVICES_COLUMNS.OS,REPEATER_NAME.RegisteredDevices);rptHeaderClickForSorting(this);">
                                                            OS / Model / App Version
                                                        </th>
                                                        <th onclick="setInfoReqAfterPostBackForSorting(this,REGISTERED_DEVICES_COLUMNS.RequestDate,REPEATER_NAME.RegisteredDevices);rptHeaderClickForSorting(this);">
                                                            Registered On
                                                        </th>
                                                        <th style="width: 55px;" class="notClickable">
                                                        </th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tbody>
                                                <tr class="repeaterItem">
                                                    <td style="display: none">
                                                        <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                        <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkName" runat="server" CommandName="Details" ToolTip="Details"
                                                            Text='<%# Eval("FULL_NAME") %>' OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                            ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                                ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label>
                                                                &nbsp;/&nbsp;
                                                                <asp:Label ID="lblappversion" runat="server" Text='<%# Eval("APP_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRegistrationDate" runat="server" Text='<%# Eval("REGISTRATION_DATE") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="repeaterLinkImageButton repeaterLink imageCross"
                                                            CommandName="Delete" ToolTip="Delete" OnClientClick="return showConfirmation('Are you sure you want to delete this device');"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tbody>
                                                <tr class="repeaterAlternatingItem">
                                                    <td style="display: none">
                                                        <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                        <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkName" runat="server" CommandName="Details" ToolTip="Details"
                                                            Text='<%# Eval("FULL_NAME") %>' OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                            ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                                ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label>
                                                                &nbsp;/&nbsp;
                                                                <asp:Label ID="lblappversion" runat="server" Text='<%# Eval("APP_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRegistrationDate" runat="server" Text='<%# Eval("REGISTRATION_DATE") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="repeaterLinkImageButton repeaterLink imageCross"
                                                            CommandName="Delete" ToolTip="Delete" OnClientClick="return showConfirmation('Are you sure you want to delete this device');"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Repeater ID="rptPendingRequest" runat="server" OnItemDataBound="rptPendingRequest_ItemDataBound"
                                        OnItemCommand="rptPendingRequest_ItemCommand">
                                        <HeaderTemplate>
                                            <table id="tblPendingRequest" class="repeaterTable">
                                                <thead class="sortable">
                                                    <tr>
                                                        <th style="display: none">
                                                            USER_ID
                                                        </th>
                                                        <th onclick="setInfoReqAfterPostBackForSorting(this,PENDING_DEVICES_COLUMNS.NAME,REPEATER_NAME.PendingRequests);rptHeaderClickForSorting(this);">
                                                            Name
                                                        </th>
                                                        <th onclick="setInfoReqAfterPostBackForSorting(this,PENDING_DEVICES_COLUMNS.OS,REPEATER_NAME.PendingRequests);rptHeaderClickForSorting(this);">
                                                            OS / Model / App Version
                                                        </th>
                                                        <th style="width:55px;" class="notClickable">
                                                            Devices <span style="position: relative; top: -1px">*</span>
                                                        </th>
                                                        <th style="width:90px;" onclick="setInfoReqAfterPostBackForSorting(this,PENDING_DEVICES_COLUMNS.RequestDate,REPEATER_NAME.PendingRequests);rptHeaderClickForSorting(this);">
                                                            Request Date
                                                        </th>
                                                        <th style="width:90px;" onclick="setInfoReqAfterPostBackForSorting(this,PENDING_DEVICES_COLUMNS.RequestType,REPEATER_NAME.PendingRequests);rptHeaderClickForSorting(this);">
                                                            Request Type
                                                        </th>
                                                        <th style="width: 55px;" class="notClickable">
                                                        </th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tbody>
                                                <tr class="repeaterItem">
                                                    <td style="display: none">
                                                        <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                        <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                         &nbsp;/&nbsp;
                                                                <asp:Label ID="lblappversion" runat="server" Text='<%# Eval("APP_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkName" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                                                            CommandName="Details"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                            ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                                ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label><asp:Label
                                                                    ID="lblItemDoubleAsterix" runat="server" Text="**" Visible="false" Style="position: relative;
                                                                    top: -2px; margin-left: 2px;"></asp:Label>
                                                                     &nbsp;/&nbsp;
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("APP_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkCntRegisteredPendingDevices" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                                                            CommandName="DetailsOfDevices"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRequestDate" runat="server" Text='<%# Eval("REQUEST_DATETIME") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRequestType" runat="server" Text='<%# Eval("REQUEST_DESCRIPTION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkApprove" runat="server" CssClass="repeaterLink repeaterLinkImageButton imageTick"
                                                            CommandName="Approve" ToolTip="Approve" CommandArgument='<%# Eval("REQUEST_TYPE") %>'
                                                            OnClientClick="return showConfirmation('Are you sure you want to approve this request');"></asp:LinkButton>
                                                        &nbsp;<asp:LinkButton ID="lnkDeny" runat="server" CommandName="Deny" CommandArgument='<%# Eval("DenyId") %>'
                                                            ToolTip="Deny" CssClass="repeaterLink repeaterLinkImageButton imageCross" Style="margin-left: 10px;"
                                                            OnClientClick="return showConfirmation('Are you sure you want to deny this request');"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tbody>
                                                <tr class="repeaterAlternatingItem">
                                                    <td style="display: none">
                                                        <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                        <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkName" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                                                            CommandName="Details"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                            ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                                ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label><asp:Label
                                                                    ID="lblItemDoubleAsterix" runat="server" Text="**" Visible="false" Style="position: relative;
                                                                    top: -2px; margin-left: 2px;"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkCntRegisteredPendingDevices" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                                                            CommandName="DetailsOfDevices"></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRequestDate" runat="server" Text='<%# Eval("REQUEST_DATETIME") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRequestType" runat="server" Text='<%# Eval("REQUEST_DESCRIPTION") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkApprove" runat="server" CssClass="repeaterLink repeaterLinkImageButton imageTick"
                                                            CommandName="Approve" ToolTip="Approve" CommandArgument='<%# Eval("REQUEST_TYPE") %>'
                                                            OnClientClick="return showConfirmation('Are you sure you want to approve this request');"></asp:LinkButton>
                                                        &nbsp;<asp:LinkButton ID="lnkDeny" runat="server" CommandName="Deny" CommandArgument='<%# Eval("DenyId") %>'
                                                            ToolTip="Deny" CssClass="repeaterLink repeaterLinkImageButton imageCross" Style="margin-left: 10px;"
                                                            OnClientClick="return showConfirmation('Are you sure you want to deny this request');"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                            <div style="font-style: italic; margin: 2px 0 0 5px; height: auto">
                                                * Registered/Pending Devices</div>
                                            <div style="font-style: italic; margin: 5px 0 0 5px; height: auto">
                                                <asp:Label ID="lblDoubleAsterixInfo" runat="server" Visible="false"></asp:Label></div>
                                            <div style="font-style: italic; margin: 5px 0 0 7px; height: auto">
                                                Note : Approval is disabled for requests, if the user has reached his max device
                                                registration.</div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <div>
                                        <asp:HiddenField ID="hidDesignationIdForEdit" runat="server" />
                                        <asp:HiddenField ID="hidPopUpMode" runat="server" />
                                        <asp:HiddenField ID="hidQueryStrFromEmail" runat="server" />
                                        <asp:HiddenField ID="hidDeviceToApprove" runat="server" />
                                        <asp:HiddenField ID="hidPendingDoubleAsterix" runat="server" />
                                        <asp:HiddenField ID="hidSortingDetail" runat="server" />
                                        <asp:HiddenField ID="hidCurrentSortOrder" runat="server" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divUserDtlsModal" style="display: none;">
        <asp:UpdatePanel runat="server" ID="updUserDtlsModal" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="ProcImgUpd">
                    <div id="divUserDetailUC">
                        <userDetails:Details ID="ucUserDetails" runat="server" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divModalRegisteredPendingDevicesDtls" style="display: none;">
        <asp:UpdatePanel ID="updRegtdPendingDtls" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divAlertStatus">
                </div>
                <div class="widget" id="widget_tabs" style="margin-top: 5px;">
                    <div class="tab">
                        <ul>
                            <li><a id="lnkTabRegistered" href="#Registered">Registered</a></li>
                            <li id="lstTabPending"><a id="lnkTabPending" href="#Pending">Pending</a></li>
                        </ul>
                        <div id="Registered" style="margin: 5px 5px 5px 3px;">
                            <asp:Panel ID="pnlRegisteredDevicesRptBox" CssClass="repeaterBox" runat="server"
                                Style="margin: 0px;">
                                <asp:Panel ID="pnlTabRegisteredDevicesHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblModalRegtredHdr" runat="server" Text="<h1>Registered Devices</h1>"></asp:Label>
                                    </div>
                                </asp:Panel>
                                <asp:Repeater ID="rptModalRegisteredDevicesDtls" runat="server" OnItemDataBound="rptModalRegisteredDevicesDtls_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable" style="table-layout: fixed; overflow: scroll">
                                            <thead>
                                                <tr>
                                                    <th style="display: none">
                                                        USER_ID
                                                    </th>
                                                    <th>
                                                        Device Type / Device Model
                                                    </th>
                                                    <th>
                                                        Device Id
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none">
                                                    <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceSize" runat="server" Text='<%# Eval("DEVICE_SIZE") %>'></asp:Label>
                                                    <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkRegisteredDevices" runat="server" />
                                                    <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                        ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                            ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDevId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none">
                                                    <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceSize" runat="server" Text='<%# Eval("DEVICE_SIZE") %>'></asp:Label>
                                                    <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkRegisteredDevices" runat="server" />
                                                    <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                        ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                            ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDevId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </div>
                        <div id="Pending" style="margin: 5px 5px 5px 3px;">
                            <asp:Panel ID="pnlPendingDevicesRptBox" CssClass="repeaterBox" runat="server" Style="margin: 0px;">
                                <asp:Panel ID="pnlTabPendingDevicesHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblModalPendingDevicesHeader" runat="server" Text="<h1>Pending Devices</h1>"></asp:Label>
                                    </div>
                                </asp:Panel>
                                <asp:Repeater ID="rptModalPendingRequestDtls" runat="server" OnItemDataBound="rptModalPendingRequestDtls_ItemDataBound"
                                    OnItemCommand="rptModalPendingRequestDtls_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable" style="table-layout: fixed; overflow: scroll">
                                            <thead>
                                                <tr>
                                                    <th style="display: none">
                                                        USER_ID
                                                    </th>
                                                    <th>
                                                        Device Type / Device Model
                                                    </th>
                                                    <th>
                                                        Device Id
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none">
                                                    <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceSize" runat="server" Text='<%# Eval("DEVICE_SIZE") %>'></asp:Label>
                                                    <asp:Label ID="lblFullName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                        ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                            ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDevId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none">
                                                    <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblDeviceSize" runat="server" Text='<%# Eval("DEVICE_SIZE") %>'></asp:Label>
                                                    <asp:Label ID="lblFullName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblPushMsgId" runat="server" Text='<%# Eval("DEVICE_PUSH_MESSAGE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblOSVersion" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDeviceType" runat="server" Text='<%# Eval("DEVICE_TYPE") %>'></asp:Label>&nbsp;<asp:Label
                                                        ID="lblOSVer" runat="server" Text='<%# Eval("OS_VERSION") %>'></asp:Label>&nbsp;/&nbsp;<asp:Label
                                                            ID="lblDeviceModel" runat="server" Text='<%# Eval("DEVICE_MODEL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDevId" runat="server" Text='<%# Eval("DEVICE_ID") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
                <div id="modalButton" class="modalButton">
                    <asp:Button ID="btnDelete" runat="server" Text="Proceed" OnClientClick="return confirm('Are you sure you want to delete selected devices');"
                        OnClick="btnDelete_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="updRowPostBack" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRowClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnRowClickPostBack" runat="server" OnClick="btnRowClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            changeImageOfSortedCol();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>
