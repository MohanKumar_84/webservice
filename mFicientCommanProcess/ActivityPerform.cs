﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using mFicientCommonProject;
using Newtonsoft.Json.Linq;


namespace mFicientCommonProcess
{
    public class SaveActivityLog
    {

        private const string SPACE_SEPARATOR = " ";
        public static void ActivityPerform(SqlConnection con, List<Activity> _lst, SqlTransaction trn)
        {
            try
            {
                string sqlquery = @"insert into dbo.TBL_ENTERPRISE_ACTIVITY_LOG(ACTIVITY_TYPE,COMPANY_ID,ACTIVITY_DATETIME,PARA0,PARA1,PARA2,PARA3,PARA4,PARA5,PARA6,PARA7,PARA8,PARA9)
                                 VALUES(@ACTIVITY_TYPE,@COMPANY_ID,@ACTIVITY_DATETIME,@PARA0,@PARA1,@PARA2,@PARA3,@PARA4,@PARA5,@PARA6,@PARA7,@PARA8,@PARA9)";
                int result = 0;
                SqlCommand cmd;
                foreach (Activity item in _lst)
                {
                    if(trn==null)
                        cmd = new SqlCommand(sqlquery, con);
                    else
                        cmd = new SqlCommand(sqlquery, con, trn);
                    cmd.Parameters.AddWithValue("@ACTIVITY_TYPE", item.Activity_Type);
                    cmd.Parameters.AddWithValue("@COMPANY_ID", item.Company_Id);
                    cmd.Parameters.AddWithValue("@ACTIVITY_DATETIME", item.Activity_date);
                    cmd.Parameters.AddWithValue("@PARA0", item.Parameter0);
                    cmd.Parameters.AddWithValue("@PARA1", item.Parameter1);
                    cmd.Parameters.AddWithValue("@PARA2", item.Parameter2);
                    cmd.Parameters.AddWithValue("@PARA3", item.Parameter3);
                    cmd.Parameters.AddWithValue("@PARA4", item.Parameter4);
                    cmd.Parameters.AddWithValue("@PARA5", item.Parameter5);
                    cmd.Parameters.AddWithValue("@PARA6", item.Parameter6);
                    cmd.Parameters.AddWithValue("@PARA7", item.Parameter7);
                    cmd.Parameters.AddWithValue("@PARA8", item.Parameter8);
                    cmd.Parameters.AddWithValue("@PARA9", item.Parameter9);
                    result = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GetActivityLog(SqlConnection con, string Companyid,  long todate, long fromdate, string subadmin, int Category, out DataTable dtSubsource,String TopRecord)
        {
            try
            {
                StringBuilder sbLog;
                TimeZoneInfo tzi = CompanyTimezone.getTimezoneInfo(Companyid, con);
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("COMPANY_ID");
                dt.Columns.Add("SUBADMIN_ID");
                dt.Columns.Add("LOG_ID");
                dt.Columns.Add("LOG");
                dt.Columns.Add("DATE");
                dt.Columns.Add("EXTRA");

                string sqlquery = @"SELECT " + TopRecord + @" E.*,isnull(A.FULL_NAME,'') as FULL_NAME FROM dbo.TBL_ENTERPRISE_ACTIVITY_LOG AS E 
                                  LEFT OUTER  JOIN  dbo.TBL_SUB_ADMIN AS A ON E.PARA0=A.SUBADMIN_ID and E.COMPANY_ID=A.COMPANY_ID 
                                  WHERE E.ACTIVITY_DATETIME BETWEEN  @Fromdate AND  @Todate ";

                if (!string.IsNullOrEmpty(Companyid)) sqlquery +=  " and E.COMPANY_ID = '" + Companyid + "'";
                if (!string.IsNullOrEmpty(subadmin)) sqlquery +=  " and E.PARA0='" + subadmin + "' ";
                if (Category > 0) sqlquery += " and ACTIVITY_TYPE='" + Category.ToString() + "' ";
                sqlquery += @" order by ACTIVITY_DATETIME DESC";
                
                SqlCommand cmd = new SqlCommand(sqlquery);
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@Todate", todate);
                cmd.Parameters.AddWithValue("@Fromdate", fromdate);

                DataTable dresult = new DataTable();
                da.Fill(dresult);
                ACTIVITYENUM actiontype;
                string strSubAdminFullName = "",extra="";

                if (dresult.Rows.Count > 0)
                {
                    foreach (DataRow row in dresult.Rows)
                    {
                        sbLog = new StringBuilder();
                        DataRow newrow = dt.NewRow();
                        
                        actiontype =(ACTIVITYENUM)Convert.ToInt32(row["ACTIVITY_TYPE"]);
                        strSubAdminFullName = Convert.ToString(row["FULL_NAME"]);
                        if (string.IsNullOrEmpty(strSubAdminFullName)) strSubAdminFullName="Auto";
                        switch (actiontype)
                        {
                            case ACTIVITYENUM.APP_SAVE_MODIFIED:
                                sbLog.Append(strSubAdminFullName).Append(" saved an app : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.APP_PUBLISH:
                                sbLog.Append(strSubAdminFullName).Append(" published an app : ").Append(Convert.ToString(row["PARA2"])).Append("("+Convert.ToString(row["PARA3"])+")");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.APP_ROLLBACK:
                                sbLog.Append(strSubAdminFullName).Append(" unpublished an app : ").Append(Convert.ToString(row["PARA2"])).Append("("+Convert.ToString(row["PARA3"])+")");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.APP_DELETE:
                                sbLog.Append(strSubAdminFullName).Append(" deleted an app : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.CATEGORY_ADDED:
                                sbLog.Append(strSubAdminFullName).Append(" added a category : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.CATEGORY_RENAMED:
                               sbLog.Append(strSubAdminFullName).Append(" remaned a category : ").Append(Convert.ToString(row["PARA3"])+" to ").Append(Convert.ToString(row["PARA2"]));
                               extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.CATEGORY_REMOVED:
                                sbLog.Append(strSubAdminFullName).Append(" deleted a category : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.CATEGORY_MODIFIED_APPADDED:
                                sbLog.Append(strSubAdminFullName).Append(" added app(s) (" + Convert.ToString(row["PARA8"]) + ") in category : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.CATEGORY_MODIFIED_APP_DELETED:
                                sbLog.Append(strSubAdminFullName).Append(" deleted app(s) (" + Convert.ToString(row["PARA8"]) + ") from category : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_ADDED:
                                sbLog.Append(strSubAdminFullName).Append(" added a group : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_RENAMED:
                                sbLog.Append(strSubAdminFullName).Append(" remaned a group : ").Append(Convert.ToString(row["PARA3"]) + " to ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_REMOVED:
                                sbLog.Append(strSubAdminFullName).Append(" deleted a group : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_MODIFIED_APP_ADDED:
                                sbLog.Append(strSubAdminFullName).Append(" added app(s) (" + Convert.ToString(row["PARA8"]) + ") in group : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_MODIFIED_APP_DELETED:
                                sbLog.Append(strSubAdminFullName).Append(" deleted app(s) (" + Convert.ToString(row["PARA8"]) + ") from group : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_MODIFIED_USER_ADDED:
                                 sbLog.Append(strSubAdminFullName).Append(" added user(s) (" + Convert.ToString(row["PARA8"]) + ") in group : ").Append(Convert.ToString(row["PARA2"]));
                                 extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.GROUP_MODIFIED_USER_DELETED:
                                sbLog.Append(strSubAdminFullName).Append(" deleted user(s) (" + Convert.ToString(row["PARA8"]) + ") from group : ").Append(Convert.ToString(row["PARA2"]));
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.DEVICE_APPROVED:
                                sbLog.Append(strSubAdminFullName).Append(" approved a device '").Append(Convert.ToString(row["PARA6"])).Append("' of ").Append(Convert.ToString(row["PARA3"]));
                                extra = Convert.ToString(row["PARA1"])+"," + Convert.ToString(row["PARA4"]);
                                break;
                            case ACTIVITYENUM.DEVICE_DENY:
                                sbLog.Append(strSubAdminFullName).Append(" denied a device '").Append(Convert.ToString(row["PARA6"])).Append("' of ").Append(Convert.ToString(row["PARA3"]));
                                extra = Convert.ToString(row["PARA1"]) + "," + Convert.ToString(row["PARA4"]);
                                break;
                            case ACTIVITYENUM.DEVICE_DELETE:
                                sbLog.Append(strSubAdminFullName).Append(" deleted a device '").Append(Convert.ToString(row["PARA6"])).Append("' of ").Append(Convert.ToString(row["PARA3"]));
                                extra = Convert.ToString(row["PARA1"]) + "," + Convert.ToString(row["PARA4"]);
                                break;
                            case ACTIVITYENUM.USER_CREATED:
                                sbLog.Append(strSubAdminFullName).Append(" created a user '").Append(Convert.ToString(row["PARA3"])+"'");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.USER_MODIFIED:
                                sbLog.Append(strSubAdminFullName).Append(" created a user '").Append(Convert.ToString(row["PARA3"]) + "'");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.USER_DELETE:
                                sbLog.Append(strSubAdminFullName).Append(" deleted a user '").Append(Convert.ToString(row["PARA3"]) + "'");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.USER_BLOCKED:
                                sbLog.Append(strSubAdminFullName).Append(" blocked a user '").Append(Convert.ToString(row["PARA3"]) + "'");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.USER_UNBLOCK:
                                sbLog.Append(strSubAdminFullName).Append(" unblocked a user '").Append(Convert.ToString(row["PARA3"]) + "'");
                                extra = Convert.ToString(row["PARA1"]);
                                break;
                            case ACTIVITYENUM.USER_LOGIN:
                                if (Convert.ToString(row["PARA5"]) == "0")
                                    sbLog.Append(Convert.ToString(row["PARA2"])).Append(" logged in by device '").Append(Convert.ToString(row["PARA7"])+"'");
                                else
                                    sbLog.Append(Convert.ToString(row["PARA2"])).Append(" logged in fail");
                                extra = Convert.ToString(row["PARA1"])+","+Convert.ToString(row["PARA4"])+","+Convert.ToString(row["PARA8"]);
                                break;
                            case ACTIVITYENUM.USER_LOGOUT:
                                sbLog.Append(Convert.ToString(row["PARA2"])).Append(" logged out by device '").Append(Convert.ToString(row["PARA6"]) + "'");
                                extra = Convert.ToString(row["PARA1"]) + "," + Convert.ToString(row["PARA4"]);
                                break;
                            case ACTIVITYENUM.SESSTION_RENEW:
                                sbLog.Append(Convert.ToString(row["PARA3"])).Append(" renewed session for device '").Append(Convert.ToString(row["PARA6"]) + "'");
                                extra = Convert.ToString(row["PARA1"]) + "," + Convert.ToString(row["PARA4"]) + "," + Convert.ToString(row["PARA7"]) + "," + Convert.ToString(row["PARA8"]);
                                break;
                            case ACTIVITYENUM.APP_USED:
                                sbLog.Append(Convert.ToString(row["PARA3"])).Append(" used a app " + Convert.ToString(row["PARA6"]) + " by device '").Append(Convert.ToString(row["PARA5"]) + "'");
                                extra = Convert.ToString(row["PARA1"]) + "," + Convert.ToString(row["PARA4"]) + "," + Convert.ToString(row["PARA7"]) + "," + Convert.ToString(row["PARA8"]);
                                break;
                            case ACTIVITYENUM.OBJECT_EXECUTE:
                                sbLog.Append(Convert.ToString(row["PARA3"])).Append(" executed a command " + Convert.ToString(row["PARA7"]) + " by device '").Append(Convert.ToString(row["PARA5"]) + "'");
                                extra = Convert.ToString(row["PARA1"]) + "," + Convert.ToString(row["PARA4"])+  "," + Convert.ToString(row["PARA6"]) + "," + Convert.ToString(row["PARA7"]) + "," + Convert.ToString(row["PARA8"]);
                                break;
                            case ACTIVITYENUM.PUSHMESSAGE_SEND:
                                sbLog.Append(strSubAdminFullName).Append(" send a mgram ");
                                break;
                            case ACTIVITYENUM.MPLUGIN_CONNECTED:
                                sbLog.Append("mPlugin '" + Convert.ToString(row["PARA2"]) + "' connected ");
                                break;
                            case ACTIVITYENUM.MPLUGIN_DISCONNECTED:
                                sbLog.Append("mPlugin '" + Convert.ToString(row["PARA2"]) + "' disconnected ");
                                break;
                            case ACTIVITYENUM.MPLUGIN_CREATE:
                                sbLog.Append("mPlugin '" + Convert.ToString(row["PARA2"]) + "' created ");
                                break;
                            case ACTIVITYENUM.MPLUGIN_DELETE:
                                sbLog.Append("mPlugin '" + Convert.ToString(row["PARA2"]) + "' deleted ");
                                break;
                            case ACTIVITYENUM.MPLUGIN_CHANGE:
                                sbLog.Append("mPlugin '" + Convert.ToString(row["PARA2"]) + "' authentication changed ");
                                break;
                            case ACTIVITYENUM.SUBADMIN_CREATED:
                                sbLog.Append("Subadmin '" + Convert.ToString(row["PARA2"]) + "' created ");
                                break;
                            case ACTIVITYENUM.SUBADMIN_MODIFIED:
                                sbLog.Append("Subadmin '" + Convert.ToString(row["PARA2"]) + "' modified ");
                                break;
                            case ACTIVITYENUM.SUBADMIN_DELETE:
                                sbLog.Append("Subadmin '" + Convert.ToString(row["PARA2"]) + "' deleted ");
                                break;
                            case ACTIVITYENUM.SUBADMIN_BLOCKED:
                                sbLog.Append("Subadmin '" + Convert.ToString(row["PARA2"]) + "' blocked ");
                                break;
                            case ACTIVITYENUM.SUBADMIN_UNBLOCK:
                                sbLog.Append("Subadmin '" + Convert.ToString(row["PARA2"]) + "' unblocked ");
                                break;
                            default:
                                break;
                        }
                        DataRow newRow = dt.NewRow();
                        newRow["COMPANY_ID"] = Convert.ToString(row["COMPANY_ID"]);
                        newRow["SUBADMIN_ID"] = Convert.ToString(row["PARA0"]);
                        newRow["LOG_ID"] = actiontype;
                        newRow["LOG"] = sbLog.ToString();
                        newRow["EXTRA"] = extra;
                        newRow["DATE"] = Utility.getCompanyLocalFormattedDateTime(tzi, Convert.ToInt64(row["ACTIVITY_DATETIME"]));
                        dt.Rows.Add(newRow);
                    }
                }
                dtSubsource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class Activity
    {
        public Activity(ACTIVITYENUM _activity_Type, string _company_Id, long _activity_date, string _parameter0, string _parameter1, string _parameter2,
            string _parameter3, string _parameter4, string _parameter5, string _parameter6, string _parameter7, string _parameter8,  SOURCE _source)
        {
            Activity_Type = (int)_activity_Type;
            Activity_date = _activity_date;
            Company_Id = _company_Id;
            Parameter0 = _parameter0;
            Parameter1 = _parameter1;
            Parameter2 = _parameter2;
            Parameter3 = _parameter3;
            Parameter4 = _parameter4;
            Parameter5 = _parameter5;
            Parameter6 = _parameter6;
            Parameter7 = _parameter7;
            Parameter8 = _parameter8;
            Parameter9 = ((int)_source).ToString();
        }
        public int Activity_Type { get; set; }

        public long Activity_date { get; set; }

        public string Company_Id { get; set; }

        public string Parameter1 { get; set; }

        public string Parameter2 { get; set; }

        public string Parameter3 { get; set; }

        public string Parameter4 { get; set; }

        public string Parameter5 { get; set; }

        public string Parameter6 { get; set; }

        public string Parameter7 { get; set; }

        public string Parameter8 { get; set; }

        public string Parameter9 { get; set; }

        public string Parameter0 { get; set; }
    }
}
