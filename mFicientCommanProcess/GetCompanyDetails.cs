﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCommonProject
{
    class GetCompanyDetails
    {
        public GetCompanyDetails(string companyid, SqlConnection con)
        {
            this.CompanyId = companyid;
            Process(con);
        }


        public void Process(SqlConnection connnection)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select * from TBL_COMPANY_DETAIL where COMPANY_ID=@COMPANY_ID;", connnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    this.ResultTable = dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string CompanyId
        {
            get;
            set;
        }
        public DataTable ResultTable
        {
            get;
            set;
        }
    }

  
}
