﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCommonProcess
{
    public class MgramDeviceDelete
    {
        public static void deleteUserAllDevices(SqlConnection con, string EnterpriseId, string UserName)
        {
            SqlCommand cmd = new SqlCommand(@"DELETE FROM TBL_USER_DEVICE
                                WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                                AND USERNAME = @USER_NAME", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", EnterpriseId);
            cmd.Parameters.AddWithValue("@USER_NAME", UserName);
            cmd.ExecuteNonQuery();
        }
        static void deleteUserOneDevice(SqlConnection con, string EnterpriseId, string UserName, string DeviceId, string DeviceType)
        {
            SqlCommand cmd = new SqlCommand(@"DELETE FROM TBL_USER_DEVICE
                                WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                                AND DEVICE_ID = @DEVICE_ID
                                AND DEVICE_TYPE = @DEVICE_TYPE
                                AND USERNAME = @USER_NAME;", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", EnterpriseId);
            cmd.Parameters.AddWithValue("@DEVICE_ID", DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", DeviceType);
            cmd.Parameters.AddWithValue("@USER_NAME", UserName);
            cmd.ExecuteNonQuery();
        }
    }
}
