﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace mFicientCommonProcess
{
    public enum NOTIFICATION_TYPE
    {
        MF_USER_BLOCK = 0,
        MF_DEVICE_REJECT = 1,
        MF_DEVICE_APPRV = 2,
        //MB_NEWMESSAGE = 3,
        //MB_ADD_CONTACT = 4,
        MF_MGRAM = 5,
        MF_CHANGE_PWD = 6,
        //MB_NEW_FILE = 7,
        MENU_CHANGE = 8,
        CUSTOM = 9
    }

    public class Utility
    {
        public static string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }

        //public static string UserDeletedorBlockedRequest(string UserName, string type, string Company_id)
        //{
        //    JObject jo = new JObject();
        //    jo.Add("mrid", GetMsgRefID());
        //    jo.Add("unm", UserName);
        //    jo.Add("type", type);
        //    jo.Add("enid", Company_id);
        //    JObject joreq = new JObject();
        //    joreq.Add("req", jo);
        //    return joreq.ToString();
        //}
        //public static string AddNewContactRequest(string _msgRefId, DataSet dsUserDetails, string Company_id, string Contacts, string type)
        //{
        //    JObject joUser = new JObject();
        //    joUser.Add("unm", Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]));
        //    joUser.Add("fnm", Convert.ToString(dsUserDetails.Tables[0].Rows[0]["FIRST_NAME"]) + " " + Convert.ToString(dsUserDetails.Tables[0].Rows[0]["LAST_NAME"]));
        //    joUser.Add("dep", Convert.ToString(dsUserDetails.Tables[0].Rows[0]["Departments"]));
        //    joUser.Add("des", Convert.ToString(dsUserDetails.Tables[0].Rows[0]["DESIGNATION_NAME"]));
        //    joUser.Add("em", Convert.ToString(dsUserDetails.Tables[0].Rows[0]["EMAIL_ID"]));
        //    joUser.Add("statu", "1");

        //    JObject jo = new JObject();
        //    jo.Add("mrid", GetMsgRefID());
        //    jo.Add("cntcs", Contacts);
        //    jo.Add("type", type);
        //    jo.Add("unm", Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]));
        //    jo.Add("user", joUser);
        //    jo.Add("enid", Company_id);
        //    JObject joreq = new JObject();
        //    joreq.Add("req", jo);
        //    return joreq.ToString();
        //}

        public static string GetMsgRefID()
        {
            Random random = new Random();
            string strMsgRefID = random.Next(989, 9999999).ToString() + DateTime.Now.Ticks.ToString();
            return strMsgRefID;
        }

        #region Format date according to timezone
        public static string getFormattedDateForUI(DateTime date)
        {
            return date.ToString("dd-MMM-yyyy");
        }

        public static string getFormattedDateTimeForUI(DateTime date)
        {
            return date.ToString("dd-MMM-yyyy HH:mm:ss");
        }

        public static string getFullFormattedDateForUI(DateTime date)
        {
            return date.ToString("F",
                  System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }

        public static string getFormattedDateForUI(
            string dateString,
            char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            DateTime dtSelected =
                new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0])
                );
            return getFormattedDateForUI(dtSelected);
        }

        public static string getCompanyLocalFormattedDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return getFormattedDateForUI(dtInLocalTimezone);
        }
        public static string getCompanyLocalFormattedDateTime(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return getFormattedDateTimeForUI(dtInLocalTimezone);
        }
        public static string getFullCompanyLocalFormattedDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return getFullFormattedDateForUI(dtInLocalTimezone);
        }

        public static DateTime getTimeInUtc(string dateString,
            int hrs,
            int min,
            int sec,
            char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            return new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0]),
                hrs,
                min,
                sec
                ).ToUniversalTime();
        }
        public static DateTime getCompanyLocalDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return dtInLocalTimezone;
        }
        #endregion
    }
}
