﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCommonProcess
{
    public class getUserDetail
    {
        string UserId, CompanyId;
        SqlConnection con ;
        bool isDevice;
        public getUserDetail(string _userId, string _companyId, SqlConnection _con,bool _isDevice)
        {
            UserId = _userId;
            CompanyId = _companyId;
            con = _con;
            isDevice =_isDevice;
        }
        public getUserDetail(string _companyId, SqlConnection _con)
        {
            CompanyId = _companyId;
            con = _con;
        }
        public DataSet Process()
        {
            DataSet dsUserDtls;
            try
            {
                string strQuery = @"SELECT userDtl.*, ISNULL( usrDivision.DIVISION_NAME,'') as DIVISION_NAME,ISNULL(usrDept.DEPARTMENT_NAME,'') as DEPARTMENT_NAME,
                             ISNULL(usrLoc.LOCATION_NAME,'') as LOCATION_NAME,ISNULL(usrCompany.COMPANY_NAME,'') as COMPANY_NAME,ISNULL(usrDesignation.DESIGNATION_NAME,'') as DESIGNATION_NAME,
                             ISNULL(usrRegion.REGION_NAME,'') as REGION_NAME,ISNULL(usrDivision.DIVISION_code,'') as DIVISION_code,ISNULL(usrDept.DEPARTMENT_code,'') as DEPARTMENT_code,
                             ISNULL(usrLoc.LOCATION_code,'') as LOCATION_code,ISNULL(usrDesignation.DESIGNATION_code,'') as DESIGNATION_code,ISNULL(usrRegion.REGION_code,'') as REGION_code ,
							 ISNULL(sadmin.[SUBADMIN_ID],'') as SUBADMIN_ID
                             FROM tbl_user_detail as userDtl left outer JOIN tbl_user_division_dep as usrDivisionDep ON userDtl.user_id = usrDivisionDep.user_id
                             left outer JOIN tbl_division as usrDivision ON usrDivisionDep.division_id = usrDivision.division_id
                             left outer JOIN tbl_department as usrDept ON usrDept.department_id = usrDivisionDep.department_id
                             left outer JOIN tbl_location as usrLoc ON userDtl.location_id = usrLoc.location_id
                             inner JOIN tbl_company_detail as usrCompany ON usrCompany.company_id = userDtl.company_id
                             left outer JOIN tbl_designation as usrDesignation ON usrDesignation.designation_id = userDtl.designation_id
                             left outer JOIN tbl_region as usrRegion ON usrRegion.region_id = userDtl.region_id
                             left outer join tbl_sub_admin as sadmin on sadmin.MOBILE_USER = userDtl.user_ID
                             WHERE userDtl.USER_ID = @USER_ID AND  userDtl.COMPANY_ID=@COMPANY_ID;";
                if (isDevice) strQuery += @"select r.* from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id  where u.USER_ID=@USER_ID and u.company_id=@COMPANY_ID;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.CommandType = CommandType.Text;
                dsUserDtls = msSqlClient.SelectDataFromSQlCommand(cmd, con, null);
                return dsUserDtls;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet AllUserDetailProcess()
        {
            DataSet dsUserDtls;
            try
            {
                string strQuery = @"select r.*,u.* from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id  where u.company_id=@COMPANY_ID;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.CommandType = CommandType.Text;
                dsUserDtls = msSqlClient.SelectDataFromSQlCommand(cmd, con, null);
                return dsUserDtls;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
    }
}
