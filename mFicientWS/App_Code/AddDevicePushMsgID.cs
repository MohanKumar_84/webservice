﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class AddDevicePushMsgID
    {
        AddDevicePushMsgIDreq _addDevicePushMsgID;

        public AddDevicePushMsgID(AddDevicePushMsgIDreq addDevicePushMsgID)
        {
            _addDevicePushMsgID = addDevicePushMsgID;
        }
        public AddDevicePushMsgIDResp Process()
        {
            SqlTransaction transaction = null;
            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con);
            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        addPushMsgIdForDevice(con, transaction);
                        SqlConnection objmGramCon = null;
                        MSSqlDatabaseClient.SqlConnectionOpen(out objmGramCon, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                            MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
                        if (String.IsNullOrEmpty(_addDevicePushMsgID.PushMsgId))
                        {
                         mFicientCommonProcess.SaveDevcDtlsInMGramInsertUpdate.deleteDeviceDtls(_addDevicePushMsgID.CompanyId,
                                       _addDevicePushMsgID.DeviceId,
                                       _addDevicePushMsgID.DeviceType,
                                       _addDevicePushMsgID.UserName, objmGramCon);   
                        }
                        else
                        {
                            mFicientCommonProcess.SaveDevcDtlsInMGramInsertUpdate.SaveDeviceDetailInMgram(_addDevicePushMsgID.CompanyId,
                                       _addDevicePushMsgID.DeviceId,
                                       _addDevicePushMsgID.DeviceType,
                                       _addDevicePushMsgID.UserName,
                                       _addDevicePushMsgID.PushMsgId, objmGramCon);
                        }
                        transaction.Commit();
                        //objRespStatus = getResponseStatus(null);
                        objRespStatus = GetResponseStatus.getRespStatus(null,
                            _addDevicePushMsgID.FunctionCode);
                        //return ((int)HttpStatusCode.OK).ToString();
                    }
                }

            }
            catch (SqlException e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (MficientException ex)
            {
                //objRespStatus = getResponseStatus(ex);
                objRespStatus = GetResponseStatus.getRespStatus(ex,
                           _addDevicePushMsgID.FunctionCode);
            }
            catch (Exception e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
            return new AddDevicePushMsgIDResp(objRespStatus, _addDevicePushMsgID.RequestId);
        }

        void addPushMsgIdForDevice(SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE TBL_REGISTERED_DEVICE
                                SET DEVICE_PUSH_MESSAGE_ID=@DEVICE_PUSH_MESSAGE_ID
                                WHERE DEVICE_ID=@DEVICE_ID
                                AND DEVICE_TYPE=@DEVICE_TYPE
                                AND USER_ID=@USER_ID
                                AND COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _addDevicePushMsgID.DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _addDevicePushMsgID.DeviceType);
            cmd.Parameters.AddWithValue("@USER_ID", _addDevicePushMsgID.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _addDevicePushMsgID.CompanyId);
            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", _addDevicePushMsgID.PushMsgId);

            int iRowsAffected = cmd.ExecuteNonQuery();
            if (iRowsAffected == 0)
                throw new MficientException(
                    (
                    (int)MFErrors.AddDevicePushMsgIdError.RecordInsertError
                    ).ToString()
                    );

        }

    }
}