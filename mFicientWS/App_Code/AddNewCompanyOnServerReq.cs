﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;
//namespace mFicientWS
//{
//    public class AddNewCompanyOnServerReq
//    {
//        string _sessionId, _requestId;
//        int _functionCode;
//        CompanyAdminDetails _cmpAdminDtls = new CompanyAdminDetails();
//        CompanyCurrentPlan _cmpCurrentPlan = new CompanyCurrentPlan();
//        CompanyDetails _cmpDetails = new CompanyDetails();

//        public CompanyAdminDetails CmpAdminDtls
//        {
//            get { return _cmpAdminDtls; }
//        }
//        public CompanyDetails CmpDetails
//        {
//            get { return _cmpDetails; }
//        }
//        public CompanyCurrentPlan CmpCurrentPlan
//        {
//            get { return _cmpCurrentPlan; }
//        }
//        public string RequestId
//        {
//            get { return _requestId; }
//        }
//        public string SessionId
//        {
//            get { return _sessionId; }
//        }
//        public int FunctionCode
//        {
//            get { return _functionCode; }
//        }


//        public AddNewCompanyOnServerReq(string requestJson)
//        {
//            RequestJsonParsing<AddNewCompanyOnServerReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<AddNewCompanyOnServerReqData>>(requestJson);
//            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
//            if (_functionCode != (int)FUNCTION_CODES.ADD_NEW_COMPANY_ON_SERVER)
//            {
//                throw new Exception("Invalid Function Code");
//            }
//            _sessionId = objRequestJsonParsing.req.sid;
//            _requestId = objRequestJsonParsing.req.rid;
//            _cmpAdminDtls = objRequestJsonParsing.req.data.ad;
//            _cmpCurrentPlan = objRequestJsonParsing.req.data.pln;
//            _cmpDetails = objRequestJsonParsing.req.data.com;
//            if (string.IsNullOrEmpty(_sessionId))
//            {
//                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
//            }
//        }
//    }

//    [DataContract]
//    public class AddNewCompanyOnServerReqData : Data
//    {
//        public AddNewCompanyOnServerReqData()
//        { }
//        /// <summary>
//        /// Company Admin Details
//        /// </summary>
//        [DataMember]
//        public CompanyAdminDetails ad { get; set; }

//        /// <summary>
//        /// Company Details
//        /// </summary>
//        [DataMember]
//        public CompanyDetails com { get; set; }

//        /// <summary>
//        /// Current Plan
//        /// </summary>
//        [DataMember]
//        public CompanyCurrentPlan pln { get; set; }
//    }

//    public class CompanyAdminDetails
//    {
//        public CompanyAdminDetails()
//        { }
//        /// <summary>
//        /// Admin Id
//        /// </summary>
//        [DataMember]
//        public string admid { get; set; }

//        /// <summary>
//        /// Email Id
//        /// </summary>
//        [DataMember]
//        public string emid { get; set; }

//        /// <summary>
//        /// Access Code
//        /// </summary>
//        [DataMember]
//        public string acccd { get; set; }

//        /// <summary>
//        ///First Name
//        /// </summary>
//        [DataMember]
//        public string frtnm { get; set; }

//        /// <summary>
//        /// Last Name
//        /// </summary>
//        [DataMember]
//        public string lstnm { get; set; }

//        /// <summary>
//        /// Street Address 1
//        /// </summary>
//        [DataMember]
//        public string stadd1 { get; set; }

//        /// <summary>
//        /// Street Address 2
//        /// </summary>
//        [DataMember]
//        public string stadd2 { get; set; }

//        /// <summary>
//        /// City Name
//        /// </summary>
//        [DataMember]
//        public string ctnm { get; set; }

//        /// <summary>
//        /// State Name
//        /// </summary>
//        [DataMember]
//        public string stnm { get; set; }

//        /// <summary>
//        /// Country Code
//        /// </summary>
//        [DataMember]
//        public string cntcd { get; set; }

//        /// <summary>
//        /// ZIP
//        /// </summary>
//        [DataMember]
//        public string zip { get; set; }

//        /// <summary>
//        /// Date of Birth
//        /// </summary>
//        [DataMember]
//        public string dob { get; set; }

//        /// <summary>
//        /// Registration Date
//        /// </summary>
//        [DataMember]
//        public string regdt { get; set; }

//        /// <summary>
//        /// Gender
//        /// </summary>
//        [DataMember]
//        public string gnd { get; set; }

//        /// <summary>
//        /// Middle Name
//        /// </summary>
//        [DataMember]
//        public string midnm { get; set; }


//        /// <summary>
//        ///Street Address 3
//        /// </summary>
//        [DataMember]
//        public string stadd3 { get; set; }


//        /// <summary>
//        /// Mobile
//        /// </summary>
//        [DataMember]
//        public string mob { get; set; }


//    }

//    public class CompanyDetails
//    {
//        public CompanyDetails()
//        { }
//        /// <summary>
//        /// Company Id
//        /// </summary>
//        [DataMember]
//        public string enid { get; set; }

//        /// <summary>
//        /// Company Name
//        /// </summary>
//        [DataMember]
//        public string cmpnm { get; set; }

//        /// <summary>
//        /// Registration No
//        /// </summary>
//        [DataMember]
//        public string regno { get; set; }

//        /// <summary>
//        /// Street Address 1
//        /// </summary>
//        [DataMember]
//        public string stadd1 { get; set; }

//        /// <summary>
//        /// Street Address 2
//        /// </summary>
//        [DataMember]
//        public string stadd2 { get; set; }

//        /// <summary>
//        /// Street Address 3
//        /// </summary>
//        [DataMember]
//        public string stadd3 { get; set; }

//        /// <summary>
//        /// City Name
//        /// </summary>
//        [DataMember]
//        public string ctnm { get; set; }

//        /// <summary>
//        /// State Name
//        /// </summary>
//        [DataMember]
//        public string stnm { get; set; }

//        /// <summary>
//        /// Country Code
//        /// </summary>
//        [DataMember]
//        public string cntcd { get; set; }

//        /// <summary>
//        /// ZIP
//        /// </summary>
//        [DataMember]
//        public string zip { get; set; }

//        /// <summary>
//        /// Admin Id
//        /// </summary>
//        [DataMember]
//        public string admid { get; set; }

//        /// <summary>
//        ///Logo Image Name
//        /// </summary>
//        [DataMember]
//        public string imgnm { get; set; }

//        /// <summary>
//        /// Support  Email
//        /// </summary>
//        [DataMember]
//        public string suppem { get; set; }

//        /// <summary>
//        /// Support Contact
//        /// </summary>
//        [DataMember]
//        public string suppcnt { get; set; }
//    }

//    public class CompanyCurrentPlan
//    {
//        public CompanyCurrentPlan()
//        { }
//        /// <summary>
//        /// Company Id
//        /// </summary>
//        [DataMember]
//        public string enid { get; set; }

//        /// <summary>
//        /// Plan Code
//        /// </summary>
//        [DataMember]
//        public string plncd { get; set; }

//        /// <summary>
//        /// Maximum Workflow
//        /// </summary>
//        [DataMember]
//        public string maxwf { get; set; }

//        /// <summary>
//        /// Maximum User
//        /// </summary>
//        [DataMember]
//        public string maxusr { get; set; }

//        /// <summary>
//        /// User Charge Per Month
//        /// </summary>
//        [DataMember]
//        public string chgpm { get; set; }

//        /// <summary>
//        ///Charge Type
//        /// </summary>
//        [DataMember]
//        public string chgtp { get; set; }

//        /// <summary>
//        /// Validity
//        /// </summary>
//        [DataMember]
//        public string val { get; set; }

//        /// <summary>
//        /// Purchase Date
//        /// </summary>
//        [DataMember]
//        public string purdt { get; set; }

//        /// <summary>
//        /// Plan Change Date
//        /// </summary>
//        [DataMember]
//        public string plnchgdt { get; set; }

//        /// <summary>
//        /// Push Message Per Day
//        /// </summary>
//        [DataMember]
//        public string pshmgpd { get; set; }

//        /// <summary>
//        /// Push Message Per Month
//        /// </summary>
//        [DataMember]
//        public string pshmgpm { get; set; }

//        /// <summary>
//        ///Feature 3
//        /// </summary>
//        [DataMember]
//        public string ftr3 { get; set; }

//        /// <summary>
//        /// Feature 4
//        /// </summary>
//        [DataMember]
//        public string ftr4 { get; set; }

//        /// <summary>
//        ///Feature 5
//        /// </summary>
//        [DataMember]
//        public string ftr5 { get; set; }

//        /// <summary>
//        /// Feature 6
//        /// </summary>
//        [DataMember]
//        public string ftr6 { get; set; }

//        /// <summary>
//        /// Feature 7
//        /// </summary>
//        [DataMember]
//        public string ftr7 { get; set; }

//        /// <summary>
//        /// Feature 8
//        /// </summary>
//        [DataMember]
//        public string ftr8 { get; set; }

//        /// <summary>
//        /// Feature 9
//        /// </summary>
//        [DataMember]
//        public string ftr9 { get; set; }

//        /// <summary>
//        /// Feature 10
//        /// </summary>
//        [DataMember]
//        public string ftr10 { get; set; }
//    }
//}