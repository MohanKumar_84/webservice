﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class AddNewContactReq
    {
        string  _requestId, _newContactName, _companyId, _userName;
        int _functionCode;

        public string NewContactName
        {
            get { return _newContactName; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }


        public AddNewContactReq(string requestJson)
        {
            RequestJsonParsing<AddNewContactReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<AddNewContactReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.ADD_CONTACT_REQUEST)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _newContactName = objRequestJsonParsing.req.data.cnctnm;
            _userName = objRequestJsonParsing.req.data.unm;
            _companyId = objRequestJsonParsing.req.data.enid;
        }
    }

    [DataContract]
    public class AddNewContactReqData : Data
    {
        public AddNewContactReqData()
        { }
        /// <summary>
        /// New Contact ID
        /// </summary>
        [DataMember]
        public string cnctnm { get; set; }

     
    }
}