﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class AddNewContactRequetForMbuzz
    {
        #region Private Members

        private string strMsgRefId, strJson, strEnterpriseId,strContact;
        private UserData user;

        private AddNewContactRequestJson addNewContactRequestJson;

        #endregion

        #region Public Properties

        public string RequestJson
        {
            get
            {
                return strJson;
            }
        }

        #endregion

        #region Constructor

        public AddNewContactRequetForMbuzz(string _msgRefId, UserData _user, string _enterpriseId, string Contacts)
        {
            addNewContactRequestJson = new AddNewContactRequestJson();
            strMsgRefId = _msgRefId;
            user = _user;
            strEnterpriseId = _enterpriseId;
            strContact = Contacts;
            strJson = CreateRequestJson();
        }

        #endregion

        #region Private Methods

        private string CreateRequestJson()
        {
            addNewContactRequestJson.mrid = strMsgRefId;
            addNewContactRequestJson.type = ((int)MbuzzMessageCode.NEW_CONTACT_ADDED_OR_UNBLOCKED).ToString();
            addNewContactRequestJson.enid = strEnterpriseId;
            addNewContactRequestJson.unm = user.unm;
            addNewContactRequestJson.user = user;
            addNewContactRequestJson.cntcs=strContact;

            string json = Utilities.SerializeJson<AddNewContactRequestJson>(addNewContactRequestJson);
            return Utilities.getFinalRequestJson(json);
        }

        #endregion
    }
    #region Classes

    [DataContract]
    public class RequestJson
    {
        public RequestJson() { }

        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string mrid { get; set; }

        [DataMember]
        public string type { get; set; }
    }

    [DataContract]
    public class AddNewContactRequestJson : RequestJson
    {
        public AddNewContactRequestJson()
        { }

        [DataMember]
        public UserData user { get; set; }

        /// <summary>
        /// Contact For send Request
        /// </summary>
        [DataMember]
        public string cntcs { get; set; }
    }

    //[DataContract]
    //public class UserData
    //{
    //    /// <summary>
    //    /// UserName
    //    /// </summary>
    //    [DataMember]
    //    public string unm { get; set; }

    //    /// <summary>
    //    /// FullName
    //    /// </summary>
    //    [DataMember]
    //    public string fnm { get; set; }

    //    /// <summary>
    //    /// Email
    //    /// </summary>
    //    [DataMember]
    //    public string em { get; set; }

    //    /// <summary>
    //    /// Designation Name
    //    /// </summary>
    //    [DataMember]
    //    public string des { get; set; }

    //    /// <summary>
    //    /// Department Name
    //    /// </summary>
    //    [DataMember]
    //    public string dep { get; set; }

    //    /// <summary>
    //    /// Status
    //    /// </summary>
    //    [DataMember]
    //    public string status { get; set; }

    //}

    #endregion
}