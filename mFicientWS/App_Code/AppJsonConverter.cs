﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;

namespace mFicientWS
{
    public class AppJsonConverter
    {
        DataSet _dtObjects;
        jqueryAppClass _appDetail;
        Hashtable _formActionButton;
        List<string> _offlineTBL;
        List<string> _offlineTBLHTML;
        public AppJsonConverter(jqueryAppClass appDetail, DataSet dtObjects, Hashtable formActionButton, List<string> offlineTBL)
        {
            _dtObjects = dtObjects;
            _appDetail = appDetail;
            _formActionButton = formActionButton;
            _offlineTBLHTML = offlineTBL;
            _offlineTBL = new List<string>();
        }
        public string ConvertInAppJsonForMobile()
        {
            JObject mobAppJson = new JObject();
            try
            {
                mobAppJson.Add("off", _appDetail.type == null ? "0" : _appDetail.type);
                mobAppJson.Add("wfnm", _appDetail.name);
                mobAppJson.Add("wfid", _appDetail.id);
                mobAppJson.Add("des", _appDetail.description);
                mobAppJson.Add("ver", _appDetail.version);
                mobAppJson.Add("fpg", "");
                mobAppJson.Add("fpgid", _appDetail.startUpViewId);
                //----------------------app startup------
                mobAppJson.Add("appst", GetCommandArrayJSON(_appDetail.startUpTasks.databindingObjs));
                //------------App availability criteria -------------------------
                if (_appDetail.availableOrNotCriteria != null)
                {
                    JObject JOmobAppAvailability = new JObject();
                    JOmobAppAvailability.Add("allow", _appDetail.availableOrNotCriteria);
                    JOmobAppAvailability.Add("days", _appDetail.availabilityDays);
                    JOmobAppAvailability.Add("time", _appDetail.availabilityFromTimeHr + ":" + _appDetail.availabilityFromTimeMin + ":" + _appDetail.availabilityToTimeHr + ":" + _appDetail.availabilityToTimeMin);
                    JOmobAppAvailability.Add("mdata", _appDetail.mobNetworkAllowed);
                    if (_appDetail.availabilityWiFi is string) JOmobAppAvailability.Add("wifi", _appDetail.availabilityWiFi.ToString());
                    else JOmobAppAvailability.Add("wifi", JArray.Parse(_appDetail.availabilityWiFi.ToString()));
                    mobAppJson.Add("rstrc", JOmobAppAvailability);
                }
                //----------------------Views-----------
                JArray JAviews = new JArray();
                foreach (Appviews view in _appDetail.views)
                {
                    JObject JOview = new JObject();
                    //wfMain.wf.views.Add(new Mobviews(vw, dtObjects, appDetail.conditionViews));
                    if (view.id == _appDetail.startUpViewId)
                        mobAppJson["fpg"] = view.formName;

                    JOview.Add("id", view.id);
                    JOview.Add("fid", view.formId);
                    JOview.Add("fnm", view.formName);
                    JOview.Add("rval", (string.IsNullOrEmpty(view.retainValue) ? "0" : view.retainValue));
                    JOview.Add("fver", (view.version == null ? "" : view.version));
                    JOview.Add("name", view.name);
                    JOview.Add("mdl", view.presentation == null ? "0" : view.presentation);
                    JOview.Add("ttl", view.title);
                    JOview.Add("lttl", (view.longTitle == null ? "" : view.longTitle));
                    JOview.Add("bknv", view.backNavigation);
                    //--------------------View init--------
                    JArray JAint = new JArray();
                    if (view.onEnterInits != null)
                    {
                        foreach (Initialization _srcint in view.onEnterInits)
                        {
                            JObject JOint = new JObject();
                            JOint.Add("srcid", _srcint.sourceId);

                            JObject JOttl = new JObject();
                            JOttl.Add("tmp", _srcint.title);
                            JOttl.Add("pval", string.IsNullOrEmpty(_srcint.parameter) ? "" : _srcint.parameter);
                            JOint.Add("ttl", JOttl);

                            JArray JAcmap = new JArray();
                            foreach (ControlInits _ctrint in _srcint.cntrlInits)
                            {
                                JObject JOcmap = new JObject();
                                JOcmap.Add("cname", _ctrint.controlName);
                                if (_ctrint.controlValue.Count == 0 && _ctrint.controlType.ToUpper() == "LABEL")
                                {
                                    JArray cval = new JArray();
                                    cval.Add(""); cval.Add(""); cval.Add(""); cval.Add(""); cval.Add("");
                                    JOcmap.Add("cval", cval);
                                }
                                else if (_ctrint.controlValue.Count == 0) JOcmap.Add("cval", "");
                                else if (_ctrint.controlValue.Count == 1) JOcmap.Add("cval", _ctrint.controlValue[0]);
                                else
                                {
                                    JArray cval = new JArray();
                                    foreach (string s in _ctrint.controlValue)
                                    {
                                        cval.Add(s);
                                    }
                                    JOcmap.Add("cval", cval);
                                }
                                JAcmap.Add(JOcmap);
                            }
                            JOint.Add("cmap", JAcmap);
                            JAint.Add(JOint);
                        }
                    }
                    JOview.Add("init", JAint);
                    //-------------------------back button -----
                    JObject JObbtn = new JObject();
                    JObbtn.Add("bkvw", string.IsNullOrEmpty(view.backButtonView) ? "" : view.backButtonView);
                    JObbtn.Add("bkskp", view.backButton);
                    JOview.Add("bbtn", JObbtn);
                    //-------------------------Next button -----
                    if (Convert.ToInt32(view.nextButton) == 1)
                    {
                        JArray JAnbtn = new JArray();
                        JObject JOnbtn = GetDestinationJSON(view.destinations, _appDetail.conditionViews, ((int)ViewTaskType.NEXT).ToString(), view.id, "");
                        JOnbtn = getTaskJSON(view.onExitCmds, JOnbtn);
                        JOnbtn.Add("pos", string.IsNullOrEmpty(view.nextButtonPosition) ? "5" : view.nextButtonPosition);
                        JOnbtn.Add("lbl", view.nextBtnLabel);
                        JAnbtn.Add(JOnbtn);
                        JOview.Add("nbtn", JAnbtn);
                    }
                    //------------------cancel button-------
                    if (Convert.ToInt32(view.cancelButton) == 1)
                    {
                        JObject JOcbtn = GetDestinationJSON(view.destinations, _appDetail.conditionViews, "", view.id, "");
                        JOcbtn = getTaskJSON(view.cancelTasks, JOcbtn);
                        JOcbtn.Add("pos", string.IsNullOrEmpty(view.cancelButtonPosition) ? "3" : view.cancelButtonPosition);
                        JOcbtn.Add("lbl", view.cancelBtnLabel);
                        JOview.Add("cbtn", JOcbtn);
                    }

                    //----------------row Click------
                    //if (Convert.ToInt32(view.rowClickAction) == 1)
                    //{
                    JObject JOrclk = GetDestinationJSON(view.destinations, _appDetail.conditionViews, ((int)ViewTaskType.ROWCLICK).ToString(), view.id, "");
                    JOrclk = getTaskJSON(view.rowClickTasks, JOrclk);
                    JOview.Add("rclk", JOrclk);
                    //}
                    //---------------List Action button------

                    JOview.Add("lbtns", GetListActionButtonJson(view, _appDetail.conditionViews));
                    //-----------------view Action sheet-----------
                    if (!string.IsNullOrEmpty(view.enableViewActionBtns) && Convert.ToInt32(view.enableViewActionBtns) == 1)
                    {
                        JArray JAabtns = new JArray();
                        if (view.viewActionBtns != null)
                            foreach (ViewActionButton objbtn in view.viewActionBtns)
                            {
                                JObject JObtn = GetDestinationJSON(view.destinations, _appDetail.conditionViews, ((int)ViewTaskType.VIEWACTIONBUTTON).ToString() + objbtn.index, view.id, "");
                                JObtn = getTaskJSON(objbtn.onExitTasks, JObtn);
                                JObtn.Add("pos", objbtn.index);
                                JObtn.Add("lbl", objbtn.name);
                                JAabtns.Add(JObtn);
                            }
                        JObject JOabtn = new JObject();
                        JOabtn.Add("pos", string.IsNullOrEmpty(view.viewActionBtnsPosition) ? "4" : view.viewActionBtnsPosition);
                        JOabtn.Add("btns", JAabtns);
                        //--------------------------------------
                        JOview.Add("abtn", JOabtn);
                    }
                    JArray JAfbtns = new JArray();
                    if (view.formActionBtns != null && view.formActionBtns.Count > 0)
                        foreach (ViewActionButton objbtn in view.formActionBtns)
                        {
                            JObject JObtn = GetDestinationJSON(view.destinations, _appDetail.conditionViews, ((int)ViewTaskType.FORMACTIONBUTTON).ToString(), view.id, objbtn.name);
                            JObtn = getTaskJSON(objbtn.onExitTasks, JObtn);
                            JObtn.Add("pos", objbtn.index);
                            JObtn.Add("ctrlid", string.IsNullOrEmpty(objbtn.userDefinedName) ? "" : objbtn.userDefinedName);
                            JAfbtns.Add(JObtn);
                        }
                    else
                    {
                        AppForm curentfrm = null;
                        foreach (AppForm frm in _appDetail.forms)
                        {
                            if (view.formId == frm.id)
                            {
                                curentfrm = frm;
                                break;
                            }
                        }
                        if (view.destinations != null)
                        foreach (ViewDestination _vdes in view.destinations)
                        {
                            int index = 0;
                            if (_vdes.type == ((int)ViewTaskType.FORMACTIONBUTTON).ToString())
                            {
                                bool flag = false;
                                foreach (sectionRowPanels rw in curentfrm.rowPanels)
                                {
                                    flag = false;
                                    foreach (SectionColPanels col in rw.colmnPanels)
                                    {
                                        foreach (Controls ctrl in col.controls)
                                            if (_vdes.evntSrcId == ctrl.id)
                                            {
                                                JObject JObtn = GetDestinationJSON(view.destinations, _appDetail.conditionViews, ((int)ViewTaskType.FORMACTIONBUTTON).ToString(), view.id, ctrl.id);
                                                JObtn.Add("pos", index++);
                                                JObtn.Add("ctrlid", string.IsNullOrEmpty(ctrl.userDefinedName) ? "" : ctrl.userDefinedName);
                                                JAfbtns.Add(JObtn);
                                                flag = true;
                                                break;
                                            }
                                        if (flag) break;
                                    }
                                    if (flag) break;
                                }
                            }
                        }
                    }
                    JOview.Add("fbtns", JAfbtns);
                    JArray JAfctrg = new JArray();
                    if (view.customTriggers != null)
                        foreach (ViewCustomTrigger objctrg in view.customTriggers)
                        {
                            JObject JOctrg = new JObject();
                            JOctrg = getTaskJSON(objctrg.onExitTasks, JOctrg);
                            JOctrg.Add("id", objctrg.name.Substring(2));
                            JAfctrg.Add(JOctrg);
                        }
                    JOview.Add("ctgr", JAfctrg);
                    JAviews.Add(JOview);
                }
                mobAppJson.Add("views", JAviews);
            }
            catch
            {

            }
            _offlineTBL.AddRange(_offlineTBLHTML);
            mobAppJson.Add("otbl", JArray.Parse(Utilities.SerializeJson<List<string>>(_offlineTBL)));
            return mobAppJson.ToString();
        }
        JArray GetListActionButtonJson(Appviews view, List<ConditionView> conView)
        {
            JArray JAlbtns = new JArray();
            if (_formActionButton.ContainsKey(view.formId))
            {
                JArray Jafrm = (JArray)(_formActionButton[view.formId]);
                int i = 0;
                foreach (JObject Jo in Jafrm)
                {
                    JObject JoNew = new JObject();
                    if (Jo["idx"].ToString() == "0")
                    {
                        JoNew = GetDestinationJSON(view.destinations, conView,  ((int)ViewTaskType.ACTIONBUTTON1).ToString(), view.id, "");
                        JoNew = getTaskJSON(view.actionBtn1Tasks, JoNew);
                    }
                    else if (Jo["idx"].ToString() == "1")
                    {
                        JoNew = GetDestinationJSON(view.destinations, conView, ((int)ViewTaskType.ACTIONBUTTON2).ToString(), view.id, "");
                        JoNew = getTaskJSON(view.actionBtn2Tasks, JoNew);
                    }
                    else if (Jo["idx"].ToString() == "2")
                    {
                        JoNew = GetDestinationJSON(view.destinations, conView, ((int)ViewTaskType.ACTIONBUTTON3).ToString(), view.id, "");
                        JoNew = getTaskJSON(view.actionBtn3Tasks, JoNew);
                    }
                    JoNew["idx"] = i.ToString();
                    JoNew["lbl"] = Jo["lbl"];
                    JAlbtns.Add(JoNew);
                    i++;
                }
            }
            return JAlbtns;
        }

        JArray GetCommandArrayJSON(List<AppCommands> _cmds)
        {
            JArray JAcmds = new JArray();
            if (_cmds != null && _cmds.Count > 0)
            {
                int idx = 0;
                foreach (AppCommands cmd in _cmds)
                {
                    string CREDENTIAL_PROPERTY = "", CACHE = "0", EXPIRY_FREQUENCY = "", EXPIRY_CONDITION = "", NO_EXECTION = "[]";

                    if (Convert.ToInt32(cmd.bindObjType) != 6)
                    {
                        DataRow[] result = _dtObjects.Tables[0].Select("COMMAND_ID = '" + cmd.id + "'");
                        if (result.Count() > 0)
                        {
                            CREDENTIAL_PROPERTY = Convert.ToString(result[0]["CREDENTIAL_PROPERTY"]);
                            CREDENTIAL_PROPERTY = CREDENTIAL_PROPERTY == "-1" ? "" : CREDENTIAL_PROPERTY;
                            CACHE = Convert.ToString(result[0]["CACHE"]);
                            EXPIRY_FREQUENCY = Convert.ToString(result[0]["EXPIRY_FREQUENCY"]);
                            EXPIRY_CONDITION = Convert.ToString(result[0]["EXPIRY_CONDITION"]);
                            NO_EXECTION = Convert.ToString(result[0]["NO_EXECUTE_CONDITION"]);
                            NO_EXECTION = string.IsNullOrEmpty(NO_EXECTION) ? "[]" : NO_EXECTION;
                        };
                    }
                    else
                    {
                        DataRow[] result = _dtObjects.Tables[1].Select("OFFLINE_OBJECT_ID = '" + cmd.id + "'");
                        if (result.Count() > 0)
                        {
                            if (!_offlineTBL.Contains(Convert.ToString(result[0]["TABLE_ID"])) && !_offlineTBLHTML.Contains(Convert.ToString(result[0]["TABLE_ID"])))
                                _offlineTBLHTML.Add(Convert.ToString(result[0]["TABLE_ID"]));
                        }
                    }
                    if (cmd != null)
                    {
                        JObject MobCommand = new JObject();
                        MobCommand.Add("cmdtyp", cmd.bindObjType);
                        MobCommand.Add("cmd", cmd.id);
                        MobCommand.Add("cname", cmd.name);
                        MobCommand.Add("idx", idx);
                        MobCommand.Add("ctrlid", cmd.dsName);
                        MobCommand.Add("cr", CREDENTIAL_PROPERTY);
                        MobCommand.Add("cache", CACHE);
                        MobCommand.Add("efrq", EXPIRY_FREQUENCY);
                        MobCommand.Add("econ", EXPIRY_CONDITION);
                        JArray JAParaList = new JArray();
                        foreach (DatabindingObjParams _lp in cmd.cmdParams)
                        {
                            if (_lp != null && _lp.name != null)
                            {
                                JObject JApara = new JObject();
                                JApara.Add("para", _lp.name);
                                JApara.Add("val", _lp.val);
                                JAParaList.Add(JApara);
                            }
                        }
                        MobCommand.Add("lp", JAParaList);
                        JArray rp = JArray.Parse(NO_EXECTION);
                        MobCommand.Add("rp", rp);
                        JAcmds.Add(MobCommand);
                    }
                    idx++;
                }
            }
            return JAcmds;
        }
        JObject GetDestinationJSON(List<ViewDestination> destinations, List<ConditionView> conditionViews, string Buttontype, string viewId, string TransitionName)
        {
            JObject JObtn = new JObject();
            ConditionView CurrentCondition = null;
            string desId = "";
            if (destinations != null)
            {
                bool flag = false;
                foreach (ViewDestination _vdes in destinations)
                {
                    if (_vdes.type == Buttontype && (TransitionName == "" || _vdes.evntSrcId == TransitionName))
                    {
                        if (_vdes.destinationId != "" && _vdes.destinationId.StartsWith("C"))
                        {
                            foreach (ConditionView _con in conditionViews)
                            {
                                if (_con.id == _vdes.destinationId && _con.sourceId == viewId)
                                {
                                    CurrentCondition = _con;
                                    flag = true;
                                    break;
                                }
                                //this.dests.Add(new MobAppDestinations(_vdes));
                            }
                        }
                        desId = _vdes.destinationId;
                    }
                    if (flag) break;

                    //this.dests.Add(new MobAppDestinations(_vdes));
                }
                JObtn.Add("dsid", desId);
                if (CurrentCondition != null)
                {
                    JObject JOcond = new JObject();
                    JOcond.Add("name", CurrentCondition.onCondition.name);
                    string dci = "0";
                    JArray JAoncnds = new JArray();
                    if (CurrentCondition.onCondition != null)
                        JAoncnds = Conditions(CurrentCondition.onCondition.destinationCond, conditionViews, CurrentCondition.id, out dci);

                    JOcond.Add("dci", dci);
                    JOcond.Add("oncnd", JAoncnds);
                    JObtn.Add("cond", JOcond);
                }
            }
            return JObtn;
        }
        JObject GetButtonJSON(Tasks _task,JObject JObtn)
        {
            return getTaskJSON(_task, JObtn);
        }

        JObject getListedConditionalJSON(List<ConditionView> conditionViews, string destinationId, string sourceId)
        {
            ConditionView CurrentCondition = null;
            foreach (ConditionView _con in conditionViews)
            {
                if (_con.id == destinationId && _con.sourceId == sourceId)
                {
                    CurrentCondition = _con;
                    break;
                }
            }

            JObject JOcond = new JObject();
            JOcond.Add("name", CurrentCondition.onCondition.name);
            string dci = "0";
            JArray JAoncnds = new JArray();
            if (CurrentCondition.onCondition != null)
            {
                JAoncnds = Conditions(CurrentCondition.onCondition.destinationCond, conditionViews, destinationId, out dci);
            }
            JOcond.Add("dci", dci);//default Conditional display
            JOcond.Add("oncnd", JAoncnds);
            return JOcond;
        }

        JArray Conditions(List<destiCond> dc, List<ConditionView> conditionViews, string destinationId, out string dindex)
        {
            JArray JAoncnds = new JArray();
            string dci = "";
            dindex = "0";
            if (dc.Count > 0) dci = dc[dc.Count - 1].viewId;
            for (int i = 0; i < dc.Count - 1; i++)
            {
                destiCond dvc = dc[i];
                JObject JOoncnd = new JObject();
                if (dindex == "0" && dci.ToLower() == dvc.viewId.ToLower()) dindex = dvc.index;
                JOoncnd.Add("idx", dvc.index);
                JOoncnd.Add("id", dvc.viewId);
                JOoncnd.Add("cctrl", dvc.compareCntrl);
                JOoncnd.Add("cndtyp", dvc.condType);
                JOoncnd.Add("val", dvc.condValue);
                if (dvc.viewId.StartsWith("C"))
                    JOoncnd.Add("cond", getListedConditionalJSON(conditionViews, dvc.viewId, destinationId));
                getTaskJSON(dvc.onExitTasks, JOoncnd);
                JAoncnds.Add(JOoncnd);
            }
            return JAoncnds;
        }

        JObject getTaskJSON(Tasks _task, JObject JObtn)
        {
            //--------dailog-------
            if (_task != null)
            {
                JArray JAdlg = new JArray();
                if (_task.dialogSettings != null)
                {
                    int i = 0;
                    foreach (DialogSettings _dia in _task.dialogSettings)
                    {
                        if (_dia != null && !string.IsNullOrEmpty(_dia.msg))
                        {
                            JObject JOdlg = new JObject();
                            JOdlg.Add("idx", i);
                            JOdlg.Add("type", _dia.type);
                            JOdlg.Add("msg", _dia.msg);
                            JOdlg.Add("dbtn", (string.IsNullOrEmpty(_dia.dismissBtnText) ? "Cancel" : _dia.dismissBtnText));
                            JOdlg.Add("pbtn", (string.IsNullOrEmpty(_dia.proceedBtnText) ? "Ok" : _dia.proceedBtnText));
                            JAdlg.Add(JOdlg);
                        }
                    }
                }
                JObtn.Add("dlg", JAdlg);
                //--------------commands-----------------       

                JObtn.Add("cmds", GetCommandArrayJSON(_task.databindingObjs));
                //----------------------------------

                if (_task.pushMsgSettings != null && !string.IsNullOrEmpty(_task.pushMsgSettings.sendTo))
                {
                    JObject JOpmsg = new JObject();
                    JOpmsg.Add("typ", _task.pushMsgSettings.sendToType);
                    JOpmsg.Add("sto", _task.pushMsgSettings.sendTo);
                    JOpmsg.Add("txt", _task.pushMsgSettings.text);
                    JArray JAtxp = new JArray();
                    JAtxp.Add(getFormatedStringParameterJSON(_task.pushMsgSettings.var1, null));
                    JAtxp.Add(getFormatedStringParameterJSON(_task.pushMsgSettings.var2, null));
                    JAtxp.Add(getFormatedStringParameterJSON(_task.pushMsgSettings.var3, null));
                    JAtxp.Add(getFormatedStringParameterJSON(_task.pushMsgSettings.var4, null));
                    JAtxp.Add(getFormatedStringParameterJSON(_task.pushMsgSettings.var5, null));
                    JAtxp.Add(getFormatedStringParameterJSON(_task.pushMsgSettings.var6, null));
                    JOpmsg.Add("txp", JAtxp);
                    JObtn.Add("pmsg", JOpmsg);
                }
            }
            else
            {
                JObtn.Add("dlg", new JArray());
                JObtn.Add("cmds", new JArray());
            }
            return JObtn;
        }

        JObject getFormatedStringParameterJSON(string value, LabelParameterFromating format)
        {
            JObject JOtxp = new JObject();
            JOtxp.Add("val", value);
            if (!string.IsNullOrEmpty(value))
            {
                JObject JOfrmt = new JObject();
                if (format == null)
                {
                    JOfrmt.Add("typ", "0");
                    JOfrmt.Add("dec", "0");
                    JOfrmt.Add("fact", "1");
                }
                else
                {
                    JOfrmt.Add("typ", format.dataType);
                    JOfrmt.Add("dec", format.noOfDecimalPlaces);
                    JOfrmt.Add("fact", format.mulFactor);
                }
                JOtxp.Add("frmt", JOfrmt);
            }
            return JOtxp;
        }
    }
}