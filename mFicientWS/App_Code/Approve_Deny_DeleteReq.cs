﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class Approve_Deny_DeleteReq
    {
        string _requestId, _deviceId, _deviceType, _companyId, _userid, _muid, _sesstionId;
        int _functionCode, _type;

        public string CompanyId
        {
            get { return _companyId;}
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string UserId
        {
            get { return _userid; }
        }
        public int Type
        {
            get { return _type; }
        }
        public string Mobileuserid
        {
            get { return _muid; }
        }
        public string DeviceUserId
        {
            get { return _muid; }
        }

        public string SesstionId
        {
            get { return _sesstionId; }
        }


        public Approve_Deny_DeleteReq(string requestJson)
        {

            RequestJsonParsing<Approve_Deny_DeleteReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<Approve_Deny_DeleteReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.APPROVE_DENY_DELETE)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userid = objRequestJsonParsing.req.data.unm;
            _muid = objRequestJsonParsing.req.data.munm;
            _type = Convert.ToInt32(objRequestJsonParsing.req.data.typ);
            _sesstionId = objRequestJsonParsing.req.sid;
        }

        [DataContract]
        public class Approve_Deny_DeleteReqData : Data
        {
            public Approve_Deny_DeleteReqData()
            { }
            [DataMember]
            public string munm { get; set; }


            [DataMember]
            public string typ { get; set; }

        }



    }
}