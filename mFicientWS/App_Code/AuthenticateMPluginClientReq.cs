﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class AuthenticateMPluginClientReq
    {
        string _requestId, _companyId, _password, _mPluginAgentName;
        int _functionCode;

        public string Password
        {
            get { return _password; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        public string mpluginAgentName
        {
            get { return _mPluginAgentName; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }


        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public AuthenticateMPluginClientReq(string requestJson)
        {
            RequestJsonParsing<AuthenticateMPluginClientData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<AuthenticateMPluginClientData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MPLUGIN_CLIENT_AUTHENTICATION)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _password = objRequestJsonParsing.req.data.pwd;
            _mPluginAgentName = objRequestJsonParsing.req.data.mpnm;
        }
    }
    [DataContract]
    public class AuthenticateMPluginClientData : Data
    {
        /// <summary>
        /// password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        /// mPlugin Name
        /// </summary>
        [DataMember]
        public string mpnm { get; set; }
    }
}