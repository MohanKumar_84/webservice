﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace mFicientWS
{
    public class CPGetPushMsgMetaData
    {
        CPGetPushMsgMetaDataReq _cpPushMsgMetaDataReq;

        public CPGetPushMsgMetaData(CPGetPushMsgMetaDataReq cpPushMsgMetaDataReq)
        {
            _cpPushMsgMetaDataReq = cpPushMsgMetaDataReq;
        }

        public CPGetPushMsgMetaDataResp Process()
        {
            if (String.IsNullOrEmpty(_cpPushMsgMetaDataReq.RequestId) ||
                 String.IsNullOrEmpty(_cpPushMsgMetaDataReq.CompanyId) ||
                 String.IsNullOrEmpty(_cpPushMsgMetaDataReq.AgentName) ||
                String.IsNullOrEmpty(_cpPushMsgMetaDataReq.AgentPassword) ||
                String.IsNullOrEmpty(_cpPushMsgMetaDataReq.DatabaseType) ||
                 String.IsNullOrEmpty(_cpPushMsgMetaDataReq.ConnString) ||
                String.IsNullOrEmpty(_cpPushMsgMetaDataReq.SqlQuery) ||
                _cpPushMsgMetaDataReq.ParamList == null)
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());

            }

            string strMetaDataOfTable = String.Empty;
            mPlugin.SqlParameters sqlParams = new mPlugin.SqlParameters();
            foreach (QueryParameters parameter in _cpPushMsgMetaDataReq.ParamList)
            {
                sqlParams.Add(parameter.para, parameter.val);
            }
            try
            {
                strMetaDataOfTable = mPlugin.RunmGramSqlQuery(_cpPushMsgMetaDataReq.CompanyId,
                      _cpPushMsgMetaDataReq.AgentName,
                      _cpPushMsgMetaDataReq.AgentPassword,
                      _cpPushMsgMetaDataReq.ConnString,
                      _cpPushMsgMetaDataReq.SqlQuery,
                      sqlParams,
                      (DatabaseType)Enum.Parse(typeof(DatabaseType), _cpPushMsgMetaDataReq.DatabaseType));
            }
            catch (mPlugin.mPluginException e)
            {
                return new CPGetPushMsgMetaDataResp("", "1", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
            return new CPGetPushMsgMetaDataResp(strMetaDataOfTable, 0.ToString(), String.Empty);
        }
           
    }
}