﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class CPGetPushMsgMetaDataReq
    {
        string _requestId, _companyId, _agentName, _agentPassword;
        string _dbType, _connString, _sqlQuery;
        List<QueryParameters> _paramList;

        public List<QueryParameters> ParamList
        {
            get { return _paramList; }
        }
        public string ConnString
        {
            get { return _connString; }
        }
        public string SqlQuery
        {
            get { return _sqlQuery; }
        }
        public string DatabaseType
        {
            get { return _dbType; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }

        public string AgentPassword
        {
            get { return _agentPassword; }

        }

        public string AgentName
        {
            get { return _agentName; }

        }
        public string CompanyId
        {
            get { return _companyId; }

        }
        public CPGetPushMsgMetaDataReq(string requestJson)
        {
            CPGetPushMsgMetaDataReqJsonParsing objReqJsonParsed = Utilities.DeserialiseJson<CPGetPushMsgMetaDataReqJsonParsing>(requestJson);
            _requestId = objReqJsonParsed.req.rid;
            _companyId = objReqJsonParsed.req.eid;
            _agentName = objReqJsonParsed.req.agtnm;
            _agentPassword = objReqJsonParsed.req.agtpwd;
            _dbType = objReqJsonParsed.req.dbtp;
            _connString = objReqJsonParsed.req.constr;
            _sqlQuery = objReqJsonParsed.req.query;
            _paramList = objReqJsonParsed.req.lp;
        }
    }


    [DataContract]
    public class CPGetPushMsgMetaDataReqJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public CPGetPushMsgMetaDataReqReqFields req { get; set; }
    }

    [DataContract]
    public class CPGetPushMsgMetaDataReqReqFields
    {

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        ///company id
        /// </summary>
        [DataMember]
        public string eid { get; set; }
        /// <summary>
        /// agent name
        /// </summary>
        [DataMember]
        public string agtnm { get; set; }
        /// <summary>
        /// agent password
        /// </summary>
        [DataMember]
        public string agtpwd { get; set; }
       /// <summary>
       /// connection string
       /// </summary>
        [DataMember]
        public string constr { get; set; }
       /// <summary>
       /// Query to run
       /// </summary>
        [DataMember]
        public string query { get; set; }
       
        /// <summary>
        /// Database Type
        /// </summary>
        [DataMember]
        public string dbtp { get; set; }

        /// <summary>
        /// Parameters for query
        /// </summary>
        [DataMember]
        public List<QueryParameters> lp { get; set; }
    }
}