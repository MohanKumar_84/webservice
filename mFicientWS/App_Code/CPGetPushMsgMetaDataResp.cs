﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class CPGetPushMsgMetaDataResp
    {
        string _data, _statusCode, _description;
        public CPGetPushMsgMetaDataResp(string data, string statusCode, string description)
        {
            try
            {
                _data = data;
                _statusCode = statusCode;
                _description = description;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating CPGetPushMsgMetaData Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            //string str= "{resp:{\"cd\":\"" + _statusCode + "\",\"desc\":\"" + _description + "\",\"data\":\"" + _data + "\"}}";
            //return str;

            CPPushMsgMetaDataResponse objCPPushMsgDataResp = new CPPushMsgMetaDataResponse();
            objCPPushMsgDataResp.cd = _statusCode;
            objCPPushMsgDataResp.desc = _description;
            objCPPushMsgDataResp.data = _data;
            string strJsonWithDetails = Utilities.SerializeJson<CPPushMsgMetaDataResponse>(objCPPushMsgDataResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class CPPushMsgMetaDataResponse
    {

        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public string data { get; set; }
    }
}