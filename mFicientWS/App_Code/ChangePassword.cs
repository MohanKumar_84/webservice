﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;

namespace mFicientWS
{
    public class ChangePassword
    {
        ChangePasswordReq _changePasswordReq;
        public ChangePassword(ChangePasswordReq changePasswordReq)
        {
            _changePasswordReq = changePasswordReq;
        }
        public ChangePasswordResp Process(HttpContext context)
        {
            MFEMobileUser objUserDetail = getUserDetailsByUserIdAndPassword();
            ResponseStatus objResponseStatus = new ResponseStatus();
            if (!String.IsNullOrEmpty(objUserDetail.UserId))
            {
                if (!string.IsNullOrEmpty(_changePasswordReq.DomainName))
                {
                    objResponseStatus.cd = "1200201";
                    objResponseStatus.desc = "Password cannot be changed for domain user.";
                    return new ChangePasswordResp(objResponseStatus, _changePasswordReq.RequestId);
                }
                SqlTransaction transaction = null;
                SqlConnection con;
                MSSqlDatabaseClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            int iExecuteReturnValue = updatePassword(con,transaction);
                            if (iExecuteReturnValue > 0)
                            {
                                
                                bool blnEmailInfoSaved = SaveEmailInfo.changePassword(objUserDetail.EmailId,objUserDetail.Username, _changePasswordReq.NewPassword, DateTime.UtcNow.Ticks,CompanyTimezone.getTimezoneInfo(_changePasswordReq.CompanyId),context);

                                if (!blnEmailInfoSaved) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                
                                transaction.Commit();
                                foreach(MobileDetail mbd in objUserDetail.MobileDetails)
                                {
                                    if (mbd.DeviceID.ToLower() != _changePasswordReq.DeviceId.ToLower())
                                    {
                                        SqlConnection objSqlConnection = null;
                                        MSSqlDatabaseClient.SqlConnectionOpen(out objSqlConnection, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                                        MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));

                                        mFicientCommonProcess.SendNotification.SaveNotificationTable(objSqlConnection, null, _changePasswordReq.CompanyId,
                                        objUserDetail.Username,"",0, mFicientCommonProcess.NOTIFICATION_TYPE.MF_CHANGE_PWD, mbd.DevicePushNotificationId, mbd.DeviceType, mbd.DeviceID, "",1);
                                    }
                                }
                                CacheManager.Remove(CacheManager.GetKey(CacheManager.CacheType.mFicientSession, _changePasswordReq.CompanyId,
                                    objUserDetail.Username, _changePasswordReq.DeviceId, _changePasswordReq.DeviceType,
                                    String.Empty, String.Empty, String.Empty));
                                //make response status
                                objResponseStatus.cd = "0";
                                objResponseStatus.desc = "";
                                return new ChangePasswordResp(objResponseStatus, _changePasswordReq.RequestId);
                            }
                            else
                            {
                                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                            }
                        }
                    }
                }
                catch (SqlException e)
                {
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                catch (Exception e)
                {
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }
            }
            else
            {
                //record not found error
                throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
            }
        }

        int updatePassword(SqlConnection con,SqlTransaction transaction)
        {
            SqlCommand cmd = new SqlCommand(@"UPDATE tbl_user_detail
                                            SET access_code = @AccessCode
                                            WHERE user_id = @UserId
                                            AND  COMPANY_ID=@COMPANY_ID",con,transaction);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _changePasswordReq.UserId);
            cmd.Parameters.AddWithValue("@AccessCode", _changePasswordReq.NewPassword);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _changePasswordReq.CompanyId);
            return cmd.ExecuteNonQuery();
        }

        MFEMobileUser getUserDetailsByUserIdAndPassword()
        {
            MFEMobileUser objUserDtl = new MFEMobileUser();
            WSCGetUserDetail objGetUserDetl =
                new WSCGetUserDetail(_changePasswordReq.UserId, _changePasswordReq.CompanyId,
                    _changePasswordReq.DeviceId);

           objUserDtl= objGetUserDetl.getUserDetail(_changePasswordReq.OldPassword);
            if (objGetUserDetl.StatusCode != 0)
                throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
            else
                return objUserDtl;

        }
    }
}