﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
namespace mFicientWS
{
    public class CommentOnQueryAndChangeStatusResp
    { ResponseStatus _respStatus;
        string _requestId;
        bool _isSameReqBefore5Min;
        public CommentOnQueryAndChangeStatusResp(ResponseStatus respStatus, string requestId,bool isSameReqBefore5Min)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _isSameReqBefore5Min = isSameReqBefore5Min;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Change Password Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            if (!_isSameReqBefore5Min)
            {
                CommentOnQueryAndChangeStatusResponse objCommentOnQueryAndChangeStatusResp = new CommentOnQueryAndChangeStatusResponse();
                objCommentOnQueryAndChangeStatusResp.func = Convert.ToString((int)FUNCTION_CODES.COMMENT_AND_UPDATE_QUERY);
                objCommentOnQueryAndChangeStatusResp.rid = _requestId;
                objCommentOnQueryAndChangeStatusResp.status = _respStatus;
                string strJsonWithDetails = Utilities.SerializeJson<CommentOnQueryAndChangeStatusResponse>(objCommentOnQueryAndChangeStatusResp);
                return Utilities.getFinalJson(strJsonWithDetails);
            }
            else
            {
                return ((int)HttpStatusCode.Accepted).ToString();
            }
        }
    }

    public class CommentOnQueryAndChangeStatusResponse : CommonResponse
    {
        public CommentOnQueryAndChangeStatusResponse()
        { }
    }
}