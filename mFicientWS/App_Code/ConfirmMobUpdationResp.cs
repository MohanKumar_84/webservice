﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class ConfirmMobUpdationResp
    { 
        ResponseStatus _respStatus;
        string _requestId;
        public ConfirmMobUpdationResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Change Password Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            ConfirmMobUpdationResponse objChangePasswordResp = new ConfirmMobUpdationResponse();
            objChangePasswordResp.func = Convert.ToString((int)FUNCTION_CODES.UPDATION_COMPLETE);
            objChangePasswordResp.rid = _requestId;
            objChangePasswordResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<ConfirmMobUpdationResponse>(objChangePasswordResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class ConfirmMobUpdationResponse : CommonResponse
    {
        public ConfirmMobUpdationResponse()
        { }
    }
}