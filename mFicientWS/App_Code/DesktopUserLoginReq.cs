﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DesktopUserLoginReq
    {
        string _password, _requestId, _deviceId, _deviceType, _companyId, _userName, _ipAddress, _hostName;
        int _functionCode;

        public string HostName
        {
            get { return _hostName; }
        }
        public string IpAddress
        {
            get { return _ipAddress; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string Password
        {
            get { return _password; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public DesktopUserLoginReq(string requestJson, string userId, string hostName, string ipAddress)
        {
            RequestJsonParsing<DesktopUserLoginReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<DesktopUserLoginReqData>>(requestJson);
            _userName = objRequestJsonParsing.req.data.unm;
            if (_userName.Contains("\\"))
            {
                _userName = _userName.Split('\\')[1];
            }
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.DESKTOP_USER_LOGIN)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _password = objRequestJsonParsing.req.data.pwd;
            _ipAddress = ipAddress;
            _hostName = hostName;
            if (String.IsNullOrEmpty(_password))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
    }

    [DataContract]
    public class DesktopUserLoginReqData : Data
    {
        /// <summary>
        /// Password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }


    }
}