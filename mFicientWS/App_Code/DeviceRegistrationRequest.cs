﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
namespace mFicientWS
{
    public class DeviceRegistrationRequest
    {

        DeviceRegistrationRequestReq _deviceRegistrationReq;
        //string _userId="";
        public DeviceRegistrationRequest(DeviceRegistrationRequestReq deviceRegistrationReq)
        {
            _deviceRegistrationReq = deviceRegistrationReq;
        }
        public bool IsAutoApprove(out bool isRemoveDevice,out DataSet ds)
        {
            isRemoveDevice = false;
            bool isAutoApprove = false;
            string QUERY = @"SELECT *  FROM TBL_SUB_ADMIN WHERE SUBADMIN_ID = (SELECT SUBADMIN_ID  FROM TBL_USER_DETAIL  WHERE USER_ID = @UserId AND  COMPANY_ID=@COMPANY_ID);
            Select * from TBL_REGISTERED_DEVICE WHERE DEVICE_PUSH_MESSAGE_ID=@DEVICE_PUSH_MESSAGE_ID;
            Select * from TBL_DEVICE_REGISTRATION_REQUEST WHERE DEVICE_PUSH_MESSAGE_ID=@DEVICE_PUSH_MESSAGE_ID;
            SELECT MAX_DEVICE_PER_USER,REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION AS DREMOVE FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @COMPANY_ID;                           
            SELECT count(*) as maxDevice FROM TBL_REGISTERED_DEVICE WHERE COMPANY_ID = @COMPANY_ID AND USER_ID = @UserId;
            select top 1 * from TBL_REGISTERED_DEVICE where user_id= @UserId and company_id=@COMPANY_ID order by REGISTRATION_DATE asc;
            
            SELECT * FROM TBL_USER_DEVICE_SETTINGS WHERE COMPANY_ID = @COMPANY_ID AND USER_ID = @UserId;
            SELECT USER_IDS,GROUP_IDS  FROM dbo.TBL_USER_DEVICE_AUTOAPPROVE_SETTING where COMPANY_ID=@COMPANY_ID;
            select * from TBL_USER_GROUP_LINK where COMPANY_ID=@COMPANY_ID and USER_ID=@UserId;
            select Max_user from TBL_COMPANY_CURRENT_PLAN where company_id=@COMPANY_ID;
            select count(*) as Dcount from TBL_REGISTERED_DEVICE where company_id=@COMPANY_ID";

            SqlCommand cmd = new SqlCommand(QUERY);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _deviceRegistrationReq.CompanyId);
            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", _deviceRegistrationReq.PushMessageId);
            cmd.Parameters.AddWithValue("@UserId", _deviceRegistrationReq.UserId);
            ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            int maxDevice=-1;
            int maxRDevice=-1;
            if (ds != null)
            {
                if (ds.Tables[6].Rows.Count > 0)//vip user
                {
                    isAutoApprove= true;
                }
                else //normal user
                {// is remove oldest registered device
                    isRemoveDevice =  processRemoveOldestdevice(ds);
                    if (ds.Tables[7].Rows.Count > 0)
                    {
                        if (Convert.ToString(ds.Tables[7].Rows[0]["USER_IDS"]).Contains(_deviceRegistrationReq.UserId)) isAutoApprove = true;
                        else if (ds.Tables[8].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[8].Rows)
                                if (Convert.ToString(ds.Tables[7].Rows[0]["GROUP_IDS"]).Contains(Convert.ToString(dr["GROUP_ID"])))
                                {
                                    isAutoApprove = true;
                                    break;
                                }
                        }
                    }
                }
            }
            if (isAutoApprove)
            {
                if (ds.Tables[3].Rows.Count > 0)
                {                    
                    maxDevice = Convert.ToInt32(ds.Tables[9].Rows[0]["Max_user"]);
                    maxRDevice = Convert.ToInt32(ds.Tables[10].Rows[0]["Dcount"]);
                    if (maxDevice == -1) isAutoApprove = true;
                    else
                    {
                        if (maxRDevice < maxDevice) isAutoApprove = true;
                        else if (isRemoveDevice) isAutoApprove = true;
                        else isAutoApprove = false;
                    }
                }
            }
            return isAutoApprove;
        }

        public DeviceRegistrationRequestResp Process(HttpContext context)
        {
            bool isRemoveDevice = false;
            DataSet UserDeviceData = null;
            if (IsAutoApprove(out isRemoveDevice, out UserDeviceData))
            {
                return processForTestCompany(isRemoveDevice, UserDeviceData);
            }
            else
            {
                return processForRegisteredCompany(context);
            }
        }

        int saveDeviceRegistrationRequest(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_DEVICE_REGISTRATION_REQUEST(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,REQUEST_TYPE,REQUEST_DATETIME,STATUS,STATUS_DATETIME,DEVICE_MODEL,DEVICE_SIZE,OS_VERSION,DEVICE_PUSH_MESSAGE_ID,APP_VERSION)
                                VALUES(@CompanyId,@UserId,@DeviceType,@DeviceId,@RequestType,@RequestDatetime,@Status,@StatusDatetime,@DEVICE_MODEL,@DEVICE_SIZE,@OS_VERSION,@DEVICE_PUSH_MESSAGE_ID,@APP_VERSION)";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CompanyId", _deviceRegistrationReq.CompanyId);
                cmd.Parameters.AddWithValue("@UserId", _deviceRegistrationReq.UserId);
                cmd.Parameters.AddWithValue("@DeviceType", _deviceRegistrationReq.DeviceType);
                cmd.Parameters.AddWithValue("@DeviceId", _deviceRegistrationReq.DeviceId);
                cmd.Parameters.AddWithValue("@RequestType", _deviceRegistrationReq.RegistrationReqType);
                cmd.Parameters.AddWithValue("@RequestDatetime", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@Status", 0);
                cmd.Parameters.AddWithValue("@StatusDatetime", 0);
                cmd.Parameters.AddWithValue("@DEVICE_MODEL", _deviceRegistrationReq.Model);
                cmd.Parameters.AddWithValue("@DEVICE_SIZE", _deviceRegistrationReq.DeviceSize);//Device Size
                cmd.Parameters.AddWithValue("@OS_VERSION", _deviceRegistrationReq.OSVersion);//OS_VERSION
                cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", _deviceRegistrationReq.PushMessageId);
                cmd.Parameters.AddWithValue("@APP_VERSION", _deviceRegistrationReq.AppVersion);
                //return MSSqlClient.ExecuteNonQueryRecord(cmd);
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    return (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                return (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        DataSet getSubAdminDtlsByUserId()
        {
            string strQuery = @"SELECT *  FROM TBL_SUB_ADMIN WHERE 
            SUBADMIN_ID = (SELECT SUBADMIN_ID  FROM TBL_USER_DETAIL  WHERE USER_ID = @UserId AND  COMPANY_ID=@COMPANY_ID);
            Select * from TBL_REGISTERED_DEVICE WHERE DEVICE_PUSH_MESSAGE_ID=@DEVICE_PUSH_MESSAGE_ID;
            Select * from TBL_DEVICE_REGISTRATION_REQUEST WHERE DEVICE_PUSH_MESSAGE_ID=@DEVICE_PUSH_MESSAGE_ID;
            SELECT MAX_DEVICE_PER_USER,REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION AS DREMOVE FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @COMPANY_ID;                           
            SELECT count(*) as maxDevice FROM TBL_REGISTERED_DEVICE WHERE COMPANY_ID = @COMPANY_ID AND USER_ID = @UserId;
            select top 1 device_id,Device_TYPE from TBL_REGISTERED_DEVICE where user_id= @UserId and company_id=@COMPANY_ID order by REGISTRATION_DATE desc";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _deviceRegistrationReq.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _deviceRegistrationReq.CompanyId);
            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", _deviceRegistrationReq.PushMessageId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

        DataSet getRegistrationReqByUserCompanyAndDeviceIdAndDevType()
        {

            string strQuery = @"SELECT *
                                FROM TBL_DEVICE_REGISTRATION_REQUEST
                                WHERE USER_ID = @UserId
                                AND COMPANY_ID = @CompanyId
                                AND DEVICE_TYPE =@DeviceType
                                AND DEVICE_ID= @DeviceId
                                AND STATUS = 0";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _deviceRegistrationReq.UserId);
            cmd.Parameters.AddWithValue("@CompanyId", _deviceRegistrationReq.CompanyId);
            cmd.Parameters.AddWithValue("@DeviceType", _deviceRegistrationReq.DeviceType);
            cmd.Parameters.AddWithValue("@DeviceId", _deviceRegistrationReq.DeviceId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

        DataSet getCountOfPreviousReqByUserIdAndCompanyId()
        {
            string strQuery = @"SELECT COUNT(*) AS COUNT
                                FROM TBL_DEVICE_REGISTRATION_REQUEST
                                WHERE USER_ID = @UserId
                                AND COMPANY_ID = @CompanyId";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _deviceRegistrationReq.UserId);
            cmd.Parameters.AddWithValue("@CompanyId", _deviceRegistrationReq.CompanyId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);

        }

        int saveUpdationRequiredInfo(int updationType, SqlTransaction transaction, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(@"INSERT INTO [TBL_UPDATION_REQUIRED_DETAIL]
                                               ([COMPANY_ID],[USER_ID],[DEVICE_ID]
                                               ,[DEVICE_TYPE],[UPDATED_ON],[UPDATION_TYPE])
                                         VALUES
                                               (@COMPANY_ID,@USER_ID,@DEVICE_ID
                                               ,@DEVICE_TYPE,@UPDATED_ON,@UPDATION_TYPE)", con, transaction);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", _deviceRegistrationReq.CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", _deviceRegistrationReq.UserId);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _deviceRegistrationReq.DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _deviceRegistrationReq.DeviceType);
            cmd.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@UPDATION_TYPE", updationType);
            return cmd.ExecuteNonQuery();
        }

        DeviceRegistrationRequestResp processForTestCompany(bool isRemove,DataSet dsSubAdminDtls)
        {
            ResponseStatus objResponseStatus = new ResponseStatus();
            objResponseStatus.cd = "0";
            objResponseStatus.desc = String.Empty;
            SqlTransaction sqlTransaction = null;
            SqlConnection con = null;
            try
            {
                MFEMobileUser objMobileUser = getUserDetail();
                //int iCountOfUserDevices = this.getCountOfUserDevices();
                //MFEmBuzzServer objMBuzzServer = this.getMbuzzServerForComp();
               // string strRequestedByUserContacts = this.getRequestedByUserContacts(objMobileUser);
               
                MSSqlDatabaseClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        int isDeviceRemove = 0;
                        if (dsSubAdminDtls.Tables[1].Rows.Count > 0)
                        {
                            logDeletedDeviceData(sqlTransaction, con, _deviceRegistrationReq.CompanyId, _deviceRegistrationReq.UserId, _deviceRegistrationReq.PushMessageId);
                            isDeviceRemove = deleteRegisteredDeviceOfUser(sqlTransaction, con, _deviceRegistrationReq.PushMessageId);
                            //Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_DELETE, this.CompanyId, this.SubAdminId, this.UserId, strUserName,strFullName, this.DeviceId, this.DeviceType, this.DeviceModel, this.OSVersion, this.AppVersion);
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_DELETE, _deviceRegistrationReq.CompanyId, "-1", _deviceRegistrationReq.UserId, _deviceRegistrationReq.UserName, (objMobileUser.FirstName + " " + objMobileUser.LastName), _deviceRegistrationReq.DeviceId, _deviceRegistrationReq.DeviceType, _deviceRegistrationReq.Model, _deviceRegistrationReq.OSVersion, _deviceRegistrationReq.AppVersion, sqlTransaction);
                        }
                        allowDeviceToUser(sqlTransaction, con,objMobileUser.SubAdminId);
                        processSaveUpdationReqInfo(sqlTransaction, con);
                        Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_APPROVED, _deviceRegistrationReq.CompanyId, "-1", _deviceRegistrationReq.UserId, _deviceRegistrationReq.UserName,(objMobileUser.FirstName+" "+objMobileUser.LastName) , _deviceRegistrationReq.DeviceId, _deviceRegistrationReq.DeviceType, _deviceRegistrationReq.Model, _deviceRegistrationReq.OSVersion, _deviceRegistrationReq.AppVersion, sqlTransaction);
                        
                        if (isRemove && isDeviceRemove <= 0 && dsSubAdminDtls.Tables[5].Rows.Count>0)
                        {
                            logDeletedDeviceData(sqlTransaction, con, _deviceRegistrationReq.CompanyId, _deviceRegistrationReq.UserId, Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["device_id"]), Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["device_type"]));
                            deleteOldestRegisteredDeviceOfUser(sqlTransaction, con, Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["device_id"]));
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_DELETE, _deviceRegistrationReq.CompanyId, "-1", _deviceRegistrationReq.UserId, _deviceRegistrationReq.UserName, (objMobileUser.FirstName + " " + objMobileUser.LastName),
                                  Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["device_id"]), Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["DEVICE_TYPE"]), Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["DEVICE_MODEL"]), Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["OS_VERSION"]), Convert.ToString(dsSubAdminDtls.Tables[5].Rows[0]["APP_VERSION"]), sqlTransaction);
                        }

                        SqlConnection objSqlConnection = null;
                        try
                        {
                            if (!string.IsNullOrEmpty(_deviceRegistrationReq.PushMessageId))
                            {                                
                                MSSqlDatabaseClient.SqlConnectionOpen(out objSqlConnection, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                                MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
                                mFicientCommonProcess.SaveDevcDtlsInMGramInsertUpdate.SaveDeviceDetailInMgram(
                                _deviceRegistrationReq.CompanyId, _deviceRegistrationReq.DeviceId, _deviceRegistrationReq.DeviceType,
                                objMobileUser.Username, _deviceRegistrationReq.PushMessageId, objSqlConnection);
                                //SaveDevcDtlsInMGramInsertUpdate objSaveDevcInMGram = new SaveDevcDtlsInMGramInsertUpdate();
                                //objSaveDevcInMGram.Process();                          

                                mFicientCommonProcess.SendNotification.SaveNotificationTable(objSqlConnection, null, _deviceRegistrationReq.CompanyId,
                                objMobileUser.Username,"",0, mFicientCommonProcess.NOTIFICATION_TYPE.MF_DEVICE_APPRV, _deviceRegistrationReq.PushMessageId, _deviceRegistrationReq.DeviceType, _deviceRegistrationReq.DeviceId, "",1);
                            }
                        }
                        catch
                        { }
                        finally
                        {
                            if (objSqlConnection != null)
                                objSqlConnection.Close();
                        }


                        //if (iCountOfUserDevices == 0 &&
                        //    !Convert.ToBoolean(objMobileUser.DesktopMessenger))
                        //{
                        //    try
                        //    {
                        //        UserData objUserDataForMbuzz = new UserData();
                        //        objUserDataForMbuzz.unm = objMobileUser.Username;
                        //        objUserDataForMbuzz.fnm = objMobileUser.FirstName + " " + objMobileUser.LastName;
                        //        objUserDataForMbuzz.dep = String.Empty;
                        //        objUserDataForMbuzz.des = "";
                        //        objUserDataForMbuzz.em = Convert.ToString(objMobileUser.EmailId);
                        //        objUserDataForMbuzz.status = "1";

                        //        AddNewContactRequetForMbuzz addNewContact = new AddNewContactRequetForMbuzz(
                        //            Utilities.GetMsgRefID(),
                        //            objUserDataForMbuzz,
                        //            _deviceRegistrationReq.CompanyId,
                        //            strRequestedByUserContacts);

                        //        mBuzzClient objClient = new mBuzzClient(
                        //            addNewContact.RequestJson,
                        //            objMBuzzServer.ServerIpAddress,
                        //            Convert.ToInt32(objMBuzzServer.ServerPort)
                        //            );
                        //        ThreadPool.QueueUserWorkItem(new WaitCallback(objClient.Connect), null);
                        //    }
                        //    catch
                        //    { }
                        //}

                        sqlTransaction.Commit();
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
            return new DeviceRegistrationRequestResp(objResponseStatus, _deviceRegistrationReq.RequestId);
        }

        bool processRemoveOldestdevice(DataSet dsSubAdminDtls)
        {
            int maxDeviceSetting = -1, maxUserRegisteredDevice = 0;
            if (dsSubAdminDtls.Tables[3].Rows.Count > 0)
            {
                // is remove oldest registered device
                if (Convert.ToInt32(dsSubAdminDtls.Tables[3].Rows[0]["DREMOVE"]) <= 0) return false; 
                maxDeviceSetting = Convert.ToInt32(dsSubAdminDtls.Tables[3].Rows[0]["MAX_DEVICE_PER_USER"]);//max device per user in enterprise
            }
            if (dsSubAdminDtls.Tables[4].Rows.Count > 0)
            {
                maxUserRegisteredDevice = Convert.ToInt32(dsSubAdminDtls.Tables[4].Rows[0]["maxDevice"]);//user max registered device
            }
            if (maxUserRegisteredDevice >= maxDeviceSetting)
            {
                return true;//remove oldest device
            }
            return false;
        }

        DeviceRegistrationRequestResp processForRegisteredCompany(HttpContext context)
        {
            //Check list
            //a user can make only 5 requests
            //a request cannot be made again for the same device type and id

            ResponseStatus objResponseStatus = new ResponseStatus();
            objResponseStatus.cd = "0";
            objResponseStatus.desc = "";
            try
            {
                int iExecuteReturnValue = 0;
                //check list 1

                DataSet dsPreviousCountOfReq = getCountOfPreviousReqByUserIdAndCompanyId();

                if (dsPreviousCountOfReq == null && dsPreviousCountOfReq.Tables == null && dsPreviousCountOfReq.Tables.Count == 0)
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                else
                {
                    if ((int)dsPreviousCountOfReq.Tables[0].Rows[0]["COUNT"] == 5)
                    {
                        //make the response status
                        objResponseStatus.cd = "1300201";
                        objResponseStatus.desc = "A User can register maximum five devices";
                        return new DeviceRegistrationRequestResp(objResponseStatus, _deviceRegistrationReq.RequestId);
                    }
                }

                //check list 2
                DataSet dsPreviousDeviceRegisterReq = getRegistrationReqByUserCompanyAndDeviceIdAndDevType();
                if (dsPreviousDeviceRegisterReq == null && dsPreviousDeviceRegisterReq.Tables.Count == 0)
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
                if (dsPreviousDeviceRegisterReq.Tables[0].Rows.Count > 0)
                {
                    //make the response status
                    objResponseStatus.cd = "1300202";
                    objResponseStatus.desc = "A registration request for this device already exists";
                }
                else
                {
                    DataSet dsSubAdminDtls = getSubAdminDtlsByUserId();
                    SqlTransaction transaction = null;
                    SqlConnection con;
                    MSSqlDatabaseClient.SqlConnectionOpen(out con);
                    try
                    {
                        using (con)
                        {
                            using (transaction = con.BeginTransaction())
                            {
                                if (dsSubAdminDtls.Tables[1].Rows.Count > 0 )
                                {
                                    logDeletedDeviceData(transaction,con,_deviceRegistrationReq.CompanyId,_deviceRegistrationReq.UserId,_deviceRegistrationReq.PushMessageId);
                                    deleteRegisteredDeviceOfUser(transaction, con, _deviceRegistrationReq.PushMessageId);
                                    Utilities.saveSubAdminActivityLog("S", "-1", _deviceRegistrationReq.UserId, _deviceRegistrationReq.CompanyId, SUBADMIN_ACTIVITY_LOG.MOBILE_DEVICE_DELETE, DateTime.UtcNow.Ticks,Convert.ToString(dsSubAdminDtls.Tables[1].Rows[0]["DEVICE_ID"]), Convert.ToString(dsSubAdminDtls.Tables[1].Rows[0]["DEVICE_TYPE"]), "", "", con, transaction);
                                }
                                if (dsSubAdminDtls.Tables[2].Rows.Count > 0)
                                {
                                    deleteDeviceRegisterationOfUser(transaction, con, _deviceRegistrationReq.PushMessageId);
                                    Utilities.saveSubAdminActivityLog("S", "-1", _deviceRegistrationReq.UserId, _deviceRegistrationReq.CompanyId, SUBADMIN_ACTIVITY_LOG.MOBILE_DEVICE_DENY, DateTime.UtcNow.Ticks, Convert.ToString(dsSubAdminDtls.Tables[2].Rows[0]["DEVICE_ID"]), Convert.ToString(dsSubAdminDtls.Tables[2].Rows[0]["DEVICE_TYPE"]), "", "", con, transaction);
                                }
                                iExecuteReturnValue = saveDeviceRegistrationRequest(con, transaction);
                                if ((iExecuteReturnValue != (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR &&
                                    iExecuteReturnValue != (int)DATABASE_ERRORS.RECORD_INSERT_ERROR) &&
                                    iExecuteReturnValue > 0)
                                {
                                    
                                    bool blnEmailSendSuccessful = SaveEmailInfo.deviceRegistration(
                                         Convert.ToString(dsSubAdminDtls.Tables[0].Rows[0]["EMAIL"]),
                                         Convert.ToString(dsSubAdminDtls.Tables[0].Rows[0]["USER_NAME"]),
                                         _deviceRegistrationReq.CompanyId,
                                         _deviceRegistrationReq.UserId,
                                         context);
                                    if (!blnEmailSendSuccessful) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                    
                                    transaction.Commit();
                                    if (dsSubAdminDtls.Tables[1].Rows.Count > 0)
                                    {
                                        deleteRegisteredDeviceOfUserFromMgram(_deviceRegistrationReq.PushMessageId);
                                    }
                                    //make the response status
                                }
                                else
                                {
                                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                }
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                        {
                            throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                        }
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                    catch (Exception e)
                    {
                        if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                        {
                            throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                        }
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                    finally
                    {
                        if (con != null)
                        {
                            con.Dispose();
                        }
                        if (transaction != null)
                        {
                            transaction.Dispose();
                        }
                    }
                }
            }
            catch
            {
                throw new Exception();
            }
            return new DeviceRegistrationRequestResp(objResponseStatus, _deviceRegistrationReq.RequestId);
        }

        MFEMobileUser getUserDetail()
        {
            MFEMobileUser objMobileUser =
                new MFEMobileUser();
            WSCGetUserDetail objUserDetail =
                new WSCGetUserDetail(
                    _deviceRegistrationReq.UserId,
                    _deviceRegistrationReq.CompanyId,_deviceRegistrationReq.DeviceId);
            objMobileUser = objUserDetail.getUserDetail();
            if (objUserDetail.StatusCode != 0) throw new Exception();
            return objMobileUser;
        }

        //MFEDepartment getUserDept(WSCGetDivDeptOfUser divDeptOfUserAfterProcess)
        //{
        //    if (divDeptOfUserAfterProcess.StatusCode != 0) throw new Exception();
        //    return divDeptOfUserAfterProcess.Department;
        //}

        //MFEDivision getUserDiv(WSCGetDivDeptOfUser divDeptOfUserAfterProcess)
        //{
        //    if (divDeptOfUserAfterProcess.StatusCode != 0) throw new Exception();
        //    return divDeptOfUserAfterProcess.Division;
        //}

        //MFEmBuzzServer getMbuzzServerForComp()
        //{
        //    GetMbuzzServerDetail objGetMbuzzServerDtl = new GetMbuzzServerDetail();
        //    MFEmBuzzServer objMbuzzServer = objGetMbuzzServerDtl.getMbuzzServerForCompany(
        //        _deviceRegistrationReq.CompanyId,null);
        //    if (objGetMbuzzServerDtl.StatusCode != 0) throw new Exception();
        //    return objMbuzzServer;
        //}

       

        //string getRequestedByUserContacts(MFEMobileUser mobileUser)
        //{
        //    WSCGetUserDetail objUserDetail =
        //        new WSCGetUserDetail(
        //            _deviceRegistrationReq.UserId,
        //            _deviceRegistrationReq.CompanyId, _deviceRegistrationReq.DeviceId);
        //    return objUserDetail.getUserContactsFromRequestedBy(mobileUser.RequestedBy);
        //}

        //int getCountOfUserDevices()
        //{
        //    WSCGetRegisteredDevForUser objUserDev =
        //        new WSCGetRegisteredDevForUser(
        //            _deviceRegistrationReq.CompanyId,
        //            _deviceRegistrationReq.UserId);
        //    List<MFEDevice> lstRegisteredDevice = objUserDev.getRegisteredDevices();
        //    if (objUserDev.StatusCode != 0) throw new Exception();
        //    return lstRegisteredDevice.Count;
        //}

        int allowDeviceToUser(SqlTransaction transaction, SqlConnection con, string subadminID)
        {
            string strSqlQuery = getSqlQueryForAllowDevice();
            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@COMPANY_ID", _deviceRegistrationReq.CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", _deviceRegistrationReq.UserId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _deviceRegistrationReq.DeviceType);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _deviceRegistrationReq.DeviceId);
            cmd.Parameters.AddWithValue("@REGISTRATION_DATE", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@SUBADMIN_ID", subadminID);
            cmd.Parameters.AddWithValue("@DEVICE_MODEL", _deviceRegistrationReq.Model);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", _deviceRegistrationReq.DeviceSize);

            cmd.Parameters.AddWithValue("@MFICIENT_KEY",
                Utilities.GetMd5Hash(_deviceRegistrationReq.DeviceSize +
                _deviceRegistrationReq.DeviceId +
                _deviceRegistrationReq.CompanyId +
                _deviceRegistrationReq.UserId).Substring(0, 10));

            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", _deviceRegistrationReq.PushMessageId);
            cmd.Parameters.AddWithValue("@OS_VERSION", _deviceRegistrationReq.OSVersion);
            cmd.Parameters.AddWithValue("@APP_VERSION", _deviceRegistrationReq.AppVersion);
            return cmd.ExecuteNonQuery();
        }

        string getSqlQueryForAllowDevice()
        {
            string strSqlQuery = @"INSERT INTO [TBL_REGISTERED_DEVICE]
                                    ([USER_ID]
                                    ,[COMPANY_ID]
                                    ,[DEVICE_TYPE]
                                    ,[DEVICE_ID]
                                    ,[REGISTRATION_DATE]
                                    ,[SUBADMIN_ID]
                                    ,[DEVICE_MODEL]
                                    ,[DEVICE_SIZE]
                                    ,[MFICIENT_KEY]
                                    ,[DEVICE_PUSH_MESSAGE_ID]
                                    ,[OS_VERSION],[APP_VERSION])
                                VALUES
                                    (@USER_ID
                                    ,@COMPANY_ID
                                    ,@DEVICE_TYPE
                                    ,@DEVICE_ID
                                    ,@REGISTRATION_DATE
                                    ,@SUBADMIN_ID
                                    ,@DEVICE_MODEL
                                    ,@DEVICE_SIZE
                                    ,@MFICIENT_KEY
                                    ,@DEVICE_PUSH_MESSAGE_ID
                                    ,@OS_VERSION,@APP_VERSION)";
            return strSqlQuery;
        }

        void processSaveUpdationReqInfo(SqlTransaction transaction, SqlConnection con)
        {
            saveUpdationRequiredInfo((int)MOBILE_UPDATE_TYPE.COMPANY_LOGO_UPDATE, transaction, con);
            saveUpdationRequiredInfo((int)MOBILE_UPDATE_TYPE.USER_DETAILS_UPDATE, transaction, con);
            saveUpdationRequiredInfo((int)MOBILE_UPDATE_TYPE.MENU_CATEGORY_UPDATE, transaction, con);
        }

        int logDeletedDeviceData(SqlTransaction transaction, SqlConnection con, string CompanyId, string UserId, string DEVICE_PUSH_MESSAGEID)
        {
            string strSqlQuery = @"INSERT INTO TBL_DELETED_DEVICE(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,DELETED_DATE,DEVICE_MODEL,DEVICE_SIZE)
                                SELECT COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,@DateTimeNow,DEVICE_MODEL,DEVICE_SIZE
                                FROM TBL_REGISTERED_DEVICE
                                WHERE USER_ID = @UserId AND DEVICE_PUSH_MESSAGE_ID = @DEVICE_PUSH_MESSAGEID AND COMPANY_ID = @CompanyId;";

            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGEID", DEVICE_PUSH_MESSAGEID);
            cmd.Parameters.AddWithValue("@DateTimeNow", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);

            return cmd.ExecuteNonQuery();

        }
        int logDeletedDeviceData(SqlTransaction transaction, SqlConnection con, string CompanyId, string UserId, string DeviceId, string DeviceType)
        {
            string strSqlQuery = @"INSERT INTO TBL_DELETED_DEVICE(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,DELETED_DATE,DEVICE_MODEL,DEVICE_SIZE)
                                SELECT COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,@DateTimeNow,DEVICE_MODEL,DEVICE_SIZE
                                FROM TBL_REGISTERED_DEVICE
                                WHERE USER_ID = @UserId AND DEVICE_ID = @DEVICE_ID AND DEVICE_TYPE=@DEVICE_TYPE AND COMPANY_ID = @CompanyId;";

            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@DEVICE_ID", DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", DeviceType);
            cmd.Parameters.AddWithValue("@DateTimeNow", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);

            return cmd.ExecuteNonQuery();

        }

        int deleteRegisteredDeviceOfUser(SqlTransaction transaction, SqlConnection con, string DEVICE_PUSH_MESSAGEID)
        {
            if (string.IsNullOrEmpty(DEVICE_PUSH_MESSAGEID)) return 0 ;
            string strQuery = @"DELETE FROM TBL_REGISTERED_DEVICE
                                WHERE DEVICE_PUSH_MESSAGE_ID = @DEVICE_PUSH_MESSAGEID";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGEID", DEVICE_PUSH_MESSAGEID);
            //execute the query

            return cmd.ExecuteNonQuery();
        }
        int deleteOldestRegisteredDeviceOfUser(SqlTransaction transaction, SqlConnection con, string DEVICEID)
        {
            string strQuery = @"DELETE FROM TBL_REGISTERED_DEVICE WHERE DEVICE_ID = @DEVICE_ID and company_id=@company_id and user_ID=@userID";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@DEVICE_ID", DEVICEID);
            cmd.Parameters.AddWithValue("@company_id", _deviceRegistrationReq.CompanyId);
            cmd.Parameters.AddWithValue("@userID", _deviceRegistrationReq.UserId);
            //execute the query

            return cmd.ExecuteNonQuery();
        }
        int deleteRegisteredDeviceOfUserFromMgram(string DEVICE_PUSH_MESSAGEID)
        {
            SqlConnection con = new SqlConnection(MficientConstants.strmGramConection);
            try
            {                
                con.Open();
                string strQuery = @"DELETE FROM TBL_USER_DEVICE
                                WHERE DEVICE_PUSH_MESSAGE_ID = @DEVICE_PUSH_MESSAGE_ID";
                SqlCommand cmd = new SqlCommand(strQuery, con);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", DEVICE_PUSH_MESSAGEID);
                //execute the query

                return cmd.ExecuteNonQuery();
            }
            catch
            {
                
            }
            finally
            {
                if(con!=null)
                    con.Close();
            }
            return 0;
        }

        int deleteDeviceRegisterationOfUser(SqlTransaction transaction, SqlConnection con,string DEVICE_PUSH_MESSAGEID)
        {
            if (string.IsNullOrEmpty(DEVICE_PUSH_MESSAGEID)) return 0;
            string strQuery = @"DELETE FROM TBL_DEVICE_REGISTRATION_REQUEST
                                WHERE  DEVICE_PUSH_MESSAGE_ID = @DEVICE_PUSH_MESSAGEID";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGEID", DEVICE_PUSH_MESSAGEID);
            //execute the query

            return cmd.ExecuteNonQuery();
        }
    }
}