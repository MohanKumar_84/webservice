﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Collections;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Text;
using System.Xml.Serialization;

namespace mFicientWS
{
    public class DoTask
    {
        string Stscode = "";        
        DoTaskReq _doTaskRequest;
        ResponseStatus objRespStatus = new ResponseStatus();
        public DoTask(DoTaskReq doTaskRequest)
        {
            _doTaskRequest = doTaskRequest;
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
        }
        public DoTaskResp Process()
        {
            string commandName = "";
            int flagStatus =1;
            DoTaskResp resp = new DoTaskResp();
            try
            {
                if (string.IsNullOrEmpty(_doTaskRequest.CommandType) || _doTaskRequest.CommandType == "-1")
                {
                    DataBaseCommand db = new DataBaseCommand(_doTaskRequest.CommandId, _doTaskRequest.CompanyId, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);
                    if (db.isExist)
                    {
                        commandName = db.CommandName;
                        resp= db.processDBCommand(_doTaskRequest);
                    }
                    else
                    {
                        WebServiceCommand ws = new WebServiceCommand(_doTaskRequest.CommandId, _doTaskRequest.CompanyId);
                        if (ws.isExist)
                        {
                            commandName = ws.CommandName;
                            resp= ws.processWSCommand(_doTaskRequest);
                        }
                        else
                        {
                            OdataCommand objODataCommand = new OdataCommand(_doTaskRequest.CommandId, _doTaskRequest.CompanyId);
                            if (objODataCommand.isExist)
                            {
                                commandName = objODataCommand.CommandName;
                                resp= objODataCommand.processODataCommand(_doTaskRequest);
                            }
                            else
                                resp=new DoTaskResp(Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND).ToString())),
                                    _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, "", "", "","",new List<List<string>>());
                        }
                    }
                }
                else
                {
                    switch (Convert.ToInt32(_doTaskRequest.CommandType))
                    {
                        case (int)RequestCommandType.Database:
                            DataBaseCommand db = new DataBaseCommand(_doTaskRequest.CommandId, _doTaskRequest.CompanyId, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);
                            if (db.isExist)
                            {
                                commandName = db.CommandName;
                            }
                            resp= db.processDBCommand(_doTaskRequest);break;
                        case (int)RequestCommandType.WebService:
                        case (int)RequestCommandType.WebServiceSoap:
                        case (int)RequestCommandType.WebServiceRPCXML:
                            WebServiceCommand ws = new WebServiceCommand(_doTaskRequest.CommandId, _doTaskRequest.CompanyId);
                            if (ws.isExist)
                            {
                                commandName = ws.CommandName;
                            }
                            resp= ws.processWSCommand(_doTaskRequest);break;
                        case (int)RequestCommandType.ODATA:
                            OdataCommand objODataCommand = new OdataCommand(_doTaskRequest.CommandId, _doTaskRequest.CompanyId);
                            if (objODataCommand.isExist)
                            {
                                commandName = objODataCommand.CommandName;
                            }
                            resp= objODataCommand.processODataCommand(_doTaskRequest);break;
                        default:
                            resp = new DoTaskResp(Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND_TYPE).ToString())),
                                _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, "", "", "","",new List<List<string>>());
                            break;

                    }
                    
                }
                flagStatus = 2;
                return resp;
            }
            catch (Exception e)
            {
                flagStatus =0;
                throw e;
            }
            finally
            {
                try
                {
                    if (!string.IsNullOrEmpty(_doTaskRequest.UserId))
                    {
                        SqlConnection con;
                        MSSqlDatabaseClient.SqlConnectionOpen(out con);
                        string AppName = "";
                        string UserName = getUserAndAppDetai(_doTaskRequest.UserId, con, out AppName);
                        string Lp = Utilities.SerializeJson<List<QueryParameters>>(_doTaskRequest.Parameters);
                        Stscode = Stscode.Length == 0 ? objRespStatus.cd : Stscode;
                        string extraJSON = @"{""lp"":" + Lp + @",""rcd"":""" + Stscode + @""",""cmd"":""" + _doTaskRequest.CommandId + @""",""dtyp"":""" + _doTaskRequest.DeviceType + @""",""resp"":""" + (flagStatus == 0 ? "-1" : resp._respStatus.cd) + @"""}";
                        Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.OBJECT_EXECUTE, _doTaskRequest.CompanyId, "", _doTaskRequest.UserId, _doTaskRequest.UserName, UserName, _doTaskRequest.DeviceId, _doTaskRequest.Model, AppName, commandName, extraJSON, null);
                    }
                }
                catch { }
            }

        }

        string getUserAndAppDetai(string userId, SqlConnection con, out string AppName)
        {
            AppName = "";
            string query = @"select WF_NAME,WF_ID from TBL_CURR_WORKFLOW_DETAIL where WF_ID=@WF_ID ;
                               Select FIRST_NAME,LAST_NAME from tbl_User_detail where User_ID=@User_ID";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@WF_ID", _doTaskRequest.AppID);
            cmd.Parameters.AddWithValue("@User_ID", userId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                    AppName = ds.Tables[0].Rows[0]["WF_NAME"].ToString();
                if (ds.Tables[1].Rows.Count > 0)
                    return ds.Tables[1].Rows[0]["FIRST_NAME"].ToString() + " " + ds.Tables[1].Rows[0]["LAST_NAME"].ToString();
            }
            return "";
        }
    }
}