﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DoTaskReq
    {
        string _userId, _requestId, _companyId, _commandId, _deviceId, _deviceType, _controlId, _userName, _commandType, _appId, _model;
        int _functionCode, _rtfn, _rcdPagingCount, _pgNumber;
        List<QueryParameters> _parameters;
        //QueryReturnValue _returnType;
        DataConnCredential _dataCredential;
        public List<QueryParameters> Parameters
        {
            get { return _parameters; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string CommandType
        {
            get { return _commandType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string CommandId
        {
            get { return _commandId; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string ControlId
        {
            get { return _controlId; }
        }
        public int PageNumber
        {
            get { return _pgNumber; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public int RcdPagingCount
        {
            get { return _rcdPagingCount; }
        }

        public string Model
        {
            get { return _model; }
        }
        public string AppID
        {
            get { return _appId; }
        }
        public DataConnCredential DataCredential
        {
            get { return _dataCredential; }
        }
        public DoTaskReq(string rid, string enid, string cmd, string ctyp, List<QueryParameters> lp, DataConnCredential cr, string userId, string model, string appId, string userName, string deviceID)
        {
            _requestId = rid;
            _companyId = enid;
            _commandId = cmd;
            _commandType = ctyp;
            _parameters = lp;
            _rcdPagingCount = 0;
            _controlId = "";
            _pgNumber = 0;
            _dataCredential = cr;
            _userId = userId;
            _appId = appId;
            _model = model == null ? "" : model;
            _userName = userName;
            _deviceId=deviceID;
        }
        public DoTaskReq(string requestJson, string requestID, out string cid)
        {
            DoTaskReqData objRequestJsonParsing = Utilities.DeserialiseJson<DoTaskReqData>(requestJson);
            _userId = "";
            _requestId = requestID;
            cid = _companyId = objRequestJsonParsing.enid;
            _userName = "sub-admin";
            _commandId = objRequestJsonParsing.cmd;
            _commandType = objRequestJsonParsing.ctyp;
            _parameters = objRequestJsonParsing.lp;
            _rcdPagingCount = 0;
            _pgNumber = 1;
            _dataCredential = new DataConnCredential();
            _dataCredential.unm = objRequestJsonParsing.cr.unm;
            _dataCredential.pwd = objRequestJsonParsing.cr.pwd;
            _controlId = "";
        }
        public DoTaskReq(string requestJson, string userId, string model)
        {
            RequestJsonParsing<DoTaskReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<DoTaskReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.DO_TASK)
            {
                throw new Exception("Invalid Function Code");
            }
            _userId = userId;
            _deviceId = objRequestJsonParsing.req.did;
            _userName = objRequestJsonParsing.req.data.unm;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _commandId = objRequestJsonParsing.req.data.cmd;
            _commandType = objRequestJsonParsing.req.data.ctyp;
            _parameters = objRequestJsonParsing.req.data.lp;
            _rcdPagingCount = objRequestJsonParsing.req.data.rcdpg;
            _appId = objRequestJsonParsing.req.data.appId;
            _appId = _appId == null ? "" : _appId;
            _model = model == null ? "" : model;
            _controlId = objRequestJsonParsing.req.data.ctrlid;
            _pgNumber = 1;
            _dataCredential = objRequestJsonParsing.req.data.cr;
            if (_parameters == null || String.IsNullOrEmpty(_commandId))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }

            if (_dataCredential != null)
            {
                _dataCredential.pwd = _dataCredential.pwd;// EncryptionDecryption.AESDecrypt(this.CompanyId.ToUpper(), _dataCredential.pwd);
                _dataCredential.unm = _dataCredential.unm;// EncryptionDecryption.AESDecrypt(this.CompanyId.ToUpper(), _dataCredential.unm);
            }
            else
            {
                _dataCredential = new DataConnCredential();
                _dataCredential.pwd = "";
                _dataCredential.unm = "";
            }
        }
    }

    [DataContract]
    public class DoTaskReqData : Data
    {

        /// <summary>
        /// Control Id                    
        /// </summary>
        [DataMember]
        public string ctrlid { get; set; }

        /// <summary>
        /// Command Id
        /// </summary>
        [DataMember]
        public string cmd { get; set; }

        /// <summary>
        /// Controltype
        /// </summary>
        [DataMember]
        public int rtfn { get; set; }

        /// <summary>
        /// Command Type
        /// </summary>
        [DataMember]
        public string ctyp { get; set; }

        /// <summary>
        /// Record Paging Count
        /// </summary>
        [DataMember]
        public int rcdpg { get; set; }

        [DataMember]
        public string appId { get; set; }

        /// <summary>
        /// Page Number
        /// </summary>
        [DataMember]
        public int pgno { get; set; }

        /// <summary>
        /// Parameters for query
        /// </summary>
        [DataMember]
        public List<QueryParameters> lp { get; set; }

        ///// <summary>
        ///// Return Value
        ///// </summary>
        //[DataMember]
        //public QueryReturnValue rt { get; set; }

        /// <summary>
        /// Credential
        /// </summary>
        [DataMember]
        public DataConnCredential cr { get; set; }

    }

    [DataContract]
    public class QueryParameters
    {
        public QueryParameters(string _para, string _val)
        {
            para = _para;
            val = _val;
        }
        [DataMember]
        public string para { get; set; }

        [DataMember]
        public string val { get; set; }
    }

    //[DataContract]
    //public class QueryReturnValue
    //{
    //    [DataMember]
    //    public string i { get; set; }
    //    [DataMember]
    //    public string b { get; set; }
    //    [DataMember]
    //    public string s { get; set; }
    //    [DataMember]
    //    public string r { get; set; }
    //    [DataMember]
    //    public string c { get; set; }
    //    [DataMember]
    //    public string n { get; set; }
    //    [DataMember]
    //    public string an { get; set; }

    //    [DataMember]
    //    public string t { get; set; }
    //    [DataMember]
    //    public string v { get; set; }


    //    [DataMember]
    //    public string v1 { get; set; }
    //    [DataMember]
    //    public string v2 { get; set; }
    //    [DataMember]
    //    public string v3 { get; set; }
    //    [DataMember]
    //    public string v4 { get; set; }
    //    [DataMember]
    //    public string v5 { get; set; }
    //    [DataMember]
    //    public List<EditableListVarToReturn> prop { get; set; }

    //}

    [DataContract]
    public class EditableListVarToReturn
    {
        [DataMember]
        public string dtcol { get; set; }

        [DataMember]
        public string name { get; set; }
    }

    [DataContract]
    public class QueryParametersType
    {
        [DataMember]
        public string para { get; set; }

        [DataMember]
        public string typ { get; set; }
    }

    [DataContract]
    public class DataConnCredential
    {
        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string pwd { get; set; }
    }
}