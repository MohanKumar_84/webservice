﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using Npgsql;
using System.Net;
using System.Collections;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Text;
using System.Xml.Serialization;
namespace mFicientWS
{
    public class DoTaskWebServiceForBatchProcess
    {
        #region Private Members
        //TODO Trim i j k value from request
        //TODO Haven't completed select all in xml rpc.
        enum RequestCommandType
        {
            Database = 1,
            WebService = 2,
            WebServiceSoap = 3
        }
        //int sqlcomdtype = 0;
        DoTaskReqForBatchProcess _doTaskRequest;
        //List<RespDataForCommand> _respDataForCmd;

        /*Select All and Table Manipulation Cmnds have the same function
         * code coming from mobile.To make distinction between the two
         * this is used
        */
        //bool _isCmdForSelectingAllClmnsAndRows = false;
        #endregion

        public DoTaskWebServiceForBatchProcess(DoTaskReqForBatchProcess doTaskRequest)
        {
            _doTaskRequest = doTaskRequest;
        }

        public DoTaskRespForBatchProcess Process(bool isTransaction)
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = String.Empty;
            List<RespDataForCommand> lstRespDataForCommand = new List<RespDataForCommand>();
            bool blnProcessSuccessfull = false;
            //sorting the request DotaskCmdDetails list
            _doTaskRequest.Commands = getSortedCmdDetailsList(_doTaskRequest.Commands);
            try
            {
                processRunBatchProcess(out lstRespDataForCommand, out blnProcessSuccessfull, isTransaction);
                if (!blnProcessSuccessfull)
                {
                    objRespStatus = Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.TRANSACTION_ERROR).ToString()));
                }
            }
            catch (mPlugin.mPluginException mex)
            {
                try
                {
                    objRespStatus = Utilities.getResponseStatus(mex);
                }
                catch
                {
                    objRespStatus = Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()));
                }
            }
            catch (HttpException hex)
            {
                objRespStatus = Utilities.HttpExecptionToResponseStatus(hex);
            }
            catch (Exception ex)
            {
                objRespStatus = Utilities.getResponseStatus(ex);
            }
            //catch (Exception e)
            //{
            //    objRespStatus = getResponseStatus(e);
            //    lstRespDataForCommand = new List<RespDataForCommand>();
            //}
            return new DoTaskRespForBatchProcess(objRespStatus, _doTaskRequest.RequestId, lstRespDataForCommand);
        }
        #region private methods

        List<DoTaskCmdDetails> getSortedCmdDetailsList(List<DoTaskCmdDetails> cmdDetails)
        {
            List<DoTaskCmdDetails> objSortedList = new List<DoTaskCmdDetails>();
            int iMaxLoopCount = cmdDetails.Count;
            foreach (DoTaskCmdDetails cmdDtl in cmdDetails)
            {
                cmdDtl.index = Convert.ToInt32(cmdDtl.idx);
            }
            objSortedList.AddRange(cmdDetails.OrderBy(x => x.index));
            return objSortedList;
        }
        #endregion
        #region BatchProcess
        List<DataBaseCommand> getDbCommandsByCommandIdForBatchProcess(List<DoTaskCmdDetails> cmdDtlsList, string companyId)
        {
            List<DataBaseCommand> lstCommandDtls = new List<DataBaseCommand>(cmdDtlsList.Count);
            foreach (DoTaskCmdDetails objCmdDtls in cmdDtlsList)
            {
                DataBaseCommand objDatabaseCmd = new DataBaseCommand(objCmdDtls.cmd, companyId, objCmdDtls.cr.unm, objCmdDtls.cr.pwd);
                //objDatabaseCmd.getCommandDetails();
                lstCommandDtls.Add(objDatabaseCmd);
            }
            return lstCommandDtls;
        }

        DataBaseConnector getDbConnector(string connectorId, string companyId, string userId, string password)
        {
            DataBaseConnector objConnector = new DataBaseConnector(connectorId, companyId, userId, password);
            objConnector.GetConnector();
            return objConnector;
        }

        void processRunBatchProcess(out List<RespDataForCommand> respDataForCmds, out bool processSuccessfull, bool isTransaction)
        {
            respDataForCmds = null;
            processSuccessfull = false;
            try
            {
                List<DataBaseCommand> lstDbCmdsDetail = getDbCommandsByCommandIdForBatchProcess(_doTaskRequest.Commands,
                                                                                                _doTaskRequest.CompanyId
                                                                                                );
                DataBaseConnector objDbConnector = getDbConnector(lstDbCmdsDetail[0].ConnectorId, _doTaskRequest.CompanyId, _doTaskRequest.Commands[0].cr.unm, _doTaskRequest.Commands[0].cr.pwd);
                if (string.IsNullOrEmpty(lstDbCmdsDetail[0].MPluginAgent))
                {
                    string userId = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.Commands[0].cr.unm);
                    string password = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.Commands[0].cr.pwd);
                    objDbConnector.getConnectionString(userId, password);
                    switch (objDbConnector.DataBaseType)
                    {
                        case DatabaseType.OTHER:
                            break;
                        case DatabaseType.MSSQL:
                            runMsSqlBatchProcess(lstDbCmdsDetail, objDbConnector, _doTaskRequest.Commands, out respDataForCmds, out processSuccessfull, isTransaction);
                            break;
                        case DatabaseType.ORACLE:
                            runMySqlBatchProcess(lstDbCmdsDetail, objDbConnector, _doTaskRequest.Commands, out respDataForCmds, out processSuccessfull, isTransaction);
                            break;
                        case DatabaseType.MYSQL:
                            runMySqlBatchProcess(lstDbCmdsDetail, objDbConnector, _doTaskRequest.Commands, out respDataForCmds, out processSuccessfull, isTransaction);
                            break;
                        case DatabaseType.POSTGRESQL:
                            runPostGreSqlBatchProcess(lstDbCmdsDetail, objDbConnector, _doTaskRequest.Commands, out respDataForCmds, out processSuccessfull, isTransaction);

                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    //throw new Exception();
                    runDBBatchProcessUsingMplugin(lstDbCmdsDetail, objDbConnector, _doTaskRequest.Commands, out respDataForCmds, out processSuccessfull, _doTaskRequest.Commands[0].cr.unm, _doTaskRequest.Commands[0].cr.pwd, isTransaction);
                }
            }
            catch (mPlugin.mPluginException mex)
            {
                throw mex;
            }
            catch (HttpException hex)
            {
                throw hex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void runDBBatchProcessUsingMplugin(List<DataBaseCommand> lstDbCmdsDetail, DataBaseConnector dbConnector, List<DoTaskCmdDetails> cmdDtlsList, out List<RespDataForCommand> respDataForCmds, out bool processSuccessfull, string username, string password, bool isTransaction)
        {
            respDataForCmds = new List<RespDataForCommand>();
            processSuccessfull = false;
            try
            {
                List<mPluginForListCommand.CommandsList> MpluginCmdLst = new List<mPluginForListCommand.CommandsList>();
                int index = 0;
                foreach (DataBaseCommand objDataBaseCommand in lstDbCmdsDetail)
                {
                    mPluginForListCommand.CommandsList objmPluginCmd = new mPluginForListCommand.CommandsList();
                    objmPluginCmd.connid = objDataBaseCommand.ConnectorId;
                    objmPluginCmd.rtyp = _doTaskRequest.Commands[0].ctyp.ToString();
                    objmPluginCmd.dbtyp = ((int)objDataBaseCommand.DBConnectorType).ToString();
                    objmPluginCmd.sql = objDataBaseCommand.SqlQuery;
                    List<mPluginForListCommand.SqlParameter> objParamsForCmd = new List<mPluginForListCommand.SqlParameter>();
                    DoTaskCmdDetails cmdDtls = cmdDtlsList[index];
                    List<mPluginForListCommand.CmdParamsList> lstCmdParamsList = new List<mPluginForListCommand.CmdParamsList>();
                    foreach (DoTaskCmdParameters cmdParams in cmdDtls.cmdlp)
                    {
                        objParamsForCmd = new List<mPluginForListCommand.SqlParameter>();
                        foreach (QueryParameters para in cmdParams.lp)
                        {
                            objParamsForCmd.Add(new mPluginForListCommand.SqlParameter(para.para, para.val));
                        }
                        mPluginForListCommand.CmdParamsList objCmdParamsList = new mPluginForListCommand.CmdParamsList();
                        objCmdParamsList.param = objParamsForCmd;
                        lstCmdParamsList.Add(objCmdParamsList);
                    }
                    objmPluginCmd.cmdlp = lstCmdParamsList;
                    objmPluginCmd.querytype = ((int)objDataBaseCommand.DataBaseCommandType).ToString();
                    objmPluginCmd.idx = cmdDtls.idx;
                    objmPluginCmd.uid = username;
                    objmPluginCmd.pwd = password;
                    MpluginCmdLst.Add(objmPluginCmd);
                    index++;
                }
                List<mPluginForListCommand.ResponseCommandsData> objmPluginResp = mPluginForListCommand.RunCommandsAndGetResponse(_doTaskRequest.CompanyId, lstDbCmdsDetail[0].MPluginAgent, lstDbCmdsDetail[0].MPluginAgentPwd, MpluginCmdLst, true, isTransaction);
                if (objmPluginResp.Count != lstDbCmdsDetail.Count)
                {
                    throw new Exception(((int)DO_TASK_ERROR.TRANSACTION_ERROR).ToString());
                }
                foreach (mPluginForListCommand.ResponseCommandsData objMpResp in objmPluginResp)
                {
                    DoTaskCmdDetails cmdDetailsForResponse = new DoTaskCmdDetails();
                    DataBaseCommand objDbCommandForResponse = null;
                    //will get DB Command in the loop.otherwise it is an error.
                    //which will happen in the switch case.

                    /**
                     * we have to find the command detail which matches 
                     * the idx of the response send from mplugin
                     * The list may not be in the same order as send.
                     **/
                    foreach (DoTaskCmdDetails cmdDetails in _doTaskRequest.Commands)
                    {
                        if (cmdDetails.idx == objMpResp.idx)
                        {
                            cmdDetailsForResponse = cmdDetails;
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    foreach (DataBaseCommand objDataBaseCommand in lstDbCmdsDetail)
                    {
                        if (cmdDetailsForResponse.cmd == objDataBaseCommand.CommandId)
                        {
                            objDbCommandForResponse = objDataBaseCommand;
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    bool blnDoesCmdSelectAllClmsAndRows = false;

                    switch (objDbCommandForResponse.DataBaseCommandType)
                    {
                        case DatabaseCommandType.SELECT:
                            RespDataForCommand objRespDataForCommand;
                            /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                            if (cmdDetailsForResponse.cmdlp.Count == 1)
                            {
                                getDBProcessResponseForSelect(objMpResp.recordDataSet,
                                                            cmdDetailsForResponse,
                                                             out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                            }
                            else
                            {
                                getDBProcessResponseForSelect(new DataSet(),
                                                            cmdDetailsForResponse,
                                                             out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                            }
                            respDataForCmds.Add(objRespDataForCommand);
                            break;

                        case DatabaseCommandType.INSERT:
                        case DatabaseCommandType.UPDATE:
                        case DatabaseCommandType.DELETE:
                            /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                            //DbManipulationCmdData objDbManipulationData = new DbManipulationCmdData();
                            //objDbManipulationData.rtv = objMpResp.reccount.ToString();
                            //List<DbManipulationCmdData> lstDbManipulationData = new List<DbManipulationCmdData>();
                            //lstDbManipulationData.Add(objDbManipulationData);
                            respDataForCmds.Add(this.getRespDataForCommand(cmdDetailsForResponse, null, Utilities.getResponseStatus(null), blnDoesCmdSelectAllClmsAndRows));
                            break;
                    }
                }
                processSuccessfull = true;
            }
            catch (mPlugin.mPluginException mex)
            {
                throw mex;
            }
            catch (HttpException hex)
            {
                throw hex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void runMsSqlBatchProcess(List<DataBaseCommand> dbCmds, DataBaseConnector dbConnector, List<DoTaskCmdDetails> cmdDtlsList, out List<RespDataForCommand> respDataForCmds, out bool processSuccessfull, bool isTransaction)
        {
            respDataForCmds = new List<RespDataForCommand>();
            processSuccessfull = false;
            SqlTransaction transaction = null;
            SqlConnection con = null;

            try
            {
                MSSqlDatabaseClient.SqlConnectionOpen(out con, dbConnector.ConnectionString);
            }
            catch (Exception ex)
            {
                if (con == null
                    &&
                    ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                throw ex;
            }

            try
            {
                using (con)
                {
                    if (isTransaction)
                        transaction = con.BeginTransaction();
                    int iCountOfDbCmdLoop = 0;
                    foreach (DataBaseCommand dbcmd in dbCmds)
                    {
                        bool blnDoesCmdSelectAllClmsAndRows = false;
                        SqlCommand cmd;
                        if (isTransaction)
                            cmd = new SqlCommand(dbcmd.SqlQuery, con, transaction);
                        else cmd = new SqlCommand(dbcmd.SqlQuery, con);
                        //sqlcomdtype = (int)mPlugin.QueryType.SELECT;
                        int iRowCountAffected, iCountOfCmdlp;
                        ResponseStatus objRespStatus = new ResponseStatus();
                        RespDataForCommand objRespDataForCommand;
                        //get the parameters list
                        List<DoTaskCmdParameters> lstCmdParamsForCmd = cmdDtlsList[iCountOfDbCmdLoop].cmdlp;

                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                        iCountOfCmdlp = lstCmdParamsForCmd != null ? lstCmdParamsForCmd.Count : 0;
                        int iCountOfCmdLpLoop = 0;
                        switch (dbcmd.DataBaseCommandType)
                        {
                            case DatabaseCommandType.SELECT:
                                iCountOfCmdLpLoop = 0;
                                DataSet ds = null;
                                try
                                {
                                    /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                    foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                    {
                                        try
                                        {
                                            ds = MSSqlDatabaseClient.SelectDataWithOpenConForDoTask(cmd, paramsForQUery.lp);
                                        }
                                        catch (SqlException ex)
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                              , false));
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                        catch (Exception ex)
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , ds,
                                              Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                              , false));
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                    }
                                    if (iCountOfCmdlp == 1)
                                    {
                                        getDBProcessResponseForSelect(ds, cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                    else
                                    {
                                        getDBProcessResponseForSelect(new DataSet(), cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                break;
                            case DatabaseCommandType.INSERT:
                            case DatabaseCommandType.UPDATE:
                            case DatabaseCommandType.DELETE:
                                iCountOfCmdLpLoop = 0;
                                foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                {
                                    iRowCountAffected = 0;
                                    try
                                    {
                                        iRowCountAffected = MSSqlDatabaseClient.ExecuteNonQueryWithOpenCon(cmd, paramsForQUery.lp);

                                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                        if (iCountOfCmdLpLoop == iCountOfCmdlp - 1)
                                        {
                                            //DbManipulationCmdData objDbManipulationData = new DbManipulationCmdData();
                                            //objDbManipulationData.rtv = iRowCountAffected.ToString();
                                            //List<DbManipulationCmdData> lstDbManipulationData = new List<DbManipulationCmdData>();
                                            //lstDbManipulationData.Add(objDbManipulationData);
                                            respDataForCmds.Add(this.getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop],
                                             null, Utilities.getResponseStatus(null),
                                               blnDoesCmdSelectAllClmsAndRows)
                                                               );
                                        }
                                        iCountOfCmdLpLoop++;
                                    }
                                    catch (SqlException ex)
                                    {
                                        if (ex.Message.Contains("Must declare the scalar variable"))
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                , false)
                                                               );
                                        }
                                        else
                                        {
                                            respDataForCmds.Add(
                                                getRespDataForCommand(
                                                cmdDtlsList[iCountOfDbCmdLoop]
                                               , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                               , false)
                                                           );
                                        }
                                        processSuccessfull = false;
                                        throw ex;
                                    }
                                    catch (Exception ex)
                                    {
                                        respDataForCmds.Add(
                                            getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                          , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                          , false)
                                                            );
                                        processSuccessfull = false;
                                        throw ex;
                                    }

                                }
                                break;
                        }
                        iCountOfDbCmdLoop++;
                    }
                    transaction.Commit();
                    processSuccessfull = true;
                }
            }
            catch (SqlException e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            catch (Exception e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            finally
            {
                if (con != null)
                {
                    MSSqlDatabaseClient.SqlConnectionClose(con);
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        void runMySqlBatchProcess(List<DataBaseCommand> dbCmds, DataBaseConnector dbConnector, List<DoTaskCmdDetails> cmdDtlsList, out List<RespDataForCommand> respDataForCmds, out bool processSuccessfull, bool isTransaction)
        {
            respDataForCmds = new List<RespDataForCommand>();
            processSuccessfull = false;
            MySqlTransaction transaction = null;
            MySqlConnection con = null;

            try
            {
                MySqlClient.OpenConnection(out con, dbConnector.ConnectionString);
            }
            catch (Exception ex)
            {
                if (con == null
                    &&
                    ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                throw ex;
            }

            try
            {
                using (con)
                {
                    if (isTransaction) transaction = con.BeginTransaction();
                    int iCountOfDbCmdLoop = 0;
                    foreach (DataBaseCommand dbcmd in dbCmds)
                    {
                        bool blnDoesCmdSelectAllClmsAndRows = false;
                        MySqlCommand cmd;//= new MySqlCommand(dbcmd.SqlQuery, con, transaction);
                        if (isTransaction)
                            cmd = new MySqlCommand(dbcmd.SqlQuery, con, transaction);
                        else cmd = new MySqlCommand(dbcmd.SqlQuery, con);
                        //sqlcomdtype = (int)mPlugin.QueryType.SELECT;
                        int iRowCountAffected, iCountOfCmdlp;
                        ResponseStatus objRespStatus = new ResponseStatus();
                        RespDataForCommand objRespDataForCommand;
                        //get the parameters list
                        List<DoTaskCmdParameters> lstCmdParamsForCmd = cmdDtlsList[iCountOfDbCmdLoop].cmdlp;

                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                        iCountOfCmdlp = lstCmdParamsForCmd != null ? lstCmdParamsForCmd.Count : 0;
                        int iCountOfCmdLpLoop = 0;
                        switch (dbcmd.DataBaseCommandType)
                        {
                            case DatabaseCommandType.SELECT:
                                iCountOfCmdLpLoop = 0;
                                DataSet ds = null;
                                try
                                {
                                    /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                    foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                    {
                                        try
                                        {
                                            ds = MySqlClient.SelectDataWithOpenConForDoTask(cmd, paramsForQUery.lp);
                                        }
                                        catch (MySqlException ex)
                                        {
                                            if (ex.Message.Contains("Must declare the scalar variable"))
                                            {
                                                respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                                  , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                   , false));
                                            }
                                            else
                                            {
                                                respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                                  , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                                  , false));
                                            }
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                        catch (Exception ex)
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , ds,
                                              Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                              , false));
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                    }
                                    if (iCountOfCmdlp == 1)
                                    {
                                        getDBProcessResponseForSelect(ds, cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                    else
                                    {
                                        getDBProcessResponseForSelect(new DataSet(), cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                break;
                            case DatabaseCommandType.INSERT:
                            case DatabaseCommandType.UPDATE:
                            case DatabaseCommandType.DELETE:
                                iCountOfCmdLpLoop = 0;
                                foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                {
                                    iRowCountAffected = 0;
                                    try
                                    {
                                        iRowCountAffected = MySqlClient.ExecuteNonQueryWithOpenCon(cmd, paramsForQUery.lp);

                                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                        if (iCountOfCmdLpLoop == iCountOfCmdlp - 1)
                                        {
                                            //DbManipulationCmdData objDbManipulationData = new DbManipulationCmdData();
                                            //objDbManipulationData.rtv = iRowCountAffected.ToString();
                                            //List<DbManipulationCmdData> lstDbManipulationData = new List<DbManipulationCmdData>();
                                            //lstDbManipulationData.Add(objDbManipulationData);
                                            respDataForCmds.Add(this.getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop],
                                             null, Utilities.getResponseStatus(null),
                                               blnDoesCmdSelectAllClmsAndRows)
                                                               );
                                        }
                                        iCountOfCmdLpLoop++;
                                    }
                                    catch (MySqlException ex)
                                    {
                                        if (ex.Message.Contains("Must declare the scalar variable"))
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                , false)
                                                               );
                                        }
                                        else
                                        {
                                            respDataForCmds.Add(
                                                getRespDataForCommand(
                                                cmdDtlsList[iCountOfDbCmdLoop]
                                               , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                               , false)
                                                           );
                                        }
                                        processSuccessfull = false;
                                        throw ex;
                                    }
                                    catch (Exception ex)
                                    {
                                        respDataForCmds.Add(
                                            getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                          , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                          , false)
                                                            );
                                        processSuccessfull = false;
                                        throw ex;
                                    }

                                }
                                break;
                        }
                        iCountOfDbCmdLoop++;
                    }
                    transaction.Commit();
                    processSuccessfull = true;
                }
            }
            catch (MySqlException e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            catch (Exception e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        void runOracleSqlBatchProcess(List<DataBaseCommand> dbCmds, DataBaseConnector dbConnector, List<DoTaskCmdDetails> cmdDtlsList, out List<RespDataForCommand> respDataForCmds, out bool processSuccessfull, bool isTransaction)
        {
            respDataForCmds = new List<RespDataForCommand>();
            processSuccessfull = false;
            OracleTransaction transaction = null;
            OracleConnection con = null;

            try
            {
                OracleDoTaskClient.OracleConnectionOpen(out con, dbConnector.ConnectionString);
            }
            catch (Exception ex)
            {
                if (con == null
                    &&
                    ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                throw ex;
            }

            try
            {
                using (con)
                {
                    if (isTransaction) transaction = con.BeginTransaction();
                    int iCountOfDbCmdLoop = 0;
                    foreach (DataBaseCommand dbcmd in dbCmds)
                    {
                        bool blnDoesCmdSelectAllClmsAndRows = false;
                        OracleCommand cmd = new OracleCommand(dbcmd.SqlQuery, con);
                        //sqlcomdtype = (int)mPlugin.QueryType.SELECT;
                        int iRowCountAffected, iCountOfCmdlp;
                        ResponseStatus objRespStatus = new ResponseStatus();
                        RespDataForCommand objRespDataForCommand;
                        //get the parameters list
                        List<DoTaskCmdParameters> lstCmdParamsForCmd = cmdDtlsList[iCountOfDbCmdLoop].cmdlp;

                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                        iCountOfCmdlp = lstCmdParamsForCmd != null ? lstCmdParamsForCmd.Count : 0;
                        int iCountOfCmdLpLoop = 0;
                        switch (dbcmd.DataBaseCommandType)
                        {
                            case DatabaseCommandType.SELECT:
                                iCountOfCmdLpLoop = 0;
                                DataSet ds = null;
                                try
                                {
                                    /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                    foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                    {
                                        try
                                        {
                                            ds = OracleDoTaskClient.SelectDataFromOpenConnectionDoTask(cmd, paramsForQUery.lp);
                                        }
                                        catch (MySqlException ex)
                                        {
                                            if (ex.Message.Contains("Must declare the scalar variable"))
                                            {
                                                respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                                  , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                   , false));
                                            }
                                            else
                                            {
                                                respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                                  , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                                  , false));
                                            }
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                        catch (Exception ex)
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , ds,
                                              Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                              , false));
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                    }
                                    if (iCountOfCmdlp == 1)
                                    {
                                        getDBProcessResponseForSelect(ds, cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                    else
                                    {
                                        getDBProcessResponseForSelect(new DataSet(), cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                break;
                            case DatabaseCommandType.INSERT:
                            case DatabaseCommandType.UPDATE:
                            case DatabaseCommandType.DELETE:
                                iCountOfCmdLpLoop = 0;
                                foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                {
                                    iRowCountAffected = 0;
                                    try
                                    {
                                        iRowCountAffected = OracleDoTaskClient.ExecuteNonQueryWithOpenCon(cmd, paramsForQUery.lp);

                                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                        if (iCountOfCmdLpLoop == iCountOfCmdlp - 1)
                                        {
                                            //DbManipulationCmdData objDbManipulationData = new DbManipulationCmdData();
                                            //objDbManipulationData.rtv = iRowCountAffected.ToString();
                                            //List<DbManipulationCmdData> lstDbManipulationData = new List<DbManipulationCmdData>();
                                            //lstDbManipulationData.Add(objDbManipulationData);
                                            respDataForCmds.Add(this.getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop],
                                             null, Utilities.getResponseStatus(null),
                                               blnDoesCmdSelectAllClmsAndRows)
                                                               );
                                        }
                                        iCountOfCmdLpLoop++;
                                    }
                                    catch (MySqlException ex)
                                    {
                                        if (ex.Message.Contains("Must declare the scalar variable"))
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                , false)
                                                               );
                                        }
                                        else
                                        {
                                            respDataForCmds.Add(
                                                getRespDataForCommand(
                                                cmdDtlsList[iCountOfDbCmdLoop]
                                               , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                               , false)
                                                           );
                                        }
                                        processSuccessfull = false;
                                        throw ex;
                                    }
                                    catch (Exception ex)
                                    {
                                        respDataForCmds.Add(
                                            getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                          , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                          , false)
                                                            );
                                        processSuccessfull = false;
                                        throw ex;
                                    }

                                }
                                break;
                        }
                        iCountOfDbCmdLoop++;
                    }
                    transaction.Commit();
                    processSuccessfull = true;
                }
            }
            catch (MySqlException e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            catch (Exception e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        void runPostGreSqlBatchProcess(List<DataBaseCommand> dbCmds, DataBaseConnector dbConnector, List<DoTaskCmdDetails> cmdDtlsList, out List<RespDataForCommand> respDataForCmds, out bool processSuccessfull, bool isTransaction)
        {
            respDataForCmds = new List<RespDataForCommand>();
            processSuccessfull = false;
            NpgsqlTransaction transaction = null;
            NpgsqlConnection con = null;

            try
            {
                PostgreSqlClient.OpenConnection(out con, dbConnector.ConnectionString);
            }
            catch (NpgsqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
            }
            catch (Exception ex)
            {
                if (con == null
                    &&
                    ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                throw ex;
            }

            try
            {
                using (con)
                {
                    if (isTransaction)
                        transaction = con.BeginTransaction();
                    int iCountOfDbCmdLoop = 0;
                    foreach (DataBaseCommand dbcmd in dbCmds)
                    {
                        bool blnDoesCmdSelectAllClmsAndRows = false;
                        NpgsqlCommand cmd = new NpgsqlCommand(dbcmd.SqlQuery, con, transaction);
                        //sqlcomdtype = (int)mPlugin.QueryType.SELECT;
                        int iRowCountAffected, iCountOfCmdlp;
                        ResponseStatus objRespStatus = new ResponseStatus();
                        RespDataForCommand objRespDataForCommand;
                        //get the parameters list
                        List<DoTaskCmdParameters> lstCmdParamsForCmd = cmdDtlsList[iCountOfDbCmdLoop].cmdlp;

                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                        iCountOfCmdlp = lstCmdParamsForCmd != null ? lstCmdParamsForCmd.Count : 0;
                        int iCountOfCmdLpLoop = 0;
                        switch (dbcmd.DataBaseCommandType)
                        {
                            case DatabaseCommandType.SELECT:
                                iCountOfCmdLpLoop = 0;
                                DataSet ds = null;
                                try
                                {
                                    /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                    foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                    {
                                        try
                                        {
                                            ds = PostgreSqlClient.SelectDataWithOpenConForDoTask(cmd, paramsForQUery.lp);
                                        }
                                        catch (NpgsqlException ex)
                                        {
                                            if (ex.Message.Contains("Must declare the scalar variable"))
                                            {
                                                respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                                  , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                   , false));
                                            }
                                            else
                                            {
                                                respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                                  , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                                  , false));
                                            }
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                        catch (Exception ex)
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , ds, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                              , false));
                                            processSuccessfull = false;
                                            throw ex;
                                        }
                                    }
                                    if (iCountOfCmdlp == 1)
                                    {
                                        getDBProcessResponseForSelect(ds, cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                    else
                                    {
                                        getDBProcessResponseForSelect(new DataSet(), cmdDtlsList[iCountOfDbCmdLoop],
                                            out objRespDataForCommand, out blnDoesCmdSelectAllClmsAndRows);
                                        respDataForCmds.Add(objRespDataForCommand);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                break;
                            case DatabaseCommandType.INSERT:
                            case DatabaseCommandType.UPDATE:
                            case DatabaseCommandType.DELETE:
                                iCountOfCmdLpLoop = 0;
                                foreach (DoTaskCmdParameters paramsForQUery in lstCmdParamsForCmd)
                                {
                                    iRowCountAffected = 0;
                                    try
                                    {
                                        iRowCountAffected = PostgreSqlClient.ExecuteNonQueryWithOpenCon(cmd, paramsForQUery.lp);

                                        /**if Cmdlp count is greater than 1 then only one dt tag is returned
                             * as response even if same command is run with different parameter list
                             * don't send different result for command run with diff parameter list.
                             * In case of select if cmdlp is greater than 1 then empty dt is returned
                             * even if they are run with different parameter list
                            * **/
                                        if (iCountOfCmdLpLoop == iCountOfCmdlp - 1)
                                        {
                                            //DbManipulationCmdData objDbManipulationData = new DbManipulationCmdData();
                                            //objDbManipulationData.rtv = iRowCountAffected.ToString();
                                            //List<DbManipulationCmdData> lstDbManipulationData = new List<DbManipulationCmdData>();
                                            //lstDbManipulationData.Add(objDbManipulationData);
                                            respDataForCmds.Add(this.getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop],
                                             null, Utilities.getResponseStatus(null),
                                               blnDoesCmdSelectAllClmsAndRows)
                                                               );
                                        }
                                        iCountOfCmdLpLoop++;
                                    }
                                    catch (NpgsqlException ex)
                                    {
                                        if (ex.Message.Contains("Must declare the scalar variable"))
                                        {
                                            respDataForCmds.Add(getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                              , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString()))
                                                , false)
                                                               );
                                        }
                                        else
                                        {
                                            respDataForCmds.Add(
                                                getRespDataForCommand(
                                                cmdDtlsList[iCountOfDbCmdLoop]
                                               , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                               , false)
                                                           );
                                        }
                                        processSuccessfull = false;
                                        throw ex;
                                    }
                                    catch (Exception ex)
                                    {
                                        respDataForCmds.Add(
                                            getRespDataForCommand(cmdDtlsList[iCountOfDbCmdLoop]
                                          , null, Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()))
                                          , false)
                                                            );
                                        processSuccessfull = false;
                                        throw ex;
                                    }

                                }
                                break;
                        }
                        iCountOfDbCmdLoop++;
                    }
                    transaction.Commit();
                    processSuccessfull = true;
                }
            }
            catch (NpgsqlException e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            catch (Exception e)
            {
                try
                {
                    transaction.Rollback();
                }
                catch { }
                throw e;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        #endregion

        #region ResponseRead
        void getDBProcessResponseForSelect(DataSet ds, DoTaskCmdDetails cmdDtls, out RespDataForCommand respDataForCommand, out bool isCmdForSelectingAllClmnsAndRows)
        {
            try
            {
                respDataForCommand = null;
                isCmdForSelectingAllClmnsAndRows = false;
                isCmdForSelectingAllClmnsAndRows = true;
                respDataForCommand = getRespDataForCommand(cmdDtls, getTablesSelectedFromDataSet(ds), Utilities.getResponseStatus(null), isCmdForSelectingAllClmnsAndRows);
            }
            catch
            {
                throw new Exception(((int)DO_TASK_ERROR.INVALID_COLUMN_IN_DATA_COMMAND).ToString());
            }
        }

        List<Seleced_cmd_Tables> getTablesSelectedFromDataSet(DataSet ds)
        {
            if (ds == null) throw new ArgumentNullException();
            List<Seleced_cmd_Tables> lstSelectedTables = new List<Seleced_cmd_Tables>();
            /**
                  * Even if a dataset contains more than one table
                  * we have to return only the data of 
                  * the first table only.
                  * **/
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataColumn dc in dt.Columns)
                {
                    Seleced_cmd_Tables objSelectedTbls = new Seleced_cmd_Tables();
                    objSelectedTbls.cnm = dc.ColumnName;
                    //add column values
                    List<string> strList = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        strList.Add(dr[dc.ColumnName].ToString());
                    }
                    objSelectedTbls.val = strList;
                    lstSelectedTables.Add(objSelectedTbls);
                }
            }
            return lstSelectedTables;
        }
        #endregion

        RespDataForCommand getRespDataForCommand(DoTaskCmdDetails cmdDetails, object lstData, ResponseStatus cmdRespStatus, bool isReturnTypeSelectAll)
        {
            CommandErrorDetail objErrorDtl = new CommandErrorDetail();
            objErrorDtl.cd = cmdRespStatus.cd;
            objErrorDtl.desc = cmdRespStatus.desc;
            return new RespDataForCommand(cmdDetails.cmd, cmdDetails.ctrlid, cmdDetails.idx,
                                         lstData, cmdDetails.rtfn, objErrorDtl, isReturnTypeSelectAll);
        }
    }
}