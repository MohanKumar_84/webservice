﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class EmailTemplateParameterNameValue
    {

        string _parameterName, _parameterValue;

        public string ParameterValue
        {
            get { return _parameterValue; }
            set { _parameterValue = value; }
        }

        public string ParameterName
        {
            get { return _parameterName; }
            set { _parameterName = value; }
        } 
    }
}