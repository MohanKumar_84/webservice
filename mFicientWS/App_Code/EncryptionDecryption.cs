﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace mFicientWS
{
    public class EncryptionDecryption
    {
/// <summary>
        /// Encrypt the plain string
        /// </summary>
        /// <param name="_InputString">plain string which is to be encrypted</param>
        /// <returns>encrypted string</returns>
        public static string AESEncrypt(string key, string _InputString)
        {
            try
            {
                string strKeyHash = Utilities.GetMd5Hash(key).ToLower();
                byte[] ptbytes = Encoding.UTF8.GetBytes(_InputString);
                string strIV = getRandomIV(16);

                MemoryStream ms = new MemoryStream();
                AesManaged AES = new AesManaged();

                AES.KeySize = 128;
                AES.BlockSize = 128;
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;

                AES.Key = Encoding.UTF8.GetBytes(strKeyHash);
                AES.IV = Encoding.UTF8.GetBytes(strIV);

                CryptoStream cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(ptbytes, 0, ptbytes.Length);
                cs.Close();

                byte[] ct = ms.ToArray();
                AES.Clear();
                return strIV + Convert.ToBase64String(ct, Base64FormattingOptions.None);
            }
            catch (Exception ex)
            {
                //throw ex;
                return "";
            }
        }

        /// <summary>
        /// Decrypt the encrypted string to plain string
        /// </summary>
        /// <param name="_EncryptedString">encrypted string</param>
        /// <returns>plain string</returns>
        public static string AESDecrypt(string key, string _EncryptedString)
        {
            if (_EncryptedString.Trim().Length <= 16)
                return "";
            try
            {
                string strKeyHash = Utilities.GetMd5Hash(key).ToLower();
                string strIV = _EncryptedString.Substring(0, 16);
                string encryptedString = _EncryptedString.Substring(16, _EncryptedString.Length - 16);
                byte[] bytesArray = Convert.FromBase64String(encryptedString);

                MemoryStream ms = new MemoryStream();
                AesManaged AES = new AesManaged();

                AES.KeySize = 128;
                AES.BlockSize = 128;
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;

                AES.Key = Encoding.UTF8.GetBytes(strKeyHash);
                AES.IV = Encoding.UTF8.GetBytes(strIV);

                CryptoStream cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write);

                cs.Write(bytesArray, 0, bytesArray.Length);
                cs.Close();
                byte[] ct = ms.ToArray();

                return Encoding.UTF8.GetString(ct);
            }
            catch (Exception ex)
            {
                //throw ex;
                return "";
            }
        }

        private static string getRandomIV(int length)
        {
            string ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
                sb.Append(ALLOWED_CHARACTERS[random.Next(ALLOWED_CHARACTERS.Length)]);
            return sb.ToString();
        }
    }
}


