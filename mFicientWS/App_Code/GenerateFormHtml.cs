using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace mFicientWS
{
    public class GenerateFormHtml
    {
        #region Constructor
        public GenerateFormHtml(AppForm _form, bool _isChild, string _viewID, DataSet _drDataObjects, out List<AppForm> _childForms, out JArray _JAActionButton, int _modelType, out List<string> _offlineTBL)
        {
            mfForm = _form;
            isChild = _isChild;
            dsDataObjects = _drDataObjects;
            initialize();
            modelType = _modelType;

            formJsonToHeadAndBodyOfHTML(_viewID);

            _childForms = childFormsForReturn;
            _JAActionButton = jaActionButtonForReturn;
            _offlineTBL = offlineTablesNameForReturn;
        }

        #endregion
        #region Enumerations

        protected enum ControlEvents
        {
            ON_BLUR = 1,
            ON_CLICK = 2,
            ON_CHANGE = 3,
        }

        protected enum EnumToggleSwitchText
        {
            ON_OFF = 0,
            YES_NO = 1,
            TRUE_FALSE = 2,
            CUSTOM = 3,
        }

        protected enum JqueryValidationType
        {
            Email = 0,
            Integer = 1,
            Decimal = 2,
            AlphaNumeric = 3,
            Alphabetical = 4,
            CustumRegx = 5,
            MultipleOf = 6
        }

        protected enum DataBindType
        {
            Manual = 0,
            Database = 1,
            webservice = 2,
        }

        public enum Code
        {
            ZERO = 0,
            ONE = 1,
            TWO = 2,
            THREE = 3
        }

        private enum CallNativeFunctionCodes
        {
            CALL_NATIVE_HTML_READY = 1,
            CALL_NATIVE_DO_TASK_RUN = 2,
            CALL_NATIVE_SCAN_BARCODE = 3,
            CALL_NATIVE_SHOW_LOCATION_IN_MAP = 4,
            CALL_NATIVE_LIST_ROW_CLICKED = 5,
            CALL_NATIVE_HYPERLINK_CLICKED = 6,
            CALL_NATIVE_FOR_RESPONSE = 7,
            CALL_NATIVE_VIEW_IN_EDITABLE_LIST = 8,
            CALL_NATIVE_EDIT_IN_EDITABLE_LIST = 9,
            CALL_NATIVE_DELETE_IN_EDITABLE_LIST = 10,
            CALL_NATIVE_LIST_PAGE_BIND = 11,
            CALL_NATIVE_FORCE_DO_TASK_RUN = 17,
        }

        private enum CallHtmlFromNativeFunctionCodes
        {
            CALL_FROM_NATIVE_SET_CONROL_VALUE = 101,
            CALL_FROM_NATIVE_GET_CONROL_VALUE = 102,
            CALL_FROM_NATIVE_BIND_DROPDOWN = 103,
            CALL_FROM_NATIVE_GET_ALL_CONROL_VALUE = 104,
            CALL_FROM_NATIVE_BIND_LIST = 105,
            CALL_FROM_NATIVE_BIND_BAR = 106,
            CALL_FROM_NATIVE_BIND_LINE = 107,
            CALL_FROM_NATIVE_BIND_PIE = 108,
            CALL_FROM_NATIVE_BIND_TABLE = 109,
            CALL_FROM_NATIVE_SET_BAR_CODE = 110,
            CALL_FROM_NATIVE_SET_LOCATION = 111,
            CALL_FROM_NATIVE_BIND_EDITABLE_LIST = 112,
            CALL_FROM_NATIVE_BIND_TEXT_CONTROL = 116,
            CALL_FROM_NATIVE_BIND_PICTURE = 120,
            CALL_FROM_NATIVE_BIND_ANGULARGAUGE = 121,
            CALL_FROM_NATIVE_BIND_CYLINDERGAUGE = 122,
            CALL_FROM_NATIVE_BIND_SCRIBBLE = 123,
            CALL_FROM_NATIVE_BIND_IMAGE = 124,
            CALL_FROM_NATIVE_BIND_LABEL = 127,
            CALL_FROM_NATIVE_BIND_DPIE = 128,
        }

        #endregion
        #region Private variables
        List<AppForm> childFormsForReturn;
        JArray jaActionButtonForReturn, ControlsListForHTMLReady, setEditableChildBindingForHTMLReady;
        private AppForm mfForm;
        private int LoctionControlExists = 0, modelType, controlTabIndex = 0;
        private bool IsjqPlotJSUsed = false, isChild;
        private string returnHtml = string.Empty, prevChkfield = string.Empty, setControlOnLoadScript = string.Empty,
            HtmlHead = string.Empty, HtmlBody = string.Empty, HtmlFileLinks, style = string.Empty;

        JObject joSetEditableListItemForHTMLReady, joTablePropertyOnReady;
        DataSet dsDataObjects;
        List<string> _scriptForWindowReady = new List<string>(), offlineTablesNameForReturn = new List<string>();
        List<string> condistionDisplayTriggers = new List<string>();
        List<string> controlsArray = new List<string>(new string[] { "BTN", "TXT", "LBL", "DTP", "SLD", "IMG", "CHK", "PIE", "DPIE", "BAR",
                                                                 "LINE", "ANGUAGE", "CYGUAGE", "SEP", "SPC", "BCODE", "PIC", "RDB","TGL", "DDL",
                                                                 "HDF", "RPT", "LOC", "TBL", "SCRB", "CUST" });

        private enum ControlTypeEnum
        {
            BTN = 0,
            TXT = 1,
            LBL = 2,
            DTP = 3,
            SLD = 4,
            IMG = 5,
            CHK = 6,
            PIE = 7,
            DPIE = 8,
            BAR = 9,
            LINE = 10,
            ANGUAGE = 11,
            CYGUAGE = 12,
            SEP = 13,
            SPC = 14,
            BCODE = 15,
            PIC = 16,
            RDB = 17,
            TGL = 18,
            DDL = 19,
            HDF = 20,
            RPT = 21,
            LOC = 22,
            TBL = 23,
            SCRB = 24,
            CUST = 25,
        }
        private void initialize()
        {
            setEditableChildBindingForHTMLReady = new JArray();
            ControlsListForHTMLReady = new JArray();
            joSetEditableListItemForHTMLReady = new JObject();
            childFormsForReturn = new List<AppForm>();
            jaActionButtonForReturn = new JArray();
            isChild = false;
        }
        #endregion
        #region Private methods Overall Html
        private void formJsonToHeadAndBodyOfHTML(string _view_id)
        {
            try
            {
                if (mfForm != null)
                {
                    string strBodyContant = createHtmlControlsByJSON();
                    createHtmlHead(_view_id);
                    createHtmlBody(strBodyContant);
                }
            }
            catch
            { }
        }
        private string createHtmlControlsByJSON()
        {
            HtmlTextWriter writer;
            StringWriter stringWriter = new StringWriter();
            using (writer = new HtmlTextWriter(stringWriter))
            {
                sectionRowPanels hd = null;
                foreach (var grow in mfForm.rowPanels)
                {
                    //create grid
                    if (grow.id != "Sec_hd")
                    {
                        addSectionInControls(grow);
                        if (Convert.ToInt32(mfForm.formModelType) == 1 && modelType == 0)
                            dawControlsTabletToPhone(writer, grow);
                        else
                            drawControls(writer, grow);
                    }
                    else
                    {
                        if (grow.colmnPanels[0].controls.Count > 0)
                        {
                            grow.type = new SectionRowType();
                            grow.type.id = "1";
                            grow.type.UiName = "100%";
                            hd = grow;
                        }
                    }
                }
                if (hd != null)
                {
                    if (Convert.ToInt32(mfForm.formModelType) == 1 && modelType == 0)
                        dawControlsTabletToPhone(writer, hd);
                    else
                        drawControls(writer, hd);
                }
            }
            return stringWriter.ToString();
        }
        private void drawControls(HtmlTextWriter writer, sectionRowPanels grow)
        {
            if (grow.type == null) grow.type = new SectionRowType();
            string strClass = setControlDisplayCondition(grow.conditionalDisplay, grow.condDisplayCntrlProps);
            string gridClass = getHtmlForGridDiv(grow.type.id);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, gridClass + " " + strClass);
            writer.AddAttribute("data-mf-class", gridClass);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, grow.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            int columnIndex = 1;
            foreach (var gcol in grow.colmnPanels)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, gcol.id);
                string blockClass = getClassForBlocks(columnIndex++, grow.type.UiName);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, blockClass);
                writer.AddAttribute("data-mf-class", blockClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                foreach (var Gctrl in gcol.controls)
                {
                    getHtml(Gctrl, gcol.id, writer);
                }
                if (!string.IsNullOrEmpty(prevChkfield))
                {
                    writer.RenderEndTag();
                    prevChkfield = string.Empty;
                }
                // end
                writer.RenderEndTag();//end block
            }
            writer.RenderEndTag();//end grid
        }
        private void dawControlsTabletToPhone(HtmlTextWriter writer, sectionRowPanels grow)
        {
            if (grow.type == null) grow.type = new SectionRowType();
            foreach (var gcol in grow.colmnPanels)
            {
                string strClass = setControlDisplayCondition(grow.conditionalDisplay, grow.condDisplayCntrlProps);
                string gridClass = getHtmlForGridDiv("1");

                writer.AddAttribute(HtmlTextWriterAttribute.Class, gridClass + " " + strClass);
                writer.AddAttribute("data-mf-class", gridClass);
                writer.AddAttribute(HtmlTextWriterAttribute.Id, grow.userDefinedName);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.AddAttribute(HtmlTextWriterAttribute.Id, gcol.id);
                string blockClass = getClassForBlocks(1, grow.type.UiName);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, blockClass);
                writer.AddAttribute("data-mf-class", blockClass);

                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                foreach (var Gctrl in gcol.controls)
                {
                    getHtml(Gctrl, gcol.id, writer);
                }
                if (!string.IsNullOrEmpty(prevChkfield))
                {
                    writer.RenderEndTag();
                    prevChkfield = string.Empty;
                }
                // end
                writer.RenderEndTag();//end block
                writer.RenderEndTag();//end grid
            }
        }
        private string createHtmlBody(string strContentHtlm)
        {
            HtmlTextWriter writer;
            StringWriter strOuterWriter = new StringWriter();
            using (writer = new HtmlTextWriter(strOuterWriter))
            {
                writer.AddAttribute("data-role", "page");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                writer.AddAttribute("data-role", "content");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderBeginTag(HtmlTextWriterTag.Form);
                writer.Write(strContentHtlm);
                writer.RenderEndTag();//Form
                writer.RenderEndTag();//content
                writer.RenderEndTag();//page
            }
            HtmlBody = strOuterWriter.ToString();
            return HtmlBody;
        }
        private string createHtmlHead(string _view_id)
        {
            HtmlTextWriter writer;
            StringWriter strOuterWriter = new StringWriter();
            using (writer = new HtmlTextWriter(strOuterWriter))
            {
                //-----comman function-----------------------
                string strCommon = Utilities.Base64Decode(mfForm.commonFunc);
                if (strCommon.Length > 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
                    writer.RenderBeginTag(HtmlTextWriterTag.Script);
                    // writer.Write("try{" + commentStripper(strCommon) + "} catch (err) { if (app.debugMode && err.message != _$IGNORE_ERROR_MESSAGE_) { var _errorMessage = 'View:' + _$CURRENT_VIEW_NAME_ + '\\r\\nSource: Common Script\\r\\n\\r\\n' + err.name + ': ' + err.message; app.alertScriptError(_errorMessage, false); }}");
                    writer.Write(commentStripper(strCommon));
                    writer.RenderEndTag();//Script
                }
                //-----------ready------------------------
                string strBindControlEvent = CreateEventsAndScripts();
                htmlInnerHeadOnRead(_view_id, writer);
                //--------------Load---------------------
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
                writer.RenderBeginTag(HtmlTextWriterTag.Script);

                string BarCodeChangeEvent = bindUserDefinedScripts("viewEvent.onAppear", mfForm.viewAppear, true, string.Empty, string.Empty) + bindUserDefinedScripts("viewEvent.onReAppear", mfForm.viewReAppear, true, string.Empty, string.Empty) +
                bindUserDefinedScripts("viewEvent.willDisappear", mfForm.viewDisAppear, true, string.Empty, string.Empty) + bindUserDefinedScripts("viewEvent.submitButtonTapped", mfForm.submitBtn, true, string.Empty, string.Empty) +
                bindUserDefinedScripts("viewEvent.backButtonTapped", mfForm.backBtn, true, string.Empty, string.Empty) + bindUserDefinedScripts("viewEvent.cancelButtonTapped", mfForm.cancelBtn, true, string.Empty, string.Empty) +
                bindUserDefinedScripts("viewEvent.actionItemTapped", mfForm.actionSheetBtn, true, string.Empty, string.Empty);

                writer.Write(BarCodeChangeEvent + strBindControlEvent);
                writer.RenderEndTag();//Script

                //---------------style-----------------------
                if (!string.IsNullOrEmpty(style))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
                    writer.RenderBeginTag(HtmlTextWriterTag.Style);
                    writer.Write(style);
                    writer.RenderEndTag();//style 
                }
            }
            HtmlHead = strOuterWriter.ToString();
            return HtmlHead;
        }

        private string commentStripper(string Input)
        {
            var blockComments = @"/\*(.*?)\*/";
            var lineComments = @"//(.*?)\r?\n";
            var strings = @"""((\\[^\n]|[^""\n])*)""";
            var verbatimStrings = @"@(""[^""]*"")+";
            var stringsSingle = @"'((\\[^\n]|[^'\n])*)'";
            var verbatimStringsSingle = @"@('[^']*')+";
            string noComments = Input;
            try
            {
                noComments = Regex.Replace(Input,
                blockComments + "|" + lineComments + "|" + strings + "|" + stringsSingle + "|" + verbatimStrings + "|" + verbatimStringsSingle,
                me =>
                {
                    if (me.Value.StartsWith("/*") || me.Value.StartsWith("//"))
                        return me.Value.StartsWith("//") ? Environment.NewLine : "";
                    // Keep the literal strings
                    return me.Value;
                },
                RegexOptions.Singleline);
            }
            catch { noComments = Input; }
            return noComments;
        }
        private void htmlInnerHeadOnRead(string _view_id, HtmlTextWriter writer)
        {
            string strControlsEvents = string.Empty;
            foreach (string str in _scriptForWindowReady)
            {
                strControlsEvents += "\r\n" + str;
            }

            string StrScripts = strControlsEvents.Trim();
            StrScripts = "try{var mf=mf$;}catch(err){mf=_mf$;} $(document).ready(function() {" + "  $('form').submit(function (event) { return mf.submitForm();} ); " + StrScripts + "});";
            HtmlFileLinks = (IsjqPlotJSUsed ? "1" : "0") + (LoctionControlExists >= 2 ? "1" : "0") + "0" + "0" + ((joTablePropertyOnReady != null) ? "1" : "0");

            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
            writer.RenderBeginTag(HtmlTextWriterTag.Script);
            writer.Write("var LINK_" + _view_id.Replace('-', '_') + "='" + HtmlFileLinks + "';" + StrScripts + getPageLoadFunction());
            writer.RenderEndTag();//Script
        }
        string bindUserDefinedScripts(string eventName, string script, bool isDecode, string controlName, string extraContant)
        {
            if (isDecode)
                script = Utilities.Base64Decode(script);

            if (!string.IsNullOrEmpty(script))
                if (string.IsNullOrEmpty(controlName))
                    return "app.thisView.bindEvent(" + eventName + ", function() {" + extraContant + " return \"" + Microsoft.JScript.GlobalObject.escape(script) + "\";});";
                else
                    return "app.thisView.bindEvent(" + eventName + ",\"" + controlName + "\", function() {" + extraContant + " return \"" + Microsoft.JScript.GlobalObject.escape(script) + "\";});";
            return "";
        }
        string getHtmlForGridDiv(string RowType)
        {
            if (string.IsNullOrEmpty(RowType)) RowType = "1";

            if (RowType.ToUpper() == "1") return "ui-grid-solo";
            else if (RowType.ToUpper() == "2") return "ui-grid-b";
            else return "ui-grid-a";
        }
        private string getClassForBlocks(int columnNo, string RowType)
        {
            if (string.IsNullOrEmpty(RowType)) RowType = "";
            string strClass = string.Empty;
            switch (columnNo)
            {
                case 1:
                    strClass = "ui-block-a";
                    if (RowType.ToUpper() == "COL-33-67")
                    {
                        strClass += " left-33";
                    }
                    else if (RowType.ToUpper() == "COL-67-33")
                    {
                        strClass += " left-67";
                    }
                    break;
                case 2:
                    strClass = "ui-block-b";
                    if (RowType.ToUpper() == "COL-33-67")
                    {
                        strClass += " right-67";
                    }
                    else if (RowType.ToUpper() == "COL-67-33")
                    {
                        strClass += " right-33";
                    }
                    break;
                case 3:
                    strClass = "ui-block-c";
                    break;
                case 4:
                    strClass = "ui-block-d";
                    break;
            }
            return strClass;
        }
        #endregion
        #region private method Control html
        private void getHtml(Controls gctrl, string groupID, HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(gctrl.userDefinedName)) gctrl.userDefinedName = gctrl.id + "_default";
            string controlType = gctrl.type.idPrefix;
            if (!string.IsNullOrEmpty(prevChkfield) && controlType != controlsArray[(int)ControlTypeEnum.CHK])
            {
                writer.RenderEndTag();
                prevChkfield = string.Empty;
            }
            switch (controlsArray.IndexOf(controlType))
            {
                case (int)ControlTypeEnum.BTN:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForButton(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.TXT:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForTextbox(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.LBL:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForLabel(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.DTP:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForDateTimePicker(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.SLD:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForSlider(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.IMG:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForImage(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.CHK:
                    if (string.IsNullOrEmpty(prevChkfield))
                    {

                        writer.AddAttribute("data-mf-type", controlType);
                        writer.AddAttribute("data-role", "controlgroup");
                        writer.AddAttribute(HtmlTextWriterAttribute.Id, gctrl.id);
                        writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
                    }
                    prevChkfield = gctrl.id;
                    getHtmlForCheckBox(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.PIE:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForPieChart(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.DPIE:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForDrilldownPieChart(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.BAR:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForBarChart(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.LINE:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForLineChart(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.ANGUAGE:
                    writer.AddAttribute("data-mf-type", "ANG");
                    getHtmlForAngularGauge(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.CYGUAGE:
                    writer.AddAttribute("data-mf-type", "CYG");
                    getHtmlForCylinderGauge(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.SEP:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForSeparator(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.SPC:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForSpacer(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.PIC:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForPicture(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.BCODE:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForBarCodeReaderControl(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.RDB:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForRadioButton(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.TGL:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForToggleButton(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.DDL:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForDropDown(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.HDF:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForHiddenField(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.RPT:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForListview(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.LOC:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForLocationControl(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.TBL:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForTable(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.SCRB:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForScribble(gctrl, writer);
                    break;
                case (int)ControlTypeEnum.CUST:
                    writer.AddAttribute("data-mf-type", controlType);
                    getHtmlForCustomControl(gctrl, writer);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private void getHtmlForLabel(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            //---div for backgound and border color------
            string containerClass = string.Empty;
            string containerStyle = string.Empty;
            getLabelContainerClass(_Gctrl.backgroundColor, _Gctrl.borderColor, _Gctrl.fontColor, Convert.ToInt32(_Gctrl.border), out containerStyle, out containerClass);

            string ControlName = _Gctrl.userDefinedName;
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ControlName);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-corner-all " + containerClass);
            if (!string.IsNullOrEmpty(containerStyle))
                writer.AddAttribute(HtmlTextWriterAttribute.Style, containerStyle);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            string titleClass = string.Empty;
            string textClass = string.Empty;
            getLabelTitleAndTextClass(Convert.ToInt32(_Gctrl.titlePosition), Convert.ToInt32(_Gctrl.fontAlign), Convert.ToInt32(_Gctrl.fontSize), Convert.ToInt32(_Gctrl.fontStyle), Convert.ToInt32(_Gctrl.wrap), Convert.ToInt32(_Gctrl.underline), out titleClass, out textClass);
            //---------------------------

            if (!string.IsNullOrEmpty(_Gctrl.title) && _Gctrl.title.Trim().Length > 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, titleClass);
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "ttl_" + ControlName);
                if (!string.IsNullOrEmpty(_Gctrl.titleColor))
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "color:" + _Gctrl.titleColor);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(_Gctrl.title);
                writer.RenderEndTag();//Div 
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, textClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "txt_" + ControlName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();//Div 

            writer.RenderEndTag();//Div 
            writer.RenderEndTag();//Fieldset 
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("v", "");
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_LABEL).ToString(), Jort, "", new JObject(), _Gctrl.id);

            //if (_Gctrl.hideIfBlank == "1") ControlHiddenOnEmpty.Add(_Gctrl.userDefinedName);
            //if (!string.IsNullOrEmpty(_Gctrl.title))
            //    _OnWindowReadyMethods.Add("PushLabelArray('" + _Gctrl.userDefinedName + "', '', '','','" + _Gctrl.title + "','" + _Gctrl.text + "');");

        }

        private void getLabelContainerClass(string _backgroundColor, string _borderColor, string _fontColor, int _border, out string _containerStyle, out string _containerClass)
        {
            _containerClass = "container-none";
            _containerStyle = string.Empty;

            if (!string.IsNullOrEmpty(_backgroundColor))
            {
                _containerStyle = "background-color:" + _backgroundColor + ";";
                _containerClass = "container";
            }
            if (_border >= 1)
            {
                _containerClass = "container-border";
                _containerStyle += "border-width:" + _border + "px;" + (string.IsNullOrEmpty(_borderColor) ? "" : ("border-color:" + _borderColor + ";"));
            }
            if (!string.IsNullOrEmpty(_fontColor))
                _containerStyle += "color:" + _fontColor + ";";
        }
        private void getLabelTitleAndTextClass(int _titleAlign, int _txtAlign, int _fontSize, int _fontStyle, int _wrap, int _underline, out string _titleClass, out string _txtClass)
        {
            _titleClass = string.Empty;
            _txtClass = string.Empty;
            if (_titleAlign == 0)//top
            {
                switch ((Code)_txtAlign)
                {
                    case Code.ZERO://left
                        _titleClass = "title-ttl-top-txt-left";
                        _txtClass = "text-ttl-top-txt-left";
                        break;
                    case Code.ONE://center
                        _titleClass = "title-ttl-top-txt-center";
                        _txtClass = "text-ttl-top-txt-center";
                        break;
                    case Code.TWO://right
                        _titleClass = "title-ttl-top-txt-right";
                        _txtClass = "text-ttl-top-txt-right";
                        break;
                }
            }
            else//left
            {
                switch ((Code)_txtAlign)
                {
                    case Code.ZERO://left
                        _titleClass = "title-ttl-left-txt-left";
                        _txtClass = "text-ttl-left-txt-left";
                        break;
                    case Code.ONE://center
                        _titleClass = "title-ttl-left-txt-center";
                        _txtClass = "text-ttl-left-txt-center";
                        break;
                    case Code.TWO://right
                        _titleClass = "title-ttl-left-txt-right";
                        _txtClass = "text-ttl-left-txt-right";
                        break;
                }
            }
            //------font size------
            switch ((Code)_fontSize)
            {
                case Code.ZERO:
                    _titleClass += " title-font-small";
                    _txtClass += " text-font-small";
                    break;
                case Code.ONE:
                    _titleClass += " title-font-normal";
                    _txtClass += " text-font-normal";
                    break;
                case Code.TWO:
                    _titleClass += " title-font-large";
                    _txtClass += " text-font-large";
                    break;
            }
            //------font style------
            _txtClass += getFontStyle(_fontStyle);
            //------word wrap------
            _txtClass += _wrap == 1 ? " text-wrap" : " text-nowrap";
            _txtClass += _underline == 1 ? " text-style-underline" : "";
        }

        private string getFontStyle(int _fontStyle)
        {
            string _txtClass = "";
            switch ((Code)_fontStyle)
            {
                case Code.ONE:
                    _txtClass += " text-style-bold";
                    break;
                case Code.TWO:
                    _txtClass += " text-style-italic";
                    break;
                case Code.THREE:
                    _txtClass += " text-style-bold text-style-italic";
                    break;
            }
            return _txtClass;
        }

        private void getHtmlForSeparator(Controls _Gctrl, HtmlTextWriter writer)
        {
            string strClass = setControlDisplayCondition(_Gctrl.conditionalDisplay, _Gctrl.condDisplayCntrlProps);
            if (_Gctrl.inset == "1") writer.AddAttribute(HtmlTextWriterAttribute.Class, strClass + " withOutDatainset Seprator");
            else writer.AddAttribute(HtmlTextWriterAttribute.Class, strClass + " datainset Seprator");
            _Gctrl.thickness = string.IsNullOrEmpty(_Gctrl.thickness) ? "1" : _Gctrl.thickness;
            string style = "solid";
            if (!string.IsNullOrEmpty(_Gctrl.borderStyle) && _Gctrl.borderStyle == "1") style = "dashed";
            if (!string.IsNullOrEmpty(_Gctrl.borderStyle) && _Gctrl.borderStyle == "2") style = "dotted";
            _Gctrl.borderStyle = style;
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "border-bottom: " + _Gctrl.thickness + "px " + _Gctrl.borderStyle + ";");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();//div
            addInControlsString(_Gctrl, "0", new JObject(), "", new JObject(), _Gctrl.id);
        }

        private void getHtmlForPicture(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            //_OnWindowReadyMethods.Add("$('#div_" + _Gctrl.userDefinedName + @"').bind('tap',function(){ OpenPictureControl('" + _Gctrl.userDefinedName + @"'); });");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            //if (_Gctrl.labelText.Trim().Length > 0)
            //{
            //    getTitleOrLabelStyle(_Gctrl, writer, "pic-title");
            //    writer.AddAttribute(HtmlTextWriterAttribute.Style, "width:100%;text-align:center;");
            //    writer.RenderBeginTag(HtmlTextWriterTag.Div);
            //    writer.Write(_Gctrl.labelText);
            //    writer.RenderEndTag(); //div
            //}

            writer.AddAttribute(HtmlTextWriterAttribute.Id, "div_" + _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "width:" + _Gctrl.width + "%");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag(); //div

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            if (!string.IsNullOrEmpty(_Gctrl.value))
                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.value);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag(); //Input

            writer.RenderEndTag();//Fieldset
            addInControlsString(_Gctrl, "", new JObject(), "", new JObject(), _Gctrl.id);
        }

        private void getHtmlForScribble(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            // _OnWindowReadyMethods.Add("$('#div_" + _Gctrl.userDefinedName + @"').bind('tap',function(){  OpenScribbleControl('" + _Gctrl.userDefinedName + @"'); });");

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);


            //if (_Gctrl.labelText.Trim().Length > 0)
            //{
            //    getTitleOrLabelStyle(_Gctrl, writer, "pic-title");
            //    writer.RenderBeginTag(HtmlTextWriterTag.Div);
            //    writer.Write(_Gctrl.labelText);
            //    writer.RenderEndTag(); //div
            //}

            writer.AddAttribute(HtmlTextWriterAttribute.Id, "div_" + _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            if (!string.IsNullOrEmpty(_Gctrl.value))
                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.value);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();//Input

            writer.RenderEndTag();//Fieldset
            addInControlsString(_Gctrl, "", new JObject(), "", new JObject(), _Gctrl.id);
        }

        private void getHtmlForCustomControl(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            //_OnWindowReadyMethods.Add("$('#div_" + _Gctrl.userDefinedName + @"').bind('tap',function(){ OpenPictureControl('" + _Gctrl.userDefinedName + @"'); });");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();//div
            writer.RenderEndTag();//Fieldset
            addInControlsString(_Gctrl, "", new JObject(), "", new JObject(), _Gctrl.id);
        }

        private void getTitleOrLabelStyle(Controls _Gctrl, HtmlTextWriter writer, string extraClass)
        {
            if (!string.IsNullOrEmpty(_Gctrl.fontColor))
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "color:" + _Gctrl.fontColor);

            if (!string.IsNullOrEmpty(_Gctrl.fontStyle) && _Gctrl.fontStyle != "0")
                writer.AddAttribute(HtmlTextWriterAttribute.Class, getFontStyle(Convert.ToInt32(_Gctrl.fontStyle)) + " " + extraClass);
        }

        private void getHtmlForSpacer(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "height:" + _Gctrl.height + "px;");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
            addInControlsString(_Gctrl, "0", new JObject(), "", new JObject(), _Gctrl.id);
        }

        private void getHtmlForDropDown(Controls _Gctrl, HtmlTextWriter writer)
        {
            string strClass = setControlDisplayCondition(_Gctrl.conditionalDisplay, _Gctrl.condDisplayCntrlProps);
            if (_Gctrl.inline == 1)
                strClass = strClass + " inlineCtrlContainer";

            writer.AddAttribute(HtmlTextWriterAttribute.Class, strClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (_Gctrl.inline == 1)
            {
                getHtmlForInlineControl(_Gctrl, writer);
            }
            else
            {
                if (_Gctrl.labelText.Trim().Length > 0)
                {
                    getTitleOrLabelStyle(_Gctrl, writer, "");
                    writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                    writer.Write(_Gctrl.labelText);
                    writer.RenderEndTag();
                }
                htmlForDropdownInner(_Gctrl, writer);
            }
            writer.RenderEndTag();
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindValueToProperty, _Gctrl.bindTextToProperty);
            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.optionValue);
            Jort.Add("t", _Gctrl.optionText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_DROPDOWN).ToString(), Jort, "", new JObject(), _Gctrl.id);
        }

        private void htmlForDropdownInner(Controls _Gctrl, HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                writer.AddAttribute("data-mini", "true");
            if (_Gctrl.inline == 1) writer.AddAttribute("data-wrapper-class", "ui-custom");
            writer.RenderBeginTag(HtmlTextWriterTag.Select);
            //setControlOnLoadScript += (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + "SetSizeOfControl('" + _Gctrl.userDefinedName + "','" + getControlWidth(_Gctrl.widthInPercent) + "','" + _Gctrl.minWidth + "',true);";
            if (_Gctrl.promptText.Length > 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.promptValue);
                writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
                writer.RenderBeginTag(HtmlTextWriterTag.Option);
                writer.Write(_Gctrl.promptText);
                writer.RenderEndTag();
                //_OnWindowReadyMethods.Add("PushSelectArray('" + _Gctrl.userDefinedName + "', '" + Convert.ToString(_Gctrl.promptText) + "', '" + Convert.ToString(_Gctrl.promptValue) + "');");
            }
            writer.RenderEndTag();
        }

        //private void getHtmlForTextbox(Controls _Gctrl, HtmlTextWriter writer)
        //{
        //    setControlDisplayCondition(_Gctrl, writer);
        //    writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
        //    writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

        //    getTitleOrLabelStyle(_Gctrl, writer, "");
        //    writer.RenderBeginTag(HtmlTextWriterTag.Legend);
        //    writer.Write(_Gctrl.labelText);

        //    writer.RenderEndTag();

        //    writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
        //    if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
        //        writer.AddAttribute("data-mini", "true");
        //    if (string.IsNullOrEmpty(_Gctrl.txtRowsCount) || Convert.ToInt32(_Gctrl.txtRowsCount) <= 1)
        //    {
        //        if (!string.IsNullOrEmpty(_Gctrl.maskValue) && _Gctrl.maskValue == "1")
        //            writer.AddAttribute(HtmlTextWriterAttribute.Type, "password");
        //        else
        //            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");

        //        writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, _Gctrl.maxLength);
        //        if (!string.IsNullOrEmpty(_Gctrl.placeholder))
        //            writer.AddAttribute("placeholder", _Gctrl.placeholder);
        //        writer.AddAttribute("data-clear-btn", "true");
        //        if (!string.IsNullOrEmpty(_Gctrl.dfltValue))
        //            writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.dfltValue);
        //        writer.RenderBeginTag(HtmlTextWriterTag.Input);
        //    }
        //    else
        //    {
        //        if (string.IsNullOrEmpty(_Gctrl.txtRowsCount)) _Gctrl.txtRowsCount = "2";
        //        writer.AddAttribute(HtmlTextWriterAttribute.Rows, _Gctrl.txtRowsCount);
        //        if (!string.IsNullOrEmpty(_Gctrl.placeholder))
        //            writer.AddAttribute("placeholder", _Gctrl.placeholder);
        //        if (!string.IsNullOrEmpty(_Gctrl.value))
        //            writer.Write(_Gctrl.value);
        //        writer.AddAttribute("data-autogrow", "false");
        //        writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
        //    }

        //    writer.RenderEndTag();
        //    writer.RenderEndTag();

        //    //setControlOnLoadScript += (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + "SetSizeOfControl('" + _Gctrl.userDefinedName + "','" + getControlWidth(_Gctrl.widthInPercent) + "','" + _Gctrl.minWidth + "',false);";
        //    createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

        //    JObject Jort = new JObject();
        //    Jort.Add("t", _Gctrl.displayText);
        //    addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
        //}

        private void getHtmlForTextbox(Controls _Gctrl, HtmlTextWriter writer)
        {
            string strClass = setControlDisplayCondition(_Gctrl.conditionalDisplay, _Gctrl.condDisplayCntrlProps);
            if (_Gctrl.inline == 1)
                strClass = strClass + " inlineCtrlContainer";

            writer.AddAttribute(HtmlTextWriterAttribute.Class, strClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (_Gctrl.inline == 1)
            {
                getHtmlForInlineControl(_Gctrl, writer);
            }
            else
            {
                if (_Gctrl.labelText.Trim().Length > 0)
                {
                    getTitleOrLabelStyle(_Gctrl, writer, "");
                    writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                    writer.Write(_Gctrl.labelText);
                    writer.RenderEndTag();
                }
                htmlForTextboxInner(_Gctrl, writer);
            }

            writer.RenderEndTag();

            // setControlOnLoadScript += (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + "SetSizeOfControl('" + _Gctrl.userDefinedName + "','" + getControlWidth(_Gctrl.widthInPercent) + "','" + _Gctrl.minWidth + "',false);";
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("t", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
        }

        private void htmlForTextboxInner(Controls _Gctrl, HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                writer.AddAttribute("data-mini", "true");

            if (!string.IsNullOrEmpty(_Gctrl.placeholder))
                writer.AddAttribute("placeholder", _Gctrl.placeholder);

            if (!string.IsNullOrEmpty(_Gctrl.dfltValue))
                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.dfltValue);

            if (_Gctrl.inline == 1) writer.AddAttribute("data-wrapper-class", "ui-custom");
            writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, _Gctrl.maxLength);

            if (string.IsNullOrEmpty(_Gctrl.txtRowsCount) || Convert.ToInt32(_Gctrl.txtRowsCount) <= 1)
            {
                if (!string.IsNullOrEmpty(_Gctrl.maskValue) && _Gctrl.maskValue == "1")
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "password");
                else
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
                writer.AddAttribute("data-clear-btn", "true");
                writer.RenderBeginTag(HtmlTextWriterTag.Input);
            }
            else
            {
                if (string.IsNullOrEmpty(_Gctrl.txtRowsCount)) _Gctrl.txtRowsCount = "2";
                writer.AddAttribute(HtmlTextWriterAttribute.Rows, _Gctrl.txtRowsCount);
                writer.AddAttribute("data-autogrow", "false");
                writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
            }
            writer.RenderEndTag();
        }

        private void getHtmlForInlineControl(Controls _Gctrl, HtmlTextWriter writer)
        {
            _Gctrl.icon = (_Gctrl.icon == null) ? "" : _Gctrl.icon.Trim();
            _Gctrl.icon = (_Gctrl.icon == "0") ? "" : _Gctrl.icon;

            if ((_Gctrl.icon.Length > 0 && _Gctrl.labelText.Trim().Length > 0) || _Gctrl.labelText.Trim().Length > 0)
            {
                string gridClass = "ui-grid-a";
                writer.AddAttribute(HtmlTextWriterAttribute.Class, gridClass);
                writer.AddAttribute("data-mf-class", gridClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                string blockClassA = "ui-block-a";
                writer.AddAttribute(HtmlTextWriterAttribute.Class, blockClassA + " inlineLeft inlinetext");
                writer.AddAttribute("data-mf-class", blockClassA);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                if (_Gctrl.icon.Length > 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-btn ui-icon-" + _Gctrl.icon + " ui-btn-icon-left ui-nodisc-icon ui-alt-icon ui-btn-ui-custom");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                }
                if (_Gctrl.labelText.Trim().Length > 0)
                {
                    getTitleOrLabelStyle(_Gctrl, writer, "");
                    writer.AddAttribute("for", _Gctrl.userDefinedName);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "text-nowrap" + (_Gctrl.icon.Length > 0 ? "" : " inlineLabel"));
                    writer.RenderBeginTag(HtmlTextWriterTag.Label);
                    writer.Write(_Gctrl.labelText);
                    writer.RenderEndTag();//label
                }
                if (_Gctrl.icon.Length > 0) writer.RenderEndTag();//icon div
                writer.RenderEndTag();//block a

                string blockClassB = "ui-block-b";
                writer.AddAttribute(HtmlTextWriterAttribute.Class, blockClassB + " inlineRight" + (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.DDL] ? " ui-nodisc-icon ui-alt-icon" : ""));
                writer.AddAttribute("data-mf-class", blockClassB);
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.TXT])
                    htmlForTextboxInner(_Gctrl, writer);
                else if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.DDL])
                    htmlForDropdownInner(_Gctrl, writer);
                else if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.DTP])
                    HtmlDateTimePickerInner(_Gctrl, writer);
                writer.RenderEndTag();//block b
                writer.RenderEndTag();// grid
            }
            else
            {
                if (_Gctrl.icon.Length > 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-btn ui-icon-" + _Gctrl.icon + " ui-btn-icon-left ui-nodisc-icon ui-alt-icon ui-btn-ui-custom");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "padding-left:32px;");
                }
                if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.TXT])
                    htmlForTextboxInner(_Gctrl, writer);
                else if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.DDL])
                {
                    if (_Gctrl.icon.Length <= 0)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-btn ui-nodisc-icon ui-alt-icon ui-btn-ui-customWoi");
                        writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    }
                    htmlForDropdownInner(_Gctrl, writer);
                    if (_Gctrl.icon.Length <= 0) writer.RenderEndTag();//icon div
                }
                else if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.DTP])
                    HtmlDateTimePickerInner(_Gctrl, writer);
                if (_Gctrl.icon.Length > 0) writer.RenderEndTag();//icon div
            }
        }

        private void getHtmlForListview(Controls _Gctrl, HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(_Gctrl.emptyListMsg)) _Gctrl.emptyListMsg = string.Empty;

            setControlDisplayCondition(_Gctrl, writer);
            string parentId = _Gctrl.id;
            if (_Gctrl.listType == "0")
            {
                string[] id = _Gctrl.id.Split('_');
                parentId = id[0] + "_S_" + id[1];
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Id, parentId);

            if ((_Gctrl.allowFilter == null || _Gctrl.allowFilter == "0") && controlTabIndex <= 0 && _Gctrl.dataInset != "1")
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-top:-16px;");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute("data-role", "listview");
            writer.AddAttribute("data-mf-list-type", _Gctrl.listType);

            if (_Gctrl.dataInset == "1") writer.AddAttribute("data-inset", "true");
            else writer.AddAttribute("data-inset", "false");

            if (_Gctrl.actionButton == "1") writer.AddAttribute("data-split-icon", "gear");

            if (_Gctrl.dataGrouping == "1") writer.AddAttribute("data-autodividers", "true");

            if (_Gctrl.allowFilter != null && _Gctrl.allowFilter == "1")
            {
                writer.AddAttribute("data-filter", "true");
                writer.AddAttribute("data-filter-placeholder", string.IsNullOrEmpty(_Gctrl.placeholder) ? "Search..." : _Gctrl.placeholder);
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-nodisc-icon ui-alt-icon");
            if (_Gctrl.databindObjs == null) _Gctrl.listType = "2";

            if (_Gctrl.listType == "3") writer.RenderBeginTag(HtmlTextWriterTag.Ol);
            else writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.RenderEndTag();

            if (_Gctrl.actionButton == "1")
            {
                if (_Gctrl.actionBtn1Settings != null && !string.IsNullOrEmpty(_Gctrl.actionBtn1Settings.text))
                {
                    JObject jo = new JObject();
                    jo.Add("lbl", _Gctrl.actionBtn1Settings.text);
                    jo.Add("idx", "0");
                    jaActionButtonForReturn.Add(jo);
                }
                if (_Gctrl.actionBtn2Settings != null && !string.IsNullOrEmpty(_Gctrl.actionBtn2Settings.text))
                {
                    JObject jo = new JObject();
                    jo.Add("lbl", _Gctrl.actionBtn2Settings.text);
                    jo.Add("idx", "1");
                    jaActionButtonForReturn.Add(jo);
                }
                if (_Gctrl.actionBtn3Settings != null && !string.IsNullOrEmpty(_Gctrl.actionBtn3Settings.text))
                {
                    JObject jo = new JObject();
                    jo.Add("lbl", _Gctrl.actionBtn3Settings.text);
                    jo.Add("idx", "2");
                    jaActionButtonForReturn.Add(jo);
                }
            }
            writer.RenderEndTag();
            JObject JOrt = new JObject();
            if (_Gctrl.databindObjs != null && _Gctrl.databindObjs.Count > 0)
            {
                if (_Gctrl.listType != "8")
                {
                    JOrt.Add("b", _Gctrl.title);
                    JOrt.Add("s", _Gctrl.subtitle);
                    JOrt.Add("c", _Gctrl.countField);
                    JOrt.Add("r", _Gctrl.rowId);
                    JOrt.Add("n", _Gctrl.information);
                    JOrt.Add("an", _Gctrl.additionalInfo);
                    JOrt.Add("rs", _Gctrl.selectedRow);
                    if (_Gctrl.listType == "6")
                    {
                        JOrt.Add("i", "");
                        JOrt.Add("p", _Gctrl.imageName);
                    }
                    else
                    {
                        JOrt.Add("i", _Gctrl.imageName);
                        JOrt.Add("p", "");
                    }
                }
            }
            else
            {
                _Gctrl.imageName = "";
            }
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_LIST).ToString(), JOrt, _Gctrl.imageName, new JObject(), parentId);
        }

        private void getHtmlForTable(Controls _Gctrl, HtmlTextWriter writer)
        {
            _Gctrl.allowSorting = string.IsNullOrEmpty(_Gctrl.allowSorting) ? "0" : _Gctrl.allowSorting;
            _Gctrl.allowPlot = string.IsNullOrEmpty(_Gctrl.allowPlot) ? "0" : _Gctrl.allowPlot;
            _Gctrl.allowGrouping = string.IsNullOrEmpty(_Gctrl.allowGrouping) ? "0" : _Gctrl.allowGrouping;
            _Gctrl.allowFilter = string.IsNullOrEmpty(_Gctrl.allowFilter) ? "0" : _Gctrl.allowFilter;
            _Gctrl.showRowNo = string.IsNullOrEmpty(_Gctrl.showRowNo) ? "0" : _Gctrl.showRowNo;
            _Gctrl.displayRowCount = string.IsNullOrEmpty(_Gctrl.displayRowCount) ? "0" : _Gctrl.displayRowCount;
            setControlDisplayCondition(_Gctrl, writer);

            JArray JAprop = new JArray();
            joTablePropertyOnReady = new JObject();
            JArray JAColprop = new JArray();
            foreach (TableColumns tc in _Gctrl.columns)
            {
                JObject JOprop = new JObject();
                JOprop.Add("dtcol", tc.column);
                JOprop.Add("name", tc.header);
                JAprop.Add(JOprop);

                JObject JOpropNat = new JObject();
                JOpropNat.Add("idx", tc.index);
                JOpropNat.Add("chead", tc.header);
                JOpropNat.Add("dtyp", tc.datatype);
                JOpropNat.Add("disp", tc.displayFormat);
                JOpropNat.Add("frmt", tc.dataFormat);
                JOpropNat.Add("pfix", tc.prefix);
                JOpropNat.Add("sfix", tc.suffix);
                JOpropNat.Add("mfact", tc.factor);
                JAColprop.Add(JOpropNat);
            }

            _scriptForWindowReady.Add("$('#TBL_" + _Gctrl.userDefinedName + "').parent().parent().addClass('tableForm');");
            joTablePropertyOnReady.Add("ctrlid", _Gctrl.userDefinedName);
            joTablePropertyOnReady.Add("sort", _Gctrl.allowSorting);
            joTablePropertyOnReady.Add("plot", _Gctrl.allowPlot);
            joTablePropertyOnReady.Add("grp", _Gctrl.allowGrouping);
            joTablePropertyOnReady.Add("fltr", _Gctrl.allowFilter);
            joTablePropertyOnReady.Add("rno", _Gctrl.showRowNo);
            joTablePropertyOnReady.Add("rcnt", _Gctrl.displayRowCount);
            joTablePropertyOnReady.Add("cols", JAColprop);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, "TBL_" + _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();//div 

            JObject JOrt = new JObject();
            JOrt.Add("prop", JAprop);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TABLE).ToString(), JOrt, "", new JObject(), "TBL_" + _Gctrl.userDefinedName);
        }

        private void getHtmlForLocationControl(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            LoctionControlExists = 1;

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, "");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            if (_Gctrl.isHidden == "0")
            {
                LoctionControlExists = 2;
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "MAP_CANVAS_" + _Gctrl.userDefinedName);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "height:" + (string.IsNullOrEmpty(_Gctrl.height) ? "100" : _Gctrl.height) + "px;");
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "page-map");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindValueToProperty, _Gctrl.bindTextToProperty);

            JObject JOrt = new JObject();
            JOrt.Add("lat", "");
            JOrt.Add("lng", "");
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_SET_LOCATION).ToString(), JOrt, "", new JObject(), _Gctrl.id);
        }

        private void getHtmlForRadioButton(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);

            writer.AddAttribute("data-role", "controlgroup");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1") writer.AddAttribute("data-mini", "true");
            if (_Gctrl.style != null && _Gctrl.style == "1")
            {
                writer.AddAttribute("data-type", "horizontal");
                string dc = (100.00 / _Gctrl.options.Count).ToString();
                dc = dc.Substring(0, dc.IndexOf('.') + 3);
                style += @" fieldset[id=""" + _Gctrl.userDefinedName + @"""] div[class^=""ui-radio""] {width:" + dc + @"% !important;}";
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
            if (!string.IsNullOrEmpty(_Gctrl.labelText))
            {
                getTitleOrLabelStyle(_Gctrl, writer, "");
                writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                writer.Write(_Gctrl.labelText);
                writer.RenderEndTag();
            }
            int optionIndex = 1;
            foreach (ControlOption op in _Gctrl.options)
            {
                string optionId = _Gctrl.userDefinedName + "_" + optionIndex;
                writer.AddAttribute(HtmlTextWriterAttribute.Id, optionId);
                writer.AddAttribute(HtmlTextWriterAttribute.Name, _Gctrl.userDefinedName);
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "radio");
                writer.AddAttribute(HtmlTextWriterAttribute.Value, op.value);
                if (op.isDefault.Trim() == "1")
                    writer.AddAttribute("checked", "checked");
                writer.RenderBeginTag(HtmlTextWriterTag.Input);
                writer.RenderEndTag();//Input

                writer.AddAttribute("for", optionId);
                writer.RenderBeginTag(HtmlTextWriterTag.Label);
                writer.Write(op.text);
                writer.RenderEndTag();//Label
                optionIndex++;
            }
            writer.RenderEndTag();//Fieldset
            if (_Gctrl.disabled == "1")
            {
                _scriptForWindowReady.Add(@"$(""#" + _Gctrl.userDefinedName + @""").attr('disabled','disabled');");
            }
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindValueToProperty, _Gctrl.bindTextToProperty);
            addInControlsString(_Gctrl, "", new JObject(), "", new JObject(), _Gctrl.userDefinedName);
        }
        private void getHtmlForButton(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);

            string btnClass = "ui-btn ui-shadow ui-corner-all";
            if (!string.IsNullOrEmpty(_Gctrl.icon))
            {
                string iconPosition = "left";
                if (!string.IsNullOrEmpty(_Gctrl.iconPosition))
                {
                    if (_Gctrl.iconPosition == "2") iconPosition = "right";
                    else if (_Gctrl.iconPosition == "3") iconPosition = "top";
                    else if (_Gctrl.iconPosition == "4") iconPosition = "bottom";
                }

                btnClass += " ui-nodisc-icon ui-icon-" + _Gctrl.icon + " ui-btn-icon-" + iconPosition;
            }

            string buttonWidth = "";
            if (_Gctrl.width == "2")
                btnClass += " ui-btn-inline";
            else if (_Gctrl.width == "3")
                buttonWidth = "width:" + _Gctrl.widthInPercent + "%;";

            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                btnClass += " ui-mini";

            if (!string.IsNullOrEmpty(_Gctrl.backgroundColor))
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "background-color:" + _Gctrl.backgroundColor + ";" + buttonWidth + (_Gctrl.backgroundColor == "#f6f6f6" ? "" : "color:white;"));

            writer.AddAttribute(HtmlTextWriterAttribute.Class, btnClass);

            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(_Gctrl.text);
            writer.RenderEndTag();
            writer.RenderEndTag();//fieldset

            addInControlsString(_Gctrl, "", new JObject(), "", new JObject(), _Gctrl.id);
        }
        private void getHtmlForBarCodeReaderControl(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            // _OnWindowReadyMethods.Add("$('#" + "btn_" + _Gctrl.id + @"').bind('tap',function(){  Scan('" + _Gctrl.userDefinedName + "','" + (string.IsNullOrEmpty(_Gctrl.barcodeType) ? "0" : _Gctrl.barcodeType) + "'); });");

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (!string.IsNullOrEmpty(_Gctrl.labelText))
            {
                getTitleOrLabelStyle(_Gctrl, writer, "");
                writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                writer.Write(_Gctrl.labelText);
                writer.RenderEndTag();
            }

            if (!string.IsNullOrEmpty(_Gctrl.rowsCount) && Convert.ToInt32(_Gctrl.rowsCount) > 1)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
                if (!string.IsNullOrEmpty(_Gctrl.placeholder))
                    writer.AddAttribute("placeholder", _Gctrl.placeholder);
                writer.AddAttribute("data-autogrow", "false");
                if (_Gctrl.manualEntry == Convert.ToInt32(Code.ZERO).ToString()) writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");

                writer.AddAttribute(HtmlTextWriterAttribute.Rows, Convert.ToString(_Gctrl.rowsCount));
                writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
                writer.RenderEndTag();
            }
            string gridClass = "ui-grid-a";
            writer.AddAttribute(HtmlTextWriterAttribute.Class, gridClass);
            writer.AddAttribute("data-mf-class", gridClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            string blockClassA = "ui-block-a";
            writer.AddAttribute(HtmlTextWriterAttribute.Class, blockClassA + " scan-Input");
            writer.AddAttribute("data-mf-class", blockClassA);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            if (string.IsNullOrEmpty(_Gctrl.rowsCount) || Convert.ToInt32(_Gctrl.rowsCount) <= 1)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
                if (!string.IsNullOrEmpty(_Gctrl.placeholder))
                    writer.AddAttribute("placeholder", _Gctrl.placeholder);
                if (_Gctrl.manualEntry == Convert.ToInt32(Code.ZERO).ToString()) writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");

                writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
                writer.RenderBeginTag(HtmlTextWriterTag.Input);
                writer.RenderEndTag();
            }
            writer.RenderEndTag();//block a

            string blockClassB = "ui-block-b";
            writer.AddAttribute(HtmlTextWriterAttribute.Class, blockClassB + " scan-Button");
            writer.AddAttribute("data-mf-class", blockClassB);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "custom-icon-border-radius");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "btn_" + _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-btn ui-icon-barcode ui-btn-icon-notext ui-corner-all ui-nodisc-icon");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.RenderEndTag();//a
            writer.RenderEndTag();//div

            writer.RenderEndTag();//block b
            writer.RenderEndTag();//grid a
            writer.RenderEndTag();//fieldset
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
        }

        //private void getHtmlForDateTimePicker(Controls _Gctrl, HtmlTextWriter writer)
        //{
        //    // _OnWindowReadyMethods.Add("ShowDateTimePicker('" + _Gctrl.userDefinedName + "','" + _Gctrl.datePickType + "','" + _Gctrl.minVal + "','" + _Gctrl.maxVal + "','" + _Gctrl.dfltValue + "');");
        //    // _OnWindowReadyMethods.Add(@"DateTimeTextBoxClearEvent();");
        //    setControlDisplayCondition(_Gctrl, writer);

        //    writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
        //    writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

        //    if (!string.IsNullOrEmpty(_Gctrl.labelText))
        //    {
        //        getTitleOrLabelStyle(_Gctrl, writer, "");
        //        writer.RenderBeginTag(HtmlTextWriterTag.Legend);
        //        writer.Write(_Gctrl.labelText);
        //        writer.RenderEndTag();
        //    }

        //    writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
        //    writer.AddAttribute("data-clear-btn", "true");
        //    if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
        //        writer.AddAttribute("data-mini", "true");

        //    writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
        //    if (!string.IsNullOrEmpty(_Gctrl.placeholder))
        //        writer.AddAttribute("placeholder", _Gctrl.placeholder);
        //    writer.RenderBeginTag(HtmlTextWriterTag.Input);
        //    writer.RenderEndTag();

        //    writer.RenderEndTag();
        //    createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

        //    JObject Jort = new JObject();
        //    Jort.Add("v", _Gctrl.displayText);
        //    addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
        //}
        private void getHtmlForDateTimePicker(Controls _Gctrl, HtmlTextWriter writer)
        {
            string strClass = setControlDisplayCondition(_Gctrl.conditionalDisplay, _Gctrl.condDisplayCntrlProps);
            if (_Gctrl.inline == 1)
                strClass = strClass + " inlineCtrlContainer";

            writer.AddAttribute(HtmlTextWriterAttribute.Class, strClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (_Gctrl.inline == 1)
            {
                getHtmlForInlineControl(_Gctrl, writer);
            }
            else
            {
                if (_Gctrl.labelText.Trim().Length > 0)
                {
                    getTitleOrLabelStyle(_Gctrl, writer, "");
                    writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                    writer.Write(_Gctrl.labelText);
                    writer.RenderEndTag();//Legend
                }
                HtmlDateTimePickerInner(_Gctrl, writer);
            }

            writer.RenderEndTag();//Fieldset
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
        }

        private void HtmlDateTimePickerInner(Controls _Gctrl, HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute("data-clear-btn", "true");
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                writer.AddAttribute("data-mini", "true");

            writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
            if (!string.IsNullOrEmpty(_Gctrl.placeholder))
                writer.AddAttribute("placeholder", _Gctrl.placeholder);
            if (_Gctrl.inline == 1) writer.AddAttribute("data-wrapper-class", "ui-custom");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();//input
        }

        private void getHtmlForSlider(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (!string.IsNullOrEmpty(_Gctrl.labelText))
            {
                getTitleOrLabelStyle(_Gctrl, writer, "");
                writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                writer.Write(_Gctrl.labelText);
                writer.RenderEndTag();
            }
            //---set default values----------
            _Gctrl.minVal = string.IsNullOrEmpty(_Gctrl.minVal) ? "0" : _Gctrl.minVal;
            _Gctrl.maxVal = string.IsNullOrEmpty(_Gctrl.maxVal) ? "100" : _Gctrl.maxVal;
            _Gctrl.incrementOnScroll = string.IsNullOrEmpty(_Gctrl.incrementOnScroll) ? "10" : _Gctrl.incrementOnScroll;
            _Gctrl.dfltValue = string.IsNullOrEmpty(_Gctrl.dfltValue) ? _Gctrl.minVal : _Gctrl.dfltValue;
            //--------------------------------------------
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "slider");
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "range");
            writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.dfltValue);
            writer.AddAttribute("data-highlight", "true");
            writer.AddAttribute("min", _Gctrl.minVal);
            writer.AddAttribute("max", _Gctrl.maxVal);
            writer.AddAttribute("step", _Gctrl.incrementOnScroll);
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                writer.AddAttribute("data-mini", "true");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            writer.RenderEndTag();
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
            if (_Gctrl.disabled == "1")
            {
                _scriptForWindowReady.Add(@"$(""#" + _Gctrl.id + @""").attr('disabled','disabled');");
            }
        }

        private void getHtmlForImage(Controls _Gctrl, HtmlTextWriter writer)
        {
            _Gctrl.imageWidth = string.IsNullOrEmpty(_Gctrl.imageWidth) ? "100" : _Gctrl.imageWidth;
            // _OnWindowReadyMethods.Add("ImagesLoadStatus('" + _Gctrl.userDefinedName + "'," + (_Gctrl.inset == "0" ? "3" : _Gctrl.imgAlignment) + ");");
            setControlDisplayCondition(_Gctrl, writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            if (_Gctrl.inset != "0")
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "width:" + _Gctrl.imageWidth + "%;");
            else
            {
                if (controlTabIndex <= 0)
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin:-16px -16px 0px -16px;");
                else
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin:0px -16px;");
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.RenderEndTag(); //div
            writer.RenderEndTag();//Fieldset
            JObject Jort = new JObject();
            Jort.Add("v", string.IsNullOrEmpty(_Gctrl.imageSource) ? "" : _Gctrl.imageSource);
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_IMAGE).ToString(), Jort, _Gctrl.imageSource, new JObject(), _Gctrl.id);
        }

        private void getHtmlForToggleButton(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-grid-a");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-block-a left-67");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.AddAttribute("for", _Gctrl.userDefinedName);
            getTitleOrLabelStyle(_Gctrl, writer, "");
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(_Gctrl.labelText);
            writer.RenderEndTag();

            writer.RenderEndTag();//Div block-a

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-block-b right-33");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "slider");
            writer.AddAttribute("data-role", "slider");
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                writer.AddAttribute("data-mini", "true");
            writer.RenderBeginTag(HtmlTextWriterTag.Select);

            if (string.IsNullOrEmpty(_Gctrl.customOnText)) _Gctrl.customOnText = "On";
            if (string.IsNullOrEmpty(_Gctrl.customOffText)) _Gctrl.customOffText = "Off";

            gettoggleSwitchOption(writer, _Gctrl.defaultState, Convert.ToInt32(_Gctrl.switchText), _Gctrl.customOnText, _Gctrl.customOffText);

            writer.RenderEndTag();//Select
            writer.RenderEndTag();//Div block-b
            writer.RenderEndTag();//Div
            writer.RenderEndTag();//Fieldset
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("t", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.id);
            if (_Gctrl.disabled == "1")
            {
                _scriptForWindowReady.Add(@"$(""#" + _Gctrl.userDefinedName + @""").attr('disabled','disabled');");
            }
        }

        private void gettoggleSwitchOption(HtmlTextWriter writer, string defaultState, int switchText, string OnText, string OffText)
        {
            int intDefaultState = 0;
            switch (switchText)
            {
                case (int)EnumToggleSwitchText.ON_OFF:
                    if (defaultState.ToUpper() == "ON") intDefaultState = 1;
                    OnText = "On"; OffText = "Off";
                    break;
                case (int)EnumToggleSwitchText.YES_NO:
                    if (defaultState.ToUpper() == "ON") intDefaultState = 1;
                    OnText = "Yes"; OffText = "No";
                    break;
                case (int)EnumToggleSwitchText.TRUE_FALSE:
                    if (defaultState.ToUpper() == "ON") intDefaultState = 1;
                    OnText = "True"; OffText = "False";
                    break;
                case (int)EnumToggleSwitchText.CUSTOM:
                    if (defaultState.ToUpper() == "ONTEXT") intDefaultState = 1;
                    break;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Value, "0");
            if (intDefaultState == 0)
                writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
            writer.RenderBeginTag(HtmlTextWriterTag.Option);
            writer.Write(OffText);
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Value, "1");
            if (intDefaultState == 1)
                writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
            writer.RenderBeginTag(HtmlTextWriterTag.Option);
            writer.Write(OnText);
            writer.RenderEndTag();//Option
        }

        private void getHtmlForCheckBox(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, _Gctrl.id);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
            if (_Gctrl.dataMini != null && _Gctrl.dataMini == "1")
                writer.AddAttribute("data-mini", "true");
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();//Input

            writer.AddAttribute("for", _Gctrl.userDefinedName);
            if (_Gctrl.alignment == "1")
                writer.AddAttribute("data-iconpos", "right");

            getTitleOrLabelStyle(_Gctrl, writer, "");
            writer.RenderBeginTag(HtmlTextWriterTag.Label);
            writer.Write(_Gctrl.labelText);
            writer.RenderEndTag();//Label
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), _Gctrl.userDefinedName);

            if (_Gctrl.disabled == "1")
            {
                _scriptForWindowReady.Add(@"$(""#" + _Gctrl.userDefinedName + @""").attr('disabled','disabled');");
            }
        }

        #region charts Html
        private void getHtmlForPieChart(Controls _Gctrl, HtmlTextWriter writer)
        {
            //SetControlDisplayCondition(_Gctrl, writer);
            IsjqPlotJSUsed = true;
            insertChartRenderContainerWithZoom(_Gctrl, writer);
            JObject JOprop = new JObject();
            JOprop.Add("ctrl", _Gctrl.userDefinedName);
            JOprop.Add("irds", _Gctrl.innerRadius);
            JOprop.Add("title", string.IsNullOrEmpty(_Gctrl.chartTitle) ? "" : _Gctrl.chartTitle);
            JOprop.Add("animation", string.IsNullOrEmpty(_Gctrl.animation) ? false : (_Gctrl.animation == "1" ? true : false));

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.value);
            Jort.Add("t", _Gctrl.title);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_PIE).ToString(), Jort, "", JOprop, _Gctrl.id);
        }
        private void getHtmlForDrilldownPieChart(Controls _Gctrl, HtmlTextWriter writer)
        {
            // SetControlDisplayCondition(_Gctrl, writer);
            IsjqPlotJSUsed = true;
            insertChartRenderContainerWithZoom(_Gctrl, writer);
            JObject JOprop = new JObject();
            JOprop.Add("ctrl", _Gctrl.userDefinedName);
            JOprop.Add("irds", _Gctrl.innerRadius);
            JOprop.Add("title", string.IsNullOrEmpty(_Gctrl.chartTitle) ? "" : _Gctrl.chartTitle);
            JOprop.Add("animation", string.IsNullOrEmpty(_Gctrl.animation) ? false : (_Gctrl.animation == "1" ? true : false));

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.value);
            Jort.Add("t", _Gctrl.title);
            Jort.Add("st", _Gctrl.subSliceTitle);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_DPIE).ToString(), Jort, "", JOprop, _Gctrl.id);
        }
        private void getHtmlForAngularGauge(Controls _Gctrl, HtmlTextWriter writer)
        {
            //  SetControlDisplayCondition(_Gctrl, writer);
            IsjqPlotJSUsed = true;

            insertChartRenderContainerWithZoom(_Gctrl, writer);

            JArray strAxis = JArray.Parse(Utilities.SerializeJson<List<AngularAxis>>(_Gctrl.axisObjs));
            if (string.IsNullOrEmpty(_Gctrl.noOfAxis)) _Gctrl.noOfAxis = _Gctrl.axisObjs.Count.ToString();

            JObject JOprop = new JObject();
            JOprop.Add("ctrl", _Gctrl.userDefinedName);
            JOprop.Add("noOfAxis", _Gctrl.noOfAxis);
            JOprop.Add("axis", strAxis);
            JOprop.Add("title", (string.IsNullOrEmpty(_Gctrl.chartTitle) ? "" : _Gctrl.chartTitle));
            JOprop.Add("NdlClor", _Gctrl.needleColor);
            JOprop.Add("NdlRds", _Gctrl.needleRadius);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.value);
            Jort.Add("mv", _Gctrl.maxVal);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_ANGULARGAUGE).ToString(), Jort, "", JOprop, _Gctrl.id);
        }
        private void getHtmlForCylinderGauge(Controls _Gctrl, HtmlTextWriter writer)
        {
            // SetControlDisplayCondition(_Gctrl, writer);
            IsjqPlotJSUsed = true;

            insertChartRenderContainerWithZoom(_Gctrl, writer);

            JObject JOprop = new JObject();
            JOprop.Add("ctrl", _Gctrl.userDefinedName);
            JOprop.Add("valueAs", _Gctrl.valueAs);
            JOprop.Add("fillColor", (string.IsNullOrEmpty(_Gctrl.fillColor) ? "" : _Gctrl.fillColor));
            JOprop.Add("title", string.IsNullOrEmpty(_Gctrl.chartTitle) ? "" : _Gctrl.chartTitle);
            JOprop.Add("min", _Gctrl.minVal);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.value);
            Jort.Add("mv", _Gctrl.maxVal);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_CYLINDERGAUGE).ToString(), Jort, "", JOprop, _Gctrl.id);
        }
        private void getHtmlForBarChart(Controls _Gctrl, HtmlTextWriter writer)
        {
            //SetControlDisplayCondition(_Gctrl, writer);
            IsjqPlotJSUsed = true;

            insertChartRenderContainerWithZoom(_Gctrl, writer);

            string strAxis = string.Empty;

            //string series = ;

            JObject JOprop = new JObject();
            JOprop.Add("ctrl", _Gctrl.userDefinedName);
            JOprop.Add("noOfSeries", _Gctrl.noOfSeries);
            JOprop.Add("seriesValue", getSeriesValueDependencies(_Gctrl));
            JOprop.Add("sdpnt", _Gctrl.noOfDecimalPoint);
            JOprop.Add("xname", _Gctrl.xAxisLabel);
            JOprop.Add("yname", _Gctrl.yAxisLabel);
            JOprop.Add("series", getLabelColorforLineAndBar(_Gctrl));
            JOprop.Add("dir", _Gctrl.barGraphDir);
            JOprop.Add("btyp", _Gctrl.barGraphType);
            JOprop.Add("title", string.IsNullOrEmpty(_Gctrl.chartTitle) ? "" : _Gctrl.chartTitle);
            JOprop.Add("animation", string.IsNullOrEmpty(_Gctrl.animation) ? false : (_Gctrl.animation == "1" ? true : false));

            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_BAR).ToString(), getSeriesValue(_Gctrl), "", JOprop, _Gctrl.id);
        }
        private void getHtmlForLineChart(Controls _Gctrl, HtmlTextWriter writer)
        {
            // SetControlDisplayCondition(_Gctrl, writer);
            IsjqPlotJSUsed = true;

            insertChartRenderContainerWithZoom(_Gctrl, writer);

            JObject JOprop = new JObject();
            JOprop.Add("ctrl", _Gctrl.userDefinedName);
            JOprop.Add("noOfSeries", _Gctrl.noOfSeries);
            JOprop.Add("seriesValue", getSeriesValueDependencies(_Gctrl));
            JOprop.Add("sdpnt", _Gctrl.noOfDecimalPoint);
            JOprop.Add("xname", _Gctrl.xAxisLabel);
            JOprop.Add("yname", _Gctrl.yAxisLabel);
            JOprop.Add("series", getLabelColorforLineAndBar(_Gctrl));
            JOprop.Add("dir", _Gctrl.barGraphDir);
            JOprop.Add("btyp", _Gctrl.barGraphType);
            JOprop.Add("title", string.IsNullOrEmpty(_Gctrl.chartTitle) ? "" : _Gctrl.chartTitle);
            JOprop.Add("animation", string.IsNullOrEmpty(_Gctrl.animation) ? false : (_Gctrl.animation == "1" ? true : false));

            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_LINE).ToString(), getSeriesValue(_Gctrl), "", JOprop, _Gctrl.id);
        }
        #endregion

        private void getHtmlForEditableListView(Controls _Gctrl, HtmlTextWriter writer)
        {
            setControlDisplayCondition(_Gctrl, writer);

            if (string.IsNullOrEmpty(_Gctrl.emptyListMsg)) _Gctrl.emptyListMsg = "List is Empty";
            string EditButtonName = "ELT_EDIT_" + _Gctrl.userDefinedName;
            string CancelButtonName = "ELT_CANCEL_" + _Gctrl.userDefinedName;
            string DeleteButtonName = "ELT_DELETE_" + _Gctrl.userDefinedName;
            string ActionPopupName = "ELT_ACTION_" + _Gctrl.userDefinedName;

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute("data-role", "listview");
            writer.AddAttribute("data-inset", "true");
            writer.AddAttribute("data-split-icon", "delete");

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName + "li1");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "white-space:normal;margin:0.0em;color:#aaaaaa");
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.RenderBeginTag(HtmlTextWriterTag.Strong);
            writer.Write(_Gctrl.emptyListMsg);
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-corner-all");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "add_" + _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
            writer.RenderBeginTag(HtmlTextWriterTag.P);
            writer.RenderBeginTag(HtmlTextWriterTag.Strong);
            writer.Write(string.IsNullOrEmpty(_Gctrl.addBtnText) ? "Add" : _Gctrl.addBtnText);
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            JArray JApropRt = new JArray();
            // string strControlsEvents = "SetListOrTable(JSON.stringify(" + genrateJsonForEditableList(_Gctrl, out JApropRt) + "));$('#add_" + _Gctrl.userDefinedName + "').bind('tap', function (event) { AddInEditableList('" + _Gctrl.userDefinedName + "'); });";
            string strControlsEvents = "$('#add_" + _Gctrl.userDefinedName + "').bind('tap', function (event) { AddInEditableList('" + _Gctrl.userDefinedName + "'); });";
            JObject prop = genrateJsonForEditableList(_Gctrl, out JApropRt);
            _scriptForWindowReady.Add(strControlsEvents);

            JObject JArt = new JObject();
            JArt.Add("prop", JApropRt);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_EDITABLE_LIST).ToString(), JArt, "", prop, _Gctrl.id);
        }
        private JObject genrateJsonForEditableList(Controls _Gctrl, out JArray JApropRt)
        {
            string tnum = "-1", atnum = "-1", dnum = "-1", anum = "-1";
            JApropRt = new JArray();
            JArray JApropElstProp = new JArray();
            foreach (EditableListItem et in _Gctrl.properties)
            {
                JObject JOprop = new JObject();
                JOprop.Add("dtcol", et.mappedDataCol);
                JOprop.Add("name", et.name);
                JApropRt.Add(JOprop);

                JObject JOpropNat = new JObject();
                JOpropNat.Add("type", et.type);
                JOpropNat.Add("name", et.name);
                JOpropNat.Add("uniq", et.unique);
                if (et.name == _Gctrl.title)
                {
                    JOpropNat.Add("disp", "1");
                    tnum = et.type;
                }
                else if (et.name == _Gctrl.additionalTitle)
                {
                    JOpropNat.Add("disp", "4");
                    atnum = et.type;
                }
                else if (et.name == _Gctrl.subTitle)
                {
                    JOpropNat.Add("disp", "2");
                    dnum = et.type;
                }
                else if (et.name == _Gctrl.additionalSubTitle)
                {
                    JOpropNat.Add("disp", "3");
                    anum = et.type;
                }
                else
                    JOpropNat.Add("disp", "0");

                JApropElstProp.Add(JOpropNat);
            }

            childFormsForReturn = _Gctrl.childForms;
            joSetEditableListItemForHTMLReady.Add("ctrlid", _Gctrl.userDefinedName);
            joSetEditableListItemForHTMLReady.Add("perm", _Gctrl.allowItemAddition + _Gctrl.allowItemEditing + _Gctrl.allowItemDelete);
            joSetEditableListItemForHTMLReady.Add("min", _Gctrl.minItems);
            joSetEditableListItemForHTMLReady.Add("max", _Gctrl.maxItems);
            joSetEditableListItemForHTMLReady.Add("addcfm", _Gctrl.addConfirmation);
            joSetEditableListItemForHTMLReady.Add("addbt", string.IsNullOrEmpty(_Gctrl.addBtnText) ? "" : _Gctrl.addBtnText);
            joSetEditableListItemForHTMLReady.Add("anext", string.IsNullOrEmpty(_Gctrl.addNextBtnText) ? "" : _Gctrl.addNextBtnText);
            joSetEditableListItemForHTMLReady.Add("enext", string.IsNullOrEmpty(_Gctrl.editNextBtnText) ? "" : _Gctrl.editNextBtnText);
            joSetEditableListItemForHTMLReady.Add("atitle", _Gctrl.addFormTitle);
            joSetEditableListItemForHTMLReady.Add("etitle", _Gctrl.editFormTitle);
            joSetEditableListItemForHTMLReady.Add("vtitle", _Gctrl.viewFormTitle);
            joSetEditableListItemForHTMLReady.Add("prop", JApropElstProp);

            JObject jo = new JObject();
            jo.Add("isdel", _Gctrl.allowItemDelete);
            jo.Add("dsptxt", _Gctrl.emptyListMsg.Replace("'", "\\'"));
            jo.Add("tnum", tnum);
            jo.Add("atnum", atnum);
            jo.Add("dnum", dnum);
            jo.Add("anum", anum);
            jo.Add("tpre", _Gctrl.titlePrefix);
            jo.Add("tsuf", _Gctrl.titleSuffix);
            jo.Add("tfct", _Gctrl.titleMultFactor);
            jo.Add("dpre", string.IsNullOrEmpty(_Gctrl.subTitlePrefix) ? "" : _Gctrl.subTitlePrefix);
            jo.Add("dsuf", string.IsNullOrEmpty(_Gctrl.subTitlePrefixSuffix) ? "" : _Gctrl.subTitlePrefixSuffix);
            jo.Add("dfct", string.IsNullOrEmpty(_Gctrl.subTitleMultFactor) ? "" : _Gctrl.subTitleMultFactor);

            if (!string.IsNullOrEmpty(_Gctrl.additionalTitle))
            {
                jo.Add("atpre", _Gctrl.additionalTitlePrefix);
                jo.Add("atsuf", _Gctrl.additionalTitleSuffix);
                jo.Add("atfct", _Gctrl.additionalTitleMultFactor);
            }
            if (!string.IsNullOrEmpty(_Gctrl.additionalSubTitle))
            {
                jo.Add("apre", _Gctrl.additionalSubTitlePrefix);
                jo.Add("asuf", _Gctrl.additionalSubTitleSuffix);
                jo.Add("afct", _Gctrl.additionalSubTitleMultFactr);
            }

            JObject joReq = new JObject();
            joReq.Add("req", jo);

            return joReq;
        }
        private void getHtmlForHiddenField(Controls _Gctrl, HtmlTextWriter writer)
        {
            // _OnWindowReadyMethods.Add(@" PushLabelArray('" + _Gctrl.userDefinedName + "', '', '','');");

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
            if (!string.IsNullOrEmpty(_Gctrl.value))
                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.value);
            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();
            createLinkItemJsonForChildForm(_Gctrl.id.Split('_')[0], _Gctrl.userDefinedName, _Gctrl.bindToProperty, string.Empty);

            JObject Jort = new JObject();
            Jort.Add("v", _Gctrl.displayText);
            addInControlsString(_Gctrl, ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL).ToString(), Jort, "", new JObject(), "");
        }

        private void setControlDisplayCondition(Controls _Gctrl, HtmlTextWriter writer)
        {
            if ((_Gctrl.conditionalDisplay == "0" && _Gctrl.condDisplayCntrlProps != null && _Gctrl.condDisplayCntrlProps.conditions.Count > 0) || _Gctrl.hideIfBlank == 1)
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "conditional");
            else
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "unconditional");
        }

        private string setControlDisplayCondition(string conditionalDisplay, ConditionPlugin condDisplayCntrlProps)
        {
            if (conditionalDisplay == "0" && condDisplayCntrlProps != null && condDisplayCntrlProps.conditions.Count > 0)
                return "conditional";
            return "unconditional";
        }

        void insertChartRenderContainerWithZoom(Controls _Gctrl, HtmlTextWriter writer)
        {
            //Mohan
            //Div for chart container
            if (controlTabIndex <= 0)
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin-top:16px;");

            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);

            string strClass = "unconditional ";
            if (_Gctrl.conditionalDisplay == "0" && _Gctrl.condDisplayCntrlProps != null && _Gctrl.condDisplayCntrlProps.conditions.Count > 0)
                strClass = "conditional ";
            if (_Gctrl.border != null && _Gctrl.border == "1")
                strClass += "containerBorder ";

            writer.AddAttribute(HtmlTextWriterAttribute.Class, strClass + "chartContainer");

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            //a tag for zoom
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "refresh");

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "refresh_" + _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();
            writer.RenderEndTag();

            //a tag for zoom
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "zoom");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "zoom_" + _Gctrl.userDefinedName);
            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();
            writer.RenderEndTag();
            //end a tag for zoom
            //Clear both div
            writer.AddAttribute(HtmlTextWriterAttribute.Id, "title_" + _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "title-chart");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
            //end clear both div
            if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.CYGUAGE])
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin:auto;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
            }
            //Div where chart is rendered
            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "height:300px;");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
            //end div where chart is rendered

            if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.CYGUAGE])
                writer.RenderEndTag();

            writer.RenderEndTag();
            //end Div for chart container
        }

        private JArray getLabelColorforLineAndBar(Controls _Gctrl)
        {
            JArray ja = new JArray();
            for (int index = 0; index < _Gctrl.yAxisDataObjs.Count; index++)
            {
                ja.Add(_Gctrl.yAxisDataObjs[index].label);
                // strLabels += ",'" + _Gctrl.yAxisDataObjs[index].label + "'";
            }
            //if (strLabels.Length > 0) strLabels = strLabels.Substring(1);
            return ja;
        }

        private JObject getSeriesValue(Controls _Gctrl)
        {
            JObject jort = new JObject();
            jort.Add("t", _Gctrl.xAxisData);
            for (int index = 0; index < _Gctrl.yAxisDataObjs.Count; index++)
            {
                jort.Add("v" + (index + 1), (_Gctrl.yAxisDataObjs[index].value));
            }
            return jort;
        }

        private JArray getSeriesValueDependencies(Controls _Gctrl)
        {
            JArray ja = new JArray();
            for (int index = 0; index < _Gctrl.yAxisDataObjs.Count; index++)
            {
                JObject jo = new JObject();
                jo.Add("sfac", _Gctrl.yAxisDataObjs[0].scaleFactor);
                ja.Add(jo);
            }
            return ja;
        }
        #endregion
        #region additional methods

        private string getControlWidth(string width)
        {
            string strWidth = "100";
            try
            {
                switch ((Code)Convert.ToInt32(width))
                {
                    case Code.ZERO:
                        strWidth = "25";
                        break;
                    case Code.ONE:
                        strWidth = "50";
                        break;
                    case Code.TWO:
                        strWidth = "75";
                        break;
                    case Code.THREE:
                        strWidth = "100";
                        break;
                }
            }
            catch
            {
            }
            return strWidth;
        }

        private void createLinkItemJsonForChildForm(string _ctrlType, string _ctrlName, string bindValueToProperty, string bindTextToProperty)
        {
            if (bindValueToProperty == "-1") bindValueToProperty = "";
            if (bindTextToProperty == "-1") bindTextToProperty = "";
            if (isChild && (!string.IsNullOrEmpty(bindValueToProperty) || !string.IsNullOrEmpty(bindTextToProperty)))//" + _Gctrl.lpre + "
            {
                JObject joChild = new JObject();
                joChild.Add("ctrlid", _ctrlName);
                joChild.Add("typ", _ctrlType);
                if (_ctrlType == controlsArray[(int)ControlTypeEnum.LBL])
                {
                    JArray ja = new JArray();
                    ja.Add(string.IsNullOrEmpty(bindValueToProperty) ? "" : bindValueToProperty);
                    ja.Add("");
                    ja.Add("");
                    ja.Add("");
                    ja.Add("");
                    joChild.Add("vprop", "");
                    joChild.Add("tprop", ja);
                }
                else
                {
                    joChild.Add("vprop", string.IsNullOrEmpty(bindValueToProperty) ? "" : bindValueToProperty);

                    JArray ja = new JArray();
                    ja.Add(string.IsNullOrEmpty(bindTextToProperty) ? "" : bindTextToProperty);
                    joChild.Add("tprop", ja);
                }
                setEditableChildBindingForHTMLReady.Add(joChild);
            }
        }
        private void addSectionInControls(sectionRowPanels _Gctrl)
        {
            JObject joctrl = new JObject();
            joctrl.Add("ctrlid", _Gctrl.userDefinedName);
            joctrl.Add("parent", _Gctrl.userDefinedName);
            joctrl.Add("typ", "SEC");
            if (_Gctrl.condDisplayCntrlProps != null)
            {
                JObject condis = new JObject();
                condis.Add("type", _Gctrl.condDisplayCntrlProps.matchingType);
                condis.Add("conditions", JArray.Parse(Utilities.SerializeJson<List<ConditionList>>(_Gctrl.condDisplayCntrlProps.conditions)));
                joctrl.Add("display", condis);
                foreach (ConditionList con in _Gctrl.condDisplayCntrlProps.conditions)
                {
                    if (!condistionDisplayTriggers.Contains(con.FirstCol))
                        condistionDisplayTriggers.Add(con.FirstCol);
                }
            }
            else
                joctrl.Add("display", new JObject());

            ControlsListForHTMLReady.Add(joctrl);
        }
        private void addInControlsString(Controls _Gctrl, string returnFunction, JObject JOrt, string imageColumn, JObject style, string parent)
        {
            JObject joctrl = new JObject();
            joctrl.Add("ctrlid", _Gctrl.userDefinedName);
            joctrl.Add("parent", parent);
            if (_Gctrl.condDisplayCntrlProps != null)
            {
                JObject condis = new JObject();
                condis.Add("type", _Gctrl.condDisplayCntrlProps.matchingType);
                condis.Add("conditions", JArray.Parse(Utilities.SerializeJson<List<ConditionList>>(_Gctrl.condDisplayCntrlProps.conditions)));
                joctrl.Add("display", condis);
                foreach (ConditionList con in _Gctrl.condDisplayCntrlProps.conditions)
                {
                    if (!condistionDisplayTriggers.Contains(con.FirstCol))
                        condistionDisplayTriggers.Add(con.FirstCol);
                }
            }
            else
                joctrl.Add("display", new JObject());

            if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.ANGUAGE])
                joctrl.Add("typ", "ANG");
            else if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.CYGUAGE])
                joctrl.Add("typ", "CYG");
            else
                joctrl.Add("typ", _Gctrl.type.idPrefix);

            joctrl.Add("lbl", string.IsNullOrEmpty(_Gctrl.labelText) ? (string.IsNullOrEmpty(_Gctrl.placeholder) ? "" : _Gctrl.placeholder) : _Gctrl.labelText);
            joctrl.Add("req", string.IsNullOrEmpty(_Gctrl.required) ? "0" : _Gctrl.required);
            joctrl.Add("bkn", string.IsNullOrEmpty(_Gctrl.backNavRefresh) ? "0" : _Gctrl.backNavRefresh);
            joctrl.Add("cidx", controlTabIndex.ToString());
            controlTabIndex++;

            int imageSrc = 0;
            string imageMplugin = "";
            JObject JOdb = createCommandJson(_Gctrl, returnFunction, JOrt, imageColumn, out imageSrc, out imageMplugin);
            switch (controlsArray.IndexOf(_Gctrl.type.idPrefix))
            {
                case (int)ControlTypeEnum.CUST:
                    joctrl.Add("uispt", string.IsNullOrEmpty(_Gctrl.uiScript) ? "" : _Gctrl.uiScript);
                    break;
                case (int)ControlTypeEnum.TXT:
                    joctrl.Add("vdl", string.IsNullOrEmpty(_Gctrl.validationType) ? "-1" : _Gctrl.validationType);
                    joctrl.Add("reg", string.IsNullOrEmpty(_Gctrl.regularExp) ? "-1" : _Gctrl.regularExp);
                    joctrl.Add("verr", string.IsNullOrEmpty(_Gctrl.errorMsg) ? "" : _Gctrl.errorMsg);
                    if (_Gctrl.validationType == "3" || _Gctrl.validationType == "4")
                    {
                        joctrl.Add("min", (string.IsNullOrEmpty(_Gctrl.minLeng) ? "" : _Gctrl.minLeng));
                        joctrl.Add("max", (string.IsNullOrEmpty(_Gctrl.maxLeng) ? "" : _Gctrl.maxLeng));
                    }
                    else
                    {
                        joctrl.Add("min", string.IsNullOrEmpty(_Gctrl.minVal) ? "" : _Gctrl.minVal);
                        joctrl.Add("max", string.IsNullOrEmpty(_Gctrl.maxVal) ? "" : _Gctrl.maxVal);
                    }
                    joctrl.Add("val", string.IsNullOrEmpty(_Gctrl.multipleOf) ? "" : _Gctrl.multipleOf);
                    joctrl.Add("width", getControlWidth(_Gctrl.widthInPercent));
                    joctrl.Add("mwidth", _Gctrl.minWidth);
                    break;
                case (int)ControlTypeEnum.LBL:
                    joctrl.Add("lt", _Gctrl.text == null ? "" : _Gctrl.text.Replace("//n//r", "<br/>"));
                    joctrl.Add("hidempty", _Gctrl.hideIfBlank.ToString());// _Gctrl.hideIfBlank == null ? "0" : _Gctrl.hideIfBlank);
                    JArray JApara = new JArray();
                    JArray JAltp = new JArray();
                    if (_Gctrl.labelDataObjs.Count > 0)
                        foreach (LabelParameterFromating ss in _Gctrl.labelDataObjs)
                        {
                            JObject JOpara = new JObject();
                            JObject JOfrmt = new JObject();
                            JOfrmt.Add("typ", string.IsNullOrEmpty(ss.dataType) ? "0" : ss.dataType);
                            JOfrmt.Add("dec", string.IsNullOrEmpty(ss.noOfDecimalPlaces) ? "0" : ss.noOfDecimalPlaces);
                            JOfrmt.Add("fact", string.IsNullOrEmpty(ss.mulFactor) ? "1" : ss.mulFactor);

                            JOpara.Add("frmt", JOfrmt);
                            JApara.Add(JOpara);
                            JAltp.Add((string.IsNullOrEmpty(ss.selectedCol) ? "" : ss.selectedCol));
                        }
                    else
                        for (int i = 0; i < 5; i++)
                        {
                            JObject JOpara = new JObject();
                            JObject JOfrmt = new JObject();
                            JOfrmt.Add("typ", "0");
                            JOfrmt.Add("dec", "0");
                            JOfrmt.Add("fact", "1");

                            JOpara.Add("frmt", JOfrmt);
                            JApara.Add(JOpara);
                            JAltp.Add("");
                        }
                    joctrl.Add("para", JApara);

                    if (JOdb != null) JOdb.Add("ltp", JAltp);
                    break;
                case (int)ControlTypeEnum.DTP:
                    joctrl.Add("min", (string.IsNullOrEmpty(_Gctrl.minDate) ? "" : _Gctrl.minDate));
                    joctrl.Add("max", (string.IsNullOrEmpty(_Gctrl.maxDate) ? "" : _Gctrl.maxDate));
                    joctrl.Add("minyr", _Gctrl.minVal);
                    joctrl.Add("maxyr", _Gctrl.maxVal);
                    joctrl.Add("dtyp", _Gctrl.datePickType);
                    joctrl.Add("ddt", _Gctrl.dfltValue);
                    break;
                case (int)ControlTypeEnum.IMG:
                    joctrl.Add("align", (_Gctrl.inset == "0" ? "3" : _Gctrl.imgAlignment));
                    if (_Gctrl.imageBindType == 0) JOdb = null;
                    else
                    {
                        _Gctrl.imageFixedType = imageSrc;
                        _Gctrl.mPlugin = imageMplugin;
                        _Gctrl.cache = 0;
                    }
                    joctrl.Add("src", _Gctrl.imageFixedType.ToString());
                    joctrl.Add("info", (JOdb == null ? _Gctrl.imageSource : ""));
                    joctrl.Add("mpl", string.IsNullOrEmpty(_Gctrl.mPlugin) ? "" : _Gctrl.mPlugin);
                    joctrl.Add("icac", _Gctrl.cache.ToString());
                    break;
                case (int)ControlTypeEnum.PIC:
                case (int)ControlTypeEnum.SCRB:
                    joctrl.Add("utyp", string.IsNullOrEmpty(_Gctrl.uploadType) ? "0" : _Gctrl.uploadType);
                    joctrl.Add("bkt", string.IsNullOrEmpty(_Gctrl.bucketName) ? "" : _Gctrl.bucketName);
                    joctrl.Add("acr", string.IsNullOrEmpty(_Gctrl.credential) ? "" : _Gctrl.credential);
                    joctrl.Add("fpath", string.IsNullOrEmpty(_Gctrl.filePath) ? "" : _Gctrl.filePath);
                    joctrl.Add("maxw", string.IsNullOrEmpty(_Gctrl.maxWidth) ? "" : _Gctrl.maxWidth);
                    joctrl.Add("maxh", string.IsNullOrEmpty(_Gctrl.maxHeight) ? "" : _Gctrl.maxHeight);
                    joctrl.Add("thw", string.IsNullOrEmpty(_Gctrl.width) ? "" : _Gctrl.width);
                    joctrl.Add("sclr", string.IsNullOrEmpty(_Gctrl.backgroundColor) ? "" : _Gctrl.backgroundColor);
                    joctrl.Add("spclr", string.IsNullOrEmpty(_Gctrl.penColor) ? "" : _Gctrl.penColor);
                    joctrl.Add("rsz", string.IsNullOrEmpty(_Gctrl.cropping) ? "0" : _Gctrl.cropping);
                    joctrl.Add("width", string.IsNullOrEmpty(_Gctrl.width) ? "" : _Gctrl.width);
                    break;
                case (int)ControlTypeEnum.DDL:
                    joctrl.Add("width", getControlWidth(_Gctrl.widthInPercent));
                    joctrl.Add("mwidth", _Gctrl.minWidth);
                    joctrl.Add("ptxt", string.IsNullOrEmpty(_Gctrl.promptText) ? "" : _Gctrl.promptText);
                    joctrl.Add("pval", string.IsNullOrEmpty(_Gctrl.promptValue) ? "" : _Gctrl.promptValue);
                    JArray ja = new JArray();
                    if (_Gctrl.bindType.id == ((int)DataBindType.Manual).ToString())
                    {
                        foreach (ControlOption op in _Gctrl.options)
                        {
                            JObject jo = new JObject();
                            jo.Add("t", op.text);
                            jo.Add("v", op.value);
                            ja.Add(jo);
                        }
                    }
                    joctrl.Add("opts", ja);
                    break;
                case (int)ControlTypeEnum.RPT:
                    joctrl.Add("LstError", _Gctrl.emptyListMsg);
                    joctrl.Add("rcdpg", string.IsNullOrEmpty(_Gctrl.noOfRecords) ? "" : _Gctrl.noOfRecords);
                    joctrl.Add("ttlclr", _Gctrl.titleColor);
                    joctrl.Add("clk", _Gctrl.rowClickable);
                    joctrl.Add("cap", "1");
                    joctrl.Add("tmp", _Gctrl.listType);
                    joctrl.Add("cntbl", _Gctrl.countBubble);
                    joctrl.Add("acbtn", _Gctrl.actionButton);
                    joctrl.Add("divider", _Gctrl.dataGrouping);
                    joctrl.Add("rt", JOrt);
                    joctrl.Add("src", imageColumn.Length > 0 ? imageSrc.ToString() : "");
                    joctrl.Add("mpl", imageMplugin);
                    joctrl.Add("icac", "0");
                    ja = new JArray();
                    if (_Gctrl.manualData != null)
                    {
                        int i = 0;
                        foreach (ListManualData manualData in _Gctrl.manualData)
                        {
                            JObject jo = new JObject();
                            jo.Add("ridx", i.ToString());
                            jo.Add("r", manualData.rowId);
                            jo.Add("b", manualData.title);
                            jo.Add("s", manualData.description);
                            ja.Add(jo);
                            i++;
                        }
                    }
                    joctrl.Add("opts", ja);
                    break;
                case (int)ControlTypeEnum.LOC:
                    joctrl.Add("vdl", string.IsNullOrEmpty(_Gctrl.validationType) ? "-1" : _Gctrl.validationType);
                    joctrl.Add("verr", string.IsNullOrEmpty(_Gctrl.errorMsg) ? "" : _Gctrl.errorMsg);
                    joctrl.Add("val", string.IsNullOrEmpty(_Gctrl.validationVal) ? "" : _Gctrl.validationVal);
                    joctrl.Add("gpsign", string.IsNullOrEmpty(_Gctrl.allowUsages) ? "0" : _Gctrl.allowUsages);
                    joctrl.Add("mtyp", _Gctrl.isHidden == "0" ? "2" : "1");
                    break;
                case (int)ControlTypeEnum.BCODE:
                    joctrl.Add("btyp", (string.IsNullOrEmpty(_Gctrl.barcodeType) ? "0" : _Gctrl.barcodeType));
                    break;
                case (int)ControlTypeEnum.PIE:
                case (int)ControlTypeEnum.DPIE:
                case (int)ControlTypeEnum.BAR:
                case (int)ControlTypeEnum.LINE:
                case (int)ControlTypeEnum.ANGUAGE:
                case (int)ControlTypeEnum.CYGUAGE:
                    //case ControlType.ELT:
                    joctrl.Add("style", style);
                    break;
                case (int)ControlTypeEnum.TBL:
                case (int)ControlTypeEnum.HDF:
                case (int)ControlTypeEnum.SPC:
                case (int)ControlTypeEnum.SEP:
                case (int)ControlTypeEnum.CHK:
                case (int)ControlTypeEnum.SLD:
                case (int)ControlTypeEnum.BTN:
                case (int)ControlTypeEnum.RDB:
                case (int)ControlTypeEnum.TGL:
                    break;
            }

            if (_Gctrl.type.idPrefix == controlsArray[(int)ControlTypeEnum.DDL])
            {
                if (JOdb != null) joctrl.Add("odb", JOdb);
            }
            else
            {
                if (JOdb != null) joctrl.Add("db", JOdb);
            }

            ControlsListForHTMLReady.Add(joctrl);
        }

        private JObject createCommandJson(Controls _Gctrl, string returnFunction, JObject JOrt, string imgColumnName, out int imgSrc, out string mplugin)
        {
            imgSrc = 0;
            mplugin = "";
            if (_Gctrl.databindObjs != null && _Gctrl.databindObjs.Count > 0 && !string.IsNullOrEmpty(_Gctrl.databindObjs[0].id))
            {
                string CACHE = "0";
                string EXPIRY_CONDITION = "-1";
                string EXPIRY_FREQUENCY = "-1";
                string CrProp = "";
                AppCommands appcmd = _Gctrl.databindObjs[0];
                if (appcmd.bindObjType != "6")
                {
                    DataRow[] result = dsDataObjects.Tables[0].Select("COMMAND_ID = '" + appcmd.id + "'");
                    if (result.Length > 0 && !string.IsNullOrEmpty(appcmd.bindObjType))
                    {
                        CACHE = Convert.ToString(result[0]["CACHE"]);
                        EXPIRY_CONDITION = Convert.ToString(result[0]["EXPIRY_CONDITION"]);
                        EXPIRY_FREQUENCY = Convert.ToString(result[0]["EXPIRY_FREQUENCY"]);
                        CrProp = Convert.ToString(result[0]["CREDENTIAL_PROPERTY"]);
                        CrProp = CrProp == "-1" ? "" : CrProp;

                        if (imgColumnName.Length > 0)
                        {
                            mplugin = Convert.ToString(result[0]["MPLUGIN_AGENT"]);
                            string img = Convert.ToString(result[0]["IMAGE_COLUMN"]);
                            if (!string.IsNullOrEmpty(img))
                            {
                                List<List<string>> _imgColumn = Utilities.DeserialiseJson<List<List<string>>>(img);
                                foreach (var item in _imgColumn)
                                {
                                    if (item[0].ToLower() == imgColumnName.ToLower())
                                    {
                                        imgSrc = Utilities.convertColumnImagetype(Convert.ToInt32(item[1]));
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    DataRow[] result = dsDataObjects.Tables[1].Select("OFFLINE_OBJECT_ID = '" + appcmd.id + "'");
                    if (result.Length > 0 && !string.IsNullOrEmpty(appcmd.bindObjType))
                    {
                        if (!offlineTablesNameForReturn.Contains(Convert.ToString(result[0]["TABLE_ID"])))
                            offlineTablesNameForReturn.Add(Convert.ToString(result[0]["TABLE_ID"]));
                    }
                }


                JObject JOobj = new JObject();
                JOobj.Add("ctrlid", _Gctrl.userDefinedName);
                JOobj.Add("ctyp", appcmd.bindObjType);
                JOobj.Add("cmd", appcmd.id);
                JOobj.Add("igc", appcmd.ignoreCache == true ? "1" : "0");
                JOobj.Add("cache", CACHE);
                JOobj.Add("efrq", EXPIRY_FREQUENCY);
                JOobj.Add("econ", EXPIRY_CONDITION);
                JOobj.Add("cr", CrProp);
                JOobj.Add("rcdpg", string.IsNullOrEmpty(_Gctrl.noOfRecords) ? "-1" : _Gctrl.noOfRecords);
                JOobj.Add("rtfn", returnFunction);
                JOobj.Add("rt", JOrt);
                JArray jaLp = new JArray();
                foreach (DatabindingObjParams dbp in appcmd.cmdParams)
                {
                    JObject jo = new JObject();
                    jo.Add("para", dbp.name);
                    if (dbp.type.ToUpper() == "CONSTANT")
                        jo.Add("val", dbp.val);
                    else
                        jo.Add("val", "_$VIEW." + dbp.val + ".Value");
                    jaLp.Add(jo);
                }
                JOobj.Add("lp", jaLp);
                return JOobj;
            }
            return null;
        }

        string CreateEventsAndScripts()
        {
            string BindEventInSDK = "";
            string strControlSearch = string.Empty;
            string dotaskJson = string.Empty;
            foreach (var grt in mfForm.rowPanels)
            {
                foreach (var gcol in grt.colmnPanels)
                {
                    foreach (var gctrl in gcol.controls)
                    {
                        dotaskJson = string.Empty;
                        if (gctrl.refreshControls != null && gctrl.refreshControls.Count > 0)
                            dotaskJson = "mf.reloadControls(" + (Utilities.SerializeJson<List<string>>(gctrl.refreshControls)) + ",'0');";

                        if (condistionDisplayTriggers.Contains(gctrl.userDefinedName))
                            dotaskJson += " mf.processConditionalDisplayTrigger('" + gctrl.userDefinedName + "');";

                        string onChange = Utilities.Base64Decode(gctrl.onChange);
                        switch (controlsArray.IndexOf(gctrl.type.idPrefix))
                        {
                            case (int)ControlTypeEnum.TXT:
                                strControlSearch = "#" + gctrl.userDefinedName;
                                BindEventInSDK += bindEvent(strControlSearch, 1, gctrl.userDefinedName, gctrl.onBlur, dotaskJson, true, true);
                                BindEventInSDK += bindEvent(strControlSearch, 2, gctrl.userDefinedName, gctrl.onChange, "", true, true);
                                BindEventInSDK += bindEvent(strControlSearch, 3, gctrl.userDefinedName, gctrl.onFocus, "", true, true);
                                break;
                            case (int)ControlTypeEnum.RDB:
                                strControlSearch = "input[name=" + gctrl.userDefinedName + "]";
                                BindEventInSDK += bindEvent(strControlSearch, 2, gctrl.userDefinedName, gctrl.onChange, dotaskJson, true, true);
                                break;
                            case (int)ControlTypeEnum.DDL:
                            case (int)ControlTypeEnum.TGL:
                            case (int)ControlTypeEnum.CHK:
                                strControlSearch = "#" + gctrl.userDefinedName;
                                BindEventInSDK += bindEvent(strControlSearch, 2, gctrl.userDefinedName, gctrl.onChange, dotaskJson, true, true);
                                break;
                            case (int)ControlTypeEnum.SLD:
                                strControlSearch = "#" + gctrl.userDefinedName;
                                BindEventInSDK += bindEvent(strControlSearch, 5, gctrl.userDefinedName, gctrl.onChange, dotaskJson, true, true);
                                break;
                            case (int)ControlTypeEnum.DTP:
                                strControlSearch = "#" + gctrl.userDefinedName;
                                BindEventInSDK += bindEvent(strControlSearch, 2, gctrl.userDefinedName, gctrl.onChange, dotaskJson, false, false);
                                break;
                            case (int)ControlTypeEnum.BCODE:
                                strControlSearch = "#" + gctrl.userDefinedName;
                                if (gctrl.manualEntry == "1")
                                {
                                    BindEventInSDK += bindEvent(strControlSearch, 1, gctrl.userDefinedName, gctrl.onBlur, "", true, true);
                                    BindEventInSDK += bindEvent(strControlSearch, 3, gctrl.userDefinedName, gctrl.onFocus, "", true, true);
                                }
                                BindEventInSDK += bindEvent(strControlSearch, 2, gctrl.userDefinedName, gctrl.onChange, dotaskJson, true, true);
                                break;
                            case (int)ControlTypeEnum.BTN:
                                strControlSearch = "#" + gctrl.userDefinedName;
                                BindEventInSDK += bindUserDefinedScripts("controlEvent.onTap", gctrl.onTouchScript, true, gctrl.userDefinedName, string.Empty);
                                break;
                            case (int)ControlTypeEnum.PIC:
                            case (int)ControlTypeEnum.SCRB:
                                BindEventInSDK += bindUserDefinedScripts("controlEvent.onChange", gctrl.onChange, true, gctrl.userDefinedName, string.Empty);
                                BindEventInSDK += bindUserDefinedScripts("controlEvent.onClear", gctrl.onClear, true, gctrl.userDefinedName, string.Empty);
                                break;
                            case (int)ControlTypeEnum.RPT:
                                BindEventInSDK += bindUserDefinedScripts("controlEvent.onRowTap", gctrl.scriptListRowTouch, true, gctrl.userDefinedName, string.Empty);
                                BindEventInSDK += bindUserDefinedScripts("controlEvent.onRowActionTap", gctrl.scriptBtnCntrl, true, gctrl.userDefinedName, string.Empty);
                                break;
                        }
                    }
                }
            }
            return BindEventInSDK;
        }

        private string bindEvent(string controlSearch, int evnt, string controlName, string eventContant, string extraContant, bool isRaiseEvent, bool isBindEvent)
        {
            string eventName = "", sdkEnvet = "";
            switch (evnt)
            {
                case 1:
                    eventName = "blur"; sdkEnvet = "controlEvent.onBlur"; break;
                case 2:
                    eventName = "change"; sdkEnvet = "controlEvent.onChange"; break;
                case 3:
                    eventName = "focus"; sdkEnvet = "controlEvent.onFocus"; break;
                case 4:
                    eventName = "tap"; sdkEnvet = "controlEvent.onTap"; break;
                case 5:
                    eventName = "slidestop"; sdkEnvet = "controlEvent.onChange"; break;
            }
            string strEventContant = Utilities.Base64Decode(eventContant);
            if (!string.IsNullOrEmpty(strEventContant))
            {
                strEventContant = bindUserDefinedScripts((sdkEnvet), strEventContant, false, controlName, extraContant);
                if (isRaiseEvent)
                    extraContant = "app.thisView.raiseEvent(" + sdkEnvet + ", '" + controlName + "', {});";
            }
            if (!string.IsNullOrEmpty(extraContant) && isBindEvent)// app.continueControlButtonTap()
                _scriptForWindowReady.Add("$('" + controlSearch + @"').bind('" + eventName + @"',function(){" + extraContant + " });");
            return strEventContant;
        }

        private string getPageLoadFunction()
        {
            string strfunction = createdoHtmlToNativeJson() + ((setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + setControlOnLoadScript);
            return "$(window).load(function() {" + strfunction + " });";
        }

        private string createdoHtmlToNativeJson()
        {
            JObject strJsonForOnReady = new JObject();
            strJsonForOnReady.Add("ctrls", ControlsListForHTMLReady);
            strJsonForOnReady.Add("elst", joSetEditableListItemForHTMLReady);
            strJsonForOnReady.Add("elbind", setEditableChildBindingForHTMLReady);
            if (joTablePropertyOnReady == null) strJsonForOnReady.Add("dtbl", new JObject());
            else strJsonForOnReady.Add("dtbl", joTablePropertyOnReady);

            return @"mf.initView(" + JsonConvert.SerializeObject(strJsonForOnReady) + ");";
        }

        #endregion

        public string FormHtmlHead { get { return HtmlHead; } }
        public string FormHtmlBody { get { return HtmlBody; } }
        public string FormHtmlLinks { get { return HtmlFileLinks; } }
        public string FormCompleteHTML { get { return returnHtml; } }
    }

}