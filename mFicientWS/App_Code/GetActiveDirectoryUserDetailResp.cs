﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetActiveDirectoryUserDetailResp
    {
        string  _statusCode, _statusDescription;
        ActiveDirectoryUser _userFields;

        public GetActiveDirectoryUserDetailResp(ActiveDirectoryUser userFields, string statusCode, string statusDescription)
        {
            try
            {
                _userFields = userFields;
                _statusCode = statusCode;
                _statusDescription = statusDescription;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPluginGetActiveDirectoryUserDetail Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            GetActiveDirectoryUserDetailRespFields objCheckUserRespFields = new GetActiveDirectoryUserDetailRespFields();
            objCheckUserRespFields.cd = _statusCode;
            objCheckUserRespFields.desc = _statusDescription;
            objCheckUserRespFields.data = _userFields;
            string strJsonWithDetails = Utilities.SerializeJson<GetActiveDirectoryUserDetailRespFields>(objCheckUserRespFields);
            return Utilities.getFinalJson(strJsonWithDetails);
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        public string StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }

        public ActiveDirectoryUser Data
        {
            get { return _userFields; }
            set { _userFields = value; }
        }
    }

    [DataContract]
    public class GetActiveDirectoryUserDetailRespFields
    {
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public ActiveDirectoryUser data { get; set; }
    }
}