﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace mFicientWS
{
    public class GetDBResponseForControls
    {
        public enum CALLED_FROM
        {
            DoTask,
            GetPagingData,
        }
        public static List<rptData> getRptData(
            DataSet ds, string rowIdColmn, string titleColmn,
            string subTitleColmn,
            string imageColmn,
            string countColmn,
            string infoColmn,
            string additionalInfoColmn,
            CALLED_FROM callingProcess)
        {
            List<rptData> lstRptData = new List<rptData>();
            /**
                 * Passing new list because we have to pass empty
                 * dt value even if we get null dataset after process.
                 * **/
            if (ds == null || ds.Tables.Count == 0)
            {
                lstRptData = new List<rptData>();
            }
            else
            {
                try
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        getRptData(
                            lstRptData,
                            row[rowIdColmn].ToString(),
                            row[titleColmn].ToString(),
                            String.IsNullOrEmpty(subTitleColmn) ? String.Empty : row[subTitleColmn].ToString(),
                            String.IsNullOrEmpty(imageColmn) ? String.Empty : row[imageColmn].ToString(),
                            String.IsNullOrEmpty(countColmn) ? String.Empty : row[countColmn].ToString(),
                            String.IsNullOrEmpty(infoColmn) ? String.Empty : row[infoColmn].ToString(),
                            String.IsNullOrEmpty(additionalInfoColmn) ? String.Empty : row[additionalInfoColmn].ToString()
                            );
                    }
                }
                catch
                {
                    switch (callingProcess)
                    {
                        case CALLED_FROM.DoTask:
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_COLUMN_IN_DATA_COMMAND).ToString());
                        case CALLED_FROM.GetPagingData:
                            throw new MficientException(((int)DO_TASK_ERROR.INVALID_COLUMN_IN_DATA_COMMAND).ToString());
                    }

                }
            }
            return lstRptData;
        }
        static void getRptData(
            List<rptData> rptdata, string rowid,
            string title, string subTitle,
            string image, string count,
            string info, string addInfo)
        {
            rptData obj = new rptData();
            obj.i = image;
            obj.b = title;
            obj.r = rowid;
            obj.s = subTitle;
            obj.c = count;
            obj.n = info;
            obj.an = addInfo;
            rptdata.Add(obj);
        }
    }
}