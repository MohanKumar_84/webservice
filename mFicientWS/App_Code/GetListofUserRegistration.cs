﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class GetListofUserRegistration
    {

        GetListofUserRegistrationReq _getlistuserregistreation;
   

        public GetListofUserRegistration(GetListofUserRegistrationReq getlistuserregistreation)
        {
            _getlistuserregistreation = getlistuserregistreation;
        }

        public GetListofUserRegistrationResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            TimeZoneInfo tziCompany = CompanyTimezone.getTimezoneInfo(_getlistuserregistreation.CompanyId);
            GetListMetaData getdevicedata = new GetListMetaData();
            string strQuery = @"SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                                     FROM TBL_REGISTERED_DEVICE device
                                     INNER JOIN TBL_USER_DETAIL as usr
                                     ON usr.USER_ID = device.USER_ID where device.COMPANY_ID=@ENTERPRISE_ID order by usr.USER_NAME asc;

        SELECT Request.*,usr.SUBADMIN_ID,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME ,usr.USER_NAME,usr.FIRST_NAME,usr.LAST_NAME,reqtype.REQUEST_DESCRIPTION,device.DEVICE_ID as 'RegDeviceId'
                                    ,(SELECT REQUEST_ID FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION WHERE REQUEST_DESCRIPTION = 'DENY')AS 'DenyId'
                                    FROM TBL_DEVICE_REGISTRATION_REQUEST AS request
                                    INNER JOIN TBL_USER_DETAIL AS usr
                                    ON request.USER_ID = usr.USER_ID
                                    INNER JOIN TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION AS reqtype
                                    ON reqtype.REQUEST_ID = request.REQUEST_TYPE
                                    LEFT OUTER JOIN TBL_REGISTERED_DEVICE as device
                                    ON device.DEVICE_ID = request.DEVICE_ID
                                    WHERE request.COMPANY_ID = @ENTERPRISE_ID
                                    AND STATUS =0 order by USER_NAME asc;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _getlistuserregistreation.CompanyId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            int detailrequied= _getlistuserregistreation.Type;
            List<Registeruserdevices> listregisteruser = new List<Registeruserdevices>();
            List<Pendinguserdevices> listpending = new List<Pendinguserdevices>();
          
            if (ds.Tables[0].Rows.Count > 0 && (detailrequied == 0 || detailrequied == 1))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    ResponseStatus objRespStatus = new ResponseStatus();
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                    Registeruserdevices objregisteruser = new Registeruserdevices();
                    objregisteruser.os = Convert.ToString(row["DEVICE_TYPE"]);
                    objregisteruser.dunm = Convert.ToString(row["USER_NAME"]);
                    objregisteruser.uid = Convert.ToString(row["USER_ID"]);
                    objregisteruser.mdl = Convert.ToString(row["DEVICE_MODEL"]);
                    objregisteruser.aver = Convert.ToString(row["OS_VERSION"]);
                    objregisteruser.did = Convert.ToString(row["DEVICE_ID"]);
                  //  objregisteruser.ron = Convert.ToString(row["REGISTRATION_DATE"]);
                    objregisteruser.ron = Utilities.getCompanyLocalFormattedDate(tziCompany, Convert.ToInt64(row["REGISTRATION_DATE"]));
                    listregisteruser.Add(objregisteruser);
                }
                getdevicedata.reg = listregisteruser;
            }
            else
            {
                getdevicedata.reg = listregisteruser;
            }
                if (ds.Tables[1].Rows.Count > 0  && (detailrequied == 0 || detailrequied == 2) )
                {
                    ResponseStatus objRespStatus = new ResponseStatus();
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                   
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Pendinguserdevices objpendinguser = new Pendinguserdevices();
                        objpendinguser.os = Convert.ToString(row["DEVICE_TYPE"]);
                        objpendinguser.dunm = Convert.ToString(row["USER_NAME"]);
                        objpendinguser.uid = Convert.ToString(row["USER_ID"]);
                        objpendinguser.mdl = Convert.ToString(row["DEVICE_MODEL"]);
                        objpendinguser.aver = Convert.ToString(row["OS_VERSION"]);
                        objpendinguser.ron = Utilities.getCompanyLocalFormattedDate(tziCompany, Convert.ToInt64(row["REGISTRATION_DATE"]));
                        //objpendinguser.ron = Convert.ToString(row["REGISTRATION_DATE"]);
                        listpending.Add(objpendinguser);
                    }
                    
                }
                else
                {
                    getdevicedata.pnd = listpending;
                }
              
                return new GetListofUserRegistrationResp(_respStatus, getdevicedata, _getlistuserregistreation.RequestId);
        }

        

    }
}
