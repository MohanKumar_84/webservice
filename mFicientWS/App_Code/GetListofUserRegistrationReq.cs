﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetListofUserRegistrationReq
    {

  string _requestId, _deviceId, _deviceType, _companyId,_userid,_sesstionId;
  int _functionCode, _type;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public int Type
        {
            get { return _type; }
        }

        public string UserId
        {
            get { return _userid; }
        }

        public string SesstionId
        {
            get { return _sesstionId; }
        }



        public GetListofUserRegistrationReq(string requestJson)
        {
            RequestJsonParsing<GetListofUserRegistrationReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetListofUserRegistrationReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_LIST_OF_USERS_REGISTERED_DEVICE)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _sesstionId = objRequestJsonParsing.req.sid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _type =  Convert.ToInt32(objRequestJsonParsing.req.data.typ);
            _userid = objRequestJsonParsing.req.data.unm;
           
        }
    }
    [DataContract]
    public class GetListofUserRegistrationReqData : Data
    {
        public string typ { get; set; }

        public string pwd { get; set; }
    }



    }
