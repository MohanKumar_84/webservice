﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class GetMPlugInConnectionDetailsReq
    {
        string _requestId, _companyId, _mpluginName;
        int _functionCode;
        
        public string CompanyId
        {
            get { return _companyId; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public string MpluginName
        {
            get { return _mpluginName; }
        }

        public GetMPlugInConnectionDetailsReq(string requestJson)
        {
            RequestJsonParsing<GetMPlugInConnectionDetailsData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetMPlugInConnectionDetailsData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_MPLUGIN_CONNECTION_DETAILS)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _mpluginName = objRequestJsonParsing.req.data.mpnm;
        }
    }
    [DataContract]
    public class GetMPlugInConnectionDetailsData : Data
    {
        [DataMember]
        public string mpnm
        {
            get;
            set;
        }
    }
}