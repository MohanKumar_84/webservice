﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
namespace mFicientWS
{
    public class GetMPlugInConnectionDetailsResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        List<MPlugInDatabaseConnection> _mPlugInDBConnectionDtbl;
        List<MPlugInWSConnection> _mPlugInWSDetailDtbl;
        List<MPlugInODataConnection> _mPluginODataDetailDtbl;
        private JObject _s3Bucket;
        public GetMPlugInConnectionDetailsResp(ResponseStatus respStatus, string requestId, List<MPlugInDatabaseConnection> mPlugInDBConnlist, List<MPlugInWSConnection> mPluginWSDetailDtbl, List<MPlugInODataConnection> mPluginODataDetailDtbl, JObject s3Bucket)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _mPlugInDBConnectionDtbl = mPlugInDBConnlist;
                _mPlugInWSDetailDtbl = mPluginWSDetailDtbl;
                _mPluginODataDetailDtbl = mPluginODataDetailDtbl;
                _s3Bucket = s3Bucket;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Company Details Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            GetMPlugInConnDetailsResponse objMPlugInConnDetailsResponse = new GetMPlugInConnDetailsResponse();
            objMPlugInConnDetailsResponse.func = Convert.ToString((int)FUNCTION_CODES.GET_MPLUGIN_CONNECTION_DETAILS);
            objMPlugInConnDetailsResponse.rid = _requestId;
            GetMPlugInConnDtlListResponseData objMPlugInConnDtlListRespData = new GetMPlugInConnDtlListResponseData();            
            objMPlugInConnDtlListRespData.dbcon = _mPlugInDBConnectionDtbl;
            objMPlugInConnDtlListRespData.wscon = _mPlugInWSDetailDtbl;
            objMPlugInConnDtlListRespData.odataCon = _mPluginODataDetailDtbl;
            objMPlugInConnDtlListRespData.s3 = _s3Bucket;
            //add data to the final response
            objMPlugInConnDetailsResponse.data = objMPlugInConnDtlListRespData;
            string strJsonWithDetails = Utilities.SerializeJson<GetMPlugInConnDetailsResponse>(objMPlugInConnDetailsResponse);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class GetMPlugInConnDetailsResponse : MPlugInCommonResponse
    {
        public GetMPlugInConnDetailsResponse()
        { }
        [DataMember]
        public GetMPlugInConnDtlListResponseData data { get; set; }
    }

    public class GetMPlugInConnDtlListResponseData
    {
        /// <summary>
        /// Database connection Details
        /// </summary>
        [DataMember]
        public List<MPlugInDatabaseConnection> dbcon { get; set; }

        /// <summary>
        /// Webservice connection details
        /// </summary>
        [DataMember]
        public List<MPlugInWSConnection> wscon { get; set; }

        /// <summary>
        /// OData connection details
        /// </summary>
        [DataMember]
        public List<MPlugInODataConnection> odataCon { get; set; }

        /// <summary>
        /// S3Bucket
        /// </summary>
        [DataMember]
        public JObject s3 { get; set; }
    }

    public class MPlugInDatabaseConnection
    {
        /// <summary>
        ///Connection Id
        /// </summary>
        [DataMember]
        public string connid { get; set; }

        /// <summary>
        ///Database type
        /// </summary>
        [DataMember]
        public string dbtyp { get; set; }

        /// <summary>
        ///Database host
        /// </summary>
        [DataMember]
        public string host { get; set; }

        /// <summary>
        ///Database name
        /// </summary>
        [DataMember]
        public string dbname { get; set; }

        /// <summary>
        ///Database Unique Id
        /// </summary>
        [DataMember]
        public string uid { get; set; }

        /// <summary>
        ///Password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        ///Connection timeout
        /// </summary>
        [DataMember]
        public string timeout { get; set; }

        /// <summary>
        ///additional connection string
        /// </summary>
        [DataMember]
        public string addstring { get; set; }
    }

    public class MPlugInWSConnection
    {
        /// <summary>
        ///Connection Id
        /// </summary>
        [DataMember]
        public string connid { get; set; }

        /// <summary>
        ///Web service type
        /// </summary>
        [DataMember]
        public string wstyp { get; set; }

        /// <summary>
        ///web service url
        /// </summary>
        [DataMember]
        public string url { get; set; }

        /// <summary>
        ///user id
        /// </summary>
        [DataMember]
        public string uid { get; set; }

        /// <summary>
        ///password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        ///Authentication Type
        /// </summary>
        [DataMember]
        public string authtype { get; set; }

    }

    public class MPlugInODataConnection
    {
        /// <summary>
        ///Connection Id
        /// </summary>
        [DataMember]
        public string connid { get; set; }

        /// <summary>
        ///end point
        /// </summary>
        [DataMember]
        public string endpnt { get; set; }

        /// <summary>
        ///user id
        /// </summary>
        [DataMember]
        public string uid { get; set; }

        /// <summary>
        ///password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        ///OdataServiceVersion
        /// </summary>
        [DataMember]
        public string ver { get; set; }

        /// <summary>
        ///Authentication Type
        /// </summary>
        [DataMember]
        public string authtype { get; set; }
    }
}