﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace mFicientWS
{
    public class GetMpluginAgentVersionReq
    {
        private string _requestId, _companyId, _agentname, _agentPassword;

        public string RequestId
        {
            get { return _requestId; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string AgentName
        {
            get { return _agentname; }
        }
        public string AgentPassword
        {
            get { return _agentPassword; }
        }
        public GetMpluginAgentVersionReq(string requestJson)
        {
            GetMpluginAgentversionJsonParsing objgetmpluginagentversionjsonparsing = Utilities.DeserialiseJson<GetMpluginAgentversionJsonParsing>(requestJson);
            _requestId=objgetmpluginagentversionjsonparsing.req.rid;
            _companyId = objgetmpluginagentversionjsonparsing.req.eid;
            _agentname = objgetmpluginagentversionjsonparsing.req.agtnm;
            _agentPassword = objgetmpluginagentversionjsonparsing.req.agtpwd;
        }
    }
    [DataContract]
    public class GetMpluginAgentversionJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public GetMpluginAgentversionReqData req { get; set; }
    }
    [DataContract]
    public class GetMpluginAgentversionReqData
    {
            /// Agent name from request
            /// </summary>
            [DataMember]
            public string agtnm { get; set; }

            /// <summary>
            /// Agent  Password
            /// </summary>
            [DataMember]
            public string agtpwd { get; set; }

            /// <summary>
            /// Company ID
            /// </summary>
            [DataMember]
            public string eid { get; set; }

            /// <summary>
            /// request id
            /// </summary>
            [DataMember]
            public string rid { get; set; }
    }
     }