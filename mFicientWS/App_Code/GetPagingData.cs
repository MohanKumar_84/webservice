﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Net;
namespace mFicientWS
{
    public class GetPagingData
    {
        enum GET_PAGING_DATA_USING
        {
            mPlugin,
            Cache
        }
        ResponseStatus objRespStatus;
        GetPagingDataReq _getPagingDataReq;

        public GetPagingData(GetPagingDataReq getPagingDataReq)
        {
            _getPagingDataReq = getPagingDataReq;
        }

        public GetPagingDataResp Process()
        {
            object objData = getDataFromCache();
            DataSet dsData = new DataSet();
            objRespStatus = Utilities.getResponseStatus(null);
            //List<rptData> lstRptData = null;
            if (objData == null)
            {
                objRespStatus = Utilities.getResponseStatus(
                    new Exception(((int)DO_TASK_ERROR.DATA_IN_CACHE_EXPIRED).ToString())
                    );
            }
            else
            {
                GET_PAGING_DATA_USING eDoProcessUsing = GET_PAGING_DATA_USING.Cache;
                if (objData.GetType() == typeof(string))
                {
                    try
                    {
                        dsData = getDataUsingMplugin((string)objData);
                        eDoProcessUsing = GET_PAGING_DATA_USING.mPlugin;
                    }
                    catch (MficientException ex)
                    {
                        objRespStatus = Utilities.getResponseStatus(ex);
                    }
                    catch (HttpException hex)
                    {
                        objRespStatus = HttpExecptionToResponseStatus(hex);
                    }
                }
                else
                {
                    string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _getPagingDataReq.CompanyId,
                                       _getPagingDataReq.UserId, _getPagingDataReq.DeviceId, _getPagingDataReq.DeviceType, String.Empty, String.Empty, _getPagingDataReq.DatasetId);
                    dsData = ProcessPagingHelpers.processGetResponseDS(key, _getPagingDataReq.PageNumber, _getPagingDataReq.RcdPagingCount);
                    eDoProcessUsing = GET_PAGING_DATA_USING.Cache;
                }
            }
            return new GetPagingDataResp(objRespStatus,
                _getPagingDataReq.RequestId,
                _getPagingDataReq.ReturnFunction,
                //_getPagingDataReq.ControlId,
                dsData);
        }

        ResponseStatus HttpExecptionToResponseStatus(HttpException hex)
        {
            DO_TASK_ERROR dtError = DO_TASK_ERROR.NONE;
            switch (hex.GetHttpCode())
            {
                case (int)HttpStatusCode.ServiceUnavailable:
                    dtError = DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND;
                    break;
                case (int)HttpStatusCode.GatewayTimeout:
                case (int)HttpStatusCode.RequestTimeout:
                    dtError = DO_TASK_ERROR.MPLUGIN_SERVER_NOT_REACHABLE;
                    break;
                case (int)HttpStatusCode.NotFound:
                    dtError = DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND;
                    break;
                case (int)HttpStatusCode.Unauthorized:
                    dtError = DO_TASK_ERROR.MPLUGIN_SERVER_UNAUTHORIZED;
                    break;
            }
            return Utilities.getResponseStatus(new Exception(Convert.ToString(((int)dtError)), hex));
        }

        object getDataFromCache()
        {
            string strAgentName = String.Empty;
            object objToReturn;

            string strKey = CacheManager.GetKey(
                CacheManager.CacheType.mFicientDSPaging,
                _getPagingDataReq.CompanyId,
                _getPagingDataReq.UserId,
                _getPagingDataReq.DeviceId,
                _getPagingDataReq.DeviceType, String.Empty,
                String.Empty, _getPagingDataReq.DatasetId
                );

            strAgentName = String.Empty;
            try
            {
                strAgentName = CacheManager.Get<string>(strKey,false);
            }
            catch
            {
                strAgentName = String.Empty;
            }
            if (String.IsNullOrEmpty(strAgentName))
            {
                DataSet ds = CacheManager.Get<DataSet>(strKey,false);
                if (ds == null)
                {
                    objToReturn = null;
                }
                else
                {
                    objToReturn = ds;
                }
            }
            else
            {
                objToReturn = strAgentName;
            }

            return objToReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agentName"></param>
        /// <returns></returns>
        ///<exception cref="System.MficientException"></exception>
        ///<exception cref="System.Exception"></exception>
        DataSet getDataUsingMplugin(string agentName)
        {
            DataSet dsData = null;
            GetMPluginAgentDtl objMPAgntDtl =
                       new GetMPluginAgentDtl(
                           _getPagingDataReq.CompanyId,
                           agentName
                           );
            objMPAgntDtl.Process();
            //TODO write the else condition
            if (objMPAgntDtl.StatusCode == 0)
            {
                try
                {
                    dsData = mPlugin.GetPagingData(
                          _getPagingDataReq.CompanyId,
                          objMPAgntDtl.MpAgentName,
                          objMPAgntDtl.MpAgentPassword,
                          _getPagingDataReq.DatasetId,
                          _getPagingDataReq.RcdPagingCount,
                          _getPagingDataReq.PageNumber, _getPagingDataReq.UserId
                          );
                }
                catch (mPlugin.mPluginException ex)
                {
                    throw new MficientException(Convert.ToString(((int)DO_TASK_ERROR.MPLUGIN_ERROR)));
                }
                catch (HttpException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    if (ex.Message == HttpStatusCode.NotFound.ToString())
                    {
                        throw new MficientException(Convert.ToString(((int)DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND)));
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return dsData;
        }
        DataSet getDataFromList(List<DataSet> DSChunksLst)
        {
            DataSet dsData = null;
            try
            {
                dsData = ProcessPagingHelpers.getResponseDSFromDSChunksLst(DSChunksLst);
            }
            catch (MficientException ex)
            {
                /**
                 * Passing null because we have to pass empty
                 * dt value even if we get null dataset after process.
                 * **/
                dsData = null;
            }
            return dsData;
        }
    }
}