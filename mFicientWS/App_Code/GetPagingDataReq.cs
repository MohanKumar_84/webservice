﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetPagingDataReq
    {
        string _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _controlId, _commandId;
        string _datasetId;
        int _functionCode, _rtfn, _commandType, _rcdPagingCount, _pgNumber;
        //List<QueryParameters> _parameters;
       // QueryReturnValue _returnType;
        #region Public Properties
        //public List<QueryParameters> Parameters
        //{
        //    get { return _parameters; }
        //}
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public int CommandType
        {
            get { return _commandType; }
        }
        public string CommandId
        {
            get { return _commandId; }
        }
        public int PageNumber
        {
            get { return _pgNumber; }
        }
        //public string ControlId
        //{
        //    get { return _controlId; }
        //}
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int ReturnFunction
        {
            get { return _rtfn; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        //public QueryReturnValue returnType
        //{
        //    get { return _returnType; }
        //}
        public int RcdPagingCount
        {
            get { return _rcdPagingCount; }
        }
        public string DatasetId
        {
            get { return _datasetId; }
        }
        #endregion

        public GetPagingDataReq(string requestJson, string userId)
        {
            RequestJsonParsing<GetPagingDataReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetPagingDataReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_PAGING_DATA)
            {
                throw new Exception("Invalid Function Code");
            }
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _controlId = objRequestJsonParsing.req.data.ctrlid;
            _commandId = objRequestJsonParsing.req.data.cmd;
            _rtfn = objRequestJsonParsing.req.data.rtfn;
            _commandType = objRequestJsonParsing.req.data.ctyp;
            _datasetId = objRequestJsonParsing.req.data.dsid;
            _pgNumber = objRequestJsonParsing.req.data.pgno;
            _rcdPagingCount = objRequestJsonParsing.req.data.rcdpg == null ?
                 0 : Convert.ToInt32(objRequestJsonParsing.req.data.rcdpg);


            if (_pgNumber <= 0 || String.IsNullOrEmpty(_datasetId))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
    }

    [DataContract]
    public class GetPagingDataReqData : Data
    {

        /// <summary>
        /// Control Id                    
        /// </summary>
        [DataMember]
        public string ctrlid { get; set; }

        /// <summary>
        /// Command Id
        /// </summary>
        [DataMember]
        public string cmd { get; set; }

        /// <summary>
        /// Controltype
        /// </summary>
        [DataMember]
        public int rtfn { get; set; }
        /// <summary>
        /// Dataset Id
        /// </summary>
        [DataMember]
        public string dsid { get; set; }
        /// <summary>
        /// Command Type
        /// </summary>
        [DataMember]
        public int ctyp { get; set; }

        /// <summary>
        /// PageNumber
        /// </summary>
        [DataMember]
        public int pgno { get; set; }
        /// <summary>
        /// Record Paging Count
        /// </summary>
        [DataMember]
        public string rcdpg { get; set; }
        
        
    }

}