﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class GetResponseStatus
    {
        /// <summary>
        /// To get the response status with error.
        /// The response status for error have 2 fields Code and description.
        /// So to get the response status Pass the exception with error code as message.
        ///To pass error message,pass it as inner exception message.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.MficientException">Thrown when Error code does not exists.
        /// And the error is not a code.</exception>
        public static ResponseStatus getRespStatus(MficientException ex,
            int fnCode)
        {
            FUNCTION_CODES eFnCode = getFunctionCodeType(fnCode);
            ResponseStatus objResponseStatus = new ResponseStatus();
            if (Enum.IsDefined(typeof(FUNCTION_CODES), eFnCode))
            {
                if (ex != null)
                {
                    int iErrorCode = 0;
                    bool isErrorACode = isErrorMsgAnIntErrorCode(
                        ex.Message, out iErrorCode);
                    if (isErrorACode)
                    {
                        objResponseStatus.cd = iErrorCode.ToString();
                        if (ex.InnerException == null)
                        {
                            objResponseStatus.desc =
                                getErrorMsgForCodeByFunctionCode(
                                iErrorCode,
                                eFnCode);
                        }
                        else
                        {
                            objResponseStatus.desc =
                                ex.InnerException.Message;
                        }
                    }
                    else
                        throw new MficientException("The error message is not an error code.");
                }
                else
                {
                    objResponseStatus.cd = "0";
                    objResponseStatus.desc = String.Empty;
                }
            }
            else
            {
                throw new MficientException("Invalid function code.");
            }
            return objResponseStatus;
        }
        static bool isErrorMsgAnIntErrorCode(string errorMsg, out int errorCode)
        {
            errorCode = 0;
            return Int32.TryParse(errorMsg, out errorCode);
        }
        static string getErrorMsgForCodeByFunctionCode(int errorCode,
            FUNCTION_CODES fnCode)
        {
            string strErrorMsg = String.Empty;
            ImFicientErrors objError = null;
            switch (fnCode)
            {
                case FUNCTION_CODES.COMPANY_DETAIL:
                    break;
                case FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED:
                    break;
                case FUNCTION_CODES.USER_DETAIL:
                    break;
                case FUNCTION_CODES.CHANGE_PASSWORD:
                    break;
                case FUNCTION_CODES.RESET_PASSWORD:
                    break;
                case FUNCTION_CODES.CHECK_ACCOUNT_AND_DEVICE:
                    break;
                case FUNCTION_CODES.MOBILE_USER_LOGIN:
                    break;
                case FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST:
                    break;
                case FUNCTION_CODES.RENEW_SESSION:
                    break;
                case FUNCTION_CODES.LOG_OUT:
                    break;
                case FUNCTION_CODES.ADD_DEVICE_PUSH_MSG_ID:
                    objError = new AddDevicePushMsgIdError();
                    strErrorMsg = objError.getErrorMsgFromErrorCode(errorCode);
                    break;
                case FUNCTION_CODES.GET_MENU_AND_WORKFLOW_LIST:
                    break;
                case FUNCTION_CODES.UPDATION_COMPLETE:
                    break;
                case FUNCTION_CODES.GET_APP_FILES_AS_ZIPPED:
                    break;
                case FUNCTION_CODES.WORK_FLOW_USAGE_LOG:
                    break;
                case FUNCTION_CODES.DO_TASK:
                    break;
                case FUNCTION_CODES.DO_TASK_FOR_BATCH_PROCESS:
                    break;
                case FUNCTION_CODES.GET_PAGING_DATA:
                    break;
                case FUNCTION_CODES.SUPPORT_QUERY:
                    break;
                case FUNCTION_CODES.USER_QUERY_LIST:
                    break;
                case FUNCTION_CODES.USER_QUERY_DETAIL:
                    break;
                case FUNCTION_CODES.COMMENT_AND_UPDATE_QUERY:
                    break;
                case FUNCTION_CODES.GET_QUERY_TYPE:
                    break;
                case FUNCTION_CODES.MGRAM_MESSAGE_LIST_GET:
                    break;
                case FUNCTION_CODES.MGRAM_MESSAGE_GET:
                    break;
                case FUNCTION_CODES.MGRAM_MESSAGE_STATUS_UPDATE:
                    break;
                case FUNCTION_CODES.USER_AUTHENTICATION:
                    break;
                case FUNCTION_CODES.MBUZZ_PUSHMESSAGE_SAVE:
                    break;
                case FUNCTION_CODES.MBUZZ_COMPANY_USER_LIST:
                    break;
                case FUNCTION_CODES.ADD_CONTACT_REQUEST:
                    break;
                case FUNCTION_CODES.ADD_CONTACT_CONFIRMATION:
                    break;
                case FUNCTION_CODES.REMOVE_CONTACT:
                    break;
                case FUNCTION_CODES.REMOVE_PENDING_REQUEST:
                    break;
                case FUNCTION_CODES.DESKTOP_USER_LOGIN:
                    break;
                case FUNCTION_CODES.USER_CONTACTS:
                    break;
                case FUNCTION_CODES.MPLUGIN_CLIENT_AUTHENTICATION:
                    break;
                case FUNCTION_CODES.GET_MPLUGIN_CONNECTION_DETAILS:
                    break;
                case FUNCTION_CODES.MGRAM_ENTERPRISE_DETAIL:
                    break;
                //case FUNCTION_CODES.COMPANY_REGISTRATION:
                //    objError = new CompanyRegistrationError();
                //    //strErrorMsg = objError.getErrorMsgFromErrorCode(errorCode);
                //    break;
                //case FUNCTION_CODES.TIMEZONE_LIST:
                //    objError = new GetAllTimezonesError();
                //    break;

            }
            strErrorMsg = objError.getErrorMsgFromErrorCode(errorCode);
            return strErrorMsg;
        }
        static FUNCTION_CODES getFunctionCodeType(int functionCode)
        {
            return (FUNCTION_CODES)Enum.Parse(
                typeof(FUNCTION_CODES),
                functionCode.ToString()
                );
        }
    }
}