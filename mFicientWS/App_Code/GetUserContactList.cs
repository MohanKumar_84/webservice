﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class GetUserContactList
    {
        private string userId, companyId;
        public GetUserContactList(string _userId, string _companyId)
        {
            userId = _userId;
            companyId = _companyId;
        }

        public List<UserData> GetContacts()
        {
            List<UserData> lstContacts = new List<UserData>();
            try
            {
                lstContacts = GetContactsFromDatabase();
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                throw ex;
            }
            return lstContacts;
        }
        public List<UserData> GetContactsFromDatabase()
        {
            List<UserData> lstContacts = new List<UserData>();
            SqlCommand cmd = new SqlCommand(@"select REQUESTED_BY from tbl_user_detail where  COMPANY_ID = @CompanyId AND USER_ID = @UserId ");

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", userId);
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            DataSet userDS = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);

            if(userDS!=null && userDS.Tables.Count>0)
            {
                cmd = new SqlCommand(@"select u.USER_ID as UserID,u.USER_NAME as UserName,u.FIRST_NAME + ' ' + u.LAST_NAME as Name,u.EMAIL_ID as EmailID,
                REQUESTED_BY from TBL_USER_DETAIL as u where u.company_id=@CompanyId and (ALLOW_MESSENGER=1 or DESKTOP_MESSENGER=1) and IS_BLOCKED=0 

                select distinct a.USER_ID ,a.USER_NAME,b.GROUP_ID from TBL_USER_DETAIL a  inner join TBL_USER_GROUP_LINK b on a.USER_ID=b.USER_ID 
                and a.COMPANY_ID=b.COMPANY_ID where a.COMPANY_ID=@CompanyId and @Requested_BY like '%'+b.GROUP_ID +'%'
            
                 SELECT CONTACTS FROM TBL_USER_CONTACT_FOR_MBUZZ where COMPANY_ID = @CompanyId AND USER_ID = @UserId 
 
                 SELECT FROM_USER_ID FROM TBL_ADD_CONTACT_REQUEST WHERE COMPANY_ID = @CompanyId AND TO_USER_ID = @UserId   
 
                 SELECT TO_USER_ID FROM TBL_ADD_CONTACT_REQUEST WHERE COMPANY_ID = @CompanyId AND FROM_USER_ID = @UserId ");

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@CompanyId", companyId);
                cmd.Parameters.AddWithValue("@Requested_BY", Convert.ToString(userDS.Tables[0].Rows[0]["REQUESTED_BY"]));

                DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables != null && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            int Status = -1, isTop = 0;

                            DataRow[] drSameGroupUsers = ds.Tables[1].Select("USER_ID = '" + Convert.ToString(dr["UserID"] + "'"));
                            //check in contact list
                            if (ds.Tables[2].Rows.Count > 0 && Convert.ToString(ds.Tables[2].Rows[0]["CONTACTS"]).Contains(Convert.ToString(dr["UserID"])))
                                Status = 0;
                            //check in get Requested List
                            else if (ds.Tables[3].Rows.Count > 0 && Status == -1)
                            {
                                DataRow[] drGetReuestUsers = ds.Tables[3].Select("FROM_USER_ID = '" + Convert.ToString(dr["UserID"] + "'"));
                                if (drGetReuestUsers.Count() > 0)
                                    Status = 3;
                            }
                            //check in send Requested List
                            else if (ds.Tables[4].Rows.Count > 0 && Status == -1)
                            {
                                DataRow[] drSendReuestUsers = ds.Tables[4].Select("TO_USER_ID = '" + Convert.ToString(dr["UserID"] + "'"));
                                if (drSendReuestUsers.Count() > 0)
                                    Status = 2;
                            }
                            //check in group List
                            if (drSameGroupUsers.Count() > 0 && Status == -1)
                            {
                                Status = 1;
                            }
                            //check in not in group list
                            if (drSameGroupUsers.Count() <= 0) isTop = 1;        

                            if (Status > -1)
                            {
                                if (userId != dr["UserID"].ToString())
                                {
                                    UserData objUsersData = new UserData();
                                    objUsersData.unm = dr["UserName"].ToString();
                                    objUsersData.fnm = dr["Name"].ToString();
                                    objUsersData.em = dr["EmailID"].ToString();
                                    objUsersData.dep = "";
                                    objUsersData.des = "";
                                    objUsersData.status = Status.ToString();
                                    objUsersData.istop = isTop.ToString();
                                    lstContacts.Add(objUsersData);
                                }
                            }
                        }
                    }
                }
            }
            return lstContacts;
        }
    }
}