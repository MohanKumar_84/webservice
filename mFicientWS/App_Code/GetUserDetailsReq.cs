﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetUserDetailsReq
    {
        string _requestId, _deviceId, _deviceType, _companyId, _userid, _mobileuserid, _sesstionid;
        int _functionCode, _type;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string UserId
        {
            get { return _userid; }
        }
        public string MobileUserId
        {
            get { return _mobileuserid; }
        }
        public string SesstionId
        {
            get { return _sesstionid; }
        }

        public GetUserDetailsReq(string requestJson)
        {
            RequestJsonParsing<GetUserDetailsReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetUserDetailsReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.Get_User_Detail)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userid = objRequestJsonParsing.req.data.unm;
            _mobileuserid = objRequestJsonParsing.req.data.muid;
            _sesstionid = objRequestJsonParsing.req.sid;
        }
        [DataContract]
        public class GetUserDetailsReqData : Data
        {

            [DataMember]
            public string muid { get; set; }

        }
    }
    
}