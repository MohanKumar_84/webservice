﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetWebservice_User_ListReq
    {
        string _requestId, _deviceId, _deviceType, _companyId,_userid,_password,_groupid,_sesstionid;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string UserId
        {
            get { return _userid; }
        }
        public string GroupId
        {
            get
            {
                return _groupid;
            }
        }
        public string SesstionId
        {
            get
            {
                return _sesstionid;
            }
        }

        public GetWebservice_User_ListReq(string requestJson)
        {
            RequestJsonParsing<GetWebservice_User_ListReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetWebservice_User_ListReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.Webservice_User_List)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userid = objRequestJsonParsing.req.data.unm;
            _groupid = objRequestJsonParsing.req.data.gid;
            _sesstionid = objRequestJsonParsing.req.sid;
          
        }


        [DataContract]
        public class GetWebservice_User_ListReqData : Data
        {
            [DataMember]
            public string gid { get; set; }

        }
    }
}