﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetWebservice_User_ListResp
    {
         ResponseStatus _respStatus;

        string _requestId;
        GetWebservice_User_ListRespMetaData _getmetadata;
        public GetWebservice_User_ListResp(ResponseStatus respStatus, GetWebservice_User_ListRespMetaData getlist, string requestId)
        {
            _respStatus = respStatus;
           _getmetadata = getlist;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            GetWebservice_User_ListRespJson objgetRegistrationResp = new GetWebservice_User_ListRespJson();
            objgetRegistrationResp.func = Convert.ToString((int)FUNCTION_CODES.Webservice_User_List);
            objgetRegistrationResp.rid = _requestId;
            objgetRegistrationResp.status = _respStatus;
            objgetRegistrationResp.data = _getmetadata;
            string strJsonWithDetails = Utilities.SerializeJson<GetWebservice_User_ListRespJson>(objgetRegistrationResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
        [DataContract]
        public class GetWebservice_User_ListRespJson : CommonResponse
        {
            public GetWebservice_User_ListRespJson()
            { }
            [DataMember]
            public GetWebservice_User_ListRespMetaData data { get; set; }
        }
    }
       [DataContract]
        public class Userss
        {
            [DataMember]
            public string nm { get; set; }
            [DataMember]
            public string id { get; set; }
        }

        [DataContract]
       public class GetWebservice_User_ListRespMetaData
        {
            [DataMember]
            public List<Userss> usr { get; set; }
        }
    
}