﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class Get_Menu_And_WorkFlow_Tester
    {
        Get_Menu_And_Workflows_Req _menuCategoryListReqTester;
        public Get_Menu_And_WorkFlow_Tester(Get_Menu_And_Workflows_Req menuCategoryListReq)
        {
            _menuCategoryListReqTester = menuCategoryListReq;
        }
        public Get_Menu_And_Workflows_Resp Process()
        {
            DataSet dstesterDtls = getListByCompanyId();
            if (dstesterDtls.Tables.Count != 0 && dstesterDtls.Tables[0].Rows.Count > 0)
            {
                ResponseStatus objRespStatus = new ResponseStatus();
                 objRespStatus.cd = "0";
                 objRespStatus.desc = "";
          
                  Get_Menu_And_Workflows_RespData objMenuCatListRespData = new Get_Menu_And_Workflows_RespData();

                  objMenuCatListRespData.mcat = new List<Menu_And_Workflows_Detail>();
                  Menu_And_Workflows_Detail objmcat = new Menu_And_Workflows_Detail();
                  objmcat.di = "0";
                  objmcat.mid = "AllApps";
                  objmcat.mnm = "AllApps";
                //  objmcat.typ = "0";
                  objmcat.icn = "";
                  List<WorkFlowList> lsttest = new List<WorkFlowList>();
                  foreach (DataRow row in dstesterDtls.Tables[0].Rows)
                  {
                      WorkFlowList objMenuCatListDtls = new WorkFlowList();
                      objMenuCatListDtls.wfid = Convert.ToString(row["WF_ID"]);
                      objMenuCatListDtls.appv = Convert.ToString(row["VERSION"]);
                      objMenuCatListDtls.wficn = Convert.ToString(row["ICON"]);
                      objMenuCatListDtls.wfnm = Convert.ToString(row["WF_NAME"]);
                      objMenuCatListDtls.dsc = Convert.ToString(row["WF_DESCRIPTION"]);
                      objMenuCatListDtls.typ = "1";
                      lsttest.Add(objMenuCatListDtls);
                  }
                  objmcat.wf = lsttest;
                  List<GetOfflineDBTableData> offtbl = new List<GetOfflineDBTableData>();
                  foreach (DataRow row in dstesterDtls.Tables[3].Rows)
                  {
                      GetOfflineDBTableData objOfflineListDtls = new GetOfflineDBTableData();
                      OfflinetblObject offlineobj = new OfflinetblObject();

                      string strname = Convert.ToString(row["TABLE_NAME"]);
                      objOfflineListDtls.nm = strname;
                      objOfflineListDtls.id = Convert.ToString(row["TABLE_ID"]);
                      objOfflineListDtls.mod = Convert.ToString(row["UPDATE_DATETIME"]);
                      DataRow[] rowsByofflinetblid = dstesterDtls.Tables[3].Select("OFFLINE_TABLES = '" + strname + "'");
                      for (int i = 0; i <= rowsByofflinetblid.Length - 1; i++)
                      {
                          offlineobj.id = Convert.ToString(rowsByofflinetblid[i]["OFFLINE_OBJECT_ID"]);
                          offlineobj.mod = Convert.ToString(rowsByofflinetblid[i]["UPDATED_ON"]);
                      }
                  }
                  objMenuCatListRespData.offtbl = offtbl;
                  return new Get_Menu_And_Workflows_Resp(objRespStatus, objMenuCatListRespData, _menuCategoryListReqTester.RequestId);
            }
            throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
        }

        DataSet getListByCompanyId()
        {
            string strQuery = @"select * from dbo.TBL_WORKFLOW_DETAIL_FOR_TESTER where COMPANY_ID=@CompanyId order by WF_NAME asc;
                 Select * from  dbo.TBL_OFFLINE_DATA_TABLE where COMPANY_ID=@CompanyId;
                SELECT TABLE_ID,UPLOAD_COMMAND_ID,DOWNLOAD_COMMAND_ID,T.DB_CONNECTOR_ID,CN.CONNECTION_NAME,CN.CREDENTIAL_PROPERTY  from dbo.TBL_OFFLINE_DATA_TABLE  AS C
                INNER JOIN (SELECT * FROM  dbo.TBL_DATABASE_COMMAND  WHERE COMPANY_ID=@CompanyId)T
                ON T. DB_COMMAND_ID=C.UPLOAD_COMMAND_ID OR  T. DB_COMMAND_ID=C.DOWNLOAD_COMMAND_ID INNER JOIN dbo.TBL_DATABASE_CONNECTION AS CN ON
               CN.DB_CONNECTOR_ID=T.DB_CONNECTOR_ID WHERE  CN.COMPANY_ID=@CompanyId  AND C.COMPANY_ID=@CompanyId 
               GROUP BY TABLE_ID,UPLOAD_COMMAND_ID,DOWNLOAD_COMMAND_ID,T.DB_CONNECTOR_ID,CN.CONNECTION_NAME,CN.CREDENTIAL_PROPERTY;
                 select * from dbo.TBL_OFFLINE_OBJECTS where COMPANY_ID=@CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _menuCategoryListReqTester.CompanyId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}