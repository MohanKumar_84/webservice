﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace mFicientWS
{
    public class Get_Menu_And_Workflows
    {
        Get_Menu_And_Workflows_Req _menuCategoryListReq;

        public Get_Menu_And_Workflows(Get_Menu_And_Workflows_Req menuCategoryListReq)
        {
            _menuCategoryListReq = menuCategoryListReq;
        }

        public Get_Menu_And_Workflows_Resp Process()
        {
            DataSet dsMenuCategoryDtls = getMenuCategoryListByCompanyAndUserId();
            string offline = "";
            if (dsMenuCategoryDtls.Tables.Count != 0 && dsMenuCategoryDtls.Tables[0].Rows.Count > 0)
            {
                if (dsMenuCategoryDtls.Tables[1].Rows.Count != 0)
                {
                    ResponseStatus objRespStatus = new ResponseStatus();
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                    Get_Menu_And_Workflows_RespData objMenuCatListRespData = new Get_Menu_And_Workflows_RespData();
                    List<GetOfflineDBTableData> offtbl = new List<GetOfflineDBTableData>();
                    List<Menu_And_Workflows_Detail> lstMenuCatgoryListDtls = new List<Menu_And_Workflows_Detail>();
                    string offlineNew;
                    foreach (DataRow row in dsMenuCategoryDtls.Tables[1].Rows)
                    {
                        offlineNew = "";
                        Menu_And_Workflows_Detail objMenuCatListDtls = new Menu_And_Workflows_Detail();
                        objMenuCatListDtls.mid = Convert.ToString(row["MENU_CATEGORY_ID"]);
                        objMenuCatListDtls.mnm = Convert.ToString(row["CATEGORY"]);
                        objMenuCatListDtls.di = Convert.ToString(row["DISPLAY_INDEX"]);
                        objMenuCatListDtls.icn = Convert.ToString(row["CATEGORY_ICON"]);
                        objMenuCatListDtls.wf = GetWfDetail(Convert.ToString(row["MENU_CATEGORY_ID"]), out offlineNew);
                        offline += offlineNew;
                        lstMenuCatgoryListDtls.Add(objMenuCatListDtls);
                    }
                    objMenuCatListRespData.mcat = lstMenuCatgoryListDtls;
                    GetOfflineDBTableData objOfflinTbl = new GetOfflineDBTableData();
                    foreach (DataRow row in dsMenuCategoryDtls.Tables[2].Rows)
                    {
                        bool isUsed = false;
                        objOfflinTbl = new GetOfflineDBTableData();
                        OfflinetblObject offlineObj = new OfflinetblObject();
                        string strname = Convert.ToString(row["TABLE_NAME"]);
                        objOfflinTbl.nm = strname;
                        objOfflinTbl.id = Convert.ToString(row["TABLE_ID"]);
                        objOfflinTbl.mod = Convert.ToString(row["UPDATE_DATETIME"]);
                        objOfflinTbl.obj = new List<OfflinetblObject>();
                        DataRow[] objectByOfflineTblid = dsMenuCategoryDtls.Tables[4].Select("OFFLINE_TABLES ='" + Convert.ToString(row["TABLE_ID"]) + "'");
                        for (int i = 0; i <= objectByOfflineTblid.Length - 1; i++)
                        {
                            offlineObj = new OfflinetblObject();
                            offlineObj.id = Convert.ToString(objectByOfflineTblid[i]["OFFLINE_OBJECT_ID"]);
                            offlineObj.mod = Convert.ToString(objectByOfflineTblid[i]["UPDATED_ON"]);
                            objOfflinTbl.obj.Add(offlineObj);
                            if (offline.Contains("OF_" + offlineObj.id.ToUpper())) isUsed = true;
                        }
                        if (isUsed)
                            offtbl.Add(objOfflinTbl);
                    }
                    objMenuCatListRespData.offtbl = offtbl;
                    return new Get_Menu_And_Workflows_Resp(objRespStatus, objMenuCatListRespData, _menuCategoryListReq.RequestId);
                }
            }
            throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
        }

        DataSet getMenuCategoryListByCompanyAndUserId()
        {
            string strQuery = @"SELECT a.GROUP_ID from TBL_USER_GROUP_LINK as a where a.user_id=@UserId;
            SELECT * FROM TBL_MENU_CATEGORY AS TBL_CAT WHERE COMPANY_ID = @CompanyId ORDER BY DISPLAY_INDEX;
            SELECT * from  dbo.TBL_OFFLINE_DATA_TABLE where COMPANY_ID=@CompanyId ;
            SELECT TABLE_ID,UPLOAD_COMMAND_ID,DOWNLOAD_COMMAND_ID,T.DB_CONNECTOR_ID,CN.CONNECTION_NAME,CN.CREDENTIAL_PROPERTY  from dbo.TBL_OFFLINE_DATA_TABLE  AS C
                INNER JOIN (SELECT * FROM  dbo.TBL_DATABASE_COMMAND  WHERE COMPANY_ID=@CompanyId)T
                ON T. DB_COMMAND_ID=C.UPLOAD_COMMAND_ID OR  T. DB_COMMAND_ID=C.DOWNLOAD_COMMAND_ID INNER JOIN dbo.TBL_DATABASE_CONNECTION AS CN ON
                CN.DB_CONNECTOR_ID=T.DB_CONNECTOR_ID WHERE  CN.COMPANY_ID=@CompanyId  AND C.COMPANY_ID=@CompanyId 
                GROUP BY TABLE_ID,UPLOAD_COMMAND_ID,DOWNLOAD_COMMAND_ID,T.DB_CONNECTOR_ID,CN.CONNECTION_NAME,CN.CREDENTIAL_PROPERTY;
            SELECT * from dbo.TBL_OFFLINE_OBJECTS where COMPANY_ID=@CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _menuCategoryListReq.CompanyId);
            cmd.Parameters.AddWithValue("@UserId", _menuCategoryListReq.UserId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

        private List<WorkFlowList> GetWfDetail(string menuID, out string offline)
        {
            offline = "";
            appFlowDetailAndForm app = new appFlowDetailAndForm(_menuCategoryListReq.UserId, _menuCategoryListReq.CompanyId, Convert.ToInt32(_menuCategoryListReq.ModelType));
            List<appFlowDetail> lstapp = app.GetAppListMenuWise(menuID);

            List<WorkFlowList> objWorkFlowList = new List<WorkFlowList>();
            foreach (appFlowDetail appflow in lstapp)
            {
                WorkFlowList objWFListDtls = new WorkFlowList();
                objWFListDtls.wfid = appflow.WF_ID;
                objWFListDtls.wfnm = appflow.WF_NAME;
                objWFListDtls.dsc = appflow.WF_DESCRIPTION;
                objWFListDtls.wficn = appflow.WF_ICON;
                objWFListDtls.appv = appflow.VERSION;
                objWFListDtls.off = appflow.Offline;
                objWFListDtls.typ = "1";
                objWorkFlowList.Add(objWFListDtls);
                offline += appflow.ObjectRefrence;
            }
            return objWorkFlowList;
        }
    }
}