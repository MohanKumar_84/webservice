﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;

//namespace mFicientWS
//{
//    public class Get_Offline_Data_Table_Meta_Resp
//    {
//        ResponseStatus _respStatus;
//        GetOfflineDataTableMetaData _OfflineMetaList;
//        string _requestId;

//        public Get_Offline_Data_Table_Meta_Resp(ResponseStatus respStatus, GetOfflineDataTableMetaData offlineMetaList, string requestId)
//        {
//            try
//            {
//                _respStatus = respStatus;
//                _OfflineMetaList = offlineMetaList;
//                _requestId = requestId;
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Error creating Menu Category List Response[" + ex.Message + "]");
//            }
//        }

//        public string GetResponseJson()
//        {
//            GetOfflineDataTableMetaResp objofflineresp = new GetOfflineDataTableMetaResp();
//            objofflineresp.func = Convert.ToString((int)FUNCTION_CODES.GET_OFFLINE_DATA_TABLE_META);
//            objofflineresp.rid = _requestId;
//            objofflineresp.status = _respStatus;
//            objofflineresp.data = _OfflineMetaList;
//            string strJsonWithDetails = Utilities.SerializeJson<GetOfflineDataTableMetaResp>(objofflineresp);
//            return Utilities.getFinalJson(strJsonWithDetails);
//        }
//    }

//    public class GetOfflineDataTableMetaResp : CommonResponse
//    {
//        public GetOfflineDataTableMetaResp()
//        {
//        }
//        [DataMember]
//        public GetOfflineDataTableMetaData data { get; set; }
//    }
//    public class GetOfflineDataTableMetaData
//    {
        
//        [DataMember]
//        public List<GetOfflineDBTableMetadata> offtbl { get; set; }
//    }

//    public class GetOfflineDBTableMetadata
//    {
       
//        [DataMember]
//        public string nm { get; set; }
//        [DataMember]
//        public string id { get; set; }
//        [DataMember]
//        public string type { get; set; }
//        [DataMember]
//        public List<string> sync { get; set; }
//        [DataMember]
//        public string prmt { get; set; }
//        [DataMember]
//        public string cr { get; set; }
//        [DataMember]
//        public OFFDownload down { get; set; }
//        [DataMember]
//        public OFFUpload up { get; set; }
//        [DataMember]
//        public List<Offline_columns> col { get; set; }
//    }

//    [DataContract]
//    public class OFFDownload
//    {
//        [DataMember]
//        public string cln { get; set; }
//        [DataMember]
//        public string disa { get; set; }
//        [DataMember]
//        public string epr { get; set; }
//        [DataMember]
//        public string efr { get; set; }
//        [DataMember]
//        public string econ { get; set; }
//        [DataMember]
//        public List<OFFListParameters> lp { get; set; }
//    }

//    [DataContract]
//    public class OFFUpload
//    {
//        [DataMember]
//        public string rem { get; set; }
//        [DataMember]
//        public string seq { get; set; }
//        [DataMember]
//        public string nor { get; set; }
//        [DataMember]
//        public List<OFFListParameters> newlp { get; set; }
//        [DataMember]
//        public List<OFFListParameters> uplp { get; set; }
//    }

//    [DataContract]
//    public class OFFListParameters
//    {
//        [DataMember]
//        public string para { get; set; }
//        [DataMember]
//        public string val { get; set; }
//    }

//    [DataContract]
//    public class Offline_columns
//    {
//        [DataMember]
//        public string cnm { get; set; }
//        [DataMember]
//        public string ctyp { get; set; }
//        [DataMember]
//        public string dpnt { get; set; }
//        [DataMember]
//        public string min { get; set; }
//        [DataMember]
//        public string max { get; set; }
//        [DataMember]
//        public string dval { get; set; }
//        [DataMember]
//        public string pk { get; set; }
//    }
//    [DataContract]
//    public class OfflineDataTableSelecttblJson
//    {
//        public OfflineDataTableSelecttblJson() { }
//        [DataMember]
//        public GetOfflineDBTableMetadata tbl { get; set; }
//    }
//    [DataContract]
//    public class OfflineDataTableSynctblCont
//    {
//        public OfflineDataTableSynctblCont() { }
//        [DataMember]
//        public OfflineTableSynctblDtls sync { get; set; }
//    }

//    [DataContract]
//    public class OfflineTableSynctblDtls
//    {
//        public OfflineTableSynctblDtls() { }
//        [DataMember]
//        public string cmdType { get; set; }
//        [DataMember]
//        public string conVal { get; set; }
//        [DataMember]
//        public string downCmdVal { get; set; }
//        [DataMember]
//        public string upCmdVal { get; set; }
//        [DataMember]
//        public List<OFFListParameters> uploadInParam { get; set; }
//        [DataMember]
//        public List<OFFListParameters> downloadInParam { get; set; }
//        [DataMember]
//        public List<OFFListParameters> downloadOutParam { get; set; }
//    }
//}