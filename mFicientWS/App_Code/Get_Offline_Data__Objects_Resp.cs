﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class Get_Offline_Data__Objects_Resp
    {
        ResponseStatus _respStatus;
        Get_Offline_Data_ObjectsData _offinedata;
        string _requestId;
        public Get_Offline_Data__Objects_Resp(ResponseStatus respStatus, Get_Offline_Data_ObjectsData offinedata, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _offinedata = offinedata;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Menu Category List Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            GetOfflineDataObjectsResp objMenuCatgoryListResp = new GetOfflineDataObjectsResp();
            objMenuCatgoryListResp.func = Convert.ToString((int)FUNCTION_CODES.GET_OFFLINE_DATA_OBJECTS);
            objMenuCatgoryListResp.rid = _requestId;
            objMenuCatgoryListResp.status = _respStatus;
            objMenuCatgoryListResp.data = _offinedata;
            string strJsonWithDetails = Utilities.SerializeJson<GetOfflineDataObjectsResp>(objMenuCatgoryListResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }

    }
    public class GetOfflineDataObjectsResp : CommonResponse
    {
        public GetOfflineDataObjectsResp()
        { }

        [DataMember]
        public Get_Offline_Data_ObjectsData data { get; set; }
    }
    public class Get_Offline_Data_ObjectsData
    {
        /// <summary>
        /// Offline object  Details
        /// </summary>
        [DataMember]
        public List<OfflineObjectDetails> obj { get; set; }
    }
    public class OfflineObjectInput
    {
        /// <summary>
        ///Object Name
        /// </summary>
        [DataMember]
        public string para { get; set; }

        /// <summary>
        ///Object Id
        /// </summary>
        [DataMember]
        public string typ { get; set; }
    }
    public class OfflineObjectDetails
    {
        /// <summary>
        ///Object Name
        /// </summary>
        [DataMember]
        public string nm { get; set; }

        /// <summary>
        ///Object Id
        /// </summary>
        [DataMember]
        public string id { get; set; }

        /// <summary>
        ///Object Type
        /// </summary>
        [DataMember]
        public string typ { get; set; }

        /// <summary>
        ///Query
        /// </summary>
        [DataMember]
        public string where { get; set; }

        [DataMember]
        public List<string> input { get; set; }

        [DataMember]
        public List<OfflineObjectColumn> col { get; set; }

        [DataMember]
        public string tbl { get; set; }

        [DataMember]
        public string error { get; set; }

        [DataMember]
        public string exit { get; set; }

        [DataMember]
        public string rtry { get; set; }

    }
    public class OfflineObjectColumn
    {
        /// <summary>
        ///Object Name
        /// </summary>
        [DataMember]
        public string para { get; set; }

        /// <summary>
        ///Object Id
        /// </summary>
        [DataMember]
        public string col { get; set; }
    }

}