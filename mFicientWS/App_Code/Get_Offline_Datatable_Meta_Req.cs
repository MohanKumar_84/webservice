﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class Get_Offline_Datatable_Meta_Req
    {
        string _sessionId, _requestId, _deviceId, _deviceType, _companyId,_username;
        List<string> _tableId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string Username
        {
            get { return _username; }
        }
        public List<string> Tableid
        {
            get { return _tableId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
       
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
       
        int FunctionCode
        {
            get { return _functionCode; }
        }
       
        public Get_Offline_Datatable_Meta_Req(string requestJson)
        {
            RequestJsonParsing<GETOfflineDatatableRequestMetaData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GETOfflineDatatableRequestMetaData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);

            if (_functionCode != ((int)FUNCTION_CODES.GET_OFFLINE_DATA_TABLE_META ))
            {
                throw new Exception("Invalid Function Code");
            }
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _username = objRequestJsonParsing.req.data.unm;
            _tableId = objRequestJsonParsing.req.data.tblid;
            _companyId = objRequestJsonParsing.req.data.enid; 
        }
    }

    [DataContract]
    public class GETOfflineDatatableRequestMetaData : Data
    {
        /// <summary>
        /// Table Id
        /// </summary>
        [DataMember]
        public List<string> tblid { get; set; }
    }

    }
