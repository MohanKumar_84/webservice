﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class HtmlFilesForAppAsHTML
    {
        GetHtmlFilesOfAppAsHTMLReq _getHtmlFilesOfAppAsHTMLReq;
        public HtmlFilesForAppAsHTML(GetHtmlFilesOfAppAsHTMLReq getHtmlFilesOfAppAsHtml)
        {
            _getHtmlFilesOfAppAsHTMLReq = getHtmlFilesOfAppAsHtml;
        }
        public string Process()
        {
            string strReturn = String.Empty;
                DataSet dtObjects;
                appFlowDetailAndForm objapp = new appFlowDetailAndForm(_getHtmlFilesOfAppAsHTMLReq.UserId, _getHtmlFilesOfAppAsHTMLReq.CompanyId, Convert.ToInt32(_getHtmlFilesOfAppAsHTMLReq.ModelType));
                jqueryAppClass appDetail = objapp.GetNewAppJson(_getHtmlFilesOfAppAsHTMLReq.AppId, out dtObjects);

                if (appDetail == null)
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

                try
                {
                    strReturn = GetAppsHtmlHelpersNew.getSingleHtmlForApp(appDetail, _getHtmlFilesOfAppAsHTMLReq.UiVersion, appDetail.name, dtObjects,Convert.ToInt32(_getHtmlFilesOfAppAsHTMLReq.ModelType));
                }
                catch (Exception e)
                {
                    strReturn = String.Empty;
                    if (e.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                    }
                }

            return strReturn;
        }
    }
}