﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class LogOutReq
    {
        string _sessionId, _requestId, _deviceId, _deviceType, _userId, _companyId, _userName, _model;

        public string UserName
        {
            get { return _userName; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }
        int _functionCode;

        public string UserId
        {
            get { return _userId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        //public string Email
        //{
        //    get { return _email; }
        //}
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public string Model
        {
            get { return _model; }
        }


        public LogOutReq(string requestJson, string userId, string model)
        {
            RequestJsonParsing<LogOutReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<LogOutReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.LOG_OUT)
            {
                throw new Exception("Invalid Function Code");
            }
            //_email = objRequestJsonParsing.req.data.em;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userName = objRequestJsonParsing.req.data.unm;
            _model = model;
            if (_userName.Contains("\\"))
            {
                _userName = _userName.Split('\\')[1];
            }
            _userId = userId;
        }

    }

    [DataContract]
    public class LogOutReqData : Data
    {
        public LogOutReqData()
        { }
    }
}