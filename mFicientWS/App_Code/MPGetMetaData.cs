﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MPGetMetaData
    {
        public enum GET_META_DATA_QUERY_TYPE
        {
            DSN = 0,
            DATABASE = 1,

        }
        MPGetMetaDataReq _mPlugInGetMetaDataReq;

        public MPGetMetaData(MPGetMetaDataReq mPlugInGetMetaDataReq)
        {
            _mPlugInGetMetaDataReq = mPlugInGetMetaDataReq;
        }

        public MPGetMetaDataResp Process()
        {
            if (String.IsNullOrEmpty(_mPlugInGetMetaDataReq.RequestId) || String.IsNullOrEmpty(_mPlugInGetMetaDataReq.ConnectorId)
                 || String.IsNullOrEmpty(_mPlugInGetMetaDataReq.CompanyId) || String.IsNullOrEmpty(_mPlugInGetMetaDataReq.AgentName)
                || String.IsNullOrEmpty(_mPlugInGetMetaDataReq.AgentPassword) || String.IsNullOrEmpty(_mPlugInGetMetaDataReq.MetaDataType)
                || String.IsNullOrEmpty(_mPlugInGetMetaDataReq.QueryType))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());

            }
            try
            {
                string mPluginResponse = "";
                if (String.IsNullOrEmpty(_mPlugInGetMetaDataReq.TableName))
                {
                    mPluginResponse = mPlugin.GetDatabaseMetaData(_mPlugInGetMetaDataReq.CompanyId, _mPlugInGetMetaDataReq.AgentName,
                        _mPlugInGetMetaDataReq.AgentPassword, _mPlugInGetMetaDataReq.ConnectorId,
                       (mPlugin.MetadataType)Enum.Parse(typeof(mPlugin.MetadataType), _mPlugInGetMetaDataReq.MetaDataType),
                        
                       ((GET_META_DATA_QUERY_TYPE)Enum.Parse(typeof(GET_META_DATA_QUERY_TYPE), _mPlugInGetMetaDataReq.QueryType)
                       == GET_META_DATA_QUERY_TYPE.DSN
                       ? mPlugin.HttpRequestType.DSN_QUERY : mPlugin.HttpRequestType.DATABASE_QUERY), _mPlugInGetMetaDataReq.ConnectionUserId, _mPlugInGetMetaDataReq.ConnectionPassword);
                }
                else
                {
                    mPluginResponse = mPlugin.GetDatabaseMetaData(

                            _mPlugInGetMetaDataReq.CompanyId, _mPlugInGetMetaDataReq.AgentName,
                            _mPlugInGetMetaDataReq.AgentPassword, _mPlugInGetMetaDataReq.ConnectorId,
                            (mPlugin.MetadataType)Enum.Parse(typeof(mPlugin.MetadataType), _mPlugInGetMetaDataReq.MetaDataType),
                            _mPlugInGetMetaDataReq.TableName,

                            ((GET_META_DATA_QUERY_TYPE)Enum.Parse(typeof(GET_META_DATA_QUERY_TYPE), _mPlugInGetMetaDataReq.QueryType)
                                == GET_META_DATA_QUERY_TYPE.DSN ?
                                mPlugin.HttpRequestType.DSN_QUERY : mPlugin.HttpRequestType.DATABASE_QUERY
                             ), _mPlugInGetMetaDataReq.ConnectionUserId, _mPlugInGetMetaDataReq.ConnectionPassword
                     );
                }
                return new MPGetMetaDataResp(mPluginResponse, "0", "");

            }
            catch (mPlugin.mPluginException e)
            {
                return new MPGetMetaDataResp("", "1", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}