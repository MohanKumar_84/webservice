﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPGetMetaDataResp
    {
        string _data,_statusCode,_description;
        public MPGetMetaDataResp(string data,string statusCode,string description)
        {
            try
            {
                _data = data;
                _statusCode = statusCode;
                _description = description;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPlugInGetMetaDataResp Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
           //string str= "{resp:{\"cd\":\"" + _statusCode + "\",\"desc\":\"" + _description + "\",\"data\":\"" + _data + "\"}}";
           //return str;

           MPGetMetaDataResponse objGetMetaDataResp = new MPGetMetaDataResponse();
           objGetMetaDataResp.cd = _statusCode;
           objGetMetaDataResp.desc = _description;
           objGetMetaDataResp.data = _data;
           string strJsonWithDetails = Utilities.SerializeJson<MPGetMetaDataResponse>(objGetMetaDataResp);
           return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class MPGetMetaDataResponse 
    {
        
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public string data { get; set; }
    }
}