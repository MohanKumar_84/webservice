﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPGetOdataMetadataReq
    {
        string _requestId, _companyId, _agentName, _agentPassword, _wsURL, _httpUserName, _httpUserPassword, _httpAuthenticationType;

        public string HttpUserPassword
        {
            get { return _httpUserPassword; }
        }
        public string HttpUserName
        {
            get { return _httpUserName; }
        }
        public string WsURL
        {
            get { return _wsURL; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string AgentPassword
        {
            get { return _agentPassword; }

        }
        public string AgentName
        {
            get { return _agentName; }

        }
        public string CompanyId
        {
            get { return _companyId; }

        }
        public string HttpAuthenticationType
        {
            get { return _httpAuthenticationType; }

        }

        public MPGetOdataMetadataReq(string requestJson)
        {
            MPGetOdataMetadataReqJsonParsing objRequestJsonParsing = Utilities.DeserialiseJson<MPGetOdataMetadataReqJsonParsing>(requestJson);
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.eid;
            _agentName = objRequestJsonParsing.req.agtnm;
            _agentPassword = objRequestJsonParsing.req.agtpwd;
            _wsURL = objRequestJsonParsing.req.wsurl;
            _httpUserName = objRequestJsonParsing.req.hunm;
            _httpUserPassword = objRequestJsonParsing.req.hpwd;
            _httpAuthenticationType = objRequestJsonParsing.req.atyp;
            if (string.IsNullOrEmpty(_httpAuthenticationType)) _httpAuthenticationType = "0";
            if (String.IsNullOrEmpty(_requestId) || String.IsNullOrEmpty(_wsURL)
                 || String.IsNullOrEmpty(_companyId) || String.IsNullOrEmpty(_agentName)
                || String.IsNullOrEmpty(_agentPassword))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());

            }
        }
    }


    [DataContract]
    public class MPGetOdataMetadataReqJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPGetOdataMetadataReqReqFields req { get; set; }
    }

    [DataContract]
    public class MPGetOdataMetadataReqReqFields
    {

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        ///company id
        /// </summary>
        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string agtnm { get; set; }

        [DataMember]
        public string agtpwd { get; set; }
        [DataMember]
        public string wsurl { get; set; }
        [DataMember]
        public string hunm { get; set; }
        [DataMember]
        public string hpwd { get; set; }
        [DataMember]
        public string atyp { get; set; }
    }
    
}