﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPGetOdataMetadataResp
    {
        string _data,_statusCode,_description;
        public MPGetOdataMetadataResp(string data, string statusCode, string description)
        {
            try
            {
                _data = data;
                _statusCode = statusCode;
                _description = description;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPGetOdataMetadataResp Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
           
            MPGetOdataMetadataResponse objGetOdataMetadataResp = new MPGetOdataMetadataResponse();
            objGetOdataMetadataResp.cd = _statusCode;
            objGetOdataMetadataResp.desc = _description;
            objGetOdataMetadataResp.data = _data;
            string strJsonWithDetails = Utilities.SerializeJson<MPGetOdataMetadataResponse>(objGetOdataMetadataResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class MPGetOdataMetadataResponse 
    {
        
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public string data { get; set; }
    }
    
}