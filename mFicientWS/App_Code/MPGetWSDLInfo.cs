﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MPGetWSDLInfo
    {
        MPGetWSDLInfoReq _mpGetWSDLInfoReq;

        public MPGetWSDLInfo(MPGetWSDLInfoReq mpGetWSDLInfoReq)
        {
            _mpGetWSDLInfoReq = mpGetWSDLInfoReq;
        }

        public MPGetWSDLInfoResp Process()
        {
            if (String.IsNullOrEmpty(_mpGetWSDLInfoReq.RequestId) || String.IsNullOrEmpty(_mpGetWSDLInfoReq.WsURL)
                 || String.IsNullOrEmpty(_mpGetWSDLInfoReq.CompanyId) || String.IsNullOrEmpty(_mpGetWSDLInfoReq.AgentName)
                || String.IsNullOrEmpty(_mpGetWSDLInfoReq.AgentPassword))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());

            }
            try
            {
                string strMPluginResponse = "";
                //if (String.IsNullOrEmpty(_mPlugInGetMetaDataReq.TableName))
                  //{
                //    mPluginResponse = mPlugin.GetDatabaseMetaData(_mPlugInGetMetaDataReq.CompanyId, _mPlugInGetMetaDataReq.AgentName, _mPlugInGetMetaDataReq.AgentPassword, _mPlugInGetMetaDataReq.ConnectorId
                //       , (mPlugin.MetadataType)Enum.Parse(typeof(mPlugin.MetadataType), _mPlugInGetMetaDataReq.MetaDataType));
                //}
                //else
                //{
                //    mPluginResponse = mPlugin.GetDatabaseMetaData(_mPlugInGetMetaDataReq.CompanyId, _mPlugInGetMetaDataReq.AgentName, _mPlugInGetMetaDataReq.AgentPassword, _mPlugInGetMetaDataReq.ConnectorId
                //    , (mPlugin.MetadataType)Enum.Parse(typeof(mPlugin.MetadataType), _mPlugInGetMetaDataReq.MetaDataType), _mPlugInGetMetaDataReq.TableName);
                //}
                strMPluginResponse = mPlugin.RunWSDLWebservice(_mpGetWSDLInfoReq.CompanyId, _mpGetWSDLInfoReq.AgentName, _mpGetWSDLInfoReq.AgentPassword, _mpGetWSDLInfoReq.WsURL
                                    , _mpGetWSDLInfoReq.HttpUserName, _mpGetWSDLInfoReq.HttpUserPassword, _mpGetWSDLInfoReq.AuthenticationType,"sub-admin");
                return new MPGetWSDLInfoResp(strMPluginResponse, "0", "");

            }
            catch (mPlugin.mPluginException e)
            {
                return new MPGetWSDLInfoResp("", "1", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch(Exception e)
            {
                throw e ;
            }
        }
    }
}