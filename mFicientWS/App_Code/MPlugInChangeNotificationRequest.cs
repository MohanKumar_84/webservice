﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPlugInChangeNotificationRequest
    {
        [DataMember]
        public string rid { get; set; }

        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string mpwd { get; set; }

        [DataMember]
        public MPlugInChangeNotificationData data { get; set; }
    }

    public class MPlugInChangeNotificationData 
    {
        [DataMember]
        public string rtyp { get; set; }
    }
}