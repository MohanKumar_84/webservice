﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MPlugInChangesNotificationToServerResp
    {
        string _response;
        public MPlugInChangesNotificationToServerResp(string response)
        {
            try
            {
                _response = response;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPluginTestConnection Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            return _response;
        }
    }
}