﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPlugInDoTaskRequest<T>
    {
        [DataMember]
        public string rid { get; set; }

        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string mpwd { get; set; }

        [DataMember]
        public T data { get; set; }
    }
    public class MPlugInDoTaskRequestCommonData
    {
        [DataMember]
        public string rtyp { get; set; }

        [DataMember]
        public string connid { get; set; }

        [DataMember]
        public string querytype { get; set; }
    }
    public class MPlugInDoTaskRequestDBData : MPlugInDoTaskRequestCommonData
    {
        [DataMember]
        public string sql { get; set; }

        [DataMember]
        public List<MPlugInDoTaskParameter> param { get; set; }
    }

    public class MPlugInDoTaskRequestWSData : MPlugInDoTaskRequestCommonData
    {
        [DataMember]
        public string httpdata { get; set; }
    }
    public class MPlugInDoTaskParameter
    {
        [DataMember]
        public string pname { get; set; }
        
        [DataMember]
        public string pvalue { get; set; }
    }
}