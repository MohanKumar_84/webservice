﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPlugInDoTaskResponse
    {
        [DataMember]
        public MPlugInDoTaskResponseFields resp { get; set; }
    }
    public class MPlugInDoTaskResponseFields
    {
        [DataMember]
        public string rid { get; set; }

        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string rtyp { get; set; }

        [DataMember]
        public MPlugInDoTaskResponseData data { get; set; }
    }
    public class MPlugInDoTaskResponseData
    {
        [DataMember]
        public string error { get; set; }

        [DataMember]
        public string errormsg { get; set; }

        [DataMember]
        public string httpresponse { get; set; }

        [DataMember]
        public string inerror { get; set; }

        [DataMember]
        public string inerrormsg { get; set; }

        [DataMember]
        public string reccount { get; set; }

        [DataMember]
        public string records { get; set; }

        [DataMember]
        public string result { get; set; }

        [DataMember]
        public string status { get; set; }
    }
}