﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MPluginTestConnectionResp
    {
        string _response;
        public MPluginTestConnectionResp(string response)
        {
            try
            {
                _response = response;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPluginTestConnection Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            return _response;
        }
    }
}