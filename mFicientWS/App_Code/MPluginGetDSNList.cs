﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
namespace mFicientWS
{
    public class MPluginGetDSNList
    {
        MPluginGetDSNListReq _mPlugInGetDSNListReq;
        public MPluginGetDSNList(MPluginGetDSNListReq getDSNListReq)
        {
            _mPlugInGetDSNListReq = getDSNListReq;
        }
        public MPluginGetDSNListResp Process()
        {
            if (String.IsNullOrEmpty(_mPlugInGetDSNListReq.RequestId)
                || String.IsNullOrEmpty(_mPlugInGetDSNListReq.EnterpriseId)
                || String.IsNullOrEmpty(_mPlugInGetDSNListReq.AgentName)
                || String.IsNullOrEmpty(_mPlugInGetDSNListReq.AgentPassword))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            MPluginGetDSNDsnField objDsnFields = new MPluginGetDSNDsnField();
            try
            {
                List<DSN> lstDSN = mPlugin.GetDSNList(_mPlugInGetDSNListReq.EnterpriseId,
                    _mPlugInGetDSNListReq.AgentName, _mPlugInGetDSNListReq.AgentPassword);
                objDsnFields.dsn = lstDSN;
                return new MPluginGetDSNListResp(objDsnFields, "0", "");
            }
            catch (mPlugin.mPluginException e)
            {
                objDsnFields.dsn = new List<DSN>();
                return new MPluginGetDSNListResp(objDsnFields, "99941", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }



        }


    }
}