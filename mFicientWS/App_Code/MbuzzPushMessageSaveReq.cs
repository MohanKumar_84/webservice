﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class MbuzzPushMessageSaveReq
    {
        string _requestId, _companyId, _message, _fromUser, _userName;
        int _functionCode, _pushMessageType, _source, _badgeCount;
        public string Username
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public int PushMessageType
        {
            get { return _pushMessageType; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string Message
        {
            get { return _message; }
        }
        public int Source
        {
            get { return _source; }
        }
        public int BadgeCount
        {
            get { return _badgeCount; }
        }
        public string FromUser
        {
            get { return _fromUser; }
        }

        public MbuzzPushMessageSaveReq(string requestJson)
        {
            RequestJsonParsing<PushMessageforContactStatusReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<PushMessageforContactStatusReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MBUZZ_PUSHMESSAGE_SAVE)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _pushMessageType = Convert.ToInt32(objRequestJsonParsing.req.data.pmtyp);
            _message =objRequestJsonParsing.req.data.msg;
            _fromUser = objRequestJsonParsing.req.data.funm;
            _badgeCount = Convert.ToInt32(objRequestJsonParsing.req.data.bdcnt);
            _source = Convert.ToInt32(objRequestJsonParsing.req.data.src);
            _userName = objRequestJsonParsing.req.data.unm;
            if (string.IsNullOrEmpty(_fromUser) || string.IsNullOrEmpty(_fromUser))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
    }

    [DataContract]
    public class PushMessageforContactStatusReqData : Data
    {
        public PushMessageforContactStatusReqData()
        { }

        /// <summary>
        /// Status Type
        /// </summary>
        [DataMember]
        public string pmtyp { get; set; }

        [DataMember]
        public string msg { get; set; }

        [DataMember]
        public string funm { get; set; }

        [DataMember]
        public string bdcnt { get; set; }

        [DataMember]
        public string src { get; set; }

        [DataMember]
        public string dtyp { get; set; }
    }
}