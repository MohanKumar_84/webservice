﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class ModifyUserforGroupResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public ModifyUserforGroupResp(ResponseStatus respStatus, string requestId)
        {
         //   try
         //   {
                _respStatus = respStatus;
                _requestId = requestId;
          //  }
            //catch (Exception ex)
            //{
            //    throw new Exception("Error getting contact status Response[" + ex.Message + "]");
            //}
        }
        public string GetResponseJson()
        {
            ModifyUserforGroupRespStatusResponse objModifyUserforGroupRespStatus = new ModifyUserforGroupRespStatusResponse();
            objModifyUserforGroupRespStatus.rid = _requestId;
            objModifyUserforGroupRespStatus.func = Convert.ToString((int)FUNCTION_CODES.MODIFY_USER_GROUP);
            objModifyUserforGroupRespStatus.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<ModifyUserforGroupRespStatusResponse>(objModifyUserforGroupRespStatus);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class ModifyUserforGroupRespStatusResponse : CommonResponse
    {
        public ModifyUserforGroupRespStatusResponse()
        {

        }
    }
}