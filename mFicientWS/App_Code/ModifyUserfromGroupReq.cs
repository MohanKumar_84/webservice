﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ModifyUserfromGroupReq
    {
        string _requestId, _deviceId, _deviceType, _companyId, _userid, _groupid, _userids, _sesstionId;
        int _functionCode, _type;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public int Type
        {
            get { return _type; }
        }
        public string User
        {
            get { return _userid; }
        }
        public string GroupId
        {
            get { return _groupid; }
        }
        public string UserId
        {
            get { return _userids; }
        }

        public string UserIds
        {
            get { return _userids; }
        }

        public string SesstionId
        {
            get { return _sesstionId; }
        }


        public ModifyUserfromGroupReq(string requestJson)
        {
            RequestJsonParsing<ModifyUserfromGroupReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<ModifyUserfromGroupReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MODIFY_USER_GROUP)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _sesstionId = objRequestJsonParsing.req.sid;
            _userid = objRequestJsonParsing.req.data.unm;
            _groupid = objRequestJsonParsing.req.data.gid;
            _type =Convert.ToInt32(objRequestJsonParsing.req.data.type);
            _userids = objRequestJsonParsing.req.data.uids;
           
        }


        [DataContract]
        public class ModifyUserfromGroupReqData : Data
        {
            public ModifyUserfromGroupReqData()
            { }

            /// <summary>
            ///  Type
            /// </summary>
            [DataMember]
            public string type { get; set; }

            [DataMember]
            public string gid { get; set; }

            [DataMember]
            public string uids { get; set; }

         
        }
    }
}