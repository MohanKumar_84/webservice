﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Data.Edm;
using Microsoft.Data.OData;
using System.Xml;
using System.IO;

namespace mFicientWS
{
    public enum FunctionType
    {
        OTHER = 0,
        WEBGET = 1,
        WEBINVOKE = 2,
        ACTION = 3
    }

    public enum ODataRequestType
    {
        ENTITYTYPE = 0,
        FUNCTION = 1
    }

    public class ODATA
    {
        private IEdmModel edmModel;

        #region Constructor

        public ODATA(string _metadata)
        {
            try
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(_metadata);
                MemoryStream ms = new System.IO.MemoryStream(bytes);
                XmlReader reader = XmlReader.Create(ms);
                edmModel = Microsoft.Data.Edm.Csdl.EdmxReader.Parse(reader);
            }
            catch (Exception ex)
            {
                throw new Exception(@"Invalid CSDL", ex); 
            }
        }

        #endregion

        #region Public Properties

        public IEdmModel EdmModel
        {
            get
            {
                return edmModel;
            }
        }

        public string MajorVersion
        {
            get
            {
                return edmModel.GetEdmVersion().Major.ToString();
            }
        }

        public string MinorVersion
        {
            get
            {
                return edmModel.GetEdmVersion().Minor.ToString();
            }
        }

        public IEnumerable<IEdmEntityContainer> EntityContainers
        {
            get
            {
                return edmModel.EntityContainers();
            }
        }

        public IEnumerable<IEdmSchemaElement> EntityTypes
        {
            get
            {
                return edmModel.SchemaElements;
            }
        }

        public IEnumerable<IEdmSchemaElement> SchemaElements
        {
            get
            {
                return edmModel.SchemaElements;
            }
        }

        #endregion

        #region Public Methods

        public IEnumerable<IEdmFunctionImport> GetFunctionImports(IEdmEntityContainer container)
        {
            try
            {
                return container.FunctionImports();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IEdmFunctionImport> GetFunctionImport(IEdmEntityContainer container, string functionName)
        {
            List<IEdmFunctionImport> reqFunctions = new List<IEdmFunctionImport>();
            try
            {
                IEnumerable<IEdmFunctionImport> functions = GetFunctionImports(container);
                foreach (IEdmFunctionImport func in functions)
                {
                    if (func.Name == functionName)
                    {
                        reqFunctions.Add(func);
                        break;
                    }
                }
                return reqFunctions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetFunctionReturnType(IEdmFunctionImport function, out Microsoft.Data.Edm.EdmTypeKind kind, out IEnumerable<IEdmStructuralProperty> structuralProperties)
        {
            structuralProperties = null;
            try
            {
                if (function.ReturnType != null)
                {
                    kind = function.ReturnType.TypeKind();
                    if (kind == EdmTypeKind.Collection || kind == EdmTypeKind.Entity || kind == EdmTypeKind.EntityReference)
                    {
                        IEdmEntitySet entitySet = null;
                        function.TryGetStaticEntitySet(out entitySet);
                        IEdmEntityType typ = entitySet.ElementType;
                        structuralProperties = GetStructuralProperties(typ);
                        return typ.Name;
                    }
                    else
                    {
                        return function.ReturnType.Definition.ToString().Split('.')[1];
                    }
                }
                else
                {
                    kind = EdmTypeKind.None;
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetHttpMethodType(IEdmFunctionImport function, out FunctionType functionType)
        {
            string methodType = string.Empty;
            functionType = FunctionType.ACTION;
            try
            {
                IEnumerable<Microsoft.Data.Edm.Annotations.IEdmDirectValueAnnotation> annotations = edmModel.DirectValueAnnotations(function);
                foreach (Microsoft.Data.Edm.Annotations.IEdmDirectValueAnnotation objAnnotaion in annotations)
                {
                    Microsoft.Data.Edm.Library.Values.EdmStringConstant val = (Microsoft.Data.Edm.Library.Values.EdmStringConstant)objAnnotaion.Value;
                    methodType = val.Value;
                    if (val.Value == "GET")
                        functionType = FunctionType.WEBGET;
                    else
                        functionType = FunctionType.WEBINVOKE;
                    break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (string.IsNullOrEmpty(methodType))
                methodType = "POST";
            return methodType;
        }

        public IEnumerable<IEdmEntitySet> GetEntitySets(IEdmEntityContainer container)
        {
            try
            {
                return container.EntitySets();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEdmEntityType GetEntityType(IEdmEntityContainer container, IEdmEntitySet entitySet)
        {
            try
            {
                if (entitySet.Container == container)
                    return entitySet.ElementType;
                else
                    throw new Exception(@"IEdmEntityContainer is different from the given entityset container!");
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<IEdmStructuralProperty> GetEntityTypeKey(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredKey;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<IEdmNavigationTargetMapping> GetNavigationPropertyMapping(IEdmEntityContainer container, IEdmEntitySet entitySet)
        {
            try
            {
                if (entitySet.Container == container)
                    return entitySet.NavigationTargets;
                else
                    throw new Exception(@"IEdmEntityContainer is different from the given entityset container!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<IEdmStructuralProperty> GetStructuralProperties(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredStructuralProperties();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<IEdmProperty> GetAllProperties(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredProperties;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<IEdmNavigationProperty> GetNavigationProperties(IEdmEntityType entityType)
        {
            try
            {
                return entityType.DeclaredNavigationProperties();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEdmProperty GetProperty(string propertyName, IEdmEntityType entityType)
        {
            try
            {
                return entityType.FindProperty(propertyName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPropertyType(IEdmProperty property, out Microsoft.Data.Edm.EdmTypeKind kind)
        {
            try
            {
                kind = property.Type.TypeKind();
                return property.Type.FullName();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEdmEntitySet GetNavigationPropertyTarget(IEdmEntityContainer container, IEdmEntitySet entitySet,IEdmNavigationProperty navigationProperty)
        {
            try
            {
                if (entitySet.Container == container)
                    return entitySet.FindNavigationTarget(navigationProperty);
                else
                    throw new Exception(@"IEdmEntityContainer is different from the given entityset container!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}