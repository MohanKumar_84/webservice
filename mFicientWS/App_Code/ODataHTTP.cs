﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Threading;

namespace mFicientWS
{

    public class ODataHTTP
    {
        internal String strURL = string.Empty;
        private String UseURL = string.Empty;
        internal int Retry = 3, Timeout = 10000;
        internal int RetryDelay = 50;
        internal String ResponseText = string.Empty, ValidResponse = string.Empty;
        internal Boolean blnRequestSuccess = false;
        internal String ErrorMessage = string.Empty;
        private Byte[] ContentData;
        private String strRequestMethod = @"GET", strUsername = string.Empty, strPassword = string.Empty, strAuthenticationType = @"Basic";
        private Boolean blnWaitForResponse = true;
        private string postString = string.Empty;
        private ResponseType requestFormat;
        private FunctionType functionType;
        private ODataRequestType oDataRequestType;
        private string maxDataServiceVersion = string.Empty;

        internal ODataHTTP(String URL)
        {
            strURL = URL;
            UseURL = strURL;
        }

        internal String URL
        {
            get
            {
                URL = strURL;
                return URL;
            }
            set
            {
                strURL = value;
                UseURL = strURL;
            }
        }

        internal string MaxDataServiceVersion
        {
            get
            {
                return maxDataServiceVersion;
            }
            set
            {
                maxDataServiceVersion = value;
            }
        }

        internal string PostString
        {
            get
            {
                return postString;
            }
            set
            {
                postString = value;
            }
        }

        internal ResponseType RequestFormat
        {
            get
            {
                return requestFormat;
            }
            set
            {
                requestFormat = value;
            }
        }

        internal void SetHttpCredentials(String UID, String PWD)
        {
            strUsername = UID;
            strPassword = PWD;
        }

        internal enum EnumHttpMethod
        {
            HTTP_POST = 1,
            HTTP_GET = 2,
            HTTP_PUT = 3,
            HTTP_PATCH = 4,
            HTTP_MERGE = 5,
            HTTP_DELETE = 6
        };

        internal FunctionType Functiontype
        {
            get
            {
                return functionType;
            }
            set
            {
                functionType = value;
            }
        }

        internal ODataRequestType ODataRequesttype
        {
            get
            {
                return oDataRequestType;
            }
            set
            {
                oDataRequestType = value;
            }
        }

        internal EnumHttpMethod HttpRequestMethod
        {
            get
            {
                switch (strRequestMethod.Trim().ToUpper())
                {
                    case "POST":
                        HttpRequestMethod = EnumHttpMethod.HTTP_POST;
                        break;
                    case "GET":
                        HttpRequestMethod = EnumHttpMethod.HTTP_GET;
                        break;
                    case "PUT":
                        HttpRequestMethod = EnumHttpMethod.HTTP_PUT;
                        break;
                    case "PATCH":
                        HttpRequestMethod = EnumHttpMethod.HTTP_PATCH;
                        break;
                    case "MERGE":
                        HttpRequestMethod = EnumHttpMethod.HTTP_MERGE;
                        break;
                    case "DELETE":
                        HttpRequestMethod = EnumHttpMethod.HTTP_DELETE;
                        break;
                }
                return HttpRequestMethod;
            }
            set
            {
                switch (value)
                {
                    case EnumHttpMethod.HTTP_GET:
                        strRequestMethod = @"GET";
                        break;
                    case EnumHttpMethod.HTTP_POST:
                        strRequestMethod = @"POST";
                        break;
                    case EnumHttpMethod.HTTP_PUT:
                        strRequestMethod = @"PUT";
                        break;
                    case EnumHttpMethod.HTTP_PATCH:
                        strRequestMethod = @"PATCH";
                        break;
                    case EnumHttpMethod.HTTP_MERGE:
                        strRequestMethod = @"MERGE";
                        break;
                    case EnumHttpMethod.HTTP_DELETE:
                        strRequestMethod = @"DELETE";
                        break;
                }
            }
        }

        internal enum EnumHttpAuthentication
        {
            BASIC = 0,
            DIGEST = 1
        };

        internal EnumHttpAuthentication HttpAuthentication
        {
            get
            {
                switch (strAuthenticationType.Trim().ToUpper())
                {
                    case "BASIC":
                        HttpAuthentication = EnumHttpAuthentication.BASIC;
                        break;
                    case "DIGEST":
                        HttpAuthentication = EnumHttpAuthentication.DIGEST;
                        break;
                }
                return HttpAuthentication;
            }
            set
            {
                switch (value)
                {
                    case EnumHttpAuthentication.BASIC:
                        strAuthenticationType = @"Basic";
                        break;
                    case EnumHttpAuthentication.DIGEST:
                        strAuthenticationType = @"Digest";
                        break;
                }
            }
        }

        public HttpResponseStatus Request()
        {
            HttpStatusCode StatusCode = HttpStatusCode.RequestTimeout;
            blnRequestSuccess = false;
            try
            {
                int i = 0;
                ResponseText = "";
                ErrorMessage = "";
                if (UseURL.Trim().Length <= 0) return new HttpResponseStatus(blnRequestSuccess, HttpStatusCode.BadRequest, @"URL not defined for HTTP request", string.Empty);

                HttpWebRequest myHttpWebRequest = null;
                HttpWebResponse myHttpWebResponse = null;
                StreamReader myHttpWebReader = null;
                String myHttpWebResult = "";
                Stream myHttpRequestStream = null;
                for (i = 0; i <= Retry; i++)
                {
                    ErrorMessage = "";
                    ResponseText = "";
                    try
                    {
                        //Creates an HttpWebRequest with the specified URL.
                        myHttpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(UseURL));
                        switch (requestFormat)
                        {
                            case ResponseType.JSON:
                                myHttpWebRequest.Accept = "application/json;odata=verbose";
                                break;
                            case ResponseType.XML:
                                myHttpWebRequest.Accept = "application/atom+xml";
                                break;
                            case ResponseType.OTHER:
                                myHttpWebRequest.Accept = "application/xml";
                                break;
                        }
                        myHttpWebRequest.Timeout = Timeout;
                        myHttpWebRequest.AllowAutoRedirect = true;
                        myHttpWebRequest.MaximumAutomaticRedirections = 10;
                        myHttpWebRequest.KeepAlive = true;
                        myHttpWebRequest.Method = strRequestMethod;
                        if (!string.IsNullOrEmpty(maxDataServiceVersion)) myHttpWebRequest.Headers.Add("MaxDataServiceVersion", maxDataServiceVersion.Trim());

                        if (strUsername.Trim().Length > 0 || strPassword.Trim().Length > 0)
                        {
                            try
                            {
                                myHttpWebRequest.PreAuthenticate = true;

                                CredentialCache myCredentialCache = new CredentialCache();
                                myCredentialCache.Add(new Uri(UseURL), strAuthenticationType, new NetworkCredential(strUsername.Trim(), strPassword.Trim()));
                                myHttpWebRequest.Credentials = myCredentialCache;
                            }
                            catch (Exception ex)
                            {
                                ErrorMessage = "Credential Error";
                            }

                            if (ErrorMessage.Trim().Length > 0) return new HttpResponseStatus(blnRequestSuccess, HttpStatusCode.Unauthorized, ErrorMessage, string.Empty);
                        }            

                        if (strRequestMethod != "GET" && strRequestMethod != "DELETE" && oDataRequestType == ODataRequestType.ENTITYTYPE)
                        {
                            ContentData = System.Text.Encoding.UTF8.GetBytes(postString);
                            myHttpWebRequest.ContentLength = ContentData.Length;
                            myHttpRequestStream = myHttpWebRequest.GetRequestStream();
                            myHttpRequestStream.Write(ContentData, 0, ContentData.Length);
                            myHttpRequestStream.Close();
                        }
                        else if (strRequestMethod != "GET" && strRequestMethod != "DELETE" && oDataRequestType == ODataRequestType.FUNCTION)
                        {
                            if (functionType == FunctionType.ACTION)
                            {
                                ContentData = System.Text.Encoding.UTF8.GetBytes(postString);
                                myHttpWebRequest.ContentLength = ContentData.Length;
                                myHttpRequestStream = myHttpWebRequest.GetRequestStream();
                                myHttpRequestStream.Write(ContentData, 0, ContentData.Length);
                                myHttpRequestStream.Close();
                            }
                        }


                        try
                        {
                            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                            if (blnWaitForResponse)
                            {
                                if (StatusCode == HttpStatusCode.OK)
                                {

                                    myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                    myHttpWebResult = myHttpWebReader.ReadToEnd();
                                    ResponseText = myHttpWebResult;
                                    blnRequestSuccess = true;
                                }
                                else
                                {
                                    try
                                    {
                                        myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                        myHttpWebResult = myHttpWebReader.ReadToEnd();
                                        ResponseText = myHttpWebResult;
                                    }
                                    catch
                                    {
                                    }
                                    if (ResponseText.Trim().Length <= 0)
                                    {
                                        ResponseText = myHttpWebResponse.StatusDescription;
                                    }
                                    if (ResponseText.Trim().Length <= 0)
                                        ResponseText = "ERROR " + myHttpWebResponse.StatusCode.ToString();
                                    ErrorMessage = ResponseText;
                                }
                            }
                        }
                        catch (WebException wex)
                        {
                            StatusCode = ((System.Net.HttpWebResponse)wex.Response).StatusCode;
                            ErrorMessage = ((System.Net.HttpWebResponse)wex.Response).StatusDescription;
                        }
                        catch (Exception ex)
                        {
                            StatusCode = HttpStatusCode.BadRequest;
                            ErrorMessage = ex.Message;
                        }
                        finally
                        {
                        }
                        // wait for Response 
                        
                    }
                    catch (Exception ex)
                    {
                        StatusCode = HttpStatusCode.BadRequest;
                        ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        if (myHttpRequestStream != null)
                        {
                            myHttpRequestStream.Close();
                            myHttpRequestStream = null;
                        }
                        if (myHttpWebReader != null)
                        {
                            myHttpWebReader.Close();
                            myHttpWebReader = null;
                        }
                        if (myHttpWebResponse != null)
                        {
                            myHttpWebResponse.Close();
                            myHttpWebResponse = null;
                        }
                        if (myHttpWebRequest != null) myHttpWebRequest = null;
                    }

                    if (blnRequestSuccess || !blnWaitForResponse)
                    {
                        break;
                    }
                    if (i < Retry) Thread.Sleep(RetryDelay);
                    Thread.Sleep(1);
                }
                if (!blnRequestSuccess && ErrorMessage.Trim().Length <= 0)
                {
                    ErrorMessage = "HTTP Request Failed";
                }
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }

            return new HttpResponseStatus(blnRequestSuccess, StatusCode, ErrorMessage, ResponseText);
        }
    }
}