﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace mFicientWS
{
    public class ProcessPagingHelpers
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="completeData"></param>
        /// <param name="chunkSize"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.MficientException">If dataset is empty.</exception>
        public static List<DataSet> makeDSChunks(DataSet completeData,
             int chunkSize)
        {
            if (completeData == null) throw new ArgumentNullException();
            DataTable dtblCompleteData = null;

            if (completeData.Tables.Count > 0)
            {
                dtblCompleteData = completeData.Tables[0];
            }
            if (dtblCompleteData == null) throw new MficientException("Dataset is empty.");
            
            List<DataSet> lstDSChunks = new List<System.Data.DataSet>();
            int iChunkSize = chunkSize;
            int iChunksAvailable = 0;

            iChunksAvailable = (dtblCompleteData.Rows.Count / iChunkSize) +
                (dtblCompleteData.Rows.Count % iChunkSize == 0 ? 0 : 1);

            int iIterationStartIndex = 0;
            int iIterationLastIndex =dtblCompleteData.Rows.Count< iChunkSize?
                dtblCompleteData.Rows.Count:iChunkSize;

            for (int i = 0; i <= iChunksAvailable - 1; i++)
            {
                DataSet dsData = new DataSet();

                DataTable newTable = dtblCompleteData.Clone();

                for (int j = iIterationStartIndex; j < iIterationLastIndex; j++)
                {
                    newTable.ImportRow(dtblCompleteData.Rows[j]);
                }
                dsData.Tables.Add(newTable);
                lstDSChunks.Add(dsData);

                iIterationStartIndex = iIterationStartIndex + iChunkSize;
                iIterationLastIndex = iIterationLastIndex + iChunkSize;
            }
            return lstDSChunks;
        }
        /// <summary>
        /// Get the response Dataset from Lst.
        /// Also Reomves the first element from the list
        /// </summary>
        /// <param name="chunkDSLst"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.MficientException">If the list is empty</exception>
        public static DataSet processGetResponseDS(string datasetId, int pgNumber, int blkSize)
        {
            try
            {
                DataTable reqDatatable = new DataTable();
                DataSet ds = new DataSet();

                DataSet dataset = CacheManager.Get<DataSet>(datasetId, false);
                if (dataset == null || dataset.Tables.Count == 0)
                    return null;

                DataTable dataTable = dataset.Tables[0];
                int recordCount = dataTable.Rows.Count;

                if (recordCount == 0) return null;

                reqDatatable = dataTable.Clone();

                if (recordCount > 0)
                {
                    reqDatatable = dataTable.Clone();
                    int startIndex = (pgNumber - 1) * blkSize;
                    int lastIndex = startIndex + blkSize - 1;
                    if (lastIndex >= recordCount) lastIndex = recordCount - 1;

                    for (int index = startIndex; index <= lastIndex; index++)
                    {
                        DataRow dataRow = dataTable.Rows[index];
                        reqDatatable.ImportRow(dataRow);
                    }
                }

                ds.Tables.Add(reqDatatable);
                return ds;
            }
            catch (MficientException ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Gets the first record from the List.
        /// </summary>
        /// <param name="chunkDSLst"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.MficientException">If the list is empty</exception>
        public static DataSet getResponseDSFromDSChunksLst(List<DataSet> chunkDSLst)
        {
            if (chunkDSLst == null) throw new ArgumentNullException();
            if (chunkDSLst.Count > 0)
            {
                DataSet dsResponseData = chunkDSLst[0];
                return dsResponseData;
            }
            else
            {
                throw new MficientException(((int)DO_TASK_ERROR.CACHE_LIST_IS_EMPTY).ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunkDSLst"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.MficientException">If the list is empty</exception>
        public static List<DataSet> removeFirstDataFromList(List<DataSet> chunkDSLst)
        {
            if (chunkDSLst == null) throw new ArgumentNullException();
            if (chunkDSLst.Count == 0) throw new MficientException(((int)DO_TASK_ERROR.CACHE_LIST_IS_EMPTY).ToString());
            if (chunkDSLst.Count > 0) chunkDSLst.RemoveAt(0);
            return chunkDSLst;
        }
    }
}