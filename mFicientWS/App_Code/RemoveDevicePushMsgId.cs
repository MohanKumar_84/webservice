﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class RemoveDevicePushMsgId
    {
        RemoveDevicePushMsgIdReq _removeDevPushMagId;
        public RemoveDevicePushMsgId(RemoveDevicePushMsgIdReq removeDevPushMagId)
        {
            _removeDevPushMagId = removeDevPushMagId;
        }
        public string Process()
        {
            SqlTransaction transaction = null;
            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con);
            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        removePushMsgIdForDevice(con, transaction);
                        SqlConnection objmGramCon = null;
                        MSSqlDatabaseClient.SqlConnectionOpen(out objmGramCon, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                            MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
                        mFicientCommonProcess.SaveDevcDtlsInMGramInsertUpdate.deleteDeviceDtls(_removeDevPushMagId.CompanyId,
                                       _removeDevPushMagId.DeviceId,
                                       _removeDevPushMagId.DeviceType,
                                       _removeDevPushMagId.UserName, objmGramCon);
                        transaction.Commit();
                        return ((int)HttpStatusCode.OK).ToString();
                    }
                }

            }
            catch (SqlException e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }

        }

        void removePushMsgIdForDevice(SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = @"UPDATE TBL_REGISTERED_DEVICE
                                SET DEVICE_PUSH_MESSAGE_ID=''
                                WHERE DEVICE_ID=@DEVICE_ID
                                AND DEVICE_TYPE=@DEVICE_TYPE
                                AND USER_ID=@USER_ID
                                AND COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _removeDevPushMagId.DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _removeDevPushMagId.DeviceType);
            cmd.Parameters.AddWithValue("@USER_ID", _removeDevPushMagId.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _removeDevPushMagId.CompanyId);

            //MSSqlClient.ExecuteNonQueryRecord(cmd);
            int iRowsEffected = cmd.ExecuteNonQuery();
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
        }

    }
}