﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class RemoveDevicePushMsgIdReq
    {
        string _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId;
        int _functionCode;
        string _userName;

        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public RemoveDevicePushMsgIdReq(string requestJson, string userId)
        {
            RequestJsonParsing<RemoveDevicePushMsgIdReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<RemoveDevicePushMsgIdReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            //if (_functionCode != (int)FUNCTION_CODES.REMOVE_DEV_PUSH_MSG_ID)
            //{
            //    throw new Exception("Invalid Function Code");
            //}
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userName = objRequestJsonParsing.req.data.unm;
        }
    }

    [DataContract]
    public class RemoveDevicePushMsgIdReqData : Data
    {
    }
}