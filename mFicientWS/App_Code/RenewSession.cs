﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;

//namespace mFicientWS
//{
//    public class RenewSession
//    {
//        RenewSessionReq _renewSessionReq;
//        public RenewSession(RenewSessionReq renewSessionReq)
//        {
//            _renewSessionReq = renewSessionReq;
//        }

//        public RenewSessionResp Process()
//        {
//            int iStatusCode = 0;
//            ResponseStatus objResponseStatus = new ResponseStatus();
//            string strNewSessionId = "";
//            try
//            {
//                //Renew Session
//                //int iExecuteReturnValue = 0;
//                strNewSessionId = getSessionId(_renewSessionReq.UserId);
//                int iExecuteReturnValue = renewSession(strNewSessionId);
//                //if update fails then we are returning Database connection error or record insert error
//                if ((iExecuteReturnValue != (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR && iExecuteReturnValue != (int)DATABASE_ERRORS.RECORD_INSERT_ERROR) && iExecuteReturnValue > 0)
//                {
//                    iStatusCode = 0;
//                    //make the response status
//                    objResponseStatus.cd = iStatusCode.ToString();
//                    objResponseStatus.desc = "";
//                }
//                else
//                {
//                    iStatusCode = iExecuteReturnValue;
//                    objResponseStatus.cd = iStatusCode.ToString();
//                    objResponseStatus.desc = "ERROR";
//                    //Removed for testing purpose only
//                    //Database table not yet created
//                    //objResponseStatus.desc = Utilities.GetDescriptionOfStatusCode(iStatusCode);
//                }
//            }
//            catch
//            {
//            }
//            return new RenewSessionResp(objResponseStatus, _renewSessionReq.RequestId, strNewSessionId);
//        }

//        int renewSession(string sessionId)
//        {
//            try
//            {
//                string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_SESSION
//                                SET SESSION_ACTIVITY_DATETIME = @SessionActivityDateTime
//                                ,SESSION_ID = @SessionId
//                                WHERE USER_ID = @UserId
//                                AND  COMPANY_ID=@COMPANY_ID";
//                SqlCommand cmd = new SqlCommand(strQuery);
//                cmd.CommandType = CommandType.Text;
//                cmd.Parameters.AddWithValue("@SessionActivityDateTime", DateTime.UtcNow.Ticks);
//                cmd.Parameters.AddWithValue("@SessionId", sessionId);
//                cmd.Parameters.AddWithValue("@UserId", _renewSessionReq.UserId);
//                cmd.Parameters.AddWithValue("@COMPANY_ID", _renewSessionReq.CompanyId);
//                return MSSqlClient.ExecuteNonQueryRecord(cmd);
//            }
//            catch (Exception ex)
//            {
//                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    return (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//                }
//                return (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
//            }
//        }

//        private string getSessionId(string userId)
//        {

//            //string strSessionIdPreFix = "1";
//            string strHashCode = Utilities.GetMd5Hash(_renewSessionReq.Email + userId + DateTime.UtcNow.Ticks.ToString());

//            //if (_renewSessionReq.DeviceToken != string.Empty)
//            //{
//            //    strSessionIdPreFix = "2";
//            //}
//            int index = 0;
//            Boolean blnIsSessionIdValid = false;
//            string strNewSessioId = string.Empty;
//            do
//            {
//                strNewSessioId = strHashCode.Substring(index, 15);
//                blnIsSessionIdValid = isSessionIdValid(strNewSessioId);
//                index = index + 1;
//            }
//            while (blnIsSessionIdValid == false);

//            return strNewSessioId;
//        }

//        private Boolean isSessionIdValid(string sessionId)
//        {
//            Boolean blnIsSessionIdValid = true;
//            string strQuery = @"SELECT USER_ID FROM TBL_MOBILE_USER_LOGIN_SESSION WHERE SESSION_ID=@SESSION_ID AND  COMPANY_ID=@COMPANY_ID;";
//            SqlCommand objSqlCommand = new SqlCommand(strQuery);
//            objSqlCommand.CommandType = CommandType.Text;
//            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
//            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _renewSessionReq.CompanyId);
//            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//            if (objDataSet.Tables[0].Rows.Count > 0)
//                blnIsSessionIdValid = false;
//            return blnIsSessionIdValid;
//        }
//    }
//}