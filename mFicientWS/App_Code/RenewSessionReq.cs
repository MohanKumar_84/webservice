﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class RenewSessionReq
    {
        string _email, _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _userName;

        public string UserName
        {
            get { return _userName; }
        }
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string Email
        {
            get { return _email; }
        }

        public RenewSessionReq(string requestJson, string userId)
        {
            RequestJsonParsing<RenewSessionReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<RenewSessionReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.RENEW_SESSION)
            {
                throw new Exception("Invalid Function Code");
            }
            _email = objRequestJsonParsing.req.data.em;
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userName = objRequestJsonParsing.req.data.unm;
        }

    }
    [DataContract]
    public class RenewSessionReqData : Data
    {

    }
}