﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class RenewSessionResp
    {
        ResponseStatus _respStatus;
        string _requestId, _sessionId;

        
        public RenewSessionResp(ResponseStatus respStatus, string requestId, string sessionId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _sessionId = sessionId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Renew Session Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            RenewSessionResponse objRenewSessionResp = new RenewSessionResponse();
            objRenewSessionResp.func = Convert.ToString((int)FUNCTION_CODES.RENEW_SESSION);
            objRenewSessionResp.rid = _requestId;
            objRenewSessionResp.status = _respStatus;
            RenewSessionResponseData objRenewSessionRespData = new RenewSessionResponseData();
            objRenewSessionRespData.sid = _sessionId;
            objRenewSessionResp.data = objRenewSessionRespData;
            string strJsonWithDetails = Utilities.SerializeJson<RenewSessionResponse>(objRenewSessionResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class RenewSessionResponse : CommonResponse
    {
        public RenewSessionResponse()
        { }
        /// <summary>
        /// Data
        /// </summary>
        [DataMember]
        public RenewSessionResponseData data { get; set; }
    }

    public class RenewSessionResponseData
    {
        /// <summary>
        /// Session Id
        /// </summary>
        [DataMember]
        public string sid { get; set; }
    }
}