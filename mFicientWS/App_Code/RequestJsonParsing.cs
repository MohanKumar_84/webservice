﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    #region Json Read Section

    /// <summary>
    /// For parsing the send request
    /// We have to pass the respective data class as parameter
    /// common for all the send request
    /// </summary>
    /// <typeparam name="T"></typeparam>

    [DataContract]
    public class RequestJsonParsing<T>
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public RequestFields<T> req { get; set; }
    }

    [DataContract]
    public class RequestFields<T>
    {

        /// <summary>
        /// function code from request
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// session id
        /// </summary>
        [DataMember]
        public string sid { get; set; }

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// Device id
        /// </summary>
        [DataMember]
        public string did { get; set; }

        /// <summary>
        /// Device Type
        /// </summary>
        [DataMember]
        public string dtyp { get; set; }

        /// <summary>
        /// Version
        /// </summary>
        [DataMember]
        public string ver { get; set; }

        /// <summary>
        /// data from request
        /// </summary>
        [DataMember]
        public T data { get; set; }
    }

    [DataContract]
    public class Data
    {
        /// <summary>
        /// Email
        /// </summary>
        [DataMember]
        public string em { get; set; }

        /// <summary>
        /// Company Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string unm { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        //[DataMember]
        //public string pwd { get; set; }

        /// <summary>
        /// Server ID
        /// </summary>
        [DataMember]
        public string srvid { get; set; }
    }

    [DataContract]
    public class RequestJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public RequestFields req { get; set; }
    }

    [DataContract]
    public class RequestFields
    {

        /// <summary>
        /// function code from request
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// session id
        /// </summary>
        [DataMember]
        public string sid { get; set; }

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

    }

    #endregion

    #region Json Response Common Classes

    [DataContract]
    public class CommonResponse
    {
        /// <summary>
        /// function code
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// Request Id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [DataMember]
        public ResponseStatus status { get; set; }
    }

    [DataContract]
    public class ResponseStatus
    {
        public ResponseStatus()
        {
            this.cd = "0";
            this.desc = string.Empty;
        }
        //status code
        [DataMember]
        public string cd { get; set; }

        //status description
        [DataMember]
        public string desc { get; set; }
    }

    public class MPlugInCommonResponse
    {
        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// function code
        /// </summary>
        [DataMember]
        public string func { get; set; }
    }
    #endregion
}