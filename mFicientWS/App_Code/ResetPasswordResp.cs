﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class ResetPasswordResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        public ResetPasswordResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Reset Password Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            ResetPasswordResponse objResetPasswordResp = new ResetPasswordResponse();
            objResetPasswordResp.func = Convert.ToString((int)FUNCTION_CODES.RESET_PASSWORD);
            objResetPasswordResp.rid = _requestId;
            objResetPasswordResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<ResetPasswordResponse>(objResetPasswordResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class ResetPasswordResponse : CommonResponse
    {
        public ResetPasswordResponse()
        { }
    }
}