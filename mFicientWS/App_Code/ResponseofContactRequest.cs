﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class ResponseofContactRequest
    {
        ResponseofContactRequestReq responseofContactRequestReq;
        System.Web.Caching.Cache appCache;
        public ResponseofContactRequest(ResponseofContactRequestReq _responseofContactRequestReq, System.Web.Caching.Cache _appCache)
        {
            responseofContactRequestReq = _responseofContactRequestReq;
            appCache = _appCache;
        }

        public ResponseofContactRequestResp Process()
        {
            string strContactId, strUserId;
            SqlTransaction transaction = null;
            strContactId = Utilities.GetUserID(responseofContactRequestReq.CompanyId, responseofContactRequestReq.ContactName);
            strUserId = Utilities.GetUserID(responseofContactRequestReq.CompanyId, responseofContactRequestReq.UserName);
            ResponseStatus objRespStatus = new ResponseStatus();
            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con);
            if (strContactId.Length > 0 && strUserId.Length > 0)
            {
                try
                {
                    transaction = con.BeginTransaction();
                    if (responseofContactRequestReq.ResponseStatus == "A")
                    {
                        int i = AddContact(con, transaction, strUserId, strContactId);
                        int j = AddContact(con, transaction, strContactId, strUserId);
                    }
                    string strDeleteContactRequest = @"DELETE FROM TBL_ADD_CONTACT_REQUEST 
                                                WHERE FROM_USER_ID = @FROM_USER_ID and TO_USER_ID= @TO_USER_ID AND COMPANY_ID= @COMPANY_ID";

                    SqlCommand cmd = new SqlCommand(strDeleteContactRequest, con, transaction);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@TO_USER_ID", strUserId);
                    cmd.Parameters.AddWithValue("@COMPANY_ID", responseofContactRequestReq.CompanyId);
                    cmd.Parameters.AddWithValue("@FROM_USER_ID", strContactId);
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        transaction.Commit();
                        objRespStatus.cd = "0";
                        objRespStatus.desc = "";
                        return new ResponseofContactRequestResp(objRespStatus, responseofContactRequestReq.RequestId);
                    }
                    else
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (transaction != null)
                        transaction.Rollback();
                    if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        MSSqlDatabaseClient.SqlConnectionClose(con);
                    }
                }
            }
            objRespStatus.cd = "2000501";
            objRespStatus.desc = "Request Not Found";
            return new ResponseofContactRequestResp(objRespStatus, responseofContactRequestReq.RequestId);
        }

        private int AddContact(SqlConnection sqlConnection, SqlTransaction sqlTransaction, string userID, string contactID)
        {

            string strAddContact = @"if exists (SELECT 'true' FROM TBL_USER_CONTACT_FOR_MBUZZ WHERE COMPANY_ID = @COMPANY_ID AND USER_ID = @USER_ID)
	                                    if exists(select 'true' FROM TBL_USER_CONTACT_FOR_MBUZZ WHERE CONTACTS ='' and COMPANY_ID = @COMPANY_ID AND USER_ID = @USER_ID)
		                                    UPDATE dbo.TBL_USER_CONTACT_FOR_MBUZZ SET CONTACTS = @CONTACTS  WHERE USER_ID= @USER_ID AND COMPANY_ID= @COMPANY_ID
	                                    else if not exists(select 'true' FROM TBL_USER_CONTACT_FOR_MBUZZ WHERE CONTACTS like '%" + contactID + @"%' and COMPANY_ID = @COMPANY_ID AND USER_ID = @USER_ID)
                                            UPDATE dbo.TBL_USER_CONTACT_FOR_MBUZZ SET CONTACTS = CONTACTS +','+ @CONTACTS WHERE USER_ID= @USER_ID AND COMPANY_ID= @COMPANY_ID
                                        else return
                                     else INSERT INTO TBL_USER_CONTACT_FOR_MBUZZ (  USER_ID, COMPANY_ID, CONTACTS) VALUES(  @USER_ID, @COMPANY_ID, @CONTACTS);";

            SqlCommand cmd = new SqlCommand(strAddContact, sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", userID);
            cmd.Parameters.AddWithValue("@COMPANY_ID", responseofContactRequestReq.CompanyId);
            cmd.Parameters.AddWithValue("@CONTACTS", contactID);
            return cmd.ExecuteNonQuery();

        }
    }
}