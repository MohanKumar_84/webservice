﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;
using System.Xml;

namespace mFicientWS
{
    public class UserAuthentication
    {
       UserAuthenticationReq userAuthenticationReq;
       string strUserID = "";
        public UserAuthentication(UserAuthenticationReq _userAuthenticationReq)
        {
            userAuthenticationReq = _userAuthenticationReq;
        }

        public UserAuthenticationResp Process()
        {
            string strUserSessionID = string.Empty;
            ResponseStatus objRespStatus = new ResponseStatus();
            List<UserData> lstUsersDataDtls = new List<UserData>();
            
            try
            {
                if (UserAuthenticationFormBuzz())
                {
                    if (userAuthenticationReq.DeviceType != "mBuzz")
                    {
                        SessionUser session = CacheManager.Get<SessionUser>(
                            CacheManager.GetKey(
                            CacheManager.CacheType.mFicientSession, userAuthenticationReq.CompanyId,
                            userAuthenticationReq.UserName, userAuthenticationReq.DeviceId,
                            userAuthenticationReq.DeviceType,
                            String.Empty, String.Empty,
                            String.Empty
                            ),false
                            );

                        if (session != null)
                        {
                            strUserSessionID = session.SessionId;
                            GetUserContactList userContacts = new GetUserContactList(strUserID, userAuthenticationReq.CompanyId);
                            lstUsersDataDtls = userContacts.GetContacts();
                        }
                        else
                        {
                            objRespStatus.cd = "2000101";
                            objRespStatus.desc = "Unauthorized User";
                            return new UserAuthenticationResp(objRespStatus, userAuthenticationReq.RequestId, "", null);
                        }
                    }
                    else if (userAuthenticationReq.DeviceType == "mBuzz")
                    {
                        GetUserContactList userContacts = new GetUserContactList(strUserID, userAuthenticationReq.CompanyId);
                        lstUsersDataDtls = userContacts.GetContacts();
                    }
                }
                else
                {
                    objRespStatus.cd = "2000101";
                    objRespStatus.desc = "Unauthorized User";
                    return new UserAuthenticationResp(objRespStatus, userAuthenticationReq.RequestId, "", null);
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                throw ex;
            }

            return new UserAuthenticationResp(objRespStatus, userAuthenticationReq.RequestId, strUserSessionID, lstUsersDataDtls);
        }
        private bool UserAuthenticationFormBuzz()
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT USER_ID FROM TBL_USER_DETAIL WHERE COMPANY_ID = @COMPANY_ID AND USER_NAME = @USER_NAME and is_blocked=0 and allow_messenger=1");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", userAuthenticationReq.CompanyId);
                cmd.Parameters.AddWithValue("@USER_NAME", userAuthenticationReq.UserName);

                DataTable dt = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    strUserID = dt.Rows[0]["USER_ID"].ToString();
                    return true;
                }
            }
            catch { }
            return false;
        }
    }
}