﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class UserDetailResp
    {
        ResponseStatus _respStatus;
        UserDetailResponseData _objUserDtlsRespData;
        string _requestId;
        public UserDetailResp(ResponseStatus respStatus,UserDetailResponseData objUserDtlsRespData, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _objUserDtlsRespData = objUserDtlsRespData;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating User Detail Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            UserDtlsResponse objUserDtlsResp = new UserDtlsResponse();
            objUserDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.USER_DETAIL);
            objUserDtlsResp.rid = _requestId;
            objUserDtlsResp.status = _respStatus;
            
            objUserDtlsResp.data = _objUserDtlsRespData;
            string strJsonWithDetails = Utilities.SerializeJson<UserDtlsResponse>(objUserDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class UserDtlsResponse : CommonResponse
    {
        public UserDtlsResponse()
        { }
        [DataMember]
        public UserDetailResponseData data { get; set; }
    }

    public class UserDetailResponseData
    {
        /// <summary>
        /// First Name 
        /// </summary>
        [DataMember]
        public string ufnm { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        [DataMember]
        public string ulnm { get; set; }

        /// <summary>
        /// Mobile
        /// </summary>
        [DataMember]
        public string mob { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DataMember]
        public string em { get; set; }
        
        /// <summary>
        /// Date of Birth
        /// </summary>
        [DataMember]
        public string dob { get; set; }

        /// <summary>
        /// Designation Id
        /// </summary>
        [DataMember]
        public string dsgc { get; set; }

        /// <summary>
        /// Designation Name
        /// </summary>
        [DataMember]
        public string des { get; set; }

        /// <summary>
        /// Location Id
        /// </summary>
        [DataMember]
        public string locc { get; set; }

        /// <summary>
        /// Location Name
        /// </summary>
        [DataMember]
        public string loc { get; set; }

        /// <summary>
        /// Region Id
        /// </summary>
        [DataMember]
        public string regc { get; set; }

        /// <summary>
        /// Region Name
        /// </summary>
        [DataMember]
        public string reg { get; set; }

        /// <summary>
        /// Allow Messenger
        /// </summary>
        [DataMember]
        public string msgr { get; set; }

        /// <summary>
        /// Allow OFFLINE WORK
        /// </summary>
        [DataMember]
        public string olw { get; set; }

        /// <summary>
        /// Division Name
        /// </summary>
        [DataMember]
        public string div { get; set; }

        /// <summary>
        /// Division Id
        /// </summary>
        [DataMember]
        public string divc { get; set; }

        /// <summary>
        /// Department Name
        /// </summary>
        [DataMember]
        public string dep { get; set; }

        /// <summary>
        /// Department Id
        /// </summary>
        [DataMember]
        public string depc { get; set; }

        /// <summary>
        /// Employee Id
        /// </summary>
        [DataMember]
        public string empid { get; set; }

        /// <summary>
        /// Employee Id
        /// </summary>
        [DataMember]
        public string admin { get; set; }

        //custome Property

        public List<CustomProperty> cust { get; set; }
    }
    //public class CustomProperty
    //{
    //    /// <summary>
    //    ///  Name 
    //    /// </summary>
    //    [DataMember]
    //    public string name { get; set; }
    //    /// <summary>
    //    ///  Name 
    //    /// </summary>
    //    [DataMember]
    //    public string value { get; set; }

    //}
}