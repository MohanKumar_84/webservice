﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using Newtonsoft.Json.Linq;
namespace mFicientWS
{
    public class UserLogin
    {
        enum LoginErrorCode
        {
            LOGIN_FAIL = 1300101,
            COMPANY_ACCOUNT_BLOCK = 1300102,
            DEVICE_NOT_REGISTER = 1300103,
            DEVICE_NOT_REGISTER_BUT_OTHER = 1300104,
            DEVICE_REGISTERATION_PENDING = 1300105,
            DEVICE_REGISTERATION_DENIED = 1300106,
            CONPANY_ACCOUNT_EXPIRED = 1300107,
            LOGIN_FAIL_AND_DEVICE_NOT_REGISTER = 1300108,
            ACTIVE_DIRECTORY_ERROR = 1300109,
            APP_VERSION_NOT_ALLOW = 1300110
        }
        UserLoginReq _userLoginReq;
        
        JArray _cre;
        JObject s3Bucket;
        JObject amazonS3;
        string _traceDevice = "";
        public UserLogin(UserLoginReq userLoginReq)
        {
            _userLoginReq = userLoginReq;
            s3Bucket = new JObject();
            _cre = new JArray();
        }

        public UserLoginResp Process(HttpContext _Context)
        {
            amazonS3 = new JObject();
            ResponseStatus objRespStatus = new ResponseStatus();
            
            /***
             * Check if company is blocked
            ***/
            MFECompanyInfo objCmpInfo = getCompanyDetails();
            if (objCmpInfo != null)
            {
                if (isCompanyBlocked(objCmpInfo))
                {
                    return ReturnError(objRespStatus, (int)LoginErrorCode.COMPANY_ACCOUNT_BLOCK, "Account Blocked.");
                }
                /*
                 * CHECK ALLOTED SERVER FOR COMPANY
                 * */
                //if (!_Context.Request.Url.AbsoluteUri.ToLower().StartsWith(objCmpInfo.ServerURL.ToLower()))
                //{
                //    throw new Exception(((int)HttpStatusCode.MovedPermanently).ToString());
                //}
            }
            else
            {
                return ReturnError(objRespStatus, (int)LoginErrorCode.LOGIN_FAIL, "Login failed.");
            }
            /*
             * CHECK COMPANY CURRENT PLAN
             * */
            DataSet dsCompanyCurrentPlanDtl = getCompanyCurrentPlan();
            if (dsCompanyCurrentPlanDtl != null && dsCompanyCurrentPlanDtl.Tables.Count > 0)
            {
                if (!isCurrentPlanValid(dsCompanyCurrentPlanDtl.Tables[0]))
                {
                    return ReturnError(objRespStatus, (int)LoginErrorCode.CONPANY_ACCOUNT_EXPIRED, "Login failed.Your current Plan has expired.Please contact the company administrator.");
                }
            }
            else
            {
                return ReturnError(objRespStatus, (int)LoginErrorCode.LOGIN_FAIL, "Login failed.");
            }

            /**
             * GET THE USER DETAIL BY USER NAME
             * */

            DataSet dsUserLoginDtls = getLoginDtlsOfUserByUserName();
            if (dsUserLoginDtls == null) throw new Exception();

            //the user does not exist
            if (dsUserLoginDtls.Tables.Count == 0 || dsUserLoginDtls.Tables[0].Rows.Count == 0)
            {
                return ReturnError(objRespStatus, (int)LoginErrorCode.LOGIN_FAIL, "Login failed.");
            }
            DataTable dtblUserLoginDtl = dsUserLoginDtls.Tables[0];
            DataTable dtblAdditionalDetail = dsUserLoginDtls.Tables[1];
            DataTable dtblGroups = dsUserLoginDtls.Tables[3];
            string strMenuUpdateRequired = "0", strCompanyUpdateReq = "0", strUserDetailUpdateReq = "0" ;           
            string _userId = Convert.ToString(dtblUserLoginDtl.Rows[0]["USER_ID"]);
            string _emailId = Convert.ToString(dtblUserLoginDtl.Rows[0]["EMAIL_ID"]);
            string fullName = Convert.ToString(dtblUserLoginDtl.Rows[0]["First_name"]) + " " + Convert.ToString(dtblUserLoginDtl.Rows[0]["Last_name"]);
            string subAdminID = Convert.ToString(dtblUserLoginDtl.Rows[0]["SUBADMIN_ID"]);
            string DeviceModel = Convert.ToString(dtblUserLoginDtl.Rows[0]["DEVICE_MODEL"]);
            string strCredentialJSON = Convert.ToString(dtblUserLoginDtl.Rows[0]["CREDENTIAL_DETAIL"]);
            string domainID = Convert.ToString(dtblUserLoginDtl.Rows[0]["DOMAIN_ID"]);
            string accessCode=Convert.ToString(dtblUserLoginDtl.Rows[0]["ACCESS_CODE"]).ToLower();
            string domainName=Convert.ToString(dtblUserLoginDtl.Rows[0]["DOMAIN_NAME"]).ToLower();
            bool isUserBolck=Convert.ToBoolean(dtblUserLoginDtl.Rows[0]["IS_BLOCKED"]);
            string MficientKey = isUserUsingAuthorisedDevice(dtblUserLoginDtl);
            /***
             * If password does not match see if the device is registered as well
             * if not then send login failed and device not registered
             * the following is done for this purpose
             * */
            DataSet dsUserWithDeviceIdAndType = getUserWithRequestedDeviceTypeAndID(_userId);
            if (dsUserWithDeviceIdAndType == null)
            {
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
            if (!CheckLatestAppVersion(dsUserLoginDtls.Tables[2]))
            {
                return ReturnError(objRespStatus, (int)LoginErrorCode.APP_VERSION_NOT_ALLOW, "App version not allow.");
            }
            /***
             * GET  USER TYPE
             * ***/
            MFICIENT_USER_TYPE eUserType = this.getUserType(domainID);
            /*login*/
            bool blnIsAutherisedUser = false;
            switch (eUserType)
            {
                case MFICIENT_USER_TYPE.CloudUser:
                    if (_userLoginReq.Password.ToLower() != accessCode)
                        blnIsAutherisedUser = false;
                    else
                        blnIsAutherisedUser = true;
                    break;
                case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                    try
                    {
                        if (_userLoginReq.DomainName.ToLower() == domainName)
                            blnIsAutherisedUser = doProcessForActiveDirectoryUser( out _emailId);
                    }
                    catch (mPlugin.mPluginException ex)
                    {
                        if (ex.InnerException == null)
                        {
                            blnIsAutherisedUser = false;
                        }
                        else
                        {
                            return ReturnError(objRespStatus, (int)LoginErrorCode.ACTIVE_DIRECTORY_ERROR, ex.Message);
                        }
                    } break;
            }
            //Now this will run only for cloud user
            if (!blnIsAutherisedUser)
            {
                if (dsUserWithDeviceIdAndType.Tables[0].Rows.Count == 0)
                {
                    return ReturnError(objRespStatus, (int)LoginErrorCode.LOGIN_FAIL_AND_DEVICE_NOT_REGISTER, "Login failed.Device not registered");
                }
                return ReturnError(objRespStatus, (int)LoginErrorCode.LOGIN_FAIL, "Login failed.");
            }
            //Account blocked check
            if (isUserBolck)
            {
                return ReturnError(objRespStatus, (int)LoginErrorCode.COMPANY_ACCOUNT_BLOCK, "Account Blocked.");
            }
            else
            {
                if (!String.IsNullOrEmpty(MficientKey))//If loging successfull get mficientkey of device
                {
                    try
                    {
                        SqlTransaction transaction = null;
                        SqlConnection con;
                        MSSqlDatabaseClient.SqlConnectionOpen(out con);
                        try
                        {
                            using (con)
                            {
                                using (transaction = con.BeginTransaction())
                                {
                                    updateResetLog(con, transaction, _userId);
                                    updateLoginHistory(con, transaction, _userId);
                                    transaction.Commit();
                                    objRespStatus.cd = "0";
                                    objRespStatus.desc = "";
                                }
                            }

                            bool isUsrDtlUpdated, isCompLogoUpdated, isMenuCatUpdated;
                            if (isMobileUpdateRequiredForUser(_userId, out isUsrDtlUpdated, out isCompLogoUpdated, out isMenuCatUpdated))
                            {
                                strCompanyUpdateReq = isCompLogoUpdated ? "1" : "0";
                                strUserDetailUpdateReq = isUsrDtlUpdated ? "1" : "0";
                                strMenuUpdateRequired = isMenuCatUpdated ? "1" : "0";
                            }

                            getCredentialList(strCredentialJSON, dtblAdditionalDetail);
                            getS3BucketDetails();
                            getAmazonCredential(dtblAdditionalDetail);
                            getDeviceTrackingSetting(dtblGroups);
                            if (!string.IsNullOrEmpty(_userLoginReq.OSVersion) && !string.IsNullOrEmpty(_userLoginReq.AppVersion))
                                UpdateUserDeviceOsAndAppVersion(_userId);
                        }
                        catch
                        {
                            throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                        }
                        finally
                        {
                            if (con != null)
                            {
                                con.Dispose();
                            }
                            if (transaction != null)
                            {
                                transaction.Dispose();
                            }
                        }
                    }
                    catch
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                }
                else
                {
                    //Is not using the autherised device
                    if (dsUserWithDeviceIdAndType.Tables.Count > 0)
                    {
                        //check device registration requeted
                        if (dsUserWithDeviceIdAndType.Tables[1].Rows.Count > 0)
                        {
                            string strFilter = String.Format("DEVICE_TYPE='{0}'AND DEVICE_ID ='{1}'", _userLoginReq.DeviceType, _userLoginReq.DeviceId);
                            DataRow[] rows = dsUserWithDeviceIdAndType.Tables[1].Select(strFilter);
                            if (rows.Length > 0)
                            {
                                if (Convert.ToString(rows[0]["STATUS"]).ToLower() == "0".ToLower())
                                {
                                    return ReturnError(objRespStatus, (int)LoginErrorCode.DEVICE_REGISTERATION_PENDING, "Registration request status is pending.");
                                }
                            }
                        }
                        //check device registration denied
                        if (dsUserWithDeviceIdAndType.Tables[3].Rows.Count > 0)
                        {
                            return ReturnError(objRespStatus, (int)LoginErrorCode.DEVICE_REGISTERATION_DENIED, "Registration request status was denied.");
                        }
                        //check device registration to other user
                        if (dsUserWithDeviceIdAndType.Tables[2].Rows.Count > 0)
                        {
                            return ReturnError(objRespStatus, (int)LoginErrorCode.DEVICE_NOT_REGISTER_BUT_OTHER, "This device is not registered but other device is registered.");
                        }
                        else
                        {
                            return ReturnError(objRespStatus, (int)LoginErrorCode.DEVICE_NOT_REGISTER, "Device is not registered.");
                        }
                    }
                    else
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                }
            }
            return new UserLoginResp(objRespStatus, _userLoginReq.RequestId, strMenuUpdateRequired, strCompanyUpdateReq, strUserDetailUpdateReq, MficientKey, _userLoginReq.DeviceId, _userLoginReq.DeviceType, _userLoginReq.CompanyId, _userLoginReq.UserName, _userId, _emailId, _cre, s3Bucket, amazonS3, subAdminID, objCmpInfo.AutoLogin, DeviceModel, fullName, _traceDevice);
        }
        private void getCredentialList(string strcredential, DataTable dtblUserLoginDtls)
        {
            JArray crdentials = new JArray();
            if (strcredential != "")
            {
                crdentials = JArray.Parse(strcredential);
            }

            if (dtblUserLoginDtls.Rows.Count > 0)
            {
                DataRow row = dtblUserLoginDtls.Rows[0];
                // string AuthTag;
                string strauthicationmeta = Convert.ToString(row["AUTHENTICATION_META"]);
                if (strauthicationmeta != "")
                {
                    JArray objAuthcation = JArray.Parse(strauthicationmeta);
                    foreach (var authItem in objAuthcation)
                    {
                        JObject objcre = new JObject();
                        objcre.Add("name", authItem["name"]);
                        objcre.Add("id",  authItem["tag"]);
                        objcre.Add("pwd",  authItem["pwd"]);
                        objcre.Add("unm",  authItem["uid"]);
                        objcre.Add("life",  authItem["lifeval"]);
                        foreach (JObject crItem in crdentials)
                        {
                            if (crItem["id"] == authItem["tag"])
                            {
                                objcre.Add("pwd", crItem["pwd"]);
                                objcre.Add("unm", crItem["uid"]);
                                break;
                            }
                        }
                        _cre.Add(objcre);
                    }
                }

            }
        }
        private bool getDeviceTrackingSetting(DataTable dtgroups)
        {
            if (_traceDevice.Length > 0)
            {
                Newtonsoft.Json.Linq.JObject jo = Newtonsoft.Json.Linq.JObject.Parse(_traceDevice);
                foreach (DataRow dr in dtgroups.Rows)
                    if (jo["groups"].Contains(dr[0].ToString()))
                    {
                        jo.Property("groups").Remove();
                        _traceDevice = jo.ToString();
                        return true;
                    }
            }
            _traceDevice = "";
            return false;
        }
        private void getAmazonCredential(DataTable dtblUserLoginDtls)
        {
            amazonS3 = new JObject();
            if (dtblUserLoginDtls.Rows.Count > 0)
            {
                string strauthicationmeta = Convert.ToString(dtblUserLoginDtls.Rows[0]["AMAZON_ATHENTICATION"]);
                if (strauthicationmeta != "")
                {
                    amazonS3 = JObject.Parse(strauthicationmeta);
                
                    if (amazonS3["acr"] != null)
                        foreach (JObject acr in (JArray)amazonS3["acr"])
                        {
                            acr.Add("ak",EncryptionDecryption.AESEncrypt(_userLoginReq.CompanyId.ToLower(), (string)acr["ak"]));
                            acr.Add("sak", EncryptionDecryption.AESEncrypt(_userLoginReq.CompanyId.ToLower(), (string)acr["sak"]));
                        }
                    else amazonS3["acr"] = new JArray();
                    amazonS3["acp"] = (amazonS3["acp"] != null) ? amazonS3["acp"] : new JArray();
                }
            }
            _traceDevice =Convert.ToString(dtblUserLoginDtls.Rows[0]["TRACE_LOCATION"]);
        }

        private void UpdateUserDeviceOsAndAppVersion(string UserID)
        {
            //SqlConnection con = new SqlConnection();
            try
            {
                string query = @"UPDATE TBL_REGISTERED_DEVICE set OS_VERSION=@OS_VERSION,APP_VERSION=@APP_VERSION 
                where COMPANY_ID=@COMPANY_ID and USER_ID=@USER_ID and DEVICE_TYPE=@DEVICE_TYPE and DEVICE_ID=@DEVICE_ID;";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@COMPANY_ID", _userLoginReq.CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", UserID);
                cmd.Parameters.AddWithValue("@DEVICE_TYPE", _userLoginReq.DeviceType);
                cmd.Parameters.AddWithValue("@DEVICE_ID", _userLoginReq.DeviceId);
                cmd.Parameters.AddWithValue("@OS_VERSION", _userLoginReq.OSVersion);
                cmd.Parameters.AddWithValue("@APP_VERSION", _userLoginReq.AppVersion);

                MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd);
            }
            catch { }
            finally
            {
                // MSSqlDatabaseClient.SqlConnectionClose(con);
            }
        }

        private void getS3BucketDetails()
        {
            try
            {
                BucketType bucketType = BucketType.MFICIENT_ANDROID;
                if (_userLoginReq.DeviceType.ToUpper() == "ANDROID")
                    bucketType = BucketType.MFICIENT_ANDROID;
                else if (_userLoginReq.DeviceType.ToUpper() == "IOS")
                    bucketType = BucketType.IOS_DEVICE;

                DataSet dsBucketInfo = Utilities.GetS3BucketDetails(bucketType);
                if (dsBucketInfo != null && dsBucketInfo.Tables.Count > 0)
                {
                    if (dsBucketInfo.Tables[0] != null && dsBucketInfo.Tables[0].Rows.Count > 0)
                    {
                        s3Bucket.Add("aid",dsBucketInfo.Tables[0].Rows[0]["USER_NAME"].ToString());
                        s3Bucket.Add("ak", dsBucketInfo.Tables[0].Rows[0]["ACCESS_KEY"].ToString());
                        s3Bucket.Add("sak", dsBucketInfo.Tables[0].Rows[0]["SECRET_ACCESS_KEY"].ToString());
                    }
                }
            }
            catch 
            {
            }
        }

        #region Do Process For Active Directory User

        bool doProcessForActiveDirectoryUser(out string _emailId)
        {
            _emailId = "";
            GetMPluginAgentDtl objMPAgentDtl = new GetMPluginAgentDtl(_userLoginReq.CompanyId);
            objMPAgentDtl.Process();

            bool blnIsAutherisedUser = false;

            if (objMPAgentDtl.StatusCode == 0)
            {
                try
                {
                    blnIsAutherisedUser =
                        mPlugin.ActiveDirectoryUserLogin(_userLoginReq.CompanyId,
                        objMPAgentDtl.MpAgentName, objMPAgentDtl.MpAgentPassword, _userLoginReq.DomainName,
                        _userLoginReq.UserName,
                        _userLoginReq.Password, out _emailId);
                }
                catch (mPlugin.mPluginException ex)
                {
                    throw ex;
                }
            }
            /**
             * If user name and password is valid then do this.
             * **/
            return blnIsAutherisedUser;
        }

        #endregion

        MFECompanyInfo getCompanyDetails()
        {
            WSCCompanyDetails objCmpDetls = new WSCCompanyDetails(_userLoginReq.CompanyId);

            objCmpDetls.Process();
            if (objCmpDetls.StatusCode != 0) throw new Exception();
            return objCmpDetls.CompanyInfo;
        }
        bool isCompanyBlocked(MFECompanyInfo cmpInfo)
        {
            if (cmpInfo == null) throw new ArgumentNullException();
            if (cmpInfo.IsBlocked) return true;
            else return false;
        }
        DataSet getLoginDtlsOfUserByUserName()
        {
            string strQuery = @"SELECT  usr.*,dmn.DOMAIN_NAME,regdevice.DEVICE_TYPE,regdevice.DEVICE_ID,regdevice.REGISTRATION_DATE,
                                regdevice.SUBADMIN_ID,regdevice.MFICIENT_KEY,regdevice.DEVICE_MODEL,ISNULL(sadmin.[SUBADMIN_ID],'') as SUBADMIN_ID
                                FROM TBL_USER_DETAIL AS usr 
                                LEFT OUTER JOIN TBL_REGISTERED_DEVICE AS regdevice ON usr.USER_ID = regdevice.USER_ID
                                LEFT OUTER JOIN TBL_ENTERPRISE_DOMAINS As dmn ON usr.DOMAIN_ID = dmn.DOMAIN_ID
                                left outer join tbl_sub_admin as sadmin on sadmin.MOBILE_USER = usr.user_ID
                                WHERE usr.USER_NAME =@UserName AND usr.COMPANY_ID = @CompanyId AND usr.IS_ACTIVE = 1;
                                select * from dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION as meta  WHERE COMPANY_ID = @CompanyId ;
                                Select * from ADMIN_TBL_MST_MOBILE_DEVICE where PHONE_DEVICE_TYPE_CODE=@DeviceType;
                                SELECT [GROUP_ID] FROM [TBL_USER_GROUP_LINK] as l inner join tbl_user_detail as u on l.[user_id]=u.[user_id] 
                                where [user_name]=@UserName and l.company_id=@CompanyId";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _userLoginReq.CompanyId);
            cmd.Parameters.AddWithValue("@UserName", _userLoginReq.UserName);
            cmd.Parameters.AddWithValue("@DeviceType", _userLoginReq.DeviceType);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

        bool isLoginUsingNewPwdWithinStipulatedTime(DataTable userLoginDtlDTable)
        {
            long lngNewPwdRequestedTime = (long)userLoginDtlDTable.Rows[0]["ACCESSCODE_RESET_DATETIME"];
            DateTime dtNewPwdReqDateTime = new DateTime(lngNewPwdRequestedTime);
            TimeSpan tsTimeIntervalBtwnReqAndUsage = DateTime.UtcNow - dtNewPwdReqDateTime;
            if (tsTimeIntervalBtwnReqAndUsage.TotalHours > 48)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        string isUserUsingAuthorisedDevice(DataTable userLoginDtlDTable)
        {
            string filter = String.Format("DEVICE_ID ='{0}' AND DEVICE_TYPE ='{1}'", _userLoginReq.DeviceId.ToLower().Trim(), _userLoginReq.DeviceType.ToLower().Trim());
            DataRow[] rows = userLoginDtlDTable.Select(filter);
            if (rows.Length > 0)
            {
                return rows[0]["MFICIENT_KEY"].ToString();
            }
            else
            {
                return "";
            }
        }

        DataSet getUserWithRequestedDeviceTypeAndID(string  _user_id)
        {
            //string strDeviceType=i
            string strQuery = @"SELECT * FROM TBL_REGISTERED_DEVICE WHERE COMPANY_ID = @CompanyId AND DEVICE_TYPE = @DeviceType AND DEVICE_ID = @DeviceId;
                           
                            SELECT * FROM TBL_DEVICE_REGISTRATION_REQUEST WHERE COMPANY_ID = @CompanyId AND USER_ID = @UserId;

                            SELECT *   FROM TBL_REGISTERED_DEVICE WHERE COMPANY_ID = @CompanyId AND USER_ID = @UserId;

                            SELECT *  FROM TBL_DEVICE_REGISTRATION_REQ_DENIED WHERE COMPANY_ID = @CompanyId AND USER_ID =@UserId AND DEVICE_TYPE =@DeviceType AND DEVICE_ID =@DeviceId;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _userLoginReq.CompanyId);
            cmd.Parameters.AddWithValue("@UserId", _user_id);
            cmd.Parameters.AddWithValue("@DeviceType", _userLoginReq.DeviceType);
            cmd.Parameters.AddWithValue("@DeviceId", _userLoginReq.DeviceId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

        /// <summary>

        /// Description :Genrate session id 
        /// Created By: Mohan  2/5/2012
        /// </summary>
        private string GetSessionId(string _UserId)
        {

            string strSessionIdPreFix = "1";
            string strHashCode = Utilities.GetMd5Hash(_userLoginReq.UserName + _UserId + DateTime.UtcNow.Ticks.ToString());

            if (_userLoginReq.DeviceType != string.Empty)
            {
                strSessionIdPreFix = "2";
            }

            int index = 0;
            Boolean blnIsSessionIdValid = false;
            string strNewSessioId = string.Empty;
            do
            {
                strNewSessioId = strSessionIdPreFix + strHashCode.Substring(index, 15);
                blnIsSessionIdValid = IsSessionIdValid(strNewSessioId);
                index = index + 1;

            }
            while (blnIsSessionIdValid == false);

            return strNewSessioId;
        }

        private Boolean IsSessionIdValid(string _SessionId)
        {
            Boolean blnIsSessionIdValid = true;
            SqlCommand objSqlCommand = new SqlCommand(@"SELECT USER_ID FROM TBL_MOBILE_USER_LOGIN_SESSION WHERE SESSION_ID=@SESSION_ID AND  COMPANY_ID=@COMPANY_ID;");
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", _SessionId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _userLoginReq.CompanyId);
            DataSet objDataSet = MSSqlDatabaseClient.SelectDataFromSqlCommand(objSqlCommand);
            if (objDataSet.Tables[0].Rows.Count > 0)
                blnIsSessionIdValid = false;
            return blnIsSessionIdValid;
        }

        bool isUserAlreadyLoggedIn(DataTable userLoginDtlsDTable)
        {
            string strQuery = @"SELECT * 
                                FROM TBL_MOBILE_USER_LOGIN_SESSION
                                WHERE USER_ID =@UserId
                                AND LOGIN_DEVICE_TOKEN = @LoginDeviceToken
                                AND LOGIN_DEVICE_TYPE_CODE=@LoginDeviceType AND  COMPANY_ID=@COMPANY_ID;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            //cmd.Parameters.AddWithValue("@CompanyId", _userLoginReq.CompanyId);
            cmd.Parameters.AddWithValue("@UserId", userLoginDtlsDTable.Rows[0]["USER_ID"]);
            cmd.Parameters.AddWithValue("@LoginDeviceType", _userLoginReq.DeviceType);
            cmd.Parameters.AddWithValue("@LoginDeviceToken", _userLoginReq.DeviceId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _userLoginReq.CompanyId);
            DataSet dsUserLoginInfo = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsUserLoginInfo != null && dsUserLoginInfo.Tables.Count > 0)
            {
                if (dsUserLoginInfo.Tables[0] != null && dsUserLoginInfo.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        void updateResetLog(SqlConnection sqlConnection, SqlTransaction sqlTransaction, string _userID)
        {
            string strQuery = @"UPDATE TBL_RESET_PASSWORD_LOG
                                    SET IS_EXPIRED = 1
                                    WHERE USER_ID = @UserId
                                    AND COMPANY_ID=@COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _userID);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _userLoginReq.CompanyId);
            cmd.ExecuteNonQuery();
        }
        int updateLoginHistory(SqlConnection sqlConnection, SqlTransaction sqlTransaction, string _userID)
        {
            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
                                                SET LOGOUT_DATETIME = @LogOutDateTime,
                                                LOGOUT_TYPE = @LogOutType
                                                WHERE USER_ID = @UserId
                                                AND LOGOUT_DATETIME = 0
                                                AND LOGIN_DEVICE_TOKEN =@LoginDeviceToken
                                                AND LOGIN_DEVICE_TYPE_CODE =@LoginDeviceTypeCode
                                                AND  COMPANY_ID=@COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _userID);
            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@LoginDeviceToken", _userLoginReq.DeviceId);
            cmd.Parameters.AddWithValue("@LoginDeviceTypeCode", _userLoginReq.DeviceType);
            cmd.Parameters.AddWithValue("@LogOutType", "LOGGED IN AGAIN");
            cmd.Parameters.AddWithValue("@COMPANY_ID", _userLoginReq.CompanyId);
            return cmd.ExecuteNonQuery();
        }

        bool isMobileUpdateRequiredForUser(string _userID, out bool isUserDtlUpdated, out bool isCompLogoUpdated, out bool isMenuCatUpdated)
        {
            string strQuery = @"SELECT * FROM TBL_UPDATION_REQUIRED_DETAIL
                                WHERE COMPANY_ID = @CompanyId
                                AND USER_ID =@UserId
                                AND DEVICE_ID =@DeviceId
                                AND DEVICE_TYPE =@DeviceType;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _userLoginReq.CompanyId);
            cmd.Parameters.AddWithValue("@UserId", _userID);
            cmd.Parameters.AddWithValue("@DeviceId", _userLoginReq.DeviceId);
            cmd.Parameters.AddWithValue("@DeviceType", _userLoginReq.DeviceType);
            try
            {
                isUserDtlUpdated = false;
                isCompLogoUpdated = false;
                isMenuCatUpdated = false;
                DataSet dsUpdationReqInfo = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsUpdationReqInfo != null && dsUpdationReqInfo.Tables.Count > 0)
                {
                    if (dsUpdationReqInfo.Tables[0] != null && dsUpdationReqInfo.Tables[0].Rows.Count > 0)
                    {
                        string filter = String.Format("UPDATION_TYPE = '{0}'", "2");
                        DataRow[] rows = dsUpdationReqInfo.Tables[0].Select(filter);
                        if (rows.Length > 0)
                        {
                            isUserDtlUpdated = true;
                        }
                        filter = String.Format("UPDATION_TYPE = '{0}'", "1");
                        rows = dsUpdationReqInfo.Tables[0].Select(filter);
                        if (rows.Length > 0)
                        {
                            isCompLogoUpdated = true;
                        }
                        filter = String.Format("UPDATION_TYPE = '{0}'", "3");
                        rows = dsUpdationReqInfo.Tables[0].Select(filter);
                        if (rows.Length > 0)
                        {
                            isMenuCatUpdated = true;
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                else
                    throw ex;
            }
        }
        DataSet getCompanyCurrentPlan()
        {
            string strQuery = @"SELECT * FROM TBL_COMPANY_CURRENT_PLAN
                                WHERE COMPANY_ID = @CompanyId;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _userLoginReq.CompanyId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
        UserLoginResp ReturnError(ResponseStatus objRespStatus, int errorCode, string ErrorMessage)
        {
            objRespStatus.cd = errorCode.ToString();
            objRespStatus.desc = ErrorMessage;
            return new UserLoginResp(objRespStatus, _userLoginReq.RequestId);
        }

        bool isCurrentPlanValid(DataTable currPlanDetails)
        {
            if (currPlanDetails != null && currPlanDetails.Rows.Count > 0)
            {
                long lngPlanPurchaseDate = Convert.ToInt64(currPlanDetails.Rows[0]["PURCHASE_DATE"]); ;
                DateTime dtPurchaseDate = new DateTime(lngPlanPurchaseDate).ToUniversalTime();
                //long lngValidTill = dtPurchaseDate.AddMonths(Convert.ToInt32(Math.Floor(Convert.ToDouble(currPlanDetails.Rows[0]["VALIDITY"])))).Ticks;
                long lngValidTill = dtPurchaseDate.AddMonths(Convert.ToInt32(Convert.ToDouble(currPlanDetails.Rows[0]["VALIDITY"]))).Ticks;
                long lngUtcNow = DateTime.UtcNow.Ticks;
                if ((lngValidTill - lngUtcNow) >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw new Exception();
            }
        }
        MFICIENT_USER_TYPE getUserType(string _user_domain)
        {
            if (!String.IsNullOrEmpty(_user_domain))
            {
                return MFICIENT_USER_TYPE.ActiveDirectoryUser;
            }
            else
            {
                return MFICIENT_USER_TYPE.CloudUser;
            }
        }
        bool CheckLatestAppVersion(DataTable AppDetail)
        {
            //if (!string.IsNullOrEmpty(_userLoginReq.AppVersion))
            //{
            //string[] ver_inPart = _userLoginReq.AppVersion.Split('.');
            //long minorDeviceAppversion = Convert.ToInt64(ver_inPart[1]) * 1000;
            //if (ver_inPart.Length >= 3)
            //{
            //    minorDeviceAppversion = minorDeviceAppversion + Convert.ToInt64(ver_inPart[2]);
            //}
            //long finalDeviceVersion = Convert.ToInt64(ver_inPart[0]) * 1000000 + minorDeviceAppversion;

            //long finalDBAppversion = (Convert.ToInt64(AppDetail.Rows[0]["MINIMUM_APP_VERSION_MAJOR1"]) * 1000000) + ((Convert.ToInt64(AppDetail.Rows[0]["MINIMUM_APP_VERSION_MINOR1"]) * 1000) + Convert.ToInt64(AppDetail.Rows[0]["MINIMUM_APP_VERSION_REV1"]));

            //if (finalDeviceVersion >= finalDBAppversion)
            //    return true;
            //else
            //    return false;
            //}
            return true;
        }

    }
}