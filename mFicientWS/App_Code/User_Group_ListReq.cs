﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class User_Group_ListReq
    {
        string _requestId, _deviceId, _deviceType, _companyId,_userid,_password,_sesstionId;
        int _functionCode, _type;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public int Type
        {
            get { return _type; }
        }
        public string UserId
        {
            get { return _userid; }
        }
        public string SesstionId
        {
            get { return _sesstionId; }
        }


        public User_Group_ListReq(string requestJson)
        {
            RequestJsonParsing<User_Group_ListReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<User_Group_ListReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.USER_GROUP_LIST)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userid = objRequestJsonParsing.req.data.unm;
            _sesstionId = objRequestJsonParsing.req.sid;
         
        }
        [DataContract]
        public class User_Group_ListReqData : Data
        {
          
        }
    }
    


    }
