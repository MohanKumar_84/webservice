﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;
//namespace mFicientWS
//{
//    public class WSCGetGroupUsers
//    {
//        public WSCGetGroupUsers(string companyId)
//        {
//            this.CompanyId = companyId;
//        }
//        public dat getGroupUsers(string groupName,string Company_id)
//        {
//            List<MFEMobileUser> lstUsers = new List<MFEMobileUser>();
//            try
//            {
                
//                if (dsGroupUsersDtls != null)
//                {
//                    this.StatusCode = 0;
//                    this.StatusDescription = "";
//                    GroupUsers = dsGroupUsersDtls.Tables[0];
//                    if (GroupUsers != null && GroupUsers.Rows.Count > 0)
//                    {
//                        foreach (DataRow row in GroupUsers.Rows)
//                        {
//                            MFEMobileUser objUser = new MFEMobileUser();
//                            objUser = getMobileUserFromDataRow(row);
//                            lstUsers.Add(objUser);
//                        }
//                    }
//                }
//                else
//                {
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
//                }
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//                this.StatusDescription = "Internal server error";
//            }
//            return lstUsers;
//        }
//        public void GetOtherUsers(string groupId)
//        {
//            try
//            {
//                string strQuery = @"SELECT FIRST_NAME,LAST_NAME,EMAIL_ID,USER_NAME,USER_ID FROM TBL_USER_DETAIL WHERE USER_ID not in 
//                                    (SELECT USER_ID FROM TBL_USER_GROUP_LINK WHERE GROUP_ID = @GROUP_ID)
//                                    AND COMPANY_ID = @CompanyId";
//                SqlCommand cmd = new SqlCommand(strQuery);
//                cmd.Parameters.AddWithValue("@GROUP_ID", groupId);
//                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
//                cmd.CommandType = CommandType.Text;
//                DataSet dsGroupUsersDtls = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
//                if (dsGroupUsersDtls != null)
//                {
//                    this.StatusCode = 0;
//                    this.StatusDescription = "";
//                    GroupUsers = dsGroupUsersDtls.Tables[0];
//                }
//                else
//                {
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
//                }
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//                this.StatusDescription = "Internal server error";
//            }
//        }

//        public string GetUsersName(string groupId)
//        {
//            try
//            {
//                string strQuery = @" SELECT SUBSTRING((select ' , '+ d.FIRST_NAME+' '+d.LAST_NAME+' ( '+d.USER_NAME +' )' from TBL_USER_GROUP g inner join TBL_USER_GROUP_LINK u
//                on g.GROUP_ID=u.GROUP_ID inner join TBL_USER_DETAIL d on u.USER_ID=d.USER_ID where g.GROUP_ID=@GROUP_ID FOR XML PATH('')),3,3000)as USERS";
//                SqlCommand cmd = new SqlCommand(strQuery);
//                cmd.Parameters.AddWithValue("@GROUP_ID", groupId);

//                cmd.CommandType = CommandType.Text;
//                DataSet dsGroupUsersDtls = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
//                if (dsGroupUsersDtls != null && dsGroupUsersDtls.Tables[0].Rows.Count > 0)
//                {
//                    this.StatusCode = 0;
//                    this.StatusDescription = "";
//                    return dsGroupUsersDtls.Tables[0].Rows[0][0].ToString();
//                }
//            }
//            catch
//            {
//                this.StatusCode = -1000;
//                this.StatusDescription = "Internal server error";
//            }
//            return "";
//        }

//        MFEMobileUser getMobileUserFromDataRow(DataRow rowUserDtl)
//        {
//            if (rowUserDtl == null) throw new ArgumentNullException();
//            MFEMobileUser objMobileUser = new MFEMobileUser();
//            objMobileUser.UserId = Convert.ToString(rowUserDtl["USER_ID"]);
//            objMobileUser.EmailId = Convert.ToString(rowUserDtl["EMAIL_ID"]);
//            objMobileUser.Password = Convert.ToString(rowUserDtl["ACCESS_CODE"]);
//            objMobileUser.FirstName = Convert.ToString(rowUserDtl["FIRST_NAME"]);
//            objMobileUser.LastName = Convert.ToString(rowUserDtl["LAST_NAME"]);
//            objMobileUser.Mobile = Convert.ToString(rowUserDtl["MOBILE"]);
//            objMobileUser.CompanyId = Convert.ToString(rowUserDtl["COMPANY_ID"]);
//            objMobileUser.Dob = Convert.ToInt64(rowUserDtl["DATE_OF_BIRTH"]);
//            objMobileUser.RegistrationDatetime = Convert.ToInt64(rowUserDtl["REGISTRATION_DATETIME"]);
//            objMobileUser.SubAdminId = Convert.ToString(rowUserDtl["SUBADMIN_ID"]);
//            objMobileUser.LocationId = Convert.ToString(rowUserDtl["LOCATION_ID"]);
//            objMobileUser.DesignationId = Convert.ToString(rowUserDtl["DESIGNATION_ID"]);
//            objMobileUser.EmployeeNo = Convert.ToString(rowUserDtl["EMPLOYEE_NO"]);
//            objMobileUser.Username = Convert.ToString(rowUserDtl["USER_NAME"]);
//            objMobileUser.IsActive = Convert.ToBoolean(rowUserDtl["IS_ACTIVE"]);
//            objMobileUser.RegionId = Convert.ToString(rowUserDtl["REGION_ID"]);
//            objMobileUser.IsBlocked = Convert.ToBoolean(rowUserDtl["IS_BLOCKED"]);
//            objMobileUser.AllowMessenger = Convert.ToBoolean(rowUserDtl["ALLOW_MESSENGER"]);
//            objMobileUser.IsOfflineWorkAllowed = Convert.ToBoolean(rowUserDtl["IS_OFFLINE_WORK"]);
//            objMobileUser.UpdatedOn = Convert.ToInt64(rowUserDtl["UPDATE_ON"]);
//            objMobileUser.DesktopMessenger = Convert.ToBoolean(rowUserDtl["DESKTOP_MESSENGER"]);
//            objMobileUser.RequestedBy = Convert.ToString(rowUserDtl["REQUESTED_BY"]);
//            objMobileUser.DomainId = Convert.ToString(rowUserDtl["DOMAIN_ID"]);
//            objMobileUser.MobileDetails = null;
//            return objMobileUser;
//        }
//        public DataTable GroupUsers
//        {
//            set;
//            get;
//        }
//        public int StatusCode
//        {
//            set;
//            get;
//        }
//        public string StatusDescription
//        {
//            set;
//            get;
//        }
//        public string GroupId
//        {
//            get;
//            set;
//        }
//        public string CompanyId
//        {
//            get;
//            set;
//        }
//    }
//}