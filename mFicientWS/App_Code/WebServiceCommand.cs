﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Net;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class WebServiceCommand
    {
        #region Private Members

        private string commandId, connectorId, enterpriseId, tag, returnType, url, recordPath, datasetPath, commandName,
            service, method, soapRequest, httpRequest, operationAction, portType, mPluginAgent, mPluginAgentPwd
            , _inputXmlRpcParam, _outputXmlRpcParam, _rpcObject, cacheType, cacheFrequency, cacheCondition,contantType;

        private List<IdeWsParam> output;
        private HttpType httpType;
        private WebServiceConnector wsConnector;
        private JArray headers;

        string[] contantTypes = new string[] { "multipart-form-data", "application/x-www-form-urlencoded", "application/json", "application/xml", "application/base64", "application/octet-stream" };
        DoTaskReq _doTaskRequest;
        WebserviceType _webServiceType;
        ResponseStatus objRespStatus = new ResponseStatus();

        #endregion

        #region Constructor
        public bool isExist = false;
        internal WebServiceCommand(string _commandId, string _enterpriseId)
        {
            commandId = _commandId;
            enterpriseId = _enterpriseId;
            output = new List<IdeWsParam>();
            GetCommandDetails();
        }

        #endregion

        #region Public Properties
        public string CommandId
        {
            get
            {
                return commandId;
            }
        }
        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
        }

        public WebServiceConnector webserviceConnector
        {
            get
            {
                try
                {
                    wsConnector = new WebServiceConnector(connectorId, enterpriseId);
                    wsConnector.GetConnector();
                }
                catch
                {
                }
                return wsConnector;
            }
        }

        public HttpType HTTPType
        {
            get
            {
                return httpType;
            }
        }

        public string Tag
        {
            get
            {
                return tag;
            }
        }

        public string ReturnType
        {
            get
            {
                return returnType;
            }
        }

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
        }

        public string Url
        {
            get
            {
                return url;
            }
        }

        public string RecordPath
        {
            get
            {
                return recordPath;
            }
        }

        public string DataSetPath
        {
            get
            {
                return datasetPath;
            }
        }

        public string CommandName
        {
            get
            {
                return commandName;
            }
        }

        public string Method
        {
            get
            {
                return method;
            }
        }

        public string Service
        {
            get
            {
                return service;
            }
        }

        public string PortType
        {
            get
            {
                return portType;
            }
        }

        public string SoapRequest
        {
            get
            {
                return soapRequest;
            }
        }

        public string HttpRequest
        {
            get
            {
                return httpRequest;
            }
        }

        public string OperationAction
        {
            get
            {
                return operationAction;
            }
        }
        public string MPluginAgent
        {
            get
            {
                return mPluginAgent;
            }
        }
        public string MPluginAgentPwd
        {
            get
            {
                return mPluginAgentPwd;
            }
        }

        public string XMLRPCObject
        {
            get
            {
                return _rpcObject;
            }
        }

        public WebserviceType WSType
        {
            get
            {
                return _webServiceType;
            }
        }

        public string OutputXmlRpcParam
        {
            get { return _outputXmlRpcParam; }
        }

        public string InputXmlRpcParam
        {
            get { return _inputXmlRpcParam; }
        }
        public List<IdeWsParam> OutPutTagPaths
        {
            get { return output; }
        }
        #endregion

        #region Method

        public DoTaskResp processWSCommand(DoTaskReq doTaskRequest)
        {
            _doTaskRequest = doTaskRequest;
            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {
                if (this.CommandName == null)
                {
                    return new DoTaskResp(Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND).ToString())),
                    _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition,mPluginAgent,new List<List<string>>());
                }
                if (!String.IsNullOrEmpty(this.MPluginAgent.Trim()))
                {
                    return processWSCommandWithmPlugin();
                }
                else
                {
                    _doTaskRequest.DataCredential.pwd = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.DataCredential.pwd);
                    _doTaskRequest.DataCredential.unm = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.DataCredential.unm);
                    return processWSCommandWithoutmPlugin();
                }
            }
            catch (Exception e)
            {
                objRespStatus = Utilities.getResponseStatus(e);
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "0", "", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, new List<List<string>>());
        }

        private void GetCommandDetails()
        {

            string strQuery = @"SELECT cmd.*,isnull(ag.MP_AGENT_PASSWORD, '') as MP_AGENT_PASSWORD,isnull(con.MPLUGIN_AGENT,'') as MPLUGIN_AGENT FROM TBL_WS_COMMAND as cmd inner join TBL_WEBSERVICE_CONNECTION as con on cmd.WS_CONNECTOR_ID=con.WS_CONNECTOR_ID and cmd.COMPANY_ID=con.COMPANY_ID 
            left outer join TBL_MPLUGIN_AGENT_DETAIL as ag on ag.MP_AGENT_NAME=con.MPLUGIN_AGENT and ag.COMPANY_ID=con.COMPANY_ID  WHERE cmd.COMPANY_ID = @COMPANY_ID ";
            if (commandId.StartsWith("name://"))
            {
                strQuery += " and WS_COMMAND_NAME = @WS_COMMAND_ID;";
                commandId = commandId.Substring(7);
            }
            else strQuery += " and WS_COMMAND_ID = @WS_COMMAND_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@WS_COMMAND_ID", commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                DataRow _dr = ds.Tables[0].Rows[0];
                isExist = true;
                connectorId = Convert.ToString(_dr["WS_CONNECTOR_ID"]);
                mPluginAgentPwd = Convert.ToString(_dr["MP_AGENT_PASSWORD"]);
                mPluginAgent = Convert.ToString(_dr["MPLUGIN_AGENT"]);
                url = Convert.ToString(_dr["URL"]);
                returnType = Convert.ToString(_dr["RETURN_TYPE"]);
                httpType = (HttpType)Convert.ToInt32(Convert.ToString(_dr["HTTP_TYPE"]));
                commandName = Convert.ToString(_dr["WS_COMMAND_NAME"]);
                recordPath = Convert.ToString(_dr["RECORD_PATH"]);
                datasetPath = Convert.ToString(_dr["DATASET_PATH"]);
                tag = Convert.ToString(_dr["TAG"]);
                if (tag != "")
                {
                    MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(tag));
                    System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(output.GetType());
                    output = serializer.ReadObject(ms) as List<IdeWsParam>;
                }

                service = Convert.ToString(_dr["SERVICE"]);
                method = Convert.ToString(_dr["METHOD"]);
                httpRequest = Convert.ToString(_dr["HTTP_REQUEST"]);

                if (this.HTTPType == HttpType.POST)
                {
                    url =  url +"?"+ httpRequest;
                }
                else
                {
                    url = url + httpRequest;
                }

                   
                contantType = contantTypes[1];
                string headers = Convert.ToString(_dr["Headers"]).Trim();
                JArray jaHeaders = JArray.Parse(headers);         
                if (headers.Length > 0)
                {
                    foreach (JArray joHeader in jaHeaders)
                    {
                        switch (joHeader[0].ToString())
                        {
                            case "ct":
                                contantType = contantTypes[int.Parse(joHeader[1].ToString())];
                                break;
                        }
                    }
                }

                soapRequest = Convert.ToString(_dr["SOAP_REQUEST"]);
                operationAction = Convert.ToString(_dr["OPERATION_ACTION"]);
                portType = Convert.ToString(_dr["PORT_TYPE"]);
                commandId = Convert.ToString(_dr["WS_COMMAND_ID"]);
                string strWebServiceType = Convert.ToString(_dr["WEBSERVICE_TYPE"]);
                cacheType = Convert.ToString(_dr["CACHE"]);
                cacheFrequency = Convert.ToString(_dr["EXPIRY_FREQUENCY"]);
                cacheCondition = Convert.ToString(_dr["EXPIRY_CONDITION"]);
                switch (Convert.ToString(_dr["WEBSERVICE_TYPE"].ToString().Trim().ToLower()))
                {
                    case "wsdl":
                        _webServiceType = WebserviceType.WSDL_SOAP;
                        break;
                    case "http":
                        _webServiceType = WebserviceType.HTTP;
                        break;
                    case "rpc":
                        _webServiceType = WebserviceType.RPC_XML;
                        break;
                    default:
                        _webServiceType = WebserviceType.HTTP;
                        break;
                }
                if (_webServiceType == WebserviceType.RPC_XML)
                {
                    _inputXmlRpcParam = Convert.ToString(_dr["PARAMETER"]);
                    _outputXmlRpcParam = Convert.ToString(_dr["TAG"]);
                }
            }
        }

        DoTaskResp processWSCommandWithmPlugin()
        {
            WebServiceConnector wscon = new WebServiceConnector(this.ConnectorId, this.EnterpriseId);
            wscon.GetConnector();
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
            string strMPluginUrl = Utilities.getMPlugInServerURL(_doTaskRequest.CompanyId);
            string strResponse = "";
            Object objRespData = null;
            if (!String.IsNullOrEmpty(strMPluginUrl))
            {
                string strHttpUrl = strMPluginUrl;
                if (String.IsNullOrEmpty(strHttpUrl))
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                else
                {
                    string QueryString = "";

                    if (wscon.WebServicetype == WebserviceType.WSDL_SOAP)
                    {
                        if (!String.IsNullOrEmpty(this.SoapRequest))
                        {
                            QueryString = this.SoapRequest;
                            foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                            {
                                QueryString = QueryString.Replace("@@" + parameters.para + "@@", parameters.val);
                            }
                            try
                            {
                                strResponse = mPlugin.RunSOAPWebservice(this.EnterpriseId, this.MPluginAgent, this.MPluginAgentPwd, this.ConnectorId, QueryString, this.OperationAction.Split(',')[1], wscon.HttpAuthenticationType, _doTaskRequest.UserName);
                                return responseForReturnFunctionZero(wscon.Responsetype, strResponse);
                            }
                            catch (HttpException hex)
                            {
                                throw Utilities.HttpExecptionToException(hex);
                            }
                            catch (mPlugin.mPluginException mex)
                            {
                                int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(mex.Message));
                                throw new Exception(errorCode.ToString());
                            }
                            catch (Exception ex)
                            {
                                if (ex.Source.ToUpper() == "SYSTEM.XML")
                                {
                                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML).ToString());
                                }
                                else
                                {
                                    throw ex;
                                }
                            }
                        }

                    }
                    else if (wscon.WebServicetype == WebserviceType.HTTP)
                    {
                        QueryString = this.Url;

                        foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                        {
                            QueryString = QueryString.Replace("@@" + parameters.para + "@@", parameters.val);
                        }
                        switch (this.HTTPType)
                        {
                            case HttpType.GET:
                                try
                                {
                                    strResponse = mPlugin.RunWebservice(this.EnterpriseId, this.MPluginAgent, this.MPluginAgentPwd, this.ConnectorId, QueryString, mPlugin.QueryType.GET, wscon.HttpAuthenticationType, _doTaskRequest.UserName);

                                    return responseForReturnFunctionZero(wscon.Responsetype, strResponse);
                                }
                                catch (HttpException hex)
                                {
                                    if (hex.Message == HttpStatusCode.NotFound.ToString())
                                    {
                                        throw new Exception(((int)DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND).ToString());
                                    }
                                    else
                                    {
                                        throw Utilities.HttpExecptionToException(hex);
                                    }
                                }
                                catch (MficientException hex)
                                {
                                    int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(hex));
                                    throw new Exception(errorCode.ToString());
                                }
                                catch (mPlugin.mPluginException mex)
                                {
                                    int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(mex.Message));
                                    throw new Exception(errorCode.ToString());
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Source.ToUpper() == "SYSTEM.XML")
                                    {
                                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML).ToString());
                                    }
                                    else
                                    {
                                        throw ex;
                                    }
                                }
                            case HttpType.POST:
                                try
                                {
                                    strResponse = mPlugin.RunWebservice(this.EnterpriseId, this.MPluginAgent, this.MPluginAgentPwd, this.ConnectorId, QueryString, mPlugin.QueryType.POST, wscon.HttpAuthenticationType, _doTaskRequest.UserName);
                                    return responseForReturnFunctionZero(wscon.Responsetype, strResponse);
                                }
                                catch (HttpException hex)
                                {
                                    if (hex.Message == HttpStatusCode.NotFound.ToString())
                                    {
                                        throw new Exception(((int)DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND).ToString());
                                    }
                                    else
                                    {
                                        throw Utilities.HttpExecptionToException(hex);
                                    }
                                }
                                catch (MficientException hex)
                                {
                                    int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(hex));
                                    throw new Exception(errorCode.ToString());
                                }
                                catch (mPlugin.mPluginException mex)
                                {
                                    int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(mex.Message));
                                    throw new Exception(errorCode.ToString());
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Source.ToUpper() == "SYSTEM.XML")
                                    {
                                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML).ToString());
                                    }
                                    else
                                    {
                                        throw ex;
                                    }
                                }
                        }
                    }

                    else if (wscon.WebServicetype == WebserviceType.RPC_XML)
                    {
                        try
                        {
                            RpcParamaters objInput = new RpcParamaters();
                            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(this.InputXmlRpcParam));
                            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(objInput.GetType());
                            objInput = serializer.ReadObject(ms) as RpcParamaters;
                            ms.Close();

                            Stream requestStream;
                            XmlRpcRequestXml objRequest = new XmlRpcRequestXml(objInput, this.Method, _doTaskRequest.Parameters, wscon.WebserviceUrl);
                            XmlDocument xmlQueryString = objRequest.getRequestXML(out requestStream);
                            StreamReader reader = new StreamReader(requestStream);

                            string rpcResponse = mPlugin.RunWebservice(this.EnterpriseId, this.MPluginAgent, this.MPluginAgentPwd, this.ConnectorId, reader.ReadToEnd(), mPlugin.QueryType.POST, wscon.HttpAuthenticationType, _doTaskRequest.UserName);

                            RpcParamaters objRpcParamOutput = new RpcParamaters();
                            ms = new MemoryStream(Encoding.Unicode.GetBytes(this.OutputXmlRpcParam));
                            serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(objRpcParamOutput.GetType());
                            objRpcParamOutput = serializer.ReadObject(ms) as RpcParamaters;
                            ms.Close();

                            XmlDocument xmlOutputResp = new XmlDocument();
                            xmlOutputResp.LoadXml(rpcResponse);
                            XmlRpcResponseParse objRpcRespParse = new XmlRpcResponseParse(xmlOutputResp, objRpcParamOutput, RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_RETURN_ZERO);
                            try
                            {
                                objRpcRespParse.Process();
                                objRespStatus.cd = ((int)DO_TASK_ERROR.NONE).ToString();
                                objRespStatus.desc = " ";
                                objRespData = getResponseDataFromXmlRpcRespParse(objRpcRespParse);
                            }
                            catch (Exception e)
                            {
                                throw e;
                            }
                        }
                        catch (InvalidOperationException ex)
                        {
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML).ToString());
                        }
                        catch (MficientException hex)
                        {
                            int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(hex));
                            throw new Exception(errorCode.ToString());
                        }
                        catch (mPlugin.mPluginException mex)
                        {
                            int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(mex.Message));
                            throw new Exception(errorCode.ToString());
                        }
                        catch (HttpException hex)
                        {
                            if (hex.Message == HttpStatusCode.NotFound.ToString())
                            {
                                throw new Exception(((int)DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND).ToString());
                            }
                            else
                            {
                                throw Utilities.HttpExecptionToException(hex);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Source.ToUpper() == "SYSTEM.XML")
                            {
                                throw new Exception(((int)DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML).ToString());
                            }
                            else
                            {
                                throw ex;
                            }
                        }
                    }
                }
            }
            else
            {
                throw new Exception();
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "0", "", objRespData, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, new List<List<string>>());//getWsdlAndWSProcessResponse(objRespStatus, objRespData);
        }

        DoTaskResp processWSCommandWithoutmPlugin()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
            bool isURLValid;
            string strResponse = "";
            Object objRespData = null;
            string strUserName = "", strPassword = "";
            if (!String.IsNullOrEmpty(this.ConnectorId))
            {
                WebServiceConnector wscon = new WebServiceConnector(this.ConnectorId, this.EnterpriseId);
                wscon.GetConnector();
                if (!String.IsNullOrEmpty(wscon.UserId) && !String.IsNullOrEmpty(wscon.Password))
                {
                    strUserName = wscon.UserId;
                    strPassword = wscon.Password;
                }
                else if (!String.IsNullOrEmpty(wscon.CredentialProperty))
                {
                    if (_doTaskRequest.DataCredential != null)
                    {
                        strUserName = _doTaskRequest.DataCredential.unm;
                        strPassword = _doTaskRequest.DataCredential.pwd;
                    }
                    else
                    {
                        //TODO throw exception
                    }
                }
                HttpStatusCode statusCode = HttpStatusCode.OK;
                try
                {

                    if (wscon.WebServicetype == WebserviceType.HTTP)
                    {
                        string strWebServiceURL = getWebServiceURL(wscon, out isURLValid, _doTaskRequest);
                        if (this.HttpRequest != "")
                        {
                            if (this.HTTPType == HttpType.POST)
                            {
                                strResponse = callHttpWebService(strWebServiceURL, WebRequestMethods.Http.Post, strUserName, strPassword,
                                    out statusCode, (HTTP.EnumHttpAuthentication)Convert.ToInt32(wscon.HttpAuthenticationType), contantType);
                            }
                            else
                            {
                                strResponse = callHttpWebService(strWebServiceURL, WebRequestMethods.Http.Get, strUserName, strPassword,
                                    out statusCode, (HTTP.EnumHttpAuthentication)Convert.ToInt32(wscon.HttpAuthenticationType), contantType);
                            }
                            if (statusCode != HttpStatusCode.OK)
                                throw new Exception(Utilities.processHttpStatusCode((int)statusCode).ToString());
                            return responseForReturnFunctionZero(wscon.Responsetype, strResponse);
                        }
                        else
                        {
                            throw new Exception(((int)DO_TASK_ERROR.WEB_SERVICE_PARAMETERS_ERROR).ToString());
                        }
                    }
                    else if (!String.IsNullOrEmpty(this.SoapRequest))
                    {
                        XmlDocument soapXmlDocument = new XmlDocument();
                        string QueryString = this.SoapRequest;
                        foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                        {
                            QueryString = QueryString.Replace("@@" + parameters.para + "@@", parameters.val);
                        }
                        soapXmlDocument.LoadXml(QueryString);

                        strResponse = callSoapWebService(
                            this.OperationAction.Split(',')[0],
                            this.OperationAction.Split(',')[1],
                            soapXmlDocument,
                            strUserName,
                            strPassword,
                            out statusCode, (HTTP.EnumHttpAuthentication)Convert.ToInt32(wscon.HttpAuthenticationType));

                        if (statusCode != HttpStatusCode.OK)
                            throw new Exception(Utilities.processHttpStatusCode((int)statusCode).ToString());

                        return responseForReturnFunctionZero(wscon.Responsetype, strResponse);
                    }
                    else if (wscon.WebServicetype == WebserviceType.RPC_XML)
                    {
                        string strXmlRpcObject = this.XMLRPCObject;

                        RpcParamaters objInput = new RpcParamaters();
                        MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(this.InputXmlRpcParam));
                        System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(objInput.GetType());
                        objInput = serializer.ReadObject(ms) as RpcParamaters;
                        ms.Close();

                        XmlRpcRequestXml obj = new XmlRpcRequestXml(objInput, this.Method, _doTaskRequest.Parameters, wscon.WebserviceUrl);
                        try
                        {
                            string rpcResponse = obj.callXMLRpcWebService(strUserName, strPassword);

                            RpcParamaters objRpcParamOutput = new RpcParamaters();
                            ms = new MemoryStream(Encoding.Unicode.GetBytes(this.OutputXmlRpcParam));
                            serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(objRpcParamOutput.GetType());
                            objRpcParamOutput = serializer.ReadObject(ms) as RpcParamaters;
                            ms.Close();

                            XmlDocument xmlOutputResp = new XmlDocument();
                            xmlOutputResp.LoadXml(rpcResponse);
                            XmlRpcResponseParse objRpcRespParse = new XmlRpcResponseParse(xmlOutputResp, objRpcParamOutput, RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_RETURN_ZERO);
                            try
                            {
                                objRpcRespParse.Process();
                                objRespStatus.cd = ((int)DO_TASK_ERROR.NONE).ToString();
                                objRespStatus.desc = " ";
                                objRespData = getResponseDataFromXmlRpcRespParse(objRpcRespParse);
                            }
                            catch (Exception e)
                            {
                                throw e;
                            }
                        }
                        catch (InvalidOperationException ex)
                        {
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML).ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                throw new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND).ToString());
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", objRespData, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, new List<List<string>>());
        }

        string getWebServiceURL(WebServiceConnector wscon, out bool isValidURL, DoTaskReq _doTaskRequest)
        {
            //if isHttpFromWSDL is true then the whole url will be available from the database itself so the first step is not required
            isValidURL = false;
            string strWebServiceURL="";

            if (wscon.WebServicetype == WebserviceType.HTTP)
            {
                // strWebServiceURL = infoForQueryDTable.Rows[0]["WEBSERVICE_TYPE"] + "://" + infoForQueryDTable.Rows[0]["WEBSERVICE_URL"] + infoForQueryDTable.Rows[0]["URL"];
                strWebServiceURL =( (wscon.WebserviceUrl.LastIndexOf("/") + 1 == wscon.WebserviceUrl.Length) ? wscon.WebserviceUrl.Substring(0, wscon.WebserviceUrl.Length - 1) : wscon.WebserviceUrl )+ (this.Url.StartsWith("/") ? this.Url : "/" + this.Url);
               
                foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                {
                    strWebServiceURL = strWebServiceURL.Replace("@@" + parameters.para + "@@", Utilities.UrlEncode(parameters.val));
                }

                if (!strWebServiceURL.Contains("@@"))
                {
                    isValidURL = true;
                }
            }
            else
            {
                strWebServiceURL = wscon.WebserviceUrl;
            }
            return strWebServiceURL;
        }
        string getXMLRPCSerialisedObject()
        {
            string strXMLSerialisedObject = String.Empty;
            string strQuery = @"SELECT * FROM TBL_SERIALIZE_OBJECT
                                  WHERE WS_COMMAND_ID=@WS_COMMAND_ID
                                  AND COMPANY_ID=@COMPANY_ID";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@WS_COMMAND_ID", this.commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.enterpriseId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                strXMLSerialisedObject = Convert.ToString(ds.Tables[0].Rows[0]["SERIALIZE_OBJECT"]);
            }
            return strXMLSerialisedObject;
        }
        void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
        HttpWebRequest CreateWebRequest(string url, string action, string username, string password)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                webRequest.Credentials = new NetworkCredential(username, password);
            }
            return webRequest;
        }
        string callHttpWebService(string webServiceURL, string httpType, string username, string password, out HttpStatusCode statusCode, HTTP.EnumHttpAuthentication httpAuthentication,string contantType)
        {
            HTTP oHttp = new HTTP(webServiceURL);
            oHttp.HttpRequestMethod = httpType;
            oHttp.HttpAuthentication = httpAuthentication;
            oHttp.SetContantType(contantType);
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                oHttp.SetHttpCredentials(username, password);
            }
            HttpResponseStatus oResponse = oHttp.Request();
            statusCode = oResponse.StatusCode;
            if (statusCode == HttpStatusCode.OK)
            {
                return oResponse.ResponseText;
            }
            else
            {
                //txtResponse.Text = oResponse.ErrorMessage;
                return "";
            }

        }
        string callSoapWebService(string url, string action, XmlDocument soapEnvelopeXml, string username, string password, out HttpStatusCode statusCode, HTTP.EnumHttpAuthentication httpAuthentication)
        {
            HttpWebRequest webRequest = CreateWebRequest(url, action, username, password);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            asyncResult.AsyncWaitHandle.WaitOne();

            string soapResult = String.Empty;
            try
            {
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    statusCode = HttpStatusCode.OK;
                    soapResult = rd.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                statusCode = ((HttpWebResponse)ex.Response).StatusCode;
            }
            return soapResult;
        }
        Object getResponseDataFromXmlRpcRespParse(XmlRpcResponseParse parsedObject)
        {
            Object objToReturn = null;
            objToReturn = parsedObject.ResponseInXMLDoc;
            return objToReturn;
        }
        DoTaskResp responseForReturnFunctionZero(ResponseType responseType, string response)
        {
            DataTable dtblData = new DataTable();
            DataSet ds = new DataSet();
            if (responseType == ResponseType.XML)
            {
                readHTTPXMLResponse objReadXMLResp = new readHTTPXMLResponse(response, this);
                dtblData = (DataTable)objReadXMLResp.Process();
            }
            else
            {

                ReadJsonResponseForDoTask objReadJson =
                   new ReadJsonResponseForDoTask(response, this);
                dtblData = (DataTable)objReadJson.Process();
            }
            if (dtblData != null)
                ds.Tables.Add(dtblData);
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", ds, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, new List<List<string>>());

        }
        #endregion
    }

    public class IdeWsParam
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string path { get; set; }
    }
}