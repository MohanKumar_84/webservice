﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class WebServiceConnector
    {
        #region Private Members

        private string enterpriseId, connectorId, connectionName, webserviceUrl, userId, password, mPluginAgentName;
        private WebserviceType webserviceType;
        private ResponseType responseType;
        string _credentialProperty;
        string _httpAuthenticationType;
        #endregion

        #region Constructor

        internal WebServiceConnector(string _connectorId, string _enterpriseId)
        {
            connectorId = _connectorId;
            enterpriseId = _enterpriseId;
        }

        #endregion

        #region Public Properties

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
        }

        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
        }

        public string ConnectionName
        {
            get
            {
                return connectionName;
            }
        }

        public string WebserviceUrl
        {
            get
            {
                return webserviceUrl;
            }
        }

        public string UserId
        {
            get
            {
                return userId;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
        }

        public string MpluginAgentName
        {
            get
            {
                return mPluginAgentName;
            }
        }

        public WebserviceType WebServicetype
        {
            get
            {
                return webserviceType;
            }
        }

        public ResponseType Responsetype
        {
            get
            {
                return responseType;
            }
        }

        public string CredentialProperty
        {
            get
            {
                return _credentialProperty;
            }
        }

        public string HttpAuthenticationType
        {
            get
            {
                return _httpAuthenticationType;
            }
        }
        #endregion

        #region Public Methods

        public void GetConnector()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_WEBSERVICE_CONNECTION as wscon left join TBL_MPLUGIN_AGENT_DETAIL as mplugin on wscon.COMPANY_ID = mplugin.COMPANY_ID
                                    AND mplugin.MP_AGENT_ID= wscon.MPLUGIN_AGENT WHERE wscon.COMPANY_ID = @COMPANY_ID AND wscon.WS_CONNECTOR_ID = @WS_CONNECTOR_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                cmd.Parameters.AddWithValue("@WS_CONNECTOR_ID", connectorId);
                DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    connectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                    webserviceUrl = ds.Tables[0].Rows[0]["WEBSERVICE_URL"].ToString();
                    
                    userId = ds.Tables[0].Rows[0]["USER_NAME"].ToString();
                    if (!String.IsNullOrEmpty(userId))
                        userId = EncryptionDecryption.AESDecrypt(this.enterpriseId, userId);

                    mPluginAgentName = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();

                    password = ds.Tables[0].Rows[0]["PASSWORD"].ToString();
                    if (!String.IsNullOrEmpty(password))
                        password = EncryptionDecryption.AESDecrypt(this.enterpriseId, password);

                    string strWebServiceType = ds.Tables[0].Rows[0]["WEBSERVICE_TYPE"].ToString();
                    switch (strWebServiceType.Trim().ToLower())
                    { 
                        case "wsdl":
                            webserviceType = WebserviceType.WSDL_SOAP;
                            break;
                        case "http":
                            webserviceType = WebserviceType.HTTP;
                            break;
                        case "rpc":
                            webserviceType = WebserviceType.RPC_XML;
                            break;
                        default:
                            webserviceType = WebserviceType.HTTP;
                            break;

                    }
                    //webserviceType = (WebserviceType)Convert.ToInt32(ds.Tables[0].Rows[0]["WEBSERVICE_TYPE"].ToString());
                    string strResponseType = ds.Tables[0].Rows[0]["RESPONSE_TYPE"].ToString();

                    switch (strResponseType.Trim().ToLower())
                    {
                        case "xml":
                            responseType = ResponseType.XML;
                            break;
                        case "json":
                            responseType = ResponseType.JSON;
                            break;
                        
                    }
                    _credentialProperty = Convert.ToString(ds.Tables[0].Rows[0]["CREDENTIAL_PROPERTY"]);
                    _httpAuthenticationType = Convert.ToString(ds.Tables[0].Rows[0]["AUTHENTICATION_TYPE"]);
                    //responseType = (ResponseType)Convert.ToInt32(ds.Tables[0].Rows[0]["RESPONSE_TYPE"].ToString());
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion
    }
}