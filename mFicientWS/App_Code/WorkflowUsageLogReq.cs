﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Net;
namespace mFicientWS
{
    public class WorkflowUsageLogReq
    {
        string _requestId, _deviceId, _deviceType, _companyId, _userID, _username, _model;
        int _functionCode;

        List<WFUsageLogAppDetails> _appDetails;

        public List<WFUsageLogAppDetails> AppDetails
        {
            get { return _appDetails; }
        }
        public string UserID
        {
            get { return _userID; }
        }
        public string Username
        {
            get { return _username; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string Model
        {
            get { return _model; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public WorkflowUsageLogReq(string requestJson, string userId, string model)
        {
            RequestJsonParsing<WorkflowUsageLogReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<WorkflowUsageLogReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.WORK_FLOW_USAGE_LOG)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userID = userId;
            _username = objRequestJsonParsing.req.data.unm;
            _model = model;
            _appDetails = objRequestJsonParsing.req.data.appu;
            if (_appDetails == null)
            {
                throw new Exception(((int)HttpStatusCode.BadRequest).ToString());
            }
        }
    }


    [DataContract]
    public class WorkflowUsageLogReqData : Data
    {
        [DataMember]
        public List<WFUsageLogAppDetails> appu { get; set; }
    }
    public class WFUsageLogAppDetails
    {
        [DataMember]
        public string ntyp { get; set; }

        [DataMember]
        public string appid { get; set; }

        [DataMember]
        public string udt { get; set; }
    }
}