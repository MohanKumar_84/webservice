﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class appFlowDetailAndForm
    {
        string UserId, Company_id;
        int Model_type;
        public appFlowDetailAndForm(string _userId, string _company_id, int _model_type)
        {
            UserId = _userId;
            Company_id = _company_id;
            Model_type = _model_type;
        }
        public List<appFlowDetail> GetAppListMenuWise(string Menu_id)
        {
            List<appFlowDetail> LstApp = new List<appFlowDetail>();
            string strQuery = @"select distinct tblAll.WF_ID, WF_NAME,WF_DESCRIPTION,isnull(tblAll.ICON,'GRAY0041.png') as WF_ICON,[VERSION],MODEL_TYPE,cnt,APP_JS_JSON,APP_TYPE,COMMAND_IDS from 
            (   SELECT WF.*,WFCat.CATEGORY_ID FROM [TBL_CURR_WORKFLOW_DETAIL] as WF 
            INNER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK as WFCat ON WF.WF_ID = WFCat.WORKFLOW_ID COLLATE SQL_Latin1_General_CP1_CI_AS 
            INNER JOIN TBL_WORKFLOW_AND_GROUP_LINK as wfg ON WFCat.WORKFLOW_ID = wfg.WORKFLOW_ID COLLATE Latin1_General_CI_AI 
            INNER JOIN TBL_USER_GROUP_LINK as ug ON wfg.GROUP_ID = ug.GROUP_ID  where  ug.USER_ID =@USER_ID  ) tblAll
            inner join (select count(wf_id) as cnt,wf_id from  TBL_CURR_WORKFLOW_DETAIL group by wf_id) tblcnt on tblAll.WF_ID=tblcnt.WF_ID                
            where tblAll.CATEGORY_ID=@CATEGORY_ID and tblAll.COMPANY_ID=@COMPANY_ID order by WF_NAME";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", Company_id);
            cmd.Parameters.AddWithValue("@USER_ID", UserId);
            cmd.Parameters.AddWithValue("@CATEGORY_ID", Menu_id);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                appFlowDetail app;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    app = new appFlowDetail();
                    app.WF_ID = Convert.ToString(dr["WF_ID"]);
                    app.WF_NAME = Convert.ToString(dr["WF_NAME"]);
                    app.WF_DESCRIPTION = Convert.ToString(dr["WF_DESCRIPTION"]);
                    app.WF_ICON = Convert.ToString(dr["WF_ICON"]);
                    app.VERSION = Convert.ToString(dr["VERSION"]);
                    app.Offline = Convert.ToString(dr["APP_TYPE"]);
                    app.ObjectRefrence = Convert.ToString(dr["COMMAND_IDS"]);
                    JObject jo = JObject.Parse(Convert.ToString(dr["APP_JS_JSON"]));
                    if (Model_type == 1)//device is tablet
                    {
                        if (Convert.ToInt32(Convert.ToString(jo["modelType"])) == Model_type)// for same model
                            LstApp.Add(app);
                        else if (Convert.ToInt32(Convert.ToString(jo["modelType"])) == 0 && Convert.ToInt32(dr["cnt"]) == 1)// mobile design in tablet
                            LstApp.Add(app);
                    }
                    else if (Model_type == 0)//device is mobile
                    {
                        if (Convert.ToInt32(Convert.ToString(jo["modelType"])) == Model_type)// for same model
                            LstApp.Add(app);
                        else if (Convert.ToInt32(Convert.ToString(jo["modelType"])) == 1 && Convert.ToInt32(jo["runsOn"]) == 0 && Convert.ToInt32(dr["cnt"]) == 1)// tablet design in mobile if runson both
                            LstApp.Add(app);
                    }
                }
            }
            return LstApp;
        }

        public jqueryAppClass GetNewAppJson(string AppId, out DataSet dtObjects)
        {
            jqueryAppClass app = new jqueryAppClass();
            string strQuery = @"SELECT APP_JS_JSON,MODEL_TYPE FROM TBL_CURR_WORKFLOW_DETAIL WF   
                                WHERE WF.WF_ID = @WorkFlowId AND  WF.COMPANY_ID=@COMPANY_ID;";


            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", Company_id);
            cmd.Parameters.AddWithValue("@WorkFlowId", AppId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            int rcnt = ds.Tables[0].Rows.Count;

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drApp in ds.Tables[0].Rows)
                {
                    if (Model_type == Convert.ToInt32(drApp["MODEL_TYPE"]))
                    {
                        if (drApp["APP_JS_JSON"].ToString().Trim().Length > 0)
                        {
                            app = new jqueryAppClass();
                            jqueryAppClass objappjson = Utilities.DeserialiseJson<jqueryAppClass>(Convert.ToString(drApp["APP_JS_JSON"]));
                            app = objappjson;
                        }
                    }
                    else if (rcnt == 1)
                    {
                        if (drApp["APP_JS_JSON"].ToString().Trim().Length > 0)
                        {
                            app = new jqueryAppClass();
                            jqueryAppClass objappjson = Utilities.DeserialiseJson<jqueryAppClass>(Convert.ToString(drApp["APP_JS_JSON"]));
                            if ((Model_type == (int)DEVICE_MODEL_TYPE.PHONE && Convert.ToInt32(drApp["MODEL_TYPE"]) == (int)DEVICE_MODEL_TYPE.TABLET && objappjson.runsOn == "0") //runson both,design tablet
                                || (Model_type == (int)DEVICE_MODEL_TYPE.TABLET && Convert.ToInt32(drApp["MODEL_TYPE"]) == (int)DEVICE_MODEL_TYPE.PHONE)) //runson both,design mobile
                                app = objappjson;
                        }
                    }
                }
            }


            strQuery = @"select DB_COMMAND_ID as COMMAND_ID,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,'DB' as ctype,ds.CREDENTIAL_PROPERTY,isnull(mp.MP_AGENT_ID,'') as MPLUGIN_AGENT,IMAGE_COLUMN,NO_EXECUTE_CONDITION from TBL_DATABASE_COMMAND as do
                inner join TBL_DATABASE_CONNECTION as ds on do.DB_CONNECTOR_ID=ds.DB_CONNECTOR_ID and do.COMPANY_ID=ds.COMPANY_ID
                left outer join TBL_MPLUGIN_AGENT_DETAIL as mp on  ds.MPLUGIN_AGENT=mp.MP_AGENT_name and mp.COMPANY_ID=ds.COMPANY_ID where do.COMPANY_ID=@COMPANY_ID
                union
                select WS_COMMAND_ID as COMMAND_ID,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,'WS'  as ctype,ws.CREDENTIAL_PROPERTY as CREDENTIAL_PROPERTY,isnull(mp.MP_AGENT_ID,'') as MPLUGIN_AGENT,'' as IMAGE_COLUMN,'[]' as NO_EXECUTE_CONDITION  from TBL_WS_COMMAND as wo inner join TBL_WEBSERVICE_CONNECTION as ws 
                on wo.WS_CONNECTOR_ID=ws.WS_CONNECTOR_ID and wo.COMPANY_ID=ws.COMPANY_ID
				left outer join TBL_MPLUGIN_AGENT_DETAIL as mp on  ws.MPLUGIN_AGENT=mp.MP_AGENT_name and mp.COMPANY_ID=ws.COMPANY_ID where ws.COMPANY_ID=@COMPANY_ID
                union
                select ODATA_COMMAND_ID as COMMAND_ID,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,'OD'  as ctype,ws.CREDENTIAL_PROPERTY as CREDENTIAL_PROPERTY,isnull(mp.MP_AGENT_ID,'') as MPLUGIN_AGENT,'' as IMAGE_COLUMN,'[]' as NO_EXECUTE_CONDITION  from TBL_ODATA_COMMAND as wo inner join TBL_ODATA_CONNECTION as ws 
                on wo.ODATA_CONNECTOR_ID=ws.ODATA_CONNECTOR_ID and wo.COMPANY_ID=ws.COMPANY_ID
				left outer join TBL_MPLUGIN_AGENT_DETAIL as mp on ws.MPLUGIN_AGENT=mp.MP_AGENT_name and mp.COMPANY_ID=ws.COMPANY_ID  where ws.COMPANY_ID=@COMPANY_ID;

                SELECT OFFLINE_OBJECT_ID ,t.TABLE_NAME,t.TABLE_ID FROM TBL_OFFLINE_OBJECTS as o  inner join TBL_OFFLINE_DATA_TABLE as t on o.Offline_tables=t.TABLE_ID where o.company_id=@COMPANY_ID;";


                 cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", Company_id);
                cmd.Parameters.AddWithValue("@WorkFlowId", AppId);
                 ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                 dtObjects = ds;
            return app;
        }
    }


    public class appFlowDetail
    {
        public string WF_ID
        {
            get;
            set;
        }
        public string WF_NAME
        {
            get;
            set;
        }
        public string WF_DESCRIPTION
        {
            get;
            set;
        }
        public string WF_ICON
        {
            get;
            set;
        }
        public string VERSION
        {
            get;
            set;
        }
        public string Offline
        {
            get;
            set;
        }
        public string ObjectRefrence
        {
            get;
            set;
        }
        public string WF_JSON
        {
            get;
            set;
        }
        public List<appFormsDetail> Form
        {
            get;
            set;
        }
        public List<appChildFormsDetail> ChildForm
        {
            get;
            set;
        }
    }

    public class appFormsDetail
    {
        public string FORM_ID
        {
            get;
            set;
        }
        public string FORM_MASTER_JSON
        {
            get;
            set;
        }
        public string FORM_JSON
        {
            get;
            set;
        }
    }
    public class appChildFormsDetail
    {
        public string CHILD_FORM_ID
        {
            get;
            set;
        }
        public string MASTER_JSON
        {
            get;
            set;
        }
        public string CHILD_FORM_JSON
        {
            get;
            set;
        }
        public string PARENT_ID
        {
            get;
            set;
        }
        public string FORM_TYPE
        {
            get;
            set;
        }
        public string HTML_FORM_ID
        {
            get;
            set;
        }
    }
}