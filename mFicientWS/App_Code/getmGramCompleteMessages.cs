﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class getmGramCompleteMessages
    {
        getmGramCompleteMessagesReq _getmGramMessagesReq;
        public getmGramCompleteMessages(getmGramCompleteMessagesReq getmGramMessagesReq)
        {
            _getmGramMessagesReq = getmGramMessagesReq;
        }

        public getmGramCompleteMessagesResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            string strQuery = @"SELECT [MESSAGE] 
                                    FROM TBL_MGRAM_MSG WHERE USERNAME=@USERNAME and ENTERPRISE_ID=@ENTERPRISE_ID and MGRAM_MSG_ID=@MGRAM_MSG_ID";
           
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@USERNAME", _getmGramMessagesReq.UserName);
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _getmGramMessagesReq.CompanyId);
            cmd.Parameters.AddWithValue("@MGRAM_MSG_ID", _getmGramMessagesReq.Message_ID);

            DataSet ds = MSSqlDatabaseClient.SelectDataFromSQlCommand(MficientConstants.strmGramConection, cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                   strQuery= Convert.ToString(dr["MESSAGE"]);
                }
            }
            else
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            return new getmGramCompleteMessagesResp(_respStatus, strQuery, _getmGramMessagesReq.RequestId);
        }
    }
}