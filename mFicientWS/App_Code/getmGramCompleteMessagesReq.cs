﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class getmGramCompleteMessagesReq
    {
        string _requestId, _companyId, _userName, _message_ID;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string Message_ID
        {
            get { return _message_ID; }
        }

        public getmGramCompleteMessagesReq(string requestJson)
        {
            RequestJsonParsing<getmGramCompleteMessagesReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<getmGramCompleteMessagesReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MGRAM_MESSAGE_GET)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userName = objRequestJsonParsing.req.data.unm;
            if (_userName.Contains("\\"))
            {
                _userName = _userName.Split('\\')[1];
            }
            _message_ID = objRequestJsonParsing.req.data.msgid;
        }

        [DataContract]
        public class getmGramCompleteMessagesReqData : Data
        {
            [DataMember]
            public string msgid { get; set; }
        }
    }
}