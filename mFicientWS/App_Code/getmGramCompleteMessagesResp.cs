﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class getmGramCompleteMessagesResp
    { ResponseStatus _respStatus;
        string _msg;
        string _requestId;
        public getmGramCompleteMessagesResp(ResponseStatus respStatus, string msg, string requestId)
        {
            _respStatus = respStatus;
            _msg = msg;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            getmGramCompleteMessagesResponse objgetmGramMessagesResp = new getmGramCompleteMessagesResponse();
            objgetmGramMessagesResp.func = Convert.ToString((int)FUNCTION_CODES.MGRAM_MESSAGE_GET);
            objgetmGramMessagesResp.rid = _requestId;
            objgetmGramMessagesResp.status = _respStatus;
            objgetmGramMessagesResp.data = new getmGramCompleteMessagesRespData();
            objgetmGramMessagesResp.data.msg = _msg;
            string strJsonWithDetails = Utilities.SerializeJson<getmGramCompleteMessagesResponse>(objgetmGramMessagesResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class getmGramCompleteMessagesResponse : CommonResponse
    {
        public getmGramCompleteMessagesResponse()
        { }
        [DataMember]
        public getmGramCompleteMessagesRespData data { get; set; }
    }

    public class getmGramCompleteMessagesRespData
    {
        /// <summary>
        /// Menu Category Details
        /// </summary>
        [DataMember]
        public string msg { get; set; }
    }
}