﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace mFicientWS
{
    public class getmGramMessages
    {
        getmGramMessagesReq _getmGramMessagesReq;
        public getmGramMessages(getmGramMessagesReq getmGramMessagesReq)
        {
            _getmGramMessagesReq = getmGramMessagesReq;
        }

        public getmGramMessagesResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            string strQuery = @"SELECT [MGRAM_MSG_ID],[ENTERPRISE_ID],[PICK_UP_TIME],[CATEGORY],[USERNAME]
                                    ,[SEND_DATETIME],[STATUS],[MESSAGE],[EXPIRATION_DATETIME],[SENT_DATETIME] 
                                    FROM TBL_MGRAM_MSG WHERE USERNAME=@USERNAME and ENTERPRISE_ID=@ENTERPRISE_ID and SEND_DATETIME<=" + DateTime.UtcNow.Ticks + "";
            if (_getmGramMessagesReq.Last_Date_time > 0)
            {
                strQuery += " and SEND_DATETIME>" + _getmGramMessagesReq.Last_Date_time.ToString() ;
            }
            strQuery += " order by SEND_DATETIME desc";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@USERNAME", _getmGramMessagesReq.UserName);
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _getmGramMessagesReq.CompanyId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSQlCommand(MficientConstants.strmGramConection, cmd);
            List<MgramMessage> msgs = new List<MgramMessage>();
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                string strDeliveredMsgId = "";
                string strExpiredMsgId = "";
                long lngDateTimeNow = DateTime.UtcNow.Ticks;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    MgramMessage msg = new MgramMessage();
                    msg.cat = Convert.ToString(dr["CATEGORY"]);
                    msg.msg = Convert.ToString(dr["MESSAGE"]);

                    if (msg.msg.Length > 45)
                        msg.msg = msg.msg.Substring(0, 45) + "...";

                    msg.msgid = Convert.ToString(dr["MGRAM_MSG_ID"]);
                    msg.sts = Convert.ToString(dr["STATUS"]);
                    msg.dttm = Convert.ToString(dr["SEND_DATETIME"]);
                    msg.edt = Convert.ToString(dr["EXPIRATION_DATETIME"]);
                    if (Convert.ToInt32(msg.sts) == (int)MGRAM_MESSAGE_STATUS.PENDING)
                    {
                        if (Convert.ToInt64(dr["EXPIRATION_DATETIME"]) < lngDateTimeNow)
                        {
                            strExpiredMsgId += ",'" + msg.msgid + "'";
                            msg.sts = ((int)MGRAM_MESSAGE_STATUS.EXPIRED).ToString();
                        }
                        else
                        {
                            strDeliveredMsgId += ",'" + msg.msgid + "'";
                            msg.sts = ((int)MGRAM_MESSAGE_STATUS.DELIVERED).ToString();
                        }
                    }
                    msgs.Add(msg);

                }
                strQuery = "";
                if (!string.IsNullOrEmpty(strExpiredMsgId))
                {
                    strQuery = "update TBL_MGRAM_MSG set STATUS=" + ((int)MGRAM_MESSAGE_STATUS.EXPIRED).ToString() + " , SENT_DATETIME=" + lngDateTimeNow.ToString() + " where MGRAM_MSG_ID in (" + strExpiredMsgId.Substring(1) + ") and STATUS=0 and  USERNAME=@USERNAME and ENTERPRISE_ID=@ENTERPRISE_ID ;";
                }
                if (!string.IsNullOrEmpty(strDeliveredMsgId))
                {
                    strQuery += "update TBL_MGRAM_MSG set STATUS=" + ((int)MGRAM_MESSAGE_STATUS.DELIVERED).ToString() + " , SENT_DATETIME=" + lngDateTimeNow.ToString() + " where MGRAM_MSG_ID in (" + strDeliveredMsgId.Substring(1) + ") and STATUS=0 and  USERNAME=@USERNAME and ENTERPRISE_ID=@ENTERPRISE_ID ;";
                }
                if (!string.IsNullOrEmpty(strQuery))
                {
                    cmd = new SqlCommand(strQuery);
                    cmd.Parameters.AddWithValue("@USERNAME", _getmGramMessagesReq.UserName);
                    cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _getmGramMessagesReq.CompanyId);
                    MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd, MficientConstants.strmGramConection);
                }
            }
            else
            {
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }

            return new getmGramMessagesResp(_respStatus, msgs, _getmGramMessagesReq.RequestId);
        }
    }
}