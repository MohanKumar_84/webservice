﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace mFicientWS
{
    #region app Classes

    [DataContract]
    public class jqueryAppBasicClass
    {
        [DataMember]
        public string startupProcMode { get; set; }
        [DataMember]
        public string runsOn { get; set; }
        [DataMember]
        public string diffInterfaceForTab { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string version { get; set; }
        [DataMember]
        public string modelType { get; set; }
        [DataMember]
        public string designMode { get; set; }
        [DataMember]
        public string type { get; set; }

    }
    [DataContract]
    public class jqueryAppClass : jqueryAppBasicClass
    {
        [DataMember]
        public List<ConditionView> conditionViews { get; set; }
        [DataMember]
        public string startUpViewId { get; set; }
        [DataMember]
        public string startUpFormId { get; set; }
        [DataMember]
        public Tasks startUpTasks { get; set; }
        [DataMember]
        public List<Appviews> views { get; set; }
        [DataMember]
        public List<AppForm> forms { get; set; }


        [DataMember]
        public string availableOrNotCriteria { get; set; }
        [DataMember]
        public string availabilityDays { get; set; }
        [DataMember]
        public string availabilityFromTimeHr { get; set; }
        [DataMember]
        public string availabilityFromTimeMin { get; set; }
        [DataMember]
        public string availabilityToTimeHr { get; set; }
        [DataMember]
        public string availabilityToTimeMin { get; set; }
        [DataMember]
        public string mobNetworkAllowed { get; set; }
        [DataMember]
        public object availabilityWiFi { get; set; }
    }
    [DataContract]
    public class ConditionView
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string sourceId { get; set; }

        [DataMember]
        public string name { get; set; }

        [DataMember]
        public location uiPosition { get; set; }

        [DataMember]
        public destinationViewCond onCondition { get; set; }
    }

    [DataContract]
    public class AppCommands
    {
        [DataMember]
        public string bindObjType { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string dsName { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public List<DatabindingObjParams> cmdParams { get; set; }
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string tempUiIndex { get; set; }
        [DataMember]
        public string offlineTblCmdType { get; set; }
        [DataMember]
        public ConditionPlugin condLogic { get; set; }
        [DataMember]
        public bool ignoreCache { get; set; }
    }

    [DataContract]
    public class Appviews
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string longTitle { get; set; }
        [DataMember]
        public string formName { get; set; }
        [DataMember]
        public string formId { get; set; }
        [DataMember]
        public location uiPosition { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string retainValue { get; set; }
        [DataMember]
        public string version { get; set; }
        [DataMember]
        public List<ViewDestination> destinations { get; set; }
        [DataMember]
        public string backNavigation { get; set; }
        [DataMember]
        public string backButton { get; set; }
        [DataMember]
        public string cancelButton { get; set; }
        [DataMember]
        public string nextButton { get; set; }
        [DataMember]
        public string nextBtnLabel { get; set; }
        [DataMember]
        public string cancelBtnLabel { get; set; }
        [DataMember]
        public string rowClickAction { get; set; }
        [DataMember]
        public string onExitProcMode { get; set; }
        [DataMember]
        public string rowClickProcMode { get; set; }
        [DataMember]
        public string cancelTaskProcMode { get; set; }
        [DataMember]
        public string actionBtn1ProcMode { get; set; }
        [DataMember]
        public string actionBtn2ProcMode { get; set; }
        [DataMember]
        public string actionBtn3ProcMode { get; set; }
        [DataMember]
        public destinationViewCond destViewCond { get; set; }
        [DataMember]
        public List<Initialization> onEnterInits { get; set; }
        [DataMember]
        public List<ControlInits> childViewEnterInits { get; set; }
        [DataMember]
        public Tasks onExitCmds { get; set; }
        [DataMember]
        public Tasks rowClickTasks { get; set; }
        [DataMember]
        public Tasks cancelTasks { get; set; }
        [DataMember]
        public Tasks actionBtn1Tasks { get; set; }
        [DataMember]
        public Tasks actionBtn2Tasks { get; set; }
        [DataMember]
        public Tasks actionBtn3Tasks { get; set; }
        [DataMember]
        public List<ViewActionButton> viewActionBtns { get; set; }
        [DataMember]
        public List<ViewActionButton> formActionBtns { get; set; }
        [DataMember]
        public string backButtonView { get; set; }
        [DataMember]
        public string cancelButtonPosition { get; set; }
        [DataMember]
        public string nextButtonPosition { get; set; }
        [DataMember]
        public string enableViewActionBtns { get; set; }
        [DataMember]
        public string viewActionBtnsPosition { get; set; }
        [DataMember]
        public string presentation { get; set; }

        [DataMember]
        public List<ViewCustomTrigger> customTriggers { get; set; }
    }

    [DataContract]
    public class ViewDestination
    {
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string destinationId { get; set; }
        [DataMember]
        public destinationViewCond conditionOn { get; set; }
        [DataMember]
        public string evntSrcId { get; set; }
       
    }
    [DataContract]
    public class ViewCustomTrigger
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string onExitTasksProcMode { get; set; }
        [DataMember]
        public Tasks onExitTasks { get; set; }
    }
    [DataContract]
    public class ViewActionButton
    {
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string userDefinedName { get; set; }
        [DataMember]
        public Tasks onExitTasks { get; set; }
    }

    [DataContract]
    public class Tasks
    {

        [DataMember]
        public List<DialogSettings> dialogSettings { get; set; }

        [DataMember]
        public List<AppCommands> databindingObjs { get; set; }

        [DataMember]
        public PushMsgSettings pushMsgSettings { get; set; }
    }

    [DataContract]
    public class DialogSettings
    {
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string msg { get; set; }
        [DataMember]
        public string dismissBtnText { get; set; }
        [DataMember]
        public string proceedBtnText { get; set; }
    }

    [DataContract]
    public class PushMsgSettings
    {
        [DataMember]
        public string sendToType { get; set; }
        [DataMember]
        public string sendTo { get; set; }
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string var1 { get; set; }
        [DataMember]
        public string var2 { get; set; }
        [DataMember]
        public string var3 { get; set; }
        [DataMember]
        public string var4 { get; set; }
        [DataMember]
        public string var5 { get; set; }
        [DataMember]
        public string var6 { get; set; }
    }

    [DataContract]
    public class OnCondition
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public List<ControlInits> cntrlInits { get; set; }
    }

    [DataContract]
    public class destinationViewCond
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public List<destiCond> destinationCond { get; set; }
    }

    [DataContract]
    public class destiCond
    {
        [DataMember]
        public string compareCntrl { get; set; }
        [DataMember]
        public string viewId { get; set; }
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string formName { get; set; }
        [DataMember]
        public string viewName { get; set; }
        [DataMember]
        public string condValue { get; set; }
        [DataMember]
        public string condType { get; set; }
        [DataMember]
        public destinationViewCond destViewCond { get; set; }
        [DataMember]
        public Tasks onExitTasks { get; set; }
    }

    [DataContract]
    public class Initialization
    {
        [DataMember]
        public string sourceId { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string parameter { get; set; }
        [DataMember]
        public List<ControlInits> cntrlInits { get; set; }
    }

    [DataContract]
    public class ControlInits
    {
        [DataMember]
        public string controlName { get; set; }
        [DataMember]
        public string controlType { get; set; }
        [DataMember]
        public List<string> controlValue { get; set; }
    }

    [DataContract]
    public class location
    {
        [DataMember]
        public string top { get; set; }
        [DataMember]
        public string left { get; set; }
    }

    #endregion
    #region form Classes
    [DataContract]
    public class ActionBtnFrmSetting
    {
        [DataMember]
        public string label { get; set; }
        [DataMember]
        public string operationType { get; set; }
    }
    [DataContract]
    public class AppForm
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string name { get; set; }

        [DataMember]
        public List<sectionRowPanels> rowPanels { get; set; }

        [DataMember]
        public string formDescription { get; set; }

        [DataMember]
        public string rptRowClick { get; set; }

        [DataMember]
        public ActionBtnFrmSetting actionBtn1Setting { get; set; }

        [DataMember]
        public ActionBtnFrmSetting actionBtn2Setting { get; set; }

        [DataMember]
        public ActionBtnFrmSetting actionBtn3Setting { get; set; }

        [DataMember]
        public string appId { get; set; }

        [DataMember]
        public string formModelType { get; set; }

        [DataMember]
        public bool isDeleted { get; set; }

        [DataMember]
        public bool isTablet { get; set; }

        [DataMember]
        public bool isChildForm { get; set; }

        [DataMember]
        public ChildFormType childFormType { get; set; }

        [DataMember]
        public string oldName { get; set; }

        [DataMember]
        public bool isNew { get; set; }

        [DataMember]
        public string viewAppear { get; set; }

        [DataMember]
        public string viewReAppear { get; set; }

        [DataMember]
        public string viewDisAppear { get; set; }

        [DataMember]
        public string cancelBtn { get; set; }

        [DataMember]
        public string commonFunc { get; set; }

        [DataMember]
        public string submitBtn { get; set; }

        [DataMember]
        public string backBtn { get; set; }

        [DataMember]
        public string actionSheetBtn { get; set; }
    }

    [DataContract]
    public class ChildFormType
    {
        [DataMember]
        public string idPrefix { get; set; }
        [DataMember]
        public string propPluginPrefix { get; set; }
    }

    [DataContract]
    public class sectionRowPanels
    {
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string userDefinedName { get; set; }
        [DataMember]
        public SectionRowType type { get; set; }
        [DataMember]
        public string conditionalDisplay { get; set; }
        [DataMember]
        public ConditionPlugin condDisplayCntrlProps { get; set; }
        [DataMember]
        public List<SectionColPanels> colmnPanels { get; set; }
    }

    [DataContract]
    public class SectionRowType
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string UiName { get; set; }
    }

    [DataContract]
    public class SectionColPanels
    {
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string display { get; set; }
        [DataMember]
        public string conditions { get; set; }
        [DataMember]
        public bool isDeleted { get; set; }
        [DataMember]
        public List<Controls> controls { get; set; }
    }

    [DataContract]
    public class Controls
    {
        #region comman
        [DataMember]
        public int inline { get; set; }
        [DataMember]
        public string dataMini { get; set; }
        [DataMember]
        public string noOfRecords { get; set; }
        [DataMember]
        public string alignment { get; set; }
        [DataMember]
        public string ver { get; set; }
        [DataMember]
        public string oldName { get; set; }
        [DataMember]
        public bool isDeleted { get; set; }
        [DataMember]
        public bool isNew { get; set; }
        [DataMember]
        public string location { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string iconPosition { get; set; }
        [DataMember]
        public string userDefinedName { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string backNavRefresh { get; set; }
        [DataMember]
        public ControlTypeObject type { get; set; }
        [DataMember]
        public SectionRowType bindType { get; set; }
        [DataMember]
        public List<AppCommands> databindObjs { get; set; }
        [DataMember]
        public string conditionalDisplay { get; set; }
        [DataMember]
        public ConditionPlugin condDisplayCntrlProps { get; set; }
        [DataMember]
        public string labelText { get; set; }
        [DataMember]
        public string placeholder { get; set; }
        [DataMember]
        public string bindValueToProperty { get; set; }
        [DataMember]
        public string bindTextToProperty { get; set; }
        [DataMember]
        public string bindToProperty { get; set; }
        #endregion
        #region spacer
        [DataMember]
        public string height { get; set; }
        #endregion
        #region textbox
        [DataMember]
        public string txtRowsCount { get; set; }
        [DataMember]
        public string maxLength { get; set; }
        [DataMember]
        public string minWidth { get; set; }
        [DataMember]
        public string maskValue { get; set; }
        [DataMember]
        public string dfltValue { get; set; }
        [DataMember]
        public string uiScript { get; set; }
        [DataMember]
        public string displayText { get; set; }
        [DataMember]
        public string required { get; set; }
        [DataMember]
        public string validationType { get; set; }
        [DataMember]
        public string errorMsg { get; set; }
        [DataMember]
        public string regularExp { get; set; }
        [DataMember]
        public string widthInPercent { get; set; }
        [DataMember]
        public string disabled { get; set; }
        #endregion
        #region Label
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string fontAlign { get; set; }
        [DataMember]
        public string fontSize { get; set; }
        [DataMember]
        public string fontStyle { get; set; }
        [DataMember]
        public string wrap { get; set; }
        [DataMember]
        public string titlePosition { get; set; }
        [DataMember]
        public string backgroundColor { get; set; }
        [DataMember]
        public string fontColor { get; set; }
        [DataMember]
        public string subSliceTitle { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string border { get; set; }
        [DataMember]
        public string borderColor { get; set; }
        [DataMember]
        public string underline { get; set; }
        [DataMember]
        public List<LabelParameterFromating> labelDataObjs { get; set; }
        [DataMember]
        public int hideIfBlank { get; set; }
        #endregion
        #region DateTimePicker
        [DataMember]
        public string datePickType { get; set; }
        #endregion
        #region Slider
        [DataMember]
        public string minLeng { get; set; }
        [DataMember]
        public string maxLeng { get; set; }
        [DataMember]
        public string minVal { get; set; }
        [DataMember]
        public string maxVal { get; set; }
        [DataMember]
        public string incrementOnScroll { get; set; }
        [DataMember]
        public string manualEntry { get; set; }
        [DataMember]
        public string value { get; set; }
        #endregion
        #region Image
        [DataMember]
        public int imageBindType { get; set; }
        [DataMember]
        public int imageFixedType { get; set; }
        [DataMember]
        public string imageSource { get; set; }
        [DataMember]
        public string imgAlignment { get; set; }
        [DataMember]
        public string imageWidth { get; set; }
        [DataMember]
        public string mPlugin { get; set; }
        [DataMember]
        public int cache { get; set; }
        #endregion
        #region Toggle
        [DataMember]
        public string switchText { get; set; }
        [DataMember]
        public string defaultState { get; set; }
        [DataMember]
        public string customOnText { get; set; }
        [DataMember]
        public string customOffText { get; set; }
        [DataMember]
        public List<string> refreshControls { get; set; }
        #endregion
        #region Checkbox
        [DataMember]
        public string checkedVal { get; set; }
        #endregion
        #region PieChart
        [DataMember]
        public string chartTitle { get; set; }
        [DataMember]
        public string dataLblThreshold { get; set; }
        [DataMember]
        public string valueAs { get; set; }
        [DataMember]
        public string fillColor { get; set; }

        [DataMember]
        public string animation { get; set; }
        #endregion
        #region barchart
        [DataMember]
        public string xAxisLabel { get; set; }
        [DataMember]
        public string yAxisLabel { get; set; }
        [DataMember]
        public string barGraphType { get; set; }
        [DataMember]
        public string barGraphDir { get; set; }
        [DataMember]
        public string noOfSeries { get; set; }
        [DataMember]
        public string noOfDecimalPoint { get; set; }
        [DataMember]
        public string xAxisData { get; set; }
        [DataMember]
        public List<SeriesObject> yAxisDataObjs { get; set; }
        #endregion
        #region EditableList
        [DataMember]
        public string addBtnText { get; set; }
        [DataMember]
        public string addNextBtnText { get; set; }
        [DataMember]
        public string addFormTitle { get; set; }
        [DataMember]
        public string editFormTitle { get; set; }
        [DataMember]
        public string viewFormTitle { get; set; }
        [DataMember]
        public string emptyListMsg { get; set; }
        [DataMember]
        public string titlePrefix { get; set; }
        [DataMember]
        public string titleSuffix { get; set; }
        [DataMember]
        public string editNextBtnText { get; set; }
        [DataMember]
        public string titleMultFactor { get; set; }
        [DataMember]
        public string additionalTitle { get; set; }
        [DataMember]
        public string additionalTitlePrefix { get; set; }
        [DataMember]
        public string additionalTitleSuffix { get; set; }
        [DataMember]
        public string additionalTitleMultFactor { get; set; }
        [DataMember]
        public string subTitle { get; set; }
        [DataMember]
        public string subTitlePrefix { get; set; }
        [DataMember]
        public string subTitlePrefixSuffix { get; set; }
        [DataMember]
        public string subTitleMultFactor { get; set; }
        [DataMember]
        public string additionalSubTitle { get; set; }
        [DataMember]
        public string additionalSubTitlePrefix { get; set; }
        [DataMember]
        public string additionalSubTitleSuffix { get; set; }
        [DataMember]
        public string additionalSubTitleMultFactr { get; set; }
        [DataMember]
        public string allowItemAddition { get; set; }
        [DataMember]
        public string allowItemEditing { get; set; }
        [DataMember]
        public string allowItemDelete { get; set; }
        [DataMember]
        public string minItems { get; set; }
        [DataMember]
        public string maxItems { get; set; }
        [DataMember]
        public string addConfirmation { get; set; }
        [DataMember]
        public List<EditableListItem> properties { get; set; }
        #endregion
        #region RADIO BUTTON
        [DataMember]
        public List<ControlOption> options { get; set; }
        [DataMember]
        public string style { get; set; }
        #endregion
        #region ddl
        [DataMember]
        public string promptText { get; set; }
        [DataMember]
        public string promptValue { get; set; }
        [DataMember]
        public string optionText { get; set; }
        [DataMember]
        public string optionValue { get; set; }
        #endregion
        #region table
        [DataMember]
        public string allowSorting { get; set; }
        [DataMember]
        public string allowPlot { get; set; }
        [DataMember]
        public string allowGrouping { get; set; }
        [DataMember]
        public string allowFilter { get; set; }
        [DataMember]
        public string showRowNo { get; set; }
        [DataMember]
        public string displayRowCount { get; set; }
        [DataMember]
        public List<TableColumns> columns { get; set; }
        #endregion
        #region BCODE
        [DataMember]
        public string rowsCount { get; set; }
        [DataMember]
        public string barcodeType { get; set; }
        #endregion
        #region Location
        [DataMember]
        public string fetchLocation { get; set; }
        [DataMember]
        public string allowUsages { get; set; }//
        [DataMember]
        public string isHidden { get; set; }//isHidden
        [DataMember]
        public string validationVal { get; set; }
        #endregion
        #region list
        [DataMember]
        public List<ListManualData> manualData { get; set; }
        [DataMember]
        public string headerText { get; set; }
        [DataMember]
        public string footerText { get; set; }
        [DataMember]
        public string listType { get; set; }
        [DataMember]
        public string thickness { get; set; }
        [DataMember]
        public string borderStyle { get; set; }
        [DataMember]
        public int donutStyle { get; set; }
        [DataMember]
        public string innerRadius { get; set; }
        [DataMember]
        public string minDate { get; set; }

        [DataMember]
        public string maxDate { get; set; }
        [DataMember]
        public string dataInset { get; set; }
        [DataMember]
        public string dataGrouping { get; set; }
        [DataMember]
        public string countBubble { get; set; }
        [DataMember]
        public string selectedRowStyle { get; set; }
        [DataMember]
        public string titleColor { get; set; }
        [DataMember]
        public string actionButton { get; set; }
        [DataMember]
        public ActionButtonSetting actionBtn1Settings { get; set; }
        [DataMember]
        public ActionButtonSetting actionBtn2Settings { get; set; }
        [DataMember]
        public ActionButtonSetting actionBtn3Settings { get; set; }
        [DataMember]
        public string rowClickable { get; set; }
        [DataMember]
        public string caption { get; set; }
        [DataMember]
        public string enablePaging { get; set; }
        [DataMember]
        public string rowId { get; set; }
        [DataMember]
        public string subtitle { get; set; }
        [DataMember]
        public string information { get; set; }
        [DataMember]
        public string additionalInfo { get; set; }
        [DataMember]
        public string imageName { get; set; }
        [DataMember]
        public string countField { get; set; }
        [DataMember]
        public string selectedRow { get; set; }
        [DataMember]
        public string noOfAxis { get; set; }
        [DataMember]
        public List<AngularAxis> axisObjs { get; set; }
        #endregion
        #region Editable childForms
        [DataMember]
        public List<AppForm> childForms { get; set; }
        #endregion
        #region angular chart
        [DataMember]
        public string needleColor { get; set; }
        [DataMember]
        public string needleRadius { get; set; }
        #endregion
        #region picture / scribble
        [DataMember]
        public string maxHeight { get; set; }
        [DataMember]
        public string maxWidth { get; set; }
        [DataMember]
        public string width { get; set; }
        [DataMember]
        public string filePath { get; set; }
        [DataMember]
        public string uploadType { get; set; }
        [DataMember]
        public string bucketName { get; set; }
        [DataMember]
        public string credential { get; set; }
        [DataMember]
        public string penColor { get; set; }
        [DataMember]
        public string multipleOf { get; set; }
        [DataMember]
        public string cropping { get; set; }
        #endregion
        [DataMember]
        public string onChange { get; set; }
        [DataMember]
        public string onClear { get; set; }
        [DataMember]
        public string onFocus { get; set; }
        [DataMember]
        public string onBlur { get; set; }
        [DataMember]
        public string onTouchScript { get; set; }
        [DataMember]
        public string onScan { get; set; }
        [DataMember]
        public string inset { get; set; }
        [DataMember]
        public string scriptListRowTouch { get; set; }
        [DataMember]
        public string scriptBtnCntrl { get; set; }
    }
    [DataContract]
    public class ListManualData
    {
        [DataMember]
        public string rowId { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string description { get; set; }
    }
    //angular axis Class
    [DataContract]
    public class AngularAxis
    {
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string tickInterval { get; set; }
        [DataMember]
        public string startVal { get; set; }
        [DataMember]
        public string endVal { get; set; }
        [DataMember]
        public List<AngularBand> bands { get; set; }
    }
    [DataContract]
    public class AngularBand
    {
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string startVal { get; set; }
        [DataMember]
        public string color { get; set; }
        [DataMember]
        public string innerRadius { get; set; }
    }
    //ActionButtonSetting
    [DataContract]
    public class ActionButtonSetting
    {
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string refreshList { get; set; }
        [DataMember]
        public string color { get; set; }
        [DataMember]
        public string actionType { get; set; }
    }

    [DataContract]
    public class DatabindingObjParams
    {
        [DataMember]
        public string para { get; set; }
        [DataMember]
        public string paraName { get; set; }
        [DataMember]
        public string dataType { get; set; }
        [DataMember]
        public string dataFormat { get; set; }
        [DataMember]
        public string noOfDecimalPlaces { get; set; }
        [DataMember]
        public string mulFactor { get; set; }
        [DataMember]
        public string selectedCol { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string val { get; set; }
    }
    [DataContract]
    public class Advance
    {
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string value { get; set; }
    }
    [DataContract]
    public class ConditionPlugin
    {
        public ConditionPlugin(Advance _adv)
        {
        }
        [DataMember]
        public string matchingType { get; set; }
        [DataMember]
        public List<ConditionList> conditions { get; set; }
    }

    [DataContract]
    public class ConditionList
    {
        public ConditionList(Advance _adv)
        {
            this.FirstCol = _adv.text;
            this.SecondCol = _adv.text;
            this.ThirdCol = _adv.value;
        }
        [DataMember]
        public string FirstCol { get; set; }
        [DataMember]
        public string SecondCol { get; set; }
        [DataMember]
        public string ThirdCol { get; set; }
    }

    [DataContract]
    public class LabelParameterFromating
    {
        [DataMember]
        public string para { get; set; }
        [DataMember]
        public string dataType { get; set; }
        [DataMember]
        public string noOfDecimalPlaces { get; set; }
        [DataMember]
        public string mulFactor { get; set; }
        [DataMember]
        public string selectedCol { get; set; }
    }

    [DataContract]
    public class EditableListItem
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string mappedDataCol { get; set; }
        [DataMember]
        public string unique { get; set; }
        [DataMember]
        public string disp { get; set; }
    }

    [DataContract]
    public class ControlOption
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string value { get; set; }
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string isDefault { get; set; }
        [DataMember]
        public string count { get; set; }
    }//
    [DataContract]
    public class SeriesObject
    {
        [DataMember]
        public string label { get; set; }
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string value { get; set; }
        [DataMember]
        public string scaleFactor { get; set; }
    }
    [DataContract]
    public class ControlTypeObject
    {
        [DataMember]
        public string idPrefix { get; set; }
        [DataMember]
        public string propPluginPrefix { get; set; }
        [DataMember]
        public string UIName { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public ControlTypePropForIntlSense propForIntlSense { get; set; }
    }
    [DataContract]
    public class ControlTypePropForIntlSense
    {
        [DataMember]
        public string prop { get; set; }
        [DataMember]
        public string val { get; set; }
        [DataMember]
        public string category { get; set; }
    }
    [DataContract]
    public class TableColumns
    {
        [DataMember]
        public string column { get; set; }
        [DataMember]
        public string index { get; set; }
        [DataMember]
        public string header { get; set; }
        [DataMember]
        public string datatype { get; set; }
        [DataMember]
        public string displayFormat { get; set; }
        [DataMember]
        public string dataFormat { get; set; }
        [DataMember]
        public string prefix { get; set; }
        [DataMember]
        public string suffix { get; set; }
        [DataMember]
        public string factor { get; set; }

    }
    #endregion
    public enum ViewTaskType
    {
        NEXT = 1,
        ROWCLICK = 2,
        ACTIONBUTTON1 = 3,
        ACTIONBUTTON2 = 4,
        ACTIONBUTTON3 = 5,
        VIEWACTIONBUTTON = 7,
        FORMACTIONBUTTON = 9
    }
}