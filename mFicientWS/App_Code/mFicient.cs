﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using Ionic.Zip;
using System.Xml.Serialization;
using System.Xml;


namespace mFicientWS
{
    public class mFicient : IHttpModule
    {
        private static int[] FUNCTIONS_WITHOUT_SESSION = {  (int)FUNCTION_CODES.COMPANY_DETAIL,(int)FUNCTION_CODES.RESET_PASSWORD,(int)FUNCTION_CODES.MOBILE_USER_LOGIN,
                                                             (int)FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST,(int)FUNCTION_CODES.RENEW_SESSION ,(int)FUNCTION_CODES.DESKTOP_USER_LOGIN
                                                         ,(int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED};
        private static int[] FUNCTIONS_WITHOUT_USER = { };


        #region IHttpModule Members

        public void Dispose()
        {
            /*clean up*/
        }

        public void Init(HttpApplication context)
        {

            context.BeginRequest += new EventHandler(Application_BeginRequest);
            context.EndRequest += new EventHandler(Application_EndRequest);

        }

        #endregion

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            string strRequestID = string.Empty, strUserName = string.Empty, strDomainName = string.Empty, strUserId = string.Empty, modelType = string.Empty;

            string strIPAddress = string.Empty, strHostName = string.Empty, strDeviceToken = string.Empty, strDeviceType = string.Empty, strServerId = string.Empty;

            int intHttpStatusCode = (int)HttpStatusCode.OK;

            int intFunctionCode = 0;
            string cid = "";
            HttpContext appContext;
            string strSessionID = string.Empty;

            appContext = ((HttpApplication)sender).Context;

            if (appContext.Request.FilePath.ToLower() != MficientConstants.FilePathOfDefaultPage.ToLower() && appContext.Request.FilePath.ToLower() != MficientConstants.FilePathOfObject.ToLower())
            {
                return;
            }

            string strResponseXml = string.Empty, dataJson = string.Empty;
            ZipFile zipFile = null;
            object objRequest;
            try
            {
                if (appContext.Request.HttpMethod == "GET")
                {
                    dataJson = appContext.Request.QueryString.Get("d");
                }
                else if (appContext.Request.HttpMethod == "POST")
                {
                    dataJson = appContext.Request.Form.Get("d");
                }
                else
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                if (string.IsNullOrEmpty(dataJson))
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                if (appContext.Request.FilePath.ToLower() == MficientConstants.FilePathOfObject.ToLower())
                {
                    intFunctionCode = (int)FUNCTION_CODES.DO_TASK;
                    strRequestID = DateTime.UtcNow.Ticks.ToString();
                    DoTaskReq req = new DoTaskReq(dataJson, strRequestID, out cid);
                    objRequest = req;
                    appContext.Items.Add("FUNCTION_CODE", intFunctionCode);
                    appContext.Items.Add("REQUEST_ID", strRequestID);
                    appContext.Items.Add("COMPANY_ID", cid);
                    appContext.Items.Add("SESSION_ID", strSessionID);
                }
                else
                {
                    //string cid = "";
                    GetAuthenticationParams(dataJson, out intFunctionCode, out strUserName, out strRequestID, out strSessionID, out strServerId, out strDeviceToken, out strDeviceType, out cid, out strDomainName);//Check And Get Values Of FunctionCode,RequestId,Email

                    strIPAddress = appContext.Request.UserHostAddress;
                    strHostName = appContext.Request.UserHostName;

                    if (!Enum.IsDefined(typeof(FUNCTION_CODES), intFunctionCode))
                    {
                        intHttpStatusCode = (int)HttpStatusCode.NotImplemented;
                        return;
                    }

                    appContext.Items.Add("FUNCTION_CODE", intFunctionCode);
                    appContext.Items.Add("REQUEST_ID", strRequestID);
                    appContext.Items.Add("COMPANY_ID", cid);
                    appContext.Items.Add("SESSION_ID", strSessionID);

                    //check added on 2-7-2012
                    if (intFunctionCode.ToString().StartsWith("1"))
                    {
                        if (string.IsNullOrEmpty(cid))
                        {
                            intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                            return;
                        }
                        if (intFunctionCode != (int)FUNCTION_CODES.COMPANY_DETAIL && intFunctionCode != (int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED)
                        {
                            if (string.IsNullOrEmpty(strUserName) && !FUNCTIONS_WITHOUT_USER.Contains(intFunctionCode))
                            {
                                intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                                return;
                            }

                            //====BEGIN CODE - NEW CODE USING CACHE=========================
                            SessionUser session;

                            AUTHENTICATION_RESULT authResult = authenticateUserFromCache(strUserName, cid, strSessionID, out session, strDeviceType, strDeviceToken, intFunctionCode, out modelType);

                            if (authResult != AUTHENTICATION_RESULT.CORRECT)
                            {
                                if (authResult == AUTHENTICATION_RESULT.SESSION_TIMEOUT)
                                {
                                    intHttpStatusCode = (int)HttpStatusCode.Forbidden;
                                    return;
                                }
                                intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                                return;
                            }
                            if (IsFunctionWithSessionID(intFunctionCode))
                            {
                                appContext.Items.Add("USER_ID", session.UserId);
                                strUserId = session.UserId;
                                //===========================================
                                if (!String.IsNullOrEmpty(session.UserId))
                                {
                                    strResponseXml = getPreviousResponseFromCache(cid, strUserId, strSessionID, strRequestID);
                                    if (!string.IsNullOrEmpty(strResponseXml))
                                    {
                                        intHttpStatusCode = (int)HttpStatusCode.Accepted;
                                        return;
                                    }
                                }
                            }
                        }


                    }
                    else if (intFunctionCode.ToString().StartsWith("2"))
                    {
                        if (!Utilities.IsmBuzzServerIDExist(strServerId))
                        {
                            intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                            return;
                        }
                    }

                    objRequest = GetRequestObject(intFunctionCode, dataJson, strIPAddress, strHostName, strUserId, strDomainName, modelType);
                }

                if (objRequest == null) throw new Exception(MficientConstants.INVALID_REQUEST_JSON.ToString());

                if (intFunctionCode == (int)FUNCTION_CODES.GET_APP_FILES_AS_ZIPPED)
                {
                    try
                    {
                        GetZippedAppFilesReq objGetZippedAppFilesReq = (GetZippedAppFilesReq)objRequest;
                        AppDownloadinZip fileZipper = new AppDownloadinZip(objGetZippedAppFilesReq, appContext);
                        zipFile = fileZipper.ZippedUIFiles();
                    }
                    catch (Exception ex)
                    {
                        if (int.Parse(ex.Message) == (int)HttpStatusCode.Unauthorized) intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                        throw ex;
                    }
                }
                else if (intFunctionCode == (int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED)
                {
                    try
                    {
                        EnterpriseLogoZipper objImagesZipper = new EnterpriseLogoZipper((EnterpriseLogoReq)objRequest, appContext);
                        zipFile = objImagesZipper.ZippedUIFiles();
                    }
                    catch (Exception ex)
                    {
                        if (int.Parse(ex.Message) == (int)HttpStatusCode.Unauthorized) intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                        throw ex;
                    }
                }
                else
                {

                    strResponseXml = DoWebServiceAction(objRequest, intFunctionCode, appContext);
                    if (strResponseXml == string.Empty)
                        throw new Exception();
                    if (intFunctionCode == (int)FUNCTION_CODES.SUPPORT_QUERY ||
                        intFunctionCode == (int)FUNCTION_CODES.COMMENT_AND_UPDATE_QUERY)
                    {
                        int iResponseXml;
                        if (int.TryParse(strResponseXml, out iResponseXml))
                        {
                            if (iResponseXml == (int)HttpStatusCode.Accepted)
                            {
                                intHttpStatusCode = iResponseXml;
                                strResponseXml = String.Empty;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                int intParseResult = 0;
                if (int.TryParse(ex.Message, out intParseResult))
                {
                    /**
                     * Never throw a httpStatusCode from the code.
                     * The only case where httpStatusCode is thrown is from DoTaskWebService
                     * Where we are calling other webservice from our code
                     * in this case we have to return the status code returned from the other webservice
                     * if it is not OK.Only in that case HttpStatusCode is thrown as exception message.
                     * 
                     * **/
                    if (Enum.IsDefined(typeof(HttpStatusCode), intParseResult)) intHttpStatusCode = intParseResult;
                    else if (intParseResult == (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR) intHttpStatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    else if (intParseResult == (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR) intHttpStatusCode = (int)HttpStatusCode.NoContent;
                    else if (intParseResult == MficientConstants.INVALID_REQUEST_JSON) intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    else if (intParseResult == (int)HttpStatusCode.Unauthorized) intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                    else intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                }
                else
                {
                    intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                }
            }
            finally
            {
                appContext.Items.Add("HTTP_STATUS_CODE", intHttpStatusCode);
                //if (intFunctionCode == (int)HttpStatusCode.Forbidden)
                //{
                //    appContext.Items.Add("RESPONSE_XML", result);
                //}
                if (intFunctionCode == (int)FUNCTION_CODES.GET_APP_FILES_AS_ZIPPED ||
                     intFunctionCode == (int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED)
                {
                    if (zipFile != null)
                        appContext.Items.Add("RESPONSE_ZIPFILE", zipFile);
                }
                else
                {
                    appContext.Items.Add("RESPONSE_XML", strResponseXml);
                }
            }
        }

        private void Application_EndRequest(object sender, EventArgs e)
        {
            //intFunctionCode was moved to class level scope.
            //Used in both application_beginrequest and application_endrequest
            string strUserID = string.Empty, strRequestID = string.Empty, strResponse = string.Empty;
            int intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
            ZipFile zipFile = null;
            int intFunctionCode = 0;
            string cid = string.Empty;
            string strSessionID = string.Empty;


            HttpContext appContext = ((HttpApplication)sender).Context;

            try
            {
                intHttpStatusCode = Convert.ToInt32(appContext.Items["HTTP_STATUS_CODE"]);
            }
            catch
            {
                intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
            }

            if (intHttpStatusCode == (int)HttpStatusCode.OK || intHttpStatusCode == (int)HttpStatusCode.Accepted)
            {

                intFunctionCode = Convert.ToInt32(appContext.Items["FUNCTION_CODE"]);

                if (intFunctionCode == (int)FUNCTION_CODES.GET_APP_FILES_AS_ZIPPED ||
                    intFunctionCode == (int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED)
                {
                    try
                    {
                        zipFile = (ZipFile)appContext.Items["RESPONSE_ZIPFILE"];
                    }
                    catch
                    {
                    }
                    if (zipFile != null)
                    {
                        appContext.Response.ContentType = @"application/zip";
                        appContext.Response.AddHeader("content-disposition", "filename=" + Convert.ToString(appContext.Items["ZIP_FILE_NAME"]).ToUpper() + ".zip");
                        zipFile.Save(appContext.Response.OutputStream);
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
                else
                {
                    try
                    {
                        strRequestID = appContext.Items["REQUEST_ID"].ToString();
                        strResponse = appContext.Items["RESPONSE_XML"].ToString();
                        cid = Convert.ToString(appContext.Items["COMPANY_ID"]);
                        strSessionID = Convert.ToString(appContext.Items["SESSION_ID"]);
                        if (IsFunctionWithSessionID(intFunctionCode) && intFunctionCode.ToString().StartsWith("1"))
                        {
                            strUserID = appContext.Items["USER_ID"].ToString();

                            if (intHttpStatusCode == (int)HttpStatusCode.OK && !string.IsNullOrEmpty(strResponse))
                            {
                                string strResponseKey = CacheManager.GetKey(
                                    CacheManager.CacheType.mFicientHttpResponse,
                                    cid, strUserID, "", "",
                                    strSessionID, strRequestID,
                                    String.Empty
                                    );

                                if (string.IsNullOrEmpty(CacheManager.Get<string>(strResponseKey, false)))
                                {
                                    CacheManager.Insert<string>(strResponseKey, strResponse, DateTime.UtcNow.AddSeconds(300), TimeSpan.Zero);
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        if (string.IsNullOrEmpty(strResponse)) strResponse = "";
                    }
                }
            }
            try
            {
                appContext.Response.StatusCode = intHttpStatusCode;
                appContext.Response.Write(strResponse);
            }
            catch
            {
            }
        }

        #region PrivateFunctions

        private void GetAuthenticationParams(string _RequestJson, out int _FunctionCode, out string _UserName, out string _RequestID, out string _SessionID, out string _ServerID, out string _DeviceToken, out string _DeviceType, out string _Cid, out string _DomainName)
        {
            _FunctionCode = 0;
            _UserName = string.Empty;
            _RequestID = string.Empty;
            _SessionID = string.Empty;
            _DeviceToken = string.Empty;
            _DeviceType = string.Empty;
            _Cid = string.Empty;
            _ServerID = string.Empty;
            _DomainName = string.Empty;
            if (_RequestJson.Length > 0)
            {
                try
                {
                    RequestJsonParsing<Data> jsonObject = Utilities.DeserialiseJson<RequestJsonParsing<Data>>(_RequestJson);

                    _FunctionCode = Convert.ToInt32(jsonObject.req.func);
                    _RequestID = jsonObject.req.rid != null ? jsonObject.req.rid : "";
                    _SessionID = jsonObject.req.sid != null ? jsonObject.req.sid : "";
                    _DeviceToken = jsonObject.req.did != null ? jsonObject.req.did : "";
                    _DeviceType = jsonObject.req.dtyp != null ? jsonObject.req.dtyp : "";
                    if (jsonObject.req.data != null)
                    {
                        _Cid = jsonObject.req.data.enid != null ? jsonObject.req.data.enid : "";

                        _UserName = jsonObject.req.data.unm != null ? jsonObject.req.data.unm : "";
                        if (!string.IsNullOrEmpty(_UserName) &&
                            ((int)FUNCTION_CODES.MOBILE_USER_LOGIN == _FunctionCode ||
                            (int)FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST == _FunctionCode ||
                            (int)FUNCTION_CODES.CHANGE_PASSWORD == _FunctionCode ||
                            (int)FUNCTION_CODES.RESET_PASSWORD == _FunctionCode))
                        {
                            _UserName = EncryptionDecryption.AESDecrypt(_Cid, jsonObject.req.data.unm);
                        }
                        if (_UserName.Contains("\\"))
                        {
                            _DomainName = _UserName.Split('\\')[0];
                            _UserName = _UserName.Split('\\')[1];
                        }

                        _ServerID = jsonObject.req.data.srvid != null ? jsonObject.req.data.srvid : "";
                    }
                }
                catch
                {
                    throw new Exception(MficientConstants.INVALID_REQUEST_JSON.ToString());
                }
            }
        }

        private void GetAuthenticationParamsForReqWithoutData(string _RequestJson, out int _FunctionCode, out string _RequestID)
        {
            _FunctionCode = 0;
            _RequestID = string.Empty;
            if (_RequestJson.Length > 0)
            {
                RequestJsonParsing jsonObject = Utilities.DeserialiseJson<RequestJsonParsing>(_RequestJson);
                _FunctionCode = int.Parse(jsonObject.req.func);
                _RequestID = jsonObject.req.rid != null ? jsonObject.req.rid : "";
            }
        }
        private bool IsFunctionWithSessionID(int _FunctionCode)
        {
            return !FUNCTIONS_WITHOUT_SESSION.Contains(_FunctionCode);
        }
        private bool isFunctionWithUserDtl(int functionCode)
        {
            return !FUNCTIONS_WITHOUT_USER.Contains(functionCode);
        }

        private object GetRequestObject(int _FunctionCode, string _dataJson, string _IPAddress, string _HostName, string _UserID, string _DomainName, string Model)
        {
            try
            {
                switch (_FunctionCode)
                {
                    case (int)FUNCTION_CODES.MOBILE_USER_LOGIN:
                        return new UserLoginReq(_dataJson, _HostName, _IPAddress);
                    case (int)FUNCTION_CODES.RESET_PASSWORD:
                        return new ResetPasswordReq(_dataJson);
                    case (int)FUNCTION_CODES.CHANGE_PASSWORD:
                        return new ChangePasswordReq(_dataJson, _UserID, _DomainName);
                    case (int)FUNCTION_CODES.DESKTOP_USER_LOGIN:
                        return new DesktopUserLoginReq(_dataJson, _UserID, _HostName, _IPAddress);
                    case (int)FUNCTION_CODES.USER_DETAIL:
                        return new UserDetailReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.RENEW_SESSION:
                        return new RenewSessionReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.LOG_OUT:
                        return new LogOutReq(_dataJson, _UserID, Model);
                    case (int)FUNCTION_CODES.GET_OFFLINE_DATA_TABLE_META:
                        return new Get_Offline_Datatable_Meta_Req(_dataJson);
                    case (int)FUNCTION_CODES.GET_OFFLINE_DATA_OBJECTS:
                        return new Get_Offline_Data__Objects_Req(_dataJson);
                    case (int)FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST:
                        return new DeviceRegistrationRequestReq(_dataJson);
                    case (int)FUNCTION_CODES.DO_TASK:
                        return new DoTaskReq(_dataJson, _UserID, Model);
                    case (int)FUNCTION_CODES.DO_TASK_FOR_BATCH_PROCESS:
                        return new DoTaskReqForBatchProcess(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.COMPANY_DETAIL:
                        return new GetCompanyDetailsReq(_dataJson);
                    case (int)FUNCTION_CODES.USER_AUTHENTICATION:
                        return new UserAuthenticationReq(_dataJson);
                    case (int)FUNCTION_CODES.ADD_CONTACT_REQUEST:
                        return new AddNewContactReq(_dataJson);
                    case (int)FUNCTION_CODES.REMOVE_CONTACT:
                        return new RemoveContactReq(_dataJson);
                    case (int)FUNCTION_CODES.REMOVE_PENDING_REQUEST:
                        return new RemovePendingRequestReq(_dataJson);
                    case (int)FUNCTION_CODES.MBUZZ_COMPANY_USER_LIST:
                        return new mBuzzCompanyUserListReq(_dataJson);
                    case (int)FUNCTION_CODES.ADD_CONTACT_CONFIRMATION:
                        return new ResponseofContactRequestReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.MBUZZ_PUSHMESSAGE_SAVE:
                        return new MbuzzPushMessageSaveReq(_dataJson);
                    case (int)FUNCTION_CODES.GET_MENU_AND_WORKFLOW_LIST:
                        return new Get_Menu_And_Workflows_Req(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.UPDATION_COMPLETE:
                        return new ConfirmMobUpdationReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.CHECK_ACCOUNT_AND_DEVICE:
                        return new CheckAccountAndDeviceReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.GET_QUERY_TYPE:
                        return new GetQueryTypeReq(_dataJson);
                    case (int)FUNCTION_CODES.USER_QUERY_LIST:
                        return new UserQueryListReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.SUPPORT_QUERY:
                        return new SupportQueryReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.USER_QUERY_DETAIL:
                        return new QueryDetailWithCommentsReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.COMMENT_AND_UPDATE_QUERY:
                        return new CommentOnQueryAndChangeStatusReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.GET_MPLUGIN_CONNECTION_DETAILS:
                        return new GetMPlugInConnectionDetailsReq(_dataJson);
                    case (int)FUNCTION_CODES.MPLUGIN_CLIENT_AUTHENTICATION:
                        return new AuthenticateMPluginClientReq(_dataJson);
                    case (int)FUNCTION_CODES.GET_APP_FILES_AS_ZIPPED:
                        return new GetZippedAppFilesReq(_dataJson, _UserID); //<<------------ Add the constructor parameters
                    case (int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED:
                        return new EnterpriseLogoReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.USER_CONTACTS:
                        return new UserContactsReq(_dataJson);
                    case (int)FUNCTION_CODES.WORK_FLOW_USAGE_LOG:
                        return new WorkflowUsageLogReq(_dataJson, _UserID, Model);
                    case (int)FUNCTION_CODES.ADD_DEVICE_PUSH_MSG_ID:
                        return new AddDevicePushMsgIDreq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.MGRAM_MESSAGE_LIST_GET:
                        return new getmGramMessagesReq(_dataJson);
                    case (int)FUNCTION_CODES.MGRAM_MESSAGE_GET:
                        return new getmGramCompleteMessagesReq(_dataJson);
                    case (int)FUNCTION_CODES.MGRAM_MESSAGE_STATUS_UPDATE:
                        return new mGramMessageStatusUpdateReq(_dataJson);
                    case (int)FUNCTION_CODES.MGRAM_ENTERPRISE_DETAIL:
                        return new mGramGetEnterpriseAndMPAgentDetailReq(_dataJson);
                    case (int)FUNCTION_CODES.GET_PAGING_DATA:
                        return new GetPagingDataReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.GET_HTML_FILES_OF_APP_AS_SINGLE_HTML:
                        return new GetHtmlFilesOfAppAsHTMLReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.DOWNLOAD_ONLINE_DATABASE_OFFLINE:
                        return new DownloadDBOnlineForOfflineTableReq(_dataJson, _UserID, Model);
                    case (int)FUNCTION_CODES.UPLOAD_OFFLINE_DATABASE_TO_ONLINE_DATABASE:
                        return new UploadtoOnlineDatabaseReq(_dataJson, _UserID, Model);
                    case (int)FUNCTION_CODES.SAVE_PUSH_MSG_FROM_USER:
                        return new SavePushMsgFromUserReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.CONVERT_LOCAL_TO_PUBLIC_IMAGEURL:
                        return new ConvertLocalToPublicImageURLReq(_dataJson, _UserID);
                    case (int)FUNCTION_CODES.Get_User_Detail:
                        return new GetUserDetailsReq(_dataJson);
                    case (int)FUNCTION_CODES.Webservice_User_List:
                        return new GetWebservice_User_ListReq(_dataJson);
                    case (int)FUNCTION_CODES.USER_GROUP_LIST:
                        return new User_Group_ListReq(_dataJson);
                    case (int)FUNCTION_CODES.BLOCK_UNBLOCK_USER:
                        return new Block_UnblockReq(_dataJson);
                    case (int)FUNCTION_CODES.MODIFY_USER_GROUP:
                        return new ModifyUserfromGroupReq(_dataJson);
                    case (int)FUNCTION_CODES.GET_LIST_OF_USERS_REGISTERED_DEVICE:
                        return new GetListofUserRegistrationReq(_dataJson);
                    case (int)FUNCTION_CODES.APPROVE_DENY_DELETE:
                        return new Approve_Deny_DeleteReq(_dataJson);
                }
                return null;
            }
            catch
            {
                throw new Exception(MficientConstants.INVALID_REQUEST_JSON.ToString());
            }
        }

        private string DoWebServiceAction(object _RequestObject, int _FunctionCode, HttpContext _Context)
        {
            switch (_FunctionCode)
            {
                case (int)FUNCTION_CODES.MOBILE_USER_LOGIN:
                    return Utilities.DoUserLogin(_RequestObject, _Context);
                case (int)FUNCTION_CODES.RESET_PASSWORD:
                    return Utilities.DoResetPassword(_RequestObject, _Context);
                case (int)FUNCTION_CODES.CHANGE_PASSWORD:
                    return Utilities.DoChangePassword(_RequestObject, _Context);
                case (int)FUNCTION_CODES.DESKTOP_USER_LOGIN:
                    return Utilities.DoDesktopUserLogin(_RequestObject);
                case (int)FUNCTION_CODES.USER_DETAIL:
                    return Utilities.DoUserDetail(_RequestObject);
                case (int)FUNCTION_CODES.RENEW_SESSION:
                    return Utilities.DoRenewSession(_RequestObject, HttpRuntime.Cache);
                case (int)FUNCTION_CODES.GET_OFFLINE_DATA_TABLE_META:
                    return Utilities.DoGetOfflineTableMeta(_RequestObject);
                case (int)FUNCTION_CODES.GET_OFFLINE_DATA_OBJECTS:
                    return Utilities.DoGetOfflineObjectMeta(_RequestObject);
                case (int)FUNCTION_CODES.LOG_OUT:
                    return Utilities.DoLogOut(_RequestObject, HttpRuntime.Cache);
                case (int)FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST:
                    return Utilities.DoDeviceRegistrationRequest(_RequestObject, _Context);
                case (int)FUNCTION_CODES.DO_TASK:
                    return Utilities.DoDoTaskRequest(_RequestObject);
                case (int)FUNCTION_CODES.DO_TASK_FOR_BATCH_PROCESS:
                    return Utilities.DoDoTaskRequestForBatchProcess(_RequestObject);
                case (int)FUNCTION_CODES.COMPANY_DETAIL:
                    return Utilities.DoCompanyDetail(_RequestObject);
                case (int)FUNCTION_CODES.USER_AUTHENTICATION:
                    return Utilities.DoUserAuthentication(_RequestObject);
                case (int)FUNCTION_CODES.USER_CONTACTS:
                    return Utilities.DoUserContacts(_RequestObject, HttpRuntime.Cache);
                case (int)FUNCTION_CODES.ADD_CONTACT_REQUEST:
                    return Utilities.DoAddNewContact(_RequestObject);
                case (int)FUNCTION_CODES.REMOVE_CONTACT:
                    return Utilities.DoRemoveContact(_RequestObject, HttpRuntime.Cache);
                case (int)FUNCTION_CODES.REMOVE_PENDING_REQUEST:
                    return Utilities.DoRemovePendingRequest(_RequestObject);
                case (int)FUNCTION_CODES.MBUZZ_COMPANY_USER_LIST:
                    return Utilities.DomBuzzCompanyUserList(_RequestObject);
                case (int)FUNCTION_CODES.UPDATION_COMPLETE:
                    return Utilities.DoConfirmMobUpdation(_RequestObject);
                case (int)FUNCTION_CODES.ADD_CONTACT_CONFIRMATION:
                    return Utilities.DoResponseofContactRequest(_RequestObject, HttpRuntime.Cache);
                case (int)FUNCTION_CODES.MBUZZ_PUSHMESSAGE_SAVE:
                    return Utilities.DoPushMessageforContactStatus(_RequestObject);
                case (int)FUNCTION_CODES.GET_MENU_AND_WORKFLOW_LIST:
                    return Utilities.DoGetMenuAndWorkFlow(_RequestObject);
                case (int)FUNCTION_CODES.CHECK_ACCOUNT_AND_DEVICE:
                    return Utilities.DoCheckAccountAndDevice(_RequestObject);
                case (int)FUNCTION_CODES.GET_QUERY_TYPE:
                    return Utilities.DoGetQueryType(_RequestObject);
                case (int)FUNCTION_CODES.USER_QUERY_LIST:
                    return Utilities.DoUserQueryList(_RequestObject);
                case (int)FUNCTION_CODES.SUPPORT_QUERY:
                    return Utilities.DoSupportQuery(_RequestObject, _Context);
                case (int)FUNCTION_CODES.USER_QUERY_DETAIL:
                    return Utilities.DoQueryDetailWithComments(_RequestObject);
                case (int)FUNCTION_CODES.COMMENT_AND_UPDATE_QUERY:
                    return Utilities.DoCommentOnQueryAndChangeStatus(_RequestObject);
                case (int)FUNCTION_CODES.MPLUGIN_CLIENT_AUTHENTICATION:
                    return Utilities.DoMPlugInClientAuthentication(_RequestObject);
                case (int)FUNCTION_CODES.GET_MPLUGIN_CONNECTION_DETAILS:
                    return Utilities.DoGetMPlugInConnectionDetails(_RequestObject);
                case (int)FUNCTION_CODES.WORK_FLOW_USAGE_LOG:
                    return Utilities.DoWFUsageLog(_RequestObject);
                case (int)FUNCTION_CODES.ADD_DEVICE_PUSH_MSG_ID:
                    return Utilities.DoAddDevPushMsgId(_RequestObject);
                case (int)FUNCTION_CODES.MGRAM_MESSAGE_LIST_GET:
                    return Utilities.DogetmGramMessages(_RequestObject);
                case (int)FUNCTION_CODES.MGRAM_MESSAGE_GET:
                    return Utilities.DogetmGramCompleteMessage(_RequestObject);
                case (int)FUNCTION_CODES.MGRAM_MESSAGE_STATUS_UPDATE:
                    return Utilities.DomGramMessageStatusUpdate(_RequestObject);
                case (int)FUNCTION_CODES.MGRAM_ENTERPRISE_DETAIL:
                    return Utilities.DomGramEnterpriseAndMPAgentDetail(_RequestObject);
                case (int)FUNCTION_CODES.GET_PAGING_DATA:
                    return Utilities.DoGetPagingData(_RequestObject);
                case (int)FUNCTION_CODES.GET_HTML_FILES_OF_APP_AS_SINGLE_HTML:
                    return Utilities.DoGetHtmlFilesForAppAsSingleHTML(_RequestObject);
                case (int)FUNCTION_CODES.SAVE_PUSH_MSG_FROM_USER:
                    return Utilities.DoSavePushMsgFromUser(_RequestObject);
                case (int)FUNCTION_CODES.DOWNLOAD_ONLINE_DATABASE_OFFLINE:
                    return Utilities.DoDownloadDBOnlineForOfflineTable(_RequestObject);
                case (int)FUNCTION_CODES.UPLOAD_OFFLINE_DATABASE_TO_ONLINE_DATABASE:
                    return Utilities.DoUploadOfflineTable(_RequestObject);
                case (int)FUNCTION_CODES.CONVERT_LOCAL_TO_PUBLIC_IMAGEURL:
                    return Utilities.DoConvertLocalToPublicImageURL(_RequestObject);
                case (int)FUNCTION_CODES.Get_User_Detail:
                    return Utilities.GetUserDetail(_RequestObject);
                case (int)FUNCTION_CODES.Webservice_User_List:
                    return Utilities.GetWebserviceUserList(_RequestObject);
                case (int)FUNCTION_CODES.USER_GROUP_LIST:
                    return Utilities.UserGrouplist(_RequestObject);
                case (int)FUNCTION_CODES.BLOCK_UNBLOCK_USER:
                    return Utilities.Block_Unblock(_RequestObject);
                case (int)FUNCTION_CODES.MODIFY_USER_GROUP:
                    return Utilities.ModifyUserforGroup(_RequestObject);
                case (int)FUNCTION_CODES.GET_LIST_OF_USERS_REGISTERED_DEVICE:
                    return Utilities.GetListofUserRegistration(_RequestObject);




                //case (int)FUNCTION_CODES.DOWNLOAD_ONLINE_DATABASE_OFFLINE:
                //    return Utilities.
                //    return new DownloadDBOnlineForOfflineTableReq(_RequestObject);
                //case (int)FUNCTION_CODES.UPLOAD_OFFLINE_DATABASE_TO_ONLINE_DATABASE:
                //    return new UploadtoOnlineDatabaseReq(_RequestObject);





            }
            return string.Empty;
        }

        private string getPreviousResponseFromCache(string companyId, string userId, string sessionId, string requestId)
        {
            return CacheManager.Get<string>(
                CacheManager.GetKey(
                CacheManager.CacheType.mFicientHttpResponse,
                companyId, userId, "", "",
                sessionId, requestId,
                String.Empty
                ),
                false
                );
            //return  CacheManager.Get(CacheManager.GetKey(CacheManager.CacheType.mFicientHttpResponse, companyId, userId,"","", sessionId, requestId));
        }
        #endregion

        private AUTHENTICATION_RESULT authenticateUserFromCache(string _username, string _companyId, string _sessionId, out SessionUser _session, string _deviceType, string _deviceId, int _functionCode, out string _modelType)
        {
            _session = null;
            _modelType = "";
            if (!IsFunctionWithSessionID(_functionCode)) return AUTHENTICATION_RESULT.CORRECT;

            try
            {
                string cacheKey = CacheManager.GetKey(
                    CacheManager.CacheType.mFicientSession,
                    _companyId, _username,
                    _deviceId, _deviceType,
                    "", "",
                    String.Empty
                    );
                _session = CacheManager.Get<SessionUser>(cacheKey, false);

                if (_session == null) return AUTHENTICATION_RESULT.LOGGED_OUT;
                if (_session.SessionId.Trim().ToUpper() != _sessionId.Trim().ToUpper())
                {
                    _session = CacheManager.Get<SessionUser>(cacheKey, true);
                    if (_session.SessionId.Trim().ToUpper() != _sessionId.Trim().ToUpper())
                    {
                        return AUTHENTICATION_RESULT.SESSION_NOT_EXISTS;
                    }
                }
                if (_session.CheckSessionAndUpdateLastActivity())
                {
                    _modelType = _session.Model;
                    //CacheManager.Insert<SessionUser>(cacheKey,_session,DateTime.UtcNow,TimeSpan.FromTicks(MficientConstants.SESSION_VALIDITY_SECONDS));
                    //HttpRuntime.Cache[cacheKey] = _session;
                    return AUTHENTICATION_RESULT.CORRECT;
                }
                return AUTHENTICATION_RESULT.SESSION_TIMEOUT;
            }
            catch (Exception ex)
            {
                CacheManager.Remove(
                    CacheManager.GetKey(
                    CacheManager.CacheType.mFicientSession, _companyId,
                    _username, _deviceId, _deviceId,
                    "", "", String.Empty
                    )
                    );
                _session = null;
                return AUTHENTICATION_RESULT.LOGGED_OUT;
            }
        }

    }
}