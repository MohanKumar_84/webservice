﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class mGramGetEnterpriseAndMPAgentDetailResp
    {
        ResponseStatus _respStatus;
        mGramEnterpriseAndMPAgentDetailRespRespData _enterpriseDetail;
        string _requestId;

        public mGramGetEnterpriseAndMPAgentDetailResp(ResponseStatus respStatus, mGramEnterpriseAndMPAgentDetailRespRespData enterpriseDetail, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _enterpriseDetail = enterpriseDetail;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error enterprise details Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            mGramEnterpriseAndMPAgentDetailResp objEnterpriseDetailResp = new mGramEnterpriseAndMPAgentDetailResp();
            objEnterpriseDetailResp.func = Convert.ToString((int)FUNCTION_CODES.MGRAM_ENTERPRISE_DETAIL);
            objEnterpriseDetailResp.rid = _requestId;
            objEnterpriseDetailResp.status = _respStatus;
            
            objEnterpriseDetailResp.data = _enterpriseDetail;
            string strJsonWithDetails = Utilities.SerializeJson<mGramEnterpriseAndMPAgentDetailResp>(objEnterpriseDetailResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class mGramEnterpriseAndMPAgentDetailResp : CommonResponse
    {
        public mGramEnterpriseAndMPAgentDetailResp()
        { }
        [DataMember]
        public mGramEnterpriseAndMPAgentDetailRespRespData data { get; set; }
    }

    public class mGramEnterpriseAndMPAgentDetailRespRespData
    {
        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string connm { get; set; }

        [DataMember]
        public string dbnm { get; set; }

        [DataMember]
        public string host { get; set; }

        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string pwd { get; set; }

        [DataMember]
        public string tblnm { get; set; }

        [DataMember]
        public string mpnm { get; set; }

        [DataMember]
        public string mpwd { get; set; }

        [DataMember]
        public int dylmt { get; set; }

        [DataMember]
        public int mnlmt { get; set; }

        [DataMember]
        public string dbtyp { get; set; }

        [DataMember]
        public string adstrg { get; set; }
    }
}