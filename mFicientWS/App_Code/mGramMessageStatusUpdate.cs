﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class mGramMessageStatusUpdate
    {
        mGramMessageStatusUpdateReq _getmGramMessagesReq;
        public mGramMessageStatusUpdate(mGramMessageStatusUpdateReq getmGramMessagesReq)
        {
            _getmGramMessagesReq = getmGramMessagesReq;
        }

        public mGramMessageStatusUpdateResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            string strQuery = "update TBL_MGRAM_MSG set STATUS=" + ((int)MGRAM_MESSAGE_STATUS.READ).ToString() + " where STATUS=1 and MGRAM_MSG_ID=@MGRAM_MSG_ID and  USERNAME=@USERNAME and ENTERPRISE_ID=@ENTERPRISE_ID ;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@USERNAME", _getmGramMessagesReq.UserName);
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _getmGramMessagesReq.CompanyId);
            cmd.Parameters.AddWithValue("@MGRAM_MSG_ID", _getmGramMessagesReq.Message_ID);
            MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd, MficientConstants.strmGramConection);
            return new mGramMessageStatusUpdateResp(_respStatus, _getmGramMessagesReq.RequestId);
        }
    }
}