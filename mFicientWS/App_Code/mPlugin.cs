﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Web.Caching;
using System.Drawing;

namespace mFicientWS
{
    internal class mPlugin
    {
        #region Private Members

        private static volatile mPlugin obj;
        private HttpRequestJson httpRequest;
        private HttpResponseJson objResponse;
        private string response;
        private static int timeOut = 240000;
        
        #endregion

        #region Enumerations

        public enum HttpRequestType
        {
            DATABASE_QUERY = 1,
            DBTEST_CONNECTION = 2,
            WEBSERVICE_QUERY = 3,
            REFRESH_CONNECTORS = 4,
            CHANGE_PASSWORD = 5,
            WSDL_WEBSERVICE_QUERY = 6,
            LIST_OF_COMMANDS = 7,
            DSN_LIST = 8,
            DSN_QUERY = 9,
            DOMAIN_LIST = 10,
            ACTIVE_DIRECTORY_USER = 11,
            PAGING_DATA = 12,
            MGRAM_DATA = 13,
            MGRAM_CONNECTION = 14,
            ODATA_SERVICE_QUERY = 15,
            ODATA_SERVICE_CSDL_QUERY = 16,
            DOWNLOAD_IMAGE = 17,
            GET_MPLUGINAGENT_VERSION = 18,
            PIC_UPLOAD_S3 = 19
        }

        public enum QueryType
        {
            OTHER = 0,
            SELECT = 1,
            INSERT = 2,
            UPDATE = 3,
            DELETE = 4,
            GET = 5,
            POST = 6,
            WSDL = 7,
            SOAP = 8,
            GET_METADATA = 9,
            ACTIVE_DIRECTORY_USER_LOGIN = 10,
            ACTIVE_DIRECTORY_USER_EXISTENCE = 11,
            ACTIVE_DIRECTORY_USER_DETAIL = 12,
            HTTP_PUT = 13,
            HTTP_PATCH = 14,
            HTTP_MERGE = 15,
            HTTP_DELETE = 16,
            CSDL = 17,
            STORED_PROCEDURE = 18
        }

        public enum MetadataType
        {
            ALL = 0,
            TABLES = 1,
            TABLE_COLUMNS = 2,
            TABLE_AND_COLUMNS = 3,
            VIEWS = 4,
            VIEW_COLUMNS = 5,
            VIEWS_AND_COLUMNS = 6,
            STORED_PROCEDURES = 7,
            STORED_PROCEDURE_PARAMS = 8
        }

        public enum Status
        {
            FALSE = 0,
            TRUE = 1
        }

        #endregion

        #region Constructor

        private mPlugin()
        {
            httpRequest = new HttpRequestJson();
            objResponse = new HttpResponseJson();
            response = string.Empty;
        }

        #endregion

        #region Public Methods

        public static bool TestDatabaseConnection(string enterpriseId, string agentName, string agentPassword, DatabaseType databaseType, string connectionString)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.DBTEST_CONNECTION).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.DBTEST_CONNECTION).ToString();
            obj.httpRequest.req.data.con = connectionString;
            obj.httpRequest.req.data.dbtyp = ((int)databaseType).ToString();
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response,0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.result)) == Status.TRUE)
                            return true;
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static void RefreshConnectors(string enterpriseId, string agentName, string agentPassword)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.REFRESH_CONNECTORS).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.REFRESH_CONNECTORS).ToString();
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode != HttpStatusCode.OK)
                throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static void CredentialsChanged(string enterpriseId, string agentName, string agentPassword)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.CHANGE_PASSWORD).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.CHANGE_PASSWORD).ToString();
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response,0);

            if (responseStatus.StatusCode != HttpStatusCode.OK)
                throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());

        }

        public static string GetDatabaseMetaData(string enterpriseId, string agentName, string agentPassword, string connectorId, MetadataType metadataType, string tableName, HttpRequestType httpRequestType, string conUserId, string conPassword)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.uid = conUserId;
            obj.httpRequest.req.data.pwd = conPassword;
            obj.httpRequest.req.data.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.querytype = ((int)QueryType.GET_METADATA).ToString();
            obj.httpRequest.req.data.sql = ((int)metadataType).ToString();
            obj.httpRequest.req.data.icol = "";
            if (metadataType == MetadataType.TABLE_COLUMNS)
            {
                SqlParameters parameters = new SqlParameters();
                parameters.Add("@TABLE_NAME", tableName);

                obj.httpRequest.req.data.param = parameters.Parameters;
            }

            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.records != null && obj.objResponse.resp.data.records.Length > 0)
                        {
                            return obj.objResponse.resp.data.records;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string GetBinaryImageData(string enterpriseId, string agentName, string agentPassword, string imageUrl,string imageMaxWidth)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.DOWNLOAD_IMAGE).ToString();
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.DOWNLOAD_IMAGE).ToString();
            obj.httpRequest.req.data.imgurl = imageUrl;
            obj.httpRequest.req.data.mwd = imageMaxWidth;

            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        return string.IsNullOrEmpty(obj.objResponse.resp.data.imgdata) ? "" : obj.objResponse.resp.data.imgdata;
                    }
                    else //if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else //if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static void UploadFileUsingS3(string enterpriseId, string agentName, string agentPassword, string s3Url, string destinationPath)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId; 
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.PIC_UPLOAD_S3).ToString();
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.PIC_UPLOAD_S3).ToString();
            obj.httpRequest.req.data.s3Url = s3Url;
            obj.httpRequest.req.data.destUrl = destinationPath;

            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string GetmPluginAgentVersionDetailsData(string enterpriseId, string agentName, string agentPassword)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId; 
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.GET_MPLUGINAGENT_VERSION).ToString();
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.GET_MPLUGINAGENT_VERSION).ToString();

            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.verdata != null)
                        {
                            return obj.objResponse.resp.verdata.majver + "." + obj.objResponse.resp.verdata.minver;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string GetDatabaseMetaData(string enterpriseId, string agentName, string agentPassword, string connectorId, MetadataType metadataType, HttpRequestType httpRequestType, string conUserId, string conPassword)
        {
            checkCreateSingleton();

            try
            {
                return GetDatabaseMetaData(enterpriseId, agentName, agentPassword, connectorId, metadataType, string.Empty, httpRequestType, conUserId, conPassword);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet GetDatabaseMetaData(string enterpriseId, string agentName, MetadataType metadataType, string agentPassword, string connectorId, string tableName, HttpRequestType httpRequestType, string conUserId, string conPassword)
        {
            DataSet ds = new DataSet();
            checkCreateSingleton();
            try
            {
                string strMetaData = GetDatabaseMetaData(enterpriseId, agentName, agentPassword, connectorId, metadataType, tableName, httpRequestType, conUserId, conPassword);
                byte[] byteArray = Encoding.UTF8.GetBytes(strMetaData);
                MemoryStream stream = new MemoryStream(byteArray);
                ds.ReadXml(stream);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public static DataSet GetDatabaseMetaData(string enterpriseId, string agentName, MetadataType metadataType, string agentPassword, string connectorId, HttpRequestType httpRequestType, string conUserId, string conPassword)
        {
            DataSet ds = new DataSet();
            checkCreateSingleton();
            try
            {
                string strMetaData = GetDatabaseMetaData(enterpriseId, agentName, agentPassword, connectorId, metadataType, httpRequestType, conUserId, conPassword);
                byte[] byteArray = Encoding.UTF8.GetBytes(strMetaData);
                MemoryStream stream = new MemoryStream(byteArray);
                ds.ReadXml(stream);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public static DataSet RunSelectSqlQuery(string enterpriseId, string agentName, string agentPassword, string connectorId, string sqlQuery, SqlParameters parameters, HttpRequestType httpRequestType, bool isPagingEnabled, int pageNumber, int blockSize, QueryType queryType, out string datasetID, out string totalPages, string conUserId, string conPassword, string userName, string imageColumn)
        {
            checkCreateSingleton();
            DataSet ds = new DataSet();
            datasetID = string.Empty;
            totalPages = string.Empty;

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.uid = conUserId;
            obj.httpRequest.req.data.pwd = conPassword;
            obj.httpRequest.req.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.icol = imageColumn;
            obj.httpRequest.req.data.querytype = ((int)queryType).ToString();
            obj.httpRequest.req.data.sql = sqlQuery;
            obj.httpRequest.req.data.pgen = isPagingEnabled;
            obj.httpRequest.req.data.pgno = pageNumber;
            obj.httpRequest.req.data.blksz = blockSize;
            obj.httpRequest.req.data.param = parameters.Parameters;

            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, timeOut);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.dsid != null)
                            datasetID = obj.objResponse.resp.data.dsid;
                        totalPages = obj.objResponse.resp.data.ttlpg;
                        if (obj.objResponse.resp.data.records != null && obj.objResponse.resp.data.records.Length > 0)
                        {
                            byte[] byteArray = Encoding.UTF8.GetBytes(obj.objResponse.resp.data.records);
                            MemoryStream stream = new MemoryStream(byteArray);
                            ds.ReadXml(stream);
                            return ds;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, obj.response);
        }

        public static DataSet RunSelectSqlQuery(string enterpriseId, string agentName, string agentPassword, string connectorId, string sqlQuery, SqlParameters parameters, HttpRequestType httpRequestType, string conUserId, string conPassword, string userName, string imageColumn)
        {
            checkCreateSingleton();
            DataSet ds = new DataSet();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.uid = conUserId;
            obj.httpRequest.req.data.pwd = conPassword;
            obj.httpRequest.req.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.icol = imageColumn;
            obj.httpRequest.req.data.querytype = ((int)QueryType.SELECT).ToString();
            obj.httpRequest.req.data.sql = sqlQuery;
            obj.httpRequest.req.data.param = parameters.Parameters;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, timeOut);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.records != null && obj.objResponse.resp.data.records.Length > 0)
                        {
                            byte[] byteArray = Encoding.UTF8.GetBytes(obj.objResponse.resp.data.records);
                            MemoryStream stream = new MemoryStream(byteArray);
                            ds.ReadXml(stream);
                            return ds;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, obj.response);
        }

        public static DataSet GetPagingData(string enterpriseId, string agentName, string agentPassword, string datasetID, int Blocksize, int pageNumber, string userName)
        {
            checkCreateSingleton();
            DataSet ds = new DataSet();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.PAGING_DATA).ToString();
            obj.httpRequest.req.data.dsid = datasetID;
            obj.httpRequest.req.data.blksz = Blocksize;
            obj.httpRequest.req.data.pgen = true;
            obj.httpRequest.req.data.pgno = pageNumber;

            obj.httpRequest.req.rtyp = ((int)HttpRequestType.PAGING_DATA).ToString();
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.records != null && obj.objResponse.resp.data.records.Length > 0)
                        {
                            byte[] byteArray = Encoding.UTF8.GetBytes(obj.objResponse.resp.data.records);
                            MemoryStream stream = new MemoryStream(byteArray);
                            ds.ReadXml(stream);
                            return ds;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, ((int)HttpStatusCode.InternalServerError).ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, obj.response);
        }

        public static string RunInsertSqlQuery(string enterpriseId, string agentName, string agentPassword, string connectorId, string sqlQuery, SqlParameters parameters, HttpRequestType httpRequestType, string conUserId, string conPassword, string OutCol, string userName, string imageColumn)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.uid = conUserId;
            obj.httpRequest.req.data.pwd = conPassword;
            obj.httpRequest.req.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.querytype = ((int)QueryType.INSERT).ToString();
            obj.httpRequest.req.data.sql = sqlQuery;
            obj.httpRequest.req.data.actn = OutCol;
            obj.httpRequest.req.data.icol = imageColumn;
            obj.httpRequest.req.data.param = parameters.Parameters;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {

                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        return obj.objResponse.resp.data.records;
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, obj.response);
        }

        public static string RunUpdateSqlQuery(string enterpriseId, string agentName, string agentPassword, string connectorId, string sqlQuery, SqlParameters parameters, HttpRequestType httpRequestType, string conUserId, string conPassword, string OutCol, string userName, string imageColumn)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.uid = conUserId;
            obj.httpRequest.req.data.pwd = conPassword;
            obj.httpRequest.req.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.icol = imageColumn;
            obj.httpRequest.req.data.querytype = ((int)QueryType.UPDATE).ToString();
            obj.httpRequest.req.data.sql = sqlQuery;
            obj.httpRequest.req.data.actn = OutCol;
            obj.httpRequest.req.data.param = parameters.Parameters;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {

                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.reccount != null)
                        {
                            return obj.objResponse.resp.data.records;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, obj.response);
        }

        public static string RunDeleteSqlQuery(string enterpriseId, string agentName, string agentPassword, string connectorId, string sqlQuery, SqlParameters parameters, HttpRequestType httpRequestType, string conUserId, string conPassword, string OutCol, string userName, string imageColumn)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.data.uid = conUserId;
            obj.httpRequest.req.data.pwd = conPassword;
            obj.httpRequest.req.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.rtyp = ((int)httpRequestType).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.querytype = ((int)QueryType.DELETE).ToString();
            obj.httpRequest.req.data.sql = sqlQuery;
            obj.httpRequest.req.data.actn = OutCol;
            obj.httpRequest.req.data.icol = imageColumn;
            obj.httpRequest.req.data.param = parameters.Parameters;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {

                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.reccount != null)
                        {
                            return obj.objResponse.resp.data.records;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, obj.response);
        }

        public static List<DSN> GetDSNList(string enterpriseId, string agentName, string agentPassword)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.DSN_LIST).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.DSN_LIST).ToString();
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if(obj.objResponse.resp.dsn == null) 
                            throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                        return obj.objResponse.resp.dsn;
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static List<string> GetDomainList(string enterpriseId, string agentName, string agentPassword)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.DOMAIN_LIST).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.DOMAIN_LIST).ToString();
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.domn == null)
                            throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                        return obj.objResponse.resp.domn;
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, null);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static bool ActiveDirectoryUserLogin(string enterpriseId, string agentName, string agentPassword, string domainName, string username, string password, out string email)
        {
            checkCreateSingleton();

            email = string.Empty;
            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.ACTIVE_DIRECTORY_USER).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.ACTIVE_DIRECTORY_USER).ToString();
            obj.httpRequest.req.data.querytype = ((int)QueryType.ACTIVE_DIRECTORY_USER_LOGIN).ToString();
            obj.httpRequest.req.data.dnm = domainName;
            obj.httpRequest.req.data.uid = username;
            obj.httpRequest.req.data.pwd = password;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        email = obj.objResponse.resp.data.useremail;
                        return Convert.ToBoolean(obj.objResponse.resp.data.result);
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, null);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static ActiveDirectoryUser ActiveDirectoryUserExistence(string enterpriseId, string agentName, string agentPassword, string domainName, string username, out bool ifExist)
        {
            checkCreateSingleton();
            ifExist = false;
            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.ACTIVE_DIRECTORY_USER).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.ACTIVE_DIRECTORY_USER).ToString();
            obj.httpRequest.req.data.querytype = ((int)QueryType.ACTIVE_DIRECTORY_USER_EXISTENCE).ToString();
            obj.httpRequest.req.data.dnm = domainName;
            obj.httpRequest.req.data.uid = username;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        ifExist = Convert.ToBoolean(obj.objResponse.resp.data.result);
                        return obj.objResponse.resp.user;
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, null);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static ActiveDirectoryUser ActiveDirectoryUserDetail(string enterpriseId, string agentName, string agentPassword, string domainName, string username)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.ACTIVE_DIRECTORY_USER).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.ACTIVE_DIRECTORY_USER).ToString();
            obj.httpRequest.req.data.querytype = ((int)QueryType.ACTIVE_DIRECTORY_USER_DETAIL).ToString();
            obj.httpRequest.req.data.dnm = domainName;
            obj.httpRequest.req.data.uid = username;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.user == null)
                            throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                        return obj.objResponse.resp.user;
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, null);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string RunSOAPWebservice(string enterpriseId, string agentName, string agentPassword, string connectorId, string queryString, string soapAction, string authenticationType, string userId)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userId;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.WEBSERVICE_QUERY).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.WEBSERVICE_QUERY).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.querytype = ((int)QueryType.SOAP).ToString();
            obj.httpRequest.req.data.httpdata = queryString;
            obj.httpRequest.req.data.actn = soapAction;
            obj.httpRequest.req.data.atyp = authenticationType;
            obj.httpRequest.req.data.icol = "";
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)) == HttpStatusCode.OK)
                    {
                        return obj.objResponse.resp.data.httpresponse;
                    }
                    else
                    {
                        //throw new mPluginException((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)));
                        throw new mPluginException(obj.objResponse.resp.data.status, new Exception(obj.objResponse.resp.data.inerrormsg));
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string RunWebservice(string enterpriseId, string agentName, string agentPassword, string connectorId, string queryString, QueryType queryType, string authenticationType, string userId)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userId;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.WEBSERVICE_QUERY).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.WEBSERVICE_QUERY).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.querytype = ((int)queryType).ToString();
            obj.httpRequest.req.data.httpdata = queryString; 
            obj.httpRequest.req.data.atyp = authenticationType;
            obj.httpRequest.req.data.icol = "";
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)// || responseStatus.StatusCode == HttpStatusCode.Accepted
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)) == HttpStatusCode.OK)
                    {
                        return obj.objResponse.resp.data.httpresponse;
                    }
                    else
                    {
                        //throw new mPluginException((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)));
                        throw new mPluginException(obj.objResponse.resp.data.status, new Exception(obj.objResponse.resp.data.inerrormsg));
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string RunWSDLWebservice(string enterpriseId, string agentName, string agentPassword, string url, string userName, string password, string authenticationType, string userId)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userId;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.WSDL_WEBSERVICE_QUERY).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.WSDL_WEBSERVICE_QUERY).ToString();
            obj.httpRequest.req.data.url = url;
            obj.httpRequest.req.data.uid = userName;
            obj.httpRequest.req.data.pwd = password;
            obj.httpRequest.req.data.querytype = ((int)QueryType.WSDL).ToString();
            obj.httpRequest.req.data.httpdata = "wsdl";
            obj.httpRequest.req.data.atyp = authenticationType;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)) == HttpStatusCode.OK)
                    {
                        return obj.objResponse.resp.data.httpresponse;
                    }
                    else
                    {
                        //throw new mPluginException((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)));
                        throw new mPluginException(obj.objResponse.resp.data.status, new Exception(obj.objResponse.resp.data.inerrormsg));
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string RunODataWebservice(string enterpriseId, string agentName, string agentPassword, string connectorId, QueryType queryType, string httpData, ODataRequestType odataRequestType, ResponseType odataResponseType, string url, FunctionType functionType, string authenticationType, string userName)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = userName;
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.ODATA_SERVICE_QUERY).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.ODATA_SERVICE_QUERY).ToString();
            obj.httpRequest.req.data.connid = connectorId;
            obj.httpRequest.req.data.querytype = ((int)queryType).ToString();
            obj.httpRequest.req.data.url = url;
            obj.httpRequest.req.data.httpdata = httpData;
            obj.httpRequest.req.data.odatafrmt = ((int)odataResponseType).ToString();
            obj.httpRequest.req.data.odatatyp = ((int)odataRequestType).ToString();
            obj.httpRequest.req.data.odatafunctyp = ((int)functionType).ToString();
            obj.httpRequest.req.data.httpdata = httpData;
            obj.httpRequest.req.data.atyp = authenticationType;
            obj.httpRequest.req.data.icol = "";
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)) == HttpStatusCode.OK)
                    {
                        return obj.objResponse.resp.data.httpresponse;
                    }
                    else
                    {
                        //throw new mPluginException((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)));
                        throw new mPluginException(obj.objResponse.resp.data.status, new Exception(obj.objResponse.resp.data.inerrormsg));
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string GetODataCSDL(string enterpriseId, string agentName, string agentPassword, string url, string userName, string password, string authenticationType)
        {
            checkCreateSingleton();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.ODATA_SERVICE_CSDL_QUERY).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.ODATA_SERVICE_CSDL_QUERY).ToString();
            obj.httpRequest.req.data.url = url;
            obj.httpRequest.req.data.uid = userName;
            obj.httpRequest.req.data.pwd = password;
            obj.httpRequest.req.data.querytype = ((int)QueryType.CSDL).ToString();
            obj.httpRequest.req.data.httpdata = "$metadata";
            obj.httpRequest.req.data.atyp = authenticationType;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)) == HttpStatusCode.OK)
                    {
                        return obj.objResponse.resp.data.httpresponse;
                    }
                    else
                    {
                        //throw new mPluginException((HttpStatusCode)(Convert.ToInt32(obj.objResponse.resp.data.status)));
                        throw new mPluginException(obj.objResponse.resp.data.status, new Exception(obj.objResponse.resp.data.inerrormsg));
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }
            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        public static string RunmGramSqlQuery(string enterpriseId, string agentName, string agentPassword, string connectionString, string sqlQuery, SqlParameters parameters, DatabaseType dbType)
        {
            checkCreateSingleton();
            //DataSet ds = new DataSet();

            obj.response = string.Empty;
            obj.objResponse = new HttpResponseJson();

            obj.httpRequest = new HttpRequestJson();
            obj.httpRequest.req.rid = obj.getRequestID();
            obj.httpRequest.req.eid = enterpriseId;
            obj.httpRequest.req.unm = "sub-admin";
            obj.httpRequest.req.mpnm = agentName;
            obj.httpRequest.req.mpwd = agentPassword;
            obj.httpRequest.req.rtyp = ((int)HttpRequestType.MGRAM_DATA).ToString();
            obj.httpRequest.req.data.rtyp = ((int)HttpRequestType.MGRAM_DATA).ToString();
            obj.httpRequest.req.data.con = connectionString;
            obj.httpRequest.req.data.querytype = ((int)QueryType.SELECT).ToString();
            obj.httpRequest.req.data.sql = sqlQuery;
            obj.httpRequest.req.data.dbtyp = ((int)dbType).ToString(); 
            obj.httpRequest.req.data.param = parameters.Parameters;
            string requestJson = obj.serializeJson<HttpRequestJson>(obj.httpRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response, 0);

            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                if (!string.IsNullOrEmpty(obj.response))
                {
                    obj.objResponse = Utilities.DeserialiseJson<HttpResponseJson>(obj.response.Trim());
                    if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.FALSE)
                    {
                        if (obj.objResponse.resp.data.records != null && obj.objResponse.resp.data.records.Length > 0)
                        {
                            return obj.objResponse.resp.data.records;
                        }
                    }
                    else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.error)) == Status.TRUE)
                    {
                        if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.TRUE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg, new Exception(obj.objResponse.resp.data.inerrormsg));
                        else if ((Status)(Convert.ToInt32(obj.objResponse.resp.data.inerror)) == Status.FALSE)
                            throw new mPluginException(obj.objResponse.resp.data.errormsg);
                    }
                }
                else
                {
                    throw new HttpException((int)HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString());
                }
            }

            throw new HttpException((int)responseStatus.StatusCode, responseStatus.StatusCode.ToString());
        }

        #endregion

        #region Private Methods

        private static void checkCreateSingleton()
        {
            if (obj == null)
                obj = new mPlugin();
        }

        private string getRequestID()
        {
            Random random = new Random();
            string strRequestID = random.Next(99, 9999).ToString() + DateTime.UtcNow.Ticks.ToString().Substring(0, 16);
            return strRequestID;
        }

        private HttpResponseStatus callmPlugin(string enterpriseId, string requestJson, out string responseJson,int timeOut)
        {            
            responseJson = "";
            HttpResponseStatus oResponse = new HttpResponseStatus(true, HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString(), string.Empty);
            string jsonData = UrlEncode(requestJson);
            try
            {
                string Url = getmPluginUrl(enterpriseId);
               // string Url = "192.168.1.104";
                if (String.IsNullOrEmpty(Url))
                {
                    oResponse.StatusCode = HttpStatusCode.NotFound;
                    responseJson = "mPlugin Server Not Found";
                    return oResponse;
                }
                HTTP oHttp = new HTTP("https://" + Url + "?d=" + jsonData);
                oHttp.HttpRequestMethod = WebRequestMethods.Http.Post;
                oHttp.UserAgent = @"mFicient-WebServices";
                if (timeOut == 0)
                    oHttp.Timeout = 30000;
                else
                    oHttp.Timeout = timeOut;
                oHttp.Retry = 0;
                oResponse = oHttp.Request();
                
                if (oResponse.StatusCode == HttpStatusCode.OK)
                {
                    responseJson = oResponse.ResponseText;
                }
                else
                {
                    responseJson = oHttp.ErrorMessage;
                }
            }
            catch(Exception ex)
            {
                oResponse.StatusCode = HttpStatusCode.InternalServerError;
                responseJson =  ex.Message;
            }
            return oResponse;
        }

        private string getmPluginUrl(string enterpriseId)
        {
            try
            {
                string strQuery = @"SELECT *  FROM ADMIN_TBL_MST_MP_SERVER AS MPServer
                                LEFT OUTER JOIN ADMIN_TBL_SERVER_MAPPING AS ServerMap
                                ON MPServer.MP_SERVER_ID = ServerMap.MPLUGIN_SERVER_ID
                                WHERE ServerMap.COMPANY_ID = @COMPANY_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                DataSet ds = mFicientWS.MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["MP_HTTP_URL"].ToString();
                }
            }
            catch
            {

            }
            return string.Empty;
        }

        //private DataBaseConnector getDataBaseConnector(string enterpriseId, string connectorId,string userId, string Password)
        //{
        //    DataBaseConnector connector = new DataBaseConnector(connectorId, enterpriseId,userId, Password);
        //    try
        //    {
        //        connector.GetConnector();
        //    }
        //    catch
        //    {
        //    }
        //    return connector;
        //}

        private string serializeJson<T>(object obj)
        {
            T objRequest = (T)obj;
            MemoryStream stream = new MemoryStream();
            StreamReader streamReader = null;
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objRequest.GetType());
                serializer.WriteObject(stream, objRequest);
                stream.Position = 0;
                streamReader = new StreamReader(stream);
            }
            catch
            {

            }
            return streamReader.ReadToEnd();
        }

        private T deserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            try
            {
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
                obj = (T)serialiser.ReadObject(ms);
                ms.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(@"Error in deserialising Json [ " + ex.Message + " ]");
            }
            return obj;
        }

        #endregion

        #region Http Utility

        public static string UrlEncode(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return HttpUtility.UrlEncode(s).Replace(@"'", @"%27");
        }

        #endregion

        #region Private Classes

        [DataContract]
        private class HttpRequestJson
        {
            public HttpRequestJson()
            {
                this.req = new HttpRequestData();
            }

            [DataMember]
            public HttpRequestData req { get; set; }
        }

        [DataContract]
        private class HttpRequestData
        {
            public HttpRequestData()
            {
                this.data = new RequestData();
                this.unm = "";
            }

            [DataMember]
            public string rid { get; set; }

            [DataMember]
            public string eid { get; set; }

            [DataMember]
            public string mpwd { get; set; }

            [DataMember]
            public string mpnm { get; set; }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public RequestData data { get; set; }

            [DataMember]
            public string unm { get; set; }

            [DataMember]
            public string pwd { get; set; }
        }

        [DataContract]
        private class RequestData
        {
            public RequestData()
            {
                this.param = new List<SqlParameter>();
            }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public bool pgen { get; set; }

            [DataMember]
            public int pgno { get; set; }

            [DataMember]
            public int ttlpg { get; set; }

            [DataMember]
            public int blksz { get; set; }

            [DataMember]
            public string dsid { get; set; }

            [DataMember]
            public string connid { get; set; }

            [DataMember]
            public string imgurl { get; set; }

            [DataMember]
            public string mwd { get; set; }

            [DataMember]
            public string dbtyp { get; set; }

            [DataMember]
            public string con { get; set; }

            [DataMember]
            public string url { get; set; }

            [DataMember]
            public string actn { get; set; }

            [DataMember]
            public string uid { get; set; }

            [DataMember]
            public string pwd { get; set; }

            [DataMember]
            public string atyp { get; set; }

            [DataMember]
            public string dnm { get; set; }

            [DataMember]
            public string querytype { get; set; }

            [DataMember]
            public string sql { get; set; }

            [DataMember]
            public List<SqlParameter> param { get; set; }

            [DataMember]
            public string httpdata { get; set; }

            [DataMember]
            public string odatafrmt { get; set; }

            [DataMember]
            public string odatatyp { get; set; }

            [DataMember]
            public string odatafunctyp { get; set; }

            [DataMember]
            public string s3Url { get; set; }

            [DataMember]
            public string destUrl { get; set; }

            [DataMember]
            public string icol { get; set; }
        }

        [DataContract]
        private class HttpResponseJson
        {
            public HttpResponseJson()
            {
                this.resp = new HttpResponseData();
            }

            [DataMember]
            public HttpResponseData resp { get; set; }
        }

        [DataContract]
        private class HttpResponseData
        {
            public HttpResponseData()
            {
                this.data = new ResponseData();
                this.dsn = new List<DSN>();
                this.user = new ActiveDirectoryUser();
                this.domn = new List<string>();
            }

            [DataMember]
            public string rid { get; set; }

            [DataMember]
            public string eid { get; set; }

            [DataMember]
            public string mpnm { get; set; }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public ResponseData data { get; set; }

            [DataMember]
            public List<DSN> dsn { get; set; }

            [DataMember]
            public List<string> domn { get; set; }

            [DataMember]
            public ActiveDirectoryUser user { get; set; }

            [DataMember]
            public GetAgentVersionResponseData verdata { get; set; }
        }

        [DataContract]
        private class ResponseData
        {
            [DataMember]
            public string idx { get; set; }

            [DataMember]
            public string dsid { get; set; }

            [DataMember]
            public string ttlpg { get; set; }

            [DataMember]
            public string error { get; set; }

            [DataMember]
            public string errormsg { get; set; }

            [DataMember]
            public string inerror { get; set; }

            [DataMember]
            public string inerrormsg { get; set; }

            [DataMember]
            public string reccount { get; set; }

            [DataMember]
            public string records { get; set; }

            [DataMember]
            public string status { get; set; }

            [DataMember]
            public string httpresponse { get; set; }

            [DataMember]
            public string result { get; set; }

            [DataMember]
            public string imgdata { get; set; }

            [DataMember]
            public string useremail { get; set; }
        }

        [DataContract]
        private class GetAgentVersionResponseData
        {
            public GetAgentVersionResponseData()
            { }

            [DataMember]
            public string minver { get; set; }

            [DataMember]
            public string majver { get; set; }

            [DataMember]
            public string dwnurl { get; set; }

            [DataMember]
            public string ismand { get; set; }
        }

        #endregion

        [DataContract]
        public class SqlParameters : IEnumerable<SqlParameter>
        {
            private List<SqlParameter> parameters;

            public SqlParameters()
            {
                parameters = new List<SqlParameter>();
            }

            public void Add(string paramName, string paramValue)
            {
                if (string.IsNullOrEmpty(paramName))
                    throw new ArgumentNullException();
                parameters.Add(new SqlParameter(paramName, paramValue));
            }

            public int Count()
            {
                return parameters.Count;
            }

            [DataMember]
            public List<SqlParameter> Parameters
            {
                get
                {
                    return parameters;
                }
            }

            #region IEnumerable<SqlParameter> Members

            public IEnumerator<SqlParameter> GetEnumerator()
            {
                return this.parameters.GetEnumerator();
            }

            #endregion

            #region IEnumerable Members

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            #endregion
        }

        [DataContract]
        public class SqlParameter
        {
            private string paramName, paramValue;

            public SqlParameter(string parameterName, string parameterValue)
            {
                paramName = parameterName;
                paramValue = parameterValue;
            }

            [DataMember]
            public string pname
            {
                get
                {
                    return paramName;
                }
                set
                {
                    paramName = value;
                }
            }

            [DataMember]
            public string pvalue
            {
                get
                {
                    return paramValue;
                }
                set
                {
                    paramValue = value;
                }
            }
        }

        [Serializable]
        public class mPluginException : Exception
        {
            public mPluginException() { }
            public mPluginException(string message) : base(message) { }
            public mPluginException(string message, Exception inner) : base(message, inner) { }
            public mPluginException(HttpStatusCode statusCode) { }

            protected mPluginException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context)
                : base(info, context) { }
        }
    }

    [DataContract]
    public class DSN
    {
        public DSN()
        {
        }

        [DataMember]
        public string dsName { get; set; }

        [DataMember]
        public string driver { get; set; }
    }

    [DataContract]
    public class ActiveDirectoryUser
    {

        internal ActiveDirectoryUser()
        {
        }

        [DataMember]
        public string fnm
        {
            get;
            set;
        }

        [DataMember]
        public string lnm
        {
            get;
            set;
        }

        [DataMember]
        public string unm
        {
            get;
            set;
        }

        [DataMember]
        public string dpt
        {
            get;
            set;
        }

        [DataMember]
        public string dsg
        {
            get;
            set;
        }

        [DataMember]
        public string email
        {
            get;
            set;
        }

        [DataMember]
        public string mbl
        {
            get;
            set;
        }
    }
}