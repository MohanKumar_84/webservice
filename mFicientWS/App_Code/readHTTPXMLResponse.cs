﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections;
using System.Xml.XPath;
using System.Data;
namespace mFicientWS
{
    public class readHTTPXMLResponse
    {
        DoTaskReq _doTaskRequest;
        string _responseXML;
        List<IdeWsParam> _outputTagPaths;
        WebServiceCommand _wsCmd;
        bool _isForReturnFunctionZero;
        OdataCommand _odataCmd;

        


        public readHTTPXMLResponse(DoTaskReq doTaskRequest,
            string responseXML,
            WebServiceCommand wsCmd)
        {
            _doTaskRequest = doTaskRequest;
            _responseXML = responseXML;
            _outputTagPaths = wsCmd.OutPutTagPaths;
            _wsCmd = wsCmd;
            _isForReturnFunctionZero = false;
            _odataCmd = null;
        }
        /// <summary>
        /// Use this for Return function 0 
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="wsCmd"></param>
        public readHTTPXMLResponse(string responseXML,
            WebServiceCommand wsCmd)
        {
            _isForReturnFunctionZero = true;
            _doTaskRequest = null;
            _outputTagPaths = wsCmd.OutPutTagPaths;
            _responseXML = responseXML;
            _wsCmd = wsCmd;
            _odataCmd = null;
        }

        public readHTTPXMLResponse(DoTaskReq doTaskRequest,
            string responseXML,
            OdataCommand odataCmd)
        {
            _doTaskRequest = doTaskRequest;
            _responseXML = responseXML;
            _outputTagPaths = odataCmd.OutputTagPath;
            _wsCmd = null;
            _isForReturnFunctionZero = false;
            _odataCmd = odataCmd;
        }
        /// <summary>
        /// Use this for Return function 0  Odata Command
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="wsCmd"></param>
        public readHTTPXMLResponse(string responseXML,
            OdataCommand odataCmd)
        {
            _isForReturnFunctionZero = true;
            _doTaskRequest = null;
            _outputTagPaths = odataCmd.OutputTagPath;
            _responseXML = responseXML;
            _wsCmd = null;
            _odataCmd = odataCmd;
        }
        public Object Process()
        {
            try
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(_responseXML);
                if (this.IsForReturnFunctionZero)
                {
                    return getDataForReturnFunctionZero(document,
                          this.WsCmd);
                }
            }
            catch
            {
            }
            return null;
        }
        public Object ProcessForOdata()
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(_responseXML);
            string strDatasetPath = _odataCmd.DataSetPath;
            if (this.IsForReturnFunctionZero)
            {
                return getDataForReturnFunctionZero(document,
                      this.OutputTagPaths,
                      strDatasetPath);
            }
            return null;
        }

        DataTable getDataForReturnFunctionZero(XmlDocument document,
            WebServiceCommand wscmd)
        {
            /***
             * for each final element tag find the number of node list
             * save each node list in a hashtable
             * where key is the name of the element tag in the finalElementTag
             * Use variable the node list with max count
             * and the index of element having that has max count
             * so to form the datatable the loop should be run that many times for each final element 
             * Normally the count of each tag should match(because of restriction in Control Panel)
             * But still if it does'nt then leave blank for those where count is less.
             * ***/
            int iLargestCountOfTags = 0; int iIndexOfFinalElementHavingMaxCount = 0;
            Hashtable hshOfNodeList = new Hashtable();
            DataTable dtblForReturning = new DataTable();
            XmlNodeList nodeList;
            int iLoopCount = 0;
            foreach (IdeWsParam outputTag in wscmd.OutPutTagPaths)
            {
                //creating the data table to return.
                dtblForReturning.Columns.Add(outputTag.name, typeof(string));
                string tagPath = getModifiedPathOfTagForHttpXML(getCompleteOutputTagPath(wscmd.DataSetPath, outputTag.name,this.OutputTagPaths));
                 //tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, finalTag);
                try
                {
                    if (!String.IsNullOrEmpty(tagPath))
                    {
                        nodeList = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
                        iLargestCountOfTags = iLargestCountOfTags < nodeList.Count ? nodeList.Count : iLargestCountOfTags;
                        if (nodeList.Count > iLargestCountOfTags)
                        {
                            iLargestCountOfTags = nodeList.Count;
                            iIndexOfFinalElementHavingMaxCount = iLoopCount;
                        }
                        hshOfNodeList.Add(outputTag.name, nodeList);
                    }
                    iLoopCount++;
                }
                catch
                {
                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                }
            }
            for (int i = 0; i <= iLargestCountOfTags - 1; i++)
            {
                DataRow newRow = dtblForReturning.NewRow();
                dtblForReturning.Rows.Add(newRow);
            }
            foreach (DictionaryEntry hashEntry in hshOfNodeList)
            {
                nodeList = (XmlNodeList)hashEntry.Value;
                string strColumnName = (string)hashEntry.Key;
                for (int i = 0; i <= iLargestCountOfTags - 1; i++)
                {
                    DataRow row = dtblForReturning.Rows[i];
                    row[strColumnName] = nodeList.Count - 1 >= i ? nodeList[i].InnerText : String.Empty;//count is always 1 greater than 0 based loop.
                }
            }

            return dtblForReturning;

        }
        DataTable getDataForReturnFunctionZero(XmlDocument document,
            List<IdeWsParam> outputTagPath,
            string datasetPath
            )
        {
            /***
             * for each final element tag find the number of node list
             * save each node list in a hashtable
             * where key is the name of the element tag in the finalElementTag
             * Use variable the node list with max count
             * and the index of element having that has max count
             * so to form the datatable the loop should be run that many times for each final element 
             * Normally the count of each tag should match(because of restriction in Control Panel)
             * But still if it does'nt then leave blank for those where count is less.
             * ***/
            int iLargestCountOfTags = 0; int iIndexOfFinalElementHavingMaxCount = 0;
            Hashtable hshOfNodeList = new Hashtable();
            DataTable dtblForReturning = new DataTable();
            XmlNodeList nodeList;
            int iLoopCount = 0;
            foreach (IdeWsParam outputTag in outputTagPath)
            {
                //creating the data table to return.
                dtblForReturning.Columns.Add(outputTag.name, typeof(string));
                string tagPath = getModifiedPathOfTagForHttpXML(getCompleteOutputTagPath(datasetPath, outputTag.name, this.OutputTagPaths));
                //tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, finalTag);
                try
                {
                    if (!String.IsNullOrEmpty(tagPath))
                    {
                        nodeList = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
                        iLargestCountOfTags = iLargestCountOfTags < nodeList.Count ? nodeList.Count : iLargestCountOfTags;
                        if (nodeList.Count > iLargestCountOfTags)
                        {
                            iLargestCountOfTags = nodeList.Count;
                            iIndexOfFinalElementHavingMaxCount = iLoopCount;
                        }
                        hshOfNodeList.Add(outputTag.name, nodeList);
                    }
                    iLoopCount++;
                }
                catch
                {
                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                }
            }
            for (int i = 0; i <= iLargestCountOfTags - 1; i++)
            {
                DataRow newRow = dtblForReturning.NewRow();
                dtblForReturning.Rows.Add(newRow);
            }
            foreach (DictionaryEntry hashEntry in hshOfNodeList)
            {
                nodeList = (XmlNodeList)hashEntry.Value;
                string strColumnName = (string)hashEntry.Key;
                for (int i = 0; i <= iLargestCountOfTags - 1; i++)
                {
                    DataRow row = dtblForReturning.Rows[i];
                    row[strColumnName] = nodeList.Count - 1 >= i ? nodeList[i].InnerText : String.Empty;//count is always 1 greater than 0 based loop.
                }
            }

            return dtblForReturning;

        }
        
        string getModifiedPathOfTagForHttpXML(string completeTagPath)
        {
            string strModifiedTagPath = String.Empty;
            string strLocalnamePrefix = "/*[local-name()='";
            string strLocalNameSuffix = "']";
            if (!String.IsNullOrEmpty(completeTagPath))
            {
                string[] arySplitTagPath = completeTagPath.Split('/');
                Hashtable hashOfTagsFromed = new Hashtable();
                for (int i = 0; i < arySplitTagPath.Length; i++)
                {
                    if (strModifiedTagPath.Length == 0)
                    {
                        strModifiedTagPath += "//*[local-name()='" + arySplitTagPath[i] + strLocalNameSuffix;
                    }
                    else
                    {
                        strModifiedTagPath += strLocalnamePrefix + arySplitTagPath[i] + strLocalNameSuffix;
                    }
                    hashOfTagsFromed.Add(arySplitTagPath[i], arySplitTagPath[i]);
                }
            }
            return strModifiedTagPath;
        }


        string getCompleteOutputTagPath(string datasetPath,
            string nameOfOutputTag
            , List<IdeWsParam> outputPathsLst)
        {
            string strPath = String.Empty;
            if (this.WsCmd != null)
            {
                if (!String.IsNullOrEmpty(datasetPath))
                {
                    strPath = datasetPath.Replace('.', '/').Replace("[]", "") +"/" + getOutputPathOfTagFromPathList(nameOfOutputTag, outputPathsLst);
                }
                else
                {
                    strPath = getOutputPathOfTagFromPathList(nameOfOutputTag, outputPathsLst); ;
                }
            }
            else if (this.OdataCmd != null)
            {
                if (!String.IsNullOrEmpty(datasetPath))
                {
                    strPath= getTheDatasetPathWithoutTheFinalArrayTag(datasetPath) 
                        + "/" 
                        + getOutputPathOfTagFromPathList(nameOfOutputTag, outputPathsLst);
                }
                else
                {
                    strPath = getOutputPathOfTagFromPathList(nameOfOutputTag, outputPathsLst); ;
                }
            }
            return strPath;
        }

        string getOutputPathOfTagFromPathList(string nameOfOutputTag, List<IdeWsParam> outputPathsLst)
        {
            string strOutputTagPath = String.Empty;
            foreach (IdeWsParam outputTagAndPath in outputPathsLst)
            {
                //Comparing the string as it is sent from the mobile end.Not converting toLower in case there is an error
                //from mobile end.
                if (outputTagAndPath.name == nameOfOutputTag)
                {
                    if (WsCmd != null)
                    {
                        strOutputTagPath = outputTagAndPath.path.Replace('.', '/');
                        strOutputTagPath = strOutputTagPath.Replace("[]", "");
                    }
                    else if (OdataCmd != null)
                        strOutputTagPath = outputTagAndPath.path.Replace('.', '/');
                    break;
                }
                else
                    continue;
            }
            return strOutputTagPath;
        }


        string getTheDatasetPathWithoutTheFinalArrayTag(string datasetPath)
        {
            /****
             * If Dataset Path is present then the last tag provided will be an array ending as Tagname[].
             * Eg: GetProductsResponse.GetProductsResult.Distributor[]
             * Result Path:Distributor.Name
             * Removing the last Distributor[] tag.
             * ****/
            if (String.IsNullOrEmpty(datasetPath)) throw new ArgumentNullException();
            //string strDataSetPath = datasetPath.Replace('.', '/');
            //   strDataSetPath=  strDataSetPath.Substring(0,
            //    strDataSetPath.Length - (strDataSetPath.Substring(strDataSetPath.LastIndexOf('/')).Length + 1));
            string strDataSetPath = String.Empty;
            string[] aryStrSplitValues = datasetPath.Replace('.', '/').Split('/');
            int iLengthOfAryAfterSplit = aryStrSplitValues.Length;
            //Normally we should not get this case
            if (iLengthOfAryAfterSplit == 1) return strDataSetPath;
            if (aryStrSplitValues.Length > 1)
            {
                for (int i = 0; i <= iLengthOfAryAfterSplit - 2; i++)
                {
                    if (String.IsNullOrEmpty(strDataSetPath))
                    {
                        strDataSetPath += aryStrSplitValues[i];
                    }
                    else
                    {
                        strDataSetPath += "/" + aryStrSplitValues[i];
                    }
                }
            }
            else
            {
                strDataSetPath = String.Empty;
            }
            return strDataSetPath;
        }

        #region Public Properties
        public List<IdeWsParam> OutputTagPaths
        {
            get { return _outputTagPaths; }
            private set { _outputTagPaths = value; }
        }

        public WebServiceCommand WsCmd
        {
            get { return _wsCmd; }
            private set { _wsCmd = value; }
        }
        public bool IsForReturnFunctionZero
        {
            get { return _isForReturnFunctionZero; }
            private set { _isForReturnFunctionZero = value; }
        }
        public OdataCommand OdataCmd
        {
            get { return _odataCmd; }
            private set { _odataCmd = value; }
        }
        #endregion
    }
}