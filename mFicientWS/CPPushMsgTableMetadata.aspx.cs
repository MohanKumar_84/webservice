﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
namespace mFicientWS
{
    public partial class CPPushMsgTableMetadata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // HttpContext appContext = ((HttpApplication)sender).Context;
            int intHttpStatusCode = (int)HttpStatusCode.OK;
            int intFunctionCode = 9990;
            string strResponse = "", strRequest = "";
            try
            {
                if (Request.HttpMethod == "GET")
                {
                    strRequest = Request.QueryString.Get("d");
                }
                else if (Request.HttpMethod == "POST")
                {
                    strRequest = Request.Form.Get("d");
                }
                else
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                if (string.IsNullOrEmpty(strRequest))
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
               
                    CPGetPushMsgMetaDataReq objPushMsgMetaDataReq =
                         new CPGetPushMsgMetaDataReq(strRequest);
                    CPGetPushMsgMetaData objPushMsgMetaData =
                        new CPGetPushMsgMetaData(objPushMsgMetaDataReq);
                  CPGetPushMsgMetaDataResp objPushMsgMetaDataResp=  objPushMsgMetaData.Process();
                  strResponse =objPushMsgMetaDataResp.GetResponseJson();
            }
            catch (Exception ex)
            {
                int intParseResult = 0;
                if (int.TryParse(ex.Message, out intParseResult))
                {
                    if (Enum.IsDefined(typeof(HttpStatusCode), intParseResult))
                    {
                        intHttpStatusCode = intParseResult;
                    }
                    else if (intParseResult == (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    }
                    else if (intParseResult == (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.NoContent;
                    }
                    else if (intParseResult == MficientConstants.INVALID_REQUEST_JSON)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    else if (intParseResult == (int)HttpStatusCode.Unauthorized)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
                else
                {
                    if (ex.Source == "System.Runtime.Serialization")
                    {
                        intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
            }
            finally
            {
                HttpContext.Current.Items.Add("FUNCTION_CODE", intFunctionCode);
                HttpContext.Current.Items.Add("HTTP_STATUS_CODE", intHttpStatusCode);
                HttpContext.Current.Items.Add("RESPONSE_XML", strResponse);
                HttpContext.Current.Items.Add("REQUEST_ID", "CPPushMsgTableMetadataID");
            }
        
        }
    }
}